-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1.1 (lin64) Build 2580384 Sat Jun 29 08:04:45 MDT 2019
-- Date        : Wed Nov 27 11:41:10 2019
-- Host        : laoshu running 64-bit unknown
-- Command     : write_vhdl -force -mode funcsim -rename_top system_AXI_PI_0_2 -prefix
--               system_AXI_PI_0_2_ system_AXI_PI_0_2_sim_netlist.vhdl
-- Design      : system_AXI_PI_0_2
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_AXI_PI_0_2_pid_loop is
  port (
    \control_reg_reg[1]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \autolock_max_reg_reg[30]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \I_term_q_reg[30]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    I_term_mon : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \loop_output_reg_reg[31]_0\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \loop_output_reg_reg[31]_1\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    ramp_module_0_reset : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \error_reg_reg[13]_0\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    P_error_unshifted_reg_reg_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \I_term_q_reg[0]_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    pid_loop_0_input_reg : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \I_term_q_reg[31]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \autolock_input_maxdex_reg[13]\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \axi_pi_output_reg_reg[13]_inv_i_3_0\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \axi_pi_output_reg_reg[13]_inv_i_4_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \axi_pi_output_reg_reg[13]_inv_i_3_1\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \ramplitude_min_reg_reg[31]_i_6_0\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \autolock_input_maxdex_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \setpoint_reg_reg[13]_0\ : in STD_LOGIC_VECTOR ( 13 downto 0 )
  );
end system_AXI_PI_0_2_pid_loop;

architecture STRUCTURE of system_AXI_PI_0_2_pid_loop is
  signal \^co\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^d\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal I_error : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal I_error_unshifted_reg0_n_100 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_101 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_102 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_103 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_104 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_105 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_106 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_107 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_108 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_109 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_110 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_111 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_112 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_113 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_114 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_115 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_116 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_117 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_118 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_119 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_120 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_121 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_122 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_123 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_124 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_125 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_126 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_127 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_128 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_129 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_130 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_131 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_132 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_133 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_134 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_135 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_136 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_137 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_138 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_139 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_140 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_141 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_142 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_143 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_144 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_145 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_146 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_147 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_148 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_149 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_150 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_151 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_152 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_153 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_58 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_59 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_60 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_61 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_62 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_63 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_64 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_65 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_66 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_67 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_68 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_69 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_70 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_71 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_72 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_73 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_74 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_75 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_76 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_77 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_78 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_79 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_80 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_81 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_82 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_83 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_84 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_85 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_86 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_87 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_88 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_89 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_90 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_91 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_92 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_93 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_94 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_95 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_96 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_97 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_98 : STD_LOGIC;
  signal I_error_unshifted_reg0_n_99 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_58 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_59 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_60 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_61 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_62 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_63 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_64 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_65 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_66 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_67 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_68 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_69 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_70 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_71 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_72 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_73 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_74 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_75 : STD_LOGIC;
  signal I_error_unshifted_reg_reg_n_76 : STD_LOGIC;
  signal \I_mon_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \I_mon_reg[11]_i_3_n_0\ : STD_LOGIC;
  signal \I_mon_reg[11]_i_4_n_0\ : STD_LOGIC;
  signal \I_mon_reg[11]_i_5_n_0\ : STD_LOGIC;
  signal \I_mon_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \I_mon_reg[15]_i_3_n_0\ : STD_LOGIC;
  signal \I_mon_reg[15]_i_4_n_0\ : STD_LOGIC;
  signal \I_mon_reg[15]_i_5_n_0\ : STD_LOGIC;
  signal \I_mon_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \I_mon_reg[19]_i_3_n_0\ : STD_LOGIC;
  signal \I_mon_reg[19]_i_4_n_0\ : STD_LOGIC;
  signal \I_mon_reg[19]_i_5_n_0\ : STD_LOGIC;
  signal \I_mon_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \I_mon_reg[23]_i_3_n_0\ : STD_LOGIC;
  signal \I_mon_reg[23]_i_4_n_0\ : STD_LOGIC;
  signal \I_mon_reg[23]_i_5_n_0\ : STD_LOGIC;
  signal \I_mon_reg[27]_i_2_n_0\ : STD_LOGIC;
  signal \I_mon_reg[27]_i_3_n_0\ : STD_LOGIC;
  signal \I_mon_reg[27]_i_4_n_0\ : STD_LOGIC;
  signal \I_mon_reg[27]_i_5_n_0\ : STD_LOGIC;
  signal \I_mon_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \I_mon_reg[31]_i_3_n_0\ : STD_LOGIC;
  signal \I_mon_reg[31]_i_4_n_0\ : STD_LOGIC;
  signal \I_mon_reg[31]_i_5_n_0\ : STD_LOGIC;
  signal \I_mon_reg[31]_i_6_n_0\ : STD_LOGIC;
  signal \I_mon_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \I_mon_reg[3]_i_3_n_0\ : STD_LOGIC;
  signal \I_mon_reg[3]_i_4_n_0\ : STD_LOGIC;
  signal \I_mon_reg[3]_i_5_n_0\ : STD_LOGIC;
  signal \I_mon_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \I_mon_reg[7]_i_3_n_0\ : STD_LOGIC;
  signal \I_mon_reg[7]_i_4_n_0\ : STD_LOGIC;
  signal \I_mon_reg[7]_i_5_n_0\ : STD_LOGIC;
  signal \I_mon_reg_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \I_mon_reg_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \I_mon_reg_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \I_mon_reg_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \I_mon_reg_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \I_mon_reg_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \I_mon_reg_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \I_mon_reg_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \I_mon_reg_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \I_mon_reg_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \I_mon_reg_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \I_mon_reg_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \I_mon_reg_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \I_mon_reg_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \I_mon_reg_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \I_mon_reg_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \I_mon_reg_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \I_mon_reg_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \I_mon_reg_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \I_mon_reg_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \I_mon_reg_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \I_mon_reg_reg[31]_i_1_n_1\ : STD_LOGIC;
  signal \I_mon_reg_reg[31]_i_1_n_2\ : STD_LOGIC;
  signal \I_mon_reg_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \I_mon_reg_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \I_mon_reg_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \I_mon_reg_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \I_mon_reg_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \I_mon_reg_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \I_mon_reg_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \I_mon_reg_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \I_mon_reg_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \^i_term_mon\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal I_term_q : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \I_term_q[0]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[10]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[11]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[12]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[13]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[14]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[15]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[16]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[17]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[1]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[2]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[3]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[4]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[5]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[6]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[7]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[8]_i_1_n_0\ : STD_LOGIC;
  signal \I_term_q[9]_i_1_n_0\ : STD_LOGIC;
  signal \^i_term_q_reg[30]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal P_error_unshifted_reg0_n_100 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_101 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_102 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_103 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_104 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_105 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_106 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_107 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_108 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_109 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_110 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_111 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_112 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_113 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_114 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_115 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_116 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_117 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_118 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_119 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_120 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_121 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_122 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_123 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_124 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_125 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_126 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_127 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_128 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_129 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_130 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_131 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_132 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_133 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_134 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_135 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_136 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_137 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_138 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_139 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_140 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_141 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_142 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_143 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_144 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_145 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_146 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_147 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_148 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_149 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_150 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_151 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_152 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_153 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_58 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_59 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_60 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_61 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_62 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_63 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_64 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_65 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_66 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_67 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_68 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_69 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_70 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_71 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_72 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_73 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_74 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_75 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_76 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_77 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_78 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_79 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_80 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_81 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_82 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_83 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_84 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_85 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_86 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_87 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_88 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_89 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_90 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_91 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_92 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_93 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_94 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_95 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_96 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_97 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_98 : STD_LOGIC;
  signal P_error_unshifted_reg0_n_99 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_58 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_59 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_60 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_61 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_62 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_63 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_64 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_65 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_66 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_67 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_68 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_69 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_70 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_71 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_72 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_73 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_74 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_75 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_76 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_77 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_78 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_79 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_80 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_81 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_82 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_83 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_84 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_85 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_86 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_87 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_88 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_89 : STD_LOGIC;
  signal P_error_unshifted_reg_reg_n_90 : STD_LOGIC;
  signal \^autolock_max_reg_reg[30]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \axi_pi_output_reg[13]_inv_i_10_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_11_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_12_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_13_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_15_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_16_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_17_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_18_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_19_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_20_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_21_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_22_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_24_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_25_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_26_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_27_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_28_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_29_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_30_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_31_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_33_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_34_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_35_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_36_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_37_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_38_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_39_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_40_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_42_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_43_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_44_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_45_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_46_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_47_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_48_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_49_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_51_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_52_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_53_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_54_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_55_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_56_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_57_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_58_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_59_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_60_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_61_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_62_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_63_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_64_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_65_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_66_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_67_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_68_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_69_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_6_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_70_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_71_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_72_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_73_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_74_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_7_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_8_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_9_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_14_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_14_n_1\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_14_n_2\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_14_n_3\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_23_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_23_n_1\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_23_n_2\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_23_n_3\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_32_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_32_n_1\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_32_n_2\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_32_n_3\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_3_n_1\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_3_n_2\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_3_n_3\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_41_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_41_n_1\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_41_n_2\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_41_n_3\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_4_n_1\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_4_n_2\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_4_n_3\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_50_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_50_n_1\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_50_n_2\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_50_n_3\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_5_n_0\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_5_n_1\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_5_n_2\ : STD_LOGIC;
  signal \axi_pi_output_reg_reg[13]_inv_i_5_n_3\ : STD_LOGIC;
  signal \^control_reg_reg[1]\ : STD_LOGIC;
  signal error_reg0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \error_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \error_reg[11]_i_3_n_0\ : STD_LOGIC;
  signal \error_reg[11]_i_4_n_0\ : STD_LOGIC;
  signal \error_reg[11]_i_5_n_0\ : STD_LOGIC;
  signal \error_reg[13]_i_3_n_0\ : STD_LOGIC;
  signal \error_reg[13]_i_4_n_0\ : STD_LOGIC;
  signal \error_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \error_reg[3]_i_3_n_0\ : STD_LOGIC;
  signal \error_reg[3]_i_4_n_0\ : STD_LOGIC;
  signal \error_reg[3]_i_5_n_0\ : STD_LOGIC;
  signal \error_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \error_reg[7]_i_3_n_0\ : STD_LOGIC;
  signal \error_reg[7]_i_4_n_0\ : STD_LOGIC;
  signal \error_reg[7]_i_5_n_0\ : STD_LOGIC;
  signal \error_reg_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \error_reg_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \error_reg_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \error_reg_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \error_reg_reg[13]_i_2_n_3\ : STD_LOGIC;
  signal \error_reg_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \error_reg_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \error_reg_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \error_reg_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \error_reg_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \error_reg_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \error_reg_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \error_reg_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_10_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_11_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_13_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_14_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_15_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_16_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_18_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_19_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_20_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_21_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_22_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_23_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_24_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_25_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_3_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_4_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_5_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_6_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_8_n_0\ : STD_LOGIC;
  signal \loop_output_reg[19]_i_9_n_0\ : STD_LOGIC;
  signal \loop_output_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \loop_output_reg[23]_i_3_n_0\ : STD_LOGIC;
  signal \loop_output_reg[23]_i_4_n_0\ : STD_LOGIC;
  signal \loop_output_reg[23]_i_5_n_0\ : STD_LOGIC;
  signal \loop_output_reg[27]_i_2_n_0\ : STD_LOGIC;
  signal \loop_output_reg[27]_i_3_n_0\ : STD_LOGIC;
  signal \loop_output_reg[27]_i_4_n_0\ : STD_LOGIC;
  signal \loop_output_reg[27]_i_5_n_0\ : STD_LOGIC;
  signal \loop_output_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \loop_output_reg[31]_i_3_n_0\ : STD_LOGIC;
  signal \loop_output_reg[31]_i_4_n_0\ : STD_LOGIC;
  signal \loop_output_reg[31]_i_5_n_0\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_12_n_0\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_12_n_1\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_12_n_2\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_12_n_3\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_17_n_0\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_17_n_1\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_17_n_2\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_17_n_3\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_2_n_1\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_2_n_2\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_2_n_3\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_7_n_0\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_7_n_1\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_7_n_2\ : STD_LOGIC;
  signal \loop_output_reg_reg[19]_i_7_n_3\ : STD_LOGIC;
  signal \loop_output_reg_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \loop_output_reg_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \loop_output_reg_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \loop_output_reg_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \loop_output_reg_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \loop_output_reg_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \loop_output_reg_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \loop_output_reg_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \loop_output_reg_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \loop_output_reg_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \loop_output_reg_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \loop_output_reg_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \loop_output_reg_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \loop_output_reg_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \loop_output_reg_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \loop_output_reg_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \^loop_output_reg_reg[31]_0\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \loop_output_reg_reg[31]_i_1_n_1\ : STD_LOGIC;
  signal \loop_output_reg_reg[31]_i_1_n_2\ : STD_LOGIC;
  signal \loop_output_reg_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \loop_output_reg_reg[31]_i_1_n_4\ : STD_LOGIC;
  signal \loop_output_reg_reg[31]_i_1_n_5\ : STD_LOGIC;
  signal \loop_output_reg_reg[31]_i_1_n_6\ : STD_LOGIC;
  signal \loop_output_reg_reg[31]_i_1_n_7\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 30 downto 18 );
  signal \ramplitude_min_reg[21]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[21]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[21]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[25]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[25]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[25]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[25]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[29]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[29]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[29]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[29]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_31_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_32_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[21]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[21]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[21]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[21]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[25]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[25]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[25]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[25]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[29]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[29]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[29]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[29]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_6_n_3\ : STD_LOGIC;
  signal setpoint_reg : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal NLW_I_error_unshifted_reg0_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg0_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg0_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg0_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg0_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg0_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg0_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_I_error_unshifted_reg0_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_I_error_unshifted_reg0_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_I_error_unshifted_reg_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_I_error_unshifted_reg_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_I_error_unshifted_reg_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_I_error_unshifted_reg_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_I_error_unshifted_reg_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal \NLW_I_term_q_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_I_term_q_reg[31]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_P_error_unshifted_reg0_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg0_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg0_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg0_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg0_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg0_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg0_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_P_error_unshifted_reg0_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_P_error_unshifted_reg0_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_P_error_unshifted_reg_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_P_error_unshifted_reg_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_P_error_unshifted_reg_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_P_error_unshifted_reg_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_P_error_unshifted_reg_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal \NLW_axi_pi_output_reg_reg[13]_inv_i_14_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_axi_pi_output_reg_reg[13]_inv_i_23_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_axi_pi_output_reg_reg[13]_inv_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_axi_pi_output_reg_reg[13]_inv_i_32_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_axi_pi_output_reg_reg[13]_inv_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_axi_pi_output_reg_reg[13]_inv_i_41_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_axi_pi_output_reg_reg[13]_inv_i_5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_axi_pi_output_reg_reg[13]_inv_i_50_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_error_reg_reg[13]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_error_reg_reg[13]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_loop_output_reg_reg[19]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_loop_output_reg_reg[19]_i_12_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_output_reg_reg[19]_i_17_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_output_reg_reg[19]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_output_reg_reg[19]_i_7_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_output_reg_reg[31]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of I_error_unshifted_reg0 : label is "{SYNTH-11 {cell *THIS*}}";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \I_term_q[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \I_term_q[10]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \I_term_q[11]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \I_term_q[12]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \I_term_q[13]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \I_term_q[14]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \I_term_q[15]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \I_term_q[16]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \I_term_q[17]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \I_term_q[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \I_term_q[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \I_term_q[3]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \I_term_q[4]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \I_term_q[5]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \I_term_q[6]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \I_term_q[7]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \I_term_q[8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \I_term_q[9]_i_1\ : label is "soft_lutpair8";
  attribute METHODOLOGY_DRC_VIOS of P_error_unshifted_reg0 : label is "{SYNTH-11 {cell *THIS*}}";
  attribute SOFT_HLUTNM of \autolock_input_mindex[0]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \autolock_input_mindex[10]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \autolock_input_mindex[11]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \autolock_input_mindex[12]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \autolock_input_mindex[13]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \autolock_input_mindex[1]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \autolock_input_mindex[2]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \autolock_input_mindex[3]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \autolock_input_mindex[4]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \autolock_input_mindex[5]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \autolock_input_mindex[6]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \autolock_input_mindex[7]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \autolock_input_mindex[8]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \autolock_input_mindex[9]_i_1\ : label is "soft_lutpair13";
begin
  CO(0) <= \^co\(0);
  D(31 downto 0) <= \^d\(31 downto 0);
  I_term_mon(31 downto 0) <= \^i_term_mon\(31 downto 0);
  \I_term_q_reg[30]_0\(0) <= \^i_term_q_reg[30]_0\(0);
  \autolock_max_reg_reg[30]\(0) <= \^autolock_max_reg_reg[30]\(0);
  \control_reg_reg[1]\ <= \^control_reg_reg[1]\;
  \loop_output_reg_reg[31]_0\(13 downto 0) <= \^loop_output_reg_reg[31]_0\(13 downto 0);
I_error_unshifted_reg0: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 1,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 1,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 17) => B"0000000000000",
      A(16 downto 0) => Q(16 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_I_error_unshifted_reg0_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => error_reg0(13),
      B(16) => error_reg0(13),
      B(15) => error_reg0(13),
      B(14) => error_reg0(13),
      B(13 downto 0) => error_reg0(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_I_error_unshifted_reg0_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_I_error_unshifted_reg0_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_I_error_unshifted_reg0_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '1',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '1',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => s00_axi_aclk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_I_error_unshifted_reg0_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_I_error_unshifted_reg0_OVERFLOW_UNCONNECTED,
      P(47) => I_error_unshifted_reg0_n_58,
      P(46) => I_error_unshifted_reg0_n_59,
      P(45) => I_error_unshifted_reg0_n_60,
      P(44) => I_error_unshifted_reg0_n_61,
      P(43) => I_error_unshifted_reg0_n_62,
      P(42) => I_error_unshifted_reg0_n_63,
      P(41) => I_error_unshifted_reg0_n_64,
      P(40) => I_error_unshifted_reg0_n_65,
      P(39) => I_error_unshifted_reg0_n_66,
      P(38) => I_error_unshifted_reg0_n_67,
      P(37) => I_error_unshifted_reg0_n_68,
      P(36) => I_error_unshifted_reg0_n_69,
      P(35) => I_error_unshifted_reg0_n_70,
      P(34) => I_error_unshifted_reg0_n_71,
      P(33) => I_error_unshifted_reg0_n_72,
      P(32) => I_error_unshifted_reg0_n_73,
      P(31) => I_error_unshifted_reg0_n_74,
      P(30) => I_error_unshifted_reg0_n_75,
      P(29) => I_error_unshifted_reg0_n_76,
      P(28) => I_error_unshifted_reg0_n_77,
      P(27) => I_error_unshifted_reg0_n_78,
      P(26) => I_error_unshifted_reg0_n_79,
      P(25) => I_error_unshifted_reg0_n_80,
      P(24) => I_error_unshifted_reg0_n_81,
      P(23) => I_error_unshifted_reg0_n_82,
      P(22) => I_error_unshifted_reg0_n_83,
      P(21) => I_error_unshifted_reg0_n_84,
      P(20) => I_error_unshifted_reg0_n_85,
      P(19) => I_error_unshifted_reg0_n_86,
      P(18) => I_error_unshifted_reg0_n_87,
      P(17) => I_error_unshifted_reg0_n_88,
      P(16) => I_error_unshifted_reg0_n_89,
      P(15) => I_error_unshifted_reg0_n_90,
      P(14) => I_error_unshifted_reg0_n_91,
      P(13) => I_error_unshifted_reg0_n_92,
      P(12) => I_error_unshifted_reg0_n_93,
      P(11) => I_error_unshifted_reg0_n_94,
      P(10) => I_error_unshifted_reg0_n_95,
      P(9) => I_error_unshifted_reg0_n_96,
      P(8) => I_error_unshifted_reg0_n_97,
      P(7) => I_error_unshifted_reg0_n_98,
      P(6) => I_error_unshifted_reg0_n_99,
      P(5) => I_error_unshifted_reg0_n_100,
      P(4) => I_error_unshifted_reg0_n_101,
      P(3) => I_error_unshifted_reg0_n_102,
      P(2) => I_error_unshifted_reg0_n_103,
      P(1) => I_error_unshifted_reg0_n_104,
      P(0) => I_error_unshifted_reg0_n_105,
      PATTERNBDETECT => NLW_I_error_unshifted_reg0_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_I_error_unshifted_reg0_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47) => I_error_unshifted_reg0_n_106,
      PCOUT(46) => I_error_unshifted_reg0_n_107,
      PCOUT(45) => I_error_unshifted_reg0_n_108,
      PCOUT(44) => I_error_unshifted_reg0_n_109,
      PCOUT(43) => I_error_unshifted_reg0_n_110,
      PCOUT(42) => I_error_unshifted_reg0_n_111,
      PCOUT(41) => I_error_unshifted_reg0_n_112,
      PCOUT(40) => I_error_unshifted_reg0_n_113,
      PCOUT(39) => I_error_unshifted_reg0_n_114,
      PCOUT(38) => I_error_unshifted_reg0_n_115,
      PCOUT(37) => I_error_unshifted_reg0_n_116,
      PCOUT(36) => I_error_unshifted_reg0_n_117,
      PCOUT(35) => I_error_unshifted_reg0_n_118,
      PCOUT(34) => I_error_unshifted_reg0_n_119,
      PCOUT(33) => I_error_unshifted_reg0_n_120,
      PCOUT(32) => I_error_unshifted_reg0_n_121,
      PCOUT(31) => I_error_unshifted_reg0_n_122,
      PCOUT(30) => I_error_unshifted_reg0_n_123,
      PCOUT(29) => I_error_unshifted_reg0_n_124,
      PCOUT(28) => I_error_unshifted_reg0_n_125,
      PCOUT(27) => I_error_unshifted_reg0_n_126,
      PCOUT(26) => I_error_unshifted_reg0_n_127,
      PCOUT(25) => I_error_unshifted_reg0_n_128,
      PCOUT(24) => I_error_unshifted_reg0_n_129,
      PCOUT(23) => I_error_unshifted_reg0_n_130,
      PCOUT(22) => I_error_unshifted_reg0_n_131,
      PCOUT(21) => I_error_unshifted_reg0_n_132,
      PCOUT(20) => I_error_unshifted_reg0_n_133,
      PCOUT(19) => I_error_unshifted_reg0_n_134,
      PCOUT(18) => I_error_unshifted_reg0_n_135,
      PCOUT(17) => I_error_unshifted_reg0_n_136,
      PCOUT(16) => I_error_unshifted_reg0_n_137,
      PCOUT(15) => I_error_unshifted_reg0_n_138,
      PCOUT(14) => I_error_unshifted_reg0_n_139,
      PCOUT(13) => I_error_unshifted_reg0_n_140,
      PCOUT(12) => I_error_unshifted_reg0_n_141,
      PCOUT(11) => I_error_unshifted_reg0_n_142,
      PCOUT(10) => I_error_unshifted_reg0_n_143,
      PCOUT(9) => I_error_unshifted_reg0_n_144,
      PCOUT(8) => I_error_unshifted_reg0_n_145,
      PCOUT(7) => I_error_unshifted_reg0_n_146,
      PCOUT(6) => I_error_unshifted_reg0_n_147,
      PCOUT(5) => I_error_unshifted_reg0_n_148,
      PCOUT(4) => I_error_unshifted_reg0_n_149,
      PCOUT(3) => I_error_unshifted_reg0_n_150,
      PCOUT(2) => I_error_unshifted_reg0_n_151,
      PCOUT(1) => I_error_unshifted_reg0_n_152,
      PCOUT(0) => I_error_unshifted_reg0_n_153,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => \^control_reg_reg[1]\,
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_I_error_unshifted_reg0_UNDERFLOW_UNCONNECTED
    );
I_error_unshifted_reg_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 1,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 1,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => Q(31),
      A(28) => Q(31),
      A(27) => Q(31),
      A(26) => Q(31),
      A(25) => Q(31),
      A(24) => Q(31),
      A(23) => Q(31),
      A(22) => Q(31),
      A(21) => Q(31),
      A(20) => Q(31),
      A(19) => Q(31),
      A(18) => Q(31),
      A(17) => Q(31),
      A(16) => Q(31),
      A(15) => Q(31),
      A(14 downto 0) => Q(31 downto 17),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_I_error_unshifted_reg_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => error_reg0(13),
      B(16) => error_reg0(13),
      B(15) => error_reg0(13),
      B(14) => error_reg0(13),
      B(13 downto 0) => error_reg0(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_I_error_unshifted_reg_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_I_error_unshifted_reg_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_I_error_unshifted_reg_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '1',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '1',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '1',
      CLK => s00_axi_aclk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_I_error_unshifted_reg_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"1010101",
      OVERFLOW => NLW_I_error_unshifted_reg_reg_OVERFLOW_UNCONNECTED,
      P(47) => I_error_unshifted_reg_reg_n_58,
      P(46) => I_error_unshifted_reg_reg_n_59,
      P(45) => I_error_unshifted_reg_reg_n_60,
      P(44) => I_error_unshifted_reg_reg_n_61,
      P(43) => I_error_unshifted_reg_reg_n_62,
      P(42) => I_error_unshifted_reg_reg_n_63,
      P(41) => I_error_unshifted_reg_reg_n_64,
      P(40) => I_error_unshifted_reg_reg_n_65,
      P(39) => I_error_unshifted_reg_reg_n_66,
      P(38) => I_error_unshifted_reg_reg_n_67,
      P(37) => I_error_unshifted_reg_reg_n_68,
      P(36) => I_error_unshifted_reg_reg_n_69,
      P(35) => I_error_unshifted_reg_reg_n_70,
      P(34) => I_error_unshifted_reg_reg_n_71,
      P(33) => I_error_unshifted_reg_reg_n_72,
      P(32) => I_error_unshifted_reg_reg_n_73,
      P(31) => I_error_unshifted_reg_reg_n_74,
      P(30) => I_error_unshifted_reg_reg_n_75,
      P(29) => I_error_unshifted_reg_reg_n_76,
      P(28 downto 0) => I_error(31 downto 3),
      PATTERNBDETECT => NLW_I_error_unshifted_reg_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_I_error_unshifted_reg_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47) => I_error_unshifted_reg0_n_106,
      PCIN(46) => I_error_unshifted_reg0_n_107,
      PCIN(45) => I_error_unshifted_reg0_n_108,
      PCIN(44) => I_error_unshifted_reg0_n_109,
      PCIN(43) => I_error_unshifted_reg0_n_110,
      PCIN(42) => I_error_unshifted_reg0_n_111,
      PCIN(41) => I_error_unshifted_reg0_n_112,
      PCIN(40) => I_error_unshifted_reg0_n_113,
      PCIN(39) => I_error_unshifted_reg0_n_114,
      PCIN(38) => I_error_unshifted_reg0_n_115,
      PCIN(37) => I_error_unshifted_reg0_n_116,
      PCIN(36) => I_error_unshifted_reg0_n_117,
      PCIN(35) => I_error_unshifted_reg0_n_118,
      PCIN(34) => I_error_unshifted_reg0_n_119,
      PCIN(33) => I_error_unshifted_reg0_n_120,
      PCIN(32) => I_error_unshifted_reg0_n_121,
      PCIN(31) => I_error_unshifted_reg0_n_122,
      PCIN(30) => I_error_unshifted_reg0_n_123,
      PCIN(29) => I_error_unshifted_reg0_n_124,
      PCIN(28) => I_error_unshifted_reg0_n_125,
      PCIN(27) => I_error_unshifted_reg0_n_126,
      PCIN(26) => I_error_unshifted_reg0_n_127,
      PCIN(25) => I_error_unshifted_reg0_n_128,
      PCIN(24) => I_error_unshifted_reg0_n_129,
      PCIN(23) => I_error_unshifted_reg0_n_130,
      PCIN(22) => I_error_unshifted_reg0_n_131,
      PCIN(21) => I_error_unshifted_reg0_n_132,
      PCIN(20) => I_error_unshifted_reg0_n_133,
      PCIN(19) => I_error_unshifted_reg0_n_134,
      PCIN(18) => I_error_unshifted_reg0_n_135,
      PCIN(17) => I_error_unshifted_reg0_n_136,
      PCIN(16) => I_error_unshifted_reg0_n_137,
      PCIN(15) => I_error_unshifted_reg0_n_138,
      PCIN(14) => I_error_unshifted_reg0_n_139,
      PCIN(13) => I_error_unshifted_reg0_n_140,
      PCIN(12) => I_error_unshifted_reg0_n_141,
      PCIN(11) => I_error_unshifted_reg0_n_142,
      PCIN(10) => I_error_unshifted_reg0_n_143,
      PCIN(9) => I_error_unshifted_reg0_n_144,
      PCIN(8) => I_error_unshifted_reg0_n_145,
      PCIN(7) => I_error_unshifted_reg0_n_146,
      PCIN(6) => I_error_unshifted_reg0_n_147,
      PCIN(5) => I_error_unshifted_reg0_n_148,
      PCIN(4) => I_error_unshifted_reg0_n_149,
      PCIN(3) => I_error_unshifted_reg0_n_150,
      PCIN(2) => I_error_unshifted_reg0_n_151,
      PCIN(1) => I_error_unshifted_reg0_n_152,
      PCIN(0) => I_error_unshifted_reg0_n_153,
      PCOUT(47 downto 0) => NLW_I_error_unshifted_reg_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => \^control_reg_reg[1]\,
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => \^control_reg_reg[1]\,
      UNDERFLOW => NLW_I_error_unshifted_reg_reg_UNDERFLOW_UNCONNECTED
    );
\I_error_unshifted_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_error_unshifted_reg0_n_91,
      Q => I_error(0),
      R => \^control_reg_reg[1]\
    );
\I_error_unshifted_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_error_unshifted_reg0_n_90,
      Q => I_error(1),
      R => \^control_reg_reg[1]\
    );
\I_error_unshifted_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_error_unshifted_reg0_n_89,
      Q => I_error(2),
      R => \^control_reg_reg[1]\
    );
\I_mon_reg[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(11),
      I1 => I_error(11),
      O => \I_mon_reg[11]_i_2_n_0\
    );
\I_mon_reg[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(10),
      I1 => I_error(10),
      O => \I_mon_reg[11]_i_3_n_0\
    );
\I_mon_reg[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(9),
      I1 => I_error(9),
      O => \I_mon_reg[11]_i_4_n_0\
    );
\I_mon_reg[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(8),
      I1 => I_error(8),
      O => \I_mon_reg[11]_i_5_n_0\
    );
\I_mon_reg[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(15),
      I1 => I_error(15),
      O => \I_mon_reg[15]_i_2_n_0\
    );
\I_mon_reg[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(14),
      I1 => I_error(14),
      O => \I_mon_reg[15]_i_3_n_0\
    );
\I_mon_reg[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(13),
      I1 => I_error(13),
      O => \I_mon_reg[15]_i_4_n_0\
    );
\I_mon_reg[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(12),
      I1 => I_error(12),
      O => \I_mon_reg[15]_i_5_n_0\
    );
\I_mon_reg[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(19),
      I1 => I_error(19),
      O => \I_mon_reg[19]_i_2_n_0\
    );
\I_mon_reg[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(18),
      I1 => I_error(18),
      O => \I_mon_reg[19]_i_3_n_0\
    );
\I_mon_reg[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(17),
      I1 => I_error(17),
      O => \I_mon_reg[19]_i_4_n_0\
    );
\I_mon_reg[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(16),
      I1 => I_error(16),
      O => \I_mon_reg[19]_i_5_n_0\
    );
\I_mon_reg[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(23),
      I1 => I_error(23),
      O => \I_mon_reg[23]_i_2_n_0\
    );
\I_mon_reg[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(22),
      I1 => I_error(22),
      O => \I_mon_reg[23]_i_3_n_0\
    );
\I_mon_reg[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(21),
      I1 => I_error(21),
      O => \I_mon_reg[23]_i_4_n_0\
    );
\I_mon_reg[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(20),
      I1 => I_error(20),
      O => \I_mon_reg[23]_i_5_n_0\
    );
\I_mon_reg[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(27),
      I1 => I_error(27),
      O => \I_mon_reg[27]_i_2_n_0\
    );
\I_mon_reg[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(26),
      I1 => I_error(26),
      O => \I_mon_reg[27]_i_3_n_0\
    );
\I_mon_reg[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(25),
      I1 => I_error(25),
      O => \I_mon_reg[27]_i_4_n_0\
    );
\I_mon_reg[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(24),
      I1 => I_error(24),
      O => \I_mon_reg[27]_i_5_n_0\
    );
\I_mon_reg[31]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => I_term_q(31),
      O => \I_mon_reg[31]_i_2_n_0\
    );
\I_mon_reg[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(31),
      I1 => I_error(31),
      O => \I_mon_reg[31]_i_3_n_0\
    );
\I_mon_reg[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(30),
      I1 => I_error(30),
      O => \I_mon_reg[31]_i_4_n_0\
    );
\I_mon_reg[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(29),
      I1 => I_error(29),
      O => \I_mon_reg[31]_i_5_n_0\
    );
\I_mon_reg[31]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(28),
      I1 => I_error(28),
      O => \I_mon_reg[31]_i_6_n_0\
    );
\I_mon_reg[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(3),
      I1 => I_error(3),
      O => \I_mon_reg[3]_i_2_n_0\
    );
\I_mon_reg[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(2),
      I1 => I_error(2),
      O => \I_mon_reg[3]_i_3_n_0\
    );
\I_mon_reg[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(1),
      I1 => I_error(1),
      O => \I_mon_reg[3]_i_4_n_0\
    );
\I_mon_reg[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(0),
      I1 => I_error(0),
      O => \I_mon_reg[3]_i_5_n_0\
    );
\I_mon_reg[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(7),
      I1 => I_error(7),
      O => \I_mon_reg[7]_i_2_n_0\
    );
\I_mon_reg[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(6),
      I1 => I_error(6),
      O => \I_mon_reg[7]_i_3_n_0\
    );
\I_mon_reg[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(5),
      I1 => I_error(5),
      O => \I_mon_reg[7]_i_4_n_0\
    );
\I_mon_reg[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => I_term_q(4),
      I1 => I_error(4),
      O => \I_mon_reg[7]_i_5_n_0\
    );
\I_mon_reg_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \I_mon_reg_reg[7]_i_1_n_0\,
      CO(3) => \I_mon_reg_reg[11]_i_1_n_0\,
      CO(2) => \I_mon_reg_reg[11]_i_1_n_1\,
      CO(1) => \I_mon_reg_reg[11]_i_1_n_2\,
      CO(0) => \I_mon_reg_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => I_term_q(11 downto 8),
      O(3 downto 0) => \^i_term_mon\(11 downto 8),
      S(3) => \I_mon_reg[11]_i_2_n_0\,
      S(2) => \I_mon_reg[11]_i_3_n_0\,
      S(1) => \I_mon_reg[11]_i_4_n_0\,
      S(0) => \I_mon_reg[11]_i_5_n_0\
    );
\I_mon_reg_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \I_mon_reg_reg[11]_i_1_n_0\,
      CO(3) => \I_mon_reg_reg[15]_i_1_n_0\,
      CO(2) => \I_mon_reg_reg[15]_i_1_n_1\,
      CO(1) => \I_mon_reg_reg[15]_i_1_n_2\,
      CO(0) => \I_mon_reg_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => I_term_q(15 downto 12),
      O(3 downto 0) => \^i_term_mon\(15 downto 12),
      S(3) => \I_mon_reg[15]_i_2_n_0\,
      S(2) => \I_mon_reg[15]_i_3_n_0\,
      S(1) => \I_mon_reg[15]_i_4_n_0\,
      S(0) => \I_mon_reg[15]_i_5_n_0\
    );
\I_mon_reg_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \I_mon_reg_reg[15]_i_1_n_0\,
      CO(3) => \I_mon_reg_reg[19]_i_1_n_0\,
      CO(2) => \I_mon_reg_reg[19]_i_1_n_1\,
      CO(1) => \I_mon_reg_reg[19]_i_1_n_2\,
      CO(0) => \I_mon_reg_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => I_term_q(19 downto 16),
      O(3 downto 0) => \^i_term_mon\(19 downto 16),
      S(3) => \I_mon_reg[19]_i_2_n_0\,
      S(2) => \I_mon_reg[19]_i_3_n_0\,
      S(1) => \I_mon_reg[19]_i_4_n_0\,
      S(0) => \I_mon_reg[19]_i_5_n_0\
    );
\I_mon_reg_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \I_mon_reg_reg[19]_i_1_n_0\,
      CO(3) => \I_mon_reg_reg[23]_i_1_n_0\,
      CO(2) => \I_mon_reg_reg[23]_i_1_n_1\,
      CO(1) => \I_mon_reg_reg[23]_i_1_n_2\,
      CO(0) => \I_mon_reg_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => I_term_q(23 downto 20),
      O(3 downto 0) => \^i_term_mon\(23 downto 20),
      S(3) => \I_mon_reg[23]_i_2_n_0\,
      S(2) => \I_mon_reg[23]_i_3_n_0\,
      S(1) => \I_mon_reg[23]_i_4_n_0\,
      S(0) => \I_mon_reg[23]_i_5_n_0\
    );
\I_mon_reg_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \I_mon_reg_reg[23]_i_1_n_0\,
      CO(3) => \I_mon_reg_reg[27]_i_1_n_0\,
      CO(2) => \I_mon_reg_reg[27]_i_1_n_1\,
      CO(1) => \I_mon_reg_reg[27]_i_1_n_2\,
      CO(0) => \I_mon_reg_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => I_term_q(27 downto 24),
      O(3 downto 0) => \^i_term_mon\(27 downto 24),
      S(3) => \I_mon_reg[27]_i_2_n_0\,
      S(2) => \I_mon_reg[27]_i_3_n_0\,
      S(1) => \I_mon_reg[27]_i_4_n_0\,
      S(0) => \I_mon_reg[27]_i_5_n_0\
    );
\I_mon_reg_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \I_mon_reg_reg[27]_i_1_n_0\,
      CO(3) => \I_mon_reg_reg[31]_i_1_n_0\,
      CO(2) => \I_mon_reg_reg[31]_i_1_n_1\,
      CO(1) => \I_mon_reg_reg[31]_i_1_n_2\,
      CO(0) => \I_mon_reg_reg[31]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \I_mon_reg[31]_i_2_n_0\,
      DI(2 downto 0) => I_term_q(30 downto 28),
      O(3 downto 0) => \^i_term_mon\(31 downto 28),
      S(3) => \I_mon_reg[31]_i_3_n_0\,
      S(2) => \I_mon_reg[31]_i_4_n_0\,
      S(1) => \I_mon_reg[31]_i_5_n_0\,
      S(0) => \I_mon_reg[31]_i_6_n_0\
    );
\I_mon_reg_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \I_mon_reg_reg[3]_i_1_n_0\,
      CO(2) => \I_mon_reg_reg[3]_i_1_n_1\,
      CO(1) => \I_mon_reg_reg[3]_i_1_n_2\,
      CO(0) => \I_mon_reg_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => I_term_q(3 downto 0),
      O(3 downto 0) => \^i_term_mon\(3 downto 0),
      S(3) => \I_mon_reg[3]_i_2_n_0\,
      S(2) => \I_mon_reg[3]_i_3_n_0\,
      S(1) => \I_mon_reg[3]_i_4_n_0\,
      S(0) => \I_mon_reg[3]_i_5_n_0\
    );
\I_mon_reg_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \I_mon_reg_reg[3]_i_1_n_0\,
      CO(3) => \I_mon_reg_reg[7]_i_1_n_0\,
      CO(2) => \I_mon_reg_reg[7]_i_1_n_1\,
      CO(1) => \I_mon_reg_reg[7]_i_1_n_2\,
      CO(0) => \I_mon_reg_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => I_term_q(7 downto 4),
      O(3 downto 0) => \^i_term_mon\(7 downto 4),
      S(3) => \I_mon_reg[7]_i_2_n_0\,
      S(2) => \I_mon_reg[7]_i_3_n_0\,
      S(1) => \I_mon_reg[7]_i_4_n_0\,
      S(0) => \I_mon_reg[7]_i_5_n_0\
    );
\I_term_q[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(0),
      O => \I_term_q[0]_i_1_n_0\
    );
\I_term_q[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(10),
      O => \I_term_q[10]_i_1_n_0\
    );
\I_term_q[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(11),
      O => \I_term_q[11]_i_1_n_0\
    );
\I_term_q[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(12),
      O => \I_term_q[12]_i_1_n_0\
    );
\I_term_q[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(13),
      O => \I_term_q[13]_i_1_n_0\
    );
\I_term_q[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(14),
      O => \I_term_q[14]_i_1_n_0\
    );
\I_term_q[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(15),
      O => \I_term_q[15]_i_1_n_0\
    );
\I_term_q[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(16),
      O => \I_term_q[16]_i_1_n_0\
    );
\I_term_q[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(17),
      O => \I_term_q[17]_i_1_n_0\
    );
\I_term_q[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(0),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(18),
      O => p_0_in(18)
    );
\I_term_q[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(1),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(19),
      O => p_0_in(19)
    );
\I_term_q[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(1),
      O => \I_term_q[1]_i_1_n_0\
    );
\I_term_q[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(2),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(20),
      O => p_0_in(20)
    );
\I_term_q[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(3),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(21),
      O => p_0_in(21)
    );
\I_term_q[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(4),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(22),
      O => p_0_in(22)
    );
\I_term_q[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(5),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(23),
      O => p_0_in(23)
    );
\I_term_q[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(6),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(24),
      O => p_0_in(24)
    );
\I_term_q[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(7),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(25),
      O => p_0_in(25)
    );
\I_term_q[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(8),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(26),
      O => p_0_in(26)
    );
\I_term_q[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(9),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(27),
      O => p_0_in(27)
    );
\I_term_q[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(10),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(28),
      O => p_0_in(28)
    );
\I_term_q[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(11),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(29),
      O => p_0_in(29)
    );
\I_term_q[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(2),
      O => \I_term_q[2]_i_1_n_0\
    );
\I_term_q[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB8B888"
    )
        port map (
      I0 => \autolock_input_maxdex_reg[13]\(12),
      I1 => \^control_reg_reg[1]\,
      I2 => \^i_term_q_reg[30]_0\(0),
      I3 => \^i_term_mon\(31),
      I4 => \^i_term_mon\(30),
      O => p_0_in(30)
    );
\I_term_q[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(3),
      O => \I_term_q[3]_i_1_n_0\
    );
\I_term_q[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(4),
      O => \I_term_q[4]_i_1_n_0\
    );
\I_term_q[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(5),
      O => \I_term_q[5]_i_1_n_0\
    );
\I_term_q[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(6),
      O => \I_term_q[6]_i_1_n_0\
    );
\I_term_q[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(7),
      O => \I_term_q[7]_i_1_n_0\
    );
\I_term_q[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(8),
      O => \I_term_q[8]_i_1_n_0\
    );
\I_term_q[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^i_term_q_reg[30]_0\(0),
      I1 => \^i_term_mon\(31),
      I2 => \^i_term_mon\(9),
      O => \I_term_q[9]_i_1_n_0\
    );
\I_term_q_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[0]_i_1_n_0\,
      Q => I_term_q(0),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[10]_i_1_n_0\,
      Q => I_term_q(10),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[11]_i_1_n_0\,
      Q => I_term_q(11),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[12]_i_1_n_0\,
      Q => I_term_q(12),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[13]_i_1_n_0\,
      Q => I_term_q(13),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[14]_i_1_n_0\,
      Q => I_term_q(14),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[15]_i_1_n_0\,
      Q => I_term_q(15),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[16]_i_1_n_0\,
      Q => I_term_q(16),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[17]_i_1_n_0\,
      Q => I_term_q(17),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(18),
      Q => I_term_q(18),
      R => '0'
    );
\I_term_q_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(19),
      Q => I_term_q(19),
      R => '0'
    );
\I_term_q_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[1]_i_1_n_0\,
      Q => I_term_q(1),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(20),
      Q => I_term_q(20),
      R => '0'
    );
\I_term_q_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(21),
      Q => I_term_q(21),
      R => '0'
    );
\I_term_q_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(22),
      Q => I_term_q(22),
      R => '0'
    );
\I_term_q_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(23),
      Q => I_term_q(23),
      R => '0'
    );
\I_term_q_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(24),
      Q => I_term_q(24),
      R => '0'
    );
\I_term_q_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(25),
      Q => I_term_q(25),
      R => '0'
    );
\I_term_q_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(26),
      Q => I_term_q(26),
      R => '0'
    );
\I_term_q_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(27),
      Q => I_term_q(27),
      R => '0'
    );
\I_term_q_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(28),
      Q => I_term_q(28),
      R => '0'
    );
\I_term_q_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(29),
      Q => I_term_q(29),
      R => '0'
    );
\I_term_q_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[2]_i_1_n_0\,
      Q => I_term_q(2),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_0_in(30),
      Q => I_term_q(30),
      R => '0'
    );
\I_term_q_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q_reg[31]_0\(0),
      Q => I_term_q(31),
      R => '0'
    );
\I_term_q_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \I_mon_reg_reg[31]_i_1_n_0\,
      CO(3 downto 1) => \NLW_I_term_q_reg[31]_i_2_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \^i_term_q_reg[30]_0\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_I_term_q_reg[31]_i_2_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\I_term_q_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[3]_i_1_n_0\,
      Q => I_term_q(3),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[4]_i_1_n_0\,
      Q => I_term_q(4),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[5]_i_1_n_0\,
      Q => I_term_q(5),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[6]_i_1_n_0\,
      Q => I_term_q(6),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[7]_i_1_n_0\,
      Q => I_term_q(7),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[8]_i_1_n_0\,
      Q => I_term_q(8),
      R => \^control_reg_reg[1]\
    );
\I_term_q_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \I_term_q[9]_i_1_n_0\,
      Q => I_term_q(9),
      R => \^control_reg_reg[1]\
    );
P_error_unshifted_reg0: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 1,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 1,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 17) => B"0000000000000",
      A(16 downto 0) => P_error_unshifted_reg_reg_0(16 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_P_error_unshifted_reg0_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => error_reg0(13),
      B(16) => error_reg0(13),
      B(15) => error_reg0(13),
      B(14) => error_reg0(13),
      B(13 downto 0) => error_reg0(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_P_error_unshifted_reg0_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_P_error_unshifted_reg0_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_P_error_unshifted_reg0_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '1',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '1',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => s00_axi_aclk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_P_error_unshifted_reg0_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_P_error_unshifted_reg0_OVERFLOW_UNCONNECTED,
      P(47) => P_error_unshifted_reg0_n_58,
      P(46) => P_error_unshifted_reg0_n_59,
      P(45) => P_error_unshifted_reg0_n_60,
      P(44) => P_error_unshifted_reg0_n_61,
      P(43) => P_error_unshifted_reg0_n_62,
      P(42) => P_error_unshifted_reg0_n_63,
      P(41) => P_error_unshifted_reg0_n_64,
      P(40) => P_error_unshifted_reg0_n_65,
      P(39) => P_error_unshifted_reg0_n_66,
      P(38) => P_error_unshifted_reg0_n_67,
      P(37) => P_error_unshifted_reg0_n_68,
      P(36) => P_error_unshifted_reg0_n_69,
      P(35) => P_error_unshifted_reg0_n_70,
      P(34) => P_error_unshifted_reg0_n_71,
      P(33) => P_error_unshifted_reg0_n_72,
      P(32) => P_error_unshifted_reg0_n_73,
      P(31) => P_error_unshifted_reg0_n_74,
      P(30) => P_error_unshifted_reg0_n_75,
      P(29) => P_error_unshifted_reg0_n_76,
      P(28) => P_error_unshifted_reg0_n_77,
      P(27) => P_error_unshifted_reg0_n_78,
      P(26) => P_error_unshifted_reg0_n_79,
      P(25) => P_error_unshifted_reg0_n_80,
      P(24) => P_error_unshifted_reg0_n_81,
      P(23) => P_error_unshifted_reg0_n_82,
      P(22) => P_error_unshifted_reg0_n_83,
      P(21) => P_error_unshifted_reg0_n_84,
      P(20) => P_error_unshifted_reg0_n_85,
      P(19) => P_error_unshifted_reg0_n_86,
      P(18) => P_error_unshifted_reg0_n_87,
      P(17) => P_error_unshifted_reg0_n_88,
      P(16) => P_error_unshifted_reg0_n_89,
      P(15) => P_error_unshifted_reg0_n_90,
      P(14) => P_error_unshifted_reg0_n_91,
      P(13) => P_error_unshifted_reg0_n_92,
      P(12) => P_error_unshifted_reg0_n_93,
      P(11) => P_error_unshifted_reg0_n_94,
      P(10) => P_error_unshifted_reg0_n_95,
      P(9) => P_error_unshifted_reg0_n_96,
      P(8) => P_error_unshifted_reg0_n_97,
      P(7) => P_error_unshifted_reg0_n_98,
      P(6) => P_error_unshifted_reg0_n_99,
      P(5) => P_error_unshifted_reg0_n_100,
      P(4) => P_error_unshifted_reg0_n_101,
      P(3) => P_error_unshifted_reg0_n_102,
      P(2) => P_error_unshifted_reg0_n_103,
      P(1) => P_error_unshifted_reg0_n_104,
      P(0) => P_error_unshifted_reg0_n_105,
      PATTERNBDETECT => NLW_P_error_unshifted_reg0_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_P_error_unshifted_reg0_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47) => P_error_unshifted_reg0_n_106,
      PCOUT(46) => P_error_unshifted_reg0_n_107,
      PCOUT(45) => P_error_unshifted_reg0_n_108,
      PCOUT(44) => P_error_unshifted_reg0_n_109,
      PCOUT(43) => P_error_unshifted_reg0_n_110,
      PCOUT(42) => P_error_unshifted_reg0_n_111,
      PCOUT(41) => P_error_unshifted_reg0_n_112,
      PCOUT(40) => P_error_unshifted_reg0_n_113,
      PCOUT(39) => P_error_unshifted_reg0_n_114,
      PCOUT(38) => P_error_unshifted_reg0_n_115,
      PCOUT(37) => P_error_unshifted_reg0_n_116,
      PCOUT(36) => P_error_unshifted_reg0_n_117,
      PCOUT(35) => P_error_unshifted_reg0_n_118,
      PCOUT(34) => P_error_unshifted_reg0_n_119,
      PCOUT(33) => P_error_unshifted_reg0_n_120,
      PCOUT(32) => P_error_unshifted_reg0_n_121,
      PCOUT(31) => P_error_unshifted_reg0_n_122,
      PCOUT(30) => P_error_unshifted_reg0_n_123,
      PCOUT(29) => P_error_unshifted_reg0_n_124,
      PCOUT(28) => P_error_unshifted_reg0_n_125,
      PCOUT(27) => P_error_unshifted_reg0_n_126,
      PCOUT(26) => P_error_unshifted_reg0_n_127,
      PCOUT(25) => P_error_unshifted_reg0_n_128,
      PCOUT(24) => P_error_unshifted_reg0_n_129,
      PCOUT(23) => P_error_unshifted_reg0_n_130,
      PCOUT(22) => P_error_unshifted_reg0_n_131,
      PCOUT(21) => P_error_unshifted_reg0_n_132,
      PCOUT(20) => P_error_unshifted_reg0_n_133,
      PCOUT(19) => P_error_unshifted_reg0_n_134,
      PCOUT(18) => P_error_unshifted_reg0_n_135,
      PCOUT(17) => P_error_unshifted_reg0_n_136,
      PCOUT(16) => P_error_unshifted_reg0_n_137,
      PCOUT(15) => P_error_unshifted_reg0_n_138,
      PCOUT(14) => P_error_unshifted_reg0_n_139,
      PCOUT(13) => P_error_unshifted_reg0_n_140,
      PCOUT(12) => P_error_unshifted_reg0_n_141,
      PCOUT(11) => P_error_unshifted_reg0_n_142,
      PCOUT(10) => P_error_unshifted_reg0_n_143,
      PCOUT(9) => P_error_unshifted_reg0_n_144,
      PCOUT(8) => P_error_unshifted_reg0_n_145,
      PCOUT(7) => P_error_unshifted_reg0_n_146,
      PCOUT(6) => P_error_unshifted_reg0_n_147,
      PCOUT(5) => P_error_unshifted_reg0_n_148,
      PCOUT(4) => P_error_unshifted_reg0_n_149,
      PCOUT(3) => P_error_unshifted_reg0_n_150,
      PCOUT(2) => P_error_unshifted_reg0_n_151,
      PCOUT(1) => P_error_unshifted_reg0_n_152,
      PCOUT(0) => P_error_unshifted_reg0_n_153,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => \^control_reg_reg[1]\,
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_P_error_unshifted_reg0_UNDERFLOW_UNCONNECTED
    );
P_error_unshifted_reg_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 1,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 1,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => P_error_unshifted_reg_reg_0(31),
      A(28) => P_error_unshifted_reg_reg_0(31),
      A(27) => P_error_unshifted_reg_reg_0(31),
      A(26) => P_error_unshifted_reg_reg_0(31),
      A(25) => P_error_unshifted_reg_reg_0(31),
      A(24) => P_error_unshifted_reg_reg_0(31),
      A(23) => P_error_unshifted_reg_reg_0(31),
      A(22) => P_error_unshifted_reg_reg_0(31),
      A(21) => P_error_unshifted_reg_reg_0(31),
      A(20) => P_error_unshifted_reg_reg_0(31),
      A(19) => P_error_unshifted_reg_reg_0(31),
      A(18) => P_error_unshifted_reg_reg_0(31),
      A(17) => P_error_unshifted_reg_reg_0(31),
      A(16) => P_error_unshifted_reg_reg_0(31),
      A(15) => P_error_unshifted_reg_reg_0(31),
      A(14 downto 0) => P_error_unshifted_reg_reg_0(31 downto 17),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_P_error_unshifted_reg_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => error_reg0(13),
      B(16) => error_reg0(13),
      B(15) => error_reg0(13),
      B(14) => error_reg0(13),
      B(13 downto 0) => error_reg0(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_P_error_unshifted_reg_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_P_error_unshifted_reg_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_P_error_unshifted_reg_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '1',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '1',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '1',
      CLK => s00_axi_aclk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_P_error_unshifted_reg_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"1010101",
      OVERFLOW => NLW_P_error_unshifted_reg_reg_OVERFLOW_UNCONNECTED,
      P(47) => P_error_unshifted_reg_reg_n_58,
      P(46) => P_error_unshifted_reg_reg_n_59,
      P(45) => P_error_unshifted_reg_reg_n_60,
      P(44) => P_error_unshifted_reg_reg_n_61,
      P(43) => P_error_unshifted_reg_reg_n_62,
      P(42) => P_error_unshifted_reg_reg_n_63,
      P(41) => P_error_unshifted_reg_reg_n_64,
      P(40) => P_error_unshifted_reg_reg_n_65,
      P(39) => P_error_unshifted_reg_reg_n_66,
      P(38) => P_error_unshifted_reg_reg_n_67,
      P(37) => P_error_unshifted_reg_reg_n_68,
      P(36) => P_error_unshifted_reg_reg_n_69,
      P(35) => P_error_unshifted_reg_reg_n_70,
      P(34) => P_error_unshifted_reg_reg_n_71,
      P(33) => P_error_unshifted_reg_reg_n_72,
      P(32) => P_error_unshifted_reg_reg_n_73,
      P(31) => P_error_unshifted_reg_reg_n_74,
      P(30) => P_error_unshifted_reg_reg_n_75,
      P(29) => P_error_unshifted_reg_reg_n_76,
      P(28) => P_error_unshifted_reg_reg_n_77,
      P(27) => P_error_unshifted_reg_reg_n_78,
      P(26) => P_error_unshifted_reg_reg_n_79,
      P(25) => P_error_unshifted_reg_reg_n_80,
      P(24) => P_error_unshifted_reg_reg_n_81,
      P(23) => P_error_unshifted_reg_reg_n_82,
      P(22) => P_error_unshifted_reg_reg_n_83,
      P(21) => P_error_unshifted_reg_reg_n_84,
      P(20) => P_error_unshifted_reg_reg_n_85,
      P(19) => P_error_unshifted_reg_reg_n_86,
      P(18) => P_error_unshifted_reg_reg_n_87,
      P(17) => P_error_unshifted_reg_reg_n_88,
      P(16) => P_error_unshifted_reg_reg_n_89,
      P(15) => P_error_unshifted_reg_reg_n_90,
      P(14 downto 0) => \^d\(31 downto 17),
      PATTERNBDETECT => NLW_P_error_unshifted_reg_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_P_error_unshifted_reg_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47) => P_error_unshifted_reg0_n_106,
      PCIN(46) => P_error_unshifted_reg0_n_107,
      PCIN(45) => P_error_unshifted_reg0_n_108,
      PCIN(44) => P_error_unshifted_reg0_n_109,
      PCIN(43) => P_error_unshifted_reg0_n_110,
      PCIN(42) => P_error_unshifted_reg0_n_111,
      PCIN(41) => P_error_unshifted_reg0_n_112,
      PCIN(40) => P_error_unshifted_reg0_n_113,
      PCIN(39) => P_error_unshifted_reg0_n_114,
      PCIN(38) => P_error_unshifted_reg0_n_115,
      PCIN(37) => P_error_unshifted_reg0_n_116,
      PCIN(36) => P_error_unshifted_reg0_n_117,
      PCIN(35) => P_error_unshifted_reg0_n_118,
      PCIN(34) => P_error_unshifted_reg0_n_119,
      PCIN(33) => P_error_unshifted_reg0_n_120,
      PCIN(32) => P_error_unshifted_reg0_n_121,
      PCIN(31) => P_error_unshifted_reg0_n_122,
      PCIN(30) => P_error_unshifted_reg0_n_123,
      PCIN(29) => P_error_unshifted_reg0_n_124,
      PCIN(28) => P_error_unshifted_reg0_n_125,
      PCIN(27) => P_error_unshifted_reg0_n_126,
      PCIN(26) => P_error_unshifted_reg0_n_127,
      PCIN(25) => P_error_unshifted_reg0_n_128,
      PCIN(24) => P_error_unshifted_reg0_n_129,
      PCIN(23) => P_error_unshifted_reg0_n_130,
      PCIN(22) => P_error_unshifted_reg0_n_131,
      PCIN(21) => P_error_unshifted_reg0_n_132,
      PCIN(20) => P_error_unshifted_reg0_n_133,
      PCIN(19) => P_error_unshifted_reg0_n_134,
      PCIN(18) => P_error_unshifted_reg0_n_135,
      PCIN(17) => P_error_unshifted_reg0_n_136,
      PCIN(16) => P_error_unshifted_reg0_n_137,
      PCIN(15) => P_error_unshifted_reg0_n_138,
      PCIN(14) => P_error_unshifted_reg0_n_139,
      PCIN(13) => P_error_unshifted_reg0_n_140,
      PCIN(12) => P_error_unshifted_reg0_n_141,
      PCIN(11) => P_error_unshifted_reg0_n_142,
      PCIN(10) => P_error_unshifted_reg0_n_143,
      PCIN(9) => P_error_unshifted_reg0_n_144,
      PCIN(8) => P_error_unshifted_reg0_n_145,
      PCIN(7) => P_error_unshifted_reg0_n_146,
      PCIN(6) => P_error_unshifted_reg0_n_147,
      PCIN(5) => P_error_unshifted_reg0_n_148,
      PCIN(4) => P_error_unshifted_reg0_n_149,
      PCIN(3) => P_error_unshifted_reg0_n_150,
      PCIN(2) => P_error_unshifted_reg0_n_151,
      PCIN(1) => P_error_unshifted_reg0_n_152,
      PCIN(0) => P_error_unshifted_reg0_n_153,
      PCOUT(47 downto 0) => NLW_P_error_unshifted_reg_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => \^control_reg_reg[1]\,
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => \^control_reg_reg[1]\,
      UNDERFLOW => NLW_P_error_unshifted_reg_reg_UNDERFLOW_UNCONNECTED
    );
\P_error_unshifted_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_105,
      Q => \^d\(0),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_95,
      Q => \^d\(10),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_94,
      Q => \^d\(11),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_93,
      Q => \^d\(12),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_92,
      Q => \^d\(13),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_91,
      Q => \^d\(14),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_90,
      Q => \^d\(15),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_89,
      Q => \^d\(16),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_104,
      Q => \^d\(1),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_103,
      Q => \^d\(2),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_102,
      Q => \^d\(3),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_101,
      Q => \^d\(4),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_100,
      Q => \^d\(5),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_99,
      Q => \^d\(6),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_98,
      Q => \^d\(7),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_97,
      Q => \^d\(8),
      R => \^control_reg_reg[1]\
    );
\P_error_unshifted_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_error_unshifted_reg0_n_96,
      Q => \^d\(9),
      R => \^control_reg_reg[1]\
    );
\autolock_input_mindex[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(0),
      I1 => \autolock_input_maxdex_reg[13]\(0),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(0)
    );
\autolock_input_mindex[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(10),
      I1 => \autolock_input_maxdex_reg[13]\(10),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(10)
    );
\autolock_input_mindex[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(11),
      I1 => \autolock_input_maxdex_reg[13]\(11),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(11)
    );
\autolock_input_mindex[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(12),
      I1 => \autolock_input_maxdex_reg[13]\(12),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(12)
    );
\autolock_input_mindex[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(13),
      I1 => \autolock_input_maxdex_reg[13]\(13),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(13)
    );
\autolock_input_mindex[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(1),
      I1 => \autolock_input_maxdex_reg[13]\(1),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(1)
    );
\autolock_input_mindex[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(2),
      I1 => \autolock_input_maxdex_reg[13]\(2),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(2)
    );
\autolock_input_mindex[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(3),
      I1 => \autolock_input_maxdex_reg[13]\(3),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(3)
    );
\autolock_input_mindex[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(4),
      I1 => \autolock_input_maxdex_reg[13]\(4),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(4)
    );
\autolock_input_mindex[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(5),
      I1 => \autolock_input_maxdex_reg[13]\(5),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(5)
    );
\autolock_input_mindex[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(6),
      I1 => \autolock_input_maxdex_reg[13]\(6),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(6)
    );
\autolock_input_mindex[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(7),
      I1 => \autolock_input_maxdex_reg[13]\(7),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(7)
    );
\autolock_input_mindex[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(8),
      I1 => \autolock_input_maxdex_reg[13]\(8),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(8)
    );
\autolock_input_mindex[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^loop_output_reg_reg[31]_0\(9),
      I1 => \autolock_input_maxdex_reg[13]\(9),
      I2 => \autolock_input_maxdex_reg[0]\(0),
      O => \loop_output_reg_reg[31]_1\(9)
    );
\axi_pi_output_reg[13]_inv_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(30),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(31),
      O => \axi_pi_output_reg[13]_inv_i_10_n_0\
    );
\axi_pi_output_reg[13]_inv_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(28),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(29),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_11_n_0\
    );
\axi_pi_output_reg[13]_inv_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(26),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(27),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_12_n_0\
    );
\axi_pi_output_reg[13]_inv_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(24),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(25),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_13_n_0\
    );
\axi_pi_output_reg[13]_inv_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(30),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(31),
      O => \axi_pi_output_reg[13]_inv_i_15_n_0\
    );
\axi_pi_output_reg[13]_inv_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(28),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(29),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_16_n_0\
    );
\axi_pi_output_reg[13]_inv_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(26),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(27),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_17_n_0\
    );
\axi_pi_output_reg[13]_inv_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(24),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(25),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_18_n_0\
    );
\axi_pi_output_reg[13]_inv_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(30),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(31),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_19_n_0\
    );
\axi_pi_output_reg[13]_inv_i_20\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(28),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(29),
      O => \axi_pi_output_reg[13]_inv_i_20_n_0\
    );
\axi_pi_output_reg[13]_inv_i_21\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(26),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(27),
      O => \axi_pi_output_reg[13]_inv_i_21_n_0\
    );
\axi_pi_output_reg[13]_inv_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(24),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(25),
      O => \axi_pi_output_reg[13]_inv_i_22_n_0\
    );
\axi_pi_output_reg[13]_inv_i_24\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(22),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(23),
      O => \axi_pi_output_reg[13]_inv_i_24_n_0\
    );
\axi_pi_output_reg[13]_inv_i_25\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(20),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(21),
      O => \axi_pi_output_reg[13]_inv_i_25_n_0\
    );
\axi_pi_output_reg[13]_inv_i_26\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(18),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(19),
      O => \axi_pi_output_reg[13]_inv_i_26_n_0\
    );
\axi_pi_output_reg[13]_inv_i_27\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(16),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(17),
      O => \axi_pi_output_reg[13]_inv_i_27_n_0\
    );
\axi_pi_output_reg[13]_inv_i_28\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(22),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(23),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_28_n_0\
    );
\axi_pi_output_reg[13]_inv_i_29\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(20),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(21),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_29_n_0\
    );
\axi_pi_output_reg[13]_inv_i_30\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(18),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(19),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_30_n_0\
    );
\axi_pi_output_reg[13]_inv_i_31\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(16),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(17),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_31_n_0\
    );
\axi_pi_output_reg[13]_inv_i_33\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(22),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(23),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_33_n_0\
    );
\axi_pi_output_reg[13]_inv_i_34\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(20),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(21),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_34_n_0\
    );
\axi_pi_output_reg[13]_inv_i_35\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(18),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(19),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_35_n_0\
    );
\axi_pi_output_reg[13]_inv_i_36\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(16),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(17),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_36_n_0\
    );
\axi_pi_output_reg[13]_inv_i_37\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(22),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(23),
      O => \axi_pi_output_reg[13]_inv_i_37_n_0\
    );
\axi_pi_output_reg[13]_inv_i_38\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(20),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(21),
      O => \axi_pi_output_reg[13]_inv_i_38_n_0\
    );
\axi_pi_output_reg[13]_inv_i_39\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(18),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(19),
      O => \axi_pi_output_reg[13]_inv_i_39_n_0\
    );
\axi_pi_output_reg[13]_inv_i_40\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(16),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(17),
      O => \axi_pi_output_reg[13]_inv_i_40_n_0\
    );
\axi_pi_output_reg[13]_inv_i_42\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(14),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(15),
      O => \axi_pi_output_reg[13]_inv_i_42_n_0\
    );
\axi_pi_output_reg[13]_inv_i_43\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(12),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(12),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(13),
      O => \axi_pi_output_reg[13]_inv_i_43_n_0\
    );
\axi_pi_output_reg[13]_inv_i_44\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(10),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(10),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(11),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(11),
      O => \axi_pi_output_reg[13]_inv_i_44_n_0\
    );
\axi_pi_output_reg[13]_inv_i_45\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(8),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(8),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(9),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(9),
      O => \axi_pi_output_reg[13]_inv_i_45_n_0\
    );
\axi_pi_output_reg[13]_inv_i_46\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(14),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(15),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_46_n_0\
    );
\axi_pi_output_reg[13]_inv_i_47\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(12),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(12),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(13),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_47_n_0\
    );
\axi_pi_output_reg[13]_inv_i_48\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(10),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(10),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(11),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(11),
      O => \axi_pi_output_reg[13]_inv_i_48_n_0\
    );
\axi_pi_output_reg[13]_inv_i_49\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(8),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(8),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(9),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(9),
      O => \axi_pi_output_reg[13]_inv_i_49_n_0\
    );
\axi_pi_output_reg[13]_inv_i_51\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(14),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(15),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_51_n_0\
    );
\axi_pi_output_reg[13]_inv_i_52\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(12),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(12),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(13),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_52_n_0\
    );
\axi_pi_output_reg[13]_inv_i_53\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(10),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(10),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(11),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(11),
      O => \axi_pi_output_reg[13]_inv_i_53_n_0\
    );
\axi_pi_output_reg[13]_inv_i_54\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(8),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(8),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(9),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(9),
      O => \axi_pi_output_reg[13]_inv_i_54_n_0\
    );
\axi_pi_output_reg[13]_inv_i_55\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(14),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(15),
      O => \axi_pi_output_reg[13]_inv_i_55_n_0\
    );
\axi_pi_output_reg[13]_inv_i_56\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(12),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(12),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_56_n_0\
    );
\axi_pi_output_reg[13]_inv_i_57\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(10),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(10),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(11),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(11),
      O => \axi_pi_output_reg[13]_inv_i_57_n_0\
    );
\axi_pi_output_reg[13]_inv_i_58\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(8),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(8),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(9),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(9),
      O => \axi_pi_output_reg[13]_inv_i_58_n_0\
    );
\axi_pi_output_reg[13]_inv_i_59\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(6),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(6),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(7),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(7),
      O => \axi_pi_output_reg[13]_inv_i_59_n_0\
    );
\axi_pi_output_reg[13]_inv_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(30),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(31),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      O => \axi_pi_output_reg[13]_inv_i_6_n_0\
    );
\axi_pi_output_reg[13]_inv_i_60\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(4),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(4),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(5),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(5),
      O => \axi_pi_output_reg[13]_inv_i_60_n_0\
    );
\axi_pi_output_reg[13]_inv_i_61\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(2),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(2),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(3),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(3),
      O => \axi_pi_output_reg[13]_inv_i_61_n_0\
    );
\axi_pi_output_reg[13]_inv_i_62\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(0),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(0),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(1),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(1),
      O => \axi_pi_output_reg[13]_inv_i_62_n_0\
    );
\axi_pi_output_reg[13]_inv_i_63\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(6),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(6),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(7),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(7),
      O => \axi_pi_output_reg[13]_inv_i_63_n_0\
    );
\axi_pi_output_reg[13]_inv_i_64\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(4),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(4),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(5),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(5),
      O => \axi_pi_output_reg[13]_inv_i_64_n_0\
    );
\axi_pi_output_reg[13]_inv_i_65\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(2),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(2),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(3),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(3),
      O => \axi_pi_output_reg[13]_inv_i_65_n_0\
    );
\axi_pi_output_reg[13]_inv_i_66\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(0),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(0),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(1),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(1),
      O => \axi_pi_output_reg[13]_inv_i_66_n_0\
    );
\axi_pi_output_reg[13]_inv_i_67\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(6),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(6),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(7),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(7),
      O => \axi_pi_output_reg[13]_inv_i_67_n_0\
    );
\axi_pi_output_reg[13]_inv_i_68\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(4),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(4),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(5),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(5),
      O => \axi_pi_output_reg[13]_inv_i_68_n_0\
    );
\axi_pi_output_reg[13]_inv_i_69\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(2),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(2),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(3),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(3),
      O => \axi_pi_output_reg[13]_inv_i_69_n_0\
    );
\axi_pi_output_reg[13]_inv_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(28),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(29),
      O => \axi_pi_output_reg[13]_inv_i_7_n_0\
    );
\axi_pi_output_reg[13]_inv_i_70\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(0),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(0),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(1),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(1),
      O => \axi_pi_output_reg[13]_inv_i_70_n_0\
    );
\axi_pi_output_reg[13]_inv_i_71\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(6),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(6),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(7),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(7),
      O => \axi_pi_output_reg[13]_inv_i_71_n_0\
    );
\axi_pi_output_reg[13]_inv_i_72\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(4),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(4),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(5),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(5),
      O => \axi_pi_output_reg[13]_inv_i_72_n_0\
    );
\axi_pi_output_reg[13]_inv_i_73\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(2),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(2),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(3),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(3),
      O => \axi_pi_output_reg[13]_inv_i_73_n_0\
    );
\axi_pi_output_reg[13]_inv_i_74\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(0),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(0),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(1),
      I3 => \axi_pi_output_reg_reg[13]_inv_i_4_0\(1),
      O => \axi_pi_output_reg[13]_inv_i_74_n_0\
    );
\axi_pi_output_reg[13]_inv_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(26),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(27),
      O => \axi_pi_output_reg[13]_inv_i_8_n_0\
    );
\axi_pi_output_reg[13]_inv_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(24),
      I1 => \axi_pi_output_reg_reg[13]_inv_i_3_0\(13),
      I2 => \axi_pi_output_reg_reg[13]_inv_i_3_1\(25),
      O => \axi_pi_output_reg[13]_inv_i_9_n_0\
    );
\axi_pi_output_reg_reg[13]_inv_i_14\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_pi_output_reg_reg[13]_inv_i_32_n_0\,
      CO(3) => \axi_pi_output_reg_reg[13]_inv_i_14_n_0\,
      CO(2) => \axi_pi_output_reg_reg[13]_inv_i_14_n_1\,
      CO(1) => \axi_pi_output_reg_reg[13]_inv_i_14_n_2\,
      CO(0) => \axi_pi_output_reg_reg[13]_inv_i_14_n_3\,
      CYINIT => '0',
      DI(3) => \axi_pi_output_reg[13]_inv_i_33_n_0\,
      DI(2) => \axi_pi_output_reg[13]_inv_i_34_n_0\,
      DI(1) => \axi_pi_output_reg[13]_inv_i_35_n_0\,
      DI(0) => \axi_pi_output_reg[13]_inv_i_36_n_0\,
      O(3 downto 0) => \NLW_axi_pi_output_reg_reg[13]_inv_i_14_O_UNCONNECTED\(3 downto 0),
      S(3) => \axi_pi_output_reg[13]_inv_i_37_n_0\,
      S(2) => \axi_pi_output_reg[13]_inv_i_38_n_0\,
      S(1) => \axi_pi_output_reg[13]_inv_i_39_n_0\,
      S(0) => \axi_pi_output_reg[13]_inv_i_40_n_0\
    );
\axi_pi_output_reg_reg[13]_inv_i_23\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_pi_output_reg_reg[13]_inv_i_41_n_0\,
      CO(3) => \axi_pi_output_reg_reg[13]_inv_i_23_n_0\,
      CO(2) => \axi_pi_output_reg_reg[13]_inv_i_23_n_1\,
      CO(1) => \axi_pi_output_reg_reg[13]_inv_i_23_n_2\,
      CO(0) => \axi_pi_output_reg_reg[13]_inv_i_23_n_3\,
      CYINIT => '0',
      DI(3) => \axi_pi_output_reg[13]_inv_i_42_n_0\,
      DI(2) => \axi_pi_output_reg[13]_inv_i_43_n_0\,
      DI(1) => \axi_pi_output_reg[13]_inv_i_44_n_0\,
      DI(0) => \axi_pi_output_reg[13]_inv_i_45_n_0\,
      O(3 downto 0) => \NLW_axi_pi_output_reg_reg[13]_inv_i_23_O_UNCONNECTED\(3 downto 0),
      S(3) => \axi_pi_output_reg[13]_inv_i_46_n_0\,
      S(2) => \axi_pi_output_reg[13]_inv_i_47_n_0\,
      S(1) => \axi_pi_output_reg[13]_inv_i_48_n_0\,
      S(0) => \axi_pi_output_reg[13]_inv_i_49_n_0\
    );
\axi_pi_output_reg_reg[13]_inv_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_pi_output_reg_reg[13]_inv_i_5_n_0\,
      CO(3) => \^autolock_max_reg_reg[30]\(0),
      CO(2) => \axi_pi_output_reg_reg[13]_inv_i_3_n_1\,
      CO(1) => \axi_pi_output_reg_reg[13]_inv_i_3_n_2\,
      CO(0) => \axi_pi_output_reg_reg[13]_inv_i_3_n_3\,
      CYINIT => '0',
      DI(3) => \axi_pi_output_reg[13]_inv_i_6_n_0\,
      DI(2) => \axi_pi_output_reg[13]_inv_i_7_n_0\,
      DI(1) => \axi_pi_output_reg[13]_inv_i_8_n_0\,
      DI(0) => \axi_pi_output_reg[13]_inv_i_9_n_0\,
      O(3 downto 0) => \NLW_axi_pi_output_reg_reg[13]_inv_i_3_O_UNCONNECTED\(3 downto 0),
      S(3) => \axi_pi_output_reg[13]_inv_i_10_n_0\,
      S(2) => \axi_pi_output_reg[13]_inv_i_11_n_0\,
      S(1) => \axi_pi_output_reg[13]_inv_i_12_n_0\,
      S(0) => \axi_pi_output_reg[13]_inv_i_13_n_0\
    );
\axi_pi_output_reg_reg[13]_inv_i_32\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_pi_output_reg_reg[13]_inv_i_50_n_0\,
      CO(3) => \axi_pi_output_reg_reg[13]_inv_i_32_n_0\,
      CO(2) => \axi_pi_output_reg_reg[13]_inv_i_32_n_1\,
      CO(1) => \axi_pi_output_reg_reg[13]_inv_i_32_n_2\,
      CO(0) => \axi_pi_output_reg_reg[13]_inv_i_32_n_3\,
      CYINIT => '0',
      DI(3) => \axi_pi_output_reg[13]_inv_i_51_n_0\,
      DI(2) => \axi_pi_output_reg[13]_inv_i_52_n_0\,
      DI(1) => \axi_pi_output_reg[13]_inv_i_53_n_0\,
      DI(0) => \axi_pi_output_reg[13]_inv_i_54_n_0\,
      O(3 downto 0) => \NLW_axi_pi_output_reg_reg[13]_inv_i_32_O_UNCONNECTED\(3 downto 0),
      S(3) => \axi_pi_output_reg[13]_inv_i_55_n_0\,
      S(2) => \axi_pi_output_reg[13]_inv_i_56_n_0\,
      S(1) => \axi_pi_output_reg[13]_inv_i_57_n_0\,
      S(0) => \axi_pi_output_reg[13]_inv_i_58_n_0\
    );
\axi_pi_output_reg_reg[13]_inv_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_pi_output_reg_reg[13]_inv_i_14_n_0\,
      CO(3) => \^co\(0),
      CO(2) => \axi_pi_output_reg_reg[13]_inv_i_4_n_1\,
      CO(1) => \axi_pi_output_reg_reg[13]_inv_i_4_n_2\,
      CO(0) => \axi_pi_output_reg_reg[13]_inv_i_4_n_3\,
      CYINIT => '0',
      DI(3) => \axi_pi_output_reg[13]_inv_i_15_n_0\,
      DI(2) => \axi_pi_output_reg[13]_inv_i_16_n_0\,
      DI(1) => \axi_pi_output_reg[13]_inv_i_17_n_0\,
      DI(0) => \axi_pi_output_reg[13]_inv_i_18_n_0\,
      O(3 downto 0) => \NLW_axi_pi_output_reg_reg[13]_inv_i_4_O_UNCONNECTED\(3 downto 0),
      S(3) => \axi_pi_output_reg[13]_inv_i_19_n_0\,
      S(2) => \axi_pi_output_reg[13]_inv_i_20_n_0\,
      S(1) => \axi_pi_output_reg[13]_inv_i_21_n_0\,
      S(0) => \axi_pi_output_reg[13]_inv_i_22_n_0\
    );
\axi_pi_output_reg_reg[13]_inv_i_41\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \axi_pi_output_reg_reg[13]_inv_i_41_n_0\,
      CO(2) => \axi_pi_output_reg_reg[13]_inv_i_41_n_1\,
      CO(1) => \axi_pi_output_reg_reg[13]_inv_i_41_n_2\,
      CO(0) => \axi_pi_output_reg_reg[13]_inv_i_41_n_3\,
      CYINIT => '0',
      DI(3) => \axi_pi_output_reg[13]_inv_i_59_n_0\,
      DI(2) => \axi_pi_output_reg[13]_inv_i_60_n_0\,
      DI(1) => \axi_pi_output_reg[13]_inv_i_61_n_0\,
      DI(0) => \axi_pi_output_reg[13]_inv_i_62_n_0\,
      O(3 downto 0) => \NLW_axi_pi_output_reg_reg[13]_inv_i_41_O_UNCONNECTED\(3 downto 0),
      S(3) => \axi_pi_output_reg[13]_inv_i_63_n_0\,
      S(2) => \axi_pi_output_reg[13]_inv_i_64_n_0\,
      S(1) => \axi_pi_output_reg[13]_inv_i_65_n_0\,
      S(0) => \axi_pi_output_reg[13]_inv_i_66_n_0\
    );
\axi_pi_output_reg_reg[13]_inv_i_5\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_pi_output_reg_reg[13]_inv_i_23_n_0\,
      CO(3) => \axi_pi_output_reg_reg[13]_inv_i_5_n_0\,
      CO(2) => \axi_pi_output_reg_reg[13]_inv_i_5_n_1\,
      CO(1) => \axi_pi_output_reg_reg[13]_inv_i_5_n_2\,
      CO(0) => \axi_pi_output_reg_reg[13]_inv_i_5_n_3\,
      CYINIT => '0',
      DI(3) => \axi_pi_output_reg[13]_inv_i_24_n_0\,
      DI(2) => \axi_pi_output_reg[13]_inv_i_25_n_0\,
      DI(1) => \axi_pi_output_reg[13]_inv_i_26_n_0\,
      DI(0) => \axi_pi_output_reg[13]_inv_i_27_n_0\,
      O(3 downto 0) => \NLW_axi_pi_output_reg_reg[13]_inv_i_5_O_UNCONNECTED\(3 downto 0),
      S(3) => \axi_pi_output_reg[13]_inv_i_28_n_0\,
      S(2) => \axi_pi_output_reg[13]_inv_i_29_n_0\,
      S(1) => \axi_pi_output_reg[13]_inv_i_30_n_0\,
      S(0) => \axi_pi_output_reg[13]_inv_i_31_n_0\
    );
\axi_pi_output_reg_reg[13]_inv_i_50\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \axi_pi_output_reg_reg[13]_inv_i_50_n_0\,
      CO(2) => \axi_pi_output_reg_reg[13]_inv_i_50_n_1\,
      CO(1) => \axi_pi_output_reg_reg[13]_inv_i_50_n_2\,
      CO(0) => \axi_pi_output_reg_reg[13]_inv_i_50_n_3\,
      CYINIT => '0',
      DI(3) => \axi_pi_output_reg[13]_inv_i_67_n_0\,
      DI(2) => \axi_pi_output_reg[13]_inv_i_68_n_0\,
      DI(1) => \axi_pi_output_reg[13]_inv_i_69_n_0\,
      DI(0) => \axi_pi_output_reg[13]_inv_i_70_n_0\,
      O(3 downto 0) => \NLW_axi_pi_output_reg_reg[13]_inv_i_50_O_UNCONNECTED\(3 downto 0),
      S(3) => \axi_pi_output_reg[13]_inv_i_71_n_0\,
      S(2) => \axi_pi_output_reg[13]_inv_i_72_n_0\,
      S(1) => \axi_pi_output_reg[13]_inv_i_73_n_0\,
      S(0) => \axi_pi_output_reg[13]_inv_i_74_n_0\
    );
\error_reg[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(11),
      I1 => pid_loop_0_input_reg(11),
      O => \error_reg[11]_i_2_n_0\
    );
\error_reg[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(10),
      I1 => pid_loop_0_input_reg(10),
      O => \error_reg[11]_i_3_n_0\
    );
\error_reg[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(9),
      I1 => pid_loop_0_input_reg(9),
      O => \error_reg[11]_i_4_n_0\
    );
\error_reg[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(8),
      I1 => pid_loop_0_input_reg(8),
      O => \error_reg[11]_i_5_n_0\
    );
\error_reg[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABABABAFFFFFFFF"
    )
        port map (
      I0 => \I_term_q_reg[0]_0\(0),
      I1 => \I_term_q_reg[0]_0\(1),
      I2 => \I_term_q_reg[0]_0\(2),
      I3 => \^co\(0),
      I4 => \^autolock_max_reg_reg[30]\(0),
      I5 => s00_axi_aresetn,
      O => \^control_reg_reg[1]\
    );
\error_reg[13]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(13),
      I1 => pid_loop_0_input_reg(13),
      O => \error_reg[13]_i_3_n_0\
    );
\error_reg[13]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(12),
      I1 => pid_loop_0_input_reg(12),
      O => \error_reg[13]_i_4_n_0\
    );
\error_reg[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(3),
      I1 => pid_loop_0_input_reg(3),
      O => \error_reg[3]_i_2_n_0\
    );
\error_reg[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(2),
      I1 => pid_loop_0_input_reg(2),
      O => \error_reg[3]_i_3_n_0\
    );
\error_reg[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(1),
      I1 => pid_loop_0_input_reg(1),
      O => \error_reg[3]_i_4_n_0\
    );
\error_reg[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(0),
      I1 => pid_loop_0_input_reg(0),
      O => \error_reg[3]_i_5_n_0\
    );
\error_reg[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(7),
      I1 => pid_loop_0_input_reg(7),
      O => \error_reg[7]_i_2_n_0\
    );
\error_reg[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(6),
      I1 => pid_loop_0_input_reg(6),
      O => \error_reg[7]_i_3_n_0\
    );
\error_reg[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(5),
      I1 => pid_loop_0_input_reg(5),
      O => \error_reg[7]_i_4_n_0\
    );
\error_reg[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => setpoint_reg(4),
      I1 => pid_loop_0_input_reg(4),
      O => \error_reg[7]_i_5_n_0\
    );
\error_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(0),
      Q => \error_reg_reg[13]_0\(0),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(10),
      Q => \error_reg_reg[13]_0\(10),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(11),
      Q => \error_reg_reg[13]_0\(11),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \error_reg_reg[7]_i_1_n_0\,
      CO(3) => \error_reg_reg[11]_i_1_n_0\,
      CO(2) => \error_reg_reg[11]_i_1_n_1\,
      CO(1) => \error_reg_reg[11]_i_1_n_2\,
      CO(0) => \error_reg_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => setpoint_reg(11 downto 8),
      O(3 downto 0) => error_reg0(11 downto 8),
      S(3) => \error_reg[11]_i_2_n_0\,
      S(2) => \error_reg[11]_i_3_n_0\,
      S(1) => \error_reg[11]_i_4_n_0\,
      S(0) => \error_reg[11]_i_5_n_0\
    );
\error_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(12),
      Q => \error_reg_reg[13]_0\(12),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(13),
      Q => \error_reg_reg[13]_0\(13),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[13]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \error_reg_reg[11]_i_1_n_0\,
      CO(3 downto 1) => \NLW_error_reg_reg[13]_i_2_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \error_reg_reg[13]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => setpoint_reg(12),
      O(3 downto 2) => \NLW_error_reg_reg[13]_i_2_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => error_reg0(13 downto 12),
      S(3 downto 2) => B"00",
      S(1) => \error_reg[13]_i_3_n_0\,
      S(0) => \error_reg[13]_i_4_n_0\
    );
\error_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(1),
      Q => \error_reg_reg[13]_0\(1),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(2),
      Q => \error_reg_reg[13]_0\(2),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(3),
      Q => \error_reg_reg[13]_0\(3),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \error_reg_reg[3]_i_1_n_0\,
      CO(2) => \error_reg_reg[3]_i_1_n_1\,
      CO(1) => \error_reg_reg[3]_i_1_n_2\,
      CO(0) => \error_reg_reg[3]_i_1_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => setpoint_reg(3 downto 0),
      O(3 downto 0) => error_reg0(3 downto 0),
      S(3) => \error_reg[3]_i_2_n_0\,
      S(2) => \error_reg[3]_i_3_n_0\,
      S(1) => \error_reg[3]_i_4_n_0\,
      S(0) => \error_reg[3]_i_5_n_0\
    );
\error_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(4),
      Q => \error_reg_reg[13]_0\(4),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(5),
      Q => \error_reg_reg[13]_0\(5),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(6),
      Q => \error_reg_reg[13]_0\(6),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(7),
      Q => \error_reg_reg[13]_0\(7),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \error_reg_reg[3]_i_1_n_0\,
      CO(3) => \error_reg_reg[7]_i_1_n_0\,
      CO(2) => \error_reg_reg[7]_i_1_n_1\,
      CO(1) => \error_reg_reg[7]_i_1_n_2\,
      CO(0) => \error_reg_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => setpoint_reg(7 downto 4),
      O(3 downto 0) => error_reg0(7 downto 4),
      S(3) => \error_reg[7]_i_2_n_0\,
      S(2) => \error_reg[7]_i_3_n_0\,
      S(1) => \error_reg[7]_i_4_n_0\,
      S(0) => \error_reg[7]_i_5_n_0\
    );
\error_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(8),
      Q => \error_reg_reg[13]_0\(8),
      R => \^control_reg_reg[1]\
    );
\error_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => error_reg0(9),
      Q => \error_reg_reg[13]_0\(9),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg[19]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(13),
      I1 => \^i_term_mon\(13),
      O => \loop_output_reg[19]_i_10_n_0\
    );
\loop_output_reg[19]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(12),
      I1 => \^i_term_mon\(12),
      O => \loop_output_reg[19]_i_11_n_0\
    );
\loop_output_reg[19]_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(11),
      I1 => \^i_term_mon\(11),
      O => \loop_output_reg[19]_i_13_n_0\
    );
\loop_output_reg[19]_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(10),
      I1 => \^i_term_mon\(10),
      O => \loop_output_reg[19]_i_14_n_0\
    );
\loop_output_reg[19]_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(9),
      I1 => \^i_term_mon\(9),
      O => \loop_output_reg[19]_i_15_n_0\
    );
\loop_output_reg[19]_i_16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(8),
      I1 => \^i_term_mon\(8),
      O => \loop_output_reg[19]_i_16_n_0\
    );
\loop_output_reg[19]_i_18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(7),
      I1 => \^i_term_mon\(7),
      O => \loop_output_reg[19]_i_18_n_0\
    );
\loop_output_reg[19]_i_19\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(6),
      I1 => \^i_term_mon\(6),
      O => \loop_output_reg[19]_i_19_n_0\
    );
\loop_output_reg[19]_i_20\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(5),
      I1 => \^i_term_mon\(5),
      O => \loop_output_reg[19]_i_20_n_0\
    );
\loop_output_reg[19]_i_21\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(4),
      I1 => \^i_term_mon\(4),
      O => \loop_output_reg[19]_i_21_n_0\
    );
\loop_output_reg[19]_i_22\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(3),
      I1 => \^i_term_mon\(3),
      O => \loop_output_reg[19]_i_22_n_0\
    );
\loop_output_reg[19]_i_23\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(2),
      I1 => \^i_term_mon\(2),
      O => \loop_output_reg[19]_i_23_n_0\
    );
\loop_output_reg[19]_i_24\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(1),
      I1 => \^i_term_mon\(1),
      O => \loop_output_reg[19]_i_24_n_0\
    );
\loop_output_reg[19]_i_25\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(0),
      I1 => \^i_term_mon\(0),
      O => \loop_output_reg[19]_i_25_n_0\
    );
\loop_output_reg[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(19),
      I1 => \^i_term_mon\(19),
      O => \loop_output_reg[19]_i_3_n_0\
    );
\loop_output_reg[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(18),
      I1 => \^i_term_mon\(18),
      O => \loop_output_reg[19]_i_4_n_0\
    );
\loop_output_reg[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(17),
      I1 => \^i_term_mon\(17),
      O => \loop_output_reg[19]_i_5_n_0\
    );
\loop_output_reg[19]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(16),
      I1 => \^i_term_mon\(16),
      O => \loop_output_reg[19]_i_6_n_0\
    );
\loop_output_reg[19]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(15),
      I1 => \^i_term_mon\(15),
      O => \loop_output_reg[19]_i_8_n_0\
    );
\loop_output_reg[19]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(14),
      I1 => \^i_term_mon\(14),
      O => \loop_output_reg[19]_i_9_n_0\
    );
\loop_output_reg[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(23),
      I1 => \^i_term_mon\(23),
      O => \loop_output_reg[23]_i_2_n_0\
    );
\loop_output_reg[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(22),
      I1 => \^i_term_mon\(22),
      O => \loop_output_reg[23]_i_3_n_0\
    );
\loop_output_reg[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(21),
      I1 => \^i_term_mon\(21),
      O => \loop_output_reg[23]_i_4_n_0\
    );
\loop_output_reg[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(20),
      I1 => \^i_term_mon\(20),
      O => \loop_output_reg[23]_i_5_n_0\
    );
\loop_output_reg[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(27),
      I1 => \^i_term_mon\(27),
      O => \loop_output_reg[27]_i_2_n_0\
    );
\loop_output_reg[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(26),
      I1 => \^i_term_mon\(26),
      O => \loop_output_reg[27]_i_3_n_0\
    );
\loop_output_reg[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(25),
      I1 => \^i_term_mon\(25),
      O => \loop_output_reg[27]_i_4_n_0\
    );
\loop_output_reg[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(24),
      I1 => \^i_term_mon\(24),
      O => \loop_output_reg[27]_i_5_n_0\
    );
\loop_output_reg[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(31),
      I1 => \^i_term_mon\(31),
      O => \loop_output_reg[31]_i_2_n_0\
    );
\loop_output_reg[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(30),
      I1 => \^i_term_mon\(30),
      O => \loop_output_reg[31]_i_3_n_0\
    );
\loop_output_reg[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(29),
      I1 => \^i_term_mon\(29),
      O => \loop_output_reg[31]_i_4_n_0\
    );
\loop_output_reg[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^d\(28),
      I1 => \^i_term_mon\(28),
      O => \loop_output_reg[31]_i_5_n_0\
    );
\loop_output_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[19]_i_1_n_5\,
      Q => \^loop_output_reg_reg[31]_0\(0),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[19]_i_1_n_4\,
      Q => \^loop_output_reg_reg[31]_0\(1),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_output_reg_reg[19]_i_2_n_0\,
      CO(3) => \loop_output_reg_reg[19]_i_1_n_0\,
      CO(2) => \loop_output_reg_reg[19]_i_1_n_1\,
      CO(1) => \loop_output_reg_reg[19]_i_1_n_2\,
      CO(0) => \loop_output_reg_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^d\(19 downto 16),
      O(3) => \loop_output_reg_reg[19]_i_1_n_4\,
      O(2) => \loop_output_reg_reg[19]_i_1_n_5\,
      O(1 downto 0) => \NLW_loop_output_reg_reg[19]_i_1_O_UNCONNECTED\(1 downto 0),
      S(3) => \loop_output_reg[19]_i_3_n_0\,
      S(2) => \loop_output_reg[19]_i_4_n_0\,
      S(1) => \loop_output_reg[19]_i_5_n_0\,
      S(0) => \loop_output_reg[19]_i_6_n_0\
    );
\loop_output_reg_reg[19]_i_12\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_output_reg_reg[19]_i_17_n_0\,
      CO(3) => \loop_output_reg_reg[19]_i_12_n_0\,
      CO(2) => \loop_output_reg_reg[19]_i_12_n_1\,
      CO(1) => \loop_output_reg_reg[19]_i_12_n_2\,
      CO(0) => \loop_output_reg_reg[19]_i_12_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^d\(7 downto 4),
      O(3 downto 0) => \NLW_loop_output_reg_reg[19]_i_12_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_output_reg[19]_i_18_n_0\,
      S(2) => \loop_output_reg[19]_i_19_n_0\,
      S(1) => \loop_output_reg[19]_i_20_n_0\,
      S(0) => \loop_output_reg[19]_i_21_n_0\
    );
\loop_output_reg_reg[19]_i_17\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \loop_output_reg_reg[19]_i_17_n_0\,
      CO(2) => \loop_output_reg_reg[19]_i_17_n_1\,
      CO(1) => \loop_output_reg_reg[19]_i_17_n_2\,
      CO(0) => \loop_output_reg_reg[19]_i_17_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^d\(3 downto 0),
      O(3 downto 0) => \NLW_loop_output_reg_reg[19]_i_17_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_output_reg[19]_i_22_n_0\,
      S(2) => \loop_output_reg[19]_i_23_n_0\,
      S(1) => \loop_output_reg[19]_i_24_n_0\,
      S(0) => \loop_output_reg[19]_i_25_n_0\
    );
\loop_output_reg_reg[19]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_output_reg_reg[19]_i_7_n_0\,
      CO(3) => \loop_output_reg_reg[19]_i_2_n_0\,
      CO(2) => \loop_output_reg_reg[19]_i_2_n_1\,
      CO(1) => \loop_output_reg_reg[19]_i_2_n_2\,
      CO(0) => \loop_output_reg_reg[19]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^d\(15 downto 12),
      O(3 downto 0) => \NLW_loop_output_reg_reg[19]_i_2_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_output_reg[19]_i_8_n_0\,
      S(2) => \loop_output_reg[19]_i_9_n_0\,
      S(1) => \loop_output_reg[19]_i_10_n_0\,
      S(0) => \loop_output_reg[19]_i_11_n_0\
    );
\loop_output_reg_reg[19]_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_output_reg_reg[19]_i_12_n_0\,
      CO(3) => \loop_output_reg_reg[19]_i_7_n_0\,
      CO(2) => \loop_output_reg_reg[19]_i_7_n_1\,
      CO(1) => \loop_output_reg_reg[19]_i_7_n_2\,
      CO(0) => \loop_output_reg_reg[19]_i_7_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^d\(11 downto 8),
      O(3 downto 0) => \NLW_loop_output_reg_reg[19]_i_7_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_output_reg[19]_i_13_n_0\,
      S(2) => \loop_output_reg[19]_i_14_n_0\,
      S(1) => \loop_output_reg[19]_i_15_n_0\,
      S(0) => \loop_output_reg[19]_i_16_n_0\
    );
\loop_output_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[23]_i_1_n_7\,
      Q => \^loop_output_reg_reg[31]_0\(2),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[23]_i_1_n_6\,
      Q => \^loop_output_reg_reg[31]_0\(3),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[23]_i_1_n_5\,
      Q => \^loop_output_reg_reg[31]_0\(4),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[23]_i_1_n_4\,
      Q => \^loop_output_reg_reg[31]_0\(5),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_output_reg_reg[19]_i_1_n_0\,
      CO(3) => \loop_output_reg_reg[23]_i_1_n_0\,
      CO(2) => \loop_output_reg_reg[23]_i_1_n_1\,
      CO(1) => \loop_output_reg_reg[23]_i_1_n_2\,
      CO(0) => \loop_output_reg_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^d\(23 downto 20),
      O(3) => \loop_output_reg_reg[23]_i_1_n_4\,
      O(2) => \loop_output_reg_reg[23]_i_1_n_5\,
      O(1) => \loop_output_reg_reg[23]_i_1_n_6\,
      O(0) => \loop_output_reg_reg[23]_i_1_n_7\,
      S(3) => \loop_output_reg[23]_i_2_n_0\,
      S(2) => \loop_output_reg[23]_i_3_n_0\,
      S(1) => \loop_output_reg[23]_i_4_n_0\,
      S(0) => \loop_output_reg[23]_i_5_n_0\
    );
\loop_output_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[27]_i_1_n_7\,
      Q => \^loop_output_reg_reg[31]_0\(6),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[27]_i_1_n_6\,
      Q => \^loop_output_reg_reg[31]_0\(7),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[27]_i_1_n_5\,
      Q => \^loop_output_reg_reg[31]_0\(8),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[27]_i_1_n_4\,
      Q => \^loop_output_reg_reg[31]_0\(9),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_output_reg_reg[23]_i_1_n_0\,
      CO(3) => \loop_output_reg_reg[27]_i_1_n_0\,
      CO(2) => \loop_output_reg_reg[27]_i_1_n_1\,
      CO(1) => \loop_output_reg_reg[27]_i_1_n_2\,
      CO(0) => \loop_output_reg_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^d\(27 downto 24),
      O(3) => \loop_output_reg_reg[27]_i_1_n_4\,
      O(2) => \loop_output_reg_reg[27]_i_1_n_5\,
      O(1) => \loop_output_reg_reg[27]_i_1_n_6\,
      O(0) => \loop_output_reg_reg[27]_i_1_n_7\,
      S(3) => \loop_output_reg[27]_i_2_n_0\,
      S(2) => \loop_output_reg[27]_i_3_n_0\,
      S(1) => \loop_output_reg[27]_i_4_n_0\,
      S(0) => \loop_output_reg[27]_i_5_n_0\
    );
\loop_output_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[31]_i_1_n_7\,
      Q => \^loop_output_reg_reg[31]_0\(10),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[31]_i_1_n_6\,
      Q => \^loop_output_reg_reg[31]_0\(11),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[31]_i_1_n_5\,
      Q => \^loop_output_reg_reg[31]_0\(12),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \loop_output_reg_reg[31]_i_1_n_4\,
      Q => \^loop_output_reg_reg[31]_0\(13),
      R => \^control_reg_reg[1]\
    );
\loop_output_reg_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_output_reg_reg[27]_i_1_n_0\,
      CO(3) => \NLW_loop_output_reg_reg[31]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \loop_output_reg_reg[31]_i_1_n_1\,
      CO(1) => \loop_output_reg_reg[31]_i_1_n_2\,
      CO(0) => \loop_output_reg_reg[31]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \^d\(30 downto 28),
      O(3) => \loop_output_reg_reg[31]_i_1_n_4\,
      O(2) => \loop_output_reg_reg[31]_i_1_n_5\,
      O(1) => \loop_output_reg_reg[31]_i_1_n_6\,
      O(0) => \loop_output_reg_reg[31]_i_1_n_7\,
      S(3) => \loop_output_reg[31]_i_2_n_0\,
      S(2) => \loop_output_reg[31]_i_3_n_0\,
      S(1) => \loop_output_reg[31]_i_4_n_0\,
      S(0) => \loop_output_reg[31]_i_5_n_0\
    );
\ramplitude_min_reg[21]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(3),
      I1 => \^loop_output_reg_reg[31]_0\(2),
      O => \ramplitude_min_reg[21]_i_3_n_0\
    );
\ramplitude_min_reg[21]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(2),
      I1 => \^loop_output_reg_reg[31]_0\(1),
      O => \ramplitude_min_reg[21]_i_4_n_0\
    );
\ramplitude_min_reg[21]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(1),
      I1 => \^loop_output_reg_reg[31]_0\(0),
      O => \ramplitude_min_reg[21]_i_5_n_0\
    );
\ramplitude_min_reg[25]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(7),
      I1 => \^loop_output_reg_reg[31]_0\(6),
      O => \ramplitude_min_reg[25]_i_3_n_0\
    );
\ramplitude_min_reg[25]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(6),
      I1 => \^loop_output_reg_reg[31]_0\(5),
      O => \ramplitude_min_reg[25]_i_4_n_0\
    );
\ramplitude_min_reg[25]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(5),
      I1 => \^loop_output_reg_reg[31]_0\(4),
      O => \ramplitude_min_reg[25]_i_5_n_0\
    );
\ramplitude_min_reg[25]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(4),
      I1 => \^loop_output_reg_reg[31]_0\(3),
      O => \ramplitude_min_reg[25]_i_6_n_0\
    );
\ramplitude_min_reg[29]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(11),
      I1 => \^loop_output_reg_reg[31]_0\(10),
      O => \ramplitude_min_reg[29]_i_3_n_0\
    );
\ramplitude_min_reg[29]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(10),
      I1 => \^loop_output_reg_reg[31]_0\(9),
      O => \ramplitude_min_reg[29]_i_4_n_0\
    );
\ramplitude_min_reg[29]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(9),
      I1 => \^loop_output_reg_reg[31]_0\(8),
      O => \ramplitude_min_reg[29]_i_5_n_0\
    );
\ramplitude_min_reg[29]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(8),
      I1 => \^loop_output_reg_reg[31]_0\(7),
      O => \ramplitude_min_reg[29]_i_6_n_0\
    );
\ramplitude_min_reg[31]_i_31\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(13),
      I1 => \^loop_output_reg_reg[31]_0\(12),
      O => \ramplitude_min_reg[31]_i_31_n_0\
    );
\ramplitude_min_reg[31]_i_32\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_6_0\(12),
      I1 => \^loop_output_reg_reg[31]_0\(11),
      O => \ramplitude_min_reg[31]_i_32_n_0\
    );
\ramplitude_min_reg_reg[21]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ramplitude_min_reg_reg[21]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[21]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[21]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[21]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => \ramplitude_min_reg_reg[31]_i_6_0\(3 downto 1),
      DI(0) => '0',
      O(3 downto 0) => ramp_module_0_reset(3 downto 0),
      S(3) => \ramplitude_min_reg[21]_i_3_n_0\,
      S(2) => \ramplitude_min_reg[21]_i_4_n_0\,
      S(1) => \ramplitude_min_reg[21]_i_5_n_0\,
      S(0) => \ramplitude_min_reg_reg[31]_i_6_0\(0)
    );
\ramplitude_min_reg_reg[25]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[21]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[25]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[25]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[25]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[25]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \ramplitude_min_reg_reg[31]_i_6_0\(7 downto 4),
      O(3 downto 0) => ramp_module_0_reset(7 downto 4),
      S(3) => \ramplitude_min_reg[25]_i_3_n_0\,
      S(2) => \ramplitude_min_reg[25]_i_4_n_0\,
      S(1) => \ramplitude_min_reg[25]_i_5_n_0\,
      S(0) => \ramplitude_min_reg[25]_i_6_n_0\
    );
\ramplitude_min_reg_reg[29]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[25]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[29]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[29]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[29]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[29]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \ramplitude_min_reg_reg[31]_i_6_0\(11 downto 8),
      O(3 downto 0) => ramp_module_0_reset(11 downto 8),
      S(3) => \ramplitude_min_reg[29]_i_3_n_0\,
      S(2) => \ramplitude_min_reg[29]_i_4_n_0\,
      S(1) => \ramplitude_min_reg[29]_i_5_n_0\,
      S(0) => \ramplitude_min_reg[29]_i_6_n_0\
    );
\ramplitude_min_reg_reg[31]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[29]_i_2_n_0\,
      CO(3 downto 1) => \NLW_ramplitude_min_reg_reg[31]_i_6_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \ramplitude_min_reg_reg[31]_i_6_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \ramplitude_min_reg_reg[31]_i_6_0\(12),
      O(3 downto 2) => \NLW_ramplitude_min_reg_reg[31]_i_6_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => ramp_module_0_reset(13 downto 12),
      S(3 downto 2) => B"00",
      S(1) => \ramplitude_min_reg[31]_i_31_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_32_n_0\
    );
\setpoint_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(0),
      Q => setpoint_reg(0),
      R => '0'
    );
\setpoint_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(10),
      Q => setpoint_reg(10),
      R => '0'
    );
\setpoint_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(11),
      Q => setpoint_reg(11),
      R => '0'
    );
\setpoint_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(12),
      Q => setpoint_reg(12),
      R => '0'
    );
\setpoint_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(13),
      Q => setpoint_reg(13),
      R => '0'
    );
\setpoint_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(1),
      Q => setpoint_reg(1),
      R => '0'
    );
\setpoint_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(2),
      Q => setpoint_reg(2),
      R => '0'
    );
\setpoint_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(3),
      Q => setpoint_reg(3),
      R => '0'
    );
\setpoint_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(4),
      Q => setpoint_reg(4),
      R => '0'
    );
\setpoint_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(5),
      Q => setpoint_reg(5),
      R => '0'
    );
\setpoint_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(6),
      Q => setpoint_reg(6),
      R => '0'
    );
\setpoint_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(7),
      Q => setpoint_reg(7),
      R => '0'
    );
\setpoint_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(8),
      Q => setpoint_reg(8),
      R => '0'
    );
\setpoint_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \setpoint_reg_reg[13]_0\(9),
      Q => setpoint_reg(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_AXI_PI_0_2_ramp_module is
  port (
    ramp_output : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \ramp_reg_reg[31]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \ramp_reg_reg[30]_0\ : out STD_LOGIC_VECTOR ( 12 downto 0 );
    \ramp_reg_reg[31]_1\ : out STD_LOGIC;
    ramp_module_0_rst : out STD_LOGIC;
    \loop_locked_counter_reg_reg[30]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    ramp_corner_reg_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    ramp_corner_reg_reg_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \I_term_q_reg[31]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_pi_output_reg_reg[13]_inv\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \axi_pi_output_reg_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aresetn : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    loop_locked_counter_reg_reg : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \status_reg_reg[10]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \autolock_input_mindex_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \autolock_input_maxdex_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \ramp_step_reg_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    \ramplitude_lim_reg_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \ramplitude_step_reg_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ramp_module_0_reset : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \ramp_reg_reg[17]_0\ : in STD_LOGIC_VECTOR ( 17 downto 0 )
  );
end system_AXI_PI_0_2_ramp_module;

architecture STRUCTURE of system_AXI_PI_0_2_ramp_module is
  signal data0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal data2 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal loop_locked_INST_0_i_10_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_10_n_1 : STD_LOGIC;
  signal loop_locked_INST_0_i_10_n_2 : STD_LOGIC;
  signal loop_locked_INST_0_i_10_n_3 : STD_LOGIC;
  signal loop_locked_INST_0_i_11_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_12_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_13_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_14_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_15_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_16_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_17_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_18_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_19_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_19_n_1 : STD_LOGIC;
  signal loop_locked_INST_0_i_19_n_2 : STD_LOGIC;
  signal loop_locked_INST_0_i_19_n_3 : STD_LOGIC;
  signal loop_locked_INST_0_i_1_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_1_n_1 : STD_LOGIC;
  signal loop_locked_INST_0_i_1_n_2 : STD_LOGIC;
  signal loop_locked_INST_0_i_1_n_3 : STD_LOGIC;
  signal loop_locked_INST_0_i_20_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_21_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_22_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_23_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_24_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_25_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_26_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_27_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_28_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_29_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_2_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_30_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_31_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_32_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_33_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_34_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_35_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_3_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_4_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_5_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_6_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_7_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_8_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_i_9_n_0 : STD_LOGIC;
  signal loop_locked_INST_0_n_1 : STD_LOGIC;
  signal loop_locked_INST_0_n_2 : STD_LOGIC;
  signal loop_locked_INST_0_n_3 : STD_LOGIC;
  signal \^loop_locked_counter_reg_reg[30]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ramp_corner_reg_i_1_n_0 : STD_LOGIC;
  signal ramp_module_0_corner : STD_LOGIC;
  signal \^ramp_module_0_rst\ : STD_LOGIC;
  signal \^ramp_output\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal ramp_reg : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \ramp_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[10]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[11]_i_10_n_0\ : STD_LOGIC;
  signal \ramp_reg[11]_i_11_n_0\ : STD_LOGIC;
  signal \ramp_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[11]_i_4_n_0\ : STD_LOGIC;
  signal \ramp_reg[11]_i_5_n_0\ : STD_LOGIC;
  signal \ramp_reg[11]_i_6_n_0\ : STD_LOGIC;
  signal \ramp_reg[11]_i_7_n_0\ : STD_LOGIC;
  signal \ramp_reg[11]_i_8_n_0\ : STD_LOGIC;
  signal \ramp_reg[11]_i_9_n_0\ : STD_LOGIC;
  signal \ramp_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[13]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[14]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[15]_i_10_n_0\ : STD_LOGIC;
  signal \ramp_reg[15]_i_11_n_0\ : STD_LOGIC;
  signal \ramp_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[15]_i_4_n_0\ : STD_LOGIC;
  signal \ramp_reg[15]_i_5_n_0\ : STD_LOGIC;
  signal \ramp_reg[15]_i_6_n_0\ : STD_LOGIC;
  signal \ramp_reg[15]_i_7_n_0\ : STD_LOGIC;
  signal \ramp_reg[15]_i_8_n_0\ : STD_LOGIC;
  signal \ramp_reg[15]_i_9_n_0\ : STD_LOGIC;
  signal \ramp_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[17]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[18]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[19]_i_10_n_0\ : STD_LOGIC;
  signal \ramp_reg[19]_i_11_n_0\ : STD_LOGIC;
  signal \ramp_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[19]_i_4_n_0\ : STD_LOGIC;
  signal \ramp_reg[19]_i_5_n_0\ : STD_LOGIC;
  signal \ramp_reg[19]_i_6_n_0\ : STD_LOGIC;
  signal \ramp_reg[19]_i_7_n_0\ : STD_LOGIC;
  signal \ramp_reg[19]_i_8_n_0\ : STD_LOGIC;
  signal \ramp_reg[19]_i_9_n_0\ : STD_LOGIC;
  signal \ramp_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[21]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[22]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[23]_i_10_n_0\ : STD_LOGIC;
  signal \ramp_reg[23]_i_11_n_0\ : STD_LOGIC;
  signal \ramp_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[23]_i_4_n_0\ : STD_LOGIC;
  signal \ramp_reg[23]_i_5_n_0\ : STD_LOGIC;
  signal \ramp_reg[23]_i_6_n_0\ : STD_LOGIC;
  signal \ramp_reg[23]_i_7_n_0\ : STD_LOGIC;
  signal \ramp_reg[23]_i_8_n_0\ : STD_LOGIC;
  signal \ramp_reg[23]_i_9_n_0\ : STD_LOGIC;
  signal \ramp_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[25]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[26]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[27]_i_10_n_0\ : STD_LOGIC;
  signal \ramp_reg[27]_i_11_n_0\ : STD_LOGIC;
  signal \ramp_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[27]_i_4_n_0\ : STD_LOGIC;
  signal \ramp_reg[27]_i_5_n_0\ : STD_LOGIC;
  signal \ramp_reg[27]_i_6_n_0\ : STD_LOGIC;
  signal \ramp_reg[27]_i_7_n_0\ : STD_LOGIC;
  signal \ramp_reg[27]_i_8_n_0\ : STD_LOGIC;
  signal \ramp_reg[27]_i_9_n_0\ : STD_LOGIC;
  signal \ramp_reg[28]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[29]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[30]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[31]_i_10_n_0\ : STD_LOGIC;
  signal \ramp_reg[31]_i_11_n_0\ : STD_LOGIC;
  signal \ramp_reg[31]_i_12_n_0\ : STD_LOGIC;
  signal \ramp_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \ramp_reg[31]_i_5_n_0\ : STD_LOGIC;
  signal \ramp_reg[31]_i_6_n_0\ : STD_LOGIC;
  signal \ramp_reg[31]_i_7_n_0\ : STD_LOGIC;
  signal \ramp_reg[31]_i_8_n_0\ : STD_LOGIC;
  signal \ramp_reg[31]_i_9_n_0\ : STD_LOGIC;
  signal \ramp_reg[3]_i_10_n_0\ : STD_LOGIC;
  signal \ramp_reg[3]_i_11_n_0\ : STD_LOGIC;
  signal \ramp_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[3]_i_4_n_0\ : STD_LOGIC;
  signal \ramp_reg[3]_i_5_n_0\ : STD_LOGIC;
  signal \ramp_reg[3]_i_6_n_0\ : STD_LOGIC;
  signal \ramp_reg[3]_i_7_n_0\ : STD_LOGIC;
  signal \ramp_reg[3]_i_8_n_0\ : STD_LOGIC;
  signal \ramp_reg[3]_i_9_n_0\ : STD_LOGIC;
  signal \ramp_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[5]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[6]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[7]_i_10_n_0\ : STD_LOGIC;
  signal \ramp_reg[7]_i_11_n_0\ : STD_LOGIC;
  signal \ramp_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[7]_i_4_n_0\ : STD_LOGIC;
  signal \ramp_reg[7]_i_5_n_0\ : STD_LOGIC;
  signal \ramp_reg[7]_i_6_n_0\ : STD_LOGIC;
  signal \ramp_reg[7]_i_7_n_0\ : STD_LOGIC;
  signal \ramp_reg[7]_i_8_n_0\ : STD_LOGIC;
  signal \ramp_reg[7]_i_9_n_0\ : STD_LOGIC;
  signal \ramp_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg[9]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_2_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_2_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_2_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_2_n_4\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_2_n_5\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_2_n_6\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_2_n_7\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_3_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_3_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_3_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[11]_i_3_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_2_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_2_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_2_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_3_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_3_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_3_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_3_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_3_n_4\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_3_n_5\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_3_n_6\ : STD_LOGIC;
  signal \ramp_reg_reg[15]_i_3_n_7\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_2_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_2_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_2_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_2_n_4\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_2_n_5\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_2_n_6\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_2_n_7\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_3_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_3_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_3_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[19]_i_3_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_2_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_2_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_2_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_2_n_4\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_2_n_5\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_2_n_6\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_2_n_7\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_3_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_3_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_3_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[23]_i_3_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_2_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_2_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_2_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_2_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_2_n_4\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_2_n_5\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_2_n_6\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_2_n_7\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_3_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_3_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_3_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[27]_i_3_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[31]_i_3_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[31]_i_3_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[31]_i_3_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[31]_i_4_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[31]_i_4_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[31]_i_4_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[31]_i_4_n_4\ : STD_LOGIC;
  signal \ramp_reg_reg[31]_i_4_n_5\ : STD_LOGIC;
  signal \ramp_reg_reg[31]_i_4_n_6\ : STD_LOGIC;
  signal \ramp_reg_reg[31]_i_4_n_7\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_2_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_2_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_2_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_2_n_4\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_2_n_5\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_2_n_6\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_2_n_7\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_3_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_3_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_3_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[3]_i_3_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_2_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_2_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_2_n_3\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_2_n_4\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_2_n_5\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_2_n_6\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_2_n_7\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_3_n_0\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_3_n_1\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_3_n_2\ : STD_LOGIC;
  signal \ramp_reg_reg[7]_i_3_n_3\ : STD_LOGIC;
  signal ramp_step_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ramp_up_i_1_n_0 : STD_LOGIC;
  signal ramp_up_reg_n_0 : STD_LOGIC;
  signal ramplitude_lim_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ramplitude_max_reg0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ramplitude_max_reg1 : STD_LOGIC;
  signal \ramplitude_max_reg[11]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[11]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[11]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[11]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[15]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[15]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[15]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[15]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[19]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[19]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[19]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[19]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[23]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[23]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[23]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[23]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[27]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[27]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[27]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[27]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_10_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_11_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_12_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_13_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_14_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_15_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_16_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_17_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_19_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_20_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_21_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_22_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_23_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_24_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_25_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_26_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_28_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_29_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_30_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_31_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_32_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_33_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_34_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_35_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_36_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_37_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_38_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_39_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_40_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_41_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_42_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_43_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_7_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_8_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[31]_i_9_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[3]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[3]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[3]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[3]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[7]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[7]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[7]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg[7]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[11]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[11]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[11]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[15]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[15]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[15]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[19]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[19]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[19]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[23]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[23]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[23]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[27]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[27]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[27]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[27]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_18_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_18_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_18_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_18_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_27_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_27_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_27_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_27_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_3_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_3_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_3_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_4_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_4_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_4_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_5_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_5_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[31]_i_5_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[3]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[3]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[3]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[7]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[7]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg[7]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[0]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[10]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[11]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[12]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[13]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[14]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[15]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[16]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[17]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[18]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[19]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[1]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[20]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[21]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[22]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[23]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[24]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[25]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[26]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[27]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[28]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[29]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[2]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[30]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[31]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[3]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[4]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[5]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[6]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[7]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[8]\ : STD_LOGIC;
  signal \ramplitude_max_reg_reg_n_0_[9]\ : STD_LOGIC;
  signal ramplitude_min_reg0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ramplitude_min_reg1 : STD_LOGIC;
  signal \ramplitude_min_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[10]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[11]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[11]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[11]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[11]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[13]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[14]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[15]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[15]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[15]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[15]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[17]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[18]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[19]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[19]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[19]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[19]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[21]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[22]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[23]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[23]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[23]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[23]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[25]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[26]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[27]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[27]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[27]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[27]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[28]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[29]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[30]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_100_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_101_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_102_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_103_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_104_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_105_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_106_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_107_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_108_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_109_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_110_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_111_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_112_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_113_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_114_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_115_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_116_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_117_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_118_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_119_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_11_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_120_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_121_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_122_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_123_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_124_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_125_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_126_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_127_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_128_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_129_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_12_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_130_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_131_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_13_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_14_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_15_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_16_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_17_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_18_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_20_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_21_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_22_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_23_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_24_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_25_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_26_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_27_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_28_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_34_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_35_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_36_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_37_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_38_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_39_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_40_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_41_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_42_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_43_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_44_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_45_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_46_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_47_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_48_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_49_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_51_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_52_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_53_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_54_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_55_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_56_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_57_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_58_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_60_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_61_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_62_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_63_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_64_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_65_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_66_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_67_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_69_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_70_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_71_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_72_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_73_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_74_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_75_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_76_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_77_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_78_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_79_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_80_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_82_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_83_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_84_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_85_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_86_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_87_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_88_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_89_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_91_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_92_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_93_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_94_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_95_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_96_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_97_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_98_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[31]_i_9_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[3]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[3]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[3]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[3]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[5]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[6]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[7]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[7]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[7]_i_5_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[7]_i_6_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg[9]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[11]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[11]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[11]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[12]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[12]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[12]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[15]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[15]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[15]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[16]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[16]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[16]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[19]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[19]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[19]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[20]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[20]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[20]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[23]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[23]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[23]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[24]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[24]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[24]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[27]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[27]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[27]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[27]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[28]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[28]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[28]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[28]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_10_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_10_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_10_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_10_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_19_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_19_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_19_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_19_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_33_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_33_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_33_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_33_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_3_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_3_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_3_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_3_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_4_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_4_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_4_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_4_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_50_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_50_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_50_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_50_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_59_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_59_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_59_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_59_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_5_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_5_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_68_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_68_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_68_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_68_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_7_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_7_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_7_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_81_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_81_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_81_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_81_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_8_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_8_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_8_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_90_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_90_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_90_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_90_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_99_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_99_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_99_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[31]_i_99_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[3]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[3]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[3]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[4]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[4]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[4]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[7]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[7]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[7]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[8]_i_2_n_1\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[8]_i_2_n_2\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg[8]_i_2_n_3\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[0]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[10]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[11]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[12]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[13]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[14]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[15]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[16]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[17]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[18]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[19]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[1]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[20]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[21]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[22]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[23]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[24]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[25]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[26]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[27]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[28]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[29]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[2]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[30]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[31]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[3]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[4]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[5]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[6]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[7]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[8]\ : STD_LOGIC;
  signal \ramplitude_min_reg_reg_n_0_[9]\ : STD_LOGIC;
  signal ramplitude_step_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_loop_locked_INST_0_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_loop_locked_INST_0_i_1_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_loop_locked_INST_0_i_10_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_loop_locked_INST_0_i_19_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramp_reg_reg[31]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ramp_reg_reg[31]_i_4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ramplitude_max_reg_reg[31]_i_18_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_max_reg_reg[31]_i_27_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_max_reg_reg[31]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_max_reg_reg[31]_i_4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ramplitude_max_reg_reg[31]_i_5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_10_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_19_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_33_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_50_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_59_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_68_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_7_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_8_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_81_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_90_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ramplitude_min_reg_reg[31]_i_99_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \autolock_input_max[13]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \mindex_reg[31]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of ramp_corner_reg_i_1 : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of ramp_up_i_1 : label is "soft_lutpair16";
begin
  \loop_locked_counter_reg_reg[30]\(0) <= \^loop_locked_counter_reg_reg[30]\(0);
  ramp_module_0_rst <= \^ramp_module_0_rst\;
  ramp_output(13 downto 0) <= \^ramp_output\(13 downto 0);
\I_term_q[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8B"
    )
        port map (
      I0 => \^ramp_output\(13),
      I1 => D(1),
      I2 => \I_term_q_reg[31]\(0),
      O => \ramp_reg_reg[31]_0\(0)
    );
\autolock_input_max[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^loop_locked_counter_reg_reg[30]\(0),
      I1 => \autolock_input_maxdex_reg[0]\(0),
      I2 => ramp_module_0_corner,
      O => ramp_corner_reg_reg_1(0)
    );
\autolock_input_min[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^loop_locked_counter_reg_reg[30]\(0),
      I1 => \autolock_input_mindex_reg[0]\(0),
      I2 => ramp_module_0_corner,
      O => ramp_corner_reg_reg_0(0)
    );
\axi_pi_output_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(0),
      I1 => \axi_pi_output_reg_reg[13]_inv\(0),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(0)
    );
\axi_pi_output_reg[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(10),
      I1 => \axi_pi_output_reg_reg[13]_inv\(10),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(10)
    );
\axi_pi_output_reg[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(11),
      I1 => \axi_pi_output_reg_reg[13]_inv\(11),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(11)
    );
\axi_pi_output_reg[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(12),
      I1 => \axi_pi_output_reg_reg[13]_inv\(12),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(12)
    );
\axi_pi_output_reg[13]_inv_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5355555553535353"
    )
        port map (
      I0 => \^ramp_output\(13),
      I1 => \axi_pi_output_reg_reg[13]_inv\(13),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[31]_1\
    );
\axi_pi_output_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(1),
      I1 => \axi_pi_output_reg_reg[13]_inv\(1),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(1)
    );
\axi_pi_output_reg[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(2),
      I1 => \axi_pi_output_reg_reg[13]_inv\(2),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(2)
    );
\axi_pi_output_reg[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(3),
      I1 => \axi_pi_output_reg_reg[13]_inv\(3),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(3)
    );
\axi_pi_output_reg[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(4),
      I1 => \axi_pi_output_reg_reg[13]_inv\(4),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(4)
    );
\axi_pi_output_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(5),
      I1 => \axi_pi_output_reg_reg[13]_inv\(5),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(5)
    );
\axi_pi_output_reg[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(6),
      I1 => \axi_pi_output_reg_reg[13]_inv\(6),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(6)
    );
\axi_pi_output_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(7),
      I1 => \axi_pi_output_reg_reg[13]_inv\(7),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(7)
    );
\axi_pi_output_reg[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(8),
      I1 => \axi_pi_output_reg_reg[13]_inv\(8),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(8)
    );
\axi_pi_output_reg[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACAAAAAAACACACAC"
    )
        port map (
      I0 => \^ramp_output\(9),
      I1 => \axi_pi_output_reg_reg[13]_inv\(9),
      I2 => D(2),
      I3 => \axi_pi_output_reg_reg[0]\(0),
      I4 => CO(0),
      I5 => D(3),
      O => \ramp_reg_reg[30]_0\(9)
    );
loop_locked_INST_0: unisim.vcomponents.CARRY4
     port map (
      CI => loop_locked_INST_0_i_1_n_0,
      CO(3) => \^loop_locked_counter_reg_reg[30]\(0),
      CO(2) => loop_locked_INST_0_n_1,
      CO(1) => loop_locked_INST_0_n_2,
      CO(0) => loop_locked_INST_0_n_3,
      CYINIT => '0',
      DI(3) => loop_locked_INST_0_i_2_n_0,
      DI(2) => loop_locked_INST_0_i_3_n_0,
      DI(1) => loop_locked_INST_0_i_4_n_0,
      DI(0) => loop_locked_INST_0_i_5_n_0,
      O(3 downto 0) => NLW_loop_locked_INST_0_O_UNCONNECTED(3 downto 0),
      S(3) => loop_locked_INST_0_i_6_n_0,
      S(2) => loop_locked_INST_0_i_7_n_0,
      S(1) => loop_locked_INST_0_i_8_n_0,
      S(0) => loop_locked_INST_0_i_9_n_0
    );
loop_locked_INST_0_i_1: unisim.vcomponents.CARRY4
     port map (
      CI => loop_locked_INST_0_i_10_n_0,
      CO(3) => loop_locked_INST_0_i_1_n_0,
      CO(2) => loop_locked_INST_0_i_1_n_1,
      CO(1) => loop_locked_INST_0_i_1_n_2,
      CO(0) => loop_locked_INST_0_i_1_n_3,
      CYINIT => '0',
      DI(3) => loop_locked_INST_0_i_11_n_0,
      DI(2) => loop_locked_INST_0_i_12_n_0,
      DI(1) => loop_locked_INST_0_i_13_n_0,
      DI(0) => loop_locked_INST_0_i_14_n_0,
      O(3 downto 0) => NLW_loop_locked_INST_0_i_1_O_UNCONNECTED(3 downto 0),
      S(3) => loop_locked_INST_0_i_15_n_0,
      S(2) => loop_locked_INST_0_i_16_n_0,
      S(1) => loop_locked_INST_0_i_17_n_0,
      S(0) => loop_locked_INST_0_i_18_n_0
    );
loop_locked_INST_0_i_10: unisim.vcomponents.CARRY4
     port map (
      CI => loop_locked_INST_0_i_19_n_0,
      CO(3) => loop_locked_INST_0_i_10_n_0,
      CO(2) => loop_locked_INST_0_i_10_n_1,
      CO(1) => loop_locked_INST_0_i_10_n_2,
      CO(0) => loop_locked_INST_0_i_10_n_3,
      CYINIT => '0',
      DI(3) => loop_locked_INST_0_i_20_n_0,
      DI(2) => loop_locked_INST_0_i_21_n_0,
      DI(1) => loop_locked_INST_0_i_22_n_0,
      DI(0) => loop_locked_INST_0_i_23_n_0,
      O(3 downto 0) => NLW_loop_locked_INST_0_i_10_O_UNCONNECTED(3 downto 0),
      S(3) => loop_locked_INST_0_i_24_n_0,
      S(2) => loop_locked_INST_0_i_25_n_0,
      S(1) => loop_locked_INST_0_i_26_n_0,
      S(0) => loop_locked_INST_0_i_27_n_0
    );
loop_locked_INST_0_i_11: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(22),
      I1 => \status_reg_reg[10]\(22),
      I2 => \status_reg_reg[10]\(23),
      I3 => loop_locked_counter_reg_reg(23),
      O => loop_locked_INST_0_i_11_n_0
    );
loop_locked_INST_0_i_12: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(20),
      I1 => \status_reg_reg[10]\(20),
      I2 => \status_reg_reg[10]\(21),
      I3 => loop_locked_counter_reg_reg(21),
      O => loop_locked_INST_0_i_12_n_0
    );
loop_locked_INST_0_i_13: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(18),
      I1 => \status_reg_reg[10]\(18),
      I2 => \status_reg_reg[10]\(19),
      I3 => loop_locked_counter_reg_reg(19),
      O => loop_locked_INST_0_i_13_n_0
    );
loop_locked_INST_0_i_14: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(16),
      I1 => \status_reg_reg[10]\(16),
      I2 => \status_reg_reg[10]\(17),
      I3 => loop_locked_counter_reg_reg(17),
      O => loop_locked_INST_0_i_14_n_0
    );
loop_locked_INST_0_i_15: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(22),
      I1 => \status_reg_reg[10]\(22),
      I2 => loop_locked_counter_reg_reg(23),
      I3 => \status_reg_reg[10]\(23),
      O => loop_locked_INST_0_i_15_n_0
    );
loop_locked_INST_0_i_16: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(20),
      I1 => \status_reg_reg[10]\(20),
      I2 => loop_locked_counter_reg_reg(21),
      I3 => \status_reg_reg[10]\(21),
      O => loop_locked_INST_0_i_16_n_0
    );
loop_locked_INST_0_i_17: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(18),
      I1 => \status_reg_reg[10]\(18),
      I2 => loop_locked_counter_reg_reg(19),
      I3 => \status_reg_reg[10]\(19),
      O => loop_locked_INST_0_i_17_n_0
    );
loop_locked_INST_0_i_18: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(16),
      I1 => \status_reg_reg[10]\(16),
      I2 => loop_locked_counter_reg_reg(17),
      I3 => \status_reg_reg[10]\(17),
      O => loop_locked_INST_0_i_18_n_0
    );
loop_locked_INST_0_i_19: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => loop_locked_INST_0_i_19_n_0,
      CO(2) => loop_locked_INST_0_i_19_n_1,
      CO(1) => loop_locked_INST_0_i_19_n_2,
      CO(0) => loop_locked_INST_0_i_19_n_3,
      CYINIT => '0',
      DI(3) => loop_locked_INST_0_i_28_n_0,
      DI(2) => loop_locked_INST_0_i_29_n_0,
      DI(1) => loop_locked_INST_0_i_30_n_0,
      DI(0) => loop_locked_INST_0_i_31_n_0,
      O(3 downto 0) => NLW_loop_locked_INST_0_i_19_O_UNCONNECTED(3 downto 0),
      S(3) => loop_locked_INST_0_i_32_n_0,
      S(2) => loop_locked_INST_0_i_33_n_0,
      S(1) => loop_locked_INST_0_i_34_n_0,
      S(0) => loop_locked_INST_0_i_35_n_0
    );
loop_locked_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(30),
      I1 => \status_reg_reg[10]\(30),
      I2 => \status_reg_reg[10]\(31),
      I3 => loop_locked_counter_reg_reg(31),
      O => loop_locked_INST_0_i_2_n_0
    );
loop_locked_INST_0_i_20: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(14),
      I1 => \status_reg_reg[10]\(14),
      I2 => \status_reg_reg[10]\(15),
      I3 => loop_locked_counter_reg_reg(15),
      O => loop_locked_INST_0_i_20_n_0
    );
loop_locked_INST_0_i_21: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(12),
      I1 => \status_reg_reg[10]\(12),
      I2 => \status_reg_reg[10]\(13),
      I3 => loop_locked_counter_reg_reg(13),
      O => loop_locked_INST_0_i_21_n_0
    );
loop_locked_INST_0_i_22: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(10),
      I1 => \status_reg_reg[10]\(10),
      I2 => \status_reg_reg[10]\(11),
      I3 => loop_locked_counter_reg_reg(11),
      O => loop_locked_INST_0_i_22_n_0
    );
loop_locked_INST_0_i_23: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(8),
      I1 => \status_reg_reg[10]\(8),
      I2 => \status_reg_reg[10]\(9),
      I3 => loop_locked_counter_reg_reg(9),
      O => loop_locked_INST_0_i_23_n_0
    );
loop_locked_INST_0_i_24: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(14),
      I1 => \status_reg_reg[10]\(14),
      I2 => loop_locked_counter_reg_reg(15),
      I3 => \status_reg_reg[10]\(15),
      O => loop_locked_INST_0_i_24_n_0
    );
loop_locked_INST_0_i_25: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(12),
      I1 => \status_reg_reg[10]\(12),
      I2 => loop_locked_counter_reg_reg(13),
      I3 => \status_reg_reg[10]\(13),
      O => loop_locked_INST_0_i_25_n_0
    );
loop_locked_INST_0_i_26: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(10),
      I1 => \status_reg_reg[10]\(10),
      I2 => loop_locked_counter_reg_reg(11),
      I3 => \status_reg_reg[10]\(11),
      O => loop_locked_INST_0_i_26_n_0
    );
loop_locked_INST_0_i_27: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(8),
      I1 => \status_reg_reg[10]\(8),
      I2 => loop_locked_counter_reg_reg(9),
      I3 => \status_reg_reg[10]\(9),
      O => loop_locked_INST_0_i_27_n_0
    );
loop_locked_INST_0_i_28: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(6),
      I1 => \status_reg_reg[10]\(6),
      I2 => \status_reg_reg[10]\(7),
      I3 => loop_locked_counter_reg_reg(7),
      O => loop_locked_INST_0_i_28_n_0
    );
loop_locked_INST_0_i_29: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(4),
      I1 => \status_reg_reg[10]\(4),
      I2 => \status_reg_reg[10]\(5),
      I3 => loop_locked_counter_reg_reg(5),
      O => loop_locked_INST_0_i_29_n_0
    );
loop_locked_INST_0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(28),
      I1 => \status_reg_reg[10]\(28),
      I2 => \status_reg_reg[10]\(29),
      I3 => loop_locked_counter_reg_reg(29),
      O => loop_locked_INST_0_i_3_n_0
    );
loop_locked_INST_0_i_30: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(2),
      I1 => \status_reg_reg[10]\(2),
      I2 => \status_reg_reg[10]\(3),
      I3 => loop_locked_counter_reg_reg(3),
      O => loop_locked_INST_0_i_30_n_0
    );
loop_locked_INST_0_i_31: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(0),
      I1 => \status_reg_reg[10]\(0),
      I2 => \status_reg_reg[10]\(1),
      I3 => loop_locked_counter_reg_reg(1),
      O => loop_locked_INST_0_i_31_n_0
    );
loop_locked_INST_0_i_32: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(6),
      I1 => \status_reg_reg[10]\(6),
      I2 => loop_locked_counter_reg_reg(7),
      I3 => \status_reg_reg[10]\(7),
      O => loop_locked_INST_0_i_32_n_0
    );
loop_locked_INST_0_i_33: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(4),
      I1 => \status_reg_reg[10]\(4),
      I2 => loop_locked_counter_reg_reg(5),
      I3 => \status_reg_reg[10]\(5),
      O => loop_locked_INST_0_i_33_n_0
    );
loop_locked_INST_0_i_34: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(2),
      I1 => \status_reg_reg[10]\(2),
      I2 => loop_locked_counter_reg_reg(3),
      I3 => \status_reg_reg[10]\(3),
      O => loop_locked_INST_0_i_34_n_0
    );
loop_locked_INST_0_i_35: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(0),
      I1 => \status_reg_reg[10]\(0),
      I2 => loop_locked_counter_reg_reg(1),
      I3 => \status_reg_reg[10]\(1),
      O => loop_locked_INST_0_i_35_n_0
    );
loop_locked_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(26),
      I1 => \status_reg_reg[10]\(26),
      I2 => \status_reg_reg[10]\(27),
      I3 => loop_locked_counter_reg_reg(27),
      O => loop_locked_INST_0_i_4_n_0
    );
loop_locked_INST_0_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(24),
      I1 => \status_reg_reg[10]\(24),
      I2 => \status_reg_reg[10]\(25),
      I3 => loop_locked_counter_reg_reg(25),
      O => loop_locked_INST_0_i_5_n_0
    );
loop_locked_INST_0_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(30),
      I1 => \status_reg_reg[10]\(30),
      I2 => loop_locked_counter_reg_reg(31),
      I3 => \status_reg_reg[10]\(31),
      O => loop_locked_INST_0_i_6_n_0
    );
loop_locked_INST_0_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(28),
      I1 => \status_reg_reg[10]\(28),
      I2 => loop_locked_counter_reg_reg(29),
      I3 => \status_reg_reg[10]\(29),
      O => loop_locked_INST_0_i_7_n_0
    );
loop_locked_INST_0_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(26),
      I1 => \status_reg_reg[10]\(26),
      I2 => loop_locked_counter_reg_reg(27),
      I3 => \status_reg_reg[10]\(27),
      O => loop_locked_INST_0_i_8_n_0
    );
loop_locked_INST_0_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(24),
      I1 => \status_reg_reg[10]\(24),
      I2 => loop_locked_counter_reg_reg(25),
      I3 => \status_reg_reg[10]\(25),
      O => loop_locked_INST_0_i_9_n_0
    );
\mindex_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^loop_locked_counter_reg_reg[30]\(0),
      I1 => ramp_module_0_corner,
      O => E(0)
    );
ramp_corner_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0E"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I1 => \ramplitude_min_reg_reg[31]_i_4_n_0\,
      I2 => \^ramp_module_0_rst\,
      O => ramp_corner_reg_i_1_n_0
    );
ramp_corner_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_corner_reg_i_1_n_0,
      Q => ramp_module_0_corner,
      R => '0'
    );
\ramp_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFF000F0EEF044"
    )
        port map (
      I0 => \ramp_reg[31]_i_2_n_0\,
      I1 => data2(0),
      I2 => \ramp_reg_reg[17]_0\(0),
      I3 => \^ramp_module_0_rst\,
      I4 => \ramp_reg_reg[3]_i_2_n_7\,
      I5 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      O => \ramp_reg[0]_i_1_n_0\
    );
\ramp_reg[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(10),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[11]_i_2_n_5\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(10),
      O => \ramp_reg[10]_i_1_n_0\
    );
\ramp_reg[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(11),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[11]_i_2_n_4\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(11),
      O => \ramp_reg[11]_i_1_n_0\
    );
\ramp_reg[11]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(9),
      I1 => ramp_reg(9),
      O => \ramp_reg[11]_i_10_n_0\
    );
\ramp_reg[11]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(8),
      I1 => ramp_reg(8),
      O => \ramp_reg[11]_i_11_n_0\
    );
\ramp_reg[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(11),
      I1 => ramp_step_reg(11),
      O => \ramp_reg[11]_i_4_n_0\
    );
\ramp_reg[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(10),
      I1 => ramp_step_reg(10),
      O => \ramp_reg[11]_i_5_n_0\
    );
\ramp_reg[11]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(9),
      I1 => ramp_step_reg(9),
      O => \ramp_reg[11]_i_6_n_0\
    );
\ramp_reg[11]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(8),
      I1 => ramp_step_reg(8),
      O => \ramp_reg[11]_i_7_n_0\
    );
\ramp_reg[11]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(11),
      I1 => ramp_reg(11),
      O => \ramp_reg[11]_i_8_n_0\
    );
\ramp_reg[11]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(10),
      I1 => ramp_reg(10),
      O => \ramp_reg[11]_i_9_n_0\
    );
\ramp_reg[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(12),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[15]_i_3_n_7\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(12),
      O => \ramp_reg[12]_i_1_n_0\
    );
\ramp_reg[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(13),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(13),
      I5 => \ramp_reg_reg[15]_i_3_n_6\,
      O => \ramp_reg[13]_i_1_n_0\
    );
\ramp_reg[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(14),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(14),
      I5 => \ramp_reg_reg[15]_i_3_n_5\,
      O => \ramp_reg[14]_i_1_n_0\
    );
\ramp_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(15),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(15),
      I5 => \ramp_reg_reg[15]_i_3_n_4\,
      O => \ramp_reg[15]_i_1_n_0\
    );
\ramp_reg[15]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(13),
      I1 => ramp_step_reg(13),
      O => \ramp_reg[15]_i_10_n_0\
    );
\ramp_reg[15]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(12),
      I1 => ramp_step_reg(12),
      O => \ramp_reg[15]_i_11_n_0\
    );
\ramp_reg[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(15),
      I1 => ramp_reg(15),
      O => \ramp_reg[15]_i_4_n_0\
    );
\ramp_reg[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(14),
      I1 => ramp_reg(14),
      O => \ramp_reg[15]_i_5_n_0\
    );
\ramp_reg[15]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(13),
      I1 => ramp_reg(13),
      O => \ramp_reg[15]_i_6_n_0\
    );
\ramp_reg[15]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(12),
      I1 => ramp_reg(12),
      O => \ramp_reg[15]_i_7_n_0\
    );
\ramp_reg[15]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(15),
      I1 => ramp_step_reg(15),
      O => \ramp_reg[15]_i_8_n_0\
    );
\ramp_reg[15]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(14),
      I1 => ramp_step_reg(14),
      O => \ramp_reg[15]_i_9_n_0\
    );
\ramp_reg[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(16),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[19]_i_2_n_7\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(16),
      O => \ramp_reg[16]_i_1_n_0\
    );
\ramp_reg[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(17),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[19]_i_2_n_6\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(17),
      O => \ramp_reg[17]_i_1_n_0\
    );
\ramp_reg[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => ramp_module_0_reset(0),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[19]_i_2_n_5\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(18),
      O => \ramp_reg[18]_i_1_n_0\
    );
\ramp_reg[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => ramp_module_0_reset(1),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[19]_i_2_n_4\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(19),
      O => \ramp_reg[19]_i_1_n_0\
    );
\ramp_reg[19]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(17),
      I1 => ramp_reg(17),
      O => \ramp_reg[19]_i_10_n_0\
    );
\ramp_reg[19]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(16),
      I1 => ramp_reg(16),
      O => \ramp_reg[19]_i_11_n_0\
    );
\ramp_reg[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^ramp_output\(1),
      I1 => ramp_step_reg(19),
      O => \ramp_reg[19]_i_4_n_0\
    );
\ramp_reg[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^ramp_output\(0),
      I1 => ramp_step_reg(18),
      O => \ramp_reg[19]_i_5_n_0\
    );
\ramp_reg[19]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(17),
      I1 => ramp_step_reg(17),
      O => \ramp_reg[19]_i_6_n_0\
    );
\ramp_reg[19]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(16),
      I1 => ramp_step_reg(16),
      O => \ramp_reg[19]_i_7_n_0\
    );
\ramp_reg[19]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(19),
      I1 => \^ramp_output\(1),
      O => \ramp_reg[19]_i_8_n_0\
    );
\ramp_reg[19]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(18),
      I1 => \^ramp_output\(0),
      O => \ramp_reg[19]_i_9_n_0\
    );
\ramp_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF0E40000F0E4"
    )
        port map (
      I0 => \ramp_reg[31]_i_2_n_0\,
      I1 => data2(1),
      I2 => \ramp_reg_reg[3]_i_2_n_6\,
      I3 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I4 => \^ramp_module_0_rst\,
      I5 => \ramp_reg_reg[17]_0\(1),
      O => \ramp_reg[1]_i_1_n_0\
    );
\ramp_reg[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => ramp_module_0_reset(2),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[23]_i_2_n_7\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(20),
      O => \ramp_reg[20]_i_1_n_0\
    );
\ramp_reg[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => ramp_module_0_reset(3),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(21),
      I5 => \ramp_reg_reg[23]_i_2_n_6\,
      O => \ramp_reg[21]_i_1_n_0\
    );
\ramp_reg[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => ramp_module_0_reset(4),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[23]_i_2_n_5\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(22),
      O => \ramp_reg[22]_i_1_n_0\
    );
\ramp_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => ramp_module_0_reset(5),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[23]_i_2_n_4\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(23),
      O => \ramp_reg[23]_i_1_n_0\
    );
\ramp_reg[23]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(21),
      I1 => \^ramp_output\(3),
      O => \ramp_reg[23]_i_10_n_0\
    );
\ramp_reg[23]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(20),
      I1 => \^ramp_output\(2),
      O => \ramp_reg[23]_i_11_n_0\
    );
\ramp_reg[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^ramp_output\(5),
      I1 => ramp_step_reg(23),
      O => \ramp_reg[23]_i_4_n_0\
    );
\ramp_reg[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^ramp_output\(4),
      I1 => ramp_step_reg(22),
      O => \ramp_reg[23]_i_5_n_0\
    );
\ramp_reg[23]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^ramp_output\(3),
      I1 => ramp_step_reg(21),
      O => \ramp_reg[23]_i_6_n_0\
    );
\ramp_reg[23]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^ramp_output\(2),
      I1 => ramp_step_reg(20),
      O => \ramp_reg[23]_i_7_n_0\
    );
\ramp_reg[23]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(23),
      I1 => \^ramp_output\(5),
      O => \ramp_reg[23]_i_8_n_0\
    );
\ramp_reg[23]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(22),
      I1 => \^ramp_output\(4),
      O => \ramp_reg[23]_i_9_n_0\
    );
\ramp_reg[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => ramp_module_0_reset(6),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[27]_i_2_n_7\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(24),
      O => \ramp_reg[24]_i_1_n_0\
    );
\ramp_reg[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => ramp_module_0_reset(7),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(25),
      I5 => \ramp_reg_reg[27]_i_2_n_6\,
      O => \ramp_reg[25]_i_1_n_0\
    );
\ramp_reg[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => ramp_module_0_reset(8),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[27]_i_2_n_5\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(26),
      O => \ramp_reg[26]_i_1_n_0\
    );
\ramp_reg[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => ramp_module_0_reset(9),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[27]_i_2_n_4\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(27),
      O => \ramp_reg[27]_i_1_n_0\
    );
\ramp_reg[27]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(25),
      I1 => \^ramp_output\(7),
      O => \ramp_reg[27]_i_10_n_0\
    );
\ramp_reg[27]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(24),
      I1 => \^ramp_output\(6),
      O => \ramp_reg[27]_i_11_n_0\
    );
\ramp_reg[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^ramp_output\(9),
      I1 => ramp_step_reg(27),
      O => \ramp_reg[27]_i_4_n_0\
    );
\ramp_reg[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^ramp_output\(8),
      I1 => ramp_step_reg(26),
      O => \ramp_reg[27]_i_5_n_0\
    );
\ramp_reg[27]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^ramp_output\(7),
      I1 => ramp_step_reg(25),
      O => \ramp_reg[27]_i_6_n_0\
    );
\ramp_reg[27]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^ramp_output\(6),
      I1 => ramp_step_reg(24),
      O => \ramp_reg[27]_i_7_n_0\
    );
\ramp_reg[27]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(27),
      I1 => \^ramp_output\(9),
      O => \ramp_reg[27]_i_8_n_0\
    );
\ramp_reg[27]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(26),
      I1 => \^ramp_output\(8),
      O => \ramp_reg[27]_i_9_n_0\
    );
\ramp_reg[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => ramp_module_0_reset(10),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(28),
      I5 => \ramp_reg_reg[31]_i_4_n_7\,
      O => \ramp_reg[28]_i_1_n_0\
    );
\ramp_reg[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => ramp_module_0_reset(11),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(29),
      I5 => \ramp_reg_reg[31]_i_4_n_6\,
      O => \ramp_reg[29]_i_1_n_0\
    );
\ramp_reg[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(2),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(2),
      I5 => \ramp_reg_reg[3]_i_2_n_5\,
      O => \ramp_reg[2]_i_1_n_0\
    );
\ramp_reg[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => ramp_module_0_reset(12),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(30),
      I5 => \ramp_reg_reg[31]_i_4_n_5\,
      O => \ramp_reg[30]_i_1_n_0\
    );
\ramp_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => ramp_module_0_reset(13),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(31),
      I5 => \ramp_reg_reg[31]_i_4_n_4\,
      O => \ramp_reg[31]_i_1_n_0\
    );
\ramp_reg[31]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_step_reg(30),
      I1 => \^ramp_output\(12),
      O => \ramp_reg[31]_i_10_n_0\
    );
\ramp_reg[31]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_step_reg(29),
      I1 => \^ramp_output\(11),
      O => \ramp_reg[31]_i_11_n_0\
    );
\ramp_reg[31]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_step_reg(28),
      I1 => \^ramp_output\(10),
      O => \ramp_reg[31]_i_12_n_0\
    );
\ramp_reg[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_4_n_0\,
      I1 => ramp_up_reg_n_0,
      O => \ramp_reg[31]_i_2_n_0\
    );
\ramp_reg[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^ramp_output\(13),
      I1 => ramp_step_reg(31),
      O => \ramp_reg[31]_i_5_n_0\
    );
\ramp_reg[31]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^ramp_output\(12),
      I1 => ramp_step_reg(30),
      O => \ramp_reg[31]_i_6_n_0\
    );
\ramp_reg[31]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^ramp_output\(11),
      I1 => ramp_step_reg(29),
      O => \ramp_reg[31]_i_7_n_0\
    );
\ramp_reg[31]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^ramp_output\(10),
      I1 => ramp_step_reg(28),
      O => \ramp_reg[31]_i_8_n_0\
    );
\ramp_reg[31]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_step_reg(31),
      I1 => \^ramp_output\(13),
      O => \ramp_reg[31]_i_9_n_0\
    );
\ramp_reg[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(3),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[3]_i_2_n_4\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(3),
      O => \ramp_reg[3]_i_1_n_0\
    );
\ramp_reg[3]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(1),
      I1 => ramp_reg(1),
      O => \ramp_reg[3]_i_10_n_0\
    );
\ramp_reg[3]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(0),
      I1 => ramp_reg(0),
      O => \ramp_reg[3]_i_11_n_0\
    );
\ramp_reg[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(3),
      I1 => ramp_step_reg(3),
      O => \ramp_reg[3]_i_4_n_0\
    );
\ramp_reg[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(2),
      I1 => ramp_step_reg(2),
      O => \ramp_reg[3]_i_5_n_0\
    );
\ramp_reg[3]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(1),
      I1 => ramp_step_reg(1),
      O => \ramp_reg[3]_i_6_n_0\
    );
\ramp_reg[3]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(0),
      I1 => ramp_step_reg(0),
      O => \ramp_reg[3]_i_7_n_0\
    );
\ramp_reg[3]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(3),
      I1 => ramp_reg(3),
      O => \ramp_reg[3]_i_8_n_0\
    );
\ramp_reg[3]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(2),
      I1 => ramp_reg(2),
      O => \ramp_reg[3]_i_9_n_0\
    );
\ramp_reg[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(4),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[7]_i_2_n_7\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(4),
      O => \ramp_reg[4]_i_1_n_0\
    );
\ramp_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(5),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(5),
      I5 => \ramp_reg_reg[7]_i_2_n_6\,
      O => \ramp_reg[5]_i_1_n_0\
    );
\ramp_reg[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(6),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[7]_i_2_n_5\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(6),
      O => \ramp_reg[6]_i_1_n_0\
    );
\ramp_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(7),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[7]_i_2_n_4\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(7),
      O => \ramp_reg[7]_i_1_n_0\
    );
\ramp_reg[7]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(5),
      I1 => ramp_reg(5),
      O => \ramp_reg[7]_i_10_n_0\
    );
\ramp_reg[7]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(4),
      I1 => ramp_reg(4),
      O => \ramp_reg[7]_i_11_n_0\
    );
\ramp_reg[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(7),
      I1 => ramp_step_reg(7),
      O => \ramp_reg[7]_i_4_n_0\
    );
\ramp_reg[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(6),
      I1 => ramp_step_reg(6),
      O => \ramp_reg[7]_i_5_n_0\
    );
\ramp_reg[7]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(5),
      I1 => ramp_step_reg(5),
      O => \ramp_reg[7]_i_6_n_0\
    );
\ramp_reg[7]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => ramp_reg(4),
      I1 => ramp_step_reg(4),
      O => \ramp_reg[7]_i_7_n_0\
    );
\ramp_reg[7]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(7),
      I1 => ramp_reg(7),
      O => \ramp_reg[7]_i_8_n_0\
    );
\ramp_reg[7]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ramp_step_reg(6),
      I1 => ramp_reg(6),
      O => \ramp_reg[7]_i_9_n_0\
    );
\ramp_reg[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8888B8888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(8),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg[31]_i_2_n_0\,
      I4 => data2(8),
      I5 => \ramp_reg_reg[11]_i_2_n_7\,
      O => \ramp_reg[8]_i_1_n_0\
    );
\ramp_reg[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8BBB88B888"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(9),
      I1 => \^ramp_module_0_rst\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \ramp_reg_reg[11]_i_2_n_6\,
      I4 => \ramp_reg[31]_i_2_n_0\,
      I5 => data2(9),
      O => \ramp_reg[9]_i_1_n_0\
    );
\ramp_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[0]_i_1_n_0\,
      Q => ramp_reg(0),
      R => '0'
    );
\ramp_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[10]_i_1_n_0\,
      Q => ramp_reg(10),
      R => '0'
    );
\ramp_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[11]_i_1_n_0\,
      Q => ramp_reg(11),
      R => '0'
    );
\ramp_reg_reg[11]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[7]_i_2_n_0\,
      CO(3) => \ramp_reg_reg[11]_i_2_n_0\,
      CO(2) => \ramp_reg_reg[11]_i_2_n_1\,
      CO(1) => \ramp_reg_reg[11]_i_2_n_2\,
      CO(0) => \ramp_reg_reg[11]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => ramp_reg(11 downto 8),
      O(3) => \ramp_reg_reg[11]_i_2_n_4\,
      O(2) => \ramp_reg_reg[11]_i_2_n_5\,
      O(1) => \ramp_reg_reg[11]_i_2_n_6\,
      O(0) => \ramp_reg_reg[11]_i_2_n_7\,
      S(3) => \ramp_reg[11]_i_4_n_0\,
      S(2) => \ramp_reg[11]_i_5_n_0\,
      S(1) => \ramp_reg[11]_i_6_n_0\,
      S(0) => \ramp_reg[11]_i_7_n_0\
    );
\ramp_reg_reg[11]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[7]_i_3_n_0\,
      CO(3) => \ramp_reg_reg[11]_i_3_n_0\,
      CO(2) => \ramp_reg_reg[11]_i_3_n_1\,
      CO(1) => \ramp_reg_reg[11]_i_3_n_2\,
      CO(0) => \ramp_reg_reg[11]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => ramp_reg(11 downto 8),
      O(3 downto 0) => data2(11 downto 8),
      S(3) => \ramp_reg[11]_i_8_n_0\,
      S(2) => \ramp_reg[11]_i_9_n_0\,
      S(1) => \ramp_reg[11]_i_10_n_0\,
      S(0) => \ramp_reg[11]_i_11_n_0\
    );
\ramp_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[12]_i_1_n_0\,
      Q => ramp_reg(12),
      R => '0'
    );
\ramp_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[13]_i_1_n_0\,
      Q => ramp_reg(13),
      R => '0'
    );
\ramp_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[14]_i_1_n_0\,
      Q => ramp_reg(14),
      R => '0'
    );
\ramp_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[15]_i_1_n_0\,
      Q => ramp_reg(15),
      R => '0'
    );
\ramp_reg_reg[15]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[11]_i_3_n_0\,
      CO(3) => \ramp_reg_reg[15]_i_2_n_0\,
      CO(2) => \ramp_reg_reg[15]_i_2_n_1\,
      CO(1) => \ramp_reg_reg[15]_i_2_n_2\,
      CO(0) => \ramp_reg_reg[15]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => ramp_reg(15 downto 12),
      O(3 downto 0) => data2(15 downto 12),
      S(3) => \ramp_reg[15]_i_4_n_0\,
      S(2) => \ramp_reg[15]_i_5_n_0\,
      S(1) => \ramp_reg[15]_i_6_n_0\,
      S(0) => \ramp_reg[15]_i_7_n_0\
    );
\ramp_reg_reg[15]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[11]_i_2_n_0\,
      CO(3) => \ramp_reg_reg[15]_i_3_n_0\,
      CO(2) => \ramp_reg_reg[15]_i_3_n_1\,
      CO(1) => \ramp_reg_reg[15]_i_3_n_2\,
      CO(0) => \ramp_reg_reg[15]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => ramp_reg(15 downto 12),
      O(3) => \ramp_reg_reg[15]_i_3_n_4\,
      O(2) => \ramp_reg_reg[15]_i_3_n_5\,
      O(1) => \ramp_reg_reg[15]_i_3_n_6\,
      O(0) => \ramp_reg_reg[15]_i_3_n_7\,
      S(3) => \ramp_reg[15]_i_8_n_0\,
      S(2) => \ramp_reg[15]_i_9_n_0\,
      S(1) => \ramp_reg[15]_i_10_n_0\,
      S(0) => \ramp_reg[15]_i_11_n_0\
    );
\ramp_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[16]_i_1_n_0\,
      Q => ramp_reg(16),
      R => '0'
    );
\ramp_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[17]_i_1_n_0\,
      Q => ramp_reg(17),
      R => '0'
    );
\ramp_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[18]_i_1_n_0\,
      Q => \^ramp_output\(0),
      R => '0'
    );
\ramp_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[19]_i_1_n_0\,
      Q => \^ramp_output\(1),
      R => '0'
    );
\ramp_reg_reg[19]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[15]_i_3_n_0\,
      CO(3) => \ramp_reg_reg[19]_i_2_n_0\,
      CO(2) => \ramp_reg_reg[19]_i_2_n_1\,
      CO(1) => \ramp_reg_reg[19]_i_2_n_2\,
      CO(0) => \ramp_reg_reg[19]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => \^ramp_output\(1 downto 0),
      DI(1 downto 0) => ramp_reg(17 downto 16),
      O(3) => \ramp_reg_reg[19]_i_2_n_4\,
      O(2) => \ramp_reg_reg[19]_i_2_n_5\,
      O(1) => \ramp_reg_reg[19]_i_2_n_6\,
      O(0) => \ramp_reg_reg[19]_i_2_n_7\,
      S(3) => \ramp_reg[19]_i_4_n_0\,
      S(2) => \ramp_reg[19]_i_5_n_0\,
      S(1) => \ramp_reg[19]_i_6_n_0\,
      S(0) => \ramp_reg[19]_i_7_n_0\
    );
\ramp_reg_reg[19]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[15]_i_2_n_0\,
      CO(3) => \ramp_reg_reg[19]_i_3_n_0\,
      CO(2) => \ramp_reg_reg[19]_i_3_n_1\,
      CO(1) => \ramp_reg_reg[19]_i_3_n_2\,
      CO(0) => \ramp_reg_reg[19]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => \^ramp_output\(1 downto 0),
      DI(1 downto 0) => ramp_reg(17 downto 16),
      O(3 downto 0) => data2(19 downto 16),
      S(3) => \ramp_reg[19]_i_8_n_0\,
      S(2) => \ramp_reg[19]_i_9_n_0\,
      S(1) => \ramp_reg[19]_i_10_n_0\,
      S(0) => \ramp_reg[19]_i_11_n_0\
    );
\ramp_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[1]_i_1_n_0\,
      Q => ramp_reg(1),
      R => '0'
    );
\ramp_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[20]_i_1_n_0\,
      Q => \^ramp_output\(2),
      R => '0'
    );
\ramp_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[21]_i_1_n_0\,
      Q => \^ramp_output\(3),
      R => '0'
    );
\ramp_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[22]_i_1_n_0\,
      Q => \^ramp_output\(4),
      R => '0'
    );
\ramp_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[23]_i_1_n_0\,
      Q => \^ramp_output\(5),
      R => '0'
    );
\ramp_reg_reg[23]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[19]_i_2_n_0\,
      CO(3) => \ramp_reg_reg[23]_i_2_n_0\,
      CO(2) => \ramp_reg_reg[23]_i_2_n_1\,
      CO(1) => \ramp_reg_reg[23]_i_2_n_2\,
      CO(0) => \ramp_reg_reg[23]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^ramp_output\(5 downto 2),
      O(3) => \ramp_reg_reg[23]_i_2_n_4\,
      O(2) => \ramp_reg_reg[23]_i_2_n_5\,
      O(1) => \ramp_reg_reg[23]_i_2_n_6\,
      O(0) => \ramp_reg_reg[23]_i_2_n_7\,
      S(3) => \ramp_reg[23]_i_4_n_0\,
      S(2) => \ramp_reg[23]_i_5_n_0\,
      S(1) => \ramp_reg[23]_i_6_n_0\,
      S(0) => \ramp_reg[23]_i_7_n_0\
    );
\ramp_reg_reg[23]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[19]_i_3_n_0\,
      CO(3) => \ramp_reg_reg[23]_i_3_n_0\,
      CO(2) => \ramp_reg_reg[23]_i_3_n_1\,
      CO(1) => \ramp_reg_reg[23]_i_3_n_2\,
      CO(0) => \ramp_reg_reg[23]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^ramp_output\(5 downto 2),
      O(3 downto 0) => data2(23 downto 20),
      S(3) => \ramp_reg[23]_i_8_n_0\,
      S(2) => \ramp_reg[23]_i_9_n_0\,
      S(1) => \ramp_reg[23]_i_10_n_0\,
      S(0) => \ramp_reg[23]_i_11_n_0\
    );
\ramp_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[24]_i_1_n_0\,
      Q => \^ramp_output\(6),
      R => '0'
    );
\ramp_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[25]_i_1_n_0\,
      Q => \^ramp_output\(7),
      R => '0'
    );
\ramp_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[26]_i_1_n_0\,
      Q => \^ramp_output\(8),
      R => '0'
    );
\ramp_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[27]_i_1_n_0\,
      Q => \^ramp_output\(9),
      R => '0'
    );
\ramp_reg_reg[27]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[23]_i_2_n_0\,
      CO(3) => \ramp_reg_reg[27]_i_2_n_0\,
      CO(2) => \ramp_reg_reg[27]_i_2_n_1\,
      CO(1) => \ramp_reg_reg[27]_i_2_n_2\,
      CO(0) => \ramp_reg_reg[27]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^ramp_output\(9 downto 6),
      O(3) => \ramp_reg_reg[27]_i_2_n_4\,
      O(2) => \ramp_reg_reg[27]_i_2_n_5\,
      O(1) => \ramp_reg_reg[27]_i_2_n_6\,
      O(0) => \ramp_reg_reg[27]_i_2_n_7\,
      S(3) => \ramp_reg[27]_i_4_n_0\,
      S(2) => \ramp_reg[27]_i_5_n_0\,
      S(1) => \ramp_reg[27]_i_6_n_0\,
      S(0) => \ramp_reg[27]_i_7_n_0\
    );
\ramp_reg_reg[27]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[23]_i_3_n_0\,
      CO(3) => \ramp_reg_reg[27]_i_3_n_0\,
      CO(2) => \ramp_reg_reg[27]_i_3_n_1\,
      CO(1) => \ramp_reg_reg[27]_i_3_n_2\,
      CO(0) => \ramp_reg_reg[27]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^ramp_output\(9 downto 6),
      O(3 downto 0) => data2(27 downto 24),
      S(3) => \ramp_reg[27]_i_8_n_0\,
      S(2) => \ramp_reg[27]_i_9_n_0\,
      S(1) => \ramp_reg[27]_i_10_n_0\,
      S(0) => \ramp_reg[27]_i_11_n_0\
    );
\ramp_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[28]_i_1_n_0\,
      Q => \^ramp_output\(10),
      R => '0'
    );
\ramp_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[29]_i_1_n_0\,
      Q => \^ramp_output\(11),
      R => '0'
    );
\ramp_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[2]_i_1_n_0\,
      Q => ramp_reg(2),
      R => '0'
    );
\ramp_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[30]_i_1_n_0\,
      Q => \^ramp_output\(12),
      R => '0'
    );
\ramp_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[31]_i_1_n_0\,
      Q => \^ramp_output\(13),
      R => '0'
    );
\ramp_reg_reg[31]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[27]_i_3_n_0\,
      CO(3) => \NLW_ramp_reg_reg[31]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \ramp_reg_reg[31]_i_3_n_1\,
      CO(1) => \ramp_reg_reg[31]_i_3_n_2\,
      CO(0) => \ramp_reg_reg[31]_i_3_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \^ramp_output\(12 downto 10),
      O(3 downto 0) => data2(31 downto 28),
      S(3) => \ramp_reg[31]_i_5_n_0\,
      S(2) => \ramp_reg[31]_i_6_n_0\,
      S(1) => \ramp_reg[31]_i_7_n_0\,
      S(0) => \ramp_reg[31]_i_8_n_0\
    );
\ramp_reg_reg[31]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[27]_i_2_n_0\,
      CO(3) => \NLW_ramp_reg_reg[31]_i_4_CO_UNCONNECTED\(3),
      CO(2) => \ramp_reg_reg[31]_i_4_n_1\,
      CO(1) => \ramp_reg_reg[31]_i_4_n_2\,
      CO(0) => \ramp_reg_reg[31]_i_4_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \^ramp_output\(12 downto 10),
      O(3) => \ramp_reg_reg[31]_i_4_n_4\,
      O(2) => \ramp_reg_reg[31]_i_4_n_5\,
      O(1) => \ramp_reg_reg[31]_i_4_n_6\,
      O(0) => \ramp_reg_reg[31]_i_4_n_7\,
      S(3) => \ramp_reg[31]_i_9_n_0\,
      S(2) => \ramp_reg[31]_i_10_n_0\,
      S(1) => \ramp_reg[31]_i_11_n_0\,
      S(0) => \ramp_reg[31]_i_12_n_0\
    );
\ramp_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[3]_i_1_n_0\,
      Q => ramp_reg(3),
      R => '0'
    );
\ramp_reg_reg[3]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ramp_reg_reg[3]_i_2_n_0\,
      CO(2) => \ramp_reg_reg[3]_i_2_n_1\,
      CO(1) => \ramp_reg_reg[3]_i_2_n_2\,
      CO(0) => \ramp_reg_reg[3]_i_2_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => ramp_reg(3 downto 0),
      O(3) => \ramp_reg_reg[3]_i_2_n_4\,
      O(2) => \ramp_reg_reg[3]_i_2_n_5\,
      O(1) => \ramp_reg_reg[3]_i_2_n_6\,
      O(0) => \ramp_reg_reg[3]_i_2_n_7\,
      S(3) => \ramp_reg[3]_i_4_n_0\,
      S(2) => \ramp_reg[3]_i_5_n_0\,
      S(1) => \ramp_reg[3]_i_6_n_0\,
      S(0) => \ramp_reg[3]_i_7_n_0\
    );
\ramp_reg_reg[3]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ramp_reg_reg[3]_i_3_n_0\,
      CO(2) => \ramp_reg_reg[3]_i_3_n_1\,
      CO(1) => \ramp_reg_reg[3]_i_3_n_2\,
      CO(0) => \ramp_reg_reg[3]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => ramp_reg(3 downto 0),
      O(3 downto 0) => data2(3 downto 0),
      S(3) => \ramp_reg[3]_i_8_n_0\,
      S(2) => \ramp_reg[3]_i_9_n_0\,
      S(1) => \ramp_reg[3]_i_10_n_0\,
      S(0) => \ramp_reg[3]_i_11_n_0\
    );
\ramp_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[4]_i_1_n_0\,
      Q => ramp_reg(4),
      R => '0'
    );
\ramp_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[5]_i_1_n_0\,
      Q => ramp_reg(5),
      R => '0'
    );
\ramp_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[6]_i_1_n_0\,
      Q => ramp_reg(6),
      R => '0'
    );
\ramp_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[7]_i_1_n_0\,
      Q => ramp_reg(7),
      R => '0'
    );
\ramp_reg_reg[7]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[3]_i_2_n_0\,
      CO(3) => \ramp_reg_reg[7]_i_2_n_0\,
      CO(2) => \ramp_reg_reg[7]_i_2_n_1\,
      CO(1) => \ramp_reg_reg[7]_i_2_n_2\,
      CO(0) => \ramp_reg_reg[7]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => ramp_reg(7 downto 4),
      O(3) => \ramp_reg_reg[7]_i_2_n_4\,
      O(2) => \ramp_reg_reg[7]_i_2_n_5\,
      O(1) => \ramp_reg_reg[7]_i_2_n_6\,
      O(0) => \ramp_reg_reg[7]_i_2_n_7\,
      S(3) => \ramp_reg[7]_i_4_n_0\,
      S(2) => \ramp_reg[7]_i_5_n_0\,
      S(1) => \ramp_reg[7]_i_6_n_0\,
      S(0) => \ramp_reg[7]_i_7_n_0\
    );
\ramp_reg_reg[7]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramp_reg_reg[3]_i_3_n_0\,
      CO(3) => \ramp_reg_reg[7]_i_3_n_0\,
      CO(2) => \ramp_reg_reg[7]_i_3_n_1\,
      CO(1) => \ramp_reg_reg[7]_i_3_n_2\,
      CO(0) => \ramp_reg_reg[7]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => ramp_reg(7 downto 4),
      O(3 downto 0) => data2(7 downto 4),
      S(3) => \ramp_reg[7]_i_8_n_0\,
      S(2) => \ramp_reg[7]_i_9_n_0\,
      S(1) => \ramp_reg[7]_i_10_n_0\,
      S(0) => \ramp_reg[7]_i_11_n_0\
    );
\ramp_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[8]_i_1_n_0\,
      Q => ramp_reg(8),
      R => '0'
    );
\ramp_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_reg[9]_i_1_n_0\,
      Q => ramp_reg(9),
      R => '0'
    );
\ramp_step_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(0),
      Q => ramp_step_reg(0),
      R => '0'
    );
\ramp_step_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(10),
      Q => ramp_step_reg(10),
      R => '0'
    );
\ramp_step_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(11),
      Q => ramp_step_reg(11),
      R => '0'
    );
\ramp_step_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(12),
      Q => ramp_step_reg(12),
      R => '0'
    );
\ramp_step_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(13),
      Q => ramp_step_reg(13),
      R => '0'
    );
\ramp_step_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(14),
      Q => ramp_step_reg(14),
      R => '0'
    );
\ramp_step_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(15),
      Q => ramp_step_reg(15),
      R => '0'
    );
\ramp_step_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(16),
      Q => ramp_step_reg(16),
      R => '0'
    );
\ramp_step_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(17),
      Q => ramp_step_reg(17),
      R => '0'
    );
\ramp_step_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(18),
      Q => ramp_step_reg(18),
      R => '0'
    );
\ramp_step_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(19),
      Q => ramp_step_reg(19),
      R => '0'
    );
\ramp_step_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(1),
      Q => ramp_step_reg(1),
      R => '0'
    );
\ramp_step_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(20),
      Q => ramp_step_reg(20),
      R => '0'
    );
\ramp_step_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(21),
      Q => ramp_step_reg(21),
      R => '0'
    );
\ramp_step_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(22),
      Q => ramp_step_reg(22),
      R => '0'
    );
\ramp_step_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(23),
      Q => ramp_step_reg(23),
      R => '0'
    );
\ramp_step_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(24),
      Q => ramp_step_reg(24),
      R => '0'
    );
\ramp_step_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(25),
      Q => ramp_step_reg(25),
      R => '0'
    );
\ramp_step_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(26),
      Q => ramp_step_reg(26),
      R => '0'
    );
\ramp_step_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(27),
      Q => ramp_step_reg(27),
      R => '0'
    );
\ramp_step_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(28),
      Q => ramp_step_reg(28),
      R => '0'
    );
\ramp_step_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(29),
      Q => ramp_step_reg(29),
      R => '0'
    );
\ramp_step_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(2),
      Q => ramp_step_reg(2),
      R => '0'
    );
\ramp_step_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(30),
      Q => ramp_step_reg(30),
      R => '0'
    );
\ramp_step_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(31),
      Q => ramp_step_reg(31),
      R => '0'
    );
\ramp_step_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(3),
      Q => ramp_step_reg(3),
      R => '0'
    );
\ramp_step_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(4),
      Q => ramp_step_reg(4),
      R => '0'
    );
\ramp_step_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(5),
      Q => ramp_step_reg(5),
      R => '0'
    );
\ramp_step_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(6),
      Q => ramp_step_reg(6),
      R => '0'
    );
\ramp_step_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(7),
      Q => ramp_step_reg(7),
      R => '0'
    );
\ramp_step_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(8),
      Q => ramp_step_reg(8),
      R => '0'
    );
\ramp_step_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramp_step_reg_reg[31]_0\(9),
      Q => ramp_step_reg(9),
      R => '0'
    );
ramp_up_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF0E"
    )
        port map (
      I0 => ramp_up_reg_n_0,
      I1 => \ramplitude_min_reg_reg[31]_i_4_n_0\,
      I2 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I3 => \^ramp_module_0_rst\,
      O => ramp_up_i_1_n_0
    );
ramp_up_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_up_i_1_n_0,
      Q => ramp_up_reg_n_0,
      R => '0'
    );
\ramplitude_lim_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(0),
      Q => ramplitude_lim_reg(0),
      R => '0'
    );
\ramplitude_lim_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(10),
      Q => ramplitude_lim_reg(10),
      R => '0'
    );
\ramplitude_lim_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(11),
      Q => ramplitude_lim_reg(11),
      R => '0'
    );
\ramplitude_lim_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(12),
      Q => ramplitude_lim_reg(12),
      R => '0'
    );
\ramplitude_lim_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(13),
      Q => ramplitude_lim_reg(13),
      R => '0'
    );
\ramplitude_lim_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(14),
      Q => ramplitude_lim_reg(14),
      R => '0'
    );
\ramplitude_lim_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(15),
      Q => ramplitude_lim_reg(15),
      R => '0'
    );
\ramplitude_lim_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(16),
      Q => ramplitude_lim_reg(16),
      R => '0'
    );
\ramplitude_lim_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(17),
      Q => ramplitude_lim_reg(17),
      R => '0'
    );
\ramplitude_lim_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(18),
      Q => ramplitude_lim_reg(18),
      R => '0'
    );
\ramplitude_lim_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(19),
      Q => ramplitude_lim_reg(19),
      R => '0'
    );
\ramplitude_lim_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(1),
      Q => ramplitude_lim_reg(1),
      R => '0'
    );
\ramplitude_lim_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(20),
      Q => ramplitude_lim_reg(20),
      R => '0'
    );
\ramplitude_lim_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(21),
      Q => ramplitude_lim_reg(21),
      R => '0'
    );
\ramplitude_lim_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(22),
      Q => ramplitude_lim_reg(22),
      R => '0'
    );
\ramplitude_lim_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(23),
      Q => ramplitude_lim_reg(23),
      R => '0'
    );
\ramplitude_lim_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(24),
      Q => ramplitude_lim_reg(24),
      R => '0'
    );
\ramplitude_lim_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(25),
      Q => ramplitude_lim_reg(25),
      R => '0'
    );
\ramplitude_lim_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(26),
      Q => ramplitude_lim_reg(26),
      R => '0'
    );
\ramplitude_lim_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(27),
      Q => ramplitude_lim_reg(27),
      R => '0'
    );
\ramplitude_lim_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(28),
      Q => ramplitude_lim_reg(28),
      R => '0'
    );
\ramplitude_lim_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(29),
      Q => ramplitude_lim_reg(29),
      R => '0'
    );
\ramplitude_lim_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(2),
      Q => ramplitude_lim_reg(2),
      R => '0'
    );
\ramplitude_lim_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(30),
      Q => ramplitude_lim_reg(30),
      R => '0'
    );
\ramplitude_lim_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(31),
      Q => ramplitude_lim_reg(31),
      R => '0'
    );
\ramplitude_lim_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(3),
      Q => ramplitude_lim_reg(3),
      R => '0'
    );
\ramplitude_lim_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(4),
      Q => ramplitude_lim_reg(4),
      R => '0'
    );
\ramplitude_lim_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(5),
      Q => ramplitude_lim_reg(5),
      R => '0'
    );
\ramplitude_lim_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(6),
      Q => ramplitude_lim_reg(6),
      R => '0'
    );
\ramplitude_lim_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(7),
      Q => ramplitude_lim_reg(7),
      R => '0'
    );
\ramplitude_lim_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(8),
      Q => ramplitude_lim_reg(8),
      R => '0'
    );
\ramplitude_lim_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_lim_reg_reg[31]_0\(9),
      Q => ramplitude_lim_reg(9),
      R => '0'
    );
\ramplitude_max_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBFFFFB8880000"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(0),
      I1 => \^ramp_module_0_rst\,
      I2 => ramplitude_max_reg0(0),
      I3 => ramplitude_max_reg1,
      I4 => \ramplitude_min_reg[31]_i_9_n_0\,
      I5 => ramplitude_lim_reg(0),
      O => p_1_in(0)
    );
\ramplitude_max_reg[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(10),
      I1 => \ramp_reg_reg[17]_0\(10),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(10),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(10)
    );
\ramplitude_max_reg[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(11),
      I1 => \ramp_reg_reg[17]_0\(11),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(11),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(11)
    );
\ramplitude_max_reg[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[11]\,
      I1 => ramplitude_step_reg(11),
      O => \ramplitude_max_reg[11]_i_3_n_0\
    );
\ramplitude_max_reg[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[10]\,
      I1 => ramplitude_step_reg(10),
      O => \ramplitude_max_reg[11]_i_4_n_0\
    );
\ramplitude_max_reg[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[9]\,
      I1 => ramplitude_step_reg(9),
      O => \ramplitude_max_reg[11]_i_5_n_0\
    );
\ramplitude_max_reg[11]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[8]\,
      I1 => ramplitude_step_reg(8),
      O => \ramplitude_max_reg[11]_i_6_n_0\
    );
\ramplitude_max_reg[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(12),
      I1 => \ramp_reg_reg[17]_0\(12),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(12),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(12)
    );
\ramplitude_max_reg[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(13),
      I1 => \ramp_reg_reg[17]_0\(13),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(13),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(13)
    );
\ramplitude_max_reg[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(14),
      I1 => \ramp_reg_reg[17]_0\(14),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(14),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(14)
    );
\ramplitude_max_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(15),
      I1 => \ramp_reg_reg[17]_0\(15),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(15),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(15)
    );
\ramplitude_max_reg[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[15]\,
      I1 => ramplitude_step_reg(15),
      O => \ramplitude_max_reg[15]_i_3_n_0\
    );
\ramplitude_max_reg[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[14]\,
      I1 => ramplitude_step_reg(14),
      O => \ramplitude_max_reg[15]_i_4_n_0\
    );
\ramplitude_max_reg[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[13]\,
      I1 => ramplitude_step_reg(13),
      O => \ramplitude_max_reg[15]_i_5_n_0\
    );
\ramplitude_max_reg[15]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[12]\,
      I1 => ramplitude_step_reg(12),
      O => \ramplitude_max_reg[15]_i_6_n_0\
    );
\ramplitude_max_reg[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(16),
      I1 => \ramp_reg_reg[17]_0\(16),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(16),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(16)
    );
\ramplitude_max_reg[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(17),
      I1 => \ramp_reg_reg[17]_0\(17),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(17),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(17)
    );
\ramplitude_max_reg[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(18),
      I1 => ramp_module_0_reset(0),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(18),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(18)
    );
\ramplitude_max_reg[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(19),
      I1 => ramp_module_0_reset(1),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(19),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(19)
    );
\ramplitude_max_reg[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[19]\,
      I1 => ramplitude_step_reg(19),
      O => \ramplitude_max_reg[19]_i_3_n_0\
    );
\ramplitude_max_reg[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[18]\,
      I1 => ramplitude_step_reg(18),
      O => \ramplitude_max_reg[19]_i_4_n_0\
    );
\ramplitude_max_reg[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[17]\,
      I1 => ramplitude_step_reg(17),
      O => \ramplitude_max_reg[19]_i_5_n_0\
    );
\ramplitude_max_reg[19]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[16]\,
      I1 => ramplitude_step_reg(16),
      O => \ramplitude_max_reg[19]_i_6_n_0\
    );
\ramplitude_max_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(1),
      I1 => \ramp_reg_reg[17]_0\(1),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(1),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(1)
    );
\ramplitude_max_reg[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(20),
      I1 => ramp_module_0_reset(2),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(20),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(20)
    );
\ramplitude_max_reg[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(21),
      I1 => ramp_module_0_reset(3),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(21),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(21)
    );
\ramplitude_max_reg[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(22),
      I1 => ramp_module_0_reset(4),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(22),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(22)
    );
\ramplitude_max_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(23),
      I1 => ramp_module_0_reset(5),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(23),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(23)
    );
\ramplitude_max_reg[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[23]\,
      I1 => ramplitude_step_reg(23),
      O => \ramplitude_max_reg[23]_i_3_n_0\
    );
\ramplitude_max_reg[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[22]\,
      I1 => ramplitude_step_reg(22),
      O => \ramplitude_max_reg[23]_i_4_n_0\
    );
\ramplitude_max_reg[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[21]\,
      I1 => ramplitude_step_reg(21),
      O => \ramplitude_max_reg[23]_i_5_n_0\
    );
\ramplitude_max_reg[23]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[20]\,
      I1 => ramplitude_step_reg(20),
      O => \ramplitude_max_reg[23]_i_6_n_0\
    );
\ramplitude_max_reg[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(24),
      I1 => ramp_module_0_reset(6),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(24),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(24)
    );
\ramplitude_max_reg[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(25),
      I1 => ramp_module_0_reset(7),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(25),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(25)
    );
\ramplitude_max_reg[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(26),
      I1 => ramp_module_0_reset(8),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(26),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(26)
    );
\ramplitude_max_reg[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(27),
      I1 => ramp_module_0_reset(9),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(27),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(27)
    );
\ramplitude_max_reg[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[27]\,
      I1 => ramplitude_step_reg(27),
      O => \ramplitude_max_reg[27]_i_3_n_0\
    );
\ramplitude_max_reg[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[26]\,
      I1 => ramplitude_step_reg(26),
      O => \ramplitude_max_reg[27]_i_4_n_0\
    );
\ramplitude_max_reg[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[25]\,
      I1 => ramplitude_step_reg(25),
      O => \ramplitude_max_reg[27]_i_5_n_0\
    );
\ramplitude_max_reg[27]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[24]\,
      I1 => ramplitude_step_reg(24),
      O => \ramplitude_max_reg[27]_i_6_n_0\
    );
\ramplitude_max_reg[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(28),
      I1 => ramp_module_0_reset(10),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(28),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(28)
    );
\ramplitude_max_reg[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(29),
      I1 => ramp_module_0_reset(11),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(29),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(29)
    );
\ramplitude_max_reg[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(2),
      I1 => \ramp_reg_reg[17]_0\(2),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(2),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(2)
    );
\ramplitude_max_reg[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(30),
      I1 => ramp_module_0_reset(12),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(30),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(30)
    );
\ramplitude_max_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I1 => \^ramp_module_0_rst\,
      O => \ramplitude_max_reg[31]_i_1_n_0\
    );
\ramplitude_max_reg[31]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => ramplitude_lim_reg(31),
      I1 => \ramplitude_max_reg_reg_n_0_[31]\,
      I2 => \ramplitude_max_reg_reg_n_0_[30]\,
      I3 => ramplitude_lim_reg(30),
      O => \ramplitude_max_reg[31]_i_10_n_0\
    );
\ramplitude_max_reg[31]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[29]\,
      I1 => ramplitude_lim_reg(29),
      I2 => \ramplitude_max_reg_reg_n_0_[28]\,
      I3 => ramplitude_lim_reg(28),
      O => \ramplitude_max_reg[31]_i_11_n_0\
    );
\ramplitude_max_reg[31]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[27]\,
      I1 => ramplitude_lim_reg(27),
      I2 => \ramplitude_max_reg_reg_n_0_[26]\,
      I3 => ramplitude_lim_reg(26),
      O => \ramplitude_max_reg[31]_i_12_n_0\
    );
\ramplitude_max_reg[31]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[25]\,
      I1 => ramplitude_lim_reg(25),
      I2 => \ramplitude_max_reg_reg_n_0_[24]\,
      I3 => ramplitude_lim_reg(24),
      O => \ramplitude_max_reg[31]_i_13_n_0\
    );
\ramplitude_max_reg[31]_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[31]\,
      I1 => ramplitude_step_reg(31),
      O => \ramplitude_max_reg[31]_i_14_n_0\
    );
\ramplitude_max_reg[31]_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[30]\,
      I1 => ramplitude_step_reg(30),
      O => \ramplitude_max_reg[31]_i_15_n_0\
    );
\ramplitude_max_reg[31]_i_16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[29]\,
      I1 => ramplitude_step_reg(29),
      O => \ramplitude_max_reg[31]_i_16_n_0\
    );
\ramplitude_max_reg[31]_i_17\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[28]\,
      I1 => ramplitude_step_reg(28),
      O => \ramplitude_max_reg[31]_i_17_n_0\
    );
\ramplitude_max_reg[31]_i_19\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(23),
      I1 => \ramplitude_max_reg_reg_n_0_[23]\,
      I2 => ramplitude_lim_reg(22),
      I3 => \ramplitude_max_reg_reg_n_0_[22]\,
      O => \ramplitude_max_reg[31]_i_19_n_0\
    );
\ramplitude_max_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(31),
      I1 => ramp_module_0_reset(13),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(31),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(31)
    );
\ramplitude_max_reg[31]_i_20\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(21),
      I1 => \ramplitude_max_reg_reg_n_0_[21]\,
      I2 => ramplitude_lim_reg(20),
      I3 => \ramplitude_max_reg_reg_n_0_[20]\,
      O => \ramplitude_max_reg[31]_i_20_n_0\
    );
\ramplitude_max_reg[31]_i_21\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(19),
      I1 => \ramplitude_max_reg_reg_n_0_[19]\,
      I2 => ramplitude_lim_reg(18),
      I3 => \ramplitude_max_reg_reg_n_0_[18]\,
      O => \ramplitude_max_reg[31]_i_21_n_0\
    );
\ramplitude_max_reg[31]_i_22\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(17),
      I1 => \ramplitude_max_reg_reg_n_0_[17]\,
      I2 => ramplitude_lim_reg(16),
      I3 => \ramplitude_max_reg_reg_n_0_[16]\,
      O => \ramplitude_max_reg[31]_i_22_n_0\
    );
\ramplitude_max_reg[31]_i_23\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[23]\,
      I1 => ramplitude_lim_reg(23),
      I2 => \ramplitude_max_reg_reg_n_0_[22]\,
      I3 => ramplitude_lim_reg(22),
      O => \ramplitude_max_reg[31]_i_23_n_0\
    );
\ramplitude_max_reg[31]_i_24\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[21]\,
      I1 => ramplitude_lim_reg(21),
      I2 => \ramplitude_max_reg_reg_n_0_[20]\,
      I3 => ramplitude_lim_reg(20),
      O => \ramplitude_max_reg[31]_i_24_n_0\
    );
\ramplitude_max_reg[31]_i_25\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[19]\,
      I1 => ramplitude_lim_reg(19),
      I2 => \ramplitude_max_reg_reg_n_0_[18]\,
      I3 => ramplitude_lim_reg(18),
      O => \ramplitude_max_reg[31]_i_25_n_0\
    );
\ramplitude_max_reg[31]_i_26\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[17]\,
      I1 => ramplitude_lim_reg(17),
      I2 => \ramplitude_max_reg_reg_n_0_[16]\,
      I3 => ramplitude_lim_reg(16),
      O => \ramplitude_max_reg[31]_i_26_n_0\
    );
\ramplitude_max_reg[31]_i_28\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(15),
      I1 => \ramplitude_max_reg_reg_n_0_[15]\,
      I2 => ramplitude_lim_reg(14),
      I3 => \ramplitude_max_reg_reg_n_0_[14]\,
      O => \ramplitude_max_reg[31]_i_28_n_0\
    );
\ramplitude_max_reg[31]_i_29\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(13),
      I1 => \ramplitude_max_reg_reg_n_0_[13]\,
      I2 => ramplitude_lim_reg(12),
      I3 => \ramplitude_max_reg_reg_n_0_[12]\,
      O => \ramplitude_max_reg[31]_i_29_n_0\
    );
\ramplitude_max_reg[31]_i_30\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(11),
      I1 => \ramplitude_max_reg_reg_n_0_[11]\,
      I2 => ramplitude_lim_reg(10),
      I3 => \ramplitude_max_reg_reg_n_0_[10]\,
      O => \ramplitude_max_reg[31]_i_30_n_0\
    );
\ramplitude_max_reg[31]_i_31\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(9),
      I1 => \ramplitude_max_reg_reg_n_0_[9]\,
      I2 => ramplitude_lim_reg(8),
      I3 => \ramplitude_max_reg_reg_n_0_[8]\,
      O => \ramplitude_max_reg[31]_i_31_n_0\
    );
\ramplitude_max_reg[31]_i_32\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[15]\,
      I1 => ramplitude_lim_reg(15),
      I2 => \ramplitude_max_reg_reg_n_0_[14]\,
      I3 => ramplitude_lim_reg(14),
      O => \ramplitude_max_reg[31]_i_32_n_0\
    );
\ramplitude_max_reg[31]_i_33\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[13]\,
      I1 => ramplitude_lim_reg(13),
      I2 => \ramplitude_max_reg_reg_n_0_[12]\,
      I3 => ramplitude_lim_reg(12),
      O => \ramplitude_max_reg[31]_i_33_n_0\
    );
\ramplitude_max_reg[31]_i_34\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[11]\,
      I1 => ramplitude_lim_reg(11),
      I2 => \ramplitude_max_reg_reg_n_0_[10]\,
      I3 => ramplitude_lim_reg(10),
      O => \ramplitude_max_reg[31]_i_34_n_0\
    );
\ramplitude_max_reg[31]_i_35\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[9]\,
      I1 => ramplitude_lim_reg(9),
      I2 => \ramplitude_max_reg_reg_n_0_[8]\,
      I3 => ramplitude_lim_reg(8),
      O => \ramplitude_max_reg[31]_i_35_n_0\
    );
\ramplitude_max_reg[31]_i_36\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(7),
      I1 => \ramplitude_max_reg_reg_n_0_[7]\,
      I2 => ramplitude_lim_reg(6),
      I3 => \ramplitude_max_reg_reg_n_0_[6]\,
      O => \ramplitude_max_reg[31]_i_36_n_0\
    );
\ramplitude_max_reg[31]_i_37\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(5),
      I1 => \ramplitude_max_reg_reg_n_0_[5]\,
      I2 => ramplitude_lim_reg(4),
      I3 => \ramplitude_max_reg_reg_n_0_[4]\,
      O => \ramplitude_max_reg[31]_i_37_n_0\
    );
\ramplitude_max_reg[31]_i_38\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(3),
      I1 => \ramplitude_max_reg_reg_n_0_[3]\,
      I2 => ramplitude_lim_reg(2),
      I3 => \ramplitude_max_reg_reg_n_0_[2]\,
      O => \ramplitude_max_reg[31]_i_38_n_0\
    );
\ramplitude_max_reg[31]_i_39\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(1),
      I1 => \ramplitude_max_reg_reg_n_0_[1]\,
      I2 => ramplitude_lim_reg(0),
      I3 => \ramplitude_max_reg_reg_n_0_[0]\,
      O => \ramplitude_max_reg[31]_i_39_n_0\
    );
\ramplitude_max_reg[31]_i_40\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[7]\,
      I1 => ramplitude_lim_reg(7),
      I2 => \ramplitude_max_reg_reg_n_0_[6]\,
      I3 => ramplitude_lim_reg(6),
      O => \ramplitude_max_reg[31]_i_40_n_0\
    );
\ramplitude_max_reg[31]_i_41\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[5]\,
      I1 => ramplitude_lim_reg(5),
      I2 => \ramplitude_max_reg_reg_n_0_[4]\,
      I3 => ramplitude_lim_reg(4),
      O => \ramplitude_max_reg[31]_i_41_n_0\
    );
\ramplitude_max_reg[31]_i_42\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[3]\,
      I1 => ramplitude_lim_reg(3),
      I2 => \ramplitude_max_reg_reg_n_0_[2]\,
      I3 => ramplitude_lim_reg(2),
      O => \ramplitude_max_reg[31]_i_42_n_0\
    );
\ramplitude_max_reg[31]_i_43\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[1]\,
      I1 => ramplitude_lim_reg(1),
      I2 => \ramplitude_max_reg_reg_n_0_[0]\,
      I3 => ramplitude_lim_reg(0),
      O => \ramplitude_max_reg[31]_i_43_n_0\
    );
\ramplitude_max_reg[31]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[31]\,
      I1 => ramplitude_lim_reg(31),
      I2 => ramplitude_lim_reg(30),
      I3 => \ramplitude_max_reg_reg_n_0_[30]\,
      O => \ramplitude_max_reg[31]_i_6_n_0\
    );
\ramplitude_max_reg[31]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(29),
      I1 => \ramplitude_max_reg_reg_n_0_[29]\,
      I2 => ramplitude_lim_reg(28),
      I3 => \ramplitude_max_reg_reg_n_0_[28]\,
      O => \ramplitude_max_reg[31]_i_7_n_0\
    );
\ramplitude_max_reg[31]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(27),
      I1 => \ramplitude_max_reg_reg_n_0_[27]\,
      I2 => ramplitude_lim_reg(26),
      I3 => \ramplitude_max_reg_reg_n_0_[26]\,
      O => \ramplitude_max_reg[31]_i_8_n_0\
    );
\ramplitude_max_reg[31]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramplitude_lim_reg(25),
      I1 => \ramplitude_max_reg_reg_n_0_[25]\,
      I2 => ramplitude_lim_reg(24),
      I3 => \ramplitude_max_reg_reg_n_0_[24]\,
      O => \ramplitude_max_reg[31]_i_9_n_0\
    );
\ramplitude_max_reg[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(3),
      I1 => \ramp_reg_reg[17]_0\(3),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(3),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(3)
    );
\ramplitude_max_reg[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[3]\,
      I1 => ramplitude_step_reg(3),
      O => \ramplitude_max_reg[3]_i_3_n_0\
    );
\ramplitude_max_reg[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[2]\,
      I1 => ramplitude_step_reg(2),
      O => \ramplitude_max_reg[3]_i_4_n_0\
    );
\ramplitude_max_reg[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[1]\,
      I1 => ramplitude_step_reg(1),
      O => \ramplitude_max_reg[3]_i_5_n_0\
    );
\ramplitude_max_reg[3]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[0]\,
      I1 => ramplitude_step_reg(0),
      O => \ramplitude_max_reg[3]_i_6_n_0\
    );
\ramplitude_max_reg[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(4),
      I1 => \ramp_reg_reg[17]_0\(4),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(4),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(4)
    );
\ramplitude_max_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(5),
      I1 => \ramp_reg_reg[17]_0\(5),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(5),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(5)
    );
\ramplitude_max_reg[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(6),
      I1 => \ramp_reg_reg[17]_0\(6),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(6),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(6)
    );
\ramplitude_max_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(7),
      I1 => \ramp_reg_reg[17]_0\(7),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(7),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(7)
    );
\ramplitude_max_reg[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[7]\,
      I1 => ramplitude_step_reg(7),
      O => \ramplitude_max_reg[7]_i_3_n_0\
    );
\ramplitude_max_reg[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[6]\,
      I1 => ramplitude_step_reg(6),
      O => \ramplitude_max_reg[7]_i_4_n_0\
    );
\ramplitude_max_reg[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[5]\,
      I1 => ramplitude_step_reg(5),
      O => \ramplitude_max_reg[7]_i_5_n_0\
    );
\ramplitude_max_reg[7]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[4]\,
      I1 => ramplitude_step_reg(4),
      O => \ramplitude_max_reg[7]_i_6_n_0\
    );
\ramplitude_max_reg[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(8),
      I1 => \ramp_reg_reg[17]_0\(8),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(8),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(8)
    );
\ramplitude_max_reg[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => ramplitude_lim_reg(9),
      I1 => \ramp_reg_reg[17]_0\(9),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_max_reg1,
      I4 => ramplitude_max_reg0(9),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => p_1_in(9)
    );
\ramplitude_max_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(0),
      Q => \ramplitude_max_reg_reg_n_0_[0]\,
      R => '0'
    );
\ramplitude_max_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(10),
      Q => \ramplitude_max_reg_reg_n_0_[10]\,
      R => '0'
    );
\ramplitude_max_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(11),
      Q => \ramplitude_max_reg_reg_n_0_[11]\,
      R => '0'
    );
\ramplitude_max_reg_reg[11]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_max_reg_reg[7]_i_2_n_0\,
      CO(3) => \ramplitude_max_reg_reg[11]_i_2_n_0\,
      CO(2) => \ramplitude_max_reg_reg[11]_i_2_n_1\,
      CO(1) => \ramplitude_max_reg_reg[11]_i_2_n_2\,
      CO(0) => \ramplitude_max_reg_reg[11]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_max_reg_reg_n_0_[11]\,
      DI(2) => \ramplitude_max_reg_reg_n_0_[10]\,
      DI(1) => \ramplitude_max_reg_reg_n_0_[9]\,
      DI(0) => \ramplitude_max_reg_reg_n_0_[8]\,
      O(3 downto 0) => ramplitude_max_reg0(11 downto 8),
      S(3) => \ramplitude_max_reg[11]_i_3_n_0\,
      S(2) => \ramplitude_max_reg[11]_i_4_n_0\,
      S(1) => \ramplitude_max_reg[11]_i_5_n_0\,
      S(0) => \ramplitude_max_reg[11]_i_6_n_0\
    );
\ramplitude_max_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(12),
      Q => \ramplitude_max_reg_reg_n_0_[12]\,
      R => '0'
    );
\ramplitude_max_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(13),
      Q => \ramplitude_max_reg_reg_n_0_[13]\,
      R => '0'
    );
\ramplitude_max_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(14),
      Q => \ramplitude_max_reg_reg_n_0_[14]\,
      R => '0'
    );
\ramplitude_max_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(15),
      Q => \ramplitude_max_reg_reg_n_0_[15]\,
      R => '0'
    );
\ramplitude_max_reg_reg[15]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_max_reg_reg[11]_i_2_n_0\,
      CO(3) => \ramplitude_max_reg_reg[15]_i_2_n_0\,
      CO(2) => \ramplitude_max_reg_reg[15]_i_2_n_1\,
      CO(1) => \ramplitude_max_reg_reg[15]_i_2_n_2\,
      CO(0) => \ramplitude_max_reg_reg[15]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_max_reg_reg_n_0_[15]\,
      DI(2) => \ramplitude_max_reg_reg_n_0_[14]\,
      DI(1) => \ramplitude_max_reg_reg_n_0_[13]\,
      DI(0) => \ramplitude_max_reg_reg_n_0_[12]\,
      O(3 downto 0) => ramplitude_max_reg0(15 downto 12),
      S(3) => \ramplitude_max_reg[15]_i_3_n_0\,
      S(2) => \ramplitude_max_reg[15]_i_4_n_0\,
      S(1) => \ramplitude_max_reg[15]_i_5_n_0\,
      S(0) => \ramplitude_max_reg[15]_i_6_n_0\
    );
\ramplitude_max_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(16),
      Q => \ramplitude_max_reg_reg_n_0_[16]\,
      R => '0'
    );
\ramplitude_max_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(17),
      Q => \ramplitude_max_reg_reg_n_0_[17]\,
      R => '0'
    );
\ramplitude_max_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(18),
      Q => \ramplitude_max_reg_reg_n_0_[18]\,
      R => '0'
    );
\ramplitude_max_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(19),
      Q => \ramplitude_max_reg_reg_n_0_[19]\,
      R => '0'
    );
\ramplitude_max_reg_reg[19]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_max_reg_reg[15]_i_2_n_0\,
      CO(3) => \ramplitude_max_reg_reg[19]_i_2_n_0\,
      CO(2) => \ramplitude_max_reg_reg[19]_i_2_n_1\,
      CO(1) => \ramplitude_max_reg_reg[19]_i_2_n_2\,
      CO(0) => \ramplitude_max_reg_reg[19]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_max_reg_reg_n_0_[19]\,
      DI(2) => \ramplitude_max_reg_reg_n_0_[18]\,
      DI(1) => \ramplitude_max_reg_reg_n_0_[17]\,
      DI(0) => \ramplitude_max_reg_reg_n_0_[16]\,
      O(3 downto 0) => ramplitude_max_reg0(19 downto 16),
      S(3) => \ramplitude_max_reg[19]_i_3_n_0\,
      S(2) => \ramplitude_max_reg[19]_i_4_n_0\,
      S(1) => \ramplitude_max_reg[19]_i_5_n_0\,
      S(0) => \ramplitude_max_reg[19]_i_6_n_0\
    );
\ramplitude_max_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(1),
      Q => \ramplitude_max_reg_reg_n_0_[1]\,
      R => '0'
    );
\ramplitude_max_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(20),
      Q => \ramplitude_max_reg_reg_n_0_[20]\,
      R => '0'
    );
\ramplitude_max_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(21),
      Q => \ramplitude_max_reg_reg_n_0_[21]\,
      R => '0'
    );
\ramplitude_max_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(22),
      Q => \ramplitude_max_reg_reg_n_0_[22]\,
      R => '0'
    );
\ramplitude_max_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(23),
      Q => \ramplitude_max_reg_reg_n_0_[23]\,
      R => '0'
    );
\ramplitude_max_reg_reg[23]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_max_reg_reg[19]_i_2_n_0\,
      CO(3) => \ramplitude_max_reg_reg[23]_i_2_n_0\,
      CO(2) => \ramplitude_max_reg_reg[23]_i_2_n_1\,
      CO(1) => \ramplitude_max_reg_reg[23]_i_2_n_2\,
      CO(0) => \ramplitude_max_reg_reg[23]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_max_reg_reg_n_0_[23]\,
      DI(2) => \ramplitude_max_reg_reg_n_0_[22]\,
      DI(1) => \ramplitude_max_reg_reg_n_0_[21]\,
      DI(0) => \ramplitude_max_reg_reg_n_0_[20]\,
      O(3 downto 0) => ramplitude_max_reg0(23 downto 20),
      S(3) => \ramplitude_max_reg[23]_i_3_n_0\,
      S(2) => \ramplitude_max_reg[23]_i_4_n_0\,
      S(1) => \ramplitude_max_reg[23]_i_5_n_0\,
      S(0) => \ramplitude_max_reg[23]_i_6_n_0\
    );
\ramplitude_max_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(24),
      Q => \ramplitude_max_reg_reg_n_0_[24]\,
      R => '0'
    );
\ramplitude_max_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(25),
      Q => \ramplitude_max_reg_reg_n_0_[25]\,
      R => '0'
    );
\ramplitude_max_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(26),
      Q => \ramplitude_max_reg_reg_n_0_[26]\,
      R => '0'
    );
\ramplitude_max_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(27),
      Q => \ramplitude_max_reg_reg_n_0_[27]\,
      R => '0'
    );
\ramplitude_max_reg_reg[27]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_max_reg_reg[23]_i_2_n_0\,
      CO(3) => \ramplitude_max_reg_reg[27]_i_2_n_0\,
      CO(2) => \ramplitude_max_reg_reg[27]_i_2_n_1\,
      CO(1) => \ramplitude_max_reg_reg[27]_i_2_n_2\,
      CO(0) => \ramplitude_max_reg_reg[27]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_max_reg_reg_n_0_[27]\,
      DI(2) => \ramplitude_max_reg_reg_n_0_[26]\,
      DI(1) => \ramplitude_max_reg_reg_n_0_[25]\,
      DI(0) => \ramplitude_max_reg_reg_n_0_[24]\,
      O(3 downto 0) => ramplitude_max_reg0(27 downto 24),
      S(3) => \ramplitude_max_reg[27]_i_3_n_0\,
      S(2) => \ramplitude_max_reg[27]_i_4_n_0\,
      S(1) => \ramplitude_max_reg[27]_i_5_n_0\,
      S(0) => \ramplitude_max_reg[27]_i_6_n_0\
    );
\ramplitude_max_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(28),
      Q => \ramplitude_max_reg_reg_n_0_[28]\,
      R => '0'
    );
\ramplitude_max_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(29),
      Q => \ramplitude_max_reg_reg_n_0_[29]\,
      R => '0'
    );
\ramplitude_max_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(2),
      Q => \ramplitude_max_reg_reg_n_0_[2]\,
      R => '0'
    );
\ramplitude_max_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(30),
      Q => \ramplitude_max_reg_reg_n_0_[30]\,
      R => '0'
    );
\ramplitude_max_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(31),
      Q => \ramplitude_max_reg_reg_n_0_[31]\,
      R => '0'
    );
\ramplitude_max_reg_reg[31]_i_18\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_max_reg_reg[31]_i_27_n_0\,
      CO(3) => \ramplitude_max_reg_reg[31]_i_18_n_0\,
      CO(2) => \ramplitude_max_reg_reg[31]_i_18_n_1\,
      CO(1) => \ramplitude_max_reg_reg[31]_i_18_n_2\,
      CO(0) => \ramplitude_max_reg_reg[31]_i_18_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_max_reg[31]_i_28_n_0\,
      DI(2) => \ramplitude_max_reg[31]_i_29_n_0\,
      DI(1) => \ramplitude_max_reg[31]_i_30_n_0\,
      DI(0) => \ramplitude_max_reg[31]_i_31_n_0\,
      O(3 downto 0) => \NLW_ramplitude_max_reg_reg[31]_i_18_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_max_reg[31]_i_32_n_0\,
      S(2) => \ramplitude_max_reg[31]_i_33_n_0\,
      S(1) => \ramplitude_max_reg[31]_i_34_n_0\,
      S(0) => \ramplitude_max_reg[31]_i_35_n_0\
    );
\ramplitude_max_reg_reg[31]_i_27\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ramplitude_max_reg_reg[31]_i_27_n_0\,
      CO(2) => \ramplitude_max_reg_reg[31]_i_27_n_1\,
      CO(1) => \ramplitude_max_reg_reg[31]_i_27_n_2\,
      CO(0) => \ramplitude_max_reg_reg[31]_i_27_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_max_reg[31]_i_36_n_0\,
      DI(2) => \ramplitude_max_reg[31]_i_37_n_0\,
      DI(1) => \ramplitude_max_reg[31]_i_38_n_0\,
      DI(0) => \ramplitude_max_reg[31]_i_39_n_0\,
      O(3 downto 0) => \NLW_ramplitude_max_reg_reg[31]_i_27_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_max_reg[31]_i_40_n_0\,
      S(2) => \ramplitude_max_reg[31]_i_41_n_0\,
      S(1) => \ramplitude_max_reg[31]_i_42_n_0\,
      S(0) => \ramplitude_max_reg[31]_i_43_n_0\
    );
\ramplitude_max_reg_reg[31]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_max_reg_reg[31]_i_5_n_0\,
      CO(3) => ramplitude_max_reg1,
      CO(2) => \ramplitude_max_reg_reg[31]_i_3_n_1\,
      CO(1) => \ramplitude_max_reg_reg[31]_i_3_n_2\,
      CO(0) => \ramplitude_max_reg_reg[31]_i_3_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_max_reg[31]_i_6_n_0\,
      DI(2) => \ramplitude_max_reg[31]_i_7_n_0\,
      DI(1) => \ramplitude_max_reg[31]_i_8_n_0\,
      DI(0) => \ramplitude_max_reg[31]_i_9_n_0\,
      O(3 downto 0) => \NLW_ramplitude_max_reg_reg[31]_i_3_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_max_reg[31]_i_10_n_0\,
      S(2) => \ramplitude_max_reg[31]_i_11_n_0\,
      S(1) => \ramplitude_max_reg[31]_i_12_n_0\,
      S(0) => \ramplitude_max_reg[31]_i_13_n_0\
    );
\ramplitude_max_reg_reg[31]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_max_reg_reg[27]_i_2_n_0\,
      CO(3) => \NLW_ramplitude_max_reg_reg[31]_i_4_CO_UNCONNECTED\(3),
      CO(2) => \ramplitude_max_reg_reg[31]_i_4_n_1\,
      CO(1) => \ramplitude_max_reg_reg[31]_i_4_n_2\,
      CO(0) => \ramplitude_max_reg_reg[31]_i_4_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \ramplitude_max_reg_reg_n_0_[30]\,
      DI(1) => \ramplitude_max_reg_reg_n_0_[29]\,
      DI(0) => \ramplitude_max_reg_reg_n_0_[28]\,
      O(3 downto 0) => ramplitude_max_reg0(31 downto 28),
      S(3) => \ramplitude_max_reg[31]_i_14_n_0\,
      S(2) => \ramplitude_max_reg[31]_i_15_n_0\,
      S(1) => \ramplitude_max_reg[31]_i_16_n_0\,
      S(0) => \ramplitude_max_reg[31]_i_17_n_0\
    );
\ramplitude_max_reg_reg[31]_i_5\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_max_reg_reg[31]_i_18_n_0\,
      CO(3) => \ramplitude_max_reg_reg[31]_i_5_n_0\,
      CO(2) => \ramplitude_max_reg_reg[31]_i_5_n_1\,
      CO(1) => \ramplitude_max_reg_reg[31]_i_5_n_2\,
      CO(0) => \ramplitude_max_reg_reg[31]_i_5_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_max_reg[31]_i_19_n_0\,
      DI(2) => \ramplitude_max_reg[31]_i_20_n_0\,
      DI(1) => \ramplitude_max_reg[31]_i_21_n_0\,
      DI(0) => \ramplitude_max_reg[31]_i_22_n_0\,
      O(3 downto 0) => \NLW_ramplitude_max_reg_reg[31]_i_5_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_max_reg[31]_i_23_n_0\,
      S(2) => \ramplitude_max_reg[31]_i_24_n_0\,
      S(1) => \ramplitude_max_reg[31]_i_25_n_0\,
      S(0) => \ramplitude_max_reg[31]_i_26_n_0\
    );
\ramplitude_max_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(3),
      Q => \ramplitude_max_reg_reg_n_0_[3]\,
      R => '0'
    );
\ramplitude_max_reg_reg[3]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ramplitude_max_reg_reg[3]_i_2_n_0\,
      CO(2) => \ramplitude_max_reg_reg[3]_i_2_n_1\,
      CO(1) => \ramplitude_max_reg_reg[3]_i_2_n_2\,
      CO(0) => \ramplitude_max_reg_reg[3]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_max_reg_reg_n_0_[3]\,
      DI(2) => \ramplitude_max_reg_reg_n_0_[2]\,
      DI(1) => \ramplitude_max_reg_reg_n_0_[1]\,
      DI(0) => \ramplitude_max_reg_reg_n_0_[0]\,
      O(3 downto 0) => ramplitude_max_reg0(3 downto 0),
      S(3) => \ramplitude_max_reg[3]_i_3_n_0\,
      S(2) => \ramplitude_max_reg[3]_i_4_n_0\,
      S(1) => \ramplitude_max_reg[3]_i_5_n_0\,
      S(0) => \ramplitude_max_reg[3]_i_6_n_0\
    );
\ramplitude_max_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(4),
      Q => \ramplitude_max_reg_reg_n_0_[4]\,
      R => '0'
    );
\ramplitude_max_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(5),
      Q => \ramplitude_max_reg_reg_n_0_[5]\,
      R => '0'
    );
\ramplitude_max_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(6),
      Q => \ramplitude_max_reg_reg_n_0_[6]\,
      R => '0'
    );
\ramplitude_max_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(7),
      Q => \ramplitude_max_reg_reg_n_0_[7]\,
      R => '0'
    );
\ramplitude_max_reg_reg[7]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_max_reg_reg[3]_i_2_n_0\,
      CO(3) => \ramplitude_max_reg_reg[7]_i_2_n_0\,
      CO(2) => \ramplitude_max_reg_reg[7]_i_2_n_1\,
      CO(1) => \ramplitude_max_reg_reg[7]_i_2_n_2\,
      CO(0) => \ramplitude_max_reg_reg[7]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_max_reg_reg_n_0_[7]\,
      DI(2) => \ramplitude_max_reg_reg_n_0_[6]\,
      DI(1) => \ramplitude_max_reg_reg_n_0_[5]\,
      DI(0) => \ramplitude_max_reg_reg_n_0_[4]\,
      O(3 downto 0) => ramplitude_max_reg0(7 downto 4),
      S(3) => \ramplitude_max_reg[7]_i_3_n_0\,
      S(2) => \ramplitude_max_reg[7]_i_4_n_0\,
      S(1) => \ramplitude_max_reg[7]_i_5_n_0\,
      S(0) => \ramplitude_max_reg[7]_i_6_n_0\
    );
\ramplitude_max_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(8),
      Q => \ramplitude_max_reg_reg_n_0_[8]\,
      R => '0'
    );
\ramplitude_max_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_max_reg[31]_i_1_n_0\,
      D => p_1_in(9),
      Q => \ramplitude_max_reg_reg_n_0_[9]\,
      R => '0'
    );
\ramplitude_min_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBFFFFB8880000"
    )
        port map (
      I0 => \ramp_reg_reg[17]_0\(0),
      I1 => \^ramp_module_0_rst\,
      I2 => ramplitude_min_reg0(0),
      I3 => ramplitude_min_reg1,
      I4 => \ramplitude_min_reg[31]_i_9_n_0\,
      I5 => ramplitude_lim_reg(0),
      O => \ramplitude_min_reg[0]_i_1_n_0\
    );
\ramplitude_min_reg[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(10),
      I1 => \ramp_reg_reg[17]_0\(10),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(10),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[10]_i_1_n_0\
    );
\ramplitude_min_reg[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(11),
      I1 => \ramp_reg_reg[17]_0\(11),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(11),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[11]_i_1_n_0\
    );
\ramplitude_min_reg[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[11]\,
      I1 => ramplitude_step_reg(11),
      O => \ramplitude_min_reg[11]_i_3_n_0\
    );
\ramplitude_min_reg[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[10]\,
      I1 => ramplitude_step_reg(10),
      O => \ramplitude_min_reg[11]_i_4_n_0\
    );
\ramplitude_min_reg[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[9]\,
      I1 => ramplitude_step_reg(9),
      O => \ramplitude_min_reg[11]_i_5_n_0\
    );
\ramplitude_min_reg[11]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[8]\,
      I1 => ramplitude_step_reg(8),
      O => \ramplitude_min_reg[11]_i_6_n_0\
    );
\ramplitude_min_reg[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(12),
      I1 => \ramp_reg_reg[17]_0\(12),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(12),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[12]_i_1_n_0\
    );
\ramplitude_min_reg[12]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(12),
      O => p_0_in(12)
    );
\ramplitude_min_reg[12]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(11),
      O => p_0_in(11)
    );
\ramplitude_min_reg[12]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(10),
      O => p_0_in(10)
    );
\ramplitude_min_reg[12]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(9),
      O => p_0_in(9)
    );
\ramplitude_min_reg[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(13),
      I1 => \ramp_reg_reg[17]_0\(13),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(13),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[13]_i_1_n_0\
    );
\ramplitude_min_reg[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(14),
      I1 => \ramp_reg_reg[17]_0\(14),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(14),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[14]_i_1_n_0\
    );
\ramplitude_min_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(15),
      I1 => \ramp_reg_reg[17]_0\(15),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(15),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[15]_i_1_n_0\
    );
\ramplitude_min_reg[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[15]\,
      I1 => ramplitude_step_reg(15),
      O => \ramplitude_min_reg[15]_i_3_n_0\
    );
\ramplitude_min_reg[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[14]\,
      I1 => ramplitude_step_reg(14),
      O => \ramplitude_min_reg[15]_i_4_n_0\
    );
\ramplitude_min_reg[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[13]\,
      I1 => ramplitude_step_reg(13),
      O => \ramplitude_min_reg[15]_i_5_n_0\
    );
\ramplitude_min_reg[15]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[12]\,
      I1 => ramplitude_step_reg(12),
      O => \ramplitude_min_reg[15]_i_6_n_0\
    );
\ramplitude_min_reg[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(16),
      I1 => \ramp_reg_reg[17]_0\(16),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(16),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[16]_i_1_n_0\
    );
\ramplitude_min_reg[16]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(16),
      O => p_0_in(16)
    );
\ramplitude_min_reg[16]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(15),
      O => p_0_in(15)
    );
\ramplitude_min_reg[16]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(14),
      O => p_0_in(14)
    );
\ramplitude_min_reg[16]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(13),
      O => p_0_in(13)
    );
\ramplitude_min_reg[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(17),
      I1 => \ramp_reg_reg[17]_0\(17),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(17),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[17]_i_1_n_0\
    );
\ramplitude_min_reg[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(18),
      I1 => ramp_module_0_reset(0),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(18),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[18]_i_1_n_0\
    );
\ramplitude_min_reg[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(19),
      I1 => ramp_module_0_reset(1),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(19),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[19]_i_1_n_0\
    );
\ramplitude_min_reg[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[19]\,
      I1 => ramplitude_step_reg(19),
      O => \ramplitude_min_reg[19]_i_3_n_0\
    );
\ramplitude_min_reg[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[18]\,
      I1 => ramplitude_step_reg(18),
      O => \ramplitude_min_reg[19]_i_4_n_0\
    );
\ramplitude_min_reg[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[17]\,
      I1 => ramplitude_step_reg(17),
      O => \ramplitude_min_reg[19]_i_5_n_0\
    );
\ramplitude_min_reg[19]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[16]\,
      I1 => ramplitude_step_reg(16),
      O => \ramplitude_min_reg[19]_i_6_n_0\
    );
\ramplitude_min_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(1),
      I1 => \ramp_reg_reg[17]_0\(1),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(1),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[1]_i_1_n_0\
    );
\ramplitude_min_reg[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(20),
      I1 => ramp_module_0_reset(2),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(20),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[20]_i_1_n_0\
    );
\ramplitude_min_reg[20]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(20),
      O => p_0_in(20)
    );
\ramplitude_min_reg[20]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(19),
      O => p_0_in(19)
    );
\ramplitude_min_reg[20]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(18),
      O => p_0_in(18)
    );
\ramplitude_min_reg[20]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(17),
      O => p_0_in(17)
    );
\ramplitude_min_reg[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(21),
      I1 => ramp_module_0_reset(3),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(21),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[21]_i_1_n_0\
    );
\ramplitude_min_reg[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(22),
      I1 => ramp_module_0_reset(4),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(22),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[22]_i_1_n_0\
    );
\ramplitude_min_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(23),
      I1 => ramp_module_0_reset(5),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(23),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[23]_i_1_n_0\
    );
\ramplitude_min_reg[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[23]\,
      I1 => ramplitude_step_reg(23),
      O => \ramplitude_min_reg[23]_i_3_n_0\
    );
\ramplitude_min_reg[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[22]\,
      I1 => ramplitude_step_reg(22),
      O => \ramplitude_min_reg[23]_i_4_n_0\
    );
\ramplitude_min_reg[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[21]\,
      I1 => ramplitude_step_reg(21),
      O => \ramplitude_min_reg[23]_i_5_n_0\
    );
\ramplitude_min_reg[23]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[20]\,
      I1 => ramplitude_step_reg(20),
      O => \ramplitude_min_reg[23]_i_6_n_0\
    );
\ramplitude_min_reg[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(24),
      I1 => ramp_module_0_reset(6),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(24),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[24]_i_1_n_0\
    );
\ramplitude_min_reg[24]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(24),
      O => p_0_in(24)
    );
\ramplitude_min_reg[24]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(23),
      O => p_0_in(23)
    );
\ramplitude_min_reg[24]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(22),
      O => p_0_in(22)
    );
\ramplitude_min_reg[24]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(21),
      O => p_0_in(21)
    );
\ramplitude_min_reg[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(25),
      I1 => ramp_module_0_reset(7),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(25),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[25]_i_1_n_0\
    );
\ramplitude_min_reg[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(26),
      I1 => ramp_module_0_reset(8),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(26),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[26]_i_1_n_0\
    );
\ramplitude_min_reg[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(27),
      I1 => ramp_module_0_reset(9),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(27),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[27]_i_1_n_0\
    );
\ramplitude_min_reg[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[27]\,
      I1 => ramplitude_step_reg(27),
      O => \ramplitude_min_reg[27]_i_3_n_0\
    );
\ramplitude_min_reg[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[26]\,
      I1 => ramplitude_step_reg(26),
      O => \ramplitude_min_reg[27]_i_4_n_0\
    );
\ramplitude_min_reg[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[25]\,
      I1 => ramplitude_step_reg(25),
      O => \ramplitude_min_reg[27]_i_5_n_0\
    );
\ramplitude_min_reg[27]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[24]\,
      I1 => ramplitude_step_reg(24),
      O => \ramplitude_min_reg[27]_i_6_n_0\
    );
\ramplitude_min_reg[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(28),
      I1 => ramp_module_0_reset(10),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(28),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[28]_i_1_n_0\
    );
\ramplitude_min_reg[28]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(28),
      O => p_0_in(28)
    );
\ramplitude_min_reg[28]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(27),
      O => p_0_in(27)
    );
\ramplitude_min_reg[28]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(26),
      O => p_0_in(26)
    );
\ramplitude_min_reg[28]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(25),
      O => p_0_in(25)
    );
\ramplitude_min_reg[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(29),
      I1 => ramp_module_0_reset(11),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(29),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[29]_i_1_n_0\
    );
\ramplitude_min_reg[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(2),
      I1 => \ramp_reg_reg[17]_0\(2),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(2),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[2]_i_1_n_0\
    );
\ramplitude_min_reg[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(30),
      I1 => ramp_module_0_reset(12),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(30),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[30]_i_1_n_0\
    );
\ramplitude_min_reg[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \^ramp_module_0_rst\,
      I1 => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      I2 => \ramplitude_min_reg_reg[31]_i_4_n_0\,
      O => \ramplitude_min_reg[31]_i_1_n_0\
    );
\ramplitude_min_reg[31]_i_100\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[15]\,
      I1 => data0(15),
      I2 => \ramplitude_min_reg_reg_n_0_[14]\,
      I3 => data0(14),
      O => \ramplitude_min_reg[31]_i_100_n_0\
    );
\ramplitude_min_reg[31]_i_101\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[13]\,
      I1 => data0(13),
      I2 => \ramplitude_min_reg_reg_n_0_[12]\,
      I3 => data0(12),
      O => \ramplitude_min_reg[31]_i_101_n_0\
    );
\ramplitude_min_reg[31]_i_102\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[11]\,
      I1 => data0(11),
      I2 => \ramplitude_min_reg_reg_n_0_[10]\,
      I3 => data0(10),
      O => \ramplitude_min_reg[31]_i_102_n_0\
    );
\ramplitude_min_reg[31]_i_103\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[9]\,
      I1 => data0(9),
      I2 => \ramplitude_min_reg_reg_n_0_[8]\,
      I3 => data0(8),
      O => \ramplitude_min_reg[31]_i_103_n_0\
    );
\ramplitude_min_reg[31]_i_104\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(15),
      I1 => \ramplitude_min_reg_reg_n_0_[15]\,
      I2 => data0(14),
      I3 => \ramplitude_min_reg_reg_n_0_[14]\,
      O => \ramplitude_min_reg[31]_i_104_n_0\
    );
\ramplitude_min_reg[31]_i_105\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(13),
      I1 => \ramplitude_min_reg_reg_n_0_[13]\,
      I2 => data0(12),
      I3 => \ramplitude_min_reg_reg_n_0_[12]\,
      O => \ramplitude_min_reg[31]_i_105_n_0\
    );
\ramplitude_min_reg[31]_i_106\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(11),
      I1 => \ramplitude_min_reg_reg_n_0_[11]\,
      I2 => data0(10),
      I3 => \ramplitude_min_reg_reg_n_0_[10]\,
      O => \ramplitude_min_reg[31]_i_106_n_0\
    );
\ramplitude_min_reg[31]_i_107\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(9),
      I1 => \ramplitude_min_reg_reg_n_0_[9]\,
      I2 => data0(8),
      I3 => \ramplitude_min_reg_reg_n_0_[8]\,
      O => \ramplitude_min_reg[31]_i_107_n_0\
    );
\ramplitude_min_reg[31]_i_108\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramp_reg(7),
      I1 => \ramplitude_max_reg_reg_n_0_[7]\,
      I2 => ramp_reg(6),
      I3 => \ramplitude_max_reg_reg_n_0_[6]\,
      O => \ramplitude_min_reg[31]_i_108_n_0\
    );
\ramplitude_min_reg[31]_i_109\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramp_reg(5),
      I1 => \ramplitude_max_reg_reg_n_0_[5]\,
      I2 => ramp_reg(4),
      I3 => \ramplitude_max_reg_reg_n_0_[4]\,
      O => \ramplitude_min_reg[31]_i_109_n_0\
    );
\ramplitude_min_reg[31]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^ramp_output\(13),
      I1 => \ramplitude_max_reg_reg_n_0_[31]\,
      I2 => \^ramp_output\(12),
      I3 => \ramplitude_max_reg_reg_n_0_[30]\,
      O => \ramplitude_min_reg[31]_i_11_n_0\
    );
\ramplitude_min_reg[31]_i_110\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramp_reg(3),
      I1 => \ramplitude_max_reg_reg_n_0_[3]\,
      I2 => ramp_reg(2),
      I3 => \ramplitude_max_reg_reg_n_0_[2]\,
      O => \ramplitude_min_reg[31]_i_110_n_0\
    );
\ramplitude_min_reg[31]_i_111\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramp_reg(1),
      I1 => \ramplitude_max_reg_reg_n_0_[1]\,
      I2 => ramp_reg(0),
      I3 => \ramplitude_max_reg_reg_n_0_[0]\,
      O => \ramplitude_min_reg[31]_i_111_n_0\
    );
\ramplitude_min_reg[31]_i_112\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[7]\,
      I1 => ramp_reg(7),
      I2 => \ramplitude_max_reg_reg_n_0_[6]\,
      I3 => ramp_reg(6),
      O => \ramplitude_min_reg[31]_i_112_n_0\
    );
\ramplitude_min_reg[31]_i_113\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[5]\,
      I1 => ramp_reg(5),
      I2 => \ramplitude_max_reg_reg_n_0_[4]\,
      I3 => ramp_reg(4),
      O => \ramplitude_min_reg[31]_i_113_n_0\
    );
\ramplitude_min_reg[31]_i_114\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[3]\,
      I1 => ramp_reg(3),
      I2 => \ramplitude_max_reg_reg_n_0_[2]\,
      I3 => ramp_reg(2),
      O => \ramplitude_min_reg[31]_i_114_n_0\
    );
\ramplitude_min_reg[31]_i_115\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[1]\,
      I1 => ramp_reg(1),
      I2 => \ramplitude_max_reg_reg_n_0_[0]\,
      I3 => ramp_reg(0),
      O => \ramplitude_min_reg[31]_i_115_n_0\
    );
\ramplitude_min_reg[31]_i_116\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[7]\,
      I1 => ramp_reg(7),
      I2 => \ramplitude_min_reg_reg_n_0_[6]\,
      I3 => ramp_reg(6),
      O => \ramplitude_min_reg[31]_i_116_n_0\
    );
\ramplitude_min_reg[31]_i_117\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[5]\,
      I1 => ramp_reg(5),
      I2 => \ramplitude_min_reg_reg_n_0_[4]\,
      I3 => ramp_reg(4),
      O => \ramplitude_min_reg[31]_i_117_n_0\
    );
\ramplitude_min_reg[31]_i_118\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[3]\,
      I1 => ramp_reg(3),
      I2 => \ramplitude_min_reg_reg_n_0_[2]\,
      I3 => ramp_reg(2),
      O => \ramplitude_min_reg[31]_i_118_n_0\
    );
\ramplitude_min_reg[31]_i_119\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[1]\,
      I1 => ramp_reg(1),
      I2 => \ramplitude_min_reg_reg_n_0_[0]\,
      I3 => ramp_reg(0),
      O => \ramplitude_min_reg[31]_i_119_n_0\
    );
\ramplitude_min_reg[31]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^ramp_output\(11),
      I1 => \ramplitude_max_reg_reg_n_0_[29]\,
      I2 => \^ramp_output\(10),
      I3 => \ramplitude_max_reg_reg_n_0_[28]\,
      O => \ramplitude_min_reg[31]_i_12_n_0\
    );
\ramplitude_min_reg[31]_i_120\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => ramp_reg(7),
      I1 => \ramplitude_min_reg_reg_n_0_[7]\,
      I2 => ramp_reg(6),
      I3 => \ramplitude_min_reg_reg_n_0_[6]\,
      O => \ramplitude_min_reg[31]_i_120_n_0\
    );
\ramplitude_min_reg[31]_i_121\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => ramp_reg(5),
      I1 => \ramplitude_min_reg_reg_n_0_[5]\,
      I2 => ramp_reg(4),
      I3 => \ramplitude_min_reg_reg_n_0_[4]\,
      O => \ramplitude_min_reg[31]_i_121_n_0\
    );
\ramplitude_min_reg[31]_i_122\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => ramp_reg(3),
      I1 => \ramplitude_min_reg_reg_n_0_[3]\,
      I2 => ramp_reg(2),
      I3 => \ramplitude_min_reg_reg_n_0_[2]\,
      O => \ramplitude_min_reg[31]_i_122_n_0\
    );
\ramplitude_min_reg[31]_i_123\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => ramp_reg(1),
      I1 => \ramplitude_min_reg_reg_n_0_[1]\,
      I2 => ramp_reg(0),
      I3 => \ramplitude_min_reg_reg_n_0_[0]\,
      O => \ramplitude_min_reg[31]_i_123_n_0\
    );
\ramplitude_min_reg[31]_i_124\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[7]\,
      I1 => data0(7),
      I2 => \ramplitude_min_reg_reg_n_0_[6]\,
      I3 => data0(6),
      O => \ramplitude_min_reg[31]_i_124_n_0\
    );
\ramplitude_min_reg[31]_i_125\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[5]\,
      I1 => data0(5),
      I2 => \ramplitude_min_reg_reg_n_0_[4]\,
      I3 => data0(4),
      O => \ramplitude_min_reg[31]_i_125_n_0\
    );
\ramplitude_min_reg[31]_i_126\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[3]\,
      I1 => data0(3),
      I2 => \ramplitude_min_reg_reg_n_0_[2]\,
      I3 => data0(2),
      O => \ramplitude_min_reg[31]_i_126_n_0\
    );
\ramplitude_min_reg[31]_i_127\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[1]\,
      I1 => data0(1),
      I2 => \ramplitude_min_reg_reg_n_0_[0]\,
      I3 => ramplitude_lim_reg(0),
      O => \ramplitude_min_reg[31]_i_127_n_0\
    );
\ramplitude_min_reg[31]_i_128\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(7),
      I1 => \ramplitude_min_reg_reg_n_0_[7]\,
      I2 => data0(6),
      I3 => \ramplitude_min_reg_reg_n_0_[6]\,
      O => \ramplitude_min_reg[31]_i_128_n_0\
    );
\ramplitude_min_reg[31]_i_129\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(5),
      I1 => \ramplitude_min_reg_reg_n_0_[5]\,
      I2 => data0(4),
      I3 => \ramplitude_min_reg_reg_n_0_[4]\,
      O => \ramplitude_min_reg[31]_i_129_n_0\
    );
\ramplitude_min_reg[31]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^ramp_output\(9),
      I1 => \ramplitude_max_reg_reg_n_0_[27]\,
      I2 => \^ramp_output\(8),
      I3 => \ramplitude_max_reg_reg_n_0_[26]\,
      O => \ramplitude_min_reg[31]_i_13_n_0\
    );
\ramplitude_min_reg[31]_i_130\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(3),
      I1 => \ramplitude_min_reg_reg_n_0_[3]\,
      I2 => data0(2),
      I3 => \ramplitude_min_reg_reg_n_0_[2]\,
      O => \ramplitude_min_reg[31]_i_130_n_0\
    );
\ramplitude_min_reg[31]_i_131\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(1),
      I1 => \ramplitude_min_reg_reg_n_0_[1]\,
      I2 => ramplitude_lim_reg(0),
      I3 => \ramplitude_min_reg_reg_n_0_[0]\,
      O => \ramplitude_min_reg[31]_i_131_n_0\
    );
\ramplitude_min_reg[31]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^ramp_output\(7),
      I1 => \ramplitude_max_reg_reg_n_0_[25]\,
      I2 => \^ramp_output\(6),
      I3 => \ramplitude_max_reg_reg_n_0_[24]\,
      O => \ramplitude_min_reg[31]_i_14_n_0\
    );
\ramplitude_min_reg[31]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[31]\,
      I1 => \^ramp_output\(13),
      I2 => \ramplitude_max_reg_reg_n_0_[30]\,
      I3 => \^ramp_output\(12),
      O => \ramplitude_min_reg[31]_i_15_n_0\
    );
\ramplitude_min_reg[31]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[29]\,
      I1 => \^ramp_output\(11),
      I2 => \ramplitude_max_reg_reg_n_0_[28]\,
      I3 => \^ramp_output\(10),
      O => \ramplitude_min_reg[31]_i_16_n_0\
    );
\ramplitude_min_reg[31]_i_17\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[27]\,
      I1 => \^ramp_output\(9),
      I2 => \ramplitude_max_reg_reg_n_0_[26]\,
      I3 => \^ramp_output\(8),
      O => \ramplitude_min_reg[31]_i_17_n_0\
    );
\ramplitude_min_reg[31]_i_18\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[25]\,
      I1 => \^ramp_output\(7),
      I2 => \ramplitude_max_reg_reg_n_0_[24]\,
      I3 => \^ramp_output\(6),
      O => \ramplitude_min_reg[31]_i_18_n_0\
    );
\ramplitude_min_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(31),
      I1 => ramp_module_0_reset(13),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(31),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[31]_i_2_n_0\
    );
\ramplitude_min_reg[31]_i_20\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^ramp_output\(13),
      I1 => \ramplitude_min_reg_reg_n_0_[31]\,
      I2 => \ramplitude_min_reg_reg_n_0_[30]\,
      I3 => \^ramp_output\(12),
      O => \ramplitude_min_reg[31]_i_20_n_0\
    );
\ramplitude_min_reg[31]_i_21\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[29]\,
      I1 => \^ramp_output\(11),
      I2 => \ramplitude_min_reg_reg_n_0_[28]\,
      I3 => \^ramp_output\(10),
      O => \ramplitude_min_reg[31]_i_21_n_0\
    );
\ramplitude_min_reg[31]_i_22\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[27]\,
      I1 => \^ramp_output\(9),
      I2 => \ramplitude_min_reg_reg_n_0_[26]\,
      I3 => \^ramp_output\(8),
      O => \ramplitude_min_reg[31]_i_22_n_0\
    );
\ramplitude_min_reg[31]_i_23\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[25]\,
      I1 => \^ramp_output\(7),
      I2 => \ramplitude_min_reg_reg_n_0_[24]\,
      I3 => \^ramp_output\(6),
      O => \ramplitude_min_reg[31]_i_23_n_0\
    );
\ramplitude_min_reg[31]_i_24\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[31]\,
      I1 => \^ramp_output\(13),
      I2 => \^ramp_output\(12),
      I3 => \ramplitude_min_reg_reg_n_0_[30]\,
      O => \ramplitude_min_reg[31]_i_24_n_0\
    );
\ramplitude_min_reg[31]_i_25\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^ramp_output\(11),
      I1 => \ramplitude_min_reg_reg_n_0_[29]\,
      I2 => \^ramp_output\(10),
      I3 => \ramplitude_min_reg_reg_n_0_[28]\,
      O => \ramplitude_min_reg[31]_i_25_n_0\
    );
\ramplitude_min_reg[31]_i_26\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^ramp_output\(9),
      I1 => \ramplitude_min_reg_reg_n_0_[27]\,
      I2 => \^ramp_output\(8),
      I3 => \ramplitude_min_reg_reg_n_0_[26]\,
      O => \ramplitude_min_reg[31]_i_26_n_0\
    );
\ramplitude_min_reg[31]_i_27\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^ramp_output\(7),
      I1 => \ramplitude_min_reg_reg_n_0_[25]\,
      I2 => \^ramp_output\(6),
      I3 => \ramplitude_min_reg_reg_n_0_[24]\,
      O => \ramplitude_min_reg[31]_i_27_n_0\
    );
\ramplitude_min_reg[31]_i_28\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(31),
      O => \ramplitude_min_reg[31]_i_28_n_0\
    );
\ramplitude_min_reg[31]_i_29\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(30),
      O => p_0_in(30)
    );
\ramplitude_min_reg[31]_i_30\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(29),
      O => p_0_in(29)
    );
\ramplitude_min_reg[31]_i_34\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => data0(31),
      I1 => \ramplitude_min_reg_reg_n_0_[31]\,
      I2 => \ramplitude_min_reg_reg_n_0_[30]\,
      I3 => data0(30),
      O => \ramplitude_min_reg[31]_i_34_n_0\
    );
\ramplitude_min_reg[31]_i_35\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[29]\,
      I1 => data0(29),
      I2 => \ramplitude_min_reg_reg_n_0_[28]\,
      I3 => data0(28),
      O => \ramplitude_min_reg[31]_i_35_n_0\
    );
\ramplitude_min_reg[31]_i_36\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[27]\,
      I1 => data0(27),
      I2 => \ramplitude_min_reg_reg_n_0_[26]\,
      I3 => data0(26),
      O => \ramplitude_min_reg[31]_i_36_n_0\
    );
\ramplitude_min_reg[31]_i_37\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[25]\,
      I1 => data0(25),
      I2 => \ramplitude_min_reg_reg_n_0_[24]\,
      I3 => data0(24),
      O => \ramplitude_min_reg[31]_i_37_n_0\
    );
\ramplitude_min_reg[31]_i_38\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[31]\,
      I1 => data0(31),
      I2 => data0(30),
      I3 => \ramplitude_min_reg_reg_n_0_[30]\,
      O => \ramplitude_min_reg[31]_i_38_n_0\
    );
\ramplitude_min_reg[31]_i_39\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(29),
      I1 => \ramplitude_min_reg_reg_n_0_[29]\,
      I2 => data0(28),
      I3 => \ramplitude_min_reg_reg_n_0_[28]\,
      O => \ramplitude_min_reg[31]_i_39_n_0\
    );
\ramplitude_min_reg[31]_i_40\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(27),
      I1 => \ramplitude_min_reg_reg_n_0_[27]\,
      I2 => data0(26),
      I3 => \ramplitude_min_reg_reg_n_0_[26]\,
      O => \ramplitude_min_reg[31]_i_40_n_0\
    );
\ramplitude_min_reg[31]_i_41\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(25),
      I1 => \ramplitude_min_reg_reg_n_0_[25]\,
      I2 => data0(24),
      I3 => \ramplitude_min_reg_reg_n_0_[24]\,
      O => \ramplitude_min_reg[31]_i_41_n_0\
    );
\ramplitude_min_reg[31]_i_42\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[31]\,
      I1 => ramplitude_step_reg(31),
      O => \ramplitude_min_reg[31]_i_42_n_0\
    );
\ramplitude_min_reg[31]_i_43\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[30]\,
      I1 => ramplitude_step_reg(30),
      O => \ramplitude_min_reg[31]_i_43_n_0\
    );
\ramplitude_min_reg[31]_i_44\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[29]\,
      I1 => ramplitude_step_reg(29),
      O => \ramplitude_min_reg[31]_i_44_n_0\
    );
\ramplitude_min_reg[31]_i_45\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[28]\,
      I1 => ramplitude_step_reg(28),
      O => \ramplitude_min_reg[31]_i_45_n_0\
    );
\ramplitude_min_reg[31]_i_46\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => ramplitude_step_reg(10),
      I1 => ramplitude_step_reg(9),
      I2 => ramplitude_step_reg(11),
      I3 => ramplitude_step_reg(8),
      I4 => \ramplitude_min_reg[31]_i_77_n_0\,
      O => \ramplitude_min_reg[31]_i_46_n_0\
    );
\ramplitude_min_reg[31]_i_47\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => ramplitude_step_reg(30),
      I1 => ramplitude_step_reg(29),
      I2 => ramplitude_step_reg(31),
      I3 => ramplitude_step_reg(28),
      I4 => \ramplitude_min_reg[31]_i_78_n_0\,
      O => \ramplitude_min_reg[31]_i_47_n_0\
    );
\ramplitude_min_reg[31]_i_48\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => ramplitude_step_reg(18),
      I1 => ramplitude_step_reg(17),
      I2 => ramplitude_step_reg(19),
      I3 => ramplitude_step_reg(16),
      I4 => \ramplitude_min_reg[31]_i_79_n_0\,
      O => \ramplitude_min_reg[31]_i_48_n_0\
    );
\ramplitude_min_reg[31]_i_49\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => ramplitude_step_reg(6),
      I1 => ramplitude_step_reg(5),
      I2 => ramplitude_step_reg(7),
      I3 => ramplitude_step_reg(4),
      I4 => \ramplitude_min_reg[31]_i_80_n_0\,
      O => \ramplitude_min_reg[31]_i_49_n_0\
    );
\ramplitude_min_reg[31]_i_51\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^ramp_output\(5),
      I1 => \ramplitude_max_reg_reg_n_0_[23]\,
      I2 => \^ramp_output\(4),
      I3 => \ramplitude_max_reg_reg_n_0_[22]\,
      O => \ramplitude_min_reg[31]_i_51_n_0\
    );
\ramplitude_min_reg[31]_i_52\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^ramp_output\(3),
      I1 => \ramplitude_max_reg_reg_n_0_[21]\,
      I2 => \^ramp_output\(2),
      I3 => \ramplitude_max_reg_reg_n_0_[20]\,
      O => \ramplitude_min_reg[31]_i_52_n_0\
    );
\ramplitude_min_reg[31]_i_53\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^ramp_output\(1),
      I1 => \ramplitude_max_reg_reg_n_0_[19]\,
      I2 => \^ramp_output\(0),
      I3 => \ramplitude_max_reg_reg_n_0_[18]\,
      O => \ramplitude_min_reg[31]_i_53_n_0\
    );
\ramplitude_min_reg[31]_i_54\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramp_reg(17),
      I1 => \ramplitude_max_reg_reg_n_0_[17]\,
      I2 => ramp_reg(16),
      I3 => \ramplitude_max_reg_reg_n_0_[16]\,
      O => \ramplitude_min_reg[31]_i_54_n_0\
    );
\ramplitude_min_reg[31]_i_55\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[23]\,
      I1 => \^ramp_output\(5),
      I2 => \ramplitude_max_reg_reg_n_0_[22]\,
      I3 => \^ramp_output\(4),
      O => \ramplitude_min_reg[31]_i_55_n_0\
    );
\ramplitude_min_reg[31]_i_56\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[21]\,
      I1 => \^ramp_output\(3),
      I2 => \ramplitude_max_reg_reg_n_0_[20]\,
      I3 => \^ramp_output\(2),
      O => \ramplitude_min_reg[31]_i_56_n_0\
    );
\ramplitude_min_reg[31]_i_57\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[19]\,
      I1 => \^ramp_output\(1),
      I2 => \ramplitude_max_reg_reg_n_0_[18]\,
      I3 => \^ramp_output\(0),
      O => \ramplitude_min_reg[31]_i_57_n_0\
    );
\ramplitude_min_reg[31]_i_58\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[17]\,
      I1 => ramp_reg(17),
      I2 => \ramplitude_max_reg_reg_n_0_[16]\,
      I3 => ramp_reg(16),
      O => \ramplitude_min_reg[31]_i_58_n_0\
    );
\ramplitude_min_reg[31]_i_60\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[23]\,
      I1 => \^ramp_output\(5),
      I2 => \ramplitude_min_reg_reg_n_0_[22]\,
      I3 => \^ramp_output\(4),
      O => \ramplitude_min_reg[31]_i_60_n_0\
    );
\ramplitude_min_reg[31]_i_61\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[21]\,
      I1 => \^ramp_output\(3),
      I2 => \ramplitude_min_reg_reg_n_0_[20]\,
      I3 => \^ramp_output\(2),
      O => \ramplitude_min_reg[31]_i_61_n_0\
    );
\ramplitude_min_reg[31]_i_62\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[19]\,
      I1 => \^ramp_output\(1),
      I2 => \ramplitude_min_reg_reg_n_0_[18]\,
      I3 => \^ramp_output\(0),
      O => \ramplitude_min_reg[31]_i_62_n_0\
    );
\ramplitude_min_reg[31]_i_63\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[17]\,
      I1 => ramp_reg(17),
      I2 => \ramplitude_min_reg_reg_n_0_[16]\,
      I3 => ramp_reg(16),
      O => \ramplitude_min_reg[31]_i_63_n_0\
    );
\ramplitude_min_reg[31]_i_64\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^ramp_output\(5),
      I1 => \ramplitude_min_reg_reg_n_0_[23]\,
      I2 => \^ramp_output\(4),
      I3 => \ramplitude_min_reg_reg_n_0_[22]\,
      O => \ramplitude_min_reg[31]_i_64_n_0\
    );
\ramplitude_min_reg[31]_i_65\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^ramp_output\(3),
      I1 => \ramplitude_min_reg_reg_n_0_[21]\,
      I2 => \^ramp_output\(2),
      I3 => \ramplitude_min_reg_reg_n_0_[20]\,
      O => \ramplitude_min_reg[31]_i_65_n_0\
    );
\ramplitude_min_reg[31]_i_66\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^ramp_output\(1),
      I1 => \ramplitude_min_reg_reg_n_0_[19]\,
      I2 => \^ramp_output\(0),
      I3 => \ramplitude_min_reg_reg_n_0_[18]\,
      O => \ramplitude_min_reg[31]_i_66_n_0\
    );
\ramplitude_min_reg[31]_i_67\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => ramp_reg(17),
      I1 => \ramplitude_min_reg_reg_n_0_[17]\,
      I2 => ramp_reg(16),
      I3 => \ramplitude_min_reg_reg_n_0_[16]\,
      O => \ramplitude_min_reg[31]_i_67_n_0\
    );
\ramplitude_min_reg[31]_i_69\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[23]\,
      I1 => data0(23),
      I2 => \ramplitude_min_reg_reg_n_0_[22]\,
      I3 => data0(22),
      O => \ramplitude_min_reg[31]_i_69_n_0\
    );
\ramplitude_min_reg[31]_i_70\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[21]\,
      I1 => data0(21),
      I2 => \ramplitude_min_reg_reg_n_0_[20]\,
      I3 => data0(20),
      O => \ramplitude_min_reg[31]_i_70_n_0\
    );
\ramplitude_min_reg[31]_i_71\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[19]\,
      I1 => data0(19),
      I2 => \ramplitude_min_reg_reg_n_0_[18]\,
      I3 => data0(18),
      O => \ramplitude_min_reg[31]_i_71_n_0\
    );
\ramplitude_min_reg[31]_i_72\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[17]\,
      I1 => data0(17),
      I2 => \ramplitude_min_reg_reg_n_0_[16]\,
      I3 => data0(16),
      O => \ramplitude_min_reg[31]_i_72_n_0\
    );
\ramplitude_min_reg[31]_i_73\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(23),
      I1 => \ramplitude_min_reg_reg_n_0_[23]\,
      I2 => data0(22),
      I3 => \ramplitude_min_reg_reg_n_0_[22]\,
      O => \ramplitude_min_reg[31]_i_73_n_0\
    );
\ramplitude_min_reg[31]_i_74\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(21),
      I1 => \ramplitude_min_reg_reg_n_0_[21]\,
      I2 => data0(20),
      I3 => \ramplitude_min_reg_reg_n_0_[20]\,
      O => \ramplitude_min_reg[31]_i_74_n_0\
    );
\ramplitude_min_reg[31]_i_75\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(19),
      I1 => \ramplitude_min_reg_reg_n_0_[19]\,
      I2 => data0(18),
      I3 => \ramplitude_min_reg_reg_n_0_[18]\,
      O => \ramplitude_min_reg[31]_i_75_n_0\
    );
\ramplitude_min_reg[31]_i_76\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => data0(17),
      I1 => \ramplitude_min_reg_reg_n_0_[17]\,
      I2 => data0(16),
      I3 => \ramplitude_min_reg_reg_n_0_[16]\,
      O => \ramplitude_min_reg[31]_i_76_n_0\
    );
\ramplitude_min_reg[31]_i_77\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => ramplitude_step_reg(24),
      I1 => ramplitude_step_reg(27),
      I2 => ramplitude_step_reg(25),
      I3 => ramplitude_step_reg(26),
      O => \ramplitude_min_reg[31]_i_77_n_0\
    );
\ramplitude_min_reg[31]_i_78\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => ramplitude_step_reg(12),
      I1 => ramplitude_step_reg(15),
      I2 => ramplitude_step_reg(13),
      I3 => ramplitude_step_reg(14),
      O => \ramplitude_min_reg[31]_i_78_n_0\
    );
\ramplitude_min_reg[31]_i_79\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => ramplitude_step_reg(0),
      I1 => ramplitude_step_reg(3),
      I2 => ramplitude_step_reg(1),
      I3 => ramplitude_step_reg(2),
      O => \ramplitude_min_reg[31]_i_79_n_0\
    );
\ramplitude_min_reg[31]_i_80\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => ramplitude_step_reg(20),
      I1 => ramplitude_step_reg(23),
      I2 => ramplitude_step_reg(21),
      I3 => ramplitude_step_reg(22),
      O => \ramplitude_min_reg[31]_i_80_n_0\
    );
\ramplitude_min_reg[31]_i_82\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramp_reg(15),
      I1 => \ramplitude_max_reg_reg_n_0_[15]\,
      I2 => ramp_reg(14),
      I3 => \ramplitude_max_reg_reg_n_0_[14]\,
      O => \ramplitude_min_reg[31]_i_82_n_0\
    );
\ramplitude_min_reg[31]_i_83\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramp_reg(13),
      I1 => \ramplitude_max_reg_reg_n_0_[13]\,
      I2 => ramp_reg(12),
      I3 => \ramplitude_max_reg_reg_n_0_[12]\,
      O => \ramplitude_min_reg[31]_i_83_n_0\
    );
\ramplitude_min_reg[31]_i_84\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramp_reg(11),
      I1 => \ramplitude_max_reg_reg_n_0_[11]\,
      I2 => ramp_reg(10),
      I3 => \ramplitude_max_reg_reg_n_0_[10]\,
      O => \ramplitude_min_reg[31]_i_84_n_0\
    );
\ramplitude_min_reg[31]_i_85\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => ramp_reg(9),
      I1 => \ramplitude_max_reg_reg_n_0_[9]\,
      I2 => ramp_reg(8),
      I3 => \ramplitude_max_reg_reg_n_0_[8]\,
      O => \ramplitude_min_reg[31]_i_85_n_0\
    );
\ramplitude_min_reg[31]_i_86\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[15]\,
      I1 => ramp_reg(15),
      I2 => \ramplitude_max_reg_reg_n_0_[14]\,
      I3 => ramp_reg(14),
      O => \ramplitude_min_reg[31]_i_86_n_0\
    );
\ramplitude_min_reg[31]_i_87\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[13]\,
      I1 => ramp_reg(13),
      I2 => \ramplitude_max_reg_reg_n_0_[12]\,
      I3 => ramp_reg(12),
      O => \ramplitude_min_reg[31]_i_87_n_0\
    );
\ramplitude_min_reg[31]_i_88\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[11]\,
      I1 => ramp_reg(11),
      I2 => \ramplitude_max_reg_reg_n_0_[10]\,
      I3 => ramp_reg(10),
      O => \ramplitude_min_reg[31]_i_88_n_0\
    );
\ramplitude_min_reg[31]_i_89\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \ramplitude_max_reg_reg_n_0_[9]\,
      I1 => ramp_reg(9),
      I2 => \ramplitude_max_reg_reg_n_0_[8]\,
      I3 => ramp_reg(8),
      O => \ramplitude_min_reg[31]_i_89_n_0\
    );
\ramplitude_min_reg[31]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \ramplitude_min_reg[31]_i_46_n_0\,
      I1 => \ramplitude_min_reg[31]_i_47_n_0\,
      I2 => \ramplitude_min_reg[31]_i_48_n_0\,
      I3 => \ramplitude_min_reg[31]_i_49_n_0\,
      O => \ramplitude_min_reg[31]_i_9_n_0\
    );
\ramplitude_min_reg[31]_i_91\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[15]\,
      I1 => ramp_reg(15),
      I2 => \ramplitude_min_reg_reg_n_0_[14]\,
      I3 => ramp_reg(14),
      O => \ramplitude_min_reg[31]_i_91_n_0\
    );
\ramplitude_min_reg[31]_i_92\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[13]\,
      I1 => ramp_reg(13),
      I2 => \ramplitude_min_reg_reg_n_0_[12]\,
      I3 => ramp_reg(12),
      O => \ramplitude_min_reg[31]_i_92_n_0\
    );
\ramplitude_min_reg[31]_i_93\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[11]\,
      I1 => ramp_reg(11),
      I2 => \ramplitude_min_reg_reg_n_0_[10]\,
      I3 => ramp_reg(10),
      O => \ramplitude_min_reg[31]_i_93_n_0\
    );
\ramplitude_min_reg[31]_i_94\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[9]\,
      I1 => ramp_reg(9),
      I2 => \ramplitude_min_reg_reg_n_0_[8]\,
      I3 => ramp_reg(8),
      O => \ramplitude_min_reg[31]_i_94_n_0\
    );
\ramplitude_min_reg[31]_i_95\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => ramp_reg(15),
      I1 => \ramplitude_min_reg_reg_n_0_[15]\,
      I2 => ramp_reg(14),
      I3 => \ramplitude_min_reg_reg_n_0_[14]\,
      O => \ramplitude_min_reg[31]_i_95_n_0\
    );
\ramplitude_min_reg[31]_i_96\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => ramp_reg(13),
      I1 => \ramplitude_min_reg_reg_n_0_[13]\,
      I2 => ramp_reg(12),
      I3 => \ramplitude_min_reg_reg_n_0_[12]\,
      O => \ramplitude_min_reg[31]_i_96_n_0\
    );
\ramplitude_min_reg[31]_i_97\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => ramp_reg(11),
      I1 => \ramplitude_min_reg_reg_n_0_[11]\,
      I2 => ramp_reg(10),
      I3 => \ramplitude_min_reg_reg_n_0_[10]\,
      O => \ramplitude_min_reg[31]_i_97_n_0\
    );
\ramplitude_min_reg[31]_i_98\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => ramp_reg(9),
      I1 => \ramplitude_min_reg_reg_n_0_[9]\,
      I2 => ramp_reg(8),
      I3 => \ramplitude_min_reg_reg_n_0_[8]\,
      O => \ramplitude_min_reg[31]_i_98_n_0\
    );
\ramplitude_min_reg[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(3),
      I1 => \ramp_reg_reg[17]_0\(3),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(3),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[3]_i_1_n_0\
    );
\ramplitude_min_reg[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[3]\,
      I1 => ramplitude_step_reg(3),
      O => \ramplitude_min_reg[3]_i_3_n_0\
    );
\ramplitude_min_reg[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[2]\,
      I1 => ramplitude_step_reg(2),
      O => \ramplitude_min_reg[3]_i_4_n_0\
    );
\ramplitude_min_reg[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[1]\,
      I1 => ramplitude_step_reg(1),
      O => \ramplitude_min_reg[3]_i_5_n_0\
    );
\ramplitude_min_reg[3]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[0]\,
      I1 => ramplitude_step_reg(0),
      O => \ramplitude_min_reg[3]_i_6_n_0\
    );
\ramplitude_min_reg[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(4),
      I1 => \ramp_reg_reg[17]_0\(4),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(4),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[4]_i_1_n_0\
    );
\ramplitude_min_reg[4]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(0),
      O => p_0_in(0)
    );
\ramplitude_min_reg[4]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(4),
      O => p_0_in(4)
    );
\ramplitude_min_reg[4]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(3),
      O => p_0_in(3)
    );
\ramplitude_min_reg[4]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(2),
      O => p_0_in(2)
    );
\ramplitude_min_reg[4]_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(1),
      O => p_0_in(1)
    );
\ramplitude_min_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(5),
      I1 => \ramp_reg_reg[17]_0\(5),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(5),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[5]_i_1_n_0\
    );
\ramplitude_min_reg[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(6),
      I1 => \ramp_reg_reg[17]_0\(6),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(6),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[6]_i_1_n_0\
    );
\ramplitude_min_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(7),
      I1 => \ramp_reg_reg[17]_0\(7),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(7),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[7]_i_1_n_0\
    );
\ramplitude_min_reg[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[7]\,
      I1 => ramplitude_step_reg(7),
      O => \ramplitude_min_reg[7]_i_3_n_0\
    );
\ramplitude_min_reg[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[6]\,
      I1 => ramplitude_step_reg(6),
      O => \ramplitude_min_reg[7]_i_4_n_0\
    );
\ramplitude_min_reg[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[5]\,
      I1 => ramplitude_step_reg(5),
      O => \ramplitude_min_reg[7]_i_5_n_0\
    );
\ramplitude_min_reg[7]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ramplitude_min_reg_reg_n_0_[4]\,
      I1 => ramplitude_step_reg(4),
      O => \ramplitude_min_reg[7]_i_6_n_0\
    );
\ramplitude_min_reg[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(8),
      I1 => \ramp_reg_reg[17]_0\(8),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(8),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[8]_i_1_n_0\
    );
\ramplitude_min_reg[8]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(8),
      O => p_0_in(8)
    );
\ramplitude_min_reg[8]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(7),
      O => p_0_in(7)
    );
\ramplitude_min_reg[8]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(6),
      O => p_0_in(6)
    );
\ramplitude_min_reg[8]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ramplitude_lim_reg(5),
      O => p_0_in(5)
    );
\ramplitude_min_reg[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCAC0CAAAAAAAAA"
    )
        port map (
      I0 => data0(9),
      I1 => \ramp_reg_reg[17]_0\(9),
      I2 => \^ramp_module_0_rst\,
      I3 => ramplitude_min_reg1,
      I4 => ramplitude_min_reg0(9),
      I5 => \ramplitude_min_reg[31]_i_9_n_0\,
      O => \ramplitude_min_reg[9]_i_1_n_0\
    );
\ramplitude_min_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[0]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[0]\,
      R => '0'
    );
\ramplitude_min_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[10]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[10]\,
      R => '0'
    );
\ramplitude_min_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[11]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[11]\,
      R => '0'
    );
\ramplitude_min_reg_reg[11]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[7]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[11]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[11]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[11]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[11]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg_reg_n_0_[11]\,
      DI(2) => \ramplitude_min_reg_reg_n_0_[10]\,
      DI(1) => \ramplitude_min_reg_reg_n_0_[9]\,
      DI(0) => \ramplitude_min_reg_reg_n_0_[8]\,
      O(3 downto 0) => ramplitude_min_reg0(11 downto 8),
      S(3) => \ramplitude_min_reg[11]_i_3_n_0\,
      S(2) => \ramplitude_min_reg[11]_i_4_n_0\,
      S(1) => \ramplitude_min_reg[11]_i_5_n_0\,
      S(0) => \ramplitude_min_reg[11]_i_6_n_0\
    );
\ramplitude_min_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[12]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[12]\,
      R => '0'
    );
\ramplitude_min_reg_reg[12]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[8]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[12]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[12]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[12]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[12]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => p_0_in(12 downto 9)
    );
\ramplitude_min_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[13]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[13]\,
      R => '0'
    );
\ramplitude_min_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[14]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[14]\,
      R => '0'
    );
\ramplitude_min_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[15]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[15]\,
      R => '0'
    );
\ramplitude_min_reg_reg[15]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[11]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[15]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[15]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[15]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[15]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg_reg_n_0_[15]\,
      DI(2) => \ramplitude_min_reg_reg_n_0_[14]\,
      DI(1) => \ramplitude_min_reg_reg_n_0_[13]\,
      DI(0) => \ramplitude_min_reg_reg_n_0_[12]\,
      O(3 downto 0) => ramplitude_min_reg0(15 downto 12),
      S(3) => \ramplitude_min_reg[15]_i_3_n_0\,
      S(2) => \ramplitude_min_reg[15]_i_4_n_0\,
      S(1) => \ramplitude_min_reg[15]_i_5_n_0\,
      S(0) => \ramplitude_min_reg[15]_i_6_n_0\
    );
\ramplitude_min_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[16]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[16]\,
      R => '0'
    );
\ramplitude_min_reg_reg[16]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[12]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[16]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[16]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[16]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[16]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(16 downto 13),
      S(3 downto 0) => p_0_in(16 downto 13)
    );
\ramplitude_min_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[17]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[17]\,
      R => '0'
    );
\ramplitude_min_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[18]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[18]\,
      R => '0'
    );
\ramplitude_min_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[19]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[19]\,
      R => '0'
    );
\ramplitude_min_reg_reg[19]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[15]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[19]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[19]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[19]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[19]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg_reg_n_0_[19]\,
      DI(2) => \ramplitude_min_reg_reg_n_0_[18]\,
      DI(1) => \ramplitude_min_reg_reg_n_0_[17]\,
      DI(0) => \ramplitude_min_reg_reg_n_0_[16]\,
      O(3 downto 0) => ramplitude_min_reg0(19 downto 16),
      S(3) => \ramplitude_min_reg[19]_i_3_n_0\,
      S(2) => \ramplitude_min_reg[19]_i_4_n_0\,
      S(1) => \ramplitude_min_reg[19]_i_5_n_0\,
      S(0) => \ramplitude_min_reg[19]_i_6_n_0\
    );
\ramplitude_min_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[1]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[1]\,
      R => '0'
    );
\ramplitude_min_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[20]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[20]\,
      R => '0'
    );
\ramplitude_min_reg_reg[20]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[16]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[20]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[20]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[20]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[20]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(20 downto 17),
      S(3 downto 0) => p_0_in(20 downto 17)
    );
\ramplitude_min_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[21]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[21]\,
      R => '0'
    );
\ramplitude_min_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[22]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[22]\,
      R => '0'
    );
\ramplitude_min_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[23]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[23]\,
      R => '0'
    );
\ramplitude_min_reg_reg[23]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[19]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[23]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[23]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[23]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[23]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg_reg_n_0_[23]\,
      DI(2) => \ramplitude_min_reg_reg_n_0_[22]\,
      DI(1) => \ramplitude_min_reg_reg_n_0_[21]\,
      DI(0) => \ramplitude_min_reg_reg_n_0_[20]\,
      O(3 downto 0) => ramplitude_min_reg0(23 downto 20),
      S(3) => \ramplitude_min_reg[23]_i_3_n_0\,
      S(2) => \ramplitude_min_reg[23]_i_4_n_0\,
      S(1) => \ramplitude_min_reg[23]_i_5_n_0\,
      S(0) => \ramplitude_min_reg[23]_i_6_n_0\
    );
\ramplitude_min_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[24]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[24]\,
      R => '0'
    );
\ramplitude_min_reg_reg[24]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[20]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[24]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[24]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[24]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[24]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(24 downto 21),
      S(3 downto 0) => p_0_in(24 downto 21)
    );
\ramplitude_min_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[25]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[25]\,
      R => '0'
    );
\ramplitude_min_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[26]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[26]\,
      R => '0'
    );
\ramplitude_min_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[27]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[27]\,
      R => '0'
    );
\ramplitude_min_reg_reg[27]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[23]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[27]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[27]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[27]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[27]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg_reg_n_0_[27]\,
      DI(2) => \ramplitude_min_reg_reg_n_0_[26]\,
      DI(1) => \ramplitude_min_reg_reg_n_0_[25]\,
      DI(0) => \ramplitude_min_reg_reg_n_0_[24]\,
      O(3 downto 0) => ramplitude_min_reg0(27 downto 24),
      S(3) => \ramplitude_min_reg[27]_i_3_n_0\,
      S(2) => \ramplitude_min_reg[27]_i_4_n_0\,
      S(1) => \ramplitude_min_reg[27]_i_5_n_0\,
      S(0) => \ramplitude_min_reg[27]_i_6_n_0\
    );
\ramplitude_min_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[28]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[28]\,
      R => '0'
    );
\ramplitude_min_reg_reg[28]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[24]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[28]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[28]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[28]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[28]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(28 downto 25),
      S(3 downto 0) => p_0_in(28 downto 25)
    );
\ramplitude_min_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[29]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[29]\,
      R => '0'
    );
\ramplitude_min_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[2]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[2]\,
      R => '0'
    );
\ramplitude_min_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[30]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[30]\,
      R => '0'
    );
\ramplitude_min_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[31]_i_2_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[31]\,
      R => '0'
    );
\ramplitude_min_reg_reg[31]_i_10\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[31]_i_50_n_0\,
      CO(3) => \ramplitude_min_reg_reg[31]_i_10_n_0\,
      CO(2) => \ramplitude_min_reg_reg[31]_i_10_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_10_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_10_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg[31]_i_51_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_52_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_53_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_54_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_10_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_55_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_56_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_57_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_58_n_0\
    );
\ramplitude_min_reg_reg[31]_i_19\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[31]_i_59_n_0\,
      CO(3) => \ramplitude_min_reg_reg[31]_i_19_n_0\,
      CO(2) => \ramplitude_min_reg_reg[31]_i_19_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_19_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_19_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg[31]_i_60_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_61_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_62_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_63_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_19_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_64_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_65_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_66_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_67_n_0\
    );
\ramplitude_min_reg_reg[31]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[31]_i_10_n_0\,
      CO(3) => \ramplitude_min_reg_reg[31]_i_3_n_0\,
      CO(2) => \ramplitude_min_reg_reg[31]_i_3_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_3_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_3_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg[31]_i_11_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_12_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_13_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_14_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_3_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_15_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_16_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_17_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_18_n_0\
    );
\ramplitude_min_reg_reg[31]_i_33\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[31]_i_68_n_0\,
      CO(3) => \ramplitude_min_reg_reg[31]_i_33_n_0\,
      CO(2) => \ramplitude_min_reg_reg[31]_i_33_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_33_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_33_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg[31]_i_69_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_70_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_71_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_72_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_33_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_73_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_74_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_75_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_76_n_0\
    );
\ramplitude_min_reg_reg[31]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[31]_i_19_n_0\,
      CO(3) => \ramplitude_min_reg_reg[31]_i_4_n_0\,
      CO(2) => \ramplitude_min_reg_reg[31]_i_4_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_4_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_4_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg[31]_i_20_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_21_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_22_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_23_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_4_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_24_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_25_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_26_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_27_n_0\
    );
\ramplitude_min_reg_reg[31]_i_5\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[28]_i_2_n_0\,
      CO(3 downto 2) => \NLW_ramplitude_min_reg_reg[31]_i_5_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \ramplitude_min_reg_reg[31]_i_5_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_ramplitude_min_reg_reg[31]_i_5_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(31 downto 29),
      S(3) => '0',
      S(2) => \ramplitude_min_reg[31]_i_28_n_0\,
      S(1 downto 0) => p_0_in(30 downto 29)
    );
\ramplitude_min_reg_reg[31]_i_50\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[31]_i_81_n_0\,
      CO(3) => \ramplitude_min_reg_reg[31]_i_50_n_0\,
      CO(2) => \ramplitude_min_reg_reg[31]_i_50_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_50_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_50_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg[31]_i_82_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_83_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_84_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_85_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_50_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_86_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_87_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_88_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_89_n_0\
    );
\ramplitude_min_reg_reg[31]_i_59\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[31]_i_90_n_0\,
      CO(3) => \ramplitude_min_reg_reg[31]_i_59_n_0\,
      CO(2) => \ramplitude_min_reg_reg[31]_i_59_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_59_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_59_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg[31]_i_91_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_92_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_93_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_94_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_59_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_95_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_96_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_97_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_98_n_0\
    );
\ramplitude_min_reg_reg[31]_i_68\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[31]_i_99_n_0\,
      CO(3) => \ramplitude_min_reg_reg[31]_i_68_n_0\,
      CO(2) => \ramplitude_min_reg_reg[31]_i_68_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_68_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_68_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg[31]_i_100_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_101_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_102_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_103_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_68_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_104_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_105_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_106_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_107_n_0\
    );
\ramplitude_min_reg_reg[31]_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[31]_i_33_n_0\,
      CO(3) => ramplitude_min_reg1,
      CO(2) => \ramplitude_min_reg_reg[31]_i_7_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_7_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_7_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg[31]_i_34_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_35_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_36_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_37_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_7_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_38_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_39_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_40_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_41_n_0\
    );
\ramplitude_min_reg_reg[31]_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[27]_i_2_n_0\,
      CO(3) => \NLW_ramplitude_min_reg_reg[31]_i_8_CO_UNCONNECTED\(3),
      CO(2) => \ramplitude_min_reg_reg[31]_i_8_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_8_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_8_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \ramplitude_min_reg_reg_n_0_[30]\,
      DI(1) => \ramplitude_min_reg_reg_n_0_[29]\,
      DI(0) => \ramplitude_min_reg_reg_n_0_[28]\,
      O(3 downto 0) => ramplitude_min_reg0(31 downto 28),
      S(3) => \ramplitude_min_reg[31]_i_42_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_43_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_44_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_45_n_0\
    );
\ramplitude_min_reg_reg[31]_i_81\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ramplitude_min_reg_reg[31]_i_81_n_0\,
      CO(2) => \ramplitude_min_reg_reg[31]_i_81_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_81_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_81_n_3\,
      CYINIT => '1',
      DI(3) => \ramplitude_min_reg[31]_i_108_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_109_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_110_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_111_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_81_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_112_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_113_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_114_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_115_n_0\
    );
\ramplitude_min_reg_reg[31]_i_90\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ramplitude_min_reg_reg[31]_i_90_n_0\,
      CO(2) => \ramplitude_min_reg_reg[31]_i_90_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_90_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_90_n_3\,
      CYINIT => '1',
      DI(3) => \ramplitude_min_reg[31]_i_116_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_117_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_118_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_119_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_90_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_120_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_121_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_122_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_123_n_0\
    );
\ramplitude_min_reg_reg[31]_i_99\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ramplitude_min_reg_reg[31]_i_99_n_0\,
      CO(2) => \ramplitude_min_reg_reg[31]_i_99_n_1\,
      CO(1) => \ramplitude_min_reg_reg[31]_i_99_n_2\,
      CO(0) => \ramplitude_min_reg_reg[31]_i_99_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg[31]_i_124_n_0\,
      DI(2) => \ramplitude_min_reg[31]_i_125_n_0\,
      DI(1) => \ramplitude_min_reg[31]_i_126_n_0\,
      DI(0) => \ramplitude_min_reg[31]_i_127_n_0\,
      O(3 downto 0) => \NLW_ramplitude_min_reg_reg[31]_i_99_O_UNCONNECTED\(3 downto 0),
      S(3) => \ramplitude_min_reg[31]_i_128_n_0\,
      S(2) => \ramplitude_min_reg[31]_i_129_n_0\,
      S(1) => \ramplitude_min_reg[31]_i_130_n_0\,
      S(0) => \ramplitude_min_reg[31]_i_131_n_0\
    );
\ramplitude_min_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[3]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[3]\,
      R => '0'
    );
\ramplitude_min_reg_reg[3]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ramplitude_min_reg_reg[3]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[3]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[3]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[3]_i_2_n_3\,
      CYINIT => '1',
      DI(3) => \ramplitude_min_reg_reg_n_0_[3]\,
      DI(2) => \ramplitude_min_reg_reg_n_0_[2]\,
      DI(1) => \ramplitude_min_reg_reg_n_0_[1]\,
      DI(0) => \ramplitude_min_reg_reg_n_0_[0]\,
      O(3 downto 0) => ramplitude_min_reg0(3 downto 0),
      S(3) => \ramplitude_min_reg[3]_i_3_n_0\,
      S(2) => \ramplitude_min_reg[3]_i_4_n_0\,
      S(1) => \ramplitude_min_reg[3]_i_5_n_0\,
      S(0) => \ramplitude_min_reg[3]_i_6_n_0\
    );
\ramplitude_min_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[4]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[4]\,
      R => '0'
    );
\ramplitude_min_reg_reg[4]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ramplitude_min_reg_reg[4]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[4]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[4]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[4]_i_2_n_3\,
      CYINIT => p_0_in(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => p_0_in(4 downto 1)
    );
\ramplitude_min_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[5]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[5]\,
      R => '0'
    );
\ramplitude_min_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[6]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[6]\,
      R => '0'
    );
\ramplitude_min_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[7]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[7]\,
      R => '0'
    );
\ramplitude_min_reg_reg[7]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[3]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[7]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[7]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[7]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[7]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \ramplitude_min_reg_reg_n_0_[7]\,
      DI(2) => \ramplitude_min_reg_reg_n_0_[6]\,
      DI(1) => \ramplitude_min_reg_reg_n_0_[5]\,
      DI(0) => \ramplitude_min_reg_reg_n_0_[4]\,
      O(3 downto 0) => ramplitude_min_reg0(7 downto 4),
      S(3) => \ramplitude_min_reg[7]_i_3_n_0\,
      S(2) => \ramplitude_min_reg[7]_i_4_n_0\,
      S(1) => \ramplitude_min_reg[7]_i_5_n_0\,
      S(0) => \ramplitude_min_reg[7]_i_6_n_0\
    );
\ramplitude_min_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[8]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[8]\,
      R => '0'
    );
\ramplitude_min_reg_reg[8]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ramplitude_min_reg_reg[4]_i_2_n_0\,
      CO(3) => \ramplitude_min_reg_reg[8]_i_2_n_0\,
      CO(2) => \ramplitude_min_reg_reg[8]_i_2_n_1\,
      CO(1) => \ramplitude_min_reg_reg[8]_i_2_n_2\,
      CO(0) => \ramplitude_min_reg_reg[8]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => p_0_in(8 downto 5)
    );
\ramplitude_min_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_min_reg[31]_i_1_n_0\,
      D => \ramplitude_min_reg[9]_i_1_n_0\,
      Q => \ramplitude_min_reg_reg_n_0_[9]\,
      R => '0'
    );
\ramplitude_step_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(0),
      Q => ramplitude_step_reg(0),
      R => '0'
    );
\ramplitude_step_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(10),
      Q => ramplitude_step_reg(10),
      R => '0'
    );
\ramplitude_step_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(11),
      Q => ramplitude_step_reg(11),
      R => '0'
    );
\ramplitude_step_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(12),
      Q => ramplitude_step_reg(12),
      R => '0'
    );
\ramplitude_step_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(13),
      Q => ramplitude_step_reg(13),
      R => '0'
    );
\ramplitude_step_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(14),
      Q => ramplitude_step_reg(14),
      R => '0'
    );
\ramplitude_step_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(15),
      Q => ramplitude_step_reg(15),
      R => '0'
    );
\ramplitude_step_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(16),
      Q => ramplitude_step_reg(16),
      R => '0'
    );
\ramplitude_step_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(17),
      Q => ramplitude_step_reg(17),
      R => '0'
    );
\ramplitude_step_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(18),
      Q => ramplitude_step_reg(18),
      R => '0'
    );
\ramplitude_step_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(19),
      Q => ramplitude_step_reg(19),
      R => '0'
    );
\ramplitude_step_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(1),
      Q => ramplitude_step_reg(1),
      R => '0'
    );
\ramplitude_step_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(20),
      Q => ramplitude_step_reg(20),
      R => '0'
    );
\ramplitude_step_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(21),
      Q => ramplitude_step_reg(21),
      R => '0'
    );
\ramplitude_step_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(22),
      Q => ramplitude_step_reg(22),
      R => '0'
    );
\ramplitude_step_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(23),
      Q => ramplitude_step_reg(23),
      R => '0'
    );
\ramplitude_step_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(24),
      Q => ramplitude_step_reg(24),
      R => '0'
    );
\ramplitude_step_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(25),
      Q => ramplitude_step_reg(25),
      R => '0'
    );
\ramplitude_step_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(26),
      Q => ramplitude_step_reg(26),
      R => '0'
    );
\ramplitude_step_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(27),
      Q => ramplitude_step_reg(27),
      R => '0'
    );
\ramplitude_step_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(28),
      Q => ramplitude_step_reg(28),
      R => '0'
    );
\ramplitude_step_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(29),
      Q => ramplitude_step_reg(29),
      R => '0'
    );
\ramplitude_step_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(2),
      Q => ramplitude_step_reg(2),
      R => '0'
    );
\ramplitude_step_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(30),
      Q => ramplitude_step_reg(30),
      R => '0'
    );
\ramplitude_step_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(31),
      Q => ramplitude_step_reg(31),
      R => '0'
    );
\ramplitude_step_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(3),
      Q => ramplitude_step_reg(3),
      R => '0'
    );
\ramplitude_step_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(4),
      Q => ramplitude_step_reg(4),
      R => '0'
    );
\ramplitude_step_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(5),
      Q => ramplitude_step_reg(5),
      R => '0'
    );
\ramplitude_step_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(6),
      Q => ramplitude_step_reg(6),
      R => '0'
    );
\ramplitude_step_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(7),
      Q => ramplitude_step_reg(7),
      R => '0'
    );
\ramplitude_step_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(8),
      Q => ramplitude_step_reg(8),
      R => '0'
    );
\ramplitude_step_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ramplitude_step_reg_reg[31]_0\(9),
      Q => ramplitude_step_reg(9),
      R => '0'
    );
\status_reg[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF5D55FFFF"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => \^loop_locked_counter_reg_reg[30]\(0),
      I2 => D(2),
      I3 => D(3),
      I4 => D(0),
      I5 => Q(0),
      O => \^ramp_module_0_rst\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_AXI_PI_0_2_AXI_PI_v1_0_S00_AXI is
  port (
    axi_awready_reg_0 : out STD_LOGIC;
    axi_wready_reg_0 : out STD_LOGIC;
    axi_arready_reg_0 : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    aw_en_reg_0 : out STD_LOGIC;
    input_reset : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    axi_pi_output : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \loop_locked_counter_reg_reg[30]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    pid_loop_input : in STD_LOGIC_VECTOR ( 12 downto 0 );
    pid_loop_0_input : in STD_LOGIC_VECTOR ( 0 to 0 );
    axi_bvalid_reg_0 : in STD_LOGIC;
    aw_en_reg_1 : in STD_LOGIC;
    axi_rvalid_reg_0 : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    output_sel : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    D : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC
  );
end system_AXI_PI_0_2_AXI_PI_v1_0_S00_AXI;

architecture STRUCTURE of system_AXI_PI_0_2_AXI_PI_v1_0_S00_AXI is
  signal I_mon_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal I_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \I_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \I_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \I_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \I_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \I_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal I_term_mon : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal P_mon_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal P_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \P_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \P_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \P_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \P_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \P_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal P_term_mon : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal autolock : STD_LOGIC;
  signal autolock_0_input : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal autolock_input_max : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \autolock_input_max[13]_i_10_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_11_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_12_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_13_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_14_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_15_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_16_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_17_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_4_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_5_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_6_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_7_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_8_n_0\ : STD_LOGIC;
  signal \autolock_input_max[13]_i_9_n_0\ : STD_LOGIC;
  signal \autolock_input_max_reg[13]_i_2_n_1\ : STD_LOGIC;
  signal \autolock_input_max_reg[13]_i_2_n_2\ : STD_LOGIC;
  signal \autolock_input_max_reg[13]_i_2_n_3\ : STD_LOGIC;
  signal \autolock_input_max_reg[13]_i_3_n_0\ : STD_LOGIC;
  signal \autolock_input_max_reg[13]_i_3_n_1\ : STD_LOGIC;
  signal \autolock_input_max_reg[13]_i_3_n_2\ : STD_LOGIC;
  signal \autolock_input_max_reg[13]_i_3_n_3\ : STD_LOGIC;
  signal autolock_input_maxdex : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal autolock_input_maxdex0_in : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal autolock_input_min : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \autolock_input_min[13]_i_10_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_11_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_12_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_13_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_14_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_15_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_16_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_17_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_4_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_5_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_6_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_7_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_8_n_0\ : STD_LOGIC;
  signal \autolock_input_min[13]_i_9_n_0\ : STD_LOGIC;
  signal \autolock_input_min_reg[13]_i_2_n_1\ : STD_LOGIC;
  signal \autolock_input_min_reg[13]_i_2_n_2\ : STD_LOGIC;
  signal \autolock_input_min_reg[13]_i_2_n_3\ : STD_LOGIC;
  signal \autolock_input_min_reg[13]_i_3_n_0\ : STD_LOGIC;
  signal \autolock_input_min_reg[13]_i_3_n_1\ : STD_LOGIC;
  signal \autolock_input_min_reg[13]_i_3_n_2\ : STD_LOGIC;
  signal \autolock_input_min_reg[13]_i_3_n_3\ : STD_LOGIC;
  signal autolock_input_mindex : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[0]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[10]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[11]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[12]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[1]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[2]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[3]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[4]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[5]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[6]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[7]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[8]\ : STD_LOGIC;
  signal \autolock_input_mindex_reg_n_0_[9]\ : STD_LOGIC;
  signal autolock_input_reg : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal autolock_max_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \autolock_max_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \autolock_max_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \autolock_max_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \autolock_max_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \autolock_max_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal autolock_min_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \autolock_min_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \autolock_min_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \autolock_min_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \autolock_min_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \autolock_min_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \^aw_en_reg_0\ : STD_LOGIC;
  signal \axi_araddr_reg[2]_rep_n_0\ : STD_LOGIC;
  signal \axi_araddr_reg[3]_rep_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal \^axi_arready_reg_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal \^axi_awready_reg_0\ : STD_LOGIC;
  signal \axi_pi_output_reg[13]_inv_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[0]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[0]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[10]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[10]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[12]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[16]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[17]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[18]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[1]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[20]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[21]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[22]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[24]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[25]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[26]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[28]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[2]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[31]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[4]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[4]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[5]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[6]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[8]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[8]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[9]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[9]_i_5_n_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal \^axi_wready_reg_0\ : STD_LOGIC;
  signal \control_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \control_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \control_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \control_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \control_reg[31]_i_3_n_0\ : STD_LOGIC;
  signal \control_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[0]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[10]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[11]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[12]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[13]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[14]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[15]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[16]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[17]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[18]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[19]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[20]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[21]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[22]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[23]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[24]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[25]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[26]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[27]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[28]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[29]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[2]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[30]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[31]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[3]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[5]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[6]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[7]\ : STD_LOGIC;
  signal \control_reg_reg_n_0_[9]\ : STD_LOGIC;
  signal engage_pid_loop0 : STD_LOGIC;
  signal engage_pid_loop06_in : STD_LOGIC;
  signal error_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal input_railed0 : STD_LOGIC;
  signal input_railed00_in : STD_LOGIC;
  signal input_railed_max_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \input_railed_max_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \input_railed_max_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \input_railed_max_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \input_railed_max_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \input_railed_max_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal input_railed_min_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \input_railed_min_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \input_railed_min_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \input_railed_min_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \input_railed_min_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \input_railed_min_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \^input_reset\ : STD_LOGIC;
  signal input_reset_reg_i_10_n_0 : STD_LOGIC;
  signal input_reset_reg_i_11_n_0 : STD_LOGIC;
  signal input_reset_reg_i_12_n_0 : STD_LOGIC;
  signal input_reset_reg_i_13_n_0 : STD_LOGIC;
  signal input_reset_reg_i_15_n_0 : STD_LOGIC;
  signal input_reset_reg_i_16_n_0 : STD_LOGIC;
  signal input_reset_reg_i_17_n_0 : STD_LOGIC;
  signal input_reset_reg_i_18_n_0 : STD_LOGIC;
  signal input_reset_reg_i_19_n_0 : STD_LOGIC;
  signal input_reset_reg_i_1_n_0 : STD_LOGIC;
  signal input_reset_reg_i_20_n_0 : STD_LOGIC;
  signal input_reset_reg_i_21_n_0 : STD_LOGIC;
  signal input_reset_reg_i_22_n_0 : STD_LOGIC;
  signal input_reset_reg_i_24_n_0 : STD_LOGIC;
  signal input_reset_reg_i_25_n_0 : STD_LOGIC;
  signal input_reset_reg_i_26_n_0 : STD_LOGIC;
  signal input_reset_reg_i_27_n_0 : STD_LOGIC;
  signal input_reset_reg_i_28_n_0 : STD_LOGIC;
  signal input_reset_reg_i_29_n_0 : STD_LOGIC;
  signal input_reset_reg_i_30_n_0 : STD_LOGIC;
  signal input_reset_reg_i_31_n_0 : STD_LOGIC;
  signal input_reset_reg_i_33_n_0 : STD_LOGIC;
  signal input_reset_reg_i_34_n_0 : STD_LOGIC;
  signal input_reset_reg_i_35_n_0 : STD_LOGIC;
  signal input_reset_reg_i_36_n_0 : STD_LOGIC;
  signal input_reset_reg_i_37_n_0 : STD_LOGIC;
  signal input_reset_reg_i_38_n_0 : STD_LOGIC;
  signal input_reset_reg_i_39_n_0 : STD_LOGIC;
  signal input_reset_reg_i_40_n_0 : STD_LOGIC;
  signal input_reset_reg_i_42_n_0 : STD_LOGIC;
  signal input_reset_reg_i_43_n_0 : STD_LOGIC;
  signal input_reset_reg_i_44_n_0 : STD_LOGIC;
  signal input_reset_reg_i_45_n_0 : STD_LOGIC;
  signal input_reset_reg_i_46_n_0 : STD_LOGIC;
  signal input_reset_reg_i_47_n_0 : STD_LOGIC;
  signal input_reset_reg_i_48_n_0 : STD_LOGIC;
  signal input_reset_reg_i_49_n_0 : STD_LOGIC;
  signal input_reset_reg_i_51_n_0 : STD_LOGIC;
  signal input_reset_reg_i_52_n_0 : STD_LOGIC;
  signal input_reset_reg_i_53_n_0 : STD_LOGIC;
  signal input_reset_reg_i_54_n_0 : STD_LOGIC;
  signal input_reset_reg_i_55_n_0 : STD_LOGIC;
  signal input_reset_reg_i_56_n_0 : STD_LOGIC;
  signal input_reset_reg_i_57_n_0 : STD_LOGIC;
  signal input_reset_reg_i_58_n_0 : STD_LOGIC;
  signal input_reset_reg_i_59_n_0 : STD_LOGIC;
  signal input_reset_reg_i_60_n_0 : STD_LOGIC;
  signal input_reset_reg_i_61_n_0 : STD_LOGIC;
  signal input_reset_reg_i_62_n_0 : STD_LOGIC;
  signal input_reset_reg_i_63_n_0 : STD_LOGIC;
  signal input_reset_reg_i_64_n_0 : STD_LOGIC;
  signal input_reset_reg_i_65_n_0 : STD_LOGIC;
  signal input_reset_reg_i_66_n_0 : STD_LOGIC;
  signal input_reset_reg_i_67_n_0 : STD_LOGIC;
  signal input_reset_reg_i_68_n_0 : STD_LOGIC;
  signal input_reset_reg_i_69_n_0 : STD_LOGIC;
  signal input_reset_reg_i_6_n_0 : STD_LOGIC;
  signal input_reset_reg_i_70_n_0 : STD_LOGIC;
  signal input_reset_reg_i_71_n_0 : STD_LOGIC;
  signal input_reset_reg_i_72_n_0 : STD_LOGIC;
  signal input_reset_reg_i_73_n_0 : STD_LOGIC;
  signal input_reset_reg_i_74_n_0 : STD_LOGIC;
  signal input_reset_reg_i_7_n_0 : STD_LOGIC;
  signal input_reset_reg_i_8_n_0 : STD_LOGIC;
  signal input_reset_reg_i_9_n_0 : STD_LOGIC;
  signal input_reset_reg_reg_i_14_n_0 : STD_LOGIC;
  signal input_reset_reg_reg_i_14_n_1 : STD_LOGIC;
  signal input_reset_reg_reg_i_14_n_2 : STD_LOGIC;
  signal input_reset_reg_reg_i_14_n_3 : STD_LOGIC;
  signal input_reset_reg_reg_i_23_n_0 : STD_LOGIC;
  signal input_reset_reg_reg_i_23_n_1 : STD_LOGIC;
  signal input_reset_reg_reg_i_23_n_2 : STD_LOGIC;
  signal input_reset_reg_reg_i_23_n_3 : STD_LOGIC;
  signal input_reset_reg_reg_i_2_n_1 : STD_LOGIC;
  signal input_reset_reg_reg_i_2_n_2 : STD_LOGIC;
  signal input_reset_reg_reg_i_2_n_3 : STD_LOGIC;
  signal input_reset_reg_reg_i_32_n_0 : STD_LOGIC;
  signal input_reset_reg_reg_i_32_n_1 : STD_LOGIC;
  signal input_reset_reg_reg_i_32_n_2 : STD_LOGIC;
  signal input_reset_reg_reg_i_32_n_3 : STD_LOGIC;
  signal input_reset_reg_reg_i_3_n_1 : STD_LOGIC;
  signal input_reset_reg_reg_i_3_n_2 : STD_LOGIC;
  signal input_reset_reg_reg_i_3_n_3 : STD_LOGIC;
  signal input_reset_reg_reg_i_41_n_0 : STD_LOGIC;
  signal input_reset_reg_reg_i_41_n_1 : STD_LOGIC;
  signal input_reset_reg_reg_i_41_n_2 : STD_LOGIC;
  signal input_reset_reg_reg_i_41_n_3 : STD_LOGIC;
  signal input_reset_reg_reg_i_50_n_0 : STD_LOGIC;
  signal input_reset_reg_reg_i_50_n_1 : STD_LOGIC;
  signal input_reset_reg_reg_i_50_n_2 : STD_LOGIC;
  signal input_reset_reg_reg_i_50_n_3 : STD_LOGIC;
  signal input_reset_reg_reg_i_5_n_0 : STD_LOGIC;
  signal input_reset_reg_reg_i_5_n_1 : STD_LOGIC;
  signal input_reset_reg_reg_i_5_n_2 : STD_LOGIC;
  signal input_reset_reg_reg_i_5_n_3 : STD_LOGIC;
  signal input_stable : STD_LOGIC;
  signal input_stable0 : STD_LOGIC;
  signal input_stable02_in : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_10_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_11_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_12_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_13_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_14_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_15_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_17_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_18_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_19_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_20_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_21_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_22_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_23_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_24_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_26_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_27_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_28_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_29_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_30_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_31_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_32_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_33_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_35_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_36_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_37_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_38_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_39_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_40_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_41_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_42_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_44_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_45_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_46_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_47_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_48_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_49_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_50_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_51_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_53_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_54_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_55_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_56_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_57_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_58_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_59_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_60_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_61_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_62_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_63_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_64_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_65_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_66_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_67_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_68_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_69_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_6_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_70_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_71_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_72_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_73_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_74_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_75_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_76_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_8_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg[0]_i_9_n_0\ : STD_LOGIC;
  signal loop_locked_counter_reg_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \loop_locked_counter_reg_reg[0]_i_16_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_16_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_16_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_16_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_25_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_25_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_25_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_25_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_34_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_34_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_34_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_34_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_43_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_43_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_43_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_43_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_4_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_4_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_4_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_52_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_52_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_52_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_52_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_5_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_5_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_5_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_7_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_7_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_7_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[0]_i_7_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \^loop_locked_counter_reg_reg[30]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \loop_locked_counter_reg_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \loop_locked_counter_reg_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal loop_locked_delay_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \loop_locked_delay_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_delay_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_delay_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_delay_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \loop_locked_delay_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal loop_locked_max_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \loop_locked_max_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_max_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_max_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_max_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \loop_locked_max_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal loop_locked_min_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \loop_locked_min_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_min_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_min_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \loop_locked_min_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \loop_locked_min_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal loop_output_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal maxdex_reg : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[0]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[10]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[11]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[12]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[15]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[16]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[17]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[18]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[19]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[1]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[20]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[21]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[22]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[23]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[24]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[25]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[26]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[27]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[28]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[2]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[31]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[3]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[4]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[5]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[6]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[7]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[8]\ : STD_LOGIC;
  signal \maxdex_reg_reg_n_0_[9]\ : STD_LOGIC;
  signal mindex_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal p_0_in0 : STD_LOGIC;
  signal p_0_in1_in : STD_LOGIC;
  signal p_0_in_0 : STD_LOGIC_VECTOR ( 31 to 31 );
  signal p_7_in : STD_LOGIC;
  signal pid_loop_0_error : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal pid_loop_0_input_reg : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal pid_loop_0_n_0 : STD_LOGIC;
  signal pid_loop_0_n_35 : STD_LOGIC;
  signal pid_loop_0_output : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal pid_loop_0_rst1 : STD_LOGIC;
  signal ramp_enable : STD_LOGIC;
  signal ramp_module_0_n_15 : STD_LOGIC;
  signal ramp_module_0_n_16 : STD_LOGIC;
  signal ramp_module_0_n_17 : STD_LOGIC;
  signal ramp_module_0_n_18 : STD_LOGIC;
  signal ramp_module_0_n_19 : STD_LOGIC;
  signal ramp_module_0_n_20 : STD_LOGIC;
  signal ramp_module_0_n_21 : STD_LOGIC;
  signal ramp_module_0_n_22 : STD_LOGIC;
  signal ramp_module_0_n_23 : STD_LOGIC;
  signal ramp_module_0_n_24 : STD_LOGIC;
  signal ramp_module_0_n_25 : STD_LOGIC;
  signal ramp_module_0_n_26 : STD_LOGIC;
  signal ramp_module_0_n_27 : STD_LOGIC;
  signal ramp_module_0_n_28 : STD_LOGIC;
  signal ramp_module_0_n_33 : STD_LOGIC;
  signal ramp_module_0_output : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal ramp_module_0_reset : STD_LOGIC_VECTOR ( 31 downto 18 );
  signal ramp_module_0_rst : STD_LOGIC;
  signal ramp_offset_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \ramp_offset_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_offset_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_offset_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_offset_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \ramp_offset_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal ramp_output_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ramp_step_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \ramp_step_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_step_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_step_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \ramp_step_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \ramp_step_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal ramplitude_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \ramplitude_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal ramplitude_step_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \ramplitude_step_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_step_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_step_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \ramplitude_step_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \ramplitude_step_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal setpoint_reg : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \setpoint_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \setpoint_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \setpoint_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \setpoint_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \setpoint_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \setpoint_reg__0\ : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal slv_reg_rden : STD_LOGIC;
  signal status_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \NLW_autolock_input_max_reg[13]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_autolock_input_max_reg[13]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_autolock_input_max_reg[13]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_autolock_input_min_reg[13]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_autolock_input_min_reg[13]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_autolock_input_min_reg[13]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_input_reset_reg_reg_i_14_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_input_reset_reg_reg_i_2_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_input_reset_reg_reg_i_23_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_input_reset_reg_reg_i_3_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_input_reset_reg_reg_i_32_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_input_reset_reg_reg_i_41_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_input_reset_reg_reg_i_5_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_input_reset_reg_reg_i_50_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_locked_counter_reg_reg[0]_i_16_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_locked_counter_reg_reg[0]_i_25_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_locked_counter_reg_reg[0]_i_34_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_locked_counter_reg_reg[0]_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_locked_counter_reg_reg[0]_i_43_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_locked_counter_reg_reg[0]_i_5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_locked_counter_reg_reg[0]_i_52_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_locked_counter_reg_reg[0]_i_7_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_loop_locked_counter_reg_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ORIG_CELL_NAME : string;
  attribute ORIG_CELL_NAME of \axi_araddr_reg[2]\ : label is "axi_araddr_reg[2]";
  attribute ORIG_CELL_NAME of \axi_araddr_reg[2]_rep\ : label is "axi_araddr_reg[2]";
  attribute ORIG_CELL_NAME of \axi_araddr_reg[3]\ : label is "axi_araddr_reg[3]";
  attribute ORIG_CELL_NAME of \axi_araddr_reg[3]_rep\ : label is "axi_araddr_reg[3]";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \control_reg[31]_i_3\ : label is "soft_lutpair18";
begin
  aw_en_reg_0 <= \^aw_en_reg_0\;
  axi_arready_reg_0 <= \^axi_arready_reg_0\;
  axi_awready_reg_0 <= \^axi_awready_reg_0\;
  axi_wready_reg_0 <= \^axi_wready_reg_0\;
  input_reset <= \^input_reset\;
  \loop_locked_counter_reg_reg[30]_0\(0) <= \^loop_locked_counter_reg_reg[30]_0\(0);
  s00_axi_rvalid <= \^s00_axi_rvalid\;
\I_mon_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(0),
      Q => I_mon_reg(0),
      R => '0'
    );
\I_mon_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(10),
      Q => I_mon_reg(10),
      R => '0'
    );
\I_mon_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(11),
      Q => I_mon_reg(11),
      R => '0'
    );
\I_mon_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(12),
      Q => I_mon_reg(12),
      R => '0'
    );
\I_mon_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(13),
      Q => I_mon_reg(13),
      R => '0'
    );
\I_mon_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(14),
      Q => I_mon_reg(14),
      R => '0'
    );
\I_mon_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(15),
      Q => I_mon_reg(15),
      R => '0'
    );
\I_mon_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(16),
      Q => I_mon_reg(16),
      R => '0'
    );
\I_mon_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(17),
      Q => I_mon_reg(17),
      R => '0'
    );
\I_mon_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(18),
      Q => I_mon_reg(18),
      R => '0'
    );
\I_mon_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(19),
      Q => I_mon_reg(19),
      R => '0'
    );
\I_mon_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(1),
      Q => I_mon_reg(1),
      R => '0'
    );
\I_mon_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(20),
      Q => I_mon_reg(20),
      R => '0'
    );
\I_mon_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(21),
      Q => I_mon_reg(21),
      R => '0'
    );
\I_mon_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(22),
      Q => I_mon_reg(22),
      R => '0'
    );
\I_mon_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(23),
      Q => I_mon_reg(23),
      R => '0'
    );
\I_mon_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(24),
      Q => I_mon_reg(24),
      R => '0'
    );
\I_mon_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(25),
      Q => I_mon_reg(25),
      R => '0'
    );
\I_mon_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(26),
      Q => I_mon_reg(26),
      R => '0'
    );
\I_mon_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(27),
      Q => I_mon_reg(27),
      R => '0'
    );
\I_mon_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(28),
      Q => I_mon_reg(28),
      R => '0'
    );
\I_mon_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(29),
      Q => I_mon_reg(29),
      R => '0'
    );
\I_mon_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(2),
      Q => I_mon_reg(2),
      R => '0'
    );
\I_mon_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(30),
      Q => I_mon_reg(30),
      R => '0'
    );
\I_mon_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(31),
      Q => I_mon_reg(31),
      R => '0'
    );
\I_mon_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(3),
      Q => I_mon_reg(3),
      R => '0'
    );
\I_mon_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(4),
      Q => I_mon_reg(4),
      R => '0'
    );
\I_mon_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(5),
      Q => I_mon_reg(5),
      R => '0'
    );
\I_mon_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(6),
      Q => I_mon_reg(6),
      R => '0'
    );
\I_mon_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(7),
      Q => I_mon_reg(7),
      R => '0'
    );
\I_mon_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(8),
      Q => I_mon_reg(8),
      R => '0'
    );
\I_mon_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => I_term_mon(9),
      Q => I_mon_reg(9),
      R => '0'
    );
\I_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \I_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \I_reg[15]_i_1_n_0\
    );
\I_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \I_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \I_reg[23]_i_1_n_0\
    );
\I_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \I_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \I_reg[31]_i_1_n_0\
    );
\I_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000200000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \I_reg[31]_i_2_n_0\
    );
\I_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \I_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \I_reg[7]_i_1_n_0\
    );
\I_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => I_reg(0),
      R => pid_loop_0_rst1
    );
\I_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => I_reg(10),
      R => pid_loop_0_rst1
    );
\I_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => I_reg(11),
      R => pid_loop_0_rst1
    );
\I_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => I_reg(12),
      R => pid_loop_0_rst1
    );
\I_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => I_reg(13),
      R => pid_loop_0_rst1
    );
\I_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => I_reg(14),
      R => pid_loop_0_rst1
    );
\I_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => I_reg(15),
      R => pid_loop_0_rst1
    );
\I_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => I_reg(16),
      R => pid_loop_0_rst1
    );
\I_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => I_reg(17),
      R => pid_loop_0_rst1
    );
\I_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => I_reg(18),
      R => pid_loop_0_rst1
    );
\I_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => I_reg(19),
      R => pid_loop_0_rst1
    );
\I_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => I_reg(1),
      R => pid_loop_0_rst1
    );
\I_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => I_reg(20),
      R => pid_loop_0_rst1
    );
\I_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => I_reg(21),
      R => pid_loop_0_rst1
    );
\I_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => I_reg(22),
      R => pid_loop_0_rst1
    );
\I_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => I_reg(23),
      R => pid_loop_0_rst1
    );
\I_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => I_reg(24),
      R => pid_loop_0_rst1
    );
\I_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => I_reg(25),
      R => pid_loop_0_rst1
    );
\I_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => I_reg(26),
      R => pid_loop_0_rst1
    );
\I_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => I_reg(27),
      R => pid_loop_0_rst1
    );
\I_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => I_reg(28),
      R => pid_loop_0_rst1
    );
\I_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => I_reg(29),
      R => pid_loop_0_rst1
    );
\I_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => I_reg(2),
      R => pid_loop_0_rst1
    );
\I_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => I_reg(30),
      R => pid_loop_0_rst1
    );
\I_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => I_reg(31),
      R => pid_loop_0_rst1
    );
\I_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => I_reg(3),
      R => pid_loop_0_rst1
    );
\I_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => I_reg(4),
      R => pid_loop_0_rst1
    );
\I_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => I_reg(5),
      R => pid_loop_0_rst1
    );
\I_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => I_reg(6),
      R => pid_loop_0_rst1
    );
\I_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => I_reg(7),
      R => pid_loop_0_rst1
    );
\I_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => I_reg(8),
      R => pid_loop_0_rst1
    );
\I_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \I_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => I_reg(9),
      R => pid_loop_0_rst1
    );
\P_mon_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(0),
      Q => P_mon_reg(0),
      R => '0'
    );
\P_mon_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(10),
      Q => P_mon_reg(10),
      R => '0'
    );
\P_mon_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(11),
      Q => P_mon_reg(11),
      R => '0'
    );
\P_mon_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(12),
      Q => P_mon_reg(12),
      R => '0'
    );
\P_mon_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(13),
      Q => P_mon_reg(13),
      R => '0'
    );
\P_mon_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(14),
      Q => P_mon_reg(14),
      R => '0'
    );
\P_mon_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(15),
      Q => P_mon_reg(15),
      R => '0'
    );
\P_mon_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(16),
      Q => P_mon_reg(16),
      R => '0'
    );
\P_mon_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(17),
      Q => P_mon_reg(17),
      R => '0'
    );
\P_mon_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(18),
      Q => P_mon_reg(18),
      R => '0'
    );
\P_mon_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(19),
      Q => P_mon_reg(19),
      R => '0'
    );
\P_mon_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(1),
      Q => P_mon_reg(1),
      R => '0'
    );
\P_mon_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(20),
      Q => P_mon_reg(20),
      R => '0'
    );
\P_mon_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(21),
      Q => P_mon_reg(21),
      R => '0'
    );
\P_mon_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(22),
      Q => P_mon_reg(22),
      R => '0'
    );
\P_mon_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(23),
      Q => P_mon_reg(23),
      R => '0'
    );
\P_mon_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(24),
      Q => P_mon_reg(24),
      R => '0'
    );
\P_mon_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(25),
      Q => P_mon_reg(25),
      R => '0'
    );
\P_mon_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(26),
      Q => P_mon_reg(26),
      R => '0'
    );
\P_mon_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(27),
      Q => P_mon_reg(27),
      R => '0'
    );
\P_mon_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(28),
      Q => P_mon_reg(28),
      R => '0'
    );
\P_mon_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(29),
      Q => P_mon_reg(29),
      R => '0'
    );
\P_mon_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(2),
      Q => P_mon_reg(2),
      R => '0'
    );
\P_mon_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(30),
      Q => P_mon_reg(30),
      R => '0'
    );
\P_mon_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(31),
      Q => P_mon_reg(31),
      R => '0'
    );
\P_mon_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(3),
      Q => P_mon_reg(3),
      R => '0'
    );
\P_mon_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(4),
      Q => P_mon_reg(4),
      R => '0'
    );
\P_mon_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(5),
      Q => P_mon_reg(5),
      R => '0'
    );
\P_mon_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(6),
      Q => P_mon_reg(6),
      R => '0'
    );
\P_mon_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(7),
      Q => P_mon_reg(7),
      R => '0'
    );
\P_mon_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(8),
      Q => P_mon_reg(8),
      R => '0'
    );
\P_mon_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => P_term_mon(9),
      Q => P_mon_reg(9),
      R => '0'
    );
\P_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \P_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \P_reg[15]_i_1_n_0\
    );
\P_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \P_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \P_reg[23]_i_1_n_0\
    );
\P_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \P_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \P_reg[31]_i_1_n_0\
    );
\P_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000100000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \P_reg[31]_i_2_n_0\
    );
\P_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \P_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \P_reg[7]_i_1_n_0\
    );
\P_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => P_reg(0),
      R => pid_loop_0_rst1
    );
\P_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => P_reg(10),
      R => pid_loop_0_rst1
    );
\P_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => P_reg(11),
      R => pid_loop_0_rst1
    );
\P_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => P_reg(12),
      R => pid_loop_0_rst1
    );
\P_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => P_reg(13),
      R => pid_loop_0_rst1
    );
\P_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => P_reg(14),
      R => pid_loop_0_rst1
    );
\P_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => P_reg(15),
      R => pid_loop_0_rst1
    );
\P_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => P_reg(16),
      R => pid_loop_0_rst1
    );
\P_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => P_reg(17),
      R => pid_loop_0_rst1
    );
\P_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => P_reg(18),
      R => pid_loop_0_rst1
    );
\P_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => P_reg(19),
      R => pid_loop_0_rst1
    );
\P_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => P_reg(1),
      R => pid_loop_0_rst1
    );
\P_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => P_reg(20),
      R => pid_loop_0_rst1
    );
\P_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => P_reg(21),
      R => pid_loop_0_rst1
    );
\P_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => P_reg(22),
      R => pid_loop_0_rst1
    );
\P_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => P_reg(23),
      R => pid_loop_0_rst1
    );
\P_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => P_reg(24),
      R => pid_loop_0_rst1
    );
\P_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => P_reg(25),
      R => pid_loop_0_rst1
    );
\P_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => P_reg(26),
      R => pid_loop_0_rst1
    );
\P_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => P_reg(27),
      R => pid_loop_0_rst1
    );
\P_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => P_reg(28),
      R => pid_loop_0_rst1
    );
\P_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => P_reg(29),
      R => pid_loop_0_rst1
    );
\P_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => P_reg(2),
      R => pid_loop_0_rst1
    );
\P_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => P_reg(30),
      R => pid_loop_0_rst1
    );
\P_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => P_reg(31),
      R => pid_loop_0_rst1
    );
\P_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => P_reg(3),
      R => pid_loop_0_rst1
    );
\P_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => P_reg(4),
      R => pid_loop_0_rst1
    );
\P_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => P_reg(5),
      R => pid_loop_0_rst1
    );
\P_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => P_reg(6),
      R => pid_loop_0_rst1
    );
\P_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => P_reg(7),
      R => pid_loop_0_rst1
    );
\P_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => P_reg(8),
      R => pid_loop_0_rst1
    );
\P_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \P_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => P_reg(9),
      R => pid_loop_0_rst1
    );
\autolock_0_input_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(0),
      Q => autolock_0_input(0),
      R => '0'
    );
\autolock_0_input_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(10),
      Q => autolock_0_input(10),
      R => '0'
    );
\autolock_0_input_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(11),
      Q => autolock_0_input(11),
      R => '0'
    );
\autolock_0_input_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(12),
      Q => autolock_0_input(12),
      R => '0'
    );
\autolock_0_input_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(13),
      Q => autolock_0_input(13),
      R => '0'
    );
\autolock_0_input_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(1),
      Q => autolock_0_input(1),
      R => '0'
    );
\autolock_0_input_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(2),
      Q => autolock_0_input(2),
      R => '0'
    );
\autolock_0_input_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(3),
      Q => autolock_0_input(3),
      R => '0'
    );
\autolock_0_input_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(4),
      Q => autolock_0_input(4),
      R => '0'
    );
\autolock_0_input_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(5),
      Q => autolock_0_input(5),
      R => '0'
    );
\autolock_0_input_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(6),
      Q => autolock_0_input(6),
      R => '0'
    );
\autolock_0_input_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(7),
      Q => autolock_0_input(7),
      R => '0'
    );
\autolock_0_input_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(8),
      Q => autolock_0_input(8),
      R => '0'
    );
\autolock_0_input_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(9),
      Q => autolock_0_input(9),
      R => '0'
    );
\autolock_input_max[13]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_0_input(6),
      I1 => autolock_input_max(6),
      I2 => autolock_input_max(7),
      I3 => autolock_0_input(7),
      O => \autolock_input_max[13]_i_10_n_0\
    );
\autolock_input_max[13]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_0_input(4),
      I1 => autolock_input_max(4),
      I2 => autolock_input_max(5),
      I3 => autolock_0_input(5),
      O => \autolock_input_max[13]_i_11_n_0\
    );
\autolock_input_max[13]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_0_input(2),
      I1 => autolock_input_max(2),
      I2 => autolock_input_max(3),
      I3 => autolock_0_input(3),
      O => \autolock_input_max[13]_i_12_n_0\
    );
\autolock_input_max[13]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_0_input(0),
      I1 => autolock_input_max(0),
      I2 => autolock_input_max(1),
      I3 => autolock_0_input(1),
      O => \autolock_input_max[13]_i_13_n_0\
    );
\autolock_input_max[13]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_0_input(6),
      I1 => autolock_input_max(6),
      I2 => autolock_0_input(7),
      I3 => autolock_input_max(7),
      O => \autolock_input_max[13]_i_14_n_0\
    );
\autolock_input_max[13]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_0_input(4),
      I1 => autolock_input_max(4),
      I2 => autolock_0_input(5),
      I3 => autolock_input_max(5),
      O => \autolock_input_max[13]_i_15_n_0\
    );
\autolock_input_max[13]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_0_input(2),
      I1 => autolock_input_max(2),
      I2 => autolock_0_input(3),
      I3 => autolock_input_max(3),
      O => \autolock_input_max[13]_i_16_n_0\
    );
\autolock_input_max[13]_i_17\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_0_input(0),
      I1 => autolock_input_max(0),
      I2 => autolock_0_input(1),
      I3 => autolock_input_max(1),
      O => \autolock_input_max[13]_i_17_n_0\
    );
\autolock_input_max[13]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_0_input(12),
      I1 => autolock_input_max(12),
      I2 => autolock_0_input(13),
      I3 => autolock_input_max(13),
      O => \autolock_input_max[13]_i_4_n_0\
    );
\autolock_input_max[13]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_0_input(10),
      I1 => autolock_input_max(10),
      I2 => autolock_input_max(11),
      I3 => autolock_0_input(11),
      O => \autolock_input_max[13]_i_5_n_0\
    );
\autolock_input_max[13]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_0_input(8),
      I1 => autolock_input_max(8),
      I2 => autolock_input_max(9),
      I3 => autolock_0_input(9),
      O => \autolock_input_max[13]_i_6_n_0\
    );
\autolock_input_max[13]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_0_input(12),
      I1 => autolock_input_max(12),
      I2 => autolock_input_max(13),
      I3 => autolock_0_input(13),
      O => \autolock_input_max[13]_i_7_n_0\
    );
\autolock_input_max[13]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_0_input(10),
      I1 => autolock_input_max(10),
      I2 => autolock_0_input(11),
      I3 => autolock_input_max(11),
      O => \autolock_input_max[13]_i_8_n_0\
    );
\autolock_input_max[13]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_0_input(8),
      I1 => autolock_input_max(8),
      I2 => autolock_0_input(9),
      I3 => autolock_input_max(9),
      O => \autolock_input_max[13]_i_9_n_0\
    );
\autolock_input_max_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(0),
      Q => autolock_input_max(0),
      R => '0'
    );
\autolock_input_max_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(10),
      Q => autolock_input_max(10),
      R => '0'
    );
\autolock_input_max_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(11),
      Q => autolock_input_max(11),
      R => '0'
    );
\autolock_input_max_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(12),
      Q => autolock_input_max(12),
      R => '0'
    );
\autolock_input_max_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(13),
      Q => autolock_input_max(13),
      R => '0'
    );
\autolock_input_max_reg[13]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \autolock_input_max_reg[13]_i_3_n_0\,
      CO(3) => \NLW_autolock_input_max_reg[13]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \autolock_input_max_reg[13]_i_2_n_1\,
      CO(1) => \autolock_input_max_reg[13]_i_2_n_2\,
      CO(0) => \autolock_input_max_reg[13]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \autolock_input_max[13]_i_4_n_0\,
      DI(1) => \autolock_input_max[13]_i_5_n_0\,
      DI(0) => \autolock_input_max[13]_i_6_n_0\,
      O(3 downto 0) => \NLW_autolock_input_max_reg[13]_i_2_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \autolock_input_max[13]_i_7_n_0\,
      S(1) => \autolock_input_max[13]_i_8_n_0\,
      S(0) => \autolock_input_max[13]_i_9_n_0\
    );
\autolock_input_max_reg[13]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \autolock_input_max_reg[13]_i_3_n_0\,
      CO(2) => \autolock_input_max_reg[13]_i_3_n_1\,
      CO(1) => \autolock_input_max_reg[13]_i_3_n_2\,
      CO(0) => \autolock_input_max_reg[13]_i_3_n_3\,
      CYINIT => '0',
      DI(3) => \autolock_input_max[13]_i_10_n_0\,
      DI(2) => \autolock_input_max[13]_i_11_n_0\,
      DI(1) => \autolock_input_max[13]_i_12_n_0\,
      DI(0) => \autolock_input_max[13]_i_13_n_0\,
      O(3 downto 0) => \NLW_autolock_input_max_reg[13]_i_3_O_UNCONNECTED\(3 downto 0),
      S(3) => \autolock_input_max[13]_i_14_n_0\,
      S(2) => \autolock_input_max[13]_i_15_n_0\,
      S(1) => \autolock_input_max[13]_i_16_n_0\,
      S(0) => \autolock_input_max[13]_i_17_n_0\
    );
\autolock_input_max_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(1),
      Q => autolock_input_max(1),
      R => '0'
    );
\autolock_input_max_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(2),
      Q => autolock_input_max(2),
      R => '0'
    );
\autolock_input_max_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(3),
      Q => autolock_input_max(3),
      R => '0'
    );
\autolock_input_max_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(4),
      Q => autolock_input_max(4),
      R => '0'
    );
\autolock_input_max_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(5),
      Q => autolock_input_max(5),
      R => '0'
    );
\autolock_input_max_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(6),
      Q => autolock_input_max(6),
      R => '0'
    );
\autolock_input_max_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(7),
      Q => autolock_input_max(7),
      R => '0'
    );
\autolock_input_max_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(8),
      Q => autolock_input_max(8),
      R => '0'
    );
\autolock_input_max_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_0_input(9),
      Q => autolock_input_max(9),
      R => '0'
    );
\autolock_input_maxdex_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(0),
      Q => autolock_input_maxdex(0),
      R => '0'
    );
\autolock_input_maxdex_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(10),
      Q => autolock_input_maxdex(10),
      R => '0'
    );
\autolock_input_maxdex_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(11),
      Q => autolock_input_maxdex(11),
      R => '0'
    );
\autolock_input_maxdex_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(12),
      Q => autolock_input_maxdex(12),
      R => '0'
    );
\autolock_input_maxdex_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(13),
      Q => autolock_input_maxdex(13),
      R => '0'
    );
\autolock_input_maxdex_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(1),
      Q => autolock_input_maxdex(1),
      R => '0'
    );
\autolock_input_maxdex_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(2),
      Q => autolock_input_maxdex(2),
      R => '0'
    );
\autolock_input_maxdex_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(3),
      Q => autolock_input_maxdex(3),
      R => '0'
    );
\autolock_input_maxdex_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(4),
      Q => autolock_input_maxdex(4),
      R => '0'
    );
\autolock_input_maxdex_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(5),
      Q => autolock_input_maxdex(5),
      R => '0'
    );
\autolock_input_maxdex_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(6),
      Q => autolock_input_maxdex(6),
      R => '0'
    );
\autolock_input_maxdex_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(7),
      Q => autolock_input_maxdex(7),
      R => '0'
    );
\autolock_input_maxdex_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(8),
      Q => autolock_input_maxdex(8),
      R => '0'
    );
\autolock_input_maxdex_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => ramp_module_0_n_33,
      D => autolock_input_maxdex0_in(9),
      Q => autolock_input_maxdex(9),
      R => '0'
    );
\autolock_input_min[13]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_input_min(6),
      I1 => autolock_0_input(6),
      I2 => autolock_0_input(7),
      I3 => autolock_input_min(7),
      O => \autolock_input_min[13]_i_10_n_0\
    );
\autolock_input_min[13]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_input_min(4),
      I1 => autolock_0_input(4),
      I2 => autolock_0_input(5),
      I3 => autolock_input_min(5),
      O => \autolock_input_min[13]_i_11_n_0\
    );
\autolock_input_min[13]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_input_min(2),
      I1 => autolock_0_input(2),
      I2 => autolock_0_input(3),
      I3 => autolock_input_min(3),
      O => \autolock_input_min[13]_i_12_n_0\
    );
\autolock_input_min[13]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_input_min(0),
      I1 => autolock_0_input(0),
      I2 => autolock_0_input(1),
      I3 => autolock_input_min(1),
      O => \autolock_input_min[13]_i_13_n_0\
    );
\autolock_input_min[13]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_input_min(6),
      I1 => autolock_0_input(6),
      I2 => autolock_input_min(7),
      I3 => autolock_0_input(7),
      O => \autolock_input_min[13]_i_14_n_0\
    );
\autolock_input_min[13]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_input_min(4),
      I1 => autolock_0_input(4),
      I2 => autolock_input_min(5),
      I3 => autolock_0_input(5),
      O => \autolock_input_min[13]_i_15_n_0\
    );
\autolock_input_min[13]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_input_min(2),
      I1 => autolock_0_input(2),
      I2 => autolock_input_min(3),
      I3 => autolock_0_input(3),
      O => \autolock_input_min[13]_i_16_n_0\
    );
\autolock_input_min[13]_i_17\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_input_min(0),
      I1 => autolock_0_input(0),
      I2 => autolock_input_min(1),
      I3 => autolock_0_input(1),
      O => \autolock_input_min[13]_i_17_n_0\
    );
\autolock_input_min[13]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_input_min(12),
      I1 => autolock_0_input(12),
      I2 => autolock_input_min(13),
      I3 => autolock_0_input(13),
      O => \autolock_input_min[13]_i_4_n_0\
    );
\autolock_input_min[13]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_input_min(10),
      I1 => autolock_0_input(10),
      I2 => autolock_0_input(11),
      I3 => autolock_input_min(11),
      O => \autolock_input_min[13]_i_5_n_0\
    );
\autolock_input_min[13]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => autolock_input_min(8),
      I1 => autolock_0_input(8),
      I2 => autolock_0_input(9),
      I3 => autolock_input_min(9),
      O => \autolock_input_min[13]_i_6_n_0\
    );
\autolock_input_min[13]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_input_min(12),
      I1 => autolock_0_input(12),
      I2 => autolock_0_input(13),
      I3 => autolock_input_min(13),
      O => \autolock_input_min[13]_i_7_n_0\
    );
\autolock_input_min[13]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_input_min(10),
      I1 => autolock_0_input(10),
      I2 => autolock_input_min(11),
      I3 => autolock_0_input(11),
      O => \autolock_input_min[13]_i_8_n_0\
    );
\autolock_input_min[13]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => autolock_input_min(8),
      I1 => autolock_0_input(8),
      I2 => autolock_input_min(9),
      I3 => autolock_0_input(9),
      O => \autolock_input_min[13]_i_9_n_0\
    );
\autolock_input_min_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(0),
      Q => autolock_input_min(0),
      R => '0'
    );
\autolock_input_min_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(10),
      Q => autolock_input_min(10),
      R => '0'
    );
\autolock_input_min_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(11),
      Q => autolock_input_min(11),
      R => '0'
    );
\autolock_input_min_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(12),
      Q => autolock_input_min(12),
      R => '0'
    );
\autolock_input_min_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(13),
      Q => autolock_input_min(13),
      R => '0'
    );
\autolock_input_min_reg[13]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \autolock_input_min_reg[13]_i_3_n_0\,
      CO(3) => \NLW_autolock_input_min_reg[13]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \autolock_input_min_reg[13]_i_2_n_1\,
      CO(1) => \autolock_input_min_reg[13]_i_2_n_2\,
      CO(0) => \autolock_input_min_reg[13]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \autolock_input_min[13]_i_4_n_0\,
      DI(1) => \autolock_input_min[13]_i_5_n_0\,
      DI(0) => \autolock_input_min[13]_i_6_n_0\,
      O(3 downto 0) => \NLW_autolock_input_min_reg[13]_i_2_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \autolock_input_min[13]_i_7_n_0\,
      S(1) => \autolock_input_min[13]_i_8_n_0\,
      S(0) => \autolock_input_min[13]_i_9_n_0\
    );
\autolock_input_min_reg[13]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \autolock_input_min_reg[13]_i_3_n_0\,
      CO(2) => \autolock_input_min_reg[13]_i_3_n_1\,
      CO(1) => \autolock_input_min_reg[13]_i_3_n_2\,
      CO(0) => \autolock_input_min_reg[13]_i_3_n_3\,
      CYINIT => '0',
      DI(3) => \autolock_input_min[13]_i_10_n_0\,
      DI(2) => \autolock_input_min[13]_i_11_n_0\,
      DI(1) => \autolock_input_min[13]_i_12_n_0\,
      DI(0) => \autolock_input_min[13]_i_13_n_0\,
      O(3 downto 0) => \NLW_autolock_input_min_reg[13]_i_3_O_UNCONNECTED\(3 downto 0),
      S(3) => \autolock_input_min[13]_i_14_n_0\,
      S(2) => \autolock_input_min[13]_i_15_n_0\,
      S(1) => \autolock_input_min[13]_i_16_n_0\,
      S(0) => \autolock_input_min[13]_i_17_n_0\
    );
\autolock_input_min_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(1),
      Q => autolock_input_min(1),
      R => '0'
    );
\autolock_input_min_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(2),
      Q => autolock_input_min(2),
      R => '0'
    );
\autolock_input_min_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(3),
      Q => autolock_input_min(3),
      R => '0'
    );
\autolock_input_min_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(4),
      Q => autolock_input_min(4),
      R => '0'
    );
\autolock_input_min_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(5),
      Q => autolock_input_min(5),
      R => '0'
    );
\autolock_input_min_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(6),
      Q => autolock_input_min(6),
      R => '0'
    );
\autolock_input_min_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(7),
      Q => autolock_input_min(7),
      R => '0'
    );
\autolock_input_min_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(8),
      Q => autolock_input_min(8),
      R => '0'
    );
\autolock_input_min_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_0_input(9),
      Q => autolock_input_min(9),
      R => '0'
    );
\autolock_input_mindex_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(0),
      Q => \autolock_input_mindex_reg_n_0_[0]\,
      R => '0'
    );
\autolock_input_mindex_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(10),
      Q => \autolock_input_mindex_reg_n_0_[10]\,
      R => '0'
    );
\autolock_input_mindex_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(11),
      Q => \autolock_input_mindex_reg_n_0_[11]\,
      R => '0'
    );
\autolock_input_mindex_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(12),
      Q => \autolock_input_mindex_reg_n_0_[12]\,
      R => '0'
    );
\autolock_input_mindex_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(13),
      Q => p_0_in0,
      R => '0'
    );
\autolock_input_mindex_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(1),
      Q => \autolock_input_mindex_reg_n_0_[1]\,
      R => '0'
    );
\autolock_input_mindex_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(2),
      Q => \autolock_input_mindex_reg_n_0_[2]\,
      R => '0'
    );
\autolock_input_mindex_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(3),
      Q => \autolock_input_mindex_reg_n_0_[3]\,
      R => '0'
    );
\autolock_input_mindex_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(4),
      Q => \autolock_input_mindex_reg_n_0_[4]\,
      R => '0'
    );
\autolock_input_mindex_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(5),
      Q => \autolock_input_mindex_reg_n_0_[5]\,
      R => '0'
    );
\autolock_input_mindex_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(6),
      Q => \autolock_input_mindex_reg_n_0_[6]\,
      R => '0'
    );
\autolock_input_mindex_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(7),
      Q => \autolock_input_mindex_reg_n_0_[7]\,
      R => '0'
    );
\autolock_input_mindex_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(8),
      Q => \autolock_input_mindex_reg_n_0_[8]\,
      R => '0'
    );
\autolock_input_mindex_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => autolock_input_mindex,
      D => autolock_input_maxdex0_in(9),
      Q => \autolock_input_mindex_reg_n_0_[9]\,
      R => '0'
    );
\autolock_input_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(0),
      Q => autolock_input_reg(0),
      R => '0'
    );
\autolock_input_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(10),
      Q => autolock_input_reg(10),
      R => '0'
    );
\autolock_input_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(11),
      Q => autolock_input_reg(11),
      R => '0'
    );
\autolock_input_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(12),
      Q => autolock_input_reg(12),
      R => '0'
    );
\autolock_input_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(13),
      Q => autolock_input_reg(13),
      R => '0'
    );
\autolock_input_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(1),
      Q => autolock_input_reg(1),
      R => '0'
    );
\autolock_input_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(2),
      Q => autolock_input_reg(2),
      R => '0'
    );
\autolock_input_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(3),
      Q => autolock_input_reg(3),
      R => '0'
    );
\autolock_input_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(4),
      Q => autolock_input_reg(4),
      R => '0'
    );
\autolock_input_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(5),
      Q => autolock_input_reg(5),
      R => '0'
    );
\autolock_input_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(6),
      Q => autolock_input_reg(6),
      R => '0'
    );
\autolock_input_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(7),
      Q => autolock_input_reg(7),
      R => '0'
    );
\autolock_input_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(8),
      Q => autolock_input_reg(8),
      R => '0'
    );
\autolock_input_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock_0_input(9),
      Q => autolock_input_reg(9),
      R => '0'
    );
\autolock_max_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \autolock_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \autolock_max_reg[15]_i_1_n_0\
    );
\autolock_max_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \autolock_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \autolock_max_reg[23]_i_1_n_0\
    );
\autolock_max_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \autolock_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \autolock_max_reg[31]_i_1_n_0\
    );
\autolock_max_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \autolock_max_reg[31]_i_2_n_0\
    );
\autolock_max_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \autolock_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \autolock_max_reg[7]_i_1_n_0\
    );
\autolock_max_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => autolock_max_reg(0),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => autolock_max_reg(10),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => autolock_max_reg(11),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => autolock_max_reg(12),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => autolock_max_reg(13),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => autolock_max_reg(14),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => autolock_max_reg(15),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => autolock_max_reg(16),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => autolock_max_reg(17),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => autolock_max_reg(18),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => autolock_max_reg(19),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => autolock_max_reg(1),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => autolock_max_reg(20),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => autolock_max_reg(21),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => autolock_max_reg(22),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => autolock_max_reg(23),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => autolock_max_reg(24),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => autolock_max_reg(25),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => autolock_max_reg(26),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => autolock_max_reg(27),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => autolock_max_reg(28),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => autolock_max_reg(29),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => autolock_max_reg(2),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => autolock_max_reg(30),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => autolock_max_reg(31),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => autolock_max_reg(3),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => autolock_max_reg(4),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => autolock_max_reg(5),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => autolock_max_reg(6),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => autolock_max_reg(7),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => autolock_max_reg(8),
      R => pid_loop_0_rst1
    );
\autolock_max_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => autolock_max_reg(9),
      R => pid_loop_0_rst1
    );
\autolock_min_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \autolock_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \autolock_min_reg[15]_i_1_n_0\
    );
\autolock_min_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \autolock_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \autolock_min_reg[23]_i_1_n_0\
    );
\autolock_min_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \autolock_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \autolock_min_reg[31]_i_1_n_0\
    );
\autolock_min_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000200000000000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \autolock_min_reg[31]_i_2_n_0\
    );
\autolock_min_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \autolock_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \autolock_min_reg[7]_i_1_n_0\
    );
\autolock_min_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => autolock_min_reg(0),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => autolock_min_reg(10),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => autolock_min_reg(11),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => autolock_min_reg(12),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => autolock_min_reg(13),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => autolock_min_reg(14),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => autolock_min_reg(15),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => autolock_min_reg(16),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => autolock_min_reg(17),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => autolock_min_reg(18),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => autolock_min_reg(19),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => autolock_min_reg(1),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => autolock_min_reg(20),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => autolock_min_reg(21),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => autolock_min_reg(22),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => autolock_min_reg(23),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => autolock_min_reg(24),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => autolock_min_reg(25),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => autolock_min_reg(26),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => autolock_min_reg(27),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => autolock_min_reg(28),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => autolock_min_reg(29),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => autolock_min_reg(2),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => autolock_min_reg(30),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => autolock_min_reg(31),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => autolock_min_reg(3),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => autolock_min_reg(4),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => autolock_min_reg(5),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => autolock_min_reg(6),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => autolock_min_reg(7),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => autolock_min_reg(8),
      R => pid_loop_0_rst1
    );
\autolock_min_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \autolock_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => autolock_min_reg(9),
      R => pid_loop_0_rst1
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_reg_1,
      Q => \^aw_en_reg_0\,
      S => pid_loop_0_rst1
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(0),
      Q => sel0(0),
      R => pid_loop_0_rst1
    );
\axi_araddr_reg[2]_rep\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(0),
      Q => \axi_araddr_reg[2]_rep_n_0\,
      R => pid_loop_0_rst1
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(1),
      Q => sel0(1),
      R => pid_loop_0_rst1
    );
\axi_araddr_reg[3]_rep\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(1),
      Q => \axi_araddr_reg[3]_rep_n_0\,
      R => pid_loop_0_rst1
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(2),
      Q => sel0(2),
      R => pid_loop_0_rst1
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(3),
      Q => sel0(3),
      R => pid_loop_0_rst1
    );
\axi_araddr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(4),
      Q => sel0(4),
      R => pid_loop_0_rst1
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^axi_arready_reg_0\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^axi_arready_reg_0\,
      R => pid_loop_0_rst1
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(0),
      Q => p_0_in(0),
      R => pid_loop_0_rst1
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(1),
      Q => p_0_in(1),
      R => pid_loop_0_rst1
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(2),
      Q => p_0_in(2),
      R => pid_loop_0_rst1
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(3),
      Q => p_0_in(3),
      R => pid_loop_0_rst1
    );
\axi_awaddr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(4),
      Q => p_0_in(4),
      R => pid_loop_0_rst1
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => pid_loop_0_rst1
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \^aw_en_reg_0\,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^axi_awready_reg_0\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^axi_awready_reg_0\,
      R => pid_loop_0_rst1
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_reg_0,
      Q => s00_axi_bvalid,
      R => pid_loop_0_rst1
    );
\axi_pi_output_reg[13]_inv_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => output_sel,
      I1 => \control_reg_reg_n_0_[0]\,
      O => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_27,
      Q => axi_pi_output(0),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_17,
      Q => axi_pi_output(10),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_16,
      Q => axi_pi_output(11),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_15,
      Q => axi_pi_output(12),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[13]_inv\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_28,
      Q => axi_pi_output(13),
      S => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_26,
      Q => axi_pi_output(1),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_25,
      Q => axi_pi_output(2),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_24,
      Q => axi_pi_output(3),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_23,
      Q => axi_pi_output(4),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_22,
      Q => axi_pi_output(5),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_21,
      Q => axi_pi_output(6),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_20,
      Q => axi_pi_output(7),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_19,
      Q => axi_pi_output(8),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_pi_output_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_n_18,
      Q => axi_pi_output(9),
      R => \axi_pi_output_reg[13]_inv_i_1_n_0\
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[0]_i_2_n_0\,
      I1 => \axi_rdata[0]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[0]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[0]_i_5_n_0\,
      O => reg_data_out(0)
    );
\axi_rdata[0]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => setpoint_reg(0),
      I1 => status_reg(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \control_reg_reg_n_0_[0]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[0]_i_10_n_0\
    );
\axi_rdata[0]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(0),
      I1 => I_reg(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(0),
      O => \axi_rdata[0]_i_11_n_0\
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[0]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[0]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[0]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(0),
      I1 => ramplitude_step_reg(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(0),
      O => \axi_rdata[0]_i_6_n_0\
    );
\axi_rdata[0]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(0),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(0),
      I5 => pid_loop_0_input_reg(0),
      O => \axi_rdata[0]_i_7_n_0\
    );
\axi_rdata[0]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(0),
      I1 => ramp_step_reg(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(0),
      O => \axi_rdata[0]_i_8_n_0\
    );
\axi_rdata[0]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(0),
      I1 => loop_locked_max_reg(0),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(0),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(0),
      O => \axi_rdata[0]_i_9_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[10]_i_2_n_0\,
      I1 => \axi_rdata[10]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[10]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[10]_i_5_n_0\,
      O => reg_data_out(10)
    );
\axi_rdata[10]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => setpoint_reg(10),
      I1 => status_reg(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \control_reg_reg_n_0_[10]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[10]_i_10_n_0\
    );
\axi_rdata[10]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(10),
      I1 => I_reg(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(10),
      O => \axi_rdata[10]_i_11_n_0\
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[10]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[10]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[10]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(10),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[10]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(10),
      I1 => ramplitude_step_reg(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(10),
      O => \axi_rdata[10]_i_6_n_0\
    );
\axi_rdata[10]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(10),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(10),
      I5 => pid_loop_0_input_reg(10),
      O => \axi_rdata[10]_i_7_n_0\
    );
\axi_rdata[10]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(10),
      I1 => ramp_step_reg(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(10),
      O => \axi_rdata[10]_i_8_n_0\
    );
\axi_rdata[10]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(10),
      I1 => loop_locked_max_reg(10),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(10),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(10),
      O => \axi_rdata[10]_i_9_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[11]_i_2_n_0\,
      I1 => \axi_rdata[11]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[11]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[11]_i_5_n_0\,
      O => reg_data_out(11)
    );
\axi_rdata[11]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => setpoint_reg(11),
      I1 => status_reg(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \control_reg_reg_n_0_[11]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[11]_i_10_n_0\
    );
\axi_rdata[11]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(11),
      I1 => I_reg(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(11),
      O => \axi_rdata[11]_i_11_n_0\
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[11]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[11]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[11]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(11),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[11]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(11),
      I1 => ramplitude_step_reg(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(11),
      O => \axi_rdata[11]_i_6_n_0\
    );
\axi_rdata[11]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(11),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(11),
      I5 => pid_loop_0_input_reg(11),
      O => \axi_rdata[11]_i_7_n_0\
    );
\axi_rdata[11]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(11),
      I1 => ramp_step_reg(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(11),
      O => \axi_rdata[11]_i_8_n_0\
    );
\axi_rdata[11]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(11),
      I1 => loop_locked_max_reg(11),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(11),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(11),
      O => \axi_rdata[11]_i_9_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[12]_i_2_n_0\,
      I1 => \axi_rdata[12]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[12]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[12]_i_5_n_0\,
      O => reg_data_out(12)
    );
\axi_rdata[12]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(12),
      I1 => I_reg(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(12),
      O => \axi_rdata[12]_i_10_n_0\
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[12]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[12]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[12]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(12),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[12]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[12]_i_10_n_0\,
      I1 => sel0(2),
      I2 => setpoint_reg(12),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => \control_reg_reg_n_0_[12]\,
      I5 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[12]_i_5_n_0\
    );
\axi_rdata[12]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(12),
      I1 => ramplitude_step_reg(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(12),
      O => \axi_rdata[12]_i_6_n_0\
    );
\axi_rdata[12]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(12),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(12),
      I5 => pid_loop_0_input_reg(12),
      O => \axi_rdata[12]_i_7_n_0\
    );
\axi_rdata[12]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(12),
      I1 => ramp_step_reg(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(12),
      O => \axi_rdata[12]_i_8_n_0\
    );
\axi_rdata[12]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(12),
      I1 => loop_locked_max_reg(12),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(12),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(12),
      O => \axi_rdata[12]_i_9_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[13]_i_2_n_0\,
      I1 => \axi_rdata[15]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[13]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[13]_i_4_n_0\,
      O => reg_data_out(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[13]_i_5_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(13),
      I4 => sel0(0),
      I5 => input_railed_min_reg(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[13]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[13]_i_8_n_0\,
      I1 => sel0(2),
      I2 => setpoint_reg(13),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[13]\,
      I5 => sel0(0),
      O => \axi_rdata[13]_i_4_n_0\
    );
\axi_rdata[13]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(13),
      I1 => ramplitude_step_reg(13),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[13]_i_5_n_0\
    );
\axi_rdata[13]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(13),
      I1 => ramp_step_reg(13),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(13),
      O => \axi_rdata[13]_i_6_n_0\
    );
\axi_rdata[13]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(13),
      I1 => loop_locked_max_reg(13),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(13),
      I4 => sel0(0),
      I5 => autolock_max_reg(13),
      O => \axi_rdata[13]_i_7_n_0\
    );
\axi_rdata[13]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(13),
      I1 => I_reg(13),
      I2 => sel0(1),
      I3 => P_mon_reg(13),
      I4 => sel0(0),
      I5 => P_reg(13),
      O => \axi_rdata[13]_i_8_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[14]_i_2_n_0\,
      I1 => \axi_rdata[15]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[14]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[14]_i_4_n_0\,
      O => reg_data_out(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[14]_i_5_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(14),
      I4 => sel0(0),
      I5 => input_railed_min_reg(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[14]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[14]_i_8_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(14),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[14]\,
      I5 => sel0(0),
      O => \axi_rdata[14]_i_4_n_0\
    );
\axi_rdata[14]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(14),
      I1 => ramplitude_step_reg(14),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[14]_i_5_n_0\
    );
\axi_rdata[14]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(14),
      I1 => ramp_step_reg(14),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(14),
      O => \axi_rdata[14]_i_6_n_0\
    );
\axi_rdata[14]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(14),
      I1 => loop_locked_max_reg(14),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(14),
      I4 => sel0(0),
      I5 => autolock_max_reg(14),
      O => \axi_rdata[14]_i_7_n_0\
    );
\axi_rdata[14]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(14),
      I1 => I_reg(14),
      I2 => sel0(1),
      I3 => P_mon_reg(14),
      I4 => sel0(0),
      I5 => P_reg(14),
      O => \axi_rdata[14]_i_8_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata[15]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[15]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[15]_i_5_n_0\,
      O => reg_data_out(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[15]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(15),
      I4 => sel0(0),
      I5 => input_railed_min_reg(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[15]\,
      I4 => sel0(0),
      I5 => mindex_reg(15),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[15]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(15),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[15]\,
      I5 => sel0(0),
      O => \axi_rdata[15]_i_5_n_0\
    );
\axi_rdata[15]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(15),
      I1 => ramplitude_step_reg(15),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[15]_i_6_n_0\
    );
\axi_rdata[15]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(15),
      I1 => ramp_step_reg(15),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(15),
      O => \axi_rdata[15]_i_7_n_0\
    );
\axi_rdata[15]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(15),
      I1 => loop_locked_max_reg(15),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(15),
      I4 => sel0(0),
      I5 => autolock_max_reg(15),
      O => \axi_rdata[15]_i_8_n_0\
    );
\axi_rdata[15]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(15),
      I1 => I_reg(15),
      I2 => sel0(1),
      I3 => P_mon_reg(15),
      I4 => sel0(0),
      I5 => P_reg(15),
      O => \axi_rdata[15]_i_9_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[16]_i_2_n_0\,
      I1 => \axi_rdata[16]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[16]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[16]_i_5_n_0\,
      O => reg_data_out(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[16]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(16),
      I4 => sel0(0),
      I5 => input_railed_min_reg(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[16]\,
      I4 => sel0(0),
      I5 => mindex_reg(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[16]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[16]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(16),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[16]\,
      I5 => sel0(0),
      O => \axi_rdata[16]_i_5_n_0\
    );
\axi_rdata[16]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(16),
      I1 => ramplitude_step_reg(16),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[16]_i_6_n_0\
    );
\axi_rdata[16]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(16),
      I1 => ramp_step_reg(16),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(16),
      O => \axi_rdata[16]_i_7_n_0\
    );
\axi_rdata[16]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(16),
      I1 => loop_locked_max_reg(16),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(16),
      I4 => sel0(0),
      I5 => autolock_max_reg(16),
      O => \axi_rdata[16]_i_8_n_0\
    );
\axi_rdata[16]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(16),
      I1 => I_reg(16),
      I2 => sel0(1),
      I3 => P_mon_reg(16),
      I4 => sel0(0),
      I5 => P_reg(16),
      O => \axi_rdata[16]_i_9_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[17]_i_2_n_0\,
      I1 => \axi_rdata[17]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[17]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[17]_i_5_n_0\,
      O => reg_data_out(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[17]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(17),
      I4 => sel0(0),
      I5 => input_railed_min_reg(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[17]\,
      I4 => sel0(0),
      I5 => mindex_reg(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[17]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[17]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(17),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[17]\,
      I5 => sel0(0),
      O => \axi_rdata[17]_i_5_n_0\
    );
\axi_rdata[17]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(17),
      I1 => ramplitude_step_reg(17),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[17]_i_6_n_0\
    );
\axi_rdata[17]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(17),
      I1 => ramp_step_reg(17),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(17),
      O => \axi_rdata[17]_i_7_n_0\
    );
\axi_rdata[17]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(17),
      I1 => loop_locked_max_reg(17),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(17),
      I4 => sel0(0),
      I5 => autolock_max_reg(17),
      O => \axi_rdata[17]_i_8_n_0\
    );
\axi_rdata[17]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(17),
      I1 => I_reg(17),
      I2 => sel0(1),
      I3 => P_mon_reg(17),
      I4 => sel0(0),
      I5 => P_reg(17),
      O => \axi_rdata[17]_i_9_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[18]_i_2_n_0\,
      I1 => \axi_rdata[18]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[18]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[18]_i_5_n_0\,
      O => reg_data_out(18)
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[18]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(18),
      I4 => sel0(0),
      I5 => input_railed_min_reg(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[18]\,
      I4 => sel0(0),
      I5 => mindex_reg(18),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[18]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[18]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(18),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[18]\,
      I5 => sel0(0),
      O => \axi_rdata[18]_i_5_n_0\
    );
\axi_rdata[18]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(18),
      I1 => ramplitude_step_reg(18),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[18]_i_6_n_0\
    );
\axi_rdata[18]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(18),
      I1 => ramp_step_reg(18),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(18),
      O => \axi_rdata[18]_i_7_n_0\
    );
\axi_rdata[18]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(18),
      I1 => loop_locked_max_reg(18),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(18),
      I4 => sel0(0),
      I5 => autolock_max_reg(18),
      O => \axi_rdata[18]_i_8_n_0\
    );
\axi_rdata[18]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(18),
      I1 => I_reg(18),
      I2 => sel0(1),
      I3 => P_mon_reg(18),
      I4 => sel0(0),
      I5 => P_reg(18),
      O => \axi_rdata[18]_i_9_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[19]_i_2_n_0\,
      I1 => \axi_rdata[19]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[19]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[19]_i_5_n_0\,
      O => reg_data_out(19)
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[19]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(19),
      I4 => sel0(0),
      I5 => input_railed_min_reg(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[19]\,
      I4 => sel0(0),
      I5 => mindex_reg(19),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[19]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[19]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(19),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[19]\,
      I5 => sel0(0),
      O => \axi_rdata[19]_i_5_n_0\
    );
\axi_rdata[19]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(19),
      I1 => ramplitude_step_reg(19),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[19]_i_6_n_0\
    );
\axi_rdata[19]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(19),
      I1 => ramp_step_reg(19),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(19),
      O => \axi_rdata[19]_i_7_n_0\
    );
\axi_rdata[19]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(19),
      I1 => loop_locked_max_reg(19),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(19),
      I4 => sel0(0),
      I5 => autolock_max_reg(19),
      O => \axi_rdata[19]_i_8_n_0\
    );
\axi_rdata[19]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(19),
      I1 => I_reg(19),
      I2 => sel0(1),
      I3 => P_mon_reg(19),
      I4 => sel0(0),
      I5 => P_reg(19),
      O => \axi_rdata[19]_i_9_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[1]_i_2_n_0\,
      I1 => \axi_rdata[1]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[1]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[1]_i_5_n_0\,
      O => reg_data_out(1)
    );
\axi_rdata[1]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => setpoint_reg(1),
      I1 => status_reg(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => p_0_in1_in,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[1]_i_10_n_0\
    );
\axi_rdata[1]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(1),
      I1 => I_reg(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(1),
      O => \axi_rdata[1]_i_11_n_0\
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[1]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[1]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[1]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(1),
      I1 => ramplitude_step_reg(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(1),
      O => \axi_rdata[1]_i_6_n_0\
    );
\axi_rdata[1]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(1),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(1),
      I5 => pid_loop_0_input_reg(1),
      O => \axi_rdata[1]_i_7_n_0\
    );
\axi_rdata[1]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(1),
      I1 => ramp_step_reg(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(1),
      O => \axi_rdata[1]_i_8_n_0\
    );
\axi_rdata[1]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(1),
      I1 => loop_locked_max_reg(1),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(1),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(1),
      O => \axi_rdata[1]_i_9_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[20]_i_2_n_0\,
      I1 => \axi_rdata[20]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[20]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[20]_i_5_n_0\,
      O => reg_data_out(20)
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[20]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(20),
      I4 => sel0(0),
      I5 => input_railed_min_reg(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[20]\,
      I4 => sel0(0),
      I5 => mindex_reg(20),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[20]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[20]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(20),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[20]\,
      I5 => sel0(0),
      O => \axi_rdata[20]_i_5_n_0\
    );
\axi_rdata[20]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(20),
      I1 => ramplitude_step_reg(20),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[20]_i_6_n_0\
    );
\axi_rdata[20]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(20),
      I1 => ramp_step_reg(20),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(20),
      O => \axi_rdata[20]_i_7_n_0\
    );
\axi_rdata[20]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(20),
      I1 => loop_locked_max_reg(20),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(20),
      I4 => sel0(0),
      I5 => autolock_max_reg(20),
      O => \axi_rdata[20]_i_8_n_0\
    );
\axi_rdata[20]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(20),
      I1 => I_reg(20),
      I2 => sel0(1),
      I3 => P_mon_reg(20),
      I4 => sel0(0),
      I5 => P_reg(20),
      O => \axi_rdata[20]_i_9_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[21]_i_2_n_0\,
      I1 => \axi_rdata[21]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[21]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[21]_i_5_n_0\,
      O => reg_data_out(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[21]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(21),
      I4 => sel0(0),
      I5 => input_railed_min_reg(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[21]\,
      I4 => sel0(0),
      I5 => mindex_reg(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[21]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[21]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(21),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[21]\,
      I5 => sel0(0),
      O => \axi_rdata[21]_i_5_n_0\
    );
\axi_rdata[21]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(21),
      I1 => ramplitude_step_reg(21),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[21]_i_6_n_0\
    );
\axi_rdata[21]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(21),
      I1 => ramp_step_reg(21),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(21),
      O => \axi_rdata[21]_i_7_n_0\
    );
\axi_rdata[21]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(21),
      I1 => loop_locked_max_reg(21),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(21),
      I4 => sel0(0),
      I5 => autolock_max_reg(21),
      O => \axi_rdata[21]_i_8_n_0\
    );
\axi_rdata[21]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(21),
      I1 => I_reg(21),
      I2 => sel0(1),
      I3 => P_mon_reg(21),
      I4 => sel0(0),
      I5 => P_reg(21),
      O => \axi_rdata[21]_i_9_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[22]_i_2_n_0\,
      I1 => \axi_rdata[22]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[22]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[22]_i_5_n_0\,
      O => reg_data_out(22)
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[22]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(22),
      I4 => sel0(0),
      I5 => input_railed_min_reg(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[22]\,
      I4 => sel0(0),
      I5 => mindex_reg(22),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[22]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[22]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(22),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[22]\,
      I5 => sel0(0),
      O => \axi_rdata[22]_i_5_n_0\
    );
\axi_rdata[22]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(22),
      I1 => ramplitude_step_reg(22),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[22]_i_6_n_0\
    );
\axi_rdata[22]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(22),
      I1 => ramp_step_reg(22),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(22),
      O => \axi_rdata[22]_i_7_n_0\
    );
\axi_rdata[22]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(22),
      I1 => loop_locked_max_reg(22),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(22),
      I4 => sel0(0),
      I5 => autolock_max_reg(22),
      O => \axi_rdata[22]_i_8_n_0\
    );
\axi_rdata[22]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(22),
      I1 => I_reg(22),
      I2 => sel0(1),
      I3 => P_mon_reg(22),
      I4 => sel0(0),
      I5 => P_reg(22),
      O => \axi_rdata[22]_i_9_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[23]_i_2_n_0\,
      I1 => \axi_rdata[23]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[23]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[23]_i_5_n_0\,
      O => reg_data_out(23)
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[23]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(23),
      I4 => sel0(0),
      I5 => input_railed_min_reg(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[23]\,
      I4 => sel0(0),
      I5 => mindex_reg(23),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[23]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(23),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[23]\,
      I5 => sel0(0),
      O => \axi_rdata[23]_i_5_n_0\
    );
\axi_rdata[23]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(23),
      I1 => ramplitude_step_reg(23),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[23]_i_6_n_0\
    );
\axi_rdata[23]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(23),
      I1 => ramp_step_reg(23),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(23),
      O => \axi_rdata[23]_i_7_n_0\
    );
\axi_rdata[23]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(23),
      I1 => loop_locked_max_reg(23),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(23),
      I4 => sel0(0),
      I5 => autolock_max_reg(23),
      O => \axi_rdata[23]_i_8_n_0\
    );
\axi_rdata[23]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(23),
      I1 => I_reg(23),
      I2 => sel0(1),
      I3 => P_mon_reg(23),
      I4 => sel0(0),
      I5 => P_reg(23),
      O => \axi_rdata[23]_i_9_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[24]_i_2_n_0\,
      I1 => \axi_rdata[24]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[24]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[24]_i_5_n_0\,
      O => reg_data_out(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[24]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(24),
      I4 => sel0(0),
      I5 => input_railed_min_reg(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[24]\,
      I4 => sel0(0),
      I5 => mindex_reg(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[24]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[24]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(24),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[24]\,
      I5 => sel0(0),
      O => \axi_rdata[24]_i_5_n_0\
    );
\axi_rdata[24]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(24),
      I1 => ramplitude_step_reg(24),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[24]_i_6_n_0\
    );
\axi_rdata[24]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(24),
      I1 => ramp_step_reg(24),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(24),
      O => \axi_rdata[24]_i_7_n_0\
    );
\axi_rdata[24]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(24),
      I1 => loop_locked_max_reg(24),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(24),
      I4 => sel0(0),
      I5 => autolock_max_reg(24),
      O => \axi_rdata[24]_i_8_n_0\
    );
\axi_rdata[24]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(24),
      I1 => I_reg(24),
      I2 => sel0(1),
      I3 => P_mon_reg(24),
      I4 => sel0(0),
      I5 => P_reg(24),
      O => \axi_rdata[24]_i_9_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[25]_i_2_n_0\,
      I1 => \axi_rdata[25]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[25]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[25]_i_5_n_0\,
      O => reg_data_out(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[25]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(25),
      I4 => sel0(0),
      I5 => input_railed_min_reg(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[25]\,
      I4 => sel0(0),
      I5 => mindex_reg(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[25]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[25]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(25),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[25]\,
      I5 => sel0(0),
      O => \axi_rdata[25]_i_5_n_0\
    );
\axi_rdata[25]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(25),
      I1 => ramplitude_step_reg(25),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[25]_i_6_n_0\
    );
\axi_rdata[25]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(25),
      I1 => ramp_step_reg(25),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(25),
      O => \axi_rdata[25]_i_7_n_0\
    );
\axi_rdata[25]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(25),
      I1 => loop_locked_max_reg(25),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(25),
      I4 => sel0(0),
      I5 => autolock_max_reg(25),
      O => \axi_rdata[25]_i_8_n_0\
    );
\axi_rdata[25]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(25),
      I1 => I_reg(25),
      I2 => sel0(1),
      I3 => P_mon_reg(25),
      I4 => sel0(0),
      I5 => P_reg(25),
      O => \axi_rdata[25]_i_9_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[26]_i_2_n_0\,
      I1 => \axi_rdata[26]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[26]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[26]_i_5_n_0\,
      O => reg_data_out(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[26]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(26),
      I4 => sel0(0),
      I5 => input_railed_min_reg(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[26]\,
      I4 => sel0(0),
      I5 => mindex_reg(26),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[26]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[26]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(26),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[26]\,
      I5 => sel0(0),
      O => \axi_rdata[26]_i_5_n_0\
    );
\axi_rdata[26]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(26),
      I1 => ramplitude_step_reg(26),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[26]_i_6_n_0\
    );
\axi_rdata[26]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(26),
      I1 => ramp_step_reg(26),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(26),
      O => \axi_rdata[26]_i_7_n_0\
    );
\axi_rdata[26]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(26),
      I1 => loop_locked_max_reg(26),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(26),
      I4 => sel0(0),
      I5 => autolock_max_reg(26),
      O => \axi_rdata[26]_i_8_n_0\
    );
\axi_rdata[26]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(26),
      I1 => I_reg(26),
      I2 => sel0(1),
      I3 => P_mon_reg(26),
      I4 => sel0(0),
      I5 => P_reg(26),
      O => \axi_rdata[26]_i_9_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[27]_i_2_n_0\,
      I1 => \axi_rdata[27]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[27]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[27]_i_5_n_0\,
      O => reg_data_out(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[27]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(27),
      I4 => sel0(0),
      I5 => input_railed_min_reg(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[27]\,
      I4 => sel0(0),
      I5 => mindex_reg(27),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[27]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[27]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(27),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[27]\,
      I5 => sel0(0),
      O => \axi_rdata[27]_i_5_n_0\
    );
\axi_rdata[27]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(27),
      I1 => ramplitude_step_reg(27),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[27]_i_6_n_0\
    );
\axi_rdata[27]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(27),
      I1 => ramp_step_reg(27),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(27),
      O => \axi_rdata[27]_i_7_n_0\
    );
\axi_rdata[27]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(27),
      I1 => loop_locked_max_reg(27),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(27),
      I4 => sel0(0),
      I5 => autolock_max_reg(27),
      O => \axi_rdata[27]_i_8_n_0\
    );
\axi_rdata[27]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(27),
      I1 => I_reg(27),
      I2 => sel0(1),
      I3 => P_mon_reg(27),
      I4 => sel0(0),
      I5 => P_reg(27),
      O => \axi_rdata[27]_i_9_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[28]_i_2_n_0\,
      I1 => \axi_rdata[28]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[28]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[28]_i_5_n_0\,
      O => reg_data_out(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[28]_i_6_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(28),
      I4 => sel0(0),
      I5 => input_railed_min_reg(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[28]\,
      I4 => sel0(0),
      I5 => mindex_reg(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[28]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[28]_i_9_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(28),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[28]\,
      I5 => sel0(0),
      O => \axi_rdata[28]_i_5_n_0\
    );
\axi_rdata[28]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(28),
      I1 => ramplitude_step_reg(28),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[28]_i_6_n_0\
    );
\axi_rdata[28]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(28),
      I1 => ramp_step_reg(28),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(28),
      O => \axi_rdata[28]_i_7_n_0\
    );
\axi_rdata[28]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(28),
      I1 => loop_locked_max_reg(28),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(28),
      I4 => sel0(0),
      I5 => autolock_max_reg(28),
      O => \axi_rdata[28]_i_8_n_0\
    );
\axi_rdata[28]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(28),
      I1 => I_reg(28),
      I2 => sel0(1),
      I3 => P_mon_reg(28),
      I4 => sel0(0),
      I5 => P_reg(28),
      O => \axi_rdata[28]_i_9_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[29]_i_2_n_0\,
      I1 => \axi_rdata[31]_i_4_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[29]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[29]_i_4_n_0\,
      O => reg_data_out(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[29]_i_5_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(29),
      I4 => sel0(0),
      I5 => input_railed_min_reg(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[29]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[29]_i_8_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(29),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[29]\,
      I5 => sel0(0),
      O => \axi_rdata[29]_i_4_n_0\
    );
\axi_rdata[29]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(29),
      I1 => ramplitude_step_reg(29),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[29]_i_5_n_0\
    );
\axi_rdata[29]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(29),
      I1 => ramp_step_reg(29),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(29),
      O => \axi_rdata[29]_i_6_n_0\
    );
\axi_rdata[29]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(29),
      I1 => loop_locked_max_reg(29),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(29),
      I4 => sel0(0),
      I5 => autolock_max_reg(29),
      O => \axi_rdata[29]_i_7_n_0\
    );
\axi_rdata[29]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(29),
      I1 => I_reg(29),
      I2 => sel0(1),
      I3 => P_mon_reg(29),
      I4 => sel0(0),
      I5 => P_reg(29),
      O => \axi_rdata[29]_i_8_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[2]_i_2_n_0\,
      I1 => \axi_rdata[2]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[2]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[2]_i_5_n_0\,
      O => reg_data_out(2)
    );
\axi_rdata[2]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(2),
      I1 => I_reg(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(2),
      O => \axi_rdata[2]_i_10_n_0\
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[2]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[2]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[2]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(2),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[2]_i_10_n_0\,
      I1 => sel0(2),
      I2 => setpoint_reg(2),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => \control_reg_reg_n_0_[2]\,
      I5 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[2]_i_5_n_0\
    );
\axi_rdata[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(2),
      I1 => ramplitude_step_reg(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(2),
      O => \axi_rdata[2]_i_6_n_0\
    );
\axi_rdata[2]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(2),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(2),
      I5 => pid_loop_0_input_reg(2),
      O => \axi_rdata[2]_i_7_n_0\
    );
\axi_rdata[2]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(2),
      I1 => ramp_step_reg(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(2),
      O => \axi_rdata[2]_i_8_n_0\
    );
\axi_rdata[2]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(2),
      I1 => loop_locked_max_reg(2),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(2),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(2),
      O => \axi_rdata[2]_i_9_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[30]_i_2_n_0\,
      I1 => \axi_rdata[31]_i_4_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[30]_i_3_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[30]_i_4_n_0\,
      O => reg_data_out(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[30]_i_5_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(30),
      I4 => sel0(0),
      I5 => input_railed_min_reg(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[30]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[30]_i_8_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(30),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[30]\,
      I5 => sel0(0),
      O => \axi_rdata[30]_i_4_n_0\
    );
\axi_rdata[30]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(30),
      I1 => ramplitude_step_reg(30),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[30]_i_5_n_0\
    );
\axi_rdata[30]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(30),
      I1 => ramp_step_reg(30),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(30),
      O => \axi_rdata[30]_i_6_n_0\
    );
\axi_rdata[30]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(30),
      I1 => loop_locked_max_reg(30),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(30),
      I4 => sel0(0),
      I5 => autolock_max_reg(30),
      O => \axi_rdata[30]_i_7_n_0\
    );
\axi_rdata[30]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(30),
      I1 => I_reg(30),
      I2 => sel0(1),
      I3 => P_mon_reg(30),
      I4 => sel0(0),
      I5 => P_reg(30),
      O => \axi_rdata[30]_i_8_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_rvalid\,
      I2 => \^axi_arready_reg_0\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(31),
      I1 => loop_locked_max_reg(31),
      I2 => sel0(1),
      I3 => loop_locked_min_reg(31),
      I4 => sel0(0),
      I5 => autolock_max_reg(31),
      O => \axi_rdata[31]_i_10_n_0\
    );
\axi_rdata[31]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(31),
      I1 => I_reg(31),
      I2 => sel0(1),
      I3 => P_mon_reg(31),
      I4 => sel0(0),
      I5 => P_reg(31),
      O => \axi_rdata[31]_i_11_n_0\
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[31]_i_3_n_0\,
      I1 => \axi_rdata[31]_i_4_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[31]_i_5_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[31]_i_6_n_0\,
      O => reg_data_out(31)
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[31]_i_7_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => input_railed_max_reg(31),
      I4 => sel0(0),
      I5 => input_railed_min_reg(31),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[31]_i_8_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[31]\,
      I4 => sel0(0),
      I5 => mindex_reg(31),
      O => \axi_rdata[31]_i_4_n_0\
    );
\axi_rdata[31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[31]_i_11_n_0\,
      I1 => sel0(2),
      I2 => \setpoint_reg__0\(31),
      I3 => sel0(1),
      I4 => \control_reg_reg_n_0_[31]\,
      I5 => sel0(0),
      O => \axi_rdata[31]_i_6_n_0\
    );
\axi_rdata[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(31),
      I1 => ramplitude_step_reg(31),
      I2 => sel0(1),
      I3 => autolock_input_reg(13),
      I4 => sel0(0),
      I5 => ramp_output_reg(31),
      O => \axi_rdata[31]_i_7_n_0\
    );
\axi_rdata[31]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => sel0(0),
      I2 => error_reg(31),
      I3 => sel0(1),
      I4 => loop_output_reg(31),
      I5 => pid_loop_0_input_reg(13),
      O => \axi_rdata[31]_i_8_n_0\
    );
\axi_rdata[31]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(31),
      I1 => ramp_step_reg(31),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => ramplitude_reg(31),
      O => \axi_rdata[31]_i_9_n_0\
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[3]_i_2_n_0\,
      I1 => \axi_rdata[3]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[3]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[3]_i_5_n_0\,
      O => reg_data_out(3)
    );
\axi_rdata[3]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(3),
      I1 => I_reg(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(3),
      O => \axi_rdata[3]_i_10_n_0\
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[3]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[3]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[3]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(3),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[3]_i_10_n_0\,
      I1 => sel0(2),
      I2 => setpoint_reg(3),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => \control_reg_reg_n_0_[3]\,
      I5 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[3]_i_5_n_0\
    );
\axi_rdata[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(3),
      I1 => ramplitude_step_reg(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(3),
      O => \axi_rdata[3]_i_6_n_0\
    );
\axi_rdata[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(3),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(3),
      I5 => pid_loop_0_input_reg(3),
      O => \axi_rdata[3]_i_7_n_0\
    );
\axi_rdata[3]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(3),
      I1 => ramp_step_reg(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(3),
      O => \axi_rdata[3]_i_8_n_0\
    );
\axi_rdata[3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(3),
      I1 => loop_locked_max_reg(3),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(3),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(3),
      O => \axi_rdata[3]_i_9_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => \axi_rdata[4]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[4]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[4]_i_5_n_0\,
      O => reg_data_out(4)
    );
\axi_rdata[4]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => setpoint_reg(4),
      I1 => status_reg(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => ramp_enable,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[4]_i_10_n_0\
    );
\axi_rdata[4]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(4),
      I1 => I_reg(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(4),
      O => \axi_rdata[4]_i_11_n_0\
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[4]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[4]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[4]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(4),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(4),
      I1 => ramplitude_step_reg(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(4),
      O => \axi_rdata[4]_i_6_n_0\
    );
\axi_rdata[4]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(4),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(4),
      I5 => pid_loop_0_input_reg(4),
      O => \axi_rdata[4]_i_7_n_0\
    );
\axi_rdata[4]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(4),
      I1 => ramp_step_reg(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(4),
      O => \axi_rdata[4]_i_8_n_0\
    );
\axi_rdata[4]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(4),
      I1 => loop_locked_max_reg(4),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(4),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(4),
      O => \axi_rdata[4]_i_9_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[5]_i_2_n_0\,
      I1 => \axi_rdata[5]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[5]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[5]_i_5_n_0\,
      O => reg_data_out(5)
    );
\axi_rdata[5]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(5),
      I1 => I_reg(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(5),
      O => \axi_rdata[5]_i_10_n_0\
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[5]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[5]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[5]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(5),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[5]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[5]_i_10_n_0\,
      I1 => sel0(2),
      I2 => setpoint_reg(5),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => \control_reg_reg_n_0_[5]\,
      I5 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[5]_i_5_n_0\
    );
\axi_rdata[5]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(5),
      I1 => ramplitude_step_reg(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(5),
      O => \axi_rdata[5]_i_6_n_0\
    );
\axi_rdata[5]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(5),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(5),
      I5 => pid_loop_0_input_reg(5),
      O => \axi_rdata[5]_i_7_n_0\
    );
\axi_rdata[5]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(5),
      I1 => ramp_step_reg(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(5),
      O => \axi_rdata[5]_i_8_n_0\
    );
\axi_rdata[5]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(5),
      I1 => loop_locked_max_reg(5),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(5),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(5),
      O => \axi_rdata[5]_i_9_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[6]_i_2_n_0\,
      I1 => \axi_rdata[6]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[6]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[6]_i_5_n_0\,
      O => reg_data_out(6)
    );
\axi_rdata[6]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(6),
      I1 => I_reg(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(6),
      O => \axi_rdata[6]_i_10_n_0\
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[6]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[6]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[6]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(6),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[6]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[6]_i_10_n_0\,
      I1 => sel0(2),
      I2 => setpoint_reg(6),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => \control_reg_reg_n_0_[6]\,
      I5 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[6]_i_5_n_0\
    );
\axi_rdata[6]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(6),
      I1 => ramplitude_step_reg(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(6),
      O => \axi_rdata[6]_i_6_n_0\
    );
\axi_rdata[6]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(6),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(6),
      I5 => pid_loop_0_input_reg(6),
      O => \axi_rdata[6]_i_7_n_0\
    );
\axi_rdata[6]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(6),
      I1 => ramp_step_reg(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(6),
      O => \axi_rdata[6]_i_8_n_0\
    );
\axi_rdata[6]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(6),
      I1 => loop_locked_max_reg(6),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(6),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(6),
      O => \axi_rdata[6]_i_9_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[7]_i_2_n_0\,
      I1 => \axi_rdata[7]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[7]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata[7]_i_5_n_0\,
      O => reg_data_out(7)
    );
\axi_rdata[7]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(7),
      I1 => I_reg(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(7),
      O => \axi_rdata[7]_i_10_n_0\
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[7]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[7]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[7]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(7),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B88888BB8888"
    )
        port map (
      I0 => \axi_rdata[7]_i_10_n_0\,
      I1 => sel0(2),
      I2 => setpoint_reg(7),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => \control_reg_reg_n_0_[7]\,
      I5 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[7]_i_5_n_0\
    );
\axi_rdata[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(7),
      I1 => ramplitude_step_reg(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(7),
      O => \axi_rdata[7]_i_6_n_0\
    );
\axi_rdata[7]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(7),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(7),
      I5 => pid_loop_0_input_reg(7),
      O => \axi_rdata[7]_i_7_n_0\
    );
\axi_rdata[7]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(7),
      I1 => ramp_step_reg(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(7),
      O => \axi_rdata[7]_i_8_n_0\
    );
\axi_rdata[7]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(7),
      I1 => loop_locked_max_reg(7),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(7),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(7),
      O => \axi_rdata[7]_i_9_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[8]_i_2_n_0\,
      I1 => \axi_rdata[8]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[8]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[8]_i_5_n_0\,
      O => reg_data_out(8)
    );
\axi_rdata[8]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => setpoint_reg(8),
      I1 => status_reg(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[8]_i_10_n_0\
    );
\axi_rdata[8]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(8),
      I1 => I_reg(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(8),
      O => \axi_rdata[8]_i_11_n_0\
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[8]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[8]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[8]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[8]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(8),
      I1 => ramplitude_step_reg(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(8),
      O => \axi_rdata[8]_i_6_n_0\
    );
\axi_rdata[8]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(8),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(8),
      I5 => pid_loop_0_input_reg(8),
      O => \axi_rdata[8]_i_7_n_0\
    );
\axi_rdata[8]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(8),
      I1 => ramp_step_reg(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(8),
      O => \axi_rdata[8]_i_8_n_0\
    );
\axi_rdata[8]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(8),
      I1 => loop_locked_max_reg(8),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(8),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(8),
      O => \axi_rdata[8]_i_9_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[9]_i_2_n_0\,
      I1 => \axi_rdata[9]_i_3_n_0\,
      I2 => sel0(3),
      I3 => \axi_rdata_reg[9]_i_4_n_0\,
      I4 => sel0(4),
      I5 => \axi_rdata_reg[9]_i_5_n_0\,
      O => reg_data_out(9)
    );
\axi_rdata[9]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => setpoint_reg(9),
      I1 => status_reg(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \control_reg_reg_n_0_[9]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      O => \axi_rdata[9]_i_10_n_0\
    );
\axi_rdata[9]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => I_mon_reg(9),
      I1 => I_reg(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => P_mon_reg(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => P_reg(9),
      O => \axi_rdata[9]_i_11_n_0\
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[9]_i_6_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => input_railed_max_reg(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => input_railed_min_reg(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => \axi_rdata[9]_i_7_n_0\,
      I1 => \axi_araddr_reg[3]_rep_n_0\,
      I2 => sel0(2),
      I3 => \maxdex_reg_reg_n_0_[9]\,
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => mindex_reg(9),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata[9]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => ramp_offset_reg(9),
      I1 => ramplitude_step_reg(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => autolock_input_reg(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => ramp_output_reg(9),
      O => \axi_rdata[9]_i_6_n_0\
    );
\axi_rdata[9]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5510441011100010"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_araddr_reg[2]_rep_n_0\,
      I2 => error_reg(9),
      I3 => \axi_araddr_reg[3]_rep_n_0\,
      I4 => loop_output_reg(9),
      I5 => pid_loop_0_input_reg(9),
      O => \axi_rdata[9]_i_7_n_0\
    );
\axi_rdata[9]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => autolock_min_reg(9),
      I1 => ramp_step_reg(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => \axi_araddr_reg[2]_rep_n_0\,
      I4 => ramplitude_reg(9),
      O => \axi_rdata[9]_i_8_n_0\
    );
\axi_rdata[9]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => loop_locked_delay_reg(9),
      I1 => loop_locked_max_reg(9),
      I2 => \axi_araddr_reg[3]_rep_n_0\,
      I3 => loop_locked_min_reg(9),
      I4 => \axi_araddr_reg[2]_rep_n_0\,
      I5 => autolock_max_reg(9),
      O => \axi_rdata[9]_i_9_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[0]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_8_n_0\,
      I1 => \axi_rdata[0]_i_9_n_0\,
      O => \axi_rdata_reg[0]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[0]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_10_n_0\,
      I1 => \axi_rdata[0]_i_11_n_0\,
      O => \axi_rdata_reg[0]_i_5_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[10]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_8_n_0\,
      I1 => \axi_rdata[10]_i_9_n_0\,
      O => \axi_rdata_reg[10]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[10]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_10_n_0\,
      I1 => \axi_rdata[10]_i_11_n_0\,
      O => \axi_rdata_reg[10]_i_5_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[11]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_8_n_0\,
      I1 => \axi_rdata[11]_i_9_n_0\,
      O => \axi_rdata_reg[11]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[11]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_10_n_0\,
      I1 => \axi_rdata[11]_i_11_n_0\,
      O => \axi_rdata_reg[11]_i_5_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[12]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_8_n_0\,
      I1 => \axi_rdata[12]_i_9_n_0\,
      O => \axi_rdata_reg[12]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[13]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_6_n_0\,
      I1 => \axi_rdata[13]_i_7_n_0\,
      O => \axi_rdata_reg[13]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[14]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_6_n_0\,
      I1 => \axi_rdata[14]_i_7_n_0\,
      O => \axi_rdata_reg[14]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[15]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_7_n_0\,
      I1 => \axi_rdata[15]_i_8_n_0\,
      O => \axi_rdata_reg[15]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[16]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_7_n_0\,
      I1 => \axi_rdata[16]_i_8_n_0\,
      O => \axi_rdata_reg[16]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[17]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_7_n_0\,
      I1 => \axi_rdata[17]_i_8_n_0\,
      O => \axi_rdata_reg[17]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[18]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_7_n_0\,
      I1 => \axi_rdata[18]_i_8_n_0\,
      O => \axi_rdata_reg[18]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[19]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_7_n_0\,
      I1 => \axi_rdata[19]_i_8_n_0\,
      O => \axi_rdata_reg[19]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[1]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_8_n_0\,
      I1 => \axi_rdata[1]_i_9_n_0\,
      O => \axi_rdata_reg[1]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[1]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_10_n_0\,
      I1 => \axi_rdata[1]_i_11_n_0\,
      O => \axi_rdata_reg[1]_i_5_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[20]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_7_n_0\,
      I1 => \axi_rdata[20]_i_8_n_0\,
      O => \axi_rdata_reg[20]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[21]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_7_n_0\,
      I1 => \axi_rdata[21]_i_8_n_0\,
      O => \axi_rdata_reg[21]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[22]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_7_n_0\,
      I1 => \axi_rdata[22]_i_8_n_0\,
      O => \axi_rdata_reg[22]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[23]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_7_n_0\,
      I1 => \axi_rdata[23]_i_8_n_0\,
      O => \axi_rdata_reg[23]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[24]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_7_n_0\,
      I1 => \axi_rdata[24]_i_8_n_0\,
      O => \axi_rdata_reg[24]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[25]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_7_n_0\,
      I1 => \axi_rdata[25]_i_8_n_0\,
      O => \axi_rdata_reg[25]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[26]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_7_n_0\,
      I1 => \axi_rdata[26]_i_8_n_0\,
      O => \axi_rdata_reg[26]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[27]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_7_n_0\,
      I1 => \axi_rdata[27]_i_8_n_0\,
      O => \axi_rdata_reg[27]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[28]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_7_n_0\,
      I1 => \axi_rdata[28]_i_8_n_0\,
      O => \axi_rdata_reg[28]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[29]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_6_n_0\,
      I1 => \axi_rdata[29]_i_7_n_0\,
      O => \axi_rdata_reg[29]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[2]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_8_n_0\,
      I1 => \axi_rdata[2]_i_9_n_0\,
      O => \axi_rdata_reg[2]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[30]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_6_n_0\,
      I1 => \axi_rdata[30]_i_7_n_0\,
      O => \axi_rdata_reg[30]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[31]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_9_n_0\,
      I1 => \axi_rdata[31]_i_10_n_0\,
      O => \axi_rdata_reg[31]_i_5_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[3]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_8_n_0\,
      I1 => \axi_rdata[3]_i_9_n_0\,
      O => \axi_rdata_reg[3]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[4]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_8_n_0\,
      I1 => \axi_rdata[4]_i_9_n_0\,
      O => \axi_rdata_reg[4]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[4]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_10_n_0\,
      I1 => \axi_rdata[4]_i_11_n_0\,
      O => \axi_rdata_reg[4]_i_5_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[5]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_8_n_0\,
      I1 => \axi_rdata[5]_i_9_n_0\,
      O => \axi_rdata_reg[5]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[6]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_8_n_0\,
      I1 => \axi_rdata[6]_i_9_n_0\,
      O => \axi_rdata_reg[6]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[7]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_8_n_0\,
      I1 => \axi_rdata[7]_i_9_n_0\,
      O => \axi_rdata_reg[7]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[8]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_8_n_0\,
      I1 => \axi_rdata[8]_i_9_n_0\,
      O => \axi_rdata_reg[8]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[8]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_10_n_0\,
      I1 => \axi_rdata[8]_i_11_n_0\,
      O => \axi_rdata_reg[8]_i_5_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => pid_loop_0_rst1
    );
\axi_rdata_reg[9]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_8_n_0\,
      I1 => \axi_rdata[9]_i_9_n_0\,
      O => \axi_rdata_reg[9]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[9]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_10_n_0\,
      I1 => \axi_rdata[9]_i_11_n_0\,
      O => \axi_rdata_reg[9]_i_5_n_0\,
      S => sel0(2)
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_reg_0,
      Q => \^s00_axi_rvalid\,
      R => pid_loop_0_rst1
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \^aw_en_reg_0\,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^axi_wready_reg_0\,
      R => pid_loop_0_rst1
    );
\control_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \control_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \control_reg[15]_i_1_n_0\
    );
\control_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \control_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \control_reg[23]_i_1_n_0\
    );
\control_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \control_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \control_reg[31]_i_1_n_0\
    );
\control_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \control_reg[31]_i_2_n_0\
    );
\control_reg[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^axi_wready_reg_0\,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^axi_awready_reg_0\,
      O => \control_reg[31]_i_3_n_0\
    );
\control_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \control_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \control_reg[7]_i_1_n_0\
    );
\control_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \control_reg_reg_n_0_[0]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \control_reg_reg_n_0_[10]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \control_reg_reg_n_0_[11]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \control_reg_reg_n_0_[12]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \control_reg_reg_n_0_[13]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \control_reg_reg_n_0_[14]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \control_reg_reg_n_0_[15]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \control_reg_reg_n_0_[16]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \control_reg_reg_n_0_[17]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \control_reg_reg_n_0_[18]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \control_reg_reg_n_0_[19]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => p_0_in1_in,
      R => pid_loop_0_rst1
    );
\control_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \control_reg_reg_n_0_[20]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \control_reg_reg_n_0_[21]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \control_reg_reg_n_0_[22]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \control_reg_reg_n_0_[23]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \control_reg_reg_n_0_[24]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \control_reg_reg_n_0_[25]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \control_reg_reg_n_0_[26]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \control_reg_reg_n_0_[27]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \control_reg_reg_n_0_[28]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \control_reg_reg_n_0_[29]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \control_reg_reg_n_0_[2]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \control_reg_reg_n_0_[30]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \control_reg_reg_n_0_[31]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \control_reg_reg_n_0_[3]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => ramp_enable,
      R => pid_loop_0_rst1
    );
\control_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \control_reg_reg_n_0_[5]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \control_reg_reg_n_0_[6]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \control_reg_reg_n_0_[7]\,
      R => pid_loop_0_rst1
    );
\control_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => autolock,
      R => pid_loop_0_rst1
    );
\control_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \control_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \control_reg_reg_n_0_[9]\,
      R => pid_loop_0_rst1
    );
\error_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(0),
      Q => error_reg(0),
      R => '0'
    );
\error_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(10),
      Q => error_reg(10),
      R => '0'
    );
\error_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(11),
      Q => error_reg(11),
      R => '0'
    );
\error_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(12),
      Q => error_reg(12),
      R => '0'
    );
\error_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(1),
      Q => error_reg(1),
      R => '0'
    );
\error_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(2),
      Q => error_reg(2),
      R => '0'
    );
\error_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(13),
      Q => error_reg(31),
      R => '0'
    );
\error_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(3),
      Q => error_reg(3),
      R => '0'
    );
\error_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(4),
      Q => error_reg(4),
      R => '0'
    );
\error_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(5),
      Q => error_reg(5),
      R => '0'
    );
\error_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(6),
      Q => error_reg(6),
      R => '0'
    );
\error_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(7),
      Q => error_reg(7),
      R => '0'
    );
\error_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(8),
      Q => error_reg(8),
      R => '0'
    );
\error_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_error(9),
      Q => error_reg(9),
      R => '0'
    );
\input_railed_max_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \input_railed_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \input_railed_max_reg[15]_i_1_n_0\
    );
\input_railed_max_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \input_railed_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \input_railed_max_reg[23]_i_1_n_0\
    );
\input_railed_max_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \input_railed_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \input_railed_max_reg[31]_i_1_n_0\
    );
\input_railed_max_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \input_railed_max_reg[31]_i_2_n_0\
    );
\input_railed_max_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \input_railed_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \input_railed_max_reg[7]_i_1_n_0\
    );
\input_railed_max_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => input_railed_max_reg(0),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => input_railed_max_reg(10),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => input_railed_max_reg(11),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => input_railed_max_reg(12),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => input_railed_max_reg(13),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => input_railed_max_reg(14),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => input_railed_max_reg(15),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => input_railed_max_reg(16),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => input_railed_max_reg(17),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => input_railed_max_reg(18),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => input_railed_max_reg(19),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => input_railed_max_reg(1),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => input_railed_max_reg(20),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => input_railed_max_reg(21),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => input_railed_max_reg(22),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => input_railed_max_reg(23),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => input_railed_max_reg(24),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => input_railed_max_reg(25),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => input_railed_max_reg(26),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => input_railed_max_reg(27),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => input_railed_max_reg(28),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => input_railed_max_reg(29),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => input_railed_max_reg(2),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => input_railed_max_reg(30),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => input_railed_max_reg(31),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => input_railed_max_reg(3),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => input_railed_max_reg(4),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => input_railed_max_reg(5),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => input_railed_max_reg(6),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => input_railed_max_reg(7),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => input_railed_max_reg(8),
      R => pid_loop_0_rst1
    );
\input_railed_max_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => input_railed_max_reg(9),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \input_railed_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \input_railed_min_reg[15]_i_1_n_0\
    );
\input_railed_min_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \input_railed_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \input_railed_min_reg[23]_i_1_n_0\
    );
\input_railed_min_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \input_railed_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \input_railed_min_reg[31]_i_1_n_0\
    );
\input_railed_min_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \input_railed_min_reg[31]_i_2_n_0\
    );
\input_railed_min_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \input_railed_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \input_railed_min_reg[7]_i_1_n_0\
    );
\input_railed_min_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => input_railed_min_reg(0),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => input_railed_min_reg(10),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => input_railed_min_reg(11),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => input_railed_min_reg(12),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => input_railed_min_reg(13),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => input_railed_min_reg(14),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => input_railed_min_reg(15),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => input_railed_min_reg(16),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => input_railed_min_reg(17),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => input_railed_min_reg(18),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => input_railed_min_reg(19),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => input_railed_min_reg(1),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => input_railed_min_reg(20),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => input_railed_min_reg(21),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => input_railed_min_reg(22),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => input_railed_min_reg(23),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => input_railed_min_reg(24),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => input_railed_min_reg(25),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => input_railed_min_reg(26),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => input_railed_min_reg(27),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => input_railed_min_reg(28),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => input_railed_min_reg(29),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => input_railed_min_reg(2),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => input_railed_min_reg(30),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => input_railed_min_reg(31),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => input_railed_min_reg(3),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => input_railed_min_reg(4),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => input_railed_min_reg(5),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => input_railed_min_reg(6),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => input_railed_min_reg(7),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => input_railed_min_reg(8),
      R => pid_loop_0_rst1
    );
\input_railed_min_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \input_railed_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => input_railed_min_reg(9),
      R => pid_loop_0_rst1
    );
input_reset_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FE0000000000"
    )
        port map (
      I0 => \^input_reset\,
      I1 => input_railed00_in,
      I2 => input_railed0,
      I3 => \control_reg_reg_n_0_[0]\,
      I4 => input_stable,
      I5 => p_7_in,
      O => input_reset_reg_i_1_n_0
    );
input_reset_reg_i_10: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_max_reg(30),
      I1 => input_railed_max_reg(31),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_10_n_0
    );
input_reset_reg_i_11: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_max_reg(28),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_max_reg(29),
      O => input_reset_reg_i_11_n_0
    );
input_reset_reg_i_12: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_max_reg(26),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_max_reg(27),
      O => input_reset_reg_i_12_n_0
    );
input_reset_reg_i_13: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_max_reg(24),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_max_reg(25),
      O => input_reset_reg_i_13_n_0
    );
input_reset_reg_i_15: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => input_railed_min_reg(30),
      I1 => input_railed_min_reg(31),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_15_n_0
    );
input_reset_reg_i_16: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => input_railed_min_reg(28),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_min_reg(29),
      O => input_reset_reg_i_16_n_0
    );
input_reset_reg_i_17: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => input_railed_min_reg(26),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_min_reg(27),
      O => input_reset_reg_i_17_n_0
    );
input_reset_reg_i_18: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => input_railed_min_reg(24),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_min_reg(25),
      O => input_reset_reg_i_18_n_0
    );
input_reset_reg_i_19: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_min_reg(30),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_min_reg(31),
      O => input_reset_reg_i_19_n_0
    );
input_reset_reg_i_20: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_min_reg(28),
      I1 => input_railed_min_reg(29),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_20_n_0
    );
input_reset_reg_i_21: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_min_reg(26),
      I1 => input_railed_min_reg(27),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_21_n_0
    );
input_reset_reg_i_22: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_min_reg(24),
      I1 => input_railed_min_reg(25),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_22_n_0
    );
input_reset_reg_i_24: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => input_railed_max_reg(22),
      I1 => input_railed_max_reg(23),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_24_n_0
    );
input_reset_reg_i_25: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => input_railed_max_reg(20),
      I1 => input_railed_max_reg(21),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_25_n_0
    );
input_reset_reg_i_26: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => input_railed_max_reg(18),
      I1 => input_railed_max_reg(19),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_26_n_0
    );
input_reset_reg_i_27: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => input_railed_max_reg(16),
      I1 => input_railed_max_reg(17),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_27_n_0
    );
input_reset_reg_i_28: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_max_reg(22),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_max_reg(23),
      O => input_reset_reg_i_28_n_0
    );
input_reset_reg_i_29: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_max_reg(20),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_max_reg(21),
      O => input_reset_reg_i_29_n_0
    );
input_reset_reg_i_30: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_max_reg(18),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_max_reg(19),
      O => input_reset_reg_i_30_n_0
    );
input_reset_reg_i_31: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_max_reg(16),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_max_reg(17),
      O => input_reset_reg_i_31_n_0
    );
input_reset_reg_i_33: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => input_railed_min_reg(22),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_min_reg(23),
      O => input_reset_reg_i_33_n_0
    );
input_reset_reg_i_34: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => input_railed_min_reg(20),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_min_reg(21),
      O => input_reset_reg_i_34_n_0
    );
input_reset_reg_i_35: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => input_railed_min_reg(18),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_min_reg(19),
      O => input_reset_reg_i_35_n_0
    );
input_reset_reg_i_36: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => input_railed_min_reg(16),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_min_reg(17),
      O => input_reset_reg_i_36_n_0
    );
input_reset_reg_i_37: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_min_reg(22),
      I1 => input_railed_min_reg(23),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_37_n_0
    );
input_reset_reg_i_38: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_min_reg(20),
      I1 => input_railed_min_reg(21),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_38_n_0
    );
input_reset_reg_i_39: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_min_reg(18),
      I1 => input_railed_min_reg(19),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_39_n_0
    );
input_reset_reg_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => input_stable0,
      I1 => input_stable02_in,
      O => input_stable
    );
input_reset_reg_i_40: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_min_reg(16),
      I1 => input_railed_min_reg(17),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_40_n_0
    );
input_reset_reg_i_42: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => input_railed_max_reg(14),
      I1 => input_railed_max_reg(15),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_42_n_0
    );
input_reset_reg_i_43: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(12),
      I1 => input_railed_max_reg(12),
      I2 => input_railed_max_reg(13),
      I3 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_43_n_0
    );
input_reset_reg_i_44: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(10),
      I1 => input_railed_max_reg(10),
      I2 => input_railed_max_reg(11),
      I3 => pid_loop_0_input_reg(11),
      O => input_reset_reg_i_44_n_0
    );
input_reset_reg_i_45: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(8),
      I1 => input_railed_max_reg(8),
      I2 => input_railed_max_reg(9),
      I3 => pid_loop_0_input_reg(9),
      O => input_reset_reg_i_45_n_0
    );
input_reset_reg_i_46: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_max_reg(14),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_max_reg(15),
      O => input_reset_reg_i_46_n_0
    );
input_reset_reg_i_47: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(12),
      I1 => input_railed_max_reg(12),
      I2 => pid_loop_0_input_reg(13),
      I3 => input_railed_max_reg(13),
      O => input_reset_reg_i_47_n_0
    );
input_reset_reg_i_48: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(10),
      I1 => input_railed_max_reg(10),
      I2 => pid_loop_0_input_reg(11),
      I3 => input_railed_max_reg(11),
      O => input_reset_reg_i_48_n_0
    );
input_reset_reg_i_49: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(8),
      I1 => input_railed_max_reg(8),
      I2 => pid_loop_0_input_reg(9),
      I3 => input_railed_max_reg(9),
      O => input_reset_reg_i_49_n_0
    );
input_reset_reg_i_51: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => input_railed_min_reg(14),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_min_reg(15),
      O => input_reset_reg_i_51_n_0
    );
input_reset_reg_i_52: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => input_railed_min_reg(12),
      I1 => pid_loop_0_input_reg(12),
      I2 => pid_loop_0_input_reg(13),
      I3 => input_railed_min_reg(13),
      O => input_reset_reg_i_52_n_0
    );
input_reset_reg_i_53: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => input_railed_min_reg(10),
      I1 => pid_loop_0_input_reg(10),
      I2 => pid_loop_0_input_reg(11),
      I3 => input_railed_min_reg(11),
      O => input_reset_reg_i_53_n_0
    );
input_reset_reg_i_54: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => input_railed_min_reg(8),
      I1 => pid_loop_0_input_reg(8),
      I2 => pid_loop_0_input_reg(9),
      I3 => input_railed_min_reg(9),
      O => input_reset_reg_i_54_n_0
    );
input_reset_reg_i_55: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => input_railed_min_reg(14),
      I1 => input_railed_min_reg(15),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_55_n_0
    );
input_reset_reg_i_56: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => input_railed_min_reg(12),
      I1 => pid_loop_0_input_reg(12),
      I2 => input_railed_min_reg(13),
      I3 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_56_n_0
    );
input_reset_reg_i_57: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => input_railed_min_reg(10),
      I1 => pid_loop_0_input_reg(10),
      I2 => input_railed_min_reg(11),
      I3 => pid_loop_0_input_reg(11),
      O => input_reset_reg_i_57_n_0
    );
input_reset_reg_i_58: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => input_railed_min_reg(8),
      I1 => pid_loop_0_input_reg(8),
      I2 => input_railed_min_reg(9),
      I3 => pid_loop_0_input_reg(9),
      O => input_reset_reg_i_58_n_0
    );
input_reset_reg_i_59: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(6),
      I1 => input_railed_max_reg(6),
      I2 => input_railed_max_reg(7),
      I3 => pid_loop_0_input_reg(7),
      O => input_reset_reg_i_59_n_0
    );
input_reset_reg_i_6: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => input_railed_max_reg(30),
      I1 => pid_loop_0_input_reg(13),
      I2 => input_railed_max_reg(31),
      O => input_reset_reg_i_6_n_0
    );
input_reset_reg_i_60: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(4),
      I1 => input_railed_max_reg(4),
      I2 => input_railed_max_reg(5),
      I3 => pid_loop_0_input_reg(5),
      O => input_reset_reg_i_60_n_0
    );
input_reset_reg_i_61: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(2),
      I1 => input_railed_max_reg(2),
      I2 => input_railed_max_reg(3),
      I3 => pid_loop_0_input_reg(3),
      O => input_reset_reg_i_61_n_0
    );
input_reset_reg_i_62: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(0),
      I1 => input_railed_max_reg(0),
      I2 => input_railed_max_reg(1),
      I3 => pid_loop_0_input_reg(1),
      O => input_reset_reg_i_62_n_0
    );
input_reset_reg_i_63: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(6),
      I1 => input_railed_max_reg(6),
      I2 => pid_loop_0_input_reg(7),
      I3 => input_railed_max_reg(7),
      O => input_reset_reg_i_63_n_0
    );
input_reset_reg_i_64: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(4),
      I1 => input_railed_max_reg(4),
      I2 => pid_loop_0_input_reg(5),
      I3 => input_railed_max_reg(5),
      O => input_reset_reg_i_64_n_0
    );
input_reset_reg_i_65: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(2),
      I1 => input_railed_max_reg(2),
      I2 => pid_loop_0_input_reg(3),
      I3 => input_railed_max_reg(3),
      O => input_reset_reg_i_65_n_0
    );
input_reset_reg_i_66: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(0),
      I1 => input_railed_max_reg(0),
      I2 => pid_loop_0_input_reg(1),
      I3 => input_railed_max_reg(1),
      O => input_reset_reg_i_66_n_0
    );
input_reset_reg_i_67: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => input_railed_min_reg(6),
      I1 => pid_loop_0_input_reg(6),
      I2 => pid_loop_0_input_reg(7),
      I3 => input_railed_min_reg(7),
      O => input_reset_reg_i_67_n_0
    );
input_reset_reg_i_68: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => input_railed_min_reg(4),
      I1 => pid_loop_0_input_reg(4),
      I2 => pid_loop_0_input_reg(5),
      I3 => input_railed_min_reg(5),
      O => input_reset_reg_i_68_n_0
    );
input_reset_reg_i_69: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => input_railed_min_reg(2),
      I1 => pid_loop_0_input_reg(2),
      I2 => pid_loop_0_input_reg(3),
      I3 => input_railed_min_reg(3),
      O => input_reset_reg_i_69_n_0
    );
input_reset_reg_i_7: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => input_railed_max_reg(28),
      I1 => input_railed_max_reg(29),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_7_n_0
    );
input_reset_reg_i_70: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => input_railed_min_reg(0),
      I1 => pid_loop_0_input_reg(0),
      I2 => pid_loop_0_input_reg(1),
      I3 => input_railed_min_reg(1),
      O => input_reset_reg_i_70_n_0
    );
input_reset_reg_i_71: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => input_railed_min_reg(6),
      I1 => pid_loop_0_input_reg(6),
      I2 => input_railed_min_reg(7),
      I3 => pid_loop_0_input_reg(7),
      O => input_reset_reg_i_71_n_0
    );
input_reset_reg_i_72: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => input_railed_min_reg(4),
      I1 => pid_loop_0_input_reg(4),
      I2 => input_railed_min_reg(5),
      I3 => pid_loop_0_input_reg(5),
      O => input_reset_reg_i_72_n_0
    );
input_reset_reg_i_73: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => input_railed_min_reg(2),
      I1 => pid_loop_0_input_reg(2),
      I2 => input_railed_min_reg(3),
      I3 => pid_loop_0_input_reg(3),
      O => input_reset_reg_i_73_n_0
    );
input_reset_reg_i_74: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => input_railed_min_reg(0),
      I1 => pid_loop_0_input_reg(0),
      I2 => input_railed_min_reg(1),
      I3 => pid_loop_0_input_reg(1),
      O => input_reset_reg_i_74_n_0
    );
input_reset_reg_i_8: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => input_railed_max_reg(26),
      I1 => input_railed_max_reg(27),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_8_n_0
    );
input_reset_reg_i_9: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => input_railed_max_reg(24),
      I1 => input_railed_max_reg(25),
      I2 => pid_loop_0_input_reg(13),
      O => input_reset_reg_i_9_n_0
    );
input_reset_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => input_reset_reg_i_1_n_0,
      Q => \^input_reset\,
      R => '0'
    );
input_reset_reg_reg_i_14: unisim.vcomponents.CARRY4
     port map (
      CI => input_reset_reg_reg_i_32_n_0,
      CO(3) => input_reset_reg_reg_i_14_n_0,
      CO(2) => input_reset_reg_reg_i_14_n_1,
      CO(1) => input_reset_reg_reg_i_14_n_2,
      CO(0) => input_reset_reg_reg_i_14_n_3,
      CYINIT => '0',
      DI(3) => input_reset_reg_i_33_n_0,
      DI(2) => input_reset_reg_i_34_n_0,
      DI(1) => input_reset_reg_i_35_n_0,
      DI(0) => input_reset_reg_i_36_n_0,
      O(3 downto 0) => NLW_input_reset_reg_reg_i_14_O_UNCONNECTED(3 downto 0),
      S(3) => input_reset_reg_i_37_n_0,
      S(2) => input_reset_reg_i_38_n_0,
      S(1) => input_reset_reg_i_39_n_0,
      S(0) => input_reset_reg_i_40_n_0
    );
input_reset_reg_reg_i_2: unisim.vcomponents.CARRY4
     port map (
      CI => input_reset_reg_reg_i_5_n_0,
      CO(3) => input_railed00_in,
      CO(2) => input_reset_reg_reg_i_2_n_1,
      CO(1) => input_reset_reg_reg_i_2_n_2,
      CO(0) => input_reset_reg_reg_i_2_n_3,
      CYINIT => '0',
      DI(3) => input_reset_reg_i_6_n_0,
      DI(2) => input_reset_reg_i_7_n_0,
      DI(1) => input_reset_reg_i_8_n_0,
      DI(0) => input_reset_reg_i_9_n_0,
      O(3 downto 0) => NLW_input_reset_reg_reg_i_2_O_UNCONNECTED(3 downto 0),
      S(3) => input_reset_reg_i_10_n_0,
      S(2) => input_reset_reg_i_11_n_0,
      S(1) => input_reset_reg_i_12_n_0,
      S(0) => input_reset_reg_i_13_n_0
    );
input_reset_reg_reg_i_23: unisim.vcomponents.CARRY4
     port map (
      CI => input_reset_reg_reg_i_41_n_0,
      CO(3) => input_reset_reg_reg_i_23_n_0,
      CO(2) => input_reset_reg_reg_i_23_n_1,
      CO(1) => input_reset_reg_reg_i_23_n_2,
      CO(0) => input_reset_reg_reg_i_23_n_3,
      CYINIT => '0',
      DI(3) => input_reset_reg_i_42_n_0,
      DI(2) => input_reset_reg_i_43_n_0,
      DI(1) => input_reset_reg_i_44_n_0,
      DI(0) => input_reset_reg_i_45_n_0,
      O(3 downto 0) => NLW_input_reset_reg_reg_i_23_O_UNCONNECTED(3 downto 0),
      S(3) => input_reset_reg_i_46_n_0,
      S(2) => input_reset_reg_i_47_n_0,
      S(1) => input_reset_reg_i_48_n_0,
      S(0) => input_reset_reg_i_49_n_0
    );
input_reset_reg_reg_i_3: unisim.vcomponents.CARRY4
     port map (
      CI => input_reset_reg_reg_i_14_n_0,
      CO(3) => input_railed0,
      CO(2) => input_reset_reg_reg_i_3_n_1,
      CO(1) => input_reset_reg_reg_i_3_n_2,
      CO(0) => input_reset_reg_reg_i_3_n_3,
      CYINIT => '0',
      DI(3) => input_reset_reg_i_15_n_0,
      DI(2) => input_reset_reg_i_16_n_0,
      DI(1) => input_reset_reg_i_17_n_0,
      DI(0) => input_reset_reg_i_18_n_0,
      O(3 downto 0) => NLW_input_reset_reg_reg_i_3_O_UNCONNECTED(3 downto 0),
      S(3) => input_reset_reg_i_19_n_0,
      S(2) => input_reset_reg_i_20_n_0,
      S(1) => input_reset_reg_i_21_n_0,
      S(0) => input_reset_reg_i_22_n_0
    );
input_reset_reg_reg_i_32: unisim.vcomponents.CARRY4
     port map (
      CI => input_reset_reg_reg_i_50_n_0,
      CO(3) => input_reset_reg_reg_i_32_n_0,
      CO(2) => input_reset_reg_reg_i_32_n_1,
      CO(1) => input_reset_reg_reg_i_32_n_2,
      CO(0) => input_reset_reg_reg_i_32_n_3,
      CYINIT => '0',
      DI(3) => input_reset_reg_i_51_n_0,
      DI(2) => input_reset_reg_i_52_n_0,
      DI(1) => input_reset_reg_i_53_n_0,
      DI(0) => input_reset_reg_i_54_n_0,
      O(3 downto 0) => NLW_input_reset_reg_reg_i_32_O_UNCONNECTED(3 downto 0),
      S(3) => input_reset_reg_i_55_n_0,
      S(2) => input_reset_reg_i_56_n_0,
      S(1) => input_reset_reg_i_57_n_0,
      S(0) => input_reset_reg_i_58_n_0
    );
input_reset_reg_reg_i_41: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => input_reset_reg_reg_i_41_n_0,
      CO(2) => input_reset_reg_reg_i_41_n_1,
      CO(1) => input_reset_reg_reg_i_41_n_2,
      CO(0) => input_reset_reg_reg_i_41_n_3,
      CYINIT => '0',
      DI(3) => input_reset_reg_i_59_n_0,
      DI(2) => input_reset_reg_i_60_n_0,
      DI(1) => input_reset_reg_i_61_n_0,
      DI(0) => input_reset_reg_i_62_n_0,
      O(3 downto 0) => NLW_input_reset_reg_reg_i_41_O_UNCONNECTED(3 downto 0),
      S(3) => input_reset_reg_i_63_n_0,
      S(2) => input_reset_reg_i_64_n_0,
      S(1) => input_reset_reg_i_65_n_0,
      S(0) => input_reset_reg_i_66_n_0
    );
input_reset_reg_reg_i_5: unisim.vcomponents.CARRY4
     port map (
      CI => input_reset_reg_reg_i_23_n_0,
      CO(3) => input_reset_reg_reg_i_5_n_0,
      CO(2) => input_reset_reg_reg_i_5_n_1,
      CO(1) => input_reset_reg_reg_i_5_n_2,
      CO(0) => input_reset_reg_reg_i_5_n_3,
      CYINIT => '0',
      DI(3) => input_reset_reg_i_24_n_0,
      DI(2) => input_reset_reg_i_25_n_0,
      DI(1) => input_reset_reg_i_26_n_0,
      DI(0) => input_reset_reg_i_27_n_0,
      O(3 downto 0) => NLW_input_reset_reg_reg_i_5_O_UNCONNECTED(3 downto 0),
      S(3) => input_reset_reg_i_28_n_0,
      S(2) => input_reset_reg_i_29_n_0,
      S(1) => input_reset_reg_i_30_n_0,
      S(0) => input_reset_reg_i_31_n_0
    );
input_reset_reg_reg_i_50: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => input_reset_reg_reg_i_50_n_0,
      CO(2) => input_reset_reg_reg_i_50_n_1,
      CO(1) => input_reset_reg_reg_i_50_n_2,
      CO(0) => input_reset_reg_reg_i_50_n_3,
      CYINIT => '0',
      DI(3) => input_reset_reg_i_67_n_0,
      DI(2) => input_reset_reg_i_68_n_0,
      DI(1) => input_reset_reg_i_69_n_0,
      DI(0) => input_reset_reg_i_70_n_0,
      O(3 downto 0) => NLW_input_reset_reg_reg_i_50_O_UNCONNECTED(3 downto 0),
      S(3) => input_reset_reg_i_71_n_0,
      S(2) => input_reset_reg_i_72_n_0,
      S(1) => input_reset_reg_i_73_n_0,
      S(0) => input_reset_reg_i_74_n_0
    );
\loop_locked_counter_reg[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => input_stable02_in,
      I1 => input_stable0,
      I2 => engage_pid_loop0,
      I3 => engage_pid_loop06_in,
      I4 => \control_reg_reg_n_0_[0]\,
      O => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg[0]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => loop_locked_min_reg(26),
      I1 => loop_locked_min_reg(27),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_10_n_0\
    );
\loop_locked_counter_reg[0]_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => loop_locked_min_reg(24),
      I1 => loop_locked_min_reg(25),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_11_n_0\
    );
\loop_locked_counter_reg[0]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_min_reg(30),
      I1 => loop_locked_min_reg(31),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_12_n_0\
    );
\loop_locked_counter_reg[0]_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_min_reg(28),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_min_reg(29),
      O => \loop_locked_counter_reg[0]_i_13_n_0\
    );
\loop_locked_counter_reg[0]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_min_reg(26),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_min_reg(27),
      O => \loop_locked_counter_reg[0]_i_14_n_0\
    );
\loop_locked_counter_reg[0]_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_min_reg(24),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_min_reg(25),
      O => \loop_locked_counter_reg[0]_i_15_n_0\
    );
\loop_locked_counter_reg[0]_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => loop_locked_max_reg(30),
      I1 => loop_locked_max_reg(31),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_17_n_0\
    );
\loop_locked_counter_reg[0]_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => loop_locked_max_reg(28),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_max_reg(29),
      O => \loop_locked_counter_reg[0]_i_18_n_0\
    );
\loop_locked_counter_reg[0]_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => loop_locked_max_reg(26),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_max_reg(27),
      O => \loop_locked_counter_reg[0]_i_19_n_0\
    );
\loop_locked_counter_reg[0]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^loop_locked_counter_reg_reg[30]_0\(0),
      O => sel
    );
\loop_locked_counter_reg[0]_i_20\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => loop_locked_max_reg(24),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_max_reg(25),
      O => \loop_locked_counter_reg[0]_i_20_n_0\
    );
\loop_locked_counter_reg[0]_i_21\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_max_reg(30),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_max_reg(31),
      O => \loop_locked_counter_reg[0]_i_21_n_0\
    );
\loop_locked_counter_reg[0]_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_max_reg(28),
      I1 => loop_locked_max_reg(29),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_22_n_0\
    );
\loop_locked_counter_reg[0]_i_23\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_max_reg(26),
      I1 => loop_locked_max_reg(27),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_23_n_0\
    );
\loop_locked_counter_reg[0]_i_24\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_max_reg(24),
      I1 => loop_locked_max_reg(25),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_24_n_0\
    );
\loop_locked_counter_reg[0]_i_26\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => loop_locked_min_reg(22),
      I1 => loop_locked_min_reg(23),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_26_n_0\
    );
\loop_locked_counter_reg[0]_i_27\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => loop_locked_min_reg(20),
      I1 => loop_locked_min_reg(21),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_27_n_0\
    );
\loop_locked_counter_reg[0]_i_28\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => loop_locked_min_reg(18),
      I1 => loop_locked_min_reg(19),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_28_n_0\
    );
\loop_locked_counter_reg[0]_i_29\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => loop_locked_min_reg(16),
      I1 => loop_locked_min_reg(17),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_29_n_0\
    );
\loop_locked_counter_reg[0]_i_30\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_min_reg(22),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_min_reg(23),
      O => \loop_locked_counter_reg[0]_i_30_n_0\
    );
\loop_locked_counter_reg[0]_i_31\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_min_reg(20),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_min_reg(21),
      O => \loop_locked_counter_reg[0]_i_31_n_0\
    );
\loop_locked_counter_reg[0]_i_32\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_min_reg(18),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_min_reg(19),
      O => \loop_locked_counter_reg[0]_i_32_n_0\
    );
\loop_locked_counter_reg[0]_i_33\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_min_reg(16),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_min_reg(17),
      O => \loop_locked_counter_reg[0]_i_33_n_0\
    );
\loop_locked_counter_reg[0]_i_35\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => loop_locked_max_reg(22),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_max_reg(23),
      O => \loop_locked_counter_reg[0]_i_35_n_0\
    );
\loop_locked_counter_reg[0]_i_36\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => loop_locked_max_reg(20),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_max_reg(21),
      O => \loop_locked_counter_reg[0]_i_36_n_0\
    );
\loop_locked_counter_reg[0]_i_37\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => loop_locked_max_reg(18),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_max_reg(19),
      O => \loop_locked_counter_reg[0]_i_37_n_0\
    );
\loop_locked_counter_reg[0]_i_38\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => loop_locked_max_reg(16),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_max_reg(17),
      O => \loop_locked_counter_reg[0]_i_38_n_0\
    );
\loop_locked_counter_reg[0]_i_39\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_max_reg(22),
      I1 => loop_locked_max_reg(23),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_39_n_0\
    );
\loop_locked_counter_reg[0]_i_40\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_max_reg(20),
      I1 => loop_locked_max_reg(21),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_40_n_0\
    );
\loop_locked_counter_reg[0]_i_41\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_max_reg(18),
      I1 => loop_locked_max_reg(19),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_41_n_0\
    );
\loop_locked_counter_reg[0]_i_42\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_max_reg(16),
      I1 => loop_locked_max_reg(17),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_42_n_0\
    );
\loop_locked_counter_reg[0]_i_44\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => loop_locked_min_reg(14),
      I1 => loop_locked_min_reg(15),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_44_n_0\
    );
\loop_locked_counter_reg[0]_i_45\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(12),
      I1 => loop_locked_min_reg(12),
      I2 => loop_locked_min_reg(13),
      I3 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_45_n_0\
    );
\loop_locked_counter_reg[0]_i_46\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(10),
      I1 => loop_locked_min_reg(10),
      I2 => loop_locked_min_reg(11),
      I3 => pid_loop_0_input_reg(11),
      O => \loop_locked_counter_reg[0]_i_46_n_0\
    );
\loop_locked_counter_reg[0]_i_47\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(8),
      I1 => loop_locked_min_reg(8),
      I2 => loop_locked_min_reg(9),
      I3 => pid_loop_0_input_reg(9),
      O => \loop_locked_counter_reg[0]_i_47_n_0\
    );
\loop_locked_counter_reg[0]_i_48\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_min_reg(14),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_min_reg(15),
      O => \loop_locked_counter_reg[0]_i_48_n_0\
    );
\loop_locked_counter_reg[0]_i_49\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(12),
      I1 => loop_locked_min_reg(12),
      I2 => pid_loop_0_input_reg(13),
      I3 => loop_locked_min_reg(13),
      O => \loop_locked_counter_reg[0]_i_49_n_0\
    );
\loop_locked_counter_reg[0]_i_50\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(10),
      I1 => loop_locked_min_reg(10),
      I2 => pid_loop_0_input_reg(11),
      I3 => loop_locked_min_reg(11),
      O => \loop_locked_counter_reg[0]_i_50_n_0\
    );
\loop_locked_counter_reg[0]_i_51\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(8),
      I1 => loop_locked_min_reg(8),
      I2 => pid_loop_0_input_reg(9),
      I3 => loop_locked_min_reg(9),
      O => \loop_locked_counter_reg[0]_i_51_n_0\
    );
\loop_locked_counter_reg[0]_i_53\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => loop_locked_max_reg(14),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_max_reg(15),
      O => \loop_locked_counter_reg[0]_i_53_n_0\
    );
\loop_locked_counter_reg[0]_i_54\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_max_reg(12),
      I1 => pid_loop_0_input_reg(12),
      I2 => pid_loop_0_input_reg(13),
      I3 => loop_locked_max_reg(13),
      O => \loop_locked_counter_reg[0]_i_54_n_0\
    );
\loop_locked_counter_reg[0]_i_55\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_max_reg(10),
      I1 => pid_loop_0_input_reg(10),
      I2 => pid_loop_0_input_reg(11),
      I3 => loop_locked_max_reg(11),
      O => \loop_locked_counter_reg[0]_i_55_n_0\
    );
\loop_locked_counter_reg[0]_i_56\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_max_reg(8),
      I1 => pid_loop_0_input_reg(8),
      I2 => pid_loop_0_input_reg(9),
      I3 => loop_locked_max_reg(9),
      O => \loop_locked_counter_reg[0]_i_56_n_0\
    );
\loop_locked_counter_reg[0]_i_57\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => loop_locked_max_reg(14),
      I1 => loop_locked_max_reg(15),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_57_n_0\
    );
\loop_locked_counter_reg[0]_i_58\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_max_reg(12),
      I1 => pid_loop_0_input_reg(12),
      I2 => loop_locked_max_reg(13),
      I3 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_58_n_0\
    );
\loop_locked_counter_reg[0]_i_59\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_max_reg(10),
      I1 => pid_loop_0_input_reg(10),
      I2 => loop_locked_max_reg(11),
      I3 => pid_loop_0_input_reg(11),
      O => \loop_locked_counter_reg[0]_i_59_n_0\
    );
\loop_locked_counter_reg[0]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => loop_locked_counter_reg_reg(0),
      O => \loop_locked_counter_reg[0]_i_6_n_0\
    );
\loop_locked_counter_reg[0]_i_60\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_max_reg(8),
      I1 => pid_loop_0_input_reg(8),
      I2 => loop_locked_max_reg(9),
      I3 => pid_loop_0_input_reg(9),
      O => \loop_locked_counter_reg[0]_i_60_n_0\
    );
\loop_locked_counter_reg[0]_i_61\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(6),
      I1 => loop_locked_min_reg(6),
      I2 => loop_locked_min_reg(7),
      I3 => pid_loop_0_input_reg(7),
      O => \loop_locked_counter_reg[0]_i_61_n_0\
    );
\loop_locked_counter_reg[0]_i_62\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(4),
      I1 => loop_locked_min_reg(4),
      I2 => loop_locked_min_reg(5),
      I3 => pid_loop_0_input_reg(5),
      O => \loop_locked_counter_reg[0]_i_62_n_0\
    );
\loop_locked_counter_reg[0]_i_63\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(2),
      I1 => loop_locked_min_reg(2),
      I2 => loop_locked_min_reg(3),
      I3 => pid_loop_0_input_reg(3),
      O => \loop_locked_counter_reg[0]_i_63_n_0\
    );
\loop_locked_counter_reg[0]_i_64\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => pid_loop_0_input_reg(0),
      I1 => loop_locked_min_reg(0),
      I2 => loop_locked_min_reg(1),
      I3 => pid_loop_0_input_reg(1),
      O => \loop_locked_counter_reg[0]_i_64_n_0\
    );
\loop_locked_counter_reg[0]_i_65\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(6),
      I1 => loop_locked_min_reg(6),
      I2 => pid_loop_0_input_reg(7),
      I3 => loop_locked_min_reg(7),
      O => \loop_locked_counter_reg[0]_i_65_n_0\
    );
\loop_locked_counter_reg[0]_i_66\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(4),
      I1 => loop_locked_min_reg(4),
      I2 => pid_loop_0_input_reg(5),
      I3 => loop_locked_min_reg(5),
      O => \loop_locked_counter_reg[0]_i_66_n_0\
    );
\loop_locked_counter_reg[0]_i_67\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(2),
      I1 => loop_locked_min_reg(2),
      I2 => pid_loop_0_input_reg(3),
      I3 => loop_locked_min_reg(3),
      O => \loop_locked_counter_reg[0]_i_67_n_0\
    );
\loop_locked_counter_reg[0]_i_68\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pid_loop_0_input_reg(0),
      I1 => loop_locked_min_reg(0),
      I2 => pid_loop_0_input_reg(1),
      I3 => loop_locked_min_reg(1),
      O => \loop_locked_counter_reg[0]_i_68_n_0\
    );
\loop_locked_counter_reg[0]_i_69\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_max_reg(6),
      I1 => pid_loop_0_input_reg(6),
      I2 => pid_loop_0_input_reg(7),
      I3 => loop_locked_max_reg(7),
      O => \loop_locked_counter_reg[0]_i_69_n_0\
    );
\loop_locked_counter_reg[0]_i_70\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_max_reg(4),
      I1 => pid_loop_0_input_reg(4),
      I2 => pid_loop_0_input_reg(5),
      I3 => loop_locked_max_reg(5),
      O => \loop_locked_counter_reg[0]_i_70_n_0\
    );
\loop_locked_counter_reg[0]_i_71\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_max_reg(2),
      I1 => pid_loop_0_input_reg(2),
      I2 => pid_loop_0_input_reg(3),
      I3 => loop_locked_max_reg(3),
      O => \loop_locked_counter_reg[0]_i_71_n_0\
    );
\loop_locked_counter_reg[0]_i_72\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => loop_locked_max_reg(0),
      I1 => pid_loop_0_input_reg(0),
      I2 => pid_loop_0_input_reg(1),
      I3 => loop_locked_max_reg(1),
      O => \loop_locked_counter_reg[0]_i_72_n_0\
    );
\loop_locked_counter_reg[0]_i_73\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_max_reg(6),
      I1 => pid_loop_0_input_reg(6),
      I2 => loop_locked_max_reg(7),
      I3 => pid_loop_0_input_reg(7),
      O => \loop_locked_counter_reg[0]_i_73_n_0\
    );
\loop_locked_counter_reg[0]_i_74\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_max_reg(4),
      I1 => pid_loop_0_input_reg(4),
      I2 => loop_locked_max_reg(5),
      I3 => pid_loop_0_input_reg(5),
      O => \loop_locked_counter_reg[0]_i_74_n_0\
    );
\loop_locked_counter_reg[0]_i_75\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_max_reg(2),
      I1 => pid_loop_0_input_reg(2),
      I2 => loop_locked_max_reg(3),
      I3 => pid_loop_0_input_reg(3),
      O => \loop_locked_counter_reg[0]_i_75_n_0\
    );
\loop_locked_counter_reg[0]_i_76\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => loop_locked_max_reg(0),
      I1 => pid_loop_0_input_reg(0),
      I2 => loop_locked_max_reg(1),
      I3 => pid_loop_0_input_reg(1),
      O => \loop_locked_counter_reg[0]_i_76_n_0\
    );
\loop_locked_counter_reg[0]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => loop_locked_min_reg(30),
      I1 => pid_loop_0_input_reg(13),
      I2 => loop_locked_min_reg(31),
      O => \loop_locked_counter_reg[0]_i_8_n_0\
    );
\loop_locked_counter_reg[0]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"70"
    )
        port map (
      I0 => loop_locked_min_reg(28),
      I1 => loop_locked_min_reg(29),
      I2 => pid_loop_0_input_reg(13),
      O => \loop_locked_counter_reg[0]_i_9_n_0\
    );
\loop_locked_counter_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[0]_i_3_n_7\,
      Q => loop_locked_counter_reg_reg(0),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[0]_i_16\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[0]_i_34_n_0\,
      CO(3) => \loop_locked_counter_reg_reg[0]_i_16_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[0]_i_16_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[0]_i_16_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[0]_i_16_n_3\,
      CYINIT => '0',
      DI(3) => \loop_locked_counter_reg[0]_i_35_n_0\,
      DI(2) => \loop_locked_counter_reg[0]_i_36_n_0\,
      DI(1) => \loop_locked_counter_reg[0]_i_37_n_0\,
      DI(0) => \loop_locked_counter_reg[0]_i_38_n_0\,
      O(3 downto 0) => \NLW_loop_locked_counter_reg_reg[0]_i_16_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_locked_counter_reg[0]_i_39_n_0\,
      S(2) => \loop_locked_counter_reg[0]_i_40_n_0\,
      S(1) => \loop_locked_counter_reg[0]_i_41_n_0\,
      S(0) => \loop_locked_counter_reg[0]_i_42_n_0\
    );
\loop_locked_counter_reg_reg[0]_i_25\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[0]_i_43_n_0\,
      CO(3) => \loop_locked_counter_reg_reg[0]_i_25_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[0]_i_25_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[0]_i_25_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[0]_i_25_n_3\,
      CYINIT => '0',
      DI(3) => \loop_locked_counter_reg[0]_i_44_n_0\,
      DI(2) => \loop_locked_counter_reg[0]_i_45_n_0\,
      DI(1) => \loop_locked_counter_reg[0]_i_46_n_0\,
      DI(0) => \loop_locked_counter_reg[0]_i_47_n_0\,
      O(3 downto 0) => \NLW_loop_locked_counter_reg_reg[0]_i_25_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_locked_counter_reg[0]_i_48_n_0\,
      S(2) => \loop_locked_counter_reg[0]_i_49_n_0\,
      S(1) => \loop_locked_counter_reg[0]_i_50_n_0\,
      S(0) => \loop_locked_counter_reg[0]_i_51_n_0\
    );
\loop_locked_counter_reg_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \loop_locked_counter_reg_reg[0]_i_3_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[0]_i_3_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[0]_i_3_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \loop_locked_counter_reg_reg[0]_i_3_n_4\,
      O(2) => \loop_locked_counter_reg_reg[0]_i_3_n_5\,
      O(1) => \loop_locked_counter_reg_reg[0]_i_3_n_6\,
      O(0) => \loop_locked_counter_reg_reg[0]_i_3_n_7\,
      S(3 downto 1) => loop_locked_counter_reg_reg(3 downto 1),
      S(0) => \loop_locked_counter_reg[0]_i_6_n_0\
    );
\loop_locked_counter_reg_reg[0]_i_34\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[0]_i_52_n_0\,
      CO(3) => \loop_locked_counter_reg_reg[0]_i_34_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[0]_i_34_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[0]_i_34_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[0]_i_34_n_3\,
      CYINIT => '0',
      DI(3) => \loop_locked_counter_reg[0]_i_53_n_0\,
      DI(2) => \loop_locked_counter_reg[0]_i_54_n_0\,
      DI(1) => \loop_locked_counter_reg[0]_i_55_n_0\,
      DI(0) => \loop_locked_counter_reg[0]_i_56_n_0\,
      O(3 downto 0) => \NLW_loop_locked_counter_reg_reg[0]_i_34_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_locked_counter_reg[0]_i_57_n_0\,
      S(2) => \loop_locked_counter_reg[0]_i_58_n_0\,
      S(1) => \loop_locked_counter_reg[0]_i_59_n_0\,
      S(0) => \loop_locked_counter_reg[0]_i_60_n_0\
    );
\loop_locked_counter_reg_reg[0]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[0]_i_7_n_0\,
      CO(3) => input_stable02_in,
      CO(2) => \loop_locked_counter_reg_reg[0]_i_4_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[0]_i_4_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[0]_i_4_n_3\,
      CYINIT => '0',
      DI(3) => \loop_locked_counter_reg[0]_i_8_n_0\,
      DI(2) => \loop_locked_counter_reg[0]_i_9_n_0\,
      DI(1) => \loop_locked_counter_reg[0]_i_10_n_0\,
      DI(0) => \loop_locked_counter_reg[0]_i_11_n_0\,
      O(3 downto 0) => \NLW_loop_locked_counter_reg_reg[0]_i_4_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_locked_counter_reg[0]_i_12_n_0\,
      S(2) => \loop_locked_counter_reg[0]_i_13_n_0\,
      S(1) => \loop_locked_counter_reg[0]_i_14_n_0\,
      S(0) => \loop_locked_counter_reg[0]_i_15_n_0\
    );
\loop_locked_counter_reg_reg[0]_i_43\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \loop_locked_counter_reg_reg[0]_i_43_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[0]_i_43_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[0]_i_43_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[0]_i_43_n_3\,
      CYINIT => '0',
      DI(3) => \loop_locked_counter_reg[0]_i_61_n_0\,
      DI(2) => \loop_locked_counter_reg[0]_i_62_n_0\,
      DI(1) => \loop_locked_counter_reg[0]_i_63_n_0\,
      DI(0) => \loop_locked_counter_reg[0]_i_64_n_0\,
      O(3 downto 0) => \NLW_loop_locked_counter_reg_reg[0]_i_43_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_locked_counter_reg[0]_i_65_n_0\,
      S(2) => \loop_locked_counter_reg[0]_i_66_n_0\,
      S(1) => \loop_locked_counter_reg[0]_i_67_n_0\,
      S(0) => \loop_locked_counter_reg[0]_i_68_n_0\
    );
\loop_locked_counter_reg_reg[0]_i_5\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[0]_i_16_n_0\,
      CO(3) => input_stable0,
      CO(2) => \loop_locked_counter_reg_reg[0]_i_5_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[0]_i_5_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[0]_i_5_n_3\,
      CYINIT => '0',
      DI(3) => \loop_locked_counter_reg[0]_i_17_n_0\,
      DI(2) => \loop_locked_counter_reg[0]_i_18_n_0\,
      DI(1) => \loop_locked_counter_reg[0]_i_19_n_0\,
      DI(0) => \loop_locked_counter_reg[0]_i_20_n_0\,
      O(3 downto 0) => \NLW_loop_locked_counter_reg_reg[0]_i_5_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_locked_counter_reg[0]_i_21_n_0\,
      S(2) => \loop_locked_counter_reg[0]_i_22_n_0\,
      S(1) => \loop_locked_counter_reg[0]_i_23_n_0\,
      S(0) => \loop_locked_counter_reg[0]_i_24_n_0\
    );
\loop_locked_counter_reg_reg[0]_i_52\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \loop_locked_counter_reg_reg[0]_i_52_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[0]_i_52_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[0]_i_52_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[0]_i_52_n_3\,
      CYINIT => '0',
      DI(3) => \loop_locked_counter_reg[0]_i_69_n_0\,
      DI(2) => \loop_locked_counter_reg[0]_i_70_n_0\,
      DI(1) => \loop_locked_counter_reg[0]_i_71_n_0\,
      DI(0) => \loop_locked_counter_reg[0]_i_72_n_0\,
      O(3 downto 0) => \NLW_loop_locked_counter_reg_reg[0]_i_52_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_locked_counter_reg[0]_i_73_n_0\,
      S(2) => \loop_locked_counter_reg[0]_i_74_n_0\,
      S(1) => \loop_locked_counter_reg[0]_i_75_n_0\,
      S(0) => \loop_locked_counter_reg[0]_i_76_n_0\
    );
\loop_locked_counter_reg_reg[0]_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[0]_i_25_n_0\,
      CO(3) => \loop_locked_counter_reg_reg[0]_i_7_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[0]_i_7_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[0]_i_7_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[0]_i_7_n_3\,
      CYINIT => '0',
      DI(3) => \loop_locked_counter_reg[0]_i_26_n_0\,
      DI(2) => \loop_locked_counter_reg[0]_i_27_n_0\,
      DI(1) => \loop_locked_counter_reg[0]_i_28_n_0\,
      DI(0) => \loop_locked_counter_reg[0]_i_29_n_0\,
      O(3 downto 0) => \NLW_loop_locked_counter_reg_reg[0]_i_7_O_UNCONNECTED\(3 downto 0),
      S(3) => \loop_locked_counter_reg[0]_i_30_n_0\,
      S(2) => \loop_locked_counter_reg[0]_i_31_n_0\,
      S(1) => \loop_locked_counter_reg[0]_i_32_n_0\,
      S(0) => \loop_locked_counter_reg[0]_i_33_n_0\
    );
\loop_locked_counter_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[8]_i_1_n_5\,
      Q => loop_locked_counter_reg_reg(10),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[8]_i_1_n_4\,
      Q => loop_locked_counter_reg_reg(11),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[12]_i_1_n_7\,
      Q => loop_locked_counter_reg_reg(12),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[8]_i_1_n_0\,
      CO(3) => \loop_locked_counter_reg_reg[12]_i_1_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[12]_i_1_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[12]_i_1_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \loop_locked_counter_reg_reg[12]_i_1_n_4\,
      O(2) => \loop_locked_counter_reg_reg[12]_i_1_n_5\,
      O(1) => \loop_locked_counter_reg_reg[12]_i_1_n_6\,
      O(0) => \loop_locked_counter_reg_reg[12]_i_1_n_7\,
      S(3 downto 0) => loop_locked_counter_reg_reg(15 downto 12)
    );
\loop_locked_counter_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[12]_i_1_n_6\,
      Q => loop_locked_counter_reg_reg(13),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[12]_i_1_n_5\,
      Q => loop_locked_counter_reg_reg(14),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[12]_i_1_n_4\,
      Q => loop_locked_counter_reg_reg(15),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[16]_i_1_n_7\,
      Q => loop_locked_counter_reg_reg(16),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[12]_i_1_n_0\,
      CO(3) => \loop_locked_counter_reg_reg[16]_i_1_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[16]_i_1_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[16]_i_1_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \loop_locked_counter_reg_reg[16]_i_1_n_4\,
      O(2) => \loop_locked_counter_reg_reg[16]_i_1_n_5\,
      O(1) => \loop_locked_counter_reg_reg[16]_i_1_n_6\,
      O(0) => \loop_locked_counter_reg_reg[16]_i_1_n_7\,
      S(3 downto 0) => loop_locked_counter_reg_reg(19 downto 16)
    );
\loop_locked_counter_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[16]_i_1_n_6\,
      Q => loop_locked_counter_reg_reg(17),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[16]_i_1_n_5\,
      Q => loop_locked_counter_reg_reg(18),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[16]_i_1_n_4\,
      Q => loop_locked_counter_reg_reg(19),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[0]_i_3_n_6\,
      Q => loop_locked_counter_reg_reg(1),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[20]_i_1_n_7\,
      Q => loop_locked_counter_reg_reg(20),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[16]_i_1_n_0\,
      CO(3) => \loop_locked_counter_reg_reg[20]_i_1_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[20]_i_1_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[20]_i_1_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \loop_locked_counter_reg_reg[20]_i_1_n_4\,
      O(2) => \loop_locked_counter_reg_reg[20]_i_1_n_5\,
      O(1) => \loop_locked_counter_reg_reg[20]_i_1_n_6\,
      O(0) => \loop_locked_counter_reg_reg[20]_i_1_n_7\,
      S(3 downto 0) => loop_locked_counter_reg_reg(23 downto 20)
    );
\loop_locked_counter_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[20]_i_1_n_6\,
      Q => loop_locked_counter_reg_reg(21),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[20]_i_1_n_5\,
      Q => loop_locked_counter_reg_reg(22),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[20]_i_1_n_4\,
      Q => loop_locked_counter_reg_reg(23),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[24]_i_1_n_7\,
      Q => loop_locked_counter_reg_reg(24),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[20]_i_1_n_0\,
      CO(3) => \loop_locked_counter_reg_reg[24]_i_1_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[24]_i_1_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[24]_i_1_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \loop_locked_counter_reg_reg[24]_i_1_n_4\,
      O(2) => \loop_locked_counter_reg_reg[24]_i_1_n_5\,
      O(1) => \loop_locked_counter_reg_reg[24]_i_1_n_6\,
      O(0) => \loop_locked_counter_reg_reg[24]_i_1_n_7\,
      S(3 downto 0) => loop_locked_counter_reg_reg(27 downto 24)
    );
\loop_locked_counter_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[24]_i_1_n_6\,
      Q => loop_locked_counter_reg_reg(25),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[24]_i_1_n_5\,
      Q => loop_locked_counter_reg_reg(26),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[24]_i_1_n_4\,
      Q => loop_locked_counter_reg_reg(27),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[28]_i_1_n_7\,
      Q => loop_locked_counter_reg_reg(28),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[24]_i_1_n_0\,
      CO(3) => \NLW_loop_locked_counter_reg_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \loop_locked_counter_reg_reg[28]_i_1_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[28]_i_1_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \loop_locked_counter_reg_reg[28]_i_1_n_4\,
      O(2) => \loop_locked_counter_reg_reg[28]_i_1_n_5\,
      O(1) => \loop_locked_counter_reg_reg[28]_i_1_n_6\,
      O(0) => \loop_locked_counter_reg_reg[28]_i_1_n_7\,
      S(3 downto 0) => loop_locked_counter_reg_reg(31 downto 28)
    );
\loop_locked_counter_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[28]_i_1_n_6\,
      Q => loop_locked_counter_reg_reg(29),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[0]_i_3_n_5\,
      Q => loop_locked_counter_reg_reg(2),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[28]_i_1_n_5\,
      Q => loop_locked_counter_reg_reg(30),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[28]_i_1_n_4\,
      Q => loop_locked_counter_reg_reg(31),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[0]_i_3_n_4\,
      Q => loop_locked_counter_reg_reg(3),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[4]_i_1_n_7\,
      Q => loop_locked_counter_reg_reg(4),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[0]_i_3_n_0\,
      CO(3) => \loop_locked_counter_reg_reg[4]_i_1_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[4]_i_1_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[4]_i_1_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \loop_locked_counter_reg_reg[4]_i_1_n_4\,
      O(2) => \loop_locked_counter_reg_reg[4]_i_1_n_5\,
      O(1) => \loop_locked_counter_reg_reg[4]_i_1_n_6\,
      O(0) => \loop_locked_counter_reg_reg[4]_i_1_n_7\,
      S(3 downto 0) => loop_locked_counter_reg_reg(7 downto 4)
    );
\loop_locked_counter_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[4]_i_1_n_6\,
      Q => loop_locked_counter_reg_reg(5),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[4]_i_1_n_5\,
      Q => loop_locked_counter_reg_reg(6),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[4]_i_1_n_4\,
      Q => loop_locked_counter_reg_reg(7),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[8]_i_1_n_7\,
      Q => loop_locked_counter_reg_reg(8),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_counter_reg_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \loop_locked_counter_reg_reg[4]_i_1_n_0\,
      CO(3) => \loop_locked_counter_reg_reg[8]_i_1_n_0\,
      CO(2) => \loop_locked_counter_reg_reg[8]_i_1_n_1\,
      CO(1) => \loop_locked_counter_reg_reg[8]_i_1_n_2\,
      CO(0) => \loop_locked_counter_reg_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \loop_locked_counter_reg_reg[8]_i_1_n_4\,
      O(2) => \loop_locked_counter_reg_reg[8]_i_1_n_5\,
      O(1) => \loop_locked_counter_reg_reg[8]_i_1_n_6\,
      O(0) => \loop_locked_counter_reg_reg[8]_i_1_n_7\,
      S(3 downto 0) => loop_locked_counter_reg_reg(11 downto 8)
    );
\loop_locked_counter_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \loop_locked_counter_reg_reg[8]_i_1_n_6\,
      Q => loop_locked_counter_reg_reg(9),
      R => \loop_locked_counter_reg[0]_i_1_n_0\
    );
\loop_locked_delay_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_delay_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \loop_locked_delay_reg[15]_i_1_n_0\
    );
\loop_locked_delay_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_delay_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \loop_locked_delay_reg[23]_i_1_n_0\
    );
\loop_locked_delay_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_delay_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \loop_locked_delay_reg[31]_i_1_n_0\
    );
\loop_locked_delay_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \loop_locked_delay_reg[31]_i_2_n_0\
    );
\loop_locked_delay_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_delay_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \loop_locked_delay_reg[7]_i_1_n_0\
    );
\loop_locked_delay_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => loop_locked_delay_reg(0),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => loop_locked_delay_reg(10),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => loop_locked_delay_reg(11),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => loop_locked_delay_reg(12),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => loop_locked_delay_reg(13),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => loop_locked_delay_reg(14),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => loop_locked_delay_reg(15),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => loop_locked_delay_reg(16),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => loop_locked_delay_reg(17),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => loop_locked_delay_reg(18),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => loop_locked_delay_reg(19),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => loop_locked_delay_reg(1),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => loop_locked_delay_reg(20),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => loop_locked_delay_reg(21),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => loop_locked_delay_reg(22),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => loop_locked_delay_reg(23),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => loop_locked_delay_reg(24),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => loop_locked_delay_reg(25),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => loop_locked_delay_reg(26),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => loop_locked_delay_reg(27),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => loop_locked_delay_reg(28),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => loop_locked_delay_reg(29),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => loop_locked_delay_reg(2),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => loop_locked_delay_reg(30),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => loop_locked_delay_reg(31),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => loop_locked_delay_reg(3),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => loop_locked_delay_reg(4),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => loop_locked_delay_reg(5),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => loop_locked_delay_reg(6),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => loop_locked_delay_reg(7),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => loop_locked_delay_reg(8),
      R => pid_loop_0_rst1
    );
\loop_locked_delay_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_delay_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => loop_locked_delay_reg(9),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \loop_locked_max_reg[15]_i_1_n_0\
    );
\loop_locked_max_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \loop_locked_max_reg[23]_i_1_n_0\
    );
\loop_locked_max_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \loop_locked_max_reg[31]_i_1_n_0\
    );
\loop_locked_max_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000020000000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \loop_locked_max_reg[31]_i_2_n_0\
    );
\loop_locked_max_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_max_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \loop_locked_max_reg[7]_i_1_n_0\
    );
\loop_locked_max_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => loop_locked_max_reg(0),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => loop_locked_max_reg(10),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => loop_locked_max_reg(11),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => loop_locked_max_reg(12),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => loop_locked_max_reg(13),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => loop_locked_max_reg(14),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => loop_locked_max_reg(15),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => loop_locked_max_reg(16),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => loop_locked_max_reg(17),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => loop_locked_max_reg(18),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => loop_locked_max_reg(19),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => loop_locked_max_reg(1),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => loop_locked_max_reg(20),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => loop_locked_max_reg(21),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => loop_locked_max_reg(22),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => loop_locked_max_reg(23),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => loop_locked_max_reg(24),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => loop_locked_max_reg(25),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => loop_locked_max_reg(26),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => loop_locked_max_reg(27),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => loop_locked_max_reg(28),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => loop_locked_max_reg(29),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => loop_locked_max_reg(2),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => loop_locked_max_reg(30),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => loop_locked_max_reg(31),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => loop_locked_max_reg(3),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => loop_locked_max_reg(4),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => loop_locked_max_reg(5),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => loop_locked_max_reg(6),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => loop_locked_max_reg(7),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => loop_locked_max_reg(8),
      R => pid_loop_0_rst1
    );
\loop_locked_max_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_max_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => loop_locked_max_reg(9),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \loop_locked_min_reg[15]_i_1_n_0\
    );
\loop_locked_min_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \loop_locked_min_reg[23]_i_1_n_0\
    );
\loop_locked_min_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \loop_locked_min_reg[31]_i_1_n_0\
    );
\loop_locked_min_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1000000000000000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \loop_locked_min_reg[31]_i_2_n_0\
    );
\loop_locked_min_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \loop_locked_min_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \loop_locked_min_reg[7]_i_1_n_0\
    );
\loop_locked_min_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => loop_locked_min_reg(0),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => loop_locked_min_reg(10),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => loop_locked_min_reg(11),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => loop_locked_min_reg(12),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => loop_locked_min_reg(13),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => loop_locked_min_reg(14),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => loop_locked_min_reg(15),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => loop_locked_min_reg(16),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => loop_locked_min_reg(17),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => loop_locked_min_reg(18),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => loop_locked_min_reg(19),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => loop_locked_min_reg(1),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => loop_locked_min_reg(20),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => loop_locked_min_reg(21),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => loop_locked_min_reg(22),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => loop_locked_min_reg(23),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => loop_locked_min_reg(24),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => loop_locked_min_reg(25),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => loop_locked_min_reg(26),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => loop_locked_min_reg(27),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => loop_locked_min_reg(28),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => loop_locked_min_reg(29),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => loop_locked_min_reg(2),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => loop_locked_min_reg(30),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => loop_locked_min_reg(31),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => loop_locked_min_reg(3),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => loop_locked_min_reg(4),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => loop_locked_min_reg(5),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => loop_locked_min_reg(6),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => loop_locked_min_reg(7),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => loop_locked_min_reg(8),
      R => pid_loop_0_rst1
    );
\loop_locked_min_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \loop_locked_min_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => loop_locked_min_reg(9),
      R => pid_loop_0_rst1
    );
\loop_output_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(0),
      Q => loop_output_reg(0),
      R => '0'
    );
\loop_output_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(10),
      Q => loop_output_reg(10),
      R => '0'
    );
\loop_output_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(11),
      Q => loop_output_reg(11),
      R => '0'
    );
\loop_output_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(12),
      Q => loop_output_reg(12),
      R => '0'
    );
\loop_output_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(1),
      Q => loop_output_reg(1),
      R => '0'
    );
\loop_output_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(2),
      Q => loop_output_reg(2),
      R => '0'
    );
\loop_output_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(13),
      Q => loop_output_reg(31),
      R => '0'
    );
\loop_output_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(3),
      Q => loop_output_reg(3),
      R => '0'
    );
\loop_output_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(4),
      Q => loop_output_reg(4),
      R => '0'
    );
\loop_output_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(5),
      Q => loop_output_reg(5),
      R => '0'
    );
\loop_output_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(6),
      Q => loop_output_reg(6),
      R => '0'
    );
\loop_output_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(7),
      Q => loop_output_reg(7),
      R => '0'
    );
\loop_output_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(8),
      Q => loop_output_reg(8),
      R => '0'
    );
\loop_output_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_output(9),
      Q => loop_output_reg(9),
      R => '0'
    );
\maxdex_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(0),
      Q => \maxdex_reg_reg_n_0_[0]\,
      R => '0'
    );
\maxdex_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(10),
      Q => \maxdex_reg_reg_n_0_[10]\,
      R => '0'
    );
\maxdex_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(11),
      Q => \maxdex_reg_reg_n_0_[11]\,
      R => '0'
    );
\maxdex_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(12),
      Q => \maxdex_reg_reg_n_0_[12]\,
      R => '0'
    );
\maxdex_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(13),
      Q => \maxdex_reg_reg_n_0_[15]\,
      R => '0'
    );
\maxdex_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(0),
      Q => \maxdex_reg_reg_n_0_[16]\,
      R => '0'
    );
\maxdex_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(1),
      Q => \maxdex_reg_reg_n_0_[17]\,
      R => '0'
    );
\maxdex_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(2),
      Q => \maxdex_reg_reg_n_0_[18]\,
      R => '0'
    );
\maxdex_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(3),
      Q => \maxdex_reg_reg_n_0_[19]\,
      R => '0'
    );
\maxdex_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(1),
      Q => \maxdex_reg_reg_n_0_[1]\,
      R => '0'
    );
\maxdex_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(4),
      Q => \maxdex_reg_reg_n_0_[20]\,
      R => '0'
    );
\maxdex_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(5),
      Q => \maxdex_reg_reg_n_0_[21]\,
      R => '0'
    );
\maxdex_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(6),
      Q => \maxdex_reg_reg_n_0_[22]\,
      R => '0'
    );
\maxdex_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(7),
      Q => \maxdex_reg_reg_n_0_[23]\,
      R => '0'
    );
\maxdex_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(8),
      Q => \maxdex_reg_reg_n_0_[24]\,
      R => '0'
    );
\maxdex_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(9),
      Q => \maxdex_reg_reg_n_0_[25]\,
      R => '0'
    );
\maxdex_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(10),
      Q => \maxdex_reg_reg_n_0_[26]\,
      R => '0'
    );
\maxdex_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(11),
      Q => \maxdex_reg_reg_n_0_[27]\,
      R => '0'
    );
\maxdex_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(12),
      Q => \maxdex_reg_reg_n_0_[28]\,
      R => '0'
    );
\maxdex_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(2),
      Q => \maxdex_reg_reg_n_0_[2]\,
      R => '0'
    );
\maxdex_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_max(13),
      Q => \maxdex_reg_reg_n_0_[31]\,
      R => '0'
    );
\maxdex_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(3),
      Q => \maxdex_reg_reg_n_0_[3]\,
      R => '0'
    );
\maxdex_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(4),
      Q => \maxdex_reg_reg_n_0_[4]\,
      R => '0'
    );
\maxdex_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(5),
      Q => \maxdex_reg_reg_n_0_[5]\,
      R => '0'
    );
\maxdex_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(6),
      Q => \maxdex_reg_reg_n_0_[6]\,
      R => '0'
    );
\maxdex_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(7),
      Q => \maxdex_reg_reg_n_0_[7]\,
      R => '0'
    );
\maxdex_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(8),
      Q => \maxdex_reg_reg_n_0_[8]\,
      R => '0'
    );
\maxdex_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_maxdex(9),
      Q => \maxdex_reg_reg_n_0_[9]\,
      R => '0'
    );
\mindex_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[0]\,
      Q => mindex_reg(0),
      R => '0'
    );
\mindex_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[10]\,
      Q => mindex_reg(10),
      R => '0'
    );
\mindex_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[11]\,
      Q => mindex_reg(11),
      R => '0'
    );
\mindex_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[12]\,
      Q => mindex_reg(12),
      R => '0'
    );
\mindex_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => p_0_in0,
      Q => mindex_reg(15),
      R => '0'
    );
\mindex_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(0),
      Q => mindex_reg(16),
      R => '0'
    );
\mindex_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(1),
      Q => mindex_reg(17),
      R => '0'
    );
\mindex_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(2),
      Q => mindex_reg(18),
      R => '0'
    );
\mindex_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(3),
      Q => mindex_reg(19),
      R => '0'
    );
\mindex_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[1]\,
      Q => mindex_reg(1),
      R => '0'
    );
\mindex_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(4),
      Q => mindex_reg(20),
      R => '0'
    );
\mindex_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(5),
      Q => mindex_reg(21),
      R => '0'
    );
\mindex_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(6),
      Q => mindex_reg(22),
      R => '0'
    );
\mindex_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(7),
      Q => mindex_reg(23),
      R => '0'
    );
\mindex_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(8),
      Q => mindex_reg(24),
      R => '0'
    );
\mindex_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(9),
      Q => mindex_reg(25),
      R => '0'
    );
\mindex_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(10),
      Q => mindex_reg(26),
      R => '0'
    );
\mindex_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(11),
      Q => mindex_reg(27),
      R => '0'
    );
\mindex_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(12),
      Q => mindex_reg(28),
      R => '0'
    );
\mindex_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[2]\,
      Q => mindex_reg(2),
      R => '0'
    );
\mindex_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => autolock_input_min(13),
      Q => mindex_reg(31),
      R => '0'
    );
\mindex_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[3]\,
      Q => mindex_reg(3),
      R => '0'
    );
\mindex_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[4]\,
      Q => mindex_reg(4),
      R => '0'
    );
\mindex_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[5]\,
      Q => mindex_reg(5),
      R => '0'
    );
\mindex_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[6]\,
      Q => mindex_reg(6),
      R => '0'
    );
\mindex_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[7]\,
      Q => mindex_reg(7),
      R => '0'
    );
\mindex_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[8]\,
      Q => mindex_reg(8),
      R => '0'
    );
\mindex_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => maxdex_reg,
      D => \autolock_input_mindex_reg_n_0_[9]\,
      Q => mindex_reg(9),
      R => '0'
    );
pid_loop_0: entity work.system_AXI_PI_0_2_pid_loop
     port map (
      CO(0) => engage_pid_loop06_in,
      D(31 downto 0) => P_term_mon(31 downto 0),
      I_term_mon(31 downto 0) => I_term_mon(31 downto 0),
      \I_term_q_reg[0]_0\(2) => autolock,
      \I_term_q_reg[0]_0\(1) => ramp_enable,
      \I_term_q_reg[0]_0\(0) => p_0_in1_in,
      \I_term_q_reg[30]_0\(0) => pid_loop_0_n_35,
      \I_term_q_reg[31]_0\(0) => p_0_in_0(31),
      P_error_unshifted_reg_reg_0(31 downto 0) => P_reg(31 downto 0),
      Q(31 downto 0) => I_reg(31 downto 0),
      \autolock_input_maxdex_reg[0]\(0) => \^loop_locked_counter_reg_reg[30]_0\(0),
      \autolock_input_maxdex_reg[13]\(13 downto 0) => ramp_module_0_output(13 downto 0),
      \autolock_max_reg_reg[30]\(0) => engage_pid_loop0,
      \axi_pi_output_reg_reg[13]_inv_i_3_0\(13 downto 0) => autolock_0_input(13 downto 0),
      \axi_pi_output_reg_reg[13]_inv_i_3_1\(31 downto 0) => autolock_max_reg(31 downto 0),
      \axi_pi_output_reg_reg[13]_inv_i_4_0\(31 downto 0) => autolock_min_reg(31 downto 0),
      \control_reg_reg[1]\ => pid_loop_0_n_0,
      \error_reg_reg[13]_0\(13 downto 0) => pid_loop_0_error(13 downto 0),
      \loop_output_reg_reg[31]_0\(13 downto 0) => pid_loop_0_output(13 downto 0),
      \loop_output_reg_reg[31]_1\(13 downto 0) => autolock_input_maxdex0_in(13 downto 0),
      pid_loop_0_input_reg(13 downto 0) => pid_loop_0_input_reg(13 downto 0),
      ramp_module_0_reset(13 downto 0) => ramp_module_0_reset(31 downto 18),
      \ramplitude_min_reg_reg[31]_i_6_0\(13 downto 0) => ramp_offset_reg(31 downto 18),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn,
      \setpoint_reg_reg[13]_0\(13 downto 0) => setpoint_reg(13 downto 0)
    );
\pid_loop_0_input_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(0),
      Q => pid_loop_0_input_reg(0),
      R => '0'
    );
\pid_loop_0_input_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(10),
      Q => pid_loop_0_input_reg(10),
      R => '0'
    );
\pid_loop_0_input_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(11),
      Q => pid_loop_0_input_reg(11),
      R => '0'
    );
\pid_loop_0_input_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(12),
      Q => pid_loop_0_input_reg(12),
      R => '0'
    );
\pid_loop_0_input_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_input(0),
      Q => pid_loop_0_input_reg(13),
      R => '0'
    );
\pid_loop_0_input_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(1),
      Q => pid_loop_0_input_reg(1),
      R => '0'
    );
\pid_loop_0_input_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(2),
      Q => pid_loop_0_input_reg(2),
      R => '0'
    );
\pid_loop_0_input_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(3),
      Q => pid_loop_0_input_reg(3),
      R => '0'
    );
\pid_loop_0_input_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(4),
      Q => pid_loop_0_input_reg(4),
      R => '0'
    );
\pid_loop_0_input_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(5),
      Q => pid_loop_0_input_reg(5),
      R => '0'
    );
\pid_loop_0_input_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(6),
      Q => pid_loop_0_input_reg(6),
      R => '0'
    );
\pid_loop_0_input_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(7),
      Q => pid_loop_0_input_reg(7),
      R => '0'
    );
\pid_loop_0_input_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(8),
      Q => pid_loop_0_input_reg(8),
      R => '0'
    );
\pid_loop_0_input_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_input(9),
      Q => pid_loop_0_input_reg(9),
      R => '0'
    );
ramp_module_0: entity work.system_AXI_PI_0_2_ramp_module
     port map (
      CO(0) => engage_pid_loop06_in,
      D(3) => autolock,
      D(2) => ramp_enable,
      D(1) => pid_loop_0_n_0,
      D(0) => \control_reg_reg_n_0_[0]\,
      E(0) => maxdex_reg,
      \I_term_q_reg[31]\(0) => pid_loop_0_n_35,
      Q(0) => \control_reg_reg_n_0_[9]\,
      \autolock_input_maxdex_reg[0]\(0) => \autolock_input_max_reg[13]_i_2_n_1\,
      \autolock_input_mindex_reg[0]\(0) => \autolock_input_min_reg[13]_i_2_n_1\,
      \axi_pi_output_reg_reg[0]\(0) => engage_pid_loop0,
      \axi_pi_output_reg_reg[13]_inv\(13 downto 0) => pid_loop_0_output(13 downto 0),
      loop_locked_counter_reg_reg(31 downto 0) => loop_locked_counter_reg_reg(31 downto 0),
      \loop_locked_counter_reg_reg[30]\(0) => \^loop_locked_counter_reg_reg[30]_0\(0),
      ramp_corner_reg_reg_0(0) => autolock_input_mindex,
      ramp_corner_reg_reg_1(0) => ramp_module_0_n_33,
      ramp_module_0_reset(13 downto 0) => ramp_module_0_reset(31 downto 18),
      ramp_module_0_rst => ramp_module_0_rst,
      ramp_output(13 downto 0) => ramp_module_0_output(13 downto 0),
      \ramp_reg_reg[17]_0\(17 downto 0) => ramp_offset_reg(17 downto 0),
      \ramp_reg_reg[30]_0\(12) => ramp_module_0_n_15,
      \ramp_reg_reg[30]_0\(11) => ramp_module_0_n_16,
      \ramp_reg_reg[30]_0\(10) => ramp_module_0_n_17,
      \ramp_reg_reg[30]_0\(9) => ramp_module_0_n_18,
      \ramp_reg_reg[30]_0\(8) => ramp_module_0_n_19,
      \ramp_reg_reg[30]_0\(7) => ramp_module_0_n_20,
      \ramp_reg_reg[30]_0\(6) => ramp_module_0_n_21,
      \ramp_reg_reg[30]_0\(5) => ramp_module_0_n_22,
      \ramp_reg_reg[30]_0\(4) => ramp_module_0_n_23,
      \ramp_reg_reg[30]_0\(3) => ramp_module_0_n_24,
      \ramp_reg_reg[30]_0\(2) => ramp_module_0_n_25,
      \ramp_reg_reg[30]_0\(1) => ramp_module_0_n_26,
      \ramp_reg_reg[30]_0\(0) => ramp_module_0_n_27,
      \ramp_reg_reg[31]_0\(0) => p_0_in_0(31),
      \ramp_reg_reg[31]_1\ => ramp_module_0_n_28,
      \ramp_step_reg_reg[31]_0\(31 downto 0) => ramp_step_reg(31 downto 0),
      \ramplitude_lim_reg_reg[31]_0\(31 downto 0) => ramplitude_reg(31 downto 0),
      \ramplitude_step_reg_reg[31]_0\(31 downto 0) => ramplitude_step_reg(31 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn,
      \status_reg_reg[10]\(31 downto 0) => loop_locked_delay_reg(31 downto 0)
    );
\ramp_offset_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramp_offset_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \ramp_offset_reg[15]_i_1_n_0\
    );
\ramp_offset_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramp_offset_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \ramp_offset_reg[23]_i_1_n_0\
    );
\ramp_offset_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramp_offset_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \ramp_offset_reg[31]_i_1_n_0\
    );
\ramp_offset_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \ramp_offset_reg[31]_i_2_n_0\
    );
\ramp_offset_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramp_offset_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \ramp_offset_reg[7]_i_1_n_0\
    );
\ramp_offset_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => ramp_offset_reg(0),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => ramp_offset_reg(10),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => ramp_offset_reg(11),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => ramp_offset_reg(12),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => ramp_offset_reg(13),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => ramp_offset_reg(14),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => ramp_offset_reg(15),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => ramp_offset_reg(16),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => ramp_offset_reg(17),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => ramp_offset_reg(18),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => ramp_offset_reg(19),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => ramp_offset_reg(1),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => ramp_offset_reg(20),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => ramp_offset_reg(21),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => ramp_offset_reg(22),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => ramp_offset_reg(23),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => ramp_offset_reg(24),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => ramp_offset_reg(25),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => ramp_offset_reg(26),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => ramp_offset_reg(27),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => ramp_offset_reg(28),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => ramp_offset_reg(29),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => ramp_offset_reg(2),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => ramp_offset_reg(30),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => ramp_offset_reg(31),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => ramp_offset_reg(3),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => ramp_offset_reg(4),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => ramp_offset_reg(5),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => ramp_offset_reg(6),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => ramp_offset_reg(7),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => ramp_offset_reg(8),
      R => pid_loop_0_rst1
    );
\ramp_offset_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_offset_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => ramp_offset_reg(9),
      R => pid_loop_0_rst1
    );
\ramp_output_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(0),
      Q => ramp_output_reg(0),
      R => '0'
    );
\ramp_output_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(10),
      Q => ramp_output_reg(10),
      R => '0'
    );
\ramp_output_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(11),
      Q => ramp_output_reg(11),
      R => '0'
    );
\ramp_output_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(12),
      Q => ramp_output_reg(12),
      R => '0'
    );
\ramp_output_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(1),
      Q => ramp_output_reg(1),
      R => '0'
    );
\ramp_output_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(2),
      Q => ramp_output_reg(2),
      R => '0'
    );
\ramp_output_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(13),
      Q => ramp_output_reg(31),
      R => '0'
    );
\ramp_output_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(3),
      Q => ramp_output_reg(3),
      R => '0'
    );
\ramp_output_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(4),
      Q => ramp_output_reg(4),
      R => '0'
    );
\ramp_output_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(5),
      Q => ramp_output_reg(5),
      R => '0'
    );
\ramp_output_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(6),
      Q => ramp_output_reg(6),
      R => '0'
    );
\ramp_output_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(7),
      Q => ramp_output_reg(7),
      R => '0'
    );
\ramp_output_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(8),
      Q => ramp_output_reg(8),
      R => '0'
    );
\ramp_output_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_output(9),
      Q => ramp_output_reg(9),
      R => '0'
    );
\ramp_step_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramp_step_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \ramp_step_reg[15]_i_1_n_0\
    );
\ramp_step_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramp_step_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \ramp_step_reg[23]_i_1_n_0\
    );
\ramp_step_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramp_step_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \ramp_step_reg[31]_i_1_n_0\
    );
\ramp_step_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000002000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \ramp_step_reg[31]_i_2_n_0\
    );
\ramp_step_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramp_step_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \ramp_step_reg[7]_i_1_n_0\
    );
\ramp_step_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => ramp_step_reg(0),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => ramp_step_reg(10),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => ramp_step_reg(11),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => ramp_step_reg(12),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => ramp_step_reg(13),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => ramp_step_reg(14),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => ramp_step_reg(15),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => ramp_step_reg(16),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => ramp_step_reg(17),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => ramp_step_reg(18),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => ramp_step_reg(19),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => ramp_step_reg(1),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => ramp_step_reg(20),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => ramp_step_reg(21),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => ramp_step_reg(22),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => ramp_step_reg(23),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => ramp_step_reg(24),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => ramp_step_reg(25),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => ramp_step_reg(26),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => ramp_step_reg(27),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => ramp_step_reg(28),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => ramp_step_reg(29),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => ramp_step_reg(2),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => ramp_step_reg(30),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => ramp_step_reg(31),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => ramp_step_reg(3),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => ramp_step_reg(4),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => ramp_step_reg(5),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => ramp_step_reg(6),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => ramp_step_reg(7),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => ramp_step_reg(8),
      R => pid_loop_0_rst1
    );
\ramp_step_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramp_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => ramp_step_reg(9),
      R => pid_loop_0_rst1
    );
\ramplitude_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramplitude_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \ramplitude_reg[15]_i_1_n_0\
    );
\ramplitude_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramplitude_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \ramplitude_reg[23]_i_1_n_0\
    );
\ramplitude_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramplitude_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \ramplitude_reg[31]_i_1_n_0\
    );
\ramplitude_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000100000000000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \ramplitude_reg[31]_i_2_n_0\
    );
\ramplitude_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramplitude_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \ramplitude_reg[7]_i_1_n_0\
    );
\ramplitude_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => ramplitude_reg(0),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => ramplitude_reg(10),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => ramplitude_reg(11),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => ramplitude_reg(12),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => ramplitude_reg(13),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => ramplitude_reg(14),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => ramplitude_reg(15),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => ramplitude_reg(16),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => ramplitude_reg(17),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => ramplitude_reg(18),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => ramplitude_reg(19),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => ramplitude_reg(1),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => ramplitude_reg(20),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => ramplitude_reg(21),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => ramplitude_reg(22),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => ramplitude_reg(23),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => ramplitude_reg(24),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => ramplitude_reg(25),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => ramplitude_reg(26),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => ramplitude_reg(27),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => ramplitude_reg(28),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => ramplitude_reg(29),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => ramplitude_reg(2),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => ramplitude_reg(30),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => ramplitude_reg(31),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => ramplitude_reg(3),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => ramplitude_reg(4),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => ramplitude_reg(5),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => ramplitude_reg(6),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => ramplitude_reg(7),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => ramplitude_reg(8),
      R => pid_loop_0_rst1
    );
\ramplitude_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => ramplitude_reg(9),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramplitude_step_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \ramplitude_step_reg[15]_i_1_n_0\
    );
\ramplitude_step_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramplitude_step_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \ramplitude_step_reg[23]_i_1_n_0\
    );
\ramplitude_step_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramplitude_step_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \ramplitude_step_reg[31]_i_1_n_0\
    );
\ramplitude_step_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \ramplitude_step_reg[31]_i_2_n_0\
    );
\ramplitude_step_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ramplitude_step_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \ramplitude_step_reg[7]_i_1_n_0\
    );
\ramplitude_step_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => ramplitude_step_reg(0),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => ramplitude_step_reg(10),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => ramplitude_step_reg(11),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => ramplitude_step_reg(12),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => ramplitude_step_reg(13),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => ramplitude_step_reg(14),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => ramplitude_step_reg(15),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => ramplitude_step_reg(16),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => ramplitude_step_reg(17),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => ramplitude_step_reg(18),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => ramplitude_step_reg(19),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => ramplitude_step_reg(1),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => ramplitude_step_reg(20),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => ramplitude_step_reg(21),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => ramplitude_step_reg(22),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => ramplitude_step_reg(23),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => ramplitude_step_reg(24),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => ramplitude_step_reg(25),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => ramplitude_step_reg(26),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => ramplitude_step_reg(27),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => ramplitude_step_reg(28),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => ramplitude_step_reg(29),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => ramplitude_step_reg(2),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => ramplitude_step_reg(30),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => ramplitude_step_reg(31),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => ramplitude_step_reg(3),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => ramplitude_step_reg(4),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => ramplitude_step_reg(5),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => ramplitude_step_reg(6),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => ramplitude_step_reg(7),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => ramplitude_step_reg(8),
      R => pid_loop_0_rst1
    );
\ramplitude_step_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ramplitude_step_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => ramplitude_step_reg(9),
      R => pid_loop_0_rst1
    );
\setpoint_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \setpoint_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(1),
      O => \setpoint_reg[15]_i_1_n_0\
    );
\setpoint_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \setpoint_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(2),
      O => \setpoint_reg[23]_i_1_n_0\
    );
\setpoint_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \setpoint_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(3),
      O => \setpoint_reg[31]_i_1_n_0\
    );
\setpoint_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(3),
      I2 => \control_reg[31]_i_3_n_0\,
      I3 => p_0_in(4),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \setpoint_reg[31]_i_2_n_0\
    );
\setpoint_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \setpoint_reg[31]_i_2_n_0\,
      I1 => s00_axi_wstrb(0),
      O => \setpoint_reg[7]_i_1_n_0\
    );
\setpoint_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => setpoint_reg(0),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => setpoint_reg(10),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => setpoint_reg(11),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => setpoint_reg(12),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => setpoint_reg(13),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \setpoint_reg__0\(14),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \setpoint_reg__0\(15),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \setpoint_reg__0\(16),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \setpoint_reg__0\(17),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \setpoint_reg__0\(18),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \setpoint_reg__0\(19),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => setpoint_reg(1),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \setpoint_reg__0\(20),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \setpoint_reg__0\(21),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \setpoint_reg__0\(22),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \setpoint_reg__0\(23),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \setpoint_reg__0\(24),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \setpoint_reg__0\(25),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \setpoint_reg__0\(26),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \setpoint_reg__0\(27),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \setpoint_reg__0\(28),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \setpoint_reg__0\(29),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => setpoint_reg(2),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \setpoint_reg__0\(30),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \setpoint_reg__0\(31),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => setpoint_reg(3),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => setpoint_reg(4),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => setpoint_reg(5),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => setpoint_reg(6),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => setpoint_reg(7),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => setpoint_reg(8),
      R => pid_loop_0_rst1
    );
\setpoint_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \setpoint_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => setpoint_reg(9),
      R => pid_loop_0_rst1
    );
\status_reg[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => autolock,
      I1 => ramp_enable,
      O => p_7_in
    );
\status_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \control_reg_reg_n_0_[0]\,
      Q => status_reg(0),
      R => '0'
    );
\status_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \^loop_locked_counter_reg_reg[30]_0\(0),
      Q => status_reg(10),
      R => '0'
    );
\status_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => p_7_in,
      Q => status_reg(11),
      R => '0'
    );
\status_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => pid_loop_0_n_0,
      Q => status_reg(1),
      R => '0'
    );
\status_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_enable,
      Q => status_reg(4),
      R => '0'
    );
\status_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => autolock,
      Q => status_reg(8),
      R => '0'
    );
\status_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => ramp_module_0_rst,
      Q => status_reg(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_AXI_PI_0_2_AXI_PI_v1_0 is
  port (
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    axi_pi_output : out STD_LOGIC_VECTOR ( 13 downto 0 );
    S_AXI_ARREADY : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    input_reset : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    output_sel : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    autolock_input : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    pid_loop_input : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end system_AXI_PI_0_2_AXI_PI_v1_0;

architecture STRUCTURE of system_AXI_PI_0_2_AXI_PI_v1_0 is
  signal AXI_PI_v1_0_S00_AXI_inst_n_4 : STD_LOGIC;
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal autolock_input_tc : STD_LOGIC_VECTOR ( 13 to 13 );
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal pid_loop_0_input : STD_LOGIC_VECTOR ( 13 to 13 );
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
AXI_PI_v1_0_S00_AXI_inst: entity work.system_AXI_PI_0_2_AXI_PI_v1_0_S00_AXI
     port map (
      D(13) => autolock_input_tc(13),
      D(12 downto 0) => autolock_input(12 downto 0),
      aw_en_reg_0 => AXI_PI_v1_0_S00_AXI_inst_n_4,
      aw_en_reg_1 => aw_en_i_1_n_0,
      axi_arready_reg_0 => \^s_axi_arready\,
      axi_awready_reg_0 => \^s_axi_awready\,
      axi_bvalid_reg_0 => axi_bvalid_i_1_n_0,
      axi_pi_output(13 downto 0) => axi_pi_output(13 downto 0),
      axi_rvalid_reg_0 => axi_rvalid_i_1_n_0,
      axi_wready_reg_0 => \^s_axi_wready\,
      input_reset => input_reset,
      \loop_locked_counter_reg_reg[30]_0\(0) => D(0),
      output_sel => output_sel,
      pid_loop_0_input(0) => pid_loop_0_input(13),
      pid_loop_input(12 downto 0) => pid_loop_input(12 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(4 downto 0) => s00_axi_araddr(4 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(4 downto 0) => s00_axi_awaddr(4 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bvalid => \^s00_axi_bvalid\,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rvalid => \^s00_axi_rvalid\,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
\autolock_0_input[13]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => autolock_input(13),
      O => autolock_input_tc(13)
    );
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8F8F8F808F8F8F8"
    )
        port map (
      I0 => \^s00_axi_bvalid\,
      I1 => s00_axi_bready,
      I2 => AXI_PI_v1_0_S00_AXI_inst_n_4,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_awvalid,
      I5 => \^s_axi_awready\,
      O => aw_en_i_1_n_0
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555555C0000000"
    )
        port map (
      I0 => s00_axi_bready,
      I1 => \^s_axi_awready\,
      I2 => s00_axi_awvalid,
      I3 => s00_axi_wvalid,
      I4 => \^s_axi_wready\,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
\pid_loop_0_input_reg[13]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pid_loop_input(13),
      O => pid_loop_0_input(13)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_AXI_PI_0_2 is
  port (
    pid_loop_input : in STD_LOGIC_VECTOR ( 13 downto 0 );
    axi_pi_output : out STD_LOGIC_VECTOR ( 13 downto 0 );
    autolock_input : in STD_LOGIC_VECTOR ( 13 downto 0 );
    output_sel : in STD_LOGIC;
    input_reset : out STD_LOGIC;
    loop_locked : out STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of system_AXI_PI_0_2 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of system_AXI_PI_0_2 : entity is "system_AXI_PI_0_2,AXI_PI_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of system_AXI_PI_0_2 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of system_AXI_PI_0_2 : entity is "AXI_PI_v1_0,Vivado 2019.1.1";
end system_AXI_PI_0_2;

architecture STRUCTURE of system_AXI_PI_0_2 is
  signal \<const0>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of input_reset : signal is "xilinx.com:signal:reset:1.0 input_reset RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of input_reset : signal is "XIL_INTERFACENAME input_reset, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute X_INTERFACE_PARAMETER of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 125000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute X_INTERFACE_INFO of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute X_INTERFACE_INFO of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute X_INTERFACE_INFO of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute X_INTERFACE_INFO of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute X_INTERFACE_INFO of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute X_INTERFACE_INFO of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute X_INTERFACE_PARAMETER of s00_axi_rready : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 32, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 125000000, ID_WIDTH 0, ADDR_WIDTH 7, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute X_INTERFACE_INFO of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute X_INTERFACE_INFO of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute X_INTERFACE_INFO of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute X_INTERFACE_INFO of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute X_INTERFACE_INFO of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute X_INTERFACE_INFO of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute X_INTERFACE_INFO of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute X_INTERFACE_INFO of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute X_INTERFACE_INFO of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute X_INTERFACE_INFO of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute X_INTERFACE_INFO of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.system_AXI_PI_0_2_AXI_PI_v1_0
     port map (
      D(0) => loop_locked,
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      autolock_input(13 downto 0) => autolock_input(13 downto 0),
      axi_pi_output(13 downto 0) => axi_pi_output(13 downto 0),
      input_reset => input_reset,
      output_sel => output_sel,
      pid_loop_input(13 downto 0) => pid_loop_input(13 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(4 downto 0) => s00_axi_araddr(6 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(4 downto 0) => s00_axi_awaddr(6 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
