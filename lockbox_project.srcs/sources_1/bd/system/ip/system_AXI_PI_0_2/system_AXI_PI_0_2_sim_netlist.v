// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1.1 (lin64) Build 2580384 Sat Jun 29 08:04:45 MDT 2019
// Date        : Wed Nov 27 11:41:10 2019
// Host        : laoshu running 64-bit unknown
// Command     : write_verilog -force -mode funcsim -rename_top system_AXI_PI_0_2 -prefix
//               system_AXI_PI_0_2_ system_AXI_PI_0_2_sim_netlist.v
// Design      : system_AXI_PI_0_2
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module system_AXI_PI_0_2_AXI_PI_v1_0
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    axi_pi_output,
    S_AXI_ARREADY,
    D,
    s00_axi_rdata,
    input_reset,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aresetn,
    output_sel,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    autolock_input,
    s00_axi_araddr,
    pid_loop_input,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_arvalid,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output [13:0]axi_pi_output;
  output S_AXI_ARREADY;
  output [0:0]D;
  output [31:0]s00_axi_rdata;
  output input_reset;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aresetn;
  input output_sel;
  input s00_axi_aclk;
  input [4:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [13:0]autolock_input;
  input [4:0]s00_axi_araddr;
  input [13:0]pid_loop_input;
  input [3:0]s00_axi_wstrb;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_arvalid;
  input s00_axi_bready;
  input s00_axi_rready;

  wire AXI_PI_v1_0_S00_AXI_inst_n_4;
  wire [0:0]D;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire [13:0]autolock_input;
  wire [13:13]autolock_input_tc;
  wire aw_en_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire [13:0]axi_pi_output;
  wire axi_rvalid_i_1_n_0;
  wire input_reset;
  wire output_sel;
  wire [13:13]pid_loop_0_input;
  wire [13:0]pid_loop_input;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  system_AXI_PI_0_2_AXI_PI_v1_0_S00_AXI AXI_PI_v1_0_S00_AXI_inst
       (.D({autolock_input_tc,autolock_input[12:0]}),
        .aw_en_reg_0(AXI_PI_v1_0_S00_AXI_inst_n_4),
        .aw_en_reg_1(aw_en_i_1_n_0),
        .axi_arready_reg_0(S_AXI_ARREADY),
        .axi_awready_reg_0(S_AXI_AWREADY),
        .axi_bvalid_reg_0(axi_bvalid_i_1_n_0),
        .axi_pi_output(axi_pi_output),
        .axi_rvalid_reg_0(axi_rvalid_i_1_n_0),
        .axi_wready_reg_0(S_AXI_WREADY),
        .input_reset(input_reset),
        .\loop_locked_counter_reg_reg[30]_0 (D),
        .output_sel(output_sel),
        .pid_loop_0_input(pid_loop_0_input),
        .pid_loop_input(pid_loop_input[12:0]),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
  LUT1 #(
    .INIT(2'h1)) 
    \autolock_0_input[13]_i_1 
       (.I0(autolock_input[13]),
        .O(autolock_input_tc));
  LUT6 #(
    .INIT(64'hF8F8F8F808F8F8F8)) 
    aw_en_i_1
       (.I0(s00_axi_bvalid),
        .I1(s00_axi_bready),
        .I2(AXI_PI_v1_0_S00_AXI_inst_n_4),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_awvalid),
        .I5(S_AXI_AWREADY),
        .O(aw_en_i_1_n_0));
  LUT6 #(
    .INIT(64'h55555555C0000000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_bready),
        .I1(S_AXI_AWREADY),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wvalid),
        .I4(S_AXI_WREADY),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \pid_loop_0_input_reg[13]_i_1 
       (.I0(pid_loop_input[13]),
        .O(pid_loop_0_input));
endmodule

module system_AXI_PI_0_2_AXI_PI_v1_0_S00_AXI
   (axi_awready_reg_0,
    axi_wready_reg_0,
    axi_arready_reg_0,
    s00_axi_bvalid,
    aw_en_reg_0,
    input_reset,
    s00_axi_rvalid,
    axi_pi_output,
    \loop_locked_counter_reg_reg[30]_0 ,
    s00_axi_rdata,
    s00_axi_aclk,
    pid_loop_input,
    pid_loop_0_input,
    axi_bvalid_reg_0,
    aw_en_reg_1,
    axi_rvalid_reg_0,
    s00_axi_aresetn,
    output_sel,
    s00_axi_awaddr,
    s00_axi_wdata,
    D,
    s00_axi_araddr,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_arvalid);
  output axi_awready_reg_0;
  output axi_wready_reg_0;
  output axi_arready_reg_0;
  output s00_axi_bvalid;
  output aw_en_reg_0;
  output input_reset;
  output s00_axi_rvalid;
  output [13:0]axi_pi_output;
  output [0:0]\loop_locked_counter_reg_reg[30]_0 ;
  output [31:0]s00_axi_rdata;
  input s00_axi_aclk;
  input [12:0]pid_loop_input;
  input [0:0]pid_loop_0_input;
  input axi_bvalid_reg_0;
  input aw_en_reg_1;
  input axi_rvalid_reg_0;
  input s00_axi_aresetn;
  input output_sel;
  input [4:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [13:0]D;
  input [4:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_arvalid;

  wire [13:0]D;
  wire [31:0]I_mon_reg;
  wire [31:0]I_reg;
  wire \I_reg[15]_i_1_n_0 ;
  wire \I_reg[23]_i_1_n_0 ;
  wire \I_reg[31]_i_1_n_0 ;
  wire \I_reg[31]_i_2_n_0 ;
  wire \I_reg[7]_i_1_n_0 ;
  wire [31:0]I_term_mon;
  wire [31:0]P_mon_reg;
  wire [31:0]P_reg;
  wire \P_reg[15]_i_1_n_0 ;
  wire \P_reg[23]_i_1_n_0 ;
  wire \P_reg[31]_i_1_n_0 ;
  wire \P_reg[31]_i_2_n_0 ;
  wire \P_reg[7]_i_1_n_0 ;
  wire [31:0]P_term_mon;
  wire autolock;
  wire [13:0]autolock_0_input;
  wire [13:0]autolock_input_max;
  wire \autolock_input_max[13]_i_10_n_0 ;
  wire \autolock_input_max[13]_i_11_n_0 ;
  wire \autolock_input_max[13]_i_12_n_0 ;
  wire \autolock_input_max[13]_i_13_n_0 ;
  wire \autolock_input_max[13]_i_14_n_0 ;
  wire \autolock_input_max[13]_i_15_n_0 ;
  wire \autolock_input_max[13]_i_16_n_0 ;
  wire \autolock_input_max[13]_i_17_n_0 ;
  wire \autolock_input_max[13]_i_4_n_0 ;
  wire \autolock_input_max[13]_i_5_n_0 ;
  wire \autolock_input_max[13]_i_6_n_0 ;
  wire \autolock_input_max[13]_i_7_n_0 ;
  wire \autolock_input_max[13]_i_8_n_0 ;
  wire \autolock_input_max[13]_i_9_n_0 ;
  wire \autolock_input_max_reg[13]_i_2_n_1 ;
  wire \autolock_input_max_reg[13]_i_2_n_2 ;
  wire \autolock_input_max_reg[13]_i_2_n_3 ;
  wire \autolock_input_max_reg[13]_i_3_n_0 ;
  wire \autolock_input_max_reg[13]_i_3_n_1 ;
  wire \autolock_input_max_reg[13]_i_3_n_2 ;
  wire \autolock_input_max_reg[13]_i_3_n_3 ;
  wire [13:0]autolock_input_maxdex;
  wire [13:0]autolock_input_maxdex0_in;
  wire [13:0]autolock_input_min;
  wire \autolock_input_min[13]_i_10_n_0 ;
  wire \autolock_input_min[13]_i_11_n_0 ;
  wire \autolock_input_min[13]_i_12_n_0 ;
  wire \autolock_input_min[13]_i_13_n_0 ;
  wire \autolock_input_min[13]_i_14_n_0 ;
  wire \autolock_input_min[13]_i_15_n_0 ;
  wire \autolock_input_min[13]_i_16_n_0 ;
  wire \autolock_input_min[13]_i_17_n_0 ;
  wire \autolock_input_min[13]_i_4_n_0 ;
  wire \autolock_input_min[13]_i_5_n_0 ;
  wire \autolock_input_min[13]_i_6_n_0 ;
  wire \autolock_input_min[13]_i_7_n_0 ;
  wire \autolock_input_min[13]_i_8_n_0 ;
  wire \autolock_input_min[13]_i_9_n_0 ;
  wire \autolock_input_min_reg[13]_i_2_n_1 ;
  wire \autolock_input_min_reg[13]_i_2_n_2 ;
  wire \autolock_input_min_reg[13]_i_2_n_3 ;
  wire \autolock_input_min_reg[13]_i_3_n_0 ;
  wire \autolock_input_min_reg[13]_i_3_n_1 ;
  wire \autolock_input_min_reg[13]_i_3_n_2 ;
  wire \autolock_input_min_reg[13]_i_3_n_3 ;
  wire autolock_input_mindex;
  wire \autolock_input_mindex_reg_n_0_[0] ;
  wire \autolock_input_mindex_reg_n_0_[10] ;
  wire \autolock_input_mindex_reg_n_0_[11] ;
  wire \autolock_input_mindex_reg_n_0_[12] ;
  wire \autolock_input_mindex_reg_n_0_[1] ;
  wire \autolock_input_mindex_reg_n_0_[2] ;
  wire \autolock_input_mindex_reg_n_0_[3] ;
  wire \autolock_input_mindex_reg_n_0_[4] ;
  wire \autolock_input_mindex_reg_n_0_[5] ;
  wire \autolock_input_mindex_reg_n_0_[6] ;
  wire \autolock_input_mindex_reg_n_0_[7] ;
  wire \autolock_input_mindex_reg_n_0_[8] ;
  wire \autolock_input_mindex_reg_n_0_[9] ;
  wire [13:0]autolock_input_reg;
  wire [31:0]autolock_max_reg;
  wire \autolock_max_reg[15]_i_1_n_0 ;
  wire \autolock_max_reg[23]_i_1_n_0 ;
  wire \autolock_max_reg[31]_i_1_n_0 ;
  wire \autolock_max_reg[31]_i_2_n_0 ;
  wire \autolock_max_reg[7]_i_1_n_0 ;
  wire [31:0]autolock_min_reg;
  wire \autolock_min_reg[15]_i_1_n_0 ;
  wire \autolock_min_reg[23]_i_1_n_0 ;
  wire \autolock_min_reg[31]_i_1_n_0 ;
  wire \autolock_min_reg[31]_i_2_n_0 ;
  wire \autolock_min_reg[7]_i_1_n_0 ;
  wire aw_en_reg_0;
  wire aw_en_reg_1;
  wire \axi_araddr_reg[2]_rep_n_0 ;
  wire \axi_araddr_reg[3]_rep_n_0 ;
  wire axi_arready0;
  wire axi_arready_reg_0;
  wire axi_awready0;
  wire axi_awready_reg_0;
  wire axi_bvalid_reg_0;
  wire [13:0]axi_pi_output;
  wire \axi_pi_output_reg[13]_inv_i_1_n_0 ;
  wire \axi_rdata[0]_i_10_n_0 ;
  wire \axi_rdata[0]_i_11_n_0 ;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[0]_i_6_n_0 ;
  wire \axi_rdata[0]_i_7_n_0 ;
  wire \axi_rdata[0]_i_8_n_0 ;
  wire \axi_rdata[0]_i_9_n_0 ;
  wire \axi_rdata[10]_i_10_n_0 ;
  wire \axi_rdata[10]_i_11_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[10]_i_6_n_0 ;
  wire \axi_rdata[10]_i_7_n_0 ;
  wire \axi_rdata[10]_i_8_n_0 ;
  wire \axi_rdata[10]_i_9_n_0 ;
  wire \axi_rdata[11]_i_10_n_0 ;
  wire \axi_rdata[11]_i_11_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[11]_i_6_n_0 ;
  wire \axi_rdata[11]_i_7_n_0 ;
  wire \axi_rdata[11]_i_8_n_0 ;
  wire \axi_rdata[11]_i_9_n_0 ;
  wire \axi_rdata[12]_i_10_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[12]_i_5_n_0 ;
  wire \axi_rdata[12]_i_6_n_0 ;
  wire \axi_rdata[12]_i_7_n_0 ;
  wire \axi_rdata[12]_i_8_n_0 ;
  wire \axi_rdata[12]_i_9_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[13]_i_4_n_0 ;
  wire \axi_rdata[13]_i_5_n_0 ;
  wire \axi_rdata[13]_i_6_n_0 ;
  wire \axi_rdata[13]_i_7_n_0 ;
  wire \axi_rdata[13]_i_8_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[14]_i_4_n_0 ;
  wire \axi_rdata[14]_i_5_n_0 ;
  wire \axi_rdata[14]_i_6_n_0 ;
  wire \axi_rdata[14]_i_7_n_0 ;
  wire \axi_rdata[14]_i_8_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[15]_i_5_n_0 ;
  wire \axi_rdata[15]_i_6_n_0 ;
  wire \axi_rdata[15]_i_7_n_0 ;
  wire \axi_rdata[15]_i_8_n_0 ;
  wire \axi_rdata[15]_i_9_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[16]_i_5_n_0 ;
  wire \axi_rdata[16]_i_6_n_0 ;
  wire \axi_rdata[16]_i_7_n_0 ;
  wire \axi_rdata[16]_i_8_n_0 ;
  wire \axi_rdata[16]_i_9_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[17]_i_5_n_0 ;
  wire \axi_rdata[17]_i_6_n_0 ;
  wire \axi_rdata[17]_i_7_n_0 ;
  wire \axi_rdata[17]_i_8_n_0 ;
  wire \axi_rdata[17]_i_9_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[18]_i_5_n_0 ;
  wire \axi_rdata[18]_i_6_n_0 ;
  wire \axi_rdata[18]_i_7_n_0 ;
  wire \axi_rdata[18]_i_8_n_0 ;
  wire \axi_rdata[18]_i_9_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[19]_i_5_n_0 ;
  wire \axi_rdata[19]_i_6_n_0 ;
  wire \axi_rdata[19]_i_7_n_0 ;
  wire \axi_rdata[19]_i_8_n_0 ;
  wire \axi_rdata[19]_i_9_n_0 ;
  wire \axi_rdata[1]_i_10_n_0 ;
  wire \axi_rdata[1]_i_11_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[1]_i_6_n_0 ;
  wire \axi_rdata[1]_i_7_n_0 ;
  wire \axi_rdata[1]_i_8_n_0 ;
  wire \axi_rdata[1]_i_9_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[20]_i_5_n_0 ;
  wire \axi_rdata[20]_i_6_n_0 ;
  wire \axi_rdata[20]_i_7_n_0 ;
  wire \axi_rdata[20]_i_8_n_0 ;
  wire \axi_rdata[20]_i_9_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[21]_i_5_n_0 ;
  wire \axi_rdata[21]_i_6_n_0 ;
  wire \axi_rdata[21]_i_7_n_0 ;
  wire \axi_rdata[21]_i_8_n_0 ;
  wire \axi_rdata[21]_i_9_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[22]_i_5_n_0 ;
  wire \axi_rdata[22]_i_6_n_0 ;
  wire \axi_rdata[22]_i_7_n_0 ;
  wire \axi_rdata[22]_i_8_n_0 ;
  wire \axi_rdata[22]_i_9_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[23]_i_5_n_0 ;
  wire \axi_rdata[23]_i_6_n_0 ;
  wire \axi_rdata[23]_i_7_n_0 ;
  wire \axi_rdata[23]_i_8_n_0 ;
  wire \axi_rdata[23]_i_9_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[24]_i_5_n_0 ;
  wire \axi_rdata[24]_i_6_n_0 ;
  wire \axi_rdata[24]_i_7_n_0 ;
  wire \axi_rdata[24]_i_8_n_0 ;
  wire \axi_rdata[24]_i_9_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[25]_i_5_n_0 ;
  wire \axi_rdata[25]_i_6_n_0 ;
  wire \axi_rdata[25]_i_7_n_0 ;
  wire \axi_rdata[25]_i_8_n_0 ;
  wire \axi_rdata[25]_i_9_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[26]_i_5_n_0 ;
  wire \axi_rdata[26]_i_6_n_0 ;
  wire \axi_rdata[26]_i_7_n_0 ;
  wire \axi_rdata[26]_i_8_n_0 ;
  wire \axi_rdata[26]_i_9_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[27]_i_5_n_0 ;
  wire \axi_rdata[27]_i_6_n_0 ;
  wire \axi_rdata[27]_i_7_n_0 ;
  wire \axi_rdata[27]_i_8_n_0 ;
  wire \axi_rdata[27]_i_9_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[28]_i_5_n_0 ;
  wire \axi_rdata[28]_i_6_n_0 ;
  wire \axi_rdata[28]_i_7_n_0 ;
  wire \axi_rdata[28]_i_8_n_0 ;
  wire \axi_rdata[28]_i_9_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[29]_i_4_n_0 ;
  wire \axi_rdata[29]_i_5_n_0 ;
  wire \axi_rdata[29]_i_6_n_0 ;
  wire \axi_rdata[29]_i_7_n_0 ;
  wire \axi_rdata[29]_i_8_n_0 ;
  wire \axi_rdata[2]_i_10_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[2]_i_5_n_0 ;
  wire \axi_rdata[2]_i_6_n_0 ;
  wire \axi_rdata[2]_i_7_n_0 ;
  wire \axi_rdata[2]_i_8_n_0 ;
  wire \axi_rdata[2]_i_9_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[30]_i_4_n_0 ;
  wire \axi_rdata[30]_i_5_n_0 ;
  wire \axi_rdata[30]_i_6_n_0 ;
  wire \axi_rdata[30]_i_7_n_0 ;
  wire \axi_rdata[30]_i_8_n_0 ;
  wire \axi_rdata[31]_i_10_n_0 ;
  wire \axi_rdata[31]_i_11_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[31]_i_4_n_0 ;
  wire \axi_rdata[31]_i_6_n_0 ;
  wire \axi_rdata[31]_i_7_n_0 ;
  wire \axi_rdata[31]_i_8_n_0 ;
  wire \axi_rdata[31]_i_9_n_0 ;
  wire \axi_rdata[3]_i_10_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[3]_i_5_n_0 ;
  wire \axi_rdata[3]_i_6_n_0 ;
  wire \axi_rdata[3]_i_7_n_0 ;
  wire \axi_rdata[3]_i_8_n_0 ;
  wire \axi_rdata[3]_i_9_n_0 ;
  wire \axi_rdata[4]_i_10_n_0 ;
  wire \axi_rdata[4]_i_11_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[4]_i_6_n_0 ;
  wire \axi_rdata[4]_i_7_n_0 ;
  wire \axi_rdata[4]_i_8_n_0 ;
  wire \axi_rdata[4]_i_9_n_0 ;
  wire \axi_rdata[5]_i_10_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[5]_i_5_n_0 ;
  wire \axi_rdata[5]_i_6_n_0 ;
  wire \axi_rdata[5]_i_7_n_0 ;
  wire \axi_rdata[5]_i_8_n_0 ;
  wire \axi_rdata[5]_i_9_n_0 ;
  wire \axi_rdata[6]_i_10_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[6]_i_5_n_0 ;
  wire \axi_rdata[6]_i_6_n_0 ;
  wire \axi_rdata[6]_i_7_n_0 ;
  wire \axi_rdata[6]_i_8_n_0 ;
  wire \axi_rdata[6]_i_9_n_0 ;
  wire \axi_rdata[7]_i_10_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[7]_i_5_n_0 ;
  wire \axi_rdata[7]_i_6_n_0 ;
  wire \axi_rdata[7]_i_7_n_0 ;
  wire \axi_rdata[7]_i_8_n_0 ;
  wire \axi_rdata[7]_i_9_n_0 ;
  wire \axi_rdata[8]_i_10_n_0 ;
  wire \axi_rdata[8]_i_11_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[8]_i_6_n_0 ;
  wire \axi_rdata[8]_i_7_n_0 ;
  wire \axi_rdata[8]_i_8_n_0 ;
  wire \axi_rdata[8]_i_9_n_0 ;
  wire \axi_rdata[9]_i_10_n_0 ;
  wire \axi_rdata[9]_i_11_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire \axi_rdata[9]_i_6_n_0 ;
  wire \axi_rdata[9]_i_7_n_0 ;
  wire \axi_rdata[9]_i_8_n_0 ;
  wire \axi_rdata[9]_i_9_n_0 ;
  wire \axi_rdata_reg[0]_i_4_n_0 ;
  wire \axi_rdata_reg[0]_i_5_n_0 ;
  wire \axi_rdata_reg[10]_i_4_n_0 ;
  wire \axi_rdata_reg[10]_i_5_n_0 ;
  wire \axi_rdata_reg[11]_i_4_n_0 ;
  wire \axi_rdata_reg[11]_i_5_n_0 ;
  wire \axi_rdata_reg[12]_i_4_n_0 ;
  wire \axi_rdata_reg[13]_i_3_n_0 ;
  wire \axi_rdata_reg[14]_i_3_n_0 ;
  wire \axi_rdata_reg[15]_i_4_n_0 ;
  wire \axi_rdata_reg[16]_i_4_n_0 ;
  wire \axi_rdata_reg[17]_i_4_n_0 ;
  wire \axi_rdata_reg[18]_i_4_n_0 ;
  wire \axi_rdata_reg[19]_i_4_n_0 ;
  wire \axi_rdata_reg[1]_i_4_n_0 ;
  wire \axi_rdata_reg[1]_i_5_n_0 ;
  wire \axi_rdata_reg[20]_i_4_n_0 ;
  wire \axi_rdata_reg[21]_i_4_n_0 ;
  wire \axi_rdata_reg[22]_i_4_n_0 ;
  wire \axi_rdata_reg[23]_i_4_n_0 ;
  wire \axi_rdata_reg[24]_i_4_n_0 ;
  wire \axi_rdata_reg[25]_i_4_n_0 ;
  wire \axi_rdata_reg[26]_i_4_n_0 ;
  wire \axi_rdata_reg[27]_i_4_n_0 ;
  wire \axi_rdata_reg[28]_i_4_n_0 ;
  wire \axi_rdata_reg[29]_i_3_n_0 ;
  wire \axi_rdata_reg[2]_i_4_n_0 ;
  wire \axi_rdata_reg[30]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_5_n_0 ;
  wire \axi_rdata_reg[3]_i_4_n_0 ;
  wire \axi_rdata_reg[4]_i_4_n_0 ;
  wire \axi_rdata_reg[4]_i_5_n_0 ;
  wire \axi_rdata_reg[5]_i_4_n_0 ;
  wire \axi_rdata_reg[6]_i_4_n_0 ;
  wire \axi_rdata_reg[7]_i_4_n_0 ;
  wire \axi_rdata_reg[8]_i_4_n_0 ;
  wire \axi_rdata_reg[8]_i_5_n_0 ;
  wire \axi_rdata_reg[9]_i_4_n_0 ;
  wire \axi_rdata_reg[9]_i_5_n_0 ;
  wire axi_rvalid_reg_0;
  wire axi_wready0;
  wire axi_wready_reg_0;
  wire \control_reg[15]_i_1_n_0 ;
  wire \control_reg[23]_i_1_n_0 ;
  wire \control_reg[31]_i_1_n_0 ;
  wire \control_reg[31]_i_2_n_0 ;
  wire \control_reg[31]_i_3_n_0 ;
  wire \control_reg[7]_i_1_n_0 ;
  wire \control_reg_reg_n_0_[0] ;
  wire \control_reg_reg_n_0_[10] ;
  wire \control_reg_reg_n_0_[11] ;
  wire \control_reg_reg_n_0_[12] ;
  wire \control_reg_reg_n_0_[13] ;
  wire \control_reg_reg_n_0_[14] ;
  wire \control_reg_reg_n_0_[15] ;
  wire \control_reg_reg_n_0_[16] ;
  wire \control_reg_reg_n_0_[17] ;
  wire \control_reg_reg_n_0_[18] ;
  wire \control_reg_reg_n_0_[19] ;
  wire \control_reg_reg_n_0_[20] ;
  wire \control_reg_reg_n_0_[21] ;
  wire \control_reg_reg_n_0_[22] ;
  wire \control_reg_reg_n_0_[23] ;
  wire \control_reg_reg_n_0_[24] ;
  wire \control_reg_reg_n_0_[25] ;
  wire \control_reg_reg_n_0_[26] ;
  wire \control_reg_reg_n_0_[27] ;
  wire \control_reg_reg_n_0_[28] ;
  wire \control_reg_reg_n_0_[29] ;
  wire \control_reg_reg_n_0_[2] ;
  wire \control_reg_reg_n_0_[30] ;
  wire \control_reg_reg_n_0_[31] ;
  wire \control_reg_reg_n_0_[3] ;
  wire \control_reg_reg_n_0_[5] ;
  wire \control_reg_reg_n_0_[6] ;
  wire \control_reg_reg_n_0_[7] ;
  wire \control_reg_reg_n_0_[9] ;
  wire engage_pid_loop0;
  wire engage_pid_loop06_in;
  wire [31:0]error_reg;
  wire input_railed0;
  wire input_railed00_in;
  wire [31:0]input_railed_max_reg;
  wire \input_railed_max_reg[15]_i_1_n_0 ;
  wire \input_railed_max_reg[23]_i_1_n_0 ;
  wire \input_railed_max_reg[31]_i_1_n_0 ;
  wire \input_railed_max_reg[31]_i_2_n_0 ;
  wire \input_railed_max_reg[7]_i_1_n_0 ;
  wire [31:0]input_railed_min_reg;
  wire \input_railed_min_reg[15]_i_1_n_0 ;
  wire \input_railed_min_reg[23]_i_1_n_0 ;
  wire \input_railed_min_reg[31]_i_1_n_0 ;
  wire \input_railed_min_reg[31]_i_2_n_0 ;
  wire \input_railed_min_reg[7]_i_1_n_0 ;
  wire input_reset;
  wire input_reset_reg_i_10_n_0;
  wire input_reset_reg_i_11_n_0;
  wire input_reset_reg_i_12_n_0;
  wire input_reset_reg_i_13_n_0;
  wire input_reset_reg_i_15_n_0;
  wire input_reset_reg_i_16_n_0;
  wire input_reset_reg_i_17_n_0;
  wire input_reset_reg_i_18_n_0;
  wire input_reset_reg_i_19_n_0;
  wire input_reset_reg_i_1_n_0;
  wire input_reset_reg_i_20_n_0;
  wire input_reset_reg_i_21_n_0;
  wire input_reset_reg_i_22_n_0;
  wire input_reset_reg_i_24_n_0;
  wire input_reset_reg_i_25_n_0;
  wire input_reset_reg_i_26_n_0;
  wire input_reset_reg_i_27_n_0;
  wire input_reset_reg_i_28_n_0;
  wire input_reset_reg_i_29_n_0;
  wire input_reset_reg_i_30_n_0;
  wire input_reset_reg_i_31_n_0;
  wire input_reset_reg_i_33_n_0;
  wire input_reset_reg_i_34_n_0;
  wire input_reset_reg_i_35_n_0;
  wire input_reset_reg_i_36_n_0;
  wire input_reset_reg_i_37_n_0;
  wire input_reset_reg_i_38_n_0;
  wire input_reset_reg_i_39_n_0;
  wire input_reset_reg_i_40_n_0;
  wire input_reset_reg_i_42_n_0;
  wire input_reset_reg_i_43_n_0;
  wire input_reset_reg_i_44_n_0;
  wire input_reset_reg_i_45_n_0;
  wire input_reset_reg_i_46_n_0;
  wire input_reset_reg_i_47_n_0;
  wire input_reset_reg_i_48_n_0;
  wire input_reset_reg_i_49_n_0;
  wire input_reset_reg_i_51_n_0;
  wire input_reset_reg_i_52_n_0;
  wire input_reset_reg_i_53_n_0;
  wire input_reset_reg_i_54_n_0;
  wire input_reset_reg_i_55_n_0;
  wire input_reset_reg_i_56_n_0;
  wire input_reset_reg_i_57_n_0;
  wire input_reset_reg_i_58_n_0;
  wire input_reset_reg_i_59_n_0;
  wire input_reset_reg_i_60_n_0;
  wire input_reset_reg_i_61_n_0;
  wire input_reset_reg_i_62_n_0;
  wire input_reset_reg_i_63_n_0;
  wire input_reset_reg_i_64_n_0;
  wire input_reset_reg_i_65_n_0;
  wire input_reset_reg_i_66_n_0;
  wire input_reset_reg_i_67_n_0;
  wire input_reset_reg_i_68_n_0;
  wire input_reset_reg_i_69_n_0;
  wire input_reset_reg_i_6_n_0;
  wire input_reset_reg_i_70_n_0;
  wire input_reset_reg_i_71_n_0;
  wire input_reset_reg_i_72_n_0;
  wire input_reset_reg_i_73_n_0;
  wire input_reset_reg_i_74_n_0;
  wire input_reset_reg_i_7_n_0;
  wire input_reset_reg_i_8_n_0;
  wire input_reset_reg_i_9_n_0;
  wire input_reset_reg_reg_i_14_n_0;
  wire input_reset_reg_reg_i_14_n_1;
  wire input_reset_reg_reg_i_14_n_2;
  wire input_reset_reg_reg_i_14_n_3;
  wire input_reset_reg_reg_i_23_n_0;
  wire input_reset_reg_reg_i_23_n_1;
  wire input_reset_reg_reg_i_23_n_2;
  wire input_reset_reg_reg_i_23_n_3;
  wire input_reset_reg_reg_i_2_n_1;
  wire input_reset_reg_reg_i_2_n_2;
  wire input_reset_reg_reg_i_2_n_3;
  wire input_reset_reg_reg_i_32_n_0;
  wire input_reset_reg_reg_i_32_n_1;
  wire input_reset_reg_reg_i_32_n_2;
  wire input_reset_reg_reg_i_32_n_3;
  wire input_reset_reg_reg_i_3_n_1;
  wire input_reset_reg_reg_i_3_n_2;
  wire input_reset_reg_reg_i_3_n_3;
  wire input_reset_reg_reg_i_41_n_0;
  wire input_reset_reg_reg_i_41_n_1;
  wire input_reset_reg_reg_i_41_n_2;
  wire input_reset_reg_reg_i_41_n_3;
  wire input_reset_reg_reg_i_50_n_0;
  wire input_reset_reg_reg_i_50_n_1;
  wire input_reset_reg_reg_i_50_n_2;
  wire input_reset_reg_reg_i_50_n_3;
  wire input_reset_reg_reg_i_5_n_0;
  wire input_reset_reg_reg_i_5_n_1;
  wire input_reset_reg_reg_i_5_n_2;
  wire input_reset_reg_reg_i_5_n_3;
  wire input_stable;
  wire input_stable0;
  wire input_stable02_in;
  wire \loop_locked_counter_reg[0]_i_10_n_0 ;
  wire \loop_locked_counter_reg[0]_i_11_n_0 ;
  wire \loop_locked_counter_reg[0]_i_12_n_0 ;
  wire \loop_locked_counter_reg[0]_i_13_n_0 ;
  wire \loop_locked_counter_reg[0]_i_14_n_0 ;
  wire \loop_locked_counter_reg[0]_i_15_n_0 ;
  wire \loop_locked_counter_reg[0]_i_17_n_0 ;
  wire \loop_locked_counter_reg[0]_i_18_n_0 ;
  wire \loop_locked_counter_reg[0]_i_19_n_0 ;
  wire \loop_locked_counter_reg[0]_i_1_n_0 ;
  wire \loop_locked_counter_reg[0]_i_20_n_0 ;
  wire \loop_locked_counter_reg[0]_i_21_n_0 ;
  wire \loop_locked_counter_reg[0]_i_22_n_0 ;
  wire \loop_locked_counter_reg[0]_i_23_n_0 ;
  wire \loop_locked_counter_reg[0]_i_24_n_0 ;
  wire \loop_locked_counter_reg[0]_i_26_n_0 ;
  wire \loop_locked_counter_reg[0]_i_27_n_0 ;
  wire \loop_locked_counter_reg[0]_i_28_n_0 ;
  wire \loop_locked_counter_reg[0]_i_29_n_0 ;
  wire \loop_locked_counter_reg[0]_i_30_n_0 ;
  wire \loop_locked_counter_reg[0]_i_31_n_0 ;
  wire \loop_locked_counter_reg[0]_i_32_n_0 ;
  wire \loop_locked_counter_reg[0]_i_33_n_0 ;
  wire \loop_locked_counter_reg[0]_i_35_n_0 ;
  wire \loop_locked_counter_reg[0]_i_36_n_0 ;
  wire \loop_locked_counter_reg[0]_i_37_n_0 ;
  wire \loop_locked_counter_reg[0]_i_38_n_0 ;
  wire \loop_locked_counter_reg[0]_i_39_n_0 ;
  wire \loop_locked_counter_reg[0]_i_40_n_0 ;
  wire \loop_locked_counter_reg[0]_i_41_n_0 ;
  wire \loop_locked_counter_reg[0]_i_42_n_0 ;
  wire \loop_locked_counter_reg[0]_i_44_n_0 ;
  wire \loop_locked_counter_reg[0]_i_45_n_0 ;
  wire \loop_locked_counter_reg[0]_i_46_n_0 ;
  wire \loop_locked_counter_reg[0]_i_47_n_0 ;
  wire \loop_locked_counter_reg[0]_i_48_n_0 ;
  wire \loop_locked_counter_reg[0]_i_49_n_0 ;
  wire \loop_locked_counter_reg[0]_i_50_n_0 ;
  wire \loop_locked_counter_reg[0]_i_51_n_0 ;
  wire \loop_locked_counter_reg[0]_i_53_n_0 ;
  wire \loop_locked_counter_reg[0]_i_54_n_0 ;
  wire \loop_locked_counter_reg[0]_i_55_n_0 ;
  wire \loop_locked_counter_reg[0]_i_56_n_0 ;
  wire \loop_locked_counter_reg[0]_i_57_n_0 ;
  wire \loop_locked_counter_reg[0]_i_58_n_0 ;
  wire \loop_locked_counter_reg[0]_i_59_n_0 ;
  wire \loop_locked_counter_reg[0]_i_60_n_0 ;
  wire \loop_locked_counter_reg[0]_i_61_n_0 ;
  wire \loop_locked_counter_reg[0]_i_62_n_0 ;
  wire \loop_locked_counter_reg[0]_i_63_n_0 ;
  wire \loop_locked_counter_reg[0]_i_64_n_0 ;
  wire \loop_locked_counter_reg[0]_i_65_n_0 ;
  wire \loop_locked_counter_reg[0]_i_66_n_0 ;
  wire \loop_locked_counter_reg[0]_i_67_n_0 ;
  wire \loop_locked_counter_reg[0]_i_68_n_0 ;
  wire \loop_locked_counter_reg[0]_i_69_n_0 ;
  wire \loop_locked_counter_reg[0]_i_6_n_0 ;
  wire \loop_locked_counter_reg[0]_i_70_n_0 ;
  wire \loop_locked_counter_reg[0]_i_71_n_0 ;
  wire \loop_locked_counter_reg[0]_i_72_n_0 ;
  wire \loop_locked_counter_reg[0]_i_73_n_0 ;
  wire \loop_locked_counter_reg[0]_i_74_n_0 ;
  wire \loop_locked_counter_reg[0]_i_75_n_0 ;
  wire \loop_locked_counter_reg[0]_i_76_n_0 ;
  wire \loop_locked_counter_reg[0]_i_8_n_0 ;
  wire \loop_locked_counter_reg[0]_i_9_n_0 ;
  wire [31:0]loop_locked_counter_reg_reg;
  wire \loop_locked_counter_reg_reg[0]_i_16_n_0 ;
  wire \loop_locked_counter_reg_reg[0]_i_16_n_1 ;
  wire \loop_locked_counter_reg_reg[0]_i_16_n_2 ;
  wire \loop_locked_counter_reg_reg[0]_i_16_n_3 ;
  wire \loop_locked_counter_reg_reg[0]_i_25_n_0 ;
  wire \loop_locked_counter_reg_reg[0]_i_25_n_1 ;
  wire \loop_locked_counter_reg_reg[0]_i_25_n_2 ;
  wire \loop_locked_counter_reg_reg[0]_i_25_n_3 ;
  wire \loop_locked_counter_reg_reg[0]_i_34_n_0 ;
  wire \loop_locked_counter_reg_reg[0]_i_34_n_1 ;
  wire \loop_locked_counter_reg_reg[0]_i_34_n_2 ;
  wire \loop_locked_counter_reg_reg[0]_i_34_n_3 ;
  wire \loop_locked_counter_reg_reg[0]_i_3_n_0 ;
  wire \loop_locked_counter_reg_reg[0]_i_3_n_1 ;
  wire \loop_locked_counter_reg_reg[0]_i_3_n_2 ;
  wire \loop_locked_counter_reg_reg[0]_i_3_n_3 ;
  wire \loop_locked_counter_reg_reg[0]_i_3_n_4 ;
  wire \loop_locked_counter_reg_reg[0]_i_3_n_5 ;
  wire \loop_locked_counter_reg_reg[0]_i_3_n_6 ;
  wire \loop_locked_counter_reg_reg[0]_i_3_n_7 ;
  wire \loop_locked_counter_reg_reg[0]_i_43_n_0 ;
  wire \loop_locked_counter_reg_reg[0]_i_43_n_1 ;
  wire \loop_locked_counter_reg_reg[0]_i_43_n_2 ;
  wire \loop_locked_counter_reg_reg[0]_i_43_n_3 ;
  wire \loop_locked_counter_reg_reg[0]_i_4_n_1 ;
  wire \loop_locked_counter_reg_reg[0]_i_4_n_2 ;
  wire \loop_locked_counter_reg_reg[0]_i_4_n_3 ;
  wire \loop_locked_counter_reg_reg[0]_i_52_n_0 ;
  wire \loop_locked_counter_reg_reg[0]_i_52_n_1 ;
  wire \loop_locked_counter_reg_reg[0]_i_52_n_2 ;
  wire \loop_locked_counter_reg_reg[0]_i_52_n_3 ;
  wire \loop_locked_counter_reg_reg[0]_i_5_n_1 ;
  wire \loop_locked_counter_reg_reg[0]_i_5_n_2 ;
  wire \loop_locked_counter_reg_reg[0]_i_5_n_3 ;
  wire \loop_locked_counter_reg_reg[0]_i_7_n_0 ;
  wire \loop_locked_counter_reg_reg[0]_i_7_n_1 ;
  wire \loop_locked_counter_reg_reg[0]_i_7_n_2 ;
  wire \loop_locked_counter_reg_reg[0]_i_7_n_3 ;
  wire \loop_locked_counter_reg_reg[12]_i_1_n_0 ;
  wire \loop_locked_counter_reg_reg[12]_i_1_n_1 ;
  wire \loop_locked_counter_reg_reg[12]_i_1_n_2 ;
  wire \loop_locked_counter_reg_reg[12]_i_1_n_3 ;
  wire \loop_locked_counter_reg_reg[12]_i_1_n_4 ;
  wire \loop_locked_counter_reg_reg[12]_i_1_n_5 ;
  wire \loop_locked_counter_reg_reg[12]_i_1_n_6 ;
  wire \loop_locked_counter_reg_reg[12]_i_1_n_7 ;
  wire \loop_locked_counter_reg_reg[16]_i_1_n_0 ;
  wire \loop_locked_counter_reg_reg[16]_i_1_n_1 ;
  wire \loop_locked_counter_reg_reg[16]_i_1_n_2 ;
  wire \loop_locked_counter_reg_reg[16]_i_1_n_3 ;
  wire \loop_locked_counter_reg_reg[16]_i_1_n_4 ;
  wire \loop_locked_counter_reg_reg[16]_i_1_n_5 ;
  wire \loop_locked_counter_reg_reg[16]_i_1_n_6 ;
  wire \loop_locked_counter_reg_reg[16]_i_1_n_7 ;
  wire \loop_locked_counter_reg_reg[20]_i_1_n_0 ;
  wire \loop_locked_counter_reg_reg[20]_i_1_n_1 ;
  wire \loop_locked_counter_reg_reg[20]_i_1_n_2 ;
  wire \loop_locked_counter_reg_reg[20]_i_1_n_3 ;
  wire \loop_locked_counter_reg_reg[20]_i_1_n_4 ;
  wire \loop_locked_counter_reg_reg[20]_i_1_n_5 ;
  wire \loop_locked_counter_reg_reg[20]_i_1_n_6 ;
  wire \loop_locked_counter_reg_reg[20]_i_1_n_7 ;
  wire \loop_locked_counter_reg_reg[24]_i_1_n_0 ;
  wire \loop_locked_counter_reg_reg[24]_i_1_n_1 ;
  wire \loop_locked_counter_reg_reg[24]_i_1_n_2 ;
  wire \loop_locked_counter_reg_reg[24]_i_1_n_3 ;
  wire \loop_locked_counter_reg_reg[24]_i_1_n_4 ;
  wire \loop_locked_counter_reg_reg[24]_i_1_n_5 ;
  wire \loop_locked_counter_reg_reg[24]_i_1_n_6 ;
  wire \loop_locked_counter_reg_reg[24]_i_1_n_7 ;
  wire \loop_locked_counter_reg_reg[28]_i_1_n_1 ;
  wire \loop_locked_counter_reg_reg[28]_i_1_n_2 ;
  wire \loop_locked_counter_reg_reg[28]_i_1_n_3 ;
  wire \loop_locked_counter_reg_reg[28]_i_1_n_4 ;
  wire \loop_locked_counter_reg_reg[28]_i_1_n_5 ;
  wire \loop_locked_counter_reg_reg[28]_i_1_n_6 ;
  wire \loop_locked_counter_reg_reg[28]_i_1_n_7 ;
  wire [0:0]\loop_locked_counter_reg_reg[30]_0 ;
  wire \loop_locked_counter_reg_reg[4]_i_1_n_0 ;
  wire \loop_locked_counter_reg_reg[4]_i_1_n_1 ;
  wire \loop_locked_counter_reg_reg[4]_i_1_n_2 ;
  wire \loop_locked_counter_reg_reg[4]_i_1_n_3 ;
  wire \loop_locked_counter_reg_reg[4]_i_1_n_4 ;
  wire \loop_locked_counter_reg_reg[4]_i_1_n_5 ;
  wire \loop_locked_counter_reg_reg[4]_i_1_n_6 ;
  wire \loop_locked_counter_reg_reg[4]_i_1_n_7 ;
  wire \loop_locked_counter_reg_reg[8]_i_1_n_0 ;
  wire \loop_locked_counter_reg_reg[8]_i_1_n_1 ;
  wire \loop_locked_counter_reg_reg[8]_i_1_n_2 ;
  wire \loop_locked_counter_reg_reg[8]_i_1_n_3 ;
  wire \loop_locked_counter_reg_reg[8]_i_1_n_4 ;
  wire \loop_locked_counter_reg_reg[8]_i_1_n_5 ;
  wire \loop_locked_counter_reg_reg[8]_i_1_n_6 ;
  wire \loop_locked_counter_reg_reg[8]_i_1_n_7 ;
  wire [31:0]loop_locked_delay_reg;
  wire \loop_locked_delay_reg[15]_i_1_n_0 ;
  wire \loop_locked_delay_reg[23]_i_1_n_0 ;
  wire \loop_locked_delay_reg[31]_i_1_n_0 ;
  wire \loop_locked_delay_reg[31]_i_2_n_0 ;
  wire \loop_locked_delay_reg[7]_i_1_n_0 ;
  wire [31:0]loop_locked_max_reg;
  wire \loop_locked_max_reg[15]_i_1_n_0 ;
  wire \loop_locked_max_reg[23]_i_1_n_0 ;
  wire \loop_locked_max_reg[31]_i_1_n_0 ;
  wire \loop_locked_max_reg[31]_i_2_n_0 ;
  wire \loop_locked_max_reg[7]_i_1_n_0 ;
  wire [31:0]loop_locked_min_reg;
  wire \loop_locked_min_reg[15]_i_1_n_0 ;
  wire \loop_locked_min_reg[23]_i_1_n_0 ;
  wire \loop_locked_min_reg[31]_i_1_n_0 ;
  wire \loop_locked_min_reg[31]_i_2_n_0 ;
  wire \loop_locked_min_reg[7]_i_1_n_0 ;
  wire [31:0]loop_output_reg;
  wire maxdex_reg;
  wire \maxdex_reg_reg_n_0_[0] ;
  wire \maxdex_reg_reg_n_0_[10] ;
  wire \maxdex_reg_reg_n_0_[11] ;
  wire \maxdex_reg_reg_n_0_[12] ;
  wire \maxdex_reg_reg_n_0_[15] ;
  wire \maxdex_reg_reg_n_0_[16] ;
  wire \maxdex_reg_reg_n_0_[17] ;
  wire \maxdex_reg_reg_n_0_[18] ;
  wire \maxdex_reg_reg_n_0_[19] ;
  wire \maxdex_reg_reg_n_0_[1] ;
  wire \maxdex_reg_reg_n_0_[20] ;
  wire \maxdex_reg_reg_n_0_[21] ;
  wire \maxdex_reg_reg_n_0_[22] ;
  wire \maxdex_reg_reg_n_0_[23] ;
  wire \maxdex_reg_reg_n_0_[24] ;
  wire \maxdex_reg_reg_n_0_[25] ;
  wire \maxdex_reg_reg_n_0_[26] ;
  wire \maxdex_reg_reg_n_0_[27] ;
  wire \maxdex_reg_reg_n_0_[28] ;
  wire \maxdex_reg_reg_n_0_[2] ;
  wire \maxdex_reg_reg_n_0_[31] ;
  wire \maxdex_reg_reg_n_0_[3] ;
  wire \maxdex_reg_reg_n_0_[4] ;
  wire \maxdex_reg_reg_n_0_[5] ;
  wire \maxdex_reg_reg_n_0_[6] ;
  wire \maxdex_reg_reg_n_0_[7] ;
  wire \maxdex_reg_reg_n_0_[8] ;
  wire \maxdex_reg_reg_n_0_[9] ;
  wire [31:0]mindex_reg;
  wire output_sel;
  wire [4:0]p_0_in;
  wire p_0_in0;
  wire p_0_in1_in;
  wire [31:31]p_0_in_0;
  wire p_7_in;
  wire [13:0]pid_loop_0_error;
  wire [0:0]pid_loop_0_input;
  wire [13:0]pid_loop_0_input_reg;
  wire pid_loop_0_n_0;
  wire pid_loop_0_n_35;
  wire [13:0]pid_loop_0_output;
  wire pid_loop_0_rst1;
  wire [12:0]pid_loop_input;
  wire ramp_enable;
  wire ramp_module_0_n_15;
  wire ramp_module_0_n_16;
  wire ramp_module_0_n_17;
  wire ramp_module_0_n_18;
  wire ramp_module_0_n_19;
  wire ramp_module_0_n_20;
  wire ramp_module_0_n_21;
  wire ramp_module_0_n_22;
  wire ramp_module_0_n_23;
  wire ramp_module_0_n_24;
  wire ramp_module_0_n_25;
  wire ramp_module_0_n_26;
  wire ramp_module_0_n_27;
  wire ramp_module_0_n_28;
  wire ramp_module_0_n_33;
  wire [13:0]ramp_module_0_output;
  wire [31:18]ramp_module_0_reset;
  wire ramp_module_0_rst;
  wire [31:0]ramp_offset_reg;
  wire \ramp_offset_reg[15]_i_1_n_0 ;
  wire \ramp_offset_reg[23]_i_1_n_0 ;
  wire \ramp_offset_reg[31]_i_1_n_0 ;
  wire \ramp_offset_reg[31]_i_2_n_0 ;
  wire \ramp_offset_reg[7]_i_1_n_0 ;
  wire [31:0]ramp_output_reg;
  wire [31:0]ramp_step_reg;
  wire \ramp_step_reg[15]_i_1_n_0 ;
  wire \ramp_step_reg[23]_i_1_n_0 ;
  wire \ramp_step_reg[31]_i_1_n_0 ;
  wire \ramp_step_reg[31]_i_2_n_0 ;
  wire \ramp_step_reg[7]_i_1_n_0 ;
  wire [31:0]ramplitude_reg;
  wire \ramplitude_reg[15]_i_1_n_0 ;
  wire \ramplitude_reg[23]_i_1_n_0 ;
  wire \ramplitude_reg[31]_i_1_n_0 ;
  wire \ramplitude_reg[31]_i_2_n_0 ;
  wire \ramplitude_reg[7]_i_1_n_0 ;
  wire [31:0]ramplitude_step_reg;
  wire \ramplitude_step_reg[15]_i_1_n_0 ;
  wire \ramplitude_step_reg[23]_i_1_n_0 ;
  wire \ramplitude_step_reg[31]_i_1_n_0 ;
  wire \ramplitude_step_reg[31]_i_2_n_0 ;
  wire \ramplitude_step_reg[7]_i_1_n_0 ;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire sel;
  wire [4:0]sel0;
  wire [13:0]setpoint_reg;
  wire \setpoint_reg[15]_i_1_n_0 ;
  wire \setpoint_reg[23]_i_1_n_0 ;
  wire \setpoint_reg[31]_i_1_n_0 ;
  wire \setpoint_reg[31]_i_2_n_0 ;
  wire \setpoint_reg[7]_i_1_n_0 ;
  wire [31:14]setpoint_reg__0;
  wire slv_reg_rden;
  wire [11:0]status_reg;
  wire [3:3]\NLW_autolock_input_max_reg[13]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_autolock_input_max_reg[13]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_autolock_input_max_reg[13]_i_3_O_UNCONNECTED ;
  wire [3:3]\NLW_autolock_input_min_reg[13]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_autolock_input_min_reg[13]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_autolock_input_min_reg[13]_i_3_O_UNCONNECTED ;
  wire [3:0]NLW_input_reset_reg_reg_i_14_O_UNCONNECTED;
  wire [3:0]NLW_input_reset_reg_reg_i_2_O_UNCONNECTED;
  wire [3:0]NLW_input_reset_reg_reg_i_23_O_UNCONNECTED;
  wire [3:0]NLW_input_reset_reg_reg_i_3_O_UNCONNECTED;
  wire [3:0]NLW_input_reset_reg_reg_i_32_O_UNCONNECTED;
  wire [3:0]NLW_input_reset_reg_reg_i_41_O_UNCONNECTED;
  wire [3:0]NLW_input_reset_reg_reg_i_5_O_UNCONNECTED;
  wire [3:0]NLW_input_reset_reg_reg_i_50_O_UNCONNECTED;
  wire [3:0]\NLW_loop_locked_counter_reg_reg[0]_i_16_O_UNCONNECTED ;
  wire [3:0]\NLW_loop_locked_counter_reg_reg[0]_i_25_O_UNCONNECTED ;
  wire [3:0]\NLW_loop_locked_counter_reg_reg[0]_i_34_O_UNCONNECTED ;
  wire [3:0]\NLW_loop_locked_counter_reg_reg[0]_i_4_O_UNCONNECTED ;
  wire [3:0]\NLW_loop_locked_counter_reg_reg[0]_i_43_O_UNCONNECTED ;
  wire [3:0]\NLW_loop_locked_counter_reg_reg[0]_i_5_O_UNCONNECTED ;
  wire [3:0]\NLW_loop_locked_counter_reg_reg[0]_i_52_O_UNCONNECTED ;
  wire [3:0]\NLW_loop_locked_counter_reg_reg[0]_i_7_O_UNCONNECTED ;
  wire [3:3]\NLW_loop_locked_counter_reg_reg[28]_i_1_CO_UNCONNECTED ;

  FDRE \I_mon_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[0]),
        .Q(I_mon_reg[0]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[10]),
        .Q(I_mon_reg[10]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[11]),
        .Q(I_mon_reg[11]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[12]),
        .Q(I_mon_reg[12]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[13]),
        .Q(I_mon_reg[13]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[14]),
        .Q(I_mon_reg[14]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[15]),
        .Q(I_mon_reg[15]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[16]),
        .Q(I_mon_reg[16]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[17]),
        .Q(I_mon_reg[17]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[18]),
        .Q(I_mon_reg[18]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[19]),
        .Q(I_mon_reg[19]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[1]),
        .Q(I_mon_reg[1]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[20]),
        .Q(I_mon_reg[20]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[21]),
        .Q(I_mon_reg[21]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[22]),
        .Q(I_mon_reg[22]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[23]),
        .Q(I_mon_reg[23]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[24]),
        .Q(I_mon_reg[24]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[25]),
        .Q(I_mon_reg[25]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[26]),
        .Q(I_mon_reg[26]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[27]),
        .Q(I_mon_reg[27]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[28]),
        .Q(I_mon_reg[28]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[29]),
        .Q(I_mon_reg[29]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[2]),
        .Q(I_mon_reg[2]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[30]),
        .Q(I_mon_reg[30]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[31]),
        .Q(I_mon_reg[31]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[3]),
        .Q(I_mon_reg[3]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[4]),
        .Q(I_mon_reg[4]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[5]),
        .Q(I_mon_reg[5]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[6]),
        .Q(I_mon_reg[6]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[7]),
        .Q(I_mon_reg[7]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[8]),
        .Q(I_mon_reg[8]),
        .R(1'b0));
  FDRE \I_mon_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_term_mon[9]),
        .Q(I_mon_reg[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \I_reg[15]_i_1 
       (.I0(\I_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\I_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \I_reg[23]_i_1 
       (.I0(\I_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\I_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \I_reg[31]_i_1 
       (.I0(\I_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\I_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000200000)) 
    \I_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\I_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \I_reg[7]_i_1 
       (.I0(\I_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\I_reg[7]_i_1_n_0 ));
  FDRE \I_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(I_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(I_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(I_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(I_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(I_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(I_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(I_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(I_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(I_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(I_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(I_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(I_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(I_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(I_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(I_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(I_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(I_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(I_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(I_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(I_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(I_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(I_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(I_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(I_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(I_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(I_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(I_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(I_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(I_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(I_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(I_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \I_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\I_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(I_reg[9]),
        .R(pid_loop_0_rst1));
  FDRE \P_mon_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[0]),
        .Q(P_mon_reg[0]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[10]),
        .Q(P_mon_reg[10]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[11]),
        .Q(P_mon_reg[11]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[12]),
        .Q(P_mon_reg[12]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[13]),
        .Q(P_mon_reg[13]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[14]),
        .Q(P_mon_reg[14]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[15]),
        .Q(P_mon_reg[15]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[16]),
        .Q(P_mon_reg[16]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[17]),
        .Q(P_mon_reg[17]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[18]),
        .Q(P_mon_reg[18]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[19]),
        .Q(P_mon_reg[19]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[1]),
        .Q(P_mon_reg[1]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[20]),
        .Q(P_mon_reg[20]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[21]),
        .Q(P_mon_reg[21]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[22]),
        .Q(P_mon_reg[22]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[23]),
        .Q(P_mon_reg[23]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[24]),
        .Q(P_mon_reg[24]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[25]),
        .Q(P_mon_reg[25]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[26]),
        .Q(P_mon_reg[26]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[27]),
        .Q(P_mon_reg[27]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[28]),
        .Q(P_mon_reg[28]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[29]),
        .Q(P_mon_reg[29]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[2]),
        .Q(P_mon_reg[2]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[30]),
        .Q(P_mon_reg[30]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[31]),
        .Q(P_mon_reg[31]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[3]),
        .Q(P_mon_reg[3]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[4]),
        .Q(P_mon_reg[4]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[5]),
        .Q(P_mon_reg[5]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[6]),
        .Q(P_mon_reg[6]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[7]),
        .Q(P_mon_reg[7]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[8]),
        .Q(P_mon_reg[8]),
        .R(1'b0));
  FDRE \P_mon_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_term_mon[9]),
        .Q(P_mon_reg[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \P_reg[15]_i_1 
       (.I0(\P_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\P_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \P_reg[23]_i_1 
       (.I0(\P_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\P_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \P_reg[31]_i_1 
       (.I0(\P_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\P_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000100000)) 
    \P_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\P_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \P_reg[7]_i_1 
       (.I0(\P_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\P_reg[7]_i_1_n_0 ));
  FDRE \P_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(P_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(P_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(P_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(P_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(P_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(P_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(P_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(P_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(P_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(P_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(P_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(P_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(P_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(P_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(P_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(P_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(P_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(P_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(P_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(P_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(P_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(P_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(P_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(P_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(P_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(P_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(P_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(P_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(P_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(P_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(P_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \P_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\P_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(P_reg[9]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_0_input_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[0]),
        .Q(autolock_0_input[0]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[10]),
        .Q(autolock_0_input[10]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[11]),
        .Q(autolock_0_input[11]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[12]),
        .Q(autolock_0_input[12]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[13]),
        .Q(autolock_0_input[13]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[1]),
        .Q(autolock_0_input[1]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[2]),
        .Q(autolock_0_input[2]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[3]),
        .Q(autolock_0_input[3]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[4]),
        .Q(autolock_0_input[4]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[5]),
        .Q(autolock_0_input[5]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[6]),
        .Q(autolock_0_input[6]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[7]),
        .Q(autolock_0_input[7]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[8]),
        .Q(autolock_0_input[8]),
        .R(1'b0));
  FDRE \autolock_0_input_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[9]),
        .Q(autolock_0_input[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_max[13]_i_10 
       (.I0(autolock_0_input[6]),
        .I1(autolock_input_max[6]),
        .I2(autolock_input_max[7]),
        .I3(autolock_0_input[7]),
        .O(\autolock_input_max[13]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_max[13]_i_11 
       (.I0(autolock_0_input[4]),
        .I1(autolock_input_max[4]),
        .I2(autolock_input_max[5]),
        .I3(autolock_0_input[5]),
        .O(\autolock_input_max[13]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_max[13]_i_12 
       (.I0(autolock_0_input[2]),
        .I1(autolock_input_max[2]),
        .I2(autolock_input_max[3]),
        .I3(autolock_0_input[3]),
        .O(\autolock_input_max[13]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_max[13]_i_13 
       (.I0(autolock_0_input[0]),
        .I1(autolock_input_max[0]),
        .I2(autolock_input_max[1]),
        .I3(autolock_0_input[1]),
        .O(\autolock_input_max[13]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_max[13]_i_14 
       (.I0(autolock_0_input[6]),
        .I1(autolock_input_max[6]),
        .I2(autolock_0_input[7]),
        .I3(autolock_input_max[7]),
        .O(\autolock_input_max[13]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_max[13]_i_15 
       (.I0(autolock_0_input[4]),
        .I1(autolock_input_max[4]),
        .I2(autolock_0_input[5]),
        .I3(autolock_input_max[5]),
        .O(\autolock_input_max[13]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_max[13]_i_16 
       (.I0(autolock_0_input[2]),
        .I1(autolock_input_max[2]),
        .I2(autolock_0_input[3]),
        .I3(autolock_input_max[3]),
        .O(\autolock_input_max[13]_i_16_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_max[13]_i_17 
       (.I0(autolock_0_input[0]),
        .I1(autolock_input_max[0]),
        .I2(autolock_0_input[1]),
        .I3(autolock_input_max[1]),
        .O(\autolock_input_max[13]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_max[13]_i_4 
       (.I0(autolock_0_input[12]),
        .I1(autolock_input_max[12]),
        .I2(autolock_0_input[13]),
        .I3(autolock_input_max[13]),
        .O(\autolock_input_max[13]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_max[13]_i_5 
       (.I0(autolock_0_input[10]),
        .I1(autolock_input_max[10]),
        .I2(autolock_input_max[11]),
        .I3(autolock_0_input[11]),
        .O(\autolock_input_max[13]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_max[13]_i_6 
       (.I0(autolock_0_input[8]),
        .I1(autolock_input_max[8]),
        .I2(autolock_input_max[9]),
        .I3(autolock_0_input[9]),
        .O(\autolock_input_max[13]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_max[13]_i_7 
       (.I0(autolock_0_input[12]),
        .I1(autolock_input_max[12]),
        .I2(autolock_input_max[13]),
        .I3(autolock_0_input[13]),
        .O(\autolock_input_max[13]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_max[13]_i_8 
       (.I0(autolock_0_input[10]),
        .I1(autolock_input_max[10]),
        .I2(autolock_0_input[11]),
        .I3(autolock_input_max[11]),
        .O(\autolock_input_max[13]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_max[13]_i_9 
       (.I0(autolock_0_input[8]),
        .I1(autolock_input_max[8]),
        .I2(autolock_0_input[9]),
        .I3(autolock_input_max[9]),
        .O(\autolock_input_max[13]_i_9_n_0 ));
  FDRE \autolock_input_max_reg[0] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[0]),
        .Q(autolock_input_max[0]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[10] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[10]),
        .Q(autolock_input_max[10]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[11] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[11]),
        .Q(autolock_input_max[11]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[12] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[12]),
        .Q(autolock_input_max[12]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[13] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[13]),
        .Q(autolock_input_max[13]),
        .R(1'b0));
  CARRY4 \autolock_input_max_reg[13]_i_2 
       (.CI(\autolock_input_max_reg[13]_i_3_n_0 ),
        .CO({\NLW_autolock_input_max_reg[13]_i_2_CO_UNCONNECTED [3],\autolock_input_max_reg[13]_i_2_n_1 ,\autolock_input_max_reg[13]_i_2_n_2 ,\autolock_input_max_reg[13]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\autolock_input_max[13]_i_4_n_0 ,\autolock_input_max[13]_i_5_n_0 ,\autolock_input_max[13]_i_6_n_0 }),
        .O(\NLW_autolock_input_max_reg[13]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,\autolock_input_max[13]_i_7_n_0 ,\autolock_input_max[13]_i_8_n_0 ,\autolock_input_max[13]_i_9_n_0 }));
  CARRY4 \autolock_input_max_reg[13]_i_3 
       (.CI(1'b0),
        .CO({\autolock_input_max_reg[13]_i_3_n_0 ,\autolock_input_max_reg[13]_i_3_n_1 ,\autolock_input_max_reg[13]_i_3_n_2 ,\autolock_input_max_reg[13]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({\autolock_input_max[13]_i_10_n_0 ,\autolock_input_max[13]_i_11_n_0 ,\autolock_input_max[13]_i_12_n_0 ,\autolock_input_max[13]_i_13_n_0 }),
        .O(\NLW_autolock_input_max_reg[13]_i_3_O_UNCONNECTED [3:0]),
        .S({\autolock_input_max[13]_i_14_n_0 ,\autolock_input_max[13]_i_15_n_0 ,\autolock_input_max[13]_i_16_n_0 ,\autolock_input_max[13]_i_17_n_0 }));
  FDRE \autolock_input_max_reg[1] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[1]),
        .Q(autolock_input_max[1]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[2] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[2]),
        .Q(autolock_input_max[2]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[3] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[3]),
        .Q(autolock_input_max[3]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[4] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[4]),
        .Q(autolock_input_max[4]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[5] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[5]),
        .Q(autolock_input_max[5]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[6] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[6]),
        .Q(autolock_input_max[6]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[7] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[7]),
        .Q(autolock_input_max[7]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[8] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[8]),
        .Q(autolock_input_max[8]),
        .R(1'b0));
  FDRE \autolock_input_max_reg[9] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_0_input[9]),
        .Q(autolock_input_max[9]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[0] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[0]),
        .Q(autolock_input_maxdex[0]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[10] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[10]),
        .Q(autolock_input_maxdex[10]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[11] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[11]),
        .Q(autolock_input_maxdex[11]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[12] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[12]),
        .Q(autolock_input_maxdex[12]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[13] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[13]),
        .Q(autolock_input_maxdex[13]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[1] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[1]),
        .Q(autolock_input_maxdex[1]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[2] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[2]),
        .Q(autolock_input_maxdex[2]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[3] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[3]),
        .Q(autolock_input_maxdex[3]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[4] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[4]),
        .Q(autolock_input_maxdex[4]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[5] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[5]),
        .Q(autolock_input_maxdex[5]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[6] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[6]),
        .Q(autolock_input_maxdex[6]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[7] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[7]),
        .Q(autolock_input_maxdex[7]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[8] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[8]),
        .Q(autolock_input_maxdex[8]),
        .R(1'b0));
  FDRE \autolock_input_maxdex_reg[9] 
       (.C(s00_axi_aclk),
        .CE(ramp_module_0_n_33),
        .D(autolock_input_maxdex0_in[9]),
        .Q(autolock_input_maxdex[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_min[13]_i_10 
       (.I0(autolock_input_min[6]),
        .I1(autolock_0_input[6]),
        .I2(autolock_0_input[7]),
        .I3(autolock_input_min[7]),
        .O(\autolock_input_min[13]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_min[13]_i_11 
       (.I0(autolock_input_min[4]),
        .I1(autolock_0_input[4]),
        .I2(autolock_0_input[5]),
        .I3(autolock_input_min[5]),
        .O(\autolock_input_min[13]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_min[13]_i_12 
       (.I0(autolock_input_min[2]),
        .I1(autolock_0_input[2]),
        .I2(autolock_0_input[3]),
        .I3(autolock_input_min[3]),
        .O(\autolock_input_min[13]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_min[13]_i_13 
       (.I0(autolock_input_min[0]),
        .I1(autolock_0_input[0]),
        .I2(autolock_0_input[1]),
        .I3(autolock_input_min[1]),
        .O(\autolock_input_min[13]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_min[13]_i_14 
       (.I0(autolock_input_min[6]),
        .I1(autolock_0_input[6]),
        .I2(autolock_input_min[7]),
        .I3(autolock_0_input[7]),
        .O(\autolock_input_min[13]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_min[13]_i_15 
       (.I0(autolock_input_min[4]),
        .I1(autolock_0_input[4]),
        .I2(autolock_input_min[5]),
        .I3(autolock_0_input[5]),
        .O(\autolock_input_min[13]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_min[13]_i_16 
       (.I0(autolock_input_min[2]),
        .I1(autolock_0_input[2]),
        .I2(autolock_input_min[3]),
        .I3(autolock_0_input[3]),
        .O(\autolock_input_min[13]_i_16_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_min[13]_i_17 
       (.I0(autolock_input_min[0]),
        .I1(autolock_0_input[0]),
        .I2(autolock_input_min[1]),
        .I3(autolock_0_input[1]),
        .O(\autolock_input_min[13]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_min[13]_i_4 
       (.I0(autolock_input_min[12]),
        .I1(autolock_0_input[12]),
        .I2(autolock_input_min[13]),
        .I3(autolock_0_input[13]),
        .O(\autolock_input_min[13]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_min[13]_i_5 
       (.I0(autolock_input_min[10]),
        .I1(autolock_0_input[10]),
        .I2(autolock_0_input[11]),
        .I3(autolock_input_min[11]),
        .O(\autolock_input_min[13]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \autolock_input_min[13]_i_6 
       (.I0(autolock_input_min[8]),
        .I1(autolock_0_input[8]),
        .I2(autolock_0_input[9]),
        .I3(autolock_input_min[9]),
        .O(\autolock_input_min[13]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_min[13]_i_7 
       (.I0(autolock_input_min[12]),
        .I1(autolock_0_input[12]),
        .I2(autolock_0_input[13]),
        .I3(autolock_input_min[13]),
        .O(\autolock_input_min[13]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_min[13]_i_8 
       (.I0(autolock_input_min[10]),
        .I1(autolock_0_input[10]),
        .I2(autolock_input_min[11]),
        .I3(autolock_0_input[11]),
        .O(\autolock_input_min[13]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \autolock_input_min[13]_i_9 
       (.I0(autolock_input_min[8]),
        .I1(autolock_0_input[8]),
        .I2(autolock_input_min[9]),
        .I3(autolock_0_input[9]),
        .O(\autolock_input_min[13]_i_9_n_0 ));
  FDRE \autolock_input_min_reg[0] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[0]),
        .Q(autolock_input_min[0]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[10] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[10]),
        .Q(autolock_input_min[10]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[11] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[11]),
        .Q(autolock_input_min[11]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[12] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[12]),
        .Q(autolock_input_min[12]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[13] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[13]),
        .Q(autolock_input_min[13]),
        .R(1'b0));
  CARRY4 \autolock_input_min_reg[13]_i_2 
       (.CI(\autolock_input_min_reg[13]_i_3_n_0 ),
        .CO({\NLW_autolock_input_min_reg[13]_i_2_CO_UNCONNECTED [3],\autolock_input_min_reg[13]_i_2_n_1 ,\autolock_input_min_reg[13]_i_2_n_2 ,\autolock_input_min_reg[13]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\autolock_input_min[13]_i_4_n_0 ,\autolock_input_min[13]_i_5_n_0 ,\autolock_input_min[13]_i_6_n_0 }),
        .O(\NLW_autolock_input_min_reg[13]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,\autolock_input_min[13]_i_7_n_0 ,\autolock_input_min[13]_i_8_n_0 ,\autolock_input_min[13]_i_9_n_0 }));
  CARRY4 \autolock_input_min_reg[13]_i_3 
       (.CI(1'b0),
        .CO({\autolock_input_min_reg[13]_i_3_n_0 ,\autolock_input_min_reg[13]_i_3_n_1 ,\autolock_input_min_reg[13]_i_3_n_2 ,\autolock_input_min_reg[13]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({\autolock_input_min[13]_i_10_n_0 ,\autolock_input_min[13]_i_11_n_0 ,\autolock_input_min[13]_i_12_n_0 ,\autolock_input_min[13]_i_13_n_0 }),
        .O(\NLW_autolock_input_min_reg[13]_i_3_O_UNCONNECTED [3:0]),
        .S({\autolock_input_min[13]_i_14_n_0 ,\autolock_input_min[13]_i_15_n_0 ,\autolock_input_min[13]_i_16_n_0 ,\autolock_input_min[13]_i_17_n_0 }));
  FDRE \autolock_input_min_reg[1] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[1]),
        .Q(autolock_input_min[1]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[2] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[2]),
        .Q(autolock_input_min[2]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[3] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[3]),
        .Q(autolock_input_min[3]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[4] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[4]),
        .Q(autolock_input_min[4]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[5] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[5]),
        .Q(autolock_input_min[5]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[6] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[6]),
        .Q(autolock_input_min[6]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[7] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[7]),
        .Q(autolock_input_min[7]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[8] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[8]),
        .Q(autolock_input_min[8]),
        .R(1'b0));
  FDRE \autolock_input_min_reg[9] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_0_input[9]),
        .Q(autolock_input_min[9]),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[0] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[0]),
        .Q(\autolock_input_mindex_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[10] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[10]),
        .Q(\autolock_input_mindex_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[11] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[11]),
        .Q(\autolock_input_mindex_reg_n_0_[11] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[12] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[12]),
        .Q(\autolock_input_mindex_reg_n_0_[12] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[13] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[13]),
        .Q(p_0_in0),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[1] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[1]),
        .Q(\autolock_input_mindex_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[2] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[2]),
        .Q(\autolock_input_mindex_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[3] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[3]),
        .Q(\autolock_input_mindex_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[4] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[4]),
        .Q(\autolock_input_mindex_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[5] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[5]),
        .Q(\autolock_input_mindex_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[6] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[6]),
        .Q(\autolock_input_mindex_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[7] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[7]),
        .Q(\autolock_input_mindex_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[8] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[8]),
        .Q(\autolock_input_mindex_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \autolock_input_mindex_reg[9] 
       (.C(s00_axi_aclk),
        .CE(autolock_input_mindex),
        .D(autolock_input_maxdex0_in[9]),
        .Q(\autolock_input_mindex_reg_n_0_[9] ),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[0]),
        .Q(autolock_input_reg[0]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[10]),
        .Q(autolock_input_reg[10]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[11]),
        .Q(autolock_input_reg[11]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[12]),
        .Q(autolock_input_reg[12]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[13]),
        .Q(autolock_input_reg[13]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[1]),
        .Q(autolock_input_reg[1]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[2]),
        .Q(autolock_input_reg[2]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[3]),
        .Q(autolock_input_reg[3]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[4]),
        .Q(autolock_input_reg[4]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[5]),
        .Q(autolock_input_reg[5]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[6]),
        .Q(autolock_input_reg[6]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[7]),
        .Q(autolock_input_reg[7]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[8]),
        .Q(autolock_input_reg[8]),
        .R(1'b0));
  FDRE \autolock_input_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock_0_input[9]),
        .Q(autolock_input_reg[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \autolock_max_reg[15]_i_1 
       (.I0(\autolock_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\autolock_max_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \autolock_max_reg[23]_i_1 
       (.I0(\autolock_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\autolock_max_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \autolock_max_reg[31]_i_1 
       (.I0(\autolock_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\autolock_max_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \autolock_max_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\autolock_max_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \autolock_max_reg[7]_i_1 
       (.I0(\autolock_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\autolock_max_reg[7]_i_1_n_0 ));
  FDRE \autolock_max_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(autolock_max_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(autolock_max_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(autolock_max_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(autolock_max_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(autolock_max_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(autolock_max_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(autolock_max_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(autolock_max_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(autolock_max_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(autolock_max_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(autolock_max_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(autolock_max_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(autolock_max_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(autolock_max_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(autolock_max_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(autolock_max_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(autolock_max_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(autolock_max_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(autolock_max_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(autolock_max_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(autolock_max_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(autolock_max_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(autolock_max_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(autolock_max_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(autolock_max_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(autolock_max_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(autolock_max_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(autolock_max_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(autolock_max_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(autolock_max_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(autolock_max_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_max_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\autolock_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(autolock_max_reg[9]),
        .R(pid_loop_0_rst1));
  LUT2 #(
    .INIT(4'h8)) 
    \autolock_min_reg[15]_i_1 
       (.I0(\autolock_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\autolock_min_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \autolock_min_reg[23]_i_1 
       (.I0(\autolock_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\autolock_min_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \autolock_min_reg[31]_i_1 
       (.I0(\autolock_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\autolock_min_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000200000000000)) 
    \autolock_min_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\autolock_min_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \autolock_min_reg[7]_i_1 
       (.I0(\autolock_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\autolock_min_reg[7]_i_1_n_0 ));
  FDRE \autolock_min_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(autolock_min_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(autolock_min_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(autolock_min_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(autolock_min_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(autolock_min_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(autolock_min_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(autolock_min_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(autolock_min_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(autolock_min_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(autolock_min_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(autolock_min_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(autolock_min_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(autolock_min_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(autolock_min_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(autolock_min_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(autolock_min_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(autolock_min_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(autolock_min_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(autolock_min_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(autolock_min_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(autolock_min_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(autolock_min_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(autolock_min_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(autolock_min_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(autolock_min_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(autolock_min_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(autolock_min_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(autolock_min_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(autolock_min_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(autolock_min_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(autolock_min_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \autolock_min_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\autolock_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(autolock_min_reg[9]),
        .R(pid_loop_0_rst1));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_reg_1),
        .Q(aw_en_reg_0),
        .S(pid_loop_0_rst1));
  (* ORIG_CELL_NAME = "axi_araddr_reg[2]" *) 
  FDRE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .R(pid_loop_0_rst1));
  (* ORIG_CELL_NAME = "axi_araddr_reg[2]" *) 
  FDRE \axi_araddr_reg[2]_rep 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(\axi_araddr_reg[2]_rep_n_0 ),
        .R(pid_loop_0_rst1));
  (* ORIG_CELL_NAME = "axi_araddr_reg[3]" *) 
  FDRE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .R(pid_loop_0_rst1));
  (* ORIG_CELL_NAME = "axi_araddr_reg[3]" *) 
  FDRE \axi_araddr_reg[3]_rep 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(\axi_araddr_reg[3]_rep_n_0 ),
        .R(pid_loop_0_rst1));
  FDRE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .R(pid_loop_0_rst1));
  FDRE \axi_araddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[3]),
        .Q(sel0[3]),
        .R(pid_loop_0_rst1));
  FDRE \axi_araddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[4]),
        .Q(sel0[4]),
        .R(pid_loop_0_rst1));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(axi_arready_reg_0),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(axi_arready_reg_0),
        .R(pid_loop_0_rst1));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(pid_loop_0_rst1));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(pid_loop_0_rst1));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(pid_loop_0_rst1));
  FDRE \axi_awaddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[3]),
        .Q(p_0_in[3]),
        .R(pid_loop_0_rst1));
  FDRE \axi_awaddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[4]),
        .Q(p_0_in[4]),
        .R(pid_loop_0_rst1));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(pid_loop_0_rst1));
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(axi_awready_reg_0),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(axi_awready_reg_0),
        .R(pid_loop_0_rst1));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_reg_0),
        .Q(s00_axi_bvalid),
        .R(pid_loop_0_rst1));
  LUT2 #(
    .INIT(4'h7)) 
    \axi_pi_output_reg[13]_inv_i_1 
       (.I0(output_sel),
        .I1(\control_reg_reg_n_0_[0] ),
        .O(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_27),
        .Q(axi_pi_output[0]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_17),
        .Q(axi_pi_output[10]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_16),
        .Q(axi_pi_output[11]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_15),
        .Q(axi_pi_output[12]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDSE \axi_pi_output_reg_reg[13]_inv 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_28),
        .Q(axi_pi_output[13]),
        .S(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_26),
        .Q(axi_pi_output[1]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_25),
        .Q(axi_pi_output[2]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_24),
        .Q(axi_pi_output[3]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_23),
        .Q(axi_pi_output[4]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_22),
        .Q(axi_pi_output[5]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_21),
        .Q(axi_pi_output[6]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_20),
        .Q(axi_pi_output[7]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_19),
        .Q(axi_pi_output[8]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  FDRE \axi_pi_output_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_n_18),
        .Q(axi_pi_output[9]),
        .R(\axi_pi_output_reg[13]_inv_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(\axi_rdata[0]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[0]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[0]_i_5_n_0 ),
        .O(reg_data_out[0]));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[0]_i_10 
       (.I0(setpoint_reg[0]),
        .I1(status_reg[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\control_reg_reg_n_0_[0] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_11 
       (.I0(I_mon_reg[0]),
        .I1(I_reg[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[0]),
        .O(\axi_rdata[0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[0]_i_2 
       (.I0(\axi_rdata[0]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[0]_i_3 
       (.I0(\axi_rdata[0]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[0] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[0]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_6 
       (.I0(ramp_offset_reg[0]),
        .I1(ramplitude_step_reg[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[0]),
        .O(\axi_rdata[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[0]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[0]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[0]),
        .I5(pid_loop_0_input_reg[0]),
        .O(\axi_rdata[0]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[0]_i_8 
       (.I0(autolock_min_reg[0]),
        .I1(ramp_step_reg[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[0]),
        .O(\axi_rdata[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_9 
       (.I0(loop_locked_delay_reg[0]),
        .I1(loop_locked_max_reg[0]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[0]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[0]),
        .O(\axi_rdata[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(\axi_rdata[10]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[10]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[10]_i_5_n_0 ),
        .O(reg_data_out[10]));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[10]_i_10 
       (.I0(setpoint_reg[10]),
        .I1(status_reg[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\control_reg_reg_n_0_[10] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[10]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_11 
       (.I0(I_mon_reg[10]),
        .I1(I_reg[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[10]),
        .O(\axi_rdata[10]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[10]_i_2 
       (.I0(\axi_rdata[10]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[10]_i_3 
       (.I0(\axi_rdata[10]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[10] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[10]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_6 
       (.I0(ramp_offset_reg[10]),
        .I1(ramplitude_step_reg[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[10]),
        .O(\axi_rdata[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[10]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[10]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[10]),
        .I5(pid_loop_0_input_reg[10]),
        .O(\axi_rdata[10]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[10]_i_8 
       (.I0(autolock_min_reg[10]),
        .I1(ramp_step_reg[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[10]),
        .O(\axi_rdata[10]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_9 
       (.I0(loop_locked_delay_reg[10]),
        .I1(loop_locked_max_reg[10]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[10]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[10]),
        .O(\axi_rdata[10]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(\axi_rdata[11]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[11]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[11]_i_5_n_0 ),
        .O(reg_data_out[11]));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[11]_i_10 
       (.I0(setpoint_reg[11]),
        .I1(status_reg[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\control_reg_reg_n_0_[11] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[11]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_11 
       (.I0(I_mon_reg[11]),
        .I1(I_reg[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[11]),
        .O(\axi_rdata[11]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[11]_i_2 
       (.I0(\axi_rdata[11]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[11]_i_3 
       (.I0(\axi_rdata[11]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[11] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[11]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_6 
       (.I0(ramp_offset_reg[11]),
        .I1(ramplitude_step_reg[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[11]),
        .O(\axi_rdata[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[11]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[11]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[11]),
        .I5(pid_loop_0_input_reg[11]),
        .O(\axi_rdata[11]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[11]_i_8 
       (.I0(autolock_min_reg[11]),
        .I1(ramp_step_reg[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[11]),
        .O(\axi_rdata[11]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_9 
       (.I0(loop_locked_delay_reg[11]),
        .I1(loop_locked_max_reg[11]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[11]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[11]),
        .O(\axi_rdata[11]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(\axi_rdata[12]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[12]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[12]_i_5_n_0 ),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_10 
       (.I0(I_mon_reg[12]),
        .I1(I_reg[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[12]),
        .O(\axi_rdata[12]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[12]_i_2 
       (.I0(\axi_rdata[12]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[12]_i_3 
       (.I0(\axi_rdata[12]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[12] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[12]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[12]_i_5 
       (.I0(\axi_rdata[12]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg[12]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(\control_reg_reg_n_0_[12] ),
        .I5(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_6 
       (.I0(ramp_offset_reg[12]),
        .I1(ramplitude_step_reg[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[12]),
        .O(\axi_rdata[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[12]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[12]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[12]),
        .I5(pid_loop_0_input_reg[12]),
        .O(\axi_rdata[12]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[12]_i_8 
       (.I0(autolock_min_reg[12]),
        .I1(ramp_step_reg[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[12]),
        .O(\axi_rdata[12]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_9 
       (.I0(loop_locked_delay_reg[12]),
        .I1(loop_locked_max_reg[12]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[12]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[12]),
        .O(\axi_rdata[12]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(\axi_rdata[15]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[13]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[13]_i_4_n_0 ),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[13]_i_2 
       (.I0(\axi_rdata[13]_i_5_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[13]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[13]_i_4 
       (.I0(\axi_rdata[13]_i_8_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg[13]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[13] ),
        .I5(sel0[0]),
        .O(\axi_rdata[13]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_5 
       (.I0(ramp_offset_reg[13]),
        .I1(ramplitude_step_reg[13]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[13]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[13]_i_6 
       (.I0(autolock_min_reg[13]),
        .I1(ramp_step_reg[13]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[13]),
        .O(\axi_rdata[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_7 
       (.I0(loop_locked_delay_reg[13]),
        .I1(loop_locked_max_reg[13]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[13]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[13]),
        .O(\axi_rdata[13]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_8 
       (.I0(I_mon_reg[13]),
        .I1(I_reg[13]),
        .I2(sel0[1]),
        .I3(P_mon_reg[13]),
        .I4(sel0[0]),
        .I5(P_reg[13]),
        .O(\axi_rdata[13]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(\axi_rdata[15]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[14]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[14]_i_4_n_0 ),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[14]_i_2 
       (.I0(\axi_rdata[14]_i_5_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[14]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[14]_i_4 
       (.I0(\axi_rdata[14]_i_8_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[14]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[14] ),
        .I5(sel0[0]),
        .O(\axi_rdata[14]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_5 
       (.I0(ramp_offset_reg[14]),
        .I1(ramplitude_step_reg[14]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[14]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[14]_i_6 
       (.I0(autolock_min_reg[14]),
        .I1(ramp_step_reg[14]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[14]),
        .O(\axi_rdata[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_7 
       (.I0(loop_locked_delay_reg[14]),
        .I1(loop_locked_max_reg[14]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[14]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[14]),
        .O(\axi_rdata[14]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_8 
       (.I0(I_mon_reg[14]),
        .I1(I_reg[14]),
        .I2(sel0[1]),
        .I3(P_mon_reg[14]),
        .I4(sel0[0]),
        .I5(P_reg[14]),
        .O(\axi_rdata[14]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata[15]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[15]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[15]_i_5_n_0 ),
        .O(reg_data_out[15]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[15]_i_2 
       (.I0(\axi_rdata[15]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[15]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[15]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[15] ),
        .I4(sel0[0]),
        .I5(mindex_reg[15]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[15]_i_5 
       (.I0(\axi_rdata[15]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[15]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[15] ),
        .I5(sel0[0]),
        .O(\axi_rdata[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_6 
       (.I0(ramp_offset_reg[15]),
        .I1(ramplitude_step_reg[15]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[15]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[15]_i_7 
       (.I0(autolock_min_reg[15]),
        .I1(ramp_step_reg[15]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[15]),
        .O(\axi_rdata[15]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_8 
       (.I0(loop_locked_delay_reg[15]),
        .I1(loop_locked_max_reg[15]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[15]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[15]),
        .O(\axi_rdata[15]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_9 
       (.I0(I_mon_reg[15]),
        .I1(I_reg[15]),
        .I2(sel0[1]),
        .I3(P_mon_reg[15]),
        .I4(sel0[0]),
        .I5(P_reg[15]),
        .O(\axi_rdata[15]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(\axi_rdata[16]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[16]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[16]_i_5_n_0 ),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[16]_i_2 
       (.I0(\axi_rdata[16]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[16]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[16]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[16] ),
        .I4(sel0[0]),
        .I5(mindex_reg[16]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[16]_i_5 
       (.I0(\axi_rdata[16]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[16]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[16] ),
        .I5(sel0[0]),
        .O(\axi_rdata[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_6 
       (.I0(ramp_offset_reg[16]),
        .I1(ramplitude_step_reg[16]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[16]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[16]_i_7 
       (.I0(autolock_min_reg[16]),
        .I1(ramp_step_reg[16]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[16]),
        .O(\axi_rdata[16]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_8 
       (.I0(loop_locked_delay_reg[16]),
        .I1(loop_locked_max_reg[16]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[16]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[16]),
        .O(\axi_rdata[16]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_9 
       (.I0(I_mon_reg[16]),
        .I1(I_reg[16]),
        .I2(sel0[1]),
        .I3(P_mon_reg[16]),
        .I4(sel0[0]),
        .I5(P_reg[16]),
        .O(\axi_rdata[16]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(\axi_rdata[17]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[17]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[17]_i_5_n_0 ),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[17]_i_2 
       (.I0(\axi_rdata[17]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[17]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[17]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[17] ),
        .I4(sel0[0]),
        .I5(mindex_reg[17]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[17]_i_5 
       (.I0(\axi_rdata[17]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[17]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[17] ),
        .I5(sel0[0]),
        .O(\axi_rdata[17]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_6 
       (.I0(ramp_offset_reg[17]),
        .I1(ramplitude_step_reg[17]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[17]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[17]_i_7 
       (.I0(autolock_min_reg[17]),
        .I1(ramp_step_reg[17]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[17]),
        .O(\axi_rdata[17]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_8 
       (.I0(loop_locked_delay_reg[17]),
        .I1(loop_locked_max_reg[17]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[17]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[17]),
        .O(\axi_rdata[17]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_9 
       (.I0(I_mon_reg[17]),
        .I1(I_reg[17]),
        .I2(sel0[1]),
        .I3(P_mon_reg[17]),
        .I4(sel0[0]),
        .I5(P_reg[17]),
        .O(\axi_rdata[17]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(\axi_rdata[18]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[18]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[18]_i_5_n_0 ),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[18]_i_2 
       (.I0(\axi_rdata[18]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[18]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[18]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[18] ),
        .I4(sel0[0]),
        .I5(mindex_reg[18]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[18]_i_5 
       (.I0(\axi_rdata[18]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[18]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[18] ),
        .I5(sel0[0]),
        .O(\axi_rdata[18]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_6 
       (.I0(ramp_offset_reg[18]),
        .I1(ramplitude_step_reg[18]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[18]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[18]_i_7 
       (.I0(autolock_min_reg[18]),
        .I1(ramp_step_reg[18]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[18]),
        .O(\axi_rdata[18]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_8 
       (.I0(loop_locked_delay_reg[18]),
        .I1(loop_locked_max_reg[18]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[18]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[18]),
        .O(\axi_rdata[18]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_9 
       (.I0(I_mon_reg[18]),
        .I1(I_reg[18]),
        .I2(sel0[1]),
        .I3(P_mon_reg[18]),
        .I4(sel0[0]),
        .I5(P_reg[18]),
        .O(\axi_rdata[18]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(\axi_rdata[19]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[19]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[19]_i_5_n_0 ),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[19]_i_2 
       (.I0(\axi_rdata[19]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[19]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[19]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[19] ),
        .I4(sel0[0]),
        .I5(mindex_reg[19]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[19]_i_5 
       (.I0(\axi_rdata[19]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[19]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[19] ),
        .I5(sel0[0]),
        .O(\axi_rdata[19]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_6 
       (.I0(ramp_offset_reg[19]),
        .I1(ramplitude_step_reg[19]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[19]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[19]_i_7 
       (.I0(autolock_min_reg[19]),
        .I1(ramp_step_reg[19]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[19]),
        .O(\axi_rdata[19]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_8 
       (.I0(loop_locked_delay_reg[19]),
        .I1(loop_locked_max_reg[19]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[19]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[19]),
        .O(\axi_rdata[19]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_9 
       (.I0(I_mon_reg[19]),
        .I1(I_reg[19]),
        .I2(sel0[1]),
        .I3(P_mon_reg[19]),
        .I4(sel0[0]),
        .I5(P_reg[19]),
        .O(\axi_rdata[19]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(\axi_rdata[1]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[1]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[1]_i_5_n_0 ),
        .O(reg_data_out[1]));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[1]_i_10 
       (.I0(setpoint_reg[1]),
        .I1(status_reg[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(p_0_in1_in),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[1]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_11 
       (.I0(I_mon_reg[1]),
        .I1(I_reg[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[1]),
        .O(\axi_rdata[1]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[1]_i_2 
       (.I0(\axi_rdata[1]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[1]_i_3 
       (.I0(\axi_rdata[1]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[1] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_6 
       (.I0(ramp_offset_reg[1]),
        .I1(ramplitude_step_reg[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[1]),
        .O(\axi_rdata[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[1]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[1]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[1]),
        .I5(pid_loop_0_input_reg[1]),
        .O(\axi_rdata[1]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[1]_i_8 
       (.I0(autolock_min_reg[1]),
        .I1(ramp_step_reg[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[1]),
        .O(\axi_rdata[1]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_9 
       (.I0(loop_locked_delay_reg[1]),
        .I1(loop_locked_max_reg[1]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[1]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[1]),
        .O(\axi_rdata[1]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(\axi_rdata[20]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[20]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[20]_i_5_n_0 ),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[20]_i_2 
       (.I0(\axi_rdata[20]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[20]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[20]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[20] ),
        .I4(sel0[0]),
        .I5(mindex_reg[20]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[20]_i_5 
       (.I0(\axi_rdata[20]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[20]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[20] ),
        .I5(sel0[0]),
        .O(\axi_rdata[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_6 
       (.I0(ramp_offset_reg[20]),
        .I1(ramplitude_step_reg[20]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[20]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[20]_i_7 
       (.I0(autolock_min_reg[20]),
        .I1(ramp_step_reg[20]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[20]),
        .O(\axi_rdata[20]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_8 
       (.I0(loop_locked_delay_reg[20]),
        .I1(loop_locked_max_reg[20]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[20]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[20]),
        .O(\axi_rdata[20]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_9 
       (.I0(I_mon_reg[20]),
        .I1(I_reg[20]),
        .I2(sel0[1]),
        .I3(P_mon_reg[20]),
        .I4(sel0[0]),
        .I5(P_reg[20]),
        .O(\axi_rdata[20]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(\axi_rdata[21]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[21]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[21]_i_5_n_0 ),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[21]_i_2 
       (.I0(\axi_rdata[21]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[21]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[21]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[21] ),
        .I4(sel0[0]),
        .I5(mindex_reg[21]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[21]_i_5 
       (.I0(\axi_rdata[21]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[21]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[21] ),
        .I5(sel0[0]),
        .O(\axi_rdata[21]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_6 
       (.I0(ramp_offset_reg[21]),
        .I1(ramplitude_step_reg[21]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[21]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[21]_i_7 
       (.I0(autolock_min_reg[21]),
        .I1(ramp_step_reg[21]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[21]),
        .O(\axi_rdata[21]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_8 
       (.I0(loop_locked_delay_reg[21]),
        .I1(loop_locked_max_reg[21]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[21]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[21]),
        .O(\axi_rdata[21]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_9 
       (.I0(I_mon_reg[21]),
        .I1(I_reg[21]),
        .I2(sel0[1]),
        .I3(P_mon_reg[21]),
        .I4(sel0[0]),
        .I5(P_reg[21]),
        .O(\axi_rdata[21]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(\axi_rdata[22]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[22]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[22]_i_5_n_0 ),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[22]_i_2 
       (.I0(\axi_rdata[22]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[22]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[22]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[22] ),
        .I4(sel0[0]),
        .I5(mindex_reg[22]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[22]_i_5 
       (.I0(\axi_rdata[22]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[22]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[22] ),
        .I5(sel0[0]),
        .O(\axi_rdata[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_6 
       (.I0(ramp_offset_reg[22]),
        .I1(ramplitude_step_reg[22]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[22]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[22]_i_7 
       (.I0(autolock_min_reg[22]),
        .I1(ramp_step_reg[22]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[22]),
        .O(\axi_rdata[22]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_8 
       (.I0(loop_locked_delay_reg[22]),
        .I1(loop_locked_max_reg[22]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[22]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[22]),
        .O(\axi_rdata[22]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_9 
       (.I0(I_mon_reg[22]),
        .I1(I_reg[22]),
        .I2(sel0[1]),
        .I3(P_mon_reg[22]),
        .I4(sel0[0]),
        .I5(P_reg[22]),
        .O(\axi_rdata[22]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(\axi_rdata[23]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[23]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[23]_i_5_n_0 ),
        .O(reg_data_out[23]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[23]_i_2 
       (.I0(\axi_rdata[23]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[23]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[23]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[23] ),
        .I4(sel0[0]),
        .I5(mindex_reg[23]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[23]_i_5 
       (.I0(\axi_rdata[23]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[23]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[23] ),
        .I5(sel0[0]),
        .O(\axi_rdata[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_6 
       (.I0(ramp_offset_reg[23]),
        .I1(ramplitude_step_reg[23]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[23]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[23]_i_7 
       (.I0(autolock_min_reg[23]),
        .I1(ramp_step_reg[23]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[23]),
        .O(\axi_rdata[23]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_8 
       (.I0(loop_locked_delay_reg[23]),
        .I1(loop_locked_max_reg[23]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[23]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[23]),
        .O(\axi_rdata[23]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_9 
       (.I0(I_mon_reg[23]),
        .I1(I_reg[23]),
        .I2(sel0[1]),
        .I3(P_mon_reg[23]),
        .I4(sel0[0]),
        .I5(P_reg[23]),
        .O(\axi_rdata[23]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(\axi_rdata[24]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[24]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[24]_i_5_n_0 ),
        .O(reg_data_out[24]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[24]_i_2 
       (.I0(\axi_rdata[24]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[24]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[24]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[24] ),
        .I4(sel0[0]),
        .I5(mindex_reg[24]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[24]_i_5 
       (.I0(\axi_rdata[24]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[24]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[24] ),
        .I5(sel0[0]),
        .O(\axi_rdata[24]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_6 
       (.I0(ramp_offset_reg[24]),
        .I1(ramplitude_step_reg[24]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[24]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[24]_i_7 
       (.I0(autolock_min_reg[24]),
        .I1(ramp_step_reg[24]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[24]),
        .O(\axi_rdata[24]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_8 
       (.I0(loop_locked_delay_reg[24]),
        .I1(loop_locked_max_reg[24]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[24]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[24]),
        .O(\axi_rdata[24]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_9 
       (.I0(I_mon_reg[24]),
        .I1(I_reg[24]),
        .I2(sel0[1]),
        .I3(P_mon_reg[24]),
        .I4(sel0[0]),
        .I5(P_reg[24]),
        .O(\axi_rdata[24]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(\axi_rdata[25]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[25]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[25]_i_5_n_0 ),
        .O(reg_data_out[25]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[25]_i_2 
       (.I0(\axi_rdata[25]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[25]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[25]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[25] ),
        .I4(sel0[0]),
        .I5(mindex_reg[25]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[25]_i_5 
       (.I0(\axi_rdata[25]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[25]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[25] ),
        .I5(sel0[0]),
        .O(\axi_rdata[25]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_6 
       (.I0(ramp_offset_reg[25]),
        .I1(ramplitude_step_reg[25]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[25]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[25]_i_7 
       (.I0(autolock_min_reg[25]),
        .I1(ramp_step_reg[25]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[25]),
        .O(\axi_rdata[25]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_8 
       (.I0(loop_locked_delay_reg[25]),
        .I1(loop_locked_max_reg[25]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[25]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[25]),
        .O(\axi_rdata[25]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_9 
       (.I0(I_mon_reg[25]),
        .I1(I_reg[25]),
        .I2(sel0[1]),
        .I3(P_mon_reg[25]),
        .I4(sel0[0]),
        .I5(P_reg[25]),
        .O(\axi_rdata[25]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(\axi_rdata[26]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[26]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[26]_i_5_n_0 ),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[26]_i_2 
       (.I0(\axi_rdata[26]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[26]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[26]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[26] ),
        .I4(sel0[0]),
        .I5(mindex_reg[26]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[26]_i_5 
       (.I0(\axi_rdata[26]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[26]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[26] ),
        .I5(sel0[0]),
        .O(\axi_rdata[26]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_6 
       (.I0(ramp_offset_reg[26]),
        .I1(ramplitude_step_reg[26]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[26]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[26]_i_7 
       (.I0(autolock_min_reg[26]),
        .I1(ramp_step_reg[26]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[26]),
        .O(\axi_rdata[26]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_8 
       (.I0(loop_locked_delay_reg[26]),
        .I1(loop_locked_max_reg[26]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[26]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[26]),
        .O(\axi_rdata[26]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_9 
       (.I0(I_mon_reg[26]),
        .I1(I_reg[26]),
        .I2(sel0[1]),
        .I3(P_mon_reg[26]),
        .I4(sel0[0]),
        .I5(P_reg[26]),
        .O(\axi_rdata[26]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(\axi_rdata[27]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[27]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[27]_i_5_n_0 ),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[27]_i_2 
       (.I0(\axi_rdata[27]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[27]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[27]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[27] ),
        .I4(sel0[0]),
        .I5(mindex_reg[27]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[27]_i_5 
       (.I0(\axi_rdata[27]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[27]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[27] ),
        .I5(sel0[0]),
        .O(\axi_rdata[27]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_6 
       (.I0(ramp_offset_reg[27]),
        .I1(ramplitude_step_reg[27]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[27]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[27]_i_7 
       (.I0(autolock_min_reg[27]),
        .I1(ramp_step_reg[27]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[27]),
        .O(\axi_rdata[27]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_8 
       (.I0(loop_locked_delay_reg[27]),
        .I1(loop_locked_max_reg[27]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[27]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[27]),
        .O(\axi_rdata[27]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_9 
       (.I0(I_mon_reg[27]),
        .I1(I_reg[27]),
        .I2(sel0[1]),
        .I3(P_mon_reg[27]),
        .I4(sel0[0]),
        .I5(P_reg[27]),
        .O(\axi_rdata[27]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(\axi_rdata[28]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[28]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[28]_i_5_n_0 ),
        .O(reg_data_out[28]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[28]_i_2 
       (.I0(\axi_rdata[28]_i_6_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[28]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[28]_i_3 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[28] ),
        .I4(sel0[0]),
        .I5(mindex_reg[28]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[28]_i_5 
       (.I0(\axi_rdata[28]_i_9_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[28]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[28] ),
        .I5(sel0[0]),
        .O(\axi_rdata[28]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_6 
       (.I0(ramp_offset_reg[28]),
        .I1(ramplitude_step_reg[28]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[28]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[28]_i_7 
       (.I0(autolock_min_reg[28]),
        .I1(ramp_step_reg[28]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[28]),
        .O(\axi_rdata[28]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_8 
       (.I0(loop_locked_delay_reg[28]),
        .I1(loop_locked_max_reg[28]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[28]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[28]),
        .O(\axi_rdata[28]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_9 
       (.I0(I_mon_reg[28]),
        .I1(I_reg[28]),
        .I2(sel0[1]),
        .I3(P_mon_reg[28]),
        .I4(sel0[0]),
        .I5(P_reg[28]),
        .O(\axi_rdata[28]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(\axi_rdata[31]_i_4_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[29]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[29]_i_4_n_0 ),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[29]_i_2 
       (.I0(\axi_rdata[29]_i_5_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[29]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[29]_i_4 
       (.I0(\axi_rdata[29]_i_8_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[29]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[29] ),
        .I5(sel0[0]),
        .O(\axi_rdata[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_5 
       (.I0(ramp_offset_reg[29]),
        .I1(ramplitude_step_reg[29]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[29]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[29]_i_6 
       (.I0(autolock_min_reg[29]),
        .I1(ramp_step_reg[29]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[29]),
        .O(\axi_rdata[29]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_7 
       (.I0(loop_locked_delay_reg[29]),
        .I1(loop_locked_max_reg[29]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[29]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[29]),
        .O(\axi_rdata[29]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_8 
       (.I0(I_mon_reg[29]),
        .I1(I_reg[29]),
        .I2(sel0[1]),
        .I3(P_mon_reg[29]),
        .I4(sel0[0]),
        .I5(P_reg[29]),
        .O(\axi_rdata[29]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(\axi_rdata[2]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[2]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[2]_i_5_n_0 ),
        .O(reg_data_out[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_10 
       (.I0(I_mon_reg[2]),
        .I1(I_reg[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[2]),
        .O(\axi_rdata[2]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[2]_i_2 
       (.I0(\axi_rdata[2]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[2]_i_3 
       (.I0(\axi_rdata[2]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[2] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[2]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[2]_i_5 
       (.I0(\axi_rdata[2]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg[2]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(\control_reg_reg_n_0_[2] ),
        .I5(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_6 
       (.I0(ramp_offset_reg[2]),
        .I1(ramplitude_step_reg[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[2]),
        .O(\axi_rdata[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[2]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[2]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[2]),
        .I5(pid_loop_0_input_reg[2]),
        .O(\axi_rdata[2]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[2]_i_8 
       (.I0(autolock_min_reg[2]),
        .I1(ramp_step_reg[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[2]),
        .O(\axi_rdata[2]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_9 
       (.I0(loop_locked_delay_reg[2]),
        .I1(loop_locked_max_reg[2]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[2]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[2]),
        .O(\axi_rdata[2]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(\axi_rdata[31]_i_4_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[30]_i_3_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[30]_i_4_n_0 ),
        .O(reg_data_out[30]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[30]_i_2 
       (.I0(\axi_rdata[30]_i_5_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[30]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[30]_i_4 
       (.I0(\axi_rdata[30]_i_8_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[30]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[30] ),
        .I5(sel0[0]),
        .O(\axi_rdata[30]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_5 
       (.I0(ramp_offset_reg[30]),
        .I1(ramplitude_step_reg[30]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[30]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[30]_i_6 
       (.I0(autolock_min_reg[30]),
        .I1(ramp_step_reg[30]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[30]),
        .O(\axi_rdata[30]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_7 
       (.I0(loop_locked_delay_reg[30]),
        .I1(loop_locked_max_reg[30]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[30]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[30]),
        .O(\axi_rdata[30]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_8 
       (.I0(I_mon_reg[30]),
        .I1(I_reg[30]),
        .I2(sel0[1]),
        .I3(P_mon_reg[30]),
        .I4(sel0[0]),
        .I5(P_reg[30]),
        .O(\axi_rdata[30]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    \axi_rdata[31]_i_1 
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_rvalid),
        .I2(axi_arready_reg_0),
        .O(slv_reg_rden));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_10 
       (.I0(loop_locked_delay_reg[31]),
        .I1(loop_locked_max_reg[31]),
        .I2(sel0[1]),
        .I3(loop_locked_min_reg[31]),
        .I4(sel0[0]),
        .I5(autolock_max_reg[31]),
        .O(\axi_rdata[31]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_11 
       (.I0(I_mon_reg[31]),
        .I1(I_reg[31]),
        .I2(sel0[1]),
        .I3(P_mon_reg[31]),
        .I4(sel0[0]),
        .I5(P_reg[31]),
        .O(\axi_rdata[31]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_2 
       (.I0(\axi_rdata[31]_i_3_n_0 ),
        .I1(\axi_rdata[31]_i_4_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[31]_i_5_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[31]_i_6_n_0 ),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[31]_i_3 
       (.I0(\axi_rdata[31]_i_7_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[31]),
        .I4(sel0[0]),
        .I5(input_railed_min_reg[31]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[31]_i_4 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[31] ),
        .I4(sel0[0]),
        .I5(mindex_reg[31]),
        .O(\axi_rdata[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[31]_i_6 
       (.I0(\axi_rdata[31]_i_11_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg__0[31]),
        .I3(sel0[1]),
        .I4(\control_reg_reg_n_0_[31] ),
        .I5(sel0[0]),
        .O(\axi_rdata[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_7 
       (.I0(ramp_offset_reg[31]),
        .I1(ramplitude_step_reg[31]),
        .I2(sel0[1]),
        .I3(autolock_input_reg[13]),
        .I4(sel0[0]),
        .I5(ramp_output_reg[31]),
        .O(\axi_rdata[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[31]_i_8 
       (.I0(sel0[2]),
        .I1(sel0[0]),
        .I2(error_reg[31]),
        .I3(sel0[1]),
        .I4(loop_output_reg[31]),
        .I5(pid_loop_0_input_reg[13]),
        .O(\axi_rdata[31]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[31]_i_9 
       (.I0(autolock_min_reg[31]),
        .I1(ramp_step_reg[31]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(ramplitude_reg[31]),
        .O(\axi_rdata[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(\axi_rdata[3]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[3]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[3]_i_5_n_0 ),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_10 
       (.I0(I_mon_reg[3]),
        .I1(I_reg[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[3]),
        .O(\axi_rdata[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[3]_i_2 
       (.I0(\axi_rdata[3]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[3]_i_3 
       (.I0(\axi_rdata[3]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[3] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[3]_i_5 
       (.I0(\axi_rdata[3]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg[3]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(\control_reg_reg_n_0_[3] ),
        .I5(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_6 
       (.I0(ramp_offset_reg[3]),
        .I1(ramplitude_step_reg[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[3]),
        .O(\axi_rdata[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[3]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[3]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[3]),
        .I5(pid_loop_0_input_reg[3]),
        .O(\axi_rdata[3]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[3]_i_8 
       (.I0(autolock_min_reg[3]),
        .I1(ramp_step_reg[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[3]),
        .O(\axi_rdata[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_9 
       (.I0(loop_locked_delay_reg[3]),
        .I1(loop_locked_max_reg[3]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[3]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[3]),
        .O(\axi_rdata[3]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(\axi_rdata[4]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[4]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[4]_i_5_n_0 ),
        .O(reg_data_out[4]));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[4]_i_10 
       (.I0(setpoint_reg[4]),
        .I1(status_reg[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(ramp_enable),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[4]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_11 
       (.I0(I_mon_reg[4]),
        .I1(I_reg[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[4]),
        .O(\axi_rdata[4]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[4]_i_2 
       (.I0(\axi_rdata[4]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[4]_i_3 
       (.I0(\axi_rdata[4]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[4] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[4]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_6 
       (.I0(ramp_offset_reg[4]),
        .I1(ramplitude_step_reg[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[4]),
        .O(\axi_rdata[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[4]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[4]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[4]),
        .I5(pid_loop_0_input_reg[4]),
        .O(\axi_rdata[4]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[4]_i_8 
       (.I0(autolock_min_reg[4]),
        .I1(ramp_step_reg[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[4]),
        .O(\axi_rdata[4]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_9 
       (.I0(loop_locked_delay_reg[4]),
        .I1(loop_locked_max_reg[4]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[4]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[4]),
        .O(\axi_rdata[4]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(\axi_rdata[5]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[5]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[5]_i_5_n_0 ),
        .O(reg_data_out[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_10 
       (.I0(I_mon_reg[5]),
        .I1(I_reg[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[5]),
        .O(\axi_rdata[5]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[5]_i_2 
       (.I0(\axi_rdata[5]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[5]_i_3 
       (.I0(\axi_rdata[5]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[5] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[5]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[5]_i_5 
       (.I0(\axi_rdata[5]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg[5]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(\control_reg_reg_n_0_[5] ),
        .I5(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_6 
       (.I0(ramp_offset_reg[5]),
        .I1(ramplitude_step_reg[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[5]),
        .O(\axi_rdata[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[5]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[5]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[5]),
        .I5(pid_loop_0_input_reg[5]),
        .O(\axi_rdata[5]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[5]_i_8 
       (.I0(autolock_min_reg[5]),
        .I1(ramp_step_reg[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[5]),
        .O(\axi_rdata[5]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_9 
       (.I0(loop_locked_delay_reg[5]),
        .I1(loop_locked_max_reg[5]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[5]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[5]),
        .O(\axi_rdata[5]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(\axi_rdata[6]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[6]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[6]_i_5_n_0 ),
        .O(reg_data_out[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_10 
       (.I0(I_mon_reg[6]),
        .I1(I_reg[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[6]),
        .O(\axi_rdata[6]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[6]_i_2 
       (.I0(\axi_rdata[6]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[6]_i_3 
       (.I0(\axi_rdata[6]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[6] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[6]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[6]_i_5 
       (.I0(\axi_rdata[6]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg[6]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(\control_reg_reg_n_0_[6] ),
        .I5(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_6 
       (.I0(ramp_offset_reg[6]),
        .I1(ramplitude_step_reg[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[6]),
        .O(\axi_rdata[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[6]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[6]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[6]),
        .I5(pid_loop_0_input_reg[6]),
        .O(\axi_rdata[6]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[6]_i_8 
       (.I0(autolock_min_reg[6]),
        .I1(ramp_step_reg[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[6]),
        .O(\axi_rdata[6]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_9 
       (.I0(loop_locked_delay_reg[6]),
        .I1(loop_locked_max_reg[6]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[6]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[6]),
        .O(\axi_rdata[6]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(\axi_rdata[7]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[7]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata[7]_i_5_n_0 ),
        .O(reg_data_out[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_10 
       (.I0(I_mon_reg[7]),
        .I1(I_reg[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[7]),
        .O(\axi_rdata[7]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[7]_i_2 
       (.I0(\axi_rdata[7]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[7]_i_3 
       (.I0(\axi_rdata[7]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[7] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[7]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB888B88888BB8888)) 
    \axi_rdata[7]_i_5 
       (.I0(\axi_rdata[7]_i_10_n_0 ),
        .I1(sel0[2]),
        .I2(setpoint_reg[7]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(\control_reg_reg_n_0_[7] ),
        .I5(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_6 
       (.I0(ramp_offset_reg[7]),
        .I1(ramplitude_step_reg[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[7]),
        .O(\axi_rdata[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[7]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[7]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[7]),
        .I5(pid_loop_0_input_reg[7]),
        .O(\axi_rdata[7]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[7]_i_8 
       (.I0(autolock_min_reg[7]),
        .I1(ramp_step_reg[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[7]),
        .O(\axi_rdata[7]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_9 
       (.I0(loop_locked_delay_reg[7]),
        .I1(loop_locked_max_reg[7]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[7]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[7]),
        .O(\axi_rdata[7]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(\axi_rdata[8]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[8]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[8]_i_5_n_0 ),
        .O(reg_data_out[8]));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[8]_i_10 
       (.I0(setpoint_reg[8]),
        .I1(status_reg[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[8]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_11 
       (.I0(I_mon_reg[8]),
        .I1(I_reg[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[8]),
        .O(\axi_rdata[8]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[8]_i_2 
       (.I0(\axi_rdata[8]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[8]_i_3 
       (.I0(\axi_rdata[8]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[8] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_6 
       (.I0(ramp_offset_reg[8]),
        .I1(ramplitude_step_reg[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[8]),
        .O(\axi_rdata[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[8]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[8]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[8]),
        .I5(pid_loop_0_input_reg[8]),
        .O(\axi_rdata[8]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[8]_i_8 
       (.I0(autolock_min_reg[8]),
        .I1(ramp_step_reg[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[8]),
        .O(\axi_rdata[8]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_9 
       (.I0(loop_locked_delay_reg[8]),
        .I1(loop_locked_max_reg[8]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[8]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[8]),
        .O(\axi_rdata[8]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(\axi_rdata[9]_i_3_n_0 ),
        .I2(sel0[3]),
        .I3(\axi_rdata_reg[9]_i_4_n_0 ),
        .I4(sel0[4]),
        .I5(\axi_rdata_reg[9]_i_5_n_0 ),
        .O(reg_data_out[9]));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[9]_i_10 
       (.I0(setpoint_reg[9]),
        .I1(status_reg[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\control_reg_reg_n_0_[9] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .O(\axi_rdata[9]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_11 
       (.I0(I_mon_reg[9]),
        .I1(I_reg[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(P_mon_reg[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(P_reg[9]),
        .O(\axi_rdata[9]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[9]_i_2 
       (.I0(\axi_rdata[9]_i_6_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(input_railed_max_reg[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(input_railed_min_reg[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \axi_rdata[9]_i_3 
       (.I0(\axi_rdata[9]_i_7_n_0 ),
        .I1(\axi_araddr_reg[3]_rep_n_0 ),
        .I2(sel0[2]),
        .I3(\maxdex_reg_reg_n_0_[9] ),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(mindex_reg[9]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_6 
       (.I0(ramp_offset_reg[9]),
        .I1(ramplitude_step_reg[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(autolock_input_reg[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(ramp_output_reg[9]),
        .O(\axi_rdata[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5510441011100010)) 
    \axi_rdata[9]_i_7 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[2]_rep_n_0 ),
        .I2(error_reg[9]),
        .I3(\axi_araddr_reg[3]_rep_n_0 ),
        .I4(loop_output_reg[9]),
        .I5(pid_loop_0_input_reg[9]),
        .O(\axi_rdata[9]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[9]_i_8 
       (.I0(autolock_min_reg[9]),
        .I1(ramp_step_reg[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(\axi_araddr_reg[2]_rep_n_0 ),
        .I4(ramplitude_reg[9]),
        .O(\axi_rdata[9]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_9 
       (.I0(loop_locked_delay_reg[9]),
        .I1(loop_locked_max_reg[9]),
        .I2(\axi_araddr_reg[3]_rep_n_0 ),
        .I3(loop_locked_min_reg[9]),
        .I4(\axi_araddr_reg[2]_rep_n_0 ),
        .I5(autolock_max_reg[9]),
        .O(\axi_rdata[9]_i_9_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[0]_i_4 
       (.I0(\axi_rdata[0]_i_8_n_0 ),
        .I1(\axi_rdata[0]_i_9_n_0 ),
        .O(\axi_rdata_reg[0]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[0]_i_5 
       (.I0(\axi_rdata[0]_i_10_n_0 ),
        .I1(\axi_rdata[0]_i_11_n_0 ),
        .O(\axi_rdata_reg[0]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[10]_i_4 
       (.I0(\axi_rdata[10]_i_8_n_0 ),
        .I1(\axi_rdata[10]_i_9_n_0 ),
        .O(\axi_rdata_reg[10]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_5 
       (.I0(\axi_rdata[10]_i_10_n_0 ),
        .I1(\axi_rdata[10]_i_11_n_0 ),
        .O(\axi_rdata_reg[10]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[11]_i_4 
       (.I0(\axi_rdata[11]_i_8_n_0 ),
        .I1(\axi_rdata[11]_i_9_n_0 ),
        .O(\axi_rdata_reg[11]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_5 
       (.I0(\axi_rdata[11]_i_10_n_0 ),
        .I1(\axi_rdata[11]_i_11_n_0 ),
        .O(\axi_rdata_reg[11]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[12]_i_4 
       (.I0(\axi_rdata[12]_i_8_n_0 ),
        .I1(\axi_rdata[12]_i_9_n_0 ),
        .O(\axi_rdata_reg[12]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[13]_i_3 
       (.I0(\axi_rdata[13]_i_6_n_0 ),
        .I1(\axi_rdata[13]_i_7_n_0 ),
        .O(\axi_rdata_reg[13]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[14]_i_3 
       (.I0(\axi_rdata[14]_i_6_n_0 ),
        .I1(\axi_rdata[14]_i_7_n_0 ),
        .O(\axi_rdata_reg[14]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[15]_i_4 
       (.I0(\axi_rdata[15]_i_7_n_0 ),
        .I1(\axi_rdata[15]_i_8_n_0 ),
        .O(\axi_rdata_reg[15]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[16]_i_4 
       (.I0(\axi_rdata[16]_i_7_n_0 ),
        .I1(\axi_rdata[16]_i_8_n_0 ),
        .O(\axi_rdata_reg[16]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[17]_i_4 
       (.I0(\axi_rdata[17]_i_7_n_0 ),
        .I1(\axi_rdata[17]_i_8_n_0 ),
        .O(\axi_rdata_reg[17]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[18]_i_4 
       (.I0(\axi_rdata[18]_i_7_n_0 ),
        .I1(\axi_rdata[18]_i_8_n_0 ),
        .O(\axi_rdata_reg[18]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[19]_i_4 
       (.I0(\axi_rdata[19]_i_7_n_0 ),
        .I1(\axi_rdata[19]_i_8_n_0 ),
        .O(\axi_rdata_reg[19]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[1]_i_4 
       (.I0(\axi_rdata[1]_i_8_n_0 ),
        .I1(\axi_rdata[1]_i_9_n_0 ),
        .O(\axi_rdata_reg[1]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_5 
       (.I0(\axi_rdata[1]_i_10_n_0 ),
        .I1(\axi_rdata[1]_i_11_n_0 ),
        .O(\axi_rdata_reg[1]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[20]_i_4 
       (.I0(\axi_rdata[20]_i_7_n_0 ),
        .I1(\axi_rdata[20]_i_8_n_0 ),
        .O(\axi_rdata_reg[20]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[21]_i_4 
       (.I0(\axi_rdata[21]_i_7_n_0 ),
        .I1(\axi_rdata[21]_i_8_n_0 ),
        .O(\axi_rdata_reg[21]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[22]_i_4 
       (.I0(\axi_rdata[22]_i_7_n_0 ),
        .I1(\axi_rdata[22]_i_8_n_0 ),
        .O(\axi_rdata_reg[22]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[23]_i_4 
       (.I0(\axi_rdata[23]_i_7_n_0 ),
        .I1(\axi_rdata[23]_i_8_n_0 ),
        .O(\axi_rdata_reg[23]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[24]_i_4 
       (.I0(\axi_rdata[24]_i_7_n_0 ),
        .I1(\axi_rdata[24]_i_8_n_0 ),
        .O(\axi_rdata_reg[24]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[25]_i_4 
       (.I0(\axi_rdata[25]_i_7_n_0 ),
        .I1(\axi_rdata[25]_i_8_n_0 ),
        .O(\axi_rdata_reg[25]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[26]_i_4 
       (.I0(\axi_rdata[26]_i_7_n_0 ),
        .I1(\axi_rdata[26]_i_8_n_0 ),
        .O(\axi_rdata_reg[26]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[27]_i_4 
       (.I0(\axi_rdata[27]_i_7_n_0 ),
        .I1(\axi_rdata[27]_i_8_n_0 ),
        .O(\axi_rdata_reg[27]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[28]_i_4 
       (.I0(\axi_rdata[28]_i_7_n_0 ),
        .I1(\axi_rdata[28]_i_8_n_0 ),
        .O(\axi_rdata_reg[28]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[29]_i_3 
       (.I0(\axi_rdata[29]_i_6_n_0 ),
        .I1(\axi_rdata[29]_i_7_n_0 ),
        .O(\axi_rdata_reg[29]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[2]_i_4 
       (.I0(\axi_rdata[2]_i_8_n_0 ),
        .I1(\axi_rdata[2]_i_9_n_0 ),
        .O(\axi_rdata_reg[2]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[30]_i_3 
       (.I0(\axi_rdata[30]_i_6_n_0 ),
        .I1(\axi_rdata[30]_i_7_n_0 ),
        .O(\axi_rdata_reg[30]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[31]_i_5 
       (.I0(\axi_rdata[31]_i_9_n_0 ),
        .I1(\axi_rdata[31]_i_10_n_0 ),
        .O(\axi_rdata_reg[31]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[3]_i_4 
       (.I0(\axi_rdata[3]_i_8_n_0 ),
        .I1(\axi_rdata[3]_i_9_n_0 ),
        .O(\axi_rdata_reg[3]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[4]_i_4 
       (.I0(\axi_rdata[4]_i_8_n_0 ),
        .I1(\axi_rdata[4]_i_9_n_0 ),
        .O(\axi_rdata_reg[4]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_5 
       (.I0(\axi_rdata[4]_i_10_n_0 ),
        .I1(\axi_rdata[4]_i_11_n_0 ),
        .O(\axi_rdata_reg[4]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[5]_i_4 
       (.I0(\axi_rdata[5]_i_8_n_0 ),
        .I1(\axi_rdata[5]_i_9_n_0 ),
        .O(\axi_rdata_reg[5]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[6]_i_4 
       (.I0(\axi_rdata[6]_i_8_n_0 ),
        .I1(\axi_rdata[6]_i_9_n_0 ),
        .O(\axi_rdata_reg[6]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[7]_i_4 
       (.I0(\axi_rdata[7]_i_8_n_0 ),
        .I1(\axi_rdata[7]_i_9_n_0 ),
        .O(\axi_rdata_reg[7]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[8]_i_4 
       (.I0(\axi_rdata[8]_i_8_n_0 ),
        .I1(\axi_rdata[8]_i_9_n_0 ),
        .O(\axi_rdata_reg[8]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_5 
       (.I0(\axi_rdata[8]_i_10_n_0 ),
        .I1(\axi_rdata[8]_i_11_n_0 ),
        .O(\axi_rdata_reg[8]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(pid_loop_0_rst1));
  MUXF7 \axi_rdata_reg[9]_i_4 
       (.I0(\axi_rdata[9]_i_8_n_0 ),
        .I1(\axi_rdata[9]_i_9_n_0 ),
        .O(\axi_rdata_reg[9]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_5 
       (.I0(\axi_rdata[9]_i_10_n_0 ),
        .I1(\axi_rdata[9]_i_11_n_0 ),
        .O(\axi_rdata_reg[9]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_reg_0),
        .Q(s00_axi_rvalid),
        .R(pid_loop_0_rst1));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(axi_wready_reg_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(axi_wready_reg_0),
        .R(pid_loop_0_rst1));
  LUT2 #(
    .INIT(4'h8)) 
    \control_reg[15]_i_1 
       (.I0(\control_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\control_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \control_reg[23]_i_1 
       (.I0(\control_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\control_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \control_reg[31]_i_1 
       (.I0(\control_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\control_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \control_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\control_reg[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \control_reg[31]_i_3 
       (.I0(axi_wready_reg_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(axi_awready_reg_0),
        .O(\control_reg[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \control_reg[7]_i_1 
       (.I0(\control_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\control_reg[7]_i_1_n_0 ));
  FDRE \control_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\control_reg_reg_n_0_[0] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(\control_reg_reg_n_0_[10] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\control_reg_reg_n_0_[11] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\control_reg_reg_n_0_[12] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\control_reg_reg_n_0_[13] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\control_reg_reg_n_0_[14] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\control_reg_reg_n_0_[15] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(\control_reg_reg_n_0_[16] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(\control_reg_reg_n_0_[17] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(\control_reg_reg_n_0_[18] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(\control_reg_reg_n_0_[19] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(p_0_in1_in),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(\control_reg_reg_n_0_[20] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(\control_reg_reg_n_0_[21] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(\control_reg_reg_n_0_[22] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(\control_reg_reg_n_0_[23] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(\control_reg_reg_n_0_[24] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(\control_reg_reg_n_0_[25] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(\control_reg_reg_n_0_[26] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(\control_reg_reg_n_0_[27] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(\control_reg_reg_n_0_[28] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(\control_reg_reg_n_0_[29] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\control_reg_reg_n_0_[2] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(\control_reg_reg_n_0_[30] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(\control_reg_reg_n_0_[31] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\control_reg_reg_n_0_[3] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(ramp_enable),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\control_reg_reg_n_0_[5] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(\control_reg_reg_n_0_[6] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(\control_reg_reg_n_0_[7] ),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(autolock),
        .R(pid_loop_0_rst1));
  FDRE \control_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\control_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(\control_reg_reg_n_0_[9] ),
        .R(pid_loop_0_rst1));
  FDRE \error_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[0]),
        .Q(error_reg[0]),
        .R(1'b0));
  FDRE \error_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[10]),
        .Q(error_reg[10]),
        .R(1'b0));
  FDRE \error_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[11]),
        .Q(error_reg[11]),
        .R(1'b0));
  FDRE \error_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[12]),
        .Q(error_reg[12]),
        .R(1'b0));
  FDRE \error_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[1]),
        .Q(error_reg[1]),
        .R(1'b0));
  FDRE \error_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[2]),
        .Q(error_reg[2]),
        .R(1'b0));
  FDRE \error_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[13]),
        .Q(error_reg[31]),
        .R(1'b0));
  FDRE \error_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[3]),
        .Q(error_reg[3]),
        .R(1'b0));
  FDRE \error_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[4]),
        .Q(error_reg[4]),
        .R(1'b0));
  FDRE \error_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[5]),
        .Q(error_reg[5]),
        .R(1'b0));
  FDRE \error_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[6]),
        .Q(error_reg[6]),
        .R(1'b0));
  FDRE \error_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[7]),
        .Q(error_reg[7]),
        .R(1'b0));
  FDRE \error_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[8]),
        .Q(error_reg[8]),
        .R(1'b0));
  FDRE \error_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_error[9]),
        .Q(error_reg[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \input_railed_max_reg[15]_i_1 
       (.I0(\input_railed_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\input_railed_max_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \input_railed_max_reg[23]_i_1 
       (.I0(\input_railed_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\input_railed_max_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \input_railed_max_reg[31]_i_1 
       (.I0(\input_railed_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\input_railed_max_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \input_railed_max_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\input_railed_max_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \input_railed_max_reg[7]_i_1 
       (.I0(\input_railed_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\input_railed_max_reg[7]_i_1_n_0 ));
  FDRE \input_railed_max_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(input_railed_max_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(input_railed_max_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(input_railed_max_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(input_railed_max_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(input_railed_max_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(input_railed_max_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(input_railed_max_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(input_railed_max_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(input_railed_max_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(input_railed_max_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(input_railed_max_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(input_railed_max_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(input_railed_max_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(input_railed_max_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(input_railed_max_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(input_railed_max_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(input_railed_max_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(input_railed_max_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(input_railed_max_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(input_railed_max_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(input_railed_max_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(input_railed_max_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(input_railed_max_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(input_railed_max_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(input_railed_max_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(input_railed_max_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(input_railed_max_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(input_railed_max_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(input_railed_max_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(input_railed_max_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(input_railed_max_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_max_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(input_railed_max_reg[9]),
        .R(pid_loop_0_rst1));
  LUT2 #(
    .INIT(4'h8)) 
    \input_railed_min_reg[15]_i_1 
       (.I0(\input_railed_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\input_railed_min_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \input_railed_min_reg[23]_i_1 
       (.I0(\input_railed_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\input_railed_min_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \input_railed_min_reg[31]_i_1 
       (.I0(\input_railed_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\input_railed_min_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \input_railed_min_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\input_railed_min_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \input_railed_min_reg[7]_i_1 
       (.I0(\input_railed_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\input_railed_min_reg[7]_i_1_n_0 ));
  FDRE \input_railed_min_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(input_railed_min_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(input_railed_min_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(input_railed_min_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(input_railed_min_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(input_railed_min_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(input_railed_min_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(input_railed_min_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(input_railed_min_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(input_railed_min_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(input_railed_min_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(input_railed_min_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(input_railed_min_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(input_railed_min_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(input_railed_min_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(input_railed_min_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(input_railed_min_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(input_railed_min_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(input_railed_min_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(input_railed_min_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(input_railed_min_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(input_railed_min_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(input_railed_min_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(input_railed_min_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(input_railed_min_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(input_railed_min_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(input_railed_min_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(input_railed_min_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(input_railed_min_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(input_railed_min_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(input_railed_min_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(input_railed_min_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \input_railed_min_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\input_railed_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(input_railed_min_reg[9]),
        .R(pid_loop_0_rst1));
  LUT6 #(
    .INIT(64'h0000FE0000000000)) 
    input_reset_reg_i_1
       (.I0(input_reset),
        .I1(input_railed00_in),
        .I2(input_railed0),
        .I3(\control_reg_reg_n_0_[0] ),
        .I4(input_stable),
        .I5(p_7_in),
        .O(input_reset_reg_i_1_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_10
       (.I0(input_railed_max_reg[30]),
        .I1(input_railed_max_reg[31]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_10_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_11
       (.I0(input_railed_max_reg[28]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_max_reg[29]),
        .O(input_reset_reg_i_11_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_12
       (.I0(input_railed_max_reg[26]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_max_reg[27]),
        .O(input_reset_reg_i_12_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_13
       (.I0(input_railed_max_reg[24]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_max_reg[25]),
        .O(input_reset_reg_i_13_n_0));
  LUT3 #(
    .INIT(8'h32)) 
    input_reset_reg_i_15
       (.I0(input_railed_min_reg[30]),
        .I1(input_railed_min_reg[31]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_15_n_0));
  LUT3 #(
    .INIT(8'h32)) 
    input_reset_reg_i_16
       (.I0(input_railed_min_reg[28]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_min_reg[29]),
        .O(input_reset_reg_i_16_n_0));
  LUT3 #(
    .INIT(8'h32)) 
    input_reset_reg_i_17
       (.I0(input_railed_min_reg[26]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_min_reg[27]),
        .O(input_reset_reg_i_17_n_0));
  LUT3 #(
    .INIT(8'h32)) 
    input_reset_reg_i_18
       (.I0(input_railed_min_reg[24]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_min_reg[25]),
        .O(input_reset_reg_i_18_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_19
       (.I0(input_railed_min_reg[30]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_min_reg[31]),
        .O(input_reset_reg_i_19_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_20
       (.I0(input_railed_min_reg[28]),
        .I1(input_railed_min_reg[29]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_20_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_21
       (.I0(input_railed_min_reg[26]),
        .I1(input_railed_min_reg[27]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_21_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_22
       (.I0(input_railed_min_reg[24]),
        .I1(input_railed_min_reg[25]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_22_n_0));
  LUT3 #(
    .INIT(8'h70)) 
    input_reset_reg_i_24
       (.I0(input_railed_max_reg[22]),
        .I1(input_railed_max_reg[23]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_24_n_0));
  LUT3 #(
    .INIT(8'h70)) 
    input_reset_reg_i_25
       (.I0(input_railed_max_reg[20]),
        .I1(input_railed_max_reg[21]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_25_n_0));
  LUT3 #(
    .INIT(8'h70)) 
    input_reset_reg_i_26
       (.I0(input_railed_max_reg[18]),
        .I1(input_railed_max_reg[19]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_26_n_0));
  LUT3 #(
    .INIT(8'h70)) 
    input_reset_reg_i_27
       (.I0(input_railed_max_reg[16]),
        .I1(input_railed_max_reg[17]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_27_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_28
       (.I0(input_railed_max_reg[22]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_max_reg[23]),
        .O(input_reset_reg_i_28_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_29
       (.I0(input_railed_max_reg[20]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_max_reg[21]),
        .O(input_reset_reg_i_29_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_30
       (.I0(input_railed_max_reg[18]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_max_reg[19]),
        .O(input_reset_reg_i_30_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_31
       (.I0(input_railed_max_reg[16]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_max_reg[17]),
        .O(input_reset_reg_i_31_n_0));
  LUT3 #(
    .INIT(8'h32)) 
    input_reset_reg_i_33
       (.I0(input_railed_min_reg[22]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_min_reg[23]),
        .O(input_reset_reg_i_33_n_0));
  LUT3 #(
    .INIT(8'h32)) 
    input_reset_reg_i_34
       (.I0(input_railed_min_reg[20]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_min_reg[21]),
        .O(input_reset_reg_i_34_n_0));
  LUT3 #(
    .INIT(8'h32)) 
    input_reset_reg_i_35
       (.I0(input_railed_min_reg[18]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_min_reg[19]),
        .O(input_reset_reg_i_35_n_0));
  LUT3 #(
    .INIT(8'h32)) 
    input_reset_reg_i_36
       (.I0(input_railed_min_reg[16]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_min_reg[17]),
        .O(input_reset_reg_i_36_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_37
       (.I0(input_railed_min_reg[22]),
        .I1(input_railed_min_reg[23]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_37_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_38
       (.I0(input_railed_min_reg[20]),
        .I1(input_railed_min_reg[21]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_38_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_39
       (.I0(input_railed_min_reg[18]),
        .I1(input_railed_min_reg[19]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_39_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    input_reset_reg_i_4
       (.I0(input_stable0),
        .I1(input_stable02_in),
        .O(input_stable));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_40
       (.I0(input_railed_min_reg[16]),
        .I1(input_railed_min_reg[17]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_40_n_0));
  LUT3 #(
    .INIT(8'h70)) 
    input_reset_reg_i_42
       (.I0(input_railed_max_reg[14]),
        .I1(input_railed_max_reg[15]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_42_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_43
       (.I0(pid_loop_0_input_reg[12]),
        .I1(input_railed_max_reg[12]),
        .I2(input_railed_max_reg[13]),
        .I3(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_43_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_44
       (.I0(pid_loop_0_input_reg[10]),
        .I1(input_railed_max_reg[10]),
        .I2(input_railed_max_reg[11]),
        .I3(pid_loop_0_input_reg[11]),
        .O(input_reset_reg_i_44_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_45
       (.I0(pid_loop_0_input_reg[8]),
        .I1(input_railed_max_reg[8]),
        .I2(input_railed_max_reg[9]),
        .I3(pid_loop_0_input_reg[9]),
        .O(input_reset_reg_i_45_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_46
       (.I0(input_railed_max_reg[14]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_max_reg[15]),
        .O(input_reset_reg_i_46_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_47
       (.I0(pid_loop_0_input_reg[12]),
        .I1(input_railed_max_reg[12]),
        .I2(pid_loop_0_input_reg[13]),
        .I3(input_railed_max_reg[13]),
        .O(input_reset_reg_i_47_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_48
       (.I0(pid_loop_0_input_reg[10]),
        .I1(input_railed_max_reg[10]),
        .I2(pid_loop_0_input_reg[11]),
        .I3(input_railed_max_reg[11]),
        .O(input_reset_reg_i_48_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_49
       (.I0(pid_loop_0_input_reg[8]),
        .I1(input_railed_max_reg[8]),
        .I2(pid_loop_0_input_reg[9]),
        .I3(input_railed_max_reg[9]),
        .O(input_reset_reg_i_49_n_0));
  LUT3 #(
    .INIT(8'h32)) 
    input_reset_reg_i_51
       (.I0(input_railed_min_reg[14]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_min_reg[15]),
        .O(input_reset_reg_i_51_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_52
       (.I0(input_railed_min_reg[12]),
        .I1(pid_loop_0_input_reg[12]),
        .I2(pid_loop_0_input_reg[13]),
        .I3(input_railed_min_reg[13]),
        .O(input_reset_reg_i_52_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_53
       (.I0(input_railed_min_reg[10]),
        .I1(pid_loop_0_input_reg[10]),
        .I2(pid_loop_0_input_reg[11]),
        .I3(input_railed_min_reg[11]),
        .O(input_reset_reg_i_53_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_54
       (.I0(input_railed_min_reg[8]),
        .I1(pid_loop_0_input_reg[8]),
        .I2(pid_loop_0_input_reg[9]),
        .I3(input_railed_min_reg[9]),
        .O(input_reset_reg_i_54_n_0));
  LUT3 #(
    .INIT(8'h81)) 
    input_reset_reg_i_55
       (.I0(input_railed_min_reg[14]),
        .I1(input_railed_min_reg[15]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_55_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_56
       (.I0(input_railed_min_reg[12]),
        .I1(pid_loop_0_input_reg[12]),
        .I2(input_railed_min_reg[13]),
        .I3(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_56_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_57
       (.I0(input_railed_min_reg[10]),
        .I1(pid_loop_0_input_reg[10]),
        .I2(input_railed_min_reg[11]),
        .I3(pid_loop_0_input_reg[11]),
        .O(input_reset_reg_i_57_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_58
       (.I0(input_railed_min_reg[8]),
        .I1(pid_loop_0_input_reg[8]),
        .I2(input_railed_min_reg[9]),
        .I3(pid_loop_0_input_reg[9]),
        .O(input_reset_reg_i_58_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_59
       (.I0(pid_loop_0_input_reg[6]),
        .I1(input_railed_max_reg[6]),
        .I2(input_railed_max_reg[7]),
        .I3(pid_loop_0_input_reg[7]),
        .O(input_reset_reg_i_59_n_0));
  LUT3 #(
    .INIT(8'h70)) 
    input_reset_reg_i_6
       (.I0(input_railed_max_reg[30]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(input_railed_max_reg[31]),
        .O(input_reset_reg_i_6_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_60
       (.I0(pid_loop_0_input_reg[4]),
        .I1(input_railed_max_reg[4]),
        .I2(input_railed_max_reg[5]),
        .I3(pid_loop_0_input_reg[5]),
        .O(input_reset_reg_i_60_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_61
       (.I0(pid_loop_0_input_reg[2]),
        .I1(input_railed_max_reg[2]),
        .I2(input_railed_max_reg[3]),
        .I3(pid_loop_0_input_reg[3]),
        .O(input_reset_reg_i_61_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_62
       (.I0(pid_loop_0_input_reg[0]),
        .I1(input_railed_max_reg[0]),
        .I2(input_railed_max_reg[1]),
        .I3(pid_loop_0_input_reg[1]),
        .O(input_reset_reg_i_62_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_63
       (.I0(pid_loop_0_input_reg[6]),
        .I1(input_railed_max_reg[6]),
        .I2(pid_loop_0_input_reg[7]),
        .I3(input_railed_max_reg[7]),
        .O(input_reset_reg_i_63_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_64
       (.I0(pid_loop_0_input_reg[4]),
        .I1(input_railed_max_reg[4]),
        .I2(pid_loop_0_input_reg[5]),
        .I3(input_railed_max_reg[5]),
        .O(input_reset_reg_i_64_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_65
       (.I0(pid_loop_0_input_reg[2]),
        .I1(input_railed_max_reg[2]),
        .I2(pid_loop_0_input_reg[3]),
        .I3(input_railed_max_reg[3]),
        .O(input_reset_reg_i_65_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_66
       (.I0(pid_loop_0_input_reg[0]),
        .I1(input_railed_max_reg[0]),
        .I2(pid_loop_0_input_reg[1]),
        .I3(input_railed_max_reg[1]),
        .O(input_reset_reg_i_66_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_67
       (.I0(input_railed_min_reg[6]),
        .I1(pid_loop_0_input_reg[6]),
        .I2(pid_loop_0_input_reg[7]),
        .I3(input_railed_min_reg[7]),
        .O(input_reset_reg_i_67_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_68
       (.I0(input_railed_min_reg[4]),
        .I1(pid_loop_0_input_reg[4]),
        .I2(pid_loop_0_input_reg[5]),
        .I3(input_railed_min_reg[5]),
        .O(input_reset_reg_i_68_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_69
       (.I0(input_railed_min_reg[2]),
        .I1(pid_loop_0_input_reg[2]),
        .I2(pid_loop_0_input_reg[3]),
        .I3(input_railed_min_reg[3]),
        .O(input_reset_reg_i_69_n_0));
  LUT3 #(
    .INIT(8'h70)) 
    input_reset_reg_i_7
       (.I0(input_railed_max_reg[28]),
        .I1(input_railed_max_reg[29]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_7_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    input_reset_reg_i_70
       (.I0(input_railed_min_reg[0]),
        .I1(pid_loop_0_input_reg[0]),
        .I2(pid_loop_0_input_reg[1]),
        .I3(input_railed_min_reg[1]),
        .O(input_reset_reg_i_70_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_71
       (.I0(input_railed_min_reg[6]),
        .I1(pid_loop_0_input_reg[6]),
        .I2(input_railed_min_reg[7]),
        .I3(pid_loop_0_input_reg[7]),
        .O(input_reset_reg_i_71_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_72
       (.I0(input_railed_min_reg[4]),
        .I1(pid_loop_0_input_reg[4]),
        .I2(input_railed_min_reg[5]),
        .I3(pid_loop_0_input_reg[5]),
        .O(input_reset_reg_i_72_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_73
       (.I0(input_railed_min_reg[2]),
        .I1(pid_loop_0_input_reg[2]),
        .I2(input_railed_min_reg[3]),
        .I3(pid_loop_0_input_reg[3]),
        .O(input_reset_reg_i_73_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    input_reset_reg_i_74
       (.I0(input_railed_min_reg[0]),
        .I1(pid_loop_0_input_reg[0]),
        .I2(input_railed_min_reg[1]),
        .I3(pid_loop_0_input_reg[1]),
        .O(input_reset_reg_i_74_n_0));
  LUT3 #(
    .INIT(8'h70)) 
    input_reset_reg_i_8
       (.I0(input_railed_max_reg[26]),
        .I1(input_railed_max_reg[27]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_8_n_0));
  LUT3 #(
    .INIT(8'h70)) 
    input_reset_reg_i_9
       (.I0(input_railed_max_reg[24]),
        .I1(input_railed_max_reg[25]),
        .I2(pid_loop_0_input_reg[13]),
        .O(input_reset_reg_i_9_n_0));
  FDRE input_reset_reg_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(input_reset_reg_i_1_n_0),
        .Q(input_reset),
        .R(1'b0));
  CARRY4 input_reset_reg_reg_i_14
       (.CI(input_reset_reg_reg_i_32_n_0),
        .CO({input_reset_reg_reg_i_14_n_0,input_reset_reg_reg_i_14_n_1,input_reset_reg_reg_i_14_n_2,input_reset_reg_reg_i_14_n_3}),
        .CYINIT(1'b0),
        .DI({input_reset_reg_i_33_n_0,input_reset_reg_i_34_n_0,input_reset_reg_i_35_n_0,input_reset_reg_i_36_n_0}),
        .O(NLW_input_reset_reg_reg_i_14_O_UNCONNECTED[3:0]),
        .S({input_reset_reg_i_37_n_0,input_reset_reg_i_38_n_0,input_reset_reg_i_39_n_0,input_reset_reg_i_40_n_0}));
  CARRY4 input_reset_reg_reg_i_2
       (.CI(input_reset_reg_reg_i_5_n_0),
        .CO({input_railed00_in,input_reset_reg_reg_i_2_n_1,input_reset_reg_reg_i_2_n_2,input_reset_reg_reg_i_2_n_3}),
        .CYINIT(1'b0),
        .DI({input_reset_reg_i_6_n_0,input_reset_reg_i_7_n_0,input_reset_reg_i_8_n_0,input_reset_reg_i_9_n_0}),
        .O(NLW_input_reset_reg_reg_i_2_O_UNCONNECTED[3:0]),
        .S({input_reset_reg_i_10_n_0,input_reset_reg_i_11_n_0,input_reset_reg_i_12_n_0,input_reset_reg_i_13_n_0}));
  CARRY4 input_reset_reg_reg_i_23
       (.CI(input_reset_reg_reg_i_41_n_0),
        .CO({input_reset_reg_reg_i_23_n_0,input_reset_reg_reg_i_23_n_1,input_reset_reg_reg_i_23_n_2,input_reset_reg_reg_i_23_n_3}),
        .CYINIT(1'b0),
        .DI({input_reset_reg_i_42_n_0,input_reset_reg_i_43_n_0,input_reset_reg_i_44_n_0,input_reset_reg_i_45_n_0}),
        .O(NLW_input_reset_reg_reg_i_23_O_UNCONNECTED[3:0]),
        .S({input_reset_reg_i_46_n_0,input_reset_reg_i_47_n_0,input_reset_reg_i_48_n_0,input_reset_reg_i_49_n_0}));
  CARRY4 input_reset_reg_reg_i_3
       (.CI(input_reset_reg_reg_i_14_n_0),
        .CO({input_railed0,input_reset_reg_reg_i_3_n_1,input_reset_reg_reg_i_3_n_2,input_reset_reg_reg_i_3_n_3}),
        .CYINIT(1'b0),
        .DI({input_reset_reg_i_15_n_0,input_reset_reg_i_16_n_0,input_reset_reg_i_17_n_0,input_reset_reg_i_18_n_0}),
        .O(NLW_input_reset_reg_reg_i_3_O_UNCONNECTED[3:0]),
        .S({input_reset_reg_i_19_n_0,input_reset_reg_i_20_n_0,input_reset_reg_i_21_n_0,input_reset_reg_i_22_n_0}));
  CARRY4 input_reset_reg_reg_i_32
       (.CI(input_reset_reg_reg_i_50_n_0),
        .CO({input_reset_reg_reg_i_32_n_0,input_reset_reg_reg_i_32_n_1,input_reset_reg_reg_i_32_n_2,input_reset_reg_reg_i_32_n_3}),
        .CYINIT(1'b0),
        .DI({input_reset_reg_i_51_n_0,input_reset_reg_i_52_n_0,input_reset_reg_i_53_n_0,input_reset_reg_i_54_n_0}),
        .O(NLW_input_reset_reg_reg_i_32_O_UNCONNECTED[3:0]),
        .S({input_reset_reg_i_55_n_0,input_reset_reg_i_56_n_0,input_reset_reg_i_57_n_0,input_reset_reg_i_58_n_0}));
  CARRY4 input_reset_reg_reg_i_41
       (.CI(1'b0),
        .CO({input_reset_reg_reg_i_41_n_0,input_reset_reg_reg_i_41_n_1,input_reset_reg_reg_i_41_n_2,input_reset_reg_reg_i_41_n_3}),
        .CYINIT(1'b0),
        .DI({input_reset_reg_i_59_n_0,input_reset_reg_i_60_n_0,input_reset_reg_i_61_n_0,input_reset_reg_i_62_n_0}),
        .O(NLW_input_reset_reg_reg_i_41_O_UNCONNECTED[3:0]),
        .S({input_reset_reg_i_63_n_0,input_reset_reg_i_64_n_0,input_reset_reg_i_65_n_0,input_reset_reg_i_66_n_0}));
  CARRY4 input_reset_reg_reg_i_5
       (.CI(input_reset_reg_reg_i_23_n_0),
        .CO({input_reset_reg_reg_i_5_n_0,input_reset_reg_reg_i_5_n_1,input_reset_reg_reg_i_5_n_2,input_reset_reg_reg_i_5_n_3}),
        .CYINIT(1'b0),
        .DI({input_reset_reg_i_24_n_0,input_reset_reg_i_25_n_0,input_reset_reg_i_26_n_0,input_reset_reg_i_27_n_0}),
        .O(NLW_input_reset_reg_reg_i_5_O_UNCONNECTED[3:0]),
        .S({input_reset_reg_i_28_n_0,input_reset_reg_i_29_n_0,input_reset_reg_i_30_n_0,input_reset_reg_i_31_n_0}));
  CARRY4 input_reset_reg_reg_i_50
       (.CI(1'b0),
        .CO({input_reset_reg_reg_i_50_n_0,input_reset_reg_reg_i_50_n_1,input_reset_reg_reg_i_50_n_2,input_reset_reg_reg_i_50_n_3}),
        .CYINIT(1'b0),
        .DI({input_reset_reg_i_67_n_0,input_reset_reg_i_68_n_0,input_reset_reg_i_69_n_0,input_reset_reg_i_70_n_0}),
        .O(NLW_input_reset_reg_reg_i_50_O_UNCONNECTED[3:0]),
        .S({input_reset_reg_i_71_n_0,input_reset_reg_i_72_n_0,input_reset_reg_i_73_n_0,input_reset_reg_i_74_n_0}));
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \loop_locked_counter_reg[0]_i_1 
       (.I0(input_stable02_in),
        .I1(input_stable0),
        .I2(engage_pid_loop0),
        .I3(engage_pid_loop06_in),
        .I4(\control_reg_reg_n_0_[0] ),
        .O(\loop_locked_counter_reg[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \loop_locked_counter_reg[0]_i_10 
       (.I0(loop_locked_min_reg[26]),
        .I1(loop_locked_min_reg[27]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_10_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \loop_locked_counter_reg[0]_i_11 
       (.I0(loop_locked_min_reg[24]),
        .I1(loop_locked_min_reg[25]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_11_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_12 
       (.I0(loop_locked_min_reg[30]),
        .I1(loop_locked_min_reg[31]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_12_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_13 
       (.I0(loop_locked_min_reg[28]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_min_reg[29]),
        .O(\loop_locked_counter_reg[0]_i_13_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_14 
       (.I0(loop_locked_min_reg[26]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_min_reg[27]),
        .O(\loop_locked_counter_reg[0]_i_14_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_15 
       (.I0(loop_locked_min_reg[24]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_min_reg[25]),
        .O(\loop_locked_counter_reg[0]_i_15_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \loop_locked_counter_reg[0]_i_17 
       (.I0(loop_locked_max_reg[30]),
        .I1(loop_locked_max_reg[31]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_17_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \loop_locked_counter_reg[0]_i_18 
       (.I0(loop_locked_max_reg[28]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_max_reg[29]),
        .O(\loop_locked_counter_reg[0]_i_18_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \loop_locked_counter_reg[0]_i_19 
       (.I0(loop_locked_max_reg[26]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_max_reg[27]),
        .O(\loop_locked_counter_reg[0]_i_19_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \loop_locked_counter_reg[0]_i_2 
       (.I0(\loop_locked_counter_reg_reg[30]_0 ),
        .O(sel));
  LUT3 #(
    .INIT(8'h32)) 
    \loop_locked_counter_reg[0]_i_20 
       (.I0(loop_locked_max_reg[24]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_max_reg[25]),
        .O(\loop_locked_counter_reg[0]_i_20_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_21 
       (.I0(loop_locked_max_reg[30]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_max_reg[31]),
        .O(\loop_locked_counter_reg[0]_i_21_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_22 
       (.I0(loop_locked_max_reg[28]),
        .I1(loop_locked_max_reg[29]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_22_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_23 
       (.I0(loop_locked_max_reg[26]),
        .I1(loop_locked_max_reg[27]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_23_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_24 
       (.I0(loop_locked_max_reg[24]),
        .I1(loop_locked_max_reg[25]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_24_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \loop_locked_counter_reg[0]_i_26 
       (.I0(loop_locked_min_reg[22]),
        .I1(loop_locked_min_reg[23]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_26_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \loop_locked_counter_reg[0]_i_27 
       (.I0(loop_locked_min_reg[20]),
        .I1(loop_locked_min_reg[21]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_27_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \loop_locked_counter_reg[0]_i_28 
       (.I0(loop_locked_min_reg[18]),
        .I1(loop_locked_min_reg[19]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_28_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \loop_locked_counter_reg[0]_i_29 
       (.I0(loop_locked_min_reg[16]),
        .I1(loop_locked_min_reg[17]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_29_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_30 
       (.I0(loop_locked_min_reg[22]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_min_reg[23]),
        .O(\loop_locked_counter_reg[0]_i_30_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_31 
       (.I0(loop_locked_min_reg[20]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_min_reg[21]),
        .O(\loop_locked_counter_reg[0]_i_31_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_32 
       (.I0(loop_locked_min_reg[18]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_min_reg[19]),
        .O(\loop_locked_counter_reg[0]_i_32_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_33 
       (.I0(loop_locked_min_reg[16]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_min_reg[17]),
        .O(\loop_locked_counter_reg[0]_i_33_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \loop_locked_counter_reg[0]_i_35 
       (.I0(loop_locked_max_reg[22]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_max_reg[23]),
        .O(\loop_locked_counter_reg[0]_i_35_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \loop_locked_counter_reg[0]_i_36 
       (.I0(loop_locked_max_reg[20]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_max_reg[21]),
        .O(\loop_locked_counter_reg[0]_i_36_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \loop_locked_counter_reg[0]_i_37 
       (.I0(loop_locked_max_reg[18]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_max_reg[19]),
        .O(\loop_locked_counter_reg[0]_i_37_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \loop_locked_counter_reg[0]_i_38 
       (.I0(loop_locked_max_reg[16]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_max_reg[17]),
        .O(\loop_locked_counter_reg[0]_i_38_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_39 
       (.I0(loop_locked_max_reg[22]),
        .I1(loop_locked_max_reg[23]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_39_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_40 
       (.I0(loop_locked_max_reg[20]),
        .I1(loop_locked_max_reg[21]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_40_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_41 
       (.I0(loop_locked_max_reg[18]),
        .I1(loop_locked_max_reg[19]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_41_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_42 
       (.I0(loop_locked_max_reg[16]),
        .I1(loop_locked_max_reg[17]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_42_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \loop_locked_counter_reg[0]_i_44 
       (.I0(loop_locked_min_reg[14]),
        .I1(loop_locked_min_reg[15]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_44_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_45 
       (.I0(pid_loop_0_input_reg[12]),
        .I1(loop_locked_min_reg[12]),
        .I2(loop_locked_min_reg[13]),
        .I3(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_45_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_46 
       (.I0(pid_loop_0_input_reg[10]),
        .I1(loop_locked_min_reg[10]),
        .I2(loop_locked_min_reg[11]),
        .I3(pid_loop_0_input_reg[11]),
        .O(\loop_locked_counter_reg[0]_i_46_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_47 
       (.I0(pid_loop_0_input_reg[8]),
        .I1(loop_locked_min_reg[8]),
        .I2(loop_locked_min_reg[9]),
        .I3(pid_loop_0_input_reg[9]),
        .O(\loop_locked_counter_reg[0]_i_47_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_48 
       (.I0(loop_locked_min_reg[14]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_min_reg[15]),
        .O(\loop_locked_counter_reg[0]_i_48_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_49 
       (.I0(pid_loop_0_input_reg[12]),
        .I1(loop_locked_min_reg[12]),
        .I2(pid_loop_0_input_reg[13]),
        .I3(loop_locked_min_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_49_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_50 
       (.I0(pid_loop_0_input_reg[10]),
        .I1(loop_locked_min_reg[10]),
        .I2(pid_loop_0_input_reg[11]),
        .I3(loop_locked_min_reg[11]),
        .O(\loop_locked_counter_reg[0]_i_50_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_51 
       (.I0(pid_loop_0_input_reg[8]),
        .I1(loop_locked_min_reg[8]),
        .I2(pid_loop_0_input_reg[9]),
        .I3(loop_locked_min_reg[9]),
        .O(\loop_locked_counter_reg[0]_i_51_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \loop_locked_counter_reg[0]_i_53 
       (.I0(loop_locked_max_reg[14]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_max_reg[15]),
        .O(\loop_locked_counter_reg[0]_i_53_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_54 
       (.I0(loop_locked_max_reg[12]),
        .I1(pid_loop_0_input_reg[12]),
        .I2(pid_loop_0_input_reg[13]),
        .I3(loop_locked_max_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_54_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_55 
       (.I0(loop_locked_max_reg[10]),
        .I1(pid_loop_0_input_reg[10]),
        .I2(pid_loop_0_input_reg[11]),
        .I3(loop_locked_max_reg[11]),
        .O(\loop_locked_counter_reg[0]_i_55_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_56 
       (.I0(loop_locked_max_reg[8]),
        .I1(pid_loop_0_input_reg[8]),
        .I2(pid_loop_0_input_reg[9]),
        .I3(loop_locked_max_reg[9]),
        .O(\loop_locked_counter_reg[0]_i_56_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \loop_locked_counter_reg[0]_i_57 
       (.I0(loop_locked_max_reg[14]),
        .I1(loop_locked_max_reg[15]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_57_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_58 
       (.I0(loop_locked_max_reg[12]),
        .I1(pid_loop_0_input_reg[12]),
        .I2(loop_locked_max_reg[13]),
        .I3(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_58_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_59 
       (.I0(loop_locked_max_reg[10]),
        .I1(pid_loop_0_input_reg[10]),
        .I2(loop_locked_max_reg[11]),
        .I3(pid_loop_0_input_reg[11]),
        .O(\loop_locked_counter_reg[0]_i_59_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \loop_locked_counter_reg[0]_i_6 
       (.I0(loop_locked_counter_reg_reg[0]),
        .O(\loop_locked_counter_reg[0]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_60 
       (.I0(loop_locked_max_reg[8]),
        .I1(pid_loop_0_input_reg[8]),
        .I2(loop_locked_max_reg[9]),
        .I3(pid_loop_0_input_reg[9]),
        .O(\loop_locked_counter_reg[0]_i_60_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_61 
       (.I0(pid_loop_0_input_reg[6]),
        .I1(loop_locked_min_reg[6]),
        .I2(loop_locked_min_reg[7]),
        .I3(pid_loop_0_input_reg[7]),
        .O(\loop_locked_counter_reg[0]_i_61_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_62 
       (.I0(pid_loop_0_input_reg[4]),
        .I1(loop_locked_min_reg[4]),
        .I2(loop_locked_min_reg[5]),
        .I3(pid_loop_0_input_reg[5]),
        .O(\loop_locked_counter_reg[0]_i_62_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_63 
       (.I0(pid_loop_0_input_reg[2]),
        .I1(loop_locked_min_reg[2]),
        .I2(loop_locked_min_reg[3]),
        .I3(pid_loop_0_input_reg[3]),
        .O(\loop_locked_counter_reg[0]_i_63_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_64 
       (.I0(pid_loop_0_input_reg[0]),
        .I1(loop_locked_min_reg[0]),
        .I2(loop_locked_min_reg[1]),
        .I3(pid_loop_0_input_reg[1]),
        .O(\loop_locked_counter_reg[0]_i_64_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_65 
       (.I0(pid_loop_0_input_reg[6]),
        .I1(loop_locked_min_reg[6]),
        .I2(pid_loop_0_input_reg[7]),
        .I3(loop_locked_min_reg[7]),
        .O(\loop_locked_counter_reg[0]_i_65_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_66 
       (.I0(pid_loop_0_input_reg[4]),
        .I1(loop_locked_min_reg[4]),
        .I2(pid_loop_0_input_reg[5]),
        .I3(loop_locked_min_reg[5]),
        .O(\loop_locked_counter_reg[0]_i_66_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_67 
       (.I0(pid_loop_0_input_reg[2]),
        .I1(loop_locked_min_reg[2]),
        .I2(pid_loop_0_input_reg[3]),
        .I3(loop_locked_min_reg[3]),
        .O(\loop_locked_counter_reg[0]_i_67_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_68 
       (.I0(pid_loop_0_input_reg[0]),
        .I1(loop_locked_min_reg[0]),
        .I2(pid_loop_0_input_reg[1]),
        .I3(loop_locked_min_reg[1]),
        .O(\loop_locked_counter_reg[0]_i_68_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_69 
       (.I0(loop_locked_max_reg[6]),
        .I1(pid_loop_0_input_reg[6]),
        .I2(pid_loop_0_input_reg[7]),
        .I3(loop_locked_max_reg[7]),
        .O(\loop_locked_counter_reg[0]_i_69_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_70 
       (.I0(loop_locked_max_reg[4]),
        .I1(pid_loop_0_input_reg[4]),
        .I2(pid_loop_0_input_reg[5]),
        .I3(loop_locked_max_reg[5]),
        .O(\loop_locked_counter_reg[0]_i_70_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_71 
       (.I0(loop_locked_max_reg[2]),
        .I1(pid_loop_0_input_reg[2]),
        .I2(pid_loop_0_input_reg[3]),
        .I3(loop_locked_max_reg[3]),
        .O(\loop_locked_counter_reg[0]_i_71_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \loop_locked_counter_reg[0]_i_72 
       (.I0(loop_locked_max_reg[0]),
        .I1(pid_loop_0_input_reg[0]),
        .I2(pid_loop_0_input_reg[1]),
        .I3(loop_locked_max_reg[1]),
        .O(\loop_locked_counter_reg[0]_i_72_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_73 
       (.I0(loop_locked_max_reg[6]),
        .I1(pid_loop_0_input_reg[6]),
        .I2(loop_locked_max_reg[7]),
        .I3(pid_loop_0_input_reg[7]),
        .O(\loop_locked_counter_reg[0]_i_73_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_74 
       (.I0(loop_locked_max_reg[4]),
        .I1(pid_loop_0_input_reg[4]),
        .I2(loop_locked_max_reg[5]),
        .I3(pid_loop_0_input_reg[5]),
        .O(\loop_locked_counter_reg[0]_i_74_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_75 
       (.I0(loop_locked_max_reg[2]),
        .I1(pid_loop_0_input_reg[2]),
        .I2(loop_locked_max_reg[3]),
        .I3(pid_loop_0_input_reg[3]),
        .O(\loop_locked_counter_reg[0]_i_75_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \loop_locked_counter_reg[0]_i_76 
       (.I0(loop_locked_max_reg[0]),
        .I1(pid_loop_0_input_reg[0]),
        .I2(loop_locked_max_reg[1]),
        .I3(pid_loop_0_input_reg[1]),
        .O(\loop_locked_counter_reg[0]_i_76_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \loop_locked_counter_reg[0]_i_8 
       (.I0(loop_locked_min_reg[30]),
        .I1(pid_loop_0_input_reg[13]),
        .I2(loop_locked_min_reg[31]),
        .O(\loop_locked_counter_reg[0]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \loop_locked_counter_reg[0]_i_9 
       (.I0(loop_locked_min_reg[28]),
        .I1(loop_locked_min_reg[29]),
        .I2(pid_loop_0_input_reg[13]),
        .O(\loop_locked_counter_reg[0]_i_9_n_0 ));
  FDRE \loop_locked_counter_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[0]_i_3_n_7 ),
        .Q(loop_locked_counter_reg_reg[0]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  CARRY4 \loop_locked_counter_reg_reg[0]_i_16 
       (.CI(\loop_locked_counter_reg_reg[0]_i_34_n_0 ),
        .CO({\loop_locked_counter_reg_reg[0]_i_16_n_0 ,\loop_locked_counter_reg_reg[0]_i_16_n_1 ,\loop_locked_counter_reg_reg[0]_i_16_n_2 ,\loop_locked_counter_reg_reg[0]_i_16_n_3 }),
        .CYINIT(1'b0),
        .DI({\loop_locked_counter_reg[0]_i_35_n_0 ,\loop_locked_counter_reg[0]_i_36_n_0 ,\loop_locked_counter_reg[0]_i_37_n_0 ,\loop_locked_counter_reg[0]_i_38_n_0 }),
        .O(\NLW_loop_locked_counter_reg_reg[0]_i_16_O_UNCONNECTED [3:0]),
        .S({\loop_locked_counter_reg[0]_i_39_n_0 ,\loop_locked_counter_reg[0]_i_40_n_0 ,\loop_locked_counter_reg[0]_i_41_n_0 ,\loop_locked_counter_reg[0]_i_42_n_0 }));
  CARRY4 \loop_locked_counter_reg_reg[0]_i_25 
       (.CI(\loop_locked_counter_reg_reg[0]_i_43_n_0 ),
        .CO({\loop_locked_counter_reg_reg[0]_i_25_n_0 ,\loop_locked_counter_reg_reg[0]_i_25_n_1 ,\loop_locked_counter_reg_reg[0]_i_25_n_2 ,\loop_locked_counter_reg_reg[0]_i_25_n_3 }),
        .CYINIT(1'b0),
        .DI({\loop_locked_counter_reg[0]_i_44_n_0 ,\loop_locked_counter_reg[0]_i_45_n_0 ,\loop_locked_counter_reg[0]_i_46_n_0 ,\loop_locked_counter_reg[0]_i_47_n_0 }),
        .O(\NLW_loop_locked_counter_reg_reg[0]_i_25_O_UNCONNECTED [3:0]),
        .S({\loop_locked_counter_reg[0]_i_48_n_0 ,\loop_locked_counter_reg[0]_i_49_n_0 ,\loop_locked_counter_reg[0]_i_50_n_0 ,\loop_locked_counter_reg[0]_i_51_n_0 }));
  CARRY4 \loop_locked_counter_reg_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\loop_locked_counter_reg_reg[0]_i_3_n_0 ,\loop_locked_counter_reg_reg[0]_i_3_n_1 ,\loop_locked_counter_reg_reg[0]_i_3_n_2 ,\loop_locked_counter_reg_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\loop_locked_counter_reg_reg[0]_i_3_n_4 ,\loop_locked_counter_reg_reg[0]_i_3_n_5 ,\loop_locked_counter_reg_reg[0]_i_3_n_6 ,\loop_locked_counter_reg_reg[0]_i_3_n_7 }),
        .S({loop_locked_counter_reg_reg[3:1],\loop_locked_counter_reg[0]_i_6_n_0 }));
  CARRY4 \loop_locked_counter_reg_reg[0]_i_34 
       (.CI(\loop_locked_counter_reg_reg[0]_i_52_n_0 ),
        .CO({\loop_locked_counter_reg_reg[0]_i_34_n_0 ,\loop_locked_counter_reg_reg[0]_i_34_n_1 ,\loop_locked_counter_reg_reg[0]_i_34_n_2 ,\loop_locked_counter_reg_reg[0]_i_34_n_3 }),
        .CYINIT(1'b0),
        .DI({\loop_locked_counter_reg[0]_i_53_n_0 ,\loop_locked_counter_reg[0]_i_54_n_0 ,\loop_locked_counter_reg[0]_i_55_n_0 ,\loop_locked_counter_reg[0]_i_56_n_0 }),
        .O(\NLW_loop_locked_counter_reg_reg[0]_i_34_O_UNCONNECTED [3:0]),
        .S({\loop_locked_counter_reg[0]_i_57_n_0 ,\loop_locked_counter_reg[0]_i_58_n_0 ,\loop_locked_counter_reg[0]_i_59_n_0 ,\loop_locked_counter_reg[0]_i_60_n_0 }));
  CARRY4 \loop_locked_counter_reg_reg[0]_i_4 
       (.CI(\loop_locked_counter_reg_reg[0]_i_7_n_0 ),
        .CO({input_stable02_in,\loop_locked_counter_reg_reg[0]_i_4_n_1 ,\loop_locked_counter_reg_reg[0]_i_4_n_2 ,\loop_locked_counter_reg_reg[0]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({\loop_locked_counter_reg[0]_i_8_n_0 ,\loop_locked_counter_reg[0]_i_9_n_0 ,\loop_locked_counter_reg[0]_i_10_n_0 ,\loop_locked_counter_reg[0]_i_11_n_0 }),
        .O(\NLW_loop_locked_counter_reg_reg[0]_i_4_O_UNCONNECTED [3:0]),
        .S({\loop_locked_counter_reg[0]_i_12_n_0 ,\loop_locked_counter_reg[0]_i_13_n_0 ,\loop_locked_counter_reg[0]_i_14_n_0 ,\loop_locked_counter_reg[0]_i_15_n_0 }));
  CARRY4 \loop_locked_counter_reg_reg[0]_i_43 
       (.CI(1'b0),
        .CO({\loop_locked_counter_reg_reg[0]_i_43_n_0 ,\loop_locked_counter_reg_reg[0]_i_43_n_1 ,\loop_locked_counter_reg_reg[0]_i_43_n_2 ,\loop_locked_counter_reg_reg[0]_i_43_n_3 }),
        .CYINIT(1'b0),
        .DI({\loop_locked_counter_reg[0]_i_61_n_0 ,\loop_locked_counter_reg[0]_i_62_n_0 ,\loop_locked_counter_reg[0]_i_63_n_0 ,\loop_locked_counter_reg[0]_i_64_n_0 }),
        .O(\NLW_loop_locked_counter_reg_reg[0]_i_43_O_UNCONNECTED [3:0]),
        .S({\loop_locked_counter_reg[0]_i_65_n_0 ,\loop_locked_counter_reg[0]_i_66_n_0 ,\loop_locked_counter_reg[0]_i_67_n_0 ,\loop_locked_counter_reg[0]_i_68_n_0 }));
  CARRY4 \loop_locked_counter_reg_reg[0]_i_5 
       (.CI(\loop_locked_counter_reg_reg[0]_i_16_n_0 ),
        .CO({input_stable0,\loop_locked_counter_reg_reg[0]_i_5_n_1 ,\loop_locked_counter_reg_reg[0]_i_5_n_2 ,\loop_locked_counter_reg_reg[0]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({\loop_locked_counter_reg[0]_i_17_n_0 ,\loop_locked_counter_reg[0]_i_18_n_0 ,\loop_locked_counter_reg[0]_i_19_n_0 ,\loop_locked_counter_reg[0]_i_20_n_0 }),
        .O(\NLW_loop_locked_counter_reg_reg[0]_i_5_O_UNCONNECTED [3:0]),
        .S({\loop_locked_counter_reg[0]_i_21_n_0 ,\loop_locked_counter_reg[0]_i_22_n_0 ,\loop_locked_counter_reg[0]_i_23_n_0 ,\loop_locked_counter_reg[0]_i_24_n_0 }));
  CARRY4 \loop_locked_counter_reg_reg[0]_i_52 
       (.CI(1'b0),
        .CO({\loop_locked_counter_reg_reg[0]_i_52_n_0 ,\loop_locked_counter_reg_reg[0]_i_52_n_1 ,\loop_locked_counter_reg_reg[0]_i_52_n_2 ,\loop_locked_counter_reg_reg[0]_i_52_n_3 }),
        .CYINIT(1'b0),
        .DI({\loop_locked_counter_reg[0]_i_69_n_0 ,\loop_locked_counter_reg[0]_i_70_n_0 ,\loop_locked_counter_reg[0]_i_71_n_0 ,\loop_locked_counter_reg[0]_i_72_n_0 }),
        .O(\NLW_loop_locked_counter_reg_reg[0]_i_52_O_UNCONNECTED [3:0]),
        .S({\loop_locked_counter_reg[0]_i_73_n_0 ,\loop_locked_counter_reg[0]_i_74_n_0 ,\loop_locked_counter_reg[0]_i_75_n_0 ,\loop_locked_counter_reg[0]_i_76_n_0 }));
  CARRY4 \loop_locked_counter_reg_reg[0]_i_7 
       (.CI(\loop_locked_counter_reg_reg[0]_i_25_n_0 ),
        .CO({\loop_locked_counter_reg_reg[0]_i_7_n_0 ,\loop_locked_counter_reg_reg[0]_i_7_n_1 ,\loop_locked_counter_reg_reg[0]_i_7_n_2 ,\loop_locked_counter_reg_reg[0]_i_7_n_3 }),
        .CYINIT(1'b0),
        .DI({\loop_locked_counter_reg[0]_i_26_n_0 ,\loop_locked_counter_reg[0]_i_27_n_0 ,\loop_locked_counter_reg[0]_i_28_n_0 ,\loop_locked_counter_reg[0]_i_29_n_0 }),
        .O(\NLW_loop_locked_counter_reg_reg[0]_i_7_O_UNCONNECTED [3:0]),
        .S({\loop_locked_counter_reg[0]_i_30_n_0 ,\loop_locked_counter_reg[0]_i_31_n_0 ,\loop_locked_counter_reg[0]_i_32_n_0 ,\loop_locked_counter_reg[0]_i_33_n_0 }));
  FDRE \loop_locked_counter_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[8]_i_1_n_5 ),
        .Q(loop_locked_counter_reg_reg[10]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[8]_i_1_n_4 ),
        .Q(loop_locked_counter_reg_reg[11]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[12]_i_1_n_7 ),
        .Q(loop_locked_counter_reg_reg[12]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  CARRY4 \loop_locked_counter_reg_reg[12]_i_1 
       (.CI(\loop_locked_counter_reg_reg[8]_i_1_n_0 ),
        .CO({\loop_locked_counter_reg_reg[12]_i_1_n_0 ,\loop_locked_counter_reg_reg[12]_i_1_n_1 ,\loop_locked_counter_reg_reg[12]_i_1_n_2 ,\loop_locked_counter_reg_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\loop_locked_counter_reg_reg[12]_i_1_n_4 ,\loop_locked_counter_reg_reg[12]_i_1_n_5 ,\loop_locked_counter_reg_reg[12]_i_1_n_6 ,\loop_locked_counter_reg_reg[12]_i_1_n_7 }),
        .S(loop_locked_counter_reg_reg[15:12]));
  FDRE \loop_locked_counter_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[12]_i_1_n_6 ),
        .Q(loop_locked_counter_reg_reg[13]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[12]_i_1_n_5 ),
        .Q(loop_locked_counter_reg_reg[14]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[12]_i_1_n_4 ),
        .Q(loop_locked_counter_reg_reg[15]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[16]_i_1_n_7 ),
        .Q(loop_locked_counter_reg_reg[16]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  CARRY4 \loop_locked_counter_reg_reg[16]_i_1 
       (.CI(\loop_locked_counter_reg_reg[12]_i_1_n_0 ),
        .CO({\loop_locked_counter_reg_reg[16]_i_1_n_0 ,\loop_locked_counter_reg_reg[16]_i_1_n_1 ,\loop_locked_counter_reg_reg[16]_i_1_n_2 ,\loop_locked_counter_reg_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\loop_locked_counter_reg_reg[16]_i_1_n_4 ,\loop_locked_counter_reg_reg[16]_i_1_n_5 ,\loop_locked_counter_reg_reg[16]_i_1_n_6 ,\loop_locked_counter_reg_reg[16]_i_1_n_7 }),
        .S(loop_locked_counter_reg_reg[19:16]));
  FDRE \loop_locked_counter_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[16]_i_1_n_6 ),
        .Q(loop_locked_counter_reg_reg[17]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[16]_i_1_n_5 ),
        .Q(loop_locked_counter_reg_reg[18]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[16]_i_1_n_4 ),
        .Q(loop_locked_counter_reg_reg[19]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[0]_i_3_n_6 ),
        .Q(loop_locked_counter_reg_reg[1]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[20]_i_1_n_7 ),
        .Q(loop_locked_counter_reg_reg[20]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  CARRY4 \loop_locked_counter_reg_reg[20]_i_1 
       (.CI(\loop_locked_counter_reg_reg[16]_i_1_n_0 ),
        .CO({\loop_locked_counter_reg_reg[20]_i_1_n_0 ,\loop_locked_counter_reg_reg[20]_i_1_n_1 ,\loop_locked_counter_reg_reg[20]_i_1_n_2 ,\loop_locked_counter_reg_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\loop_locked_counter_reg_reg[20]_i_1_n_4 ,\loop_locked_counter_reg_reg[20]_i_1_n_5 ,\loop_locked_counter_reg_reg[20]_i_1_n_6 ,\loop_locked_counter_reg_reg[20]_i_1_n_7 }),
        .S(loop_locked_counter_reg_reg[23:20]));
  FDRE \loop_locked_counter_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[20]_i_1_n_6 ),
        .Q(loop_locked_counter_reg_reg[21]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[20]_i_1_n_5 ),
        .Q(loop_locked_counter_reg_reg[22]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[20]_i_1_n_4 ),
        .Q(loop_locked_counter_reg_reg[23]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[24]_i_1_n_7 ),
        .Q(loop_locked_counter_reg_reg[24]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  CARRY4 \loop_locked_counter_reg_reg[24]_i_1 
       (.CI(\loop_locked_counter_reg_reg[20]_i_1_n_0 ),
        .CO({\loop_locked_counter_reg_reg[24]_i_1_n_0 ,\loop_locked_counter_reg_reg[24]_i_1_n_1 ,\loop_locked_counter_reg_reg[24]_i_1_n_2 ,\loop_locked_counter_reg_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\loop_locked_counter_reg_reg[24]_i_1_n_4 ,\loop_locked_counter_reg_reg[24]_i_1_n_5 ,\loop_locked_counter_reg_reg[24]_i_1_n_6 ,\loop_locked_counter_reg_reg[24]_i_1_n_7 }),
        .S(loop_locked_counter_reg_reg[27:24]));
  FDRE \loop_locked_counter_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[24]_i_1_n_6 ),
        .Q(loop_locked_counter_reg_reg[25]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[24]_i_1_n_5 ),
        .Q(loop_locked_counter_reg_reg[26]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[24]_i_1_n_4 ),
        .Q(loop_locked_counter_reg_reg[27]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[28]_i_1_n_7 ),
        .Q(loop_locked_counter_reg_reg[28]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  CARRY4 \loop_locked_counter_reg_reg[28]_i_1 
       (.CI(\loop_locked_counter_reg_reg[24]_i_1_n_0 ),
        .CO({\NLW_loop_locked_counter_reg_reg[28]_i_1_CO_UNCONNECTED [3],\loop_locked_counter_reg_reg[28]_i_1_n_1 ,\loop_locked_counter_reg_reg[28]_i_1_n_2 ,\loop_locked_counter_reg_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\loop_locked_counter_reg_reg[28]_i_1_n_4 ,\loop_locked_counter_reg_reg[28]_i_1_n_5 ,\loop_locked_counter_reg_reg[28]_i_1_n_6 ,\loop_locked_counter_reg_reg[28]_i_1_n_7 }),
        .S(loop_locked_counter_reg_reg[31:28]));
  FDRE \loop_locked_counter_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[28]_i_1_n_6 ),
        .Q(loop_locked_counter_reg_reg[29]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[0]_i_3_n_5 ),
        .Q(loop_locked_counter_reg_reg[2]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[28]_i_1_n_5 ),
        .Q(loop_locked_counter_reg_reg[30]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[28]_i_1_n_4 ),
        .Q(loop_locked_counter_reg_reg[31]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[0]_i_3_n_4 ),
        .Q(loop_locked_counter_reg_reg[3]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[4]_i_1_n_7 ),
        .Q(loop_locked_counter_reg_reg[4]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  CARRY4 \loop_locked_counter_reg_reg[4]_i_1 
       (.CI(\loop_locked_counter_reg_reg[0]_i_3_n_0 ),
        .CO({\loop_locked_counter_reg_reg[4]_i_1_n_0 ,\loop_locked_counter_reg_reg[4]_i_1_n_1 ,\loop_locked_counter_reg_reg[4]_i_1_n_2 ,\loop_locked_counter_reg_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\loop_locked_counter_reg_reg[4]_i_1_n_4 ,\loop_locked_counter_reg_reg[4]_i_1_n_5 ,\loop_locked_counter_reg_reg[4]_i_1_n_6 ,\loop_locked_counter_reg_reg[4]_i_1_n_7 }),
        .S(loop_locked_counter_reg_reg[7:4]));
  FDRE \loop_locked_counter_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[4]_i_1_n_6 ),
        .Q(loop_locked_counter_reg_reg[5]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[4]_i_1_n_5 ),
        .Q(loop_locked_counter_reg_reg[6]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[4]_i_1_n_4 ),
        .Q(loop_locked_counter_reg_reg[7]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  FDRE \loop_locked_counter_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[8]_i_1_n_7 ),
        .Q(loop_locked_counter_reg_reg[8]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  CARRY4 \loop_locked_counter_reg_reg[8]_i_1 
       (.CI(\loop_locked_counter_reg_reg[4]_i_1_n_0 ),
        .CO({\loop_locked_counter_reg_reg[8]_i_1_n_0 ,\loop_locked_counter_reg_reg[8]_i_1_n_1 ,\loop_locked_counter_reg_reg[8]_i_1_n_2 ,\loop_locked_counter_reg_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\loop_locked_counter_reg_reg[8]_i_1_n_4 ,\loop_locked_counter_reg_reg[8]_i_1_n_5 ,\loop_locked_counter_reg_reg[8]_i_1_n_6 ,\loop_locked_counter_reg_reg[8]_i_1_n_7 }),
        .S(loop_locked_counter_reg_reg[11:8]));
  FDRE \loop_locked_counter_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\loop_locked_counter_reg_reg[8]_i_1_n_6 ),
        .Q(loop_locked_counter_reg_reg[9]),
        .R(\loop_locked_counter_reg[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_delay_reg[15]_i_1 
       (.I0(\loop_locked_delay_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\loop_locked_delay_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_delay_reg[23]_i_1 
       (.I0(\loop_locked_delay_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\loop_locked_delay_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_delay_reg[31]_i_1 
       (.I0(\loop_locked_delay_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\loop_locked_delay_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \loop_locked_delay_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\loop_locked_delay_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_delay_reg[7]_i_1 
       (.I0(\loop_locked_delay_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\loop_locked_delay_reg[7]_i_1_n_0 ));
  FDRE \loop_locked_delay_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(loop_locked_delay_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(loop_locked_delay_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(loop_locked_delay_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(loop_locked_delay_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(loop_locked_delay_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(loop_locked_delay_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(loop_locked_delay_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(loop_locked_delay_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(loop_locked_delay_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(loop_locked_delay_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(loop_locked_delay_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(loop_locked_delay_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(loop_locked_delay_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(loop_locked_delay_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(loop_locked_delay_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(loop_locked_delay_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(loop_locked_delay_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(loop_locked_delay_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(loop_locked_delay_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(loop_locked_delay_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(loop_locked_delay_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(loop_locked_delay_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(loop_locked_delay_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(loop_locked_delay_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(loop_locked_delay_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(loop_locked_delay_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(loop_locked_delay_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(loop_locked_delay_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(loop_locked_delay_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(loop_locked_delay_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(loop_locked_delay_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_delay_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_delay_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(loop_locked_delay_reg[9]),
        .R(pid_loop_0_rst1));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_max_reg[15]_i_1 
       (.I0(\loop_locked_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\loop_locked_max_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_max_reg[23]_i_1 
       (.I0(\loop_locked_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\loop_locked_max_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_max_reg[31]_i_1 
       (.I0(\loop_locked_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\loop_locked_max_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000020000000)) 
    \loop_locked_max_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\loop_locked_max_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_max_reg[7]_i_1 
       (.I0(\loop_locked_max_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\loop_locked_max_reg[7]_i_1_n_0 ));
  FDRE \loop_locked_max_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(loop_locked_max_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(loop_locked_max_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(loop_locked_max_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(loop_locked_max_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(loop_locked_max_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(loop_locked_max_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(loop_locked_max_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(loop_locked_max_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(loop_locked_max_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(loop_locked_max_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(loop_locked_max_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(loop_locked_max_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(loop_locked_max_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(loop_locked_max_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(loop_locked_max_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(loop_locked_max_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(loop_locked_max_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(loop_locked_max_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(loop_locked_max_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(loop_locked_max_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(loop_locked_max_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(loop_locked_max_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(loop_locked_max_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(loop_locked_max_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(loop_locked_max_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(loop_locked_max_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(loop_locked_max_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(loop_locked_max_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(loop_locked_max_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(loop_locked_max_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(loop_locked_max_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_max_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_max_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(loop_locked_max_reg[9]),
        .R(pid_loop_0_rst1));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_min_reg[15]_i_1 
       (.I0(\loop_locked_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\loop_locked_min_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_min_reg[23]_i_1 
       (.I0(\loop_locked_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\loop_locked_min_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_min_reg[31]_i_1 
       (.I0(\loop_locked_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\loop_locked_min_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \loop_locked_min_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\loop_locked_min_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \loop_locked_min_reg[7]_i_1 
       (.I0(\loop_locked_min_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\loop_locked_min_reg[7]_i_1_n_0 ));
  FDRE \loop_locked_min_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(loop_locked_min_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(loop_locked_min_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(loop_locked_min_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(loop_locked_min_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(loop_locked_min_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(loop_locked_min_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(loop_locked_min_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(loop_locked_min_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(loop_locked_min_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(loop_locked_min_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(loop_locked_min_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(loop_locked_min_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(loop_locked_min_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(loop_locked_min_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(loop_locked_min_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(loop_locked_min_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(loop_locked_min_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(loop_locked_min_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(loop_locked_min_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(loop_locked_min_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(loop_locked_min_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(loop_locked_min_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(loop_locked_min_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(loop_locked_min_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(loop_locked_min_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(loop_locked_min_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(loop_locked_min_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(loop_locked_min_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(loop_locked_min_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(loop_locked_min_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(loop_locked_min_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \loop_locked_min_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\loop_locked_min_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(loop_locked_min_reg[9]),
        .R(pid_loop_0_rst1));
  FDRE \loop_output_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[0]),
        .Q(loop_output_reg[0]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[10]),
        .Q(loop_output_reg[10]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[11]),
        .Q(loop_output_reg[11]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[12]),
        .Q(loop_output_reg[12]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[1]),
        .Q(loop_output_reg[1]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[2]),
        .Q(loop_output_reg[2]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[13]),
        .Q(loop_output_reg[31]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[3]),
        .Q(loop_output_reg[3]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[4]),
        .Q(loop_output_reg[4]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[5]),
        .Q(loop_output_reg[5]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[6]),
        .Q(loop_output_reg[6]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[7]),
        .Q(loop_output_reg[7]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[8]),
        .Q(loop_output_reg[8]),
        .R(1'b0));
  FDRE \loop_output_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_output[9]),
        .Q(loop_output_reg[9]),
        .R(1'b0));
  FDRE \maxdex_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[0]),
        .Q(\maxdex_reg_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[10]),
        .Q(\maxdex_reg_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[11]),
        .Q(\maxdex_reg_reg_n_0_[11] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[12]),
        .Q(\maxdex_reg_reg_n_0_[12] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[13]),
        .Q(\maxdex_reg_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[0]),
        .Q(\maxdex_reg_reg_n_0_[16] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[1]),
        .Q(\maxdex_reg_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[2]),
        .Q(\maxdex_reg_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[3]),
        .Q(\maxdex_reg_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[1]),
        .Q(\maxdex_reg_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[4]),
        .Q(\maxdex_reg_reg_n_0_[20] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[5]),
        .Q(\maxdex_reg_reg_n_0_[21] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[6]),
        .Q(\maxdex_reg_reg_n_0_[22] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[7]),
        .Q(\maxdex_reg_reg_n_0_[23] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[8]),
        .Q(\maxdex_reg_reg_n_0_[24] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[9]),
        .Q(\maxdex_reg_reg_n_0_[25] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[10]),
        .Q(\maxdex_reg_reg_n_0_[26] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[11]),
        .Q(\maxdex_reg_reg_n_0_[27] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[12]),
        .Q(\maxdex_reg_reg_n_0_[28] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[2]),
        .Q(\maxdex_reg_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_max[13]),
        .Q(\maxdex_reg_reg_n_0_[31] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[3]),
        .Q(\maxdex_reg_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[4]),
        .Q(\maxdex_reg_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[5]),
        .Q(\maxdex_reg_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[6]),
        .Q(\maxdex_reg_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[7]),
        .Q(\maxdex_reg_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[8]),
        .Q(\maxdex_reg_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \maxdex_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_maxdex[9]),
        .Q(\maxdex_reg_reg_n_0_[9] ),
        .R(1'b0));
  FDRE \mindex_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[0] ),
        .Q(mindex_reg[0]),
        .R(1'b0));
  FDRE \mindex_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[10] ),
        .Q(mindex_reg[10]),
        .R(1'b0));
  FDRE \mindex_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[11] ),
        .Q(mindex_reg[11]),
        .R(1'b0));
  FDRE \mindex_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[12] ),
        .Q(mindex_reg[12]),
        .R(1'b0));
  FDRE \mindex_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(p_0_in0),
        .Q(mindex_reg[15]),
        .R(1'b0));
  FDRE \mindex_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[0]),
        .Q(mindex_reg[16]),
        .R(1'b0));
  FDRE \mindex_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[1]),
        .Q(mindex_reg[17]),
        .R(1'b0));
  FDRE \mindex_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[2]),
        .Q(mindex_reg[18]),
        .R(1'b0));
  FDRE \mindex_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[3]),
        .Q(mindex_reg[19]),
        .R(1'b0));
  FDRE \mindex_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[1] ),
        .Q(mindex_reg[1]),
        .R(1'b0));
  FDRE \mindex_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[4]),
        .Q(mindex_reg[20]),
        .R(1'b0));
  FDRE \mindex_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[5]),
        .Q(mindex_reg[21]),
        .R(1'b0));
  FDRE \mindex_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[6]),
        .Q(mindex_reg[22]),
        .R(1'b0));
  FDRE \mindex_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[7]),
        .Q(mindex_reg[23]),
        .R(1'b0));
  FDRE \mindex_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[8]),
        .Q(mindex_reg[24]),
        .R(1'b0));
  FDRE \mindex_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[9]),
        .Q(mindex_reg[25]),
        .R(1'b0));
  FDRE \mindex_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[10]),
        .Q(mindex_reg[26]),
        .R(1'b0));
  FDRE \mindex_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[11]),
        .Q(mindex_reg[27]),
        .R(1'b0));
  FDRE \mindex_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[12]),
        .Q(mindex_reg[28]),
        .R(1'b0));
  FDRE \mindex_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[2] ),
        .Q(mindex_reg[2]),
        .R(1'b0));
  FDRE \mindex_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(autolock_input_min[13]),
        .Q(mindex_reg[31]),
        .R(1'b0));
  FDRE \mindex_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[3] ),
        .Q(mindex_reg[3]),
        .R(1'b0));
  FDRE \mindex_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[4] ),
        .Q(mindex_reg[4]),
        .R(1'b0));
  FDRE \mindex_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[5] ),
        .Q(mindex_reg[5]),
        .R(1'b0));
  FDRE \mindex_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[6] ),
        .Q(mindex_reg[6]),
        .R(1'b0));
  FDRE \mindex_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[7] ),
        .Q(mindex_reg[7]),
        .R(1'b0));
  FDRE \mindex_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[8] ),
        .Q(mindex_reg[8]),
        .R(1'b0));
  FDRE \mindex_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(maxdex_reg),
        .D(\autolock_input_mindex_reg_n_0_[9] ),
        .Q(mindex_reg[9]),
        .R(1'b0));
  system_AXI_PI_0_2_pid_loop pid_loop_0
       (.CO(engage_pid_loop06_in),
        .D(P_term_mon),
        .I_term_mon(I_term_mon),
        .\I_term_q_reg[0]_0 ({autolock,ramp_enable,p_0_in1_in}),
        .\I_term_q_reg[30]_0 (pid_loop_0_n_35),
        .\I_term_q_reg[31]_0 (p_0_in_0),
        .P_error_unshifted_reg_reg_0(P_reg),
        .Q(I_reg),
        .\autolock_input_maxdex_reg[0] (\loop_locked_counter_reg_reg[30]_0 ),
        .\autolock_input_maxdex_reg[13] (ramp_module_0_output),
        .\autolock_max_reg_reg[30] (engage_pid_loop0),
        .\axi_pi_output_reg_reg[13]_inv_i_3_0 (autolock_0_input),
        .\axi_pi_output_reg_reg[13]_inv_i_3_1 (autolock_max_reg),
        .\axi_pi_output_reg_reg[13]_inv_i_4_0 (autolock_min_reg),
        .\control_reg_reg[1] (pid_loop_0_n_0),
        .\error_reg_reg[13]_0 (pid_loop_0_error),
        .\loop_output_reg_reg[31]_0 (pid_loop_0_output),
        .\loop_output_reg_reg[31]_1 (autolock_input_maxdex0_in),
        .pid_loop_0_input_reg(pid_loop_0_input_reg),
        .ramp_module_0_reset(ramp_module_0_reset),
        .\ramplitude_min_reg_reg[31]_i_6_0 (ramp_offset_reg[31:18]),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .\setpoint_reg_reg[13]_0 (setpoint_reg));
  FDRE \pid_loop_0_input_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[0]),
        .Q(pid_loop_0_input_reg[0]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[10]),
        .Q(pid_loop_0_input_reg[10]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[11]),
        .Q(pid_loop_0_input_reg[11]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[12]),
        .Q(pid_loop_0_input_reg[12]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_input),
        .Q(pid_loop_0_input_reg[13]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[1]),
        .Q(pid_loop_0_input_reg[1]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[2]),
        .Q(pid_loop_0_input_reg[2]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[3]),
        .Q(pid_loop_0_input_reg[3]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[4]),
        .Q(pid_loop_0_input_reg[4]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[5]),
        .Q(pid_loop_0_input_reg[5]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[6]),
        .Q(pid_loop_0_input_reg[6]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[7]),
        .Q(pid_loop_0_input_reg[7]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[8]),
        .Q(pid_loop_0_input_reg[8]),
        .R(1'b0));
  FDRE \pid_loop_0_input_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_input[9]),
        .Q(pid_loop_0_input_reg[9]),
        .R(1'b0));
  system_AXI_PI_0_2_ramp_module ramp_module_0
       (.CO(engage_pid_loop06_in),
        .D({autolock,ramp_enable,pid_loop_0_n_0,\control_reg_reg_n_0_[0] }),
        .E(maxdex_reg),
        .\I_term_q_reg[31] (pid_loop_0_n_35),
        .Q(\control_reg_reg_n_0_[9] ),
        .\autolock_input_maxdex_reg[0] (\autolock_input_max_reg[13]_i_2_n_1 ),
        .\autolock_input_mindex_reg[0] (\autolock_input_min_reg[13]_i_2_n_1 ),
        .\axi_pi_output_reg_reg[0] (engage_pid_loop0),
        .\axi_pi_output_reg_reg[13]_inv (pid_loop_0_output),
        .loop_locked_counter_reg_reg(loop_locked_counter_reg_reg),
        .\loop_locked_counter_reg_reg[30] (\loop_locked_counter_reg_reg[30]_0 ),
        .ramp_corner_reg_reg_0(autolock_input_mindex),
        .ramp_corner_reg_reg_1(ramp_module_0_n_33),
        .ramp_module_0_reset(ramp_module_0_reset),
        .ramp_module_0_rst(ramp_module_0_rst),
        .ramp_output(ramp_module_0_output),
        .\ramp_reg_reg[17]_0 (ramp_offset_reg[17:0]),
        .\ramp_reg_reg[30]_0 ({ramp_module_0_n_15,ramp_module_0_n_16,ramp_module_0_n_17,ramp_module_0_n_18,ramp_module_0_n_19,ramp_module_0_n_20,ramp_module_0_n_21,ramp_module_0_n_22,ramp_module_0_n_23,ramp_module_0_n_24,ramp_module_0_n_25,ramp_module_0_n_26,ramp_module_0_n_27}),
        .\ramp_reg_reg[31]_0 (p_0_in_0),
        .\ramp_reg_reg[31]_1 (ramp_module_0_n_28),
        .\ramp_step_reg_reg[31]_0 (ramp_step_reg),
        .\ramplitude_lim_reg_reg[31]_0 (ramplitude_reg),
        .\ramplitude_step_reg_reg[31]_0 (ramplitude_step_reg),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .\status_reg_reg[10] (loop_locked_delay_reg));
  LUT2 #(
    .INIT(4'h8)) 
    \ramp_offset_reg[15]_i_1 
       (.I0(\ramp_offset_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\ramp_offset_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramp_offset_reg[23]_i_1 
       (.I0(\ramp_offset_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\ramp_offset_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramp_offset_reg[31]_i_1 
       (.I0(\ramp_offset_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\ramp_offset_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \ramp_offset_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\ramp_offset_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramp_offset_reg[7]_i_1 
       (.I0(\ramp_offset_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\ramp_offset_reg[7]_i_1_n_0 ));
  FDRE \ramp_offset_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(ramp_offset_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(ramp_offset_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(ramp_offset_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(ramp_offset_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(ramp_offset_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(ramp_offset_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(ramp_offset_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(ramp_offset_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(ramp_offset_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(ramp_offset_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(ramp_offset_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(ramp_offset_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(ramp_offset_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(ramp_offset_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(ramp_offset_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(ramp_offset_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(ramp_offset_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(ramp_offset_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(ramp_offset_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(ramp_offset_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(ramp_offset_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(ramp_offset_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(ramp_offset_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(ramp_offset_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(ramp_offset_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(ramp_offset_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(ramp_offset_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(ramp_offset_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(ramp_offset_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(ramp_offset_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(ramp_offset_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_offset_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\ramp_offset_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(ramp_offset_reg[9]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_output_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[0]),
        .Q(ramp_output_reg[0]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[10]),
        .Q(ramp_output_reg[10]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[11]),
        .Q(ramp_output_reg[11]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[12]),
        .Q(ramp_output_reg[12]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[1]),
        .Q(ramp_output_reg[1]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[2]),
        .Q(ramp_output_reg[2]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[13]),
        .Q(ramp_output_reg[31]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[3]),
        .Q(ramp_output_reg[3]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[4]),
        .Q(ramp_output_reg[4]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[5]),
        .Q(ramp_output_reg[5]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[6]),
        .Q(ramp_output_reg[6]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[7]),
        .Q(ramp_output_reg[7]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[8]),
        .Q(ramp_output_reg[8]),
        .R(1'b0));
  FDRE \ramp_output_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_output[9]),
        .Q(ramp_output_reg[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \ramp_step_reg[15]_i_1 
       (.I0(\ramp_step_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\ramp_step_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramp_step_reg[23]_i_1 
       (.I0(\ramp_step_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\ramp_step_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramp_step_reg[31]_i_1 
       (.I0(\ramp_step_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\ramp_step_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    \ramp_step_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\ramp_step_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramp_step_reg[7]_i_1 
       (.I0(\ramp_step_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\ramp_step_reg[7]_i_1_n_0 ));
  FDRE \ramp_step_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(ramp_step_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(ramp_step_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(ramp_step_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(ramp_step_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(ramp_step_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(ramp_step_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(ramp_step_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(ramp_step_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(ramp_step_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(ramp_step_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(ramp_step_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(ramp_step_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(ramp_step_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(ramp_step_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(ramp_step_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(ramp_step_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(ramp_step_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(ramp_step_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(ramp_step_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(ramp_step_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(ramp_step_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(ramp_step_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(ramp_step_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(ramp_step_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(ramp_step_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(ramp_step_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(ramp_step_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(ramp_step_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(ramp_step_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(ramp_step_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(ramp_step_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \ramp_step_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\ramp_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(ramp_step_reg[9]),
        .R(pid_loop_0_rst1));
  LUT2 #(
    .INIT(4'h8)) 
    \ramplitude_reg[15]_i_1 
       (.I0(\ramplitude_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\ramplitude_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramplitude_reg[23]_i_1 
       (.I0(\ramplitude_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\ramplitude_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramplitude_reg[31]_i_1 
       (.I0(\ramplitude_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\ramplitude_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000100000000000)) 
    \ramplitude_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\ramplitude_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramplitude_reg[7]_i_1 
       (.I0(\ramplitude_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\ramplitude_reg[7]_i_1_n_0 ));
  FDRE \ramplitude_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(ramplitude_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(ramplitude_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(ramplitude_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(ramplitude_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(ramplitude_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(ramplitude_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(ramplitude_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(ramplitude_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(ramplitude_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(ramplitude_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(ramplitude_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(ramplitude_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(ramplitude_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(ramplitude_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(ramplitude_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(ramplitude_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(ramplitude_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(ramplitude_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(ramplitude_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(ramplitude_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(ramplitude_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(ramplitude_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(ramplitude_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(ramplitude_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(ramplitude_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(ramplitude_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(ramplitude_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(ramplitude_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(ramplitude_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(ramplitude_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(ramplitude_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(ramplitude_reg[9]),
        .R(pid_loop_0_rst1));
  LUT2 #(
    .INIT(4'h8)) 
    \ramplitude_step_reg[15]_i_1 
       (.I0(\ramplitude_step_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\ramplitude_step_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramplitude_step_reg[23]_i_1 
       (.I0(\ramplitude_step_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\ramplitude_step_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramplitude_step_reg[31]_i_1 
       (.I0(\ramplitude_step_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\ramplitude_step_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \ramplitude_step_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\ramplitude_step_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \ramplitude_step_reg[7]_i_1 
       (.I0(\ramplitude_step_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\ramplitude_step_reg[7]_i_1_n_0 ));
  FDRE \ramplitude_step_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(ramplitude_step_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(ramplitude_step_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(ramplitude_step_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(ramplitude_step_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(ramplitude_step_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(ramplitude_step_reg[14]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(ramplitude_step_reg[15]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(ramplitude_step_reg[16]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(ramplitude_step_reg[17]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(ramplitude_step_reg[18]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(ramplitude_step_reg[19]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(ramplitude_step_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(ramplitude_step_reg[20]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(ramplitude_step_reg[21]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(ramplitude_step_reg[22]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(ramplitude_step_reg[23]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(ramplitude_step_reg[24]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(ramplitude_step_reg[25]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(ramplitude_step_reg[26]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(ramplitude_step_reg[27]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(ramplitude_step_reg[28]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(ramplitude_step_reg[29]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(ramplitude_step_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(ramplitude_step_reg[30]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(ramplitude_step_reg[31]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(ramplitude_step_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(ramplitude_step_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(ramplitude_step_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(ramplitude_step_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(ramplitude_step_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(ramplitude_step_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \ramplitude_step_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_step_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(ramplitude_step_reg[9]),
        .R(pid_loop_0_rst1));
  LUT2 #(
    .INIT(4'h8)) 
    \setpoint_reg[15]_i_1 
       (.I0(\setpoint_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[1]),
        .O(\setpoint_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \setpoint_reg[23]_i_1 
       (.I0(\setpoint_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[2]),
        .O(\setpoint_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \setpoint_reg[31]_i_1 
       (.I0(\setpoint_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[3]),
        .O(\setpoint_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \setpoint_reg[31]_i_2 
       (.I0(p_0_in[1]),
        .I1(p_0_in[3]),
        .I2(\control_reg[31]_i_3_n_0 ),
        .I3(p_0_in[4]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\setpoint_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \setpoint_reg[7]_i_1 
       (.I0(\setpoint_reg[31]_i_2_n_0 ),
        .I1(s00_axi_wstrb[0]),
        .O(\setpoint_reg[7]_i_1_n_0 ));
  FDRE \setpoint_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(setpoint_reg[0]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(setpoint_reg[10]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(setpoint_reg[11]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(setpoint_reg[12]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(setpoint_reg[13]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(setpoint_reg__0[14]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(setpoint_reg__0[15]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(setpoint_reg__0[16]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(setpoint_reg__0[17]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(setpoint_reg__0[18]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(setpoint_reg__0[19]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(setpoint_reg[1]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(setpoint_reg__0[20]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(setpoint_reg__0[21]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(setpoint_reg__0[22]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(setpoint_reg__0[23]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(setpoint_reg__0[24]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(setpoint_reg__0[25]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(setpoint_reg__0[26]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(setpoint_reg__0[27]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(setpoint_reg__0[28]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(setpoint_reg__0[29]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(setpoint_reg[2]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(setpoint_reg__0[30]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(setpoint_reg__0[31]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(setpoint_reg[3]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(setpoint_reg[4]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(setpoint_reg[5]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(setpoint_reg[6]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(setpoint_reg[7]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(setpoint_reg[8]),
        .R(pid_loop_0_rst1));
  FDRE \setpoint_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\setpoint_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(setpoint_reg[9]),
        .R(pid_loop_0_rst1));
  LUT2 #(
    .INIT(4'h2)) 
    \status_reg[11]_i_1 
       (.I0(autolock),
        .I1(ramp_enable),
        .O(p_7_in));
  FDRE \status_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\control_reg_reg_n_0_[0] ),
        .Q(status_reg[0]),
        .R(1'b0));
  FDRE \status_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_locked_counter_reg_reg[30]_0 ),
        .Q(status_reg[10]),
        .R(1'b0));
  FDRE \status_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_7_in),
        .Q(status_reg[11]),
        .R(1'b0));
  FDRE \status_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pid_loop_0_n_0),
        .Q(status_reg[1]),
        .R(1'b0));
  FDRE \status_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_enable),
        .Q(status_reg[4]),
        .R(1'b0));
  FDRE \status_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(autolock),
        .Q(status_reg[8]),
        .R(1'b0));
  FDRE \status_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_module_0_rst),
        .Q(status_reg[9]),
        .R(1'b0));
endmodule

module system_AXI_PI_0_2_pid_loop
   (\control_reg_reg[1] ,
    D,
    CO,
    \autolock_max_reg_reg[30] ,
    \I_term_q_reg[30]_0 ,
    I_term_mon,
    \loop_output_reg_reg[31]_0 ,
    \loop_output_reg_reg[31]_1 ,
    ramp_module_0_reset,
    \error_reg_reg[13]_0 ,
    s00_axi_aclk,
    Q,
    P_error_unshifted_reg_reg_0,
    \I_term_q_reg[0]_0 ,
    s00_axi_aresetn,
    pid_loop_0_input_reg,
    \I_term_q_reg[31]_0 ,
    \autolock_input_maxdex_reg[13] ,
    \axi_pi_output_reg_reg[13]_inv_i_3_0 ,
    \axi_pi_output_reg_reg[13]_inv_i_4_0 ,
    \axi_pi_output_reg_reg[13]_inv_i_3_1 ,
    \ramplitude_min_reg_reg[31]_i_6_0 ,
    \autolock_input_maxdex_reg[0] ,
    \setpoint_reg_reg[13]_0 );
  output \control_reg_reg[1] ;
  output [31:0]D;
  output [0:0]CO;
  output [0:0]\autolock_max_reg_reg[30] ;
  output [0:0]\I_term_q_reg[30]_0 ;
  output [31:0]I_term_mon;
  output [13:0]\loop_output_reg_reg[31]_0 ;
  output [13:0]\loop_output_reg_reg[31]_1 ;
  output [13:0]ramp_module_0_reset;
  output [13:0]\error_reg_reg[13]_0 ;
  input s00_axi_aclk;
  input [31:0]Q;
  input [31:0]P_error_unshifted_reg_reg_0;
  input [2:0]\I_term_q_reg[0]_0 ;
  input s00_axi_aresetn;
  input [13:0]pid_loop_0_input_reg;
  input [0:0]\I_term_q_reg[31]_0 ;
  input [13:0]\autolock_input_maxdex_reg[13] ;
  input [13:0]\axi_pi_output_reg_reg[13]_inv_i_3_0 ;
  input [31:0]\axi_pi_output_reg_reg[13]_inv_i_4_0 ;
  input [31:0]\axi_pi_output_reg_reg[13]_inv_i_3_1 ;
  input [13:0]\ramplitude_min_reg_reg[31]_i_6_0 ;
  input [0:0]\autolock_input_maxdex_reg[0] ;
  input [13:0]\setpoint_reg_reg[13]_0 ;

  wire [0:0]CO;
  wire [31:0]D;
  wire [31:0]I_error;
  wire I_error_unshifted_reg0_n_100;
  wire I_error_unshifted_reg0_n_101;
  wire I_error_unshifted_reg0_n_102;
  wire I_error_unshifted_reg0_n_103;
  wire I_error_unshifted_reg0_n_104;
  wire I_error_unshifted_reg0_n_105;
  wire I_error_unshifted_reg0_n_106;
  wire I_error_unshifted_reg0_n_107;
  wire I_error_unshifted_reg0_n_108;
  wire I_error_unshifted_reg0_n_109;
  wire I_error_unshifted_reg0_n_110;
  wire I_error_unshifted_reg0_n_111;
  wire I_error_unshifted_reg0_n_112;
  wire I_error_unshifted_reg0_n_113;
  wire I_error_unshifted_reg0_n_114;
  wire I_error_unshifted_reg0_n_115;
  wire I_error_unshifted_reg0_n_116;
  wire I_error_unshifted_reg0_n_117;
  wire I_error_unshifted_reg0_n_118;
  wire I_error_unshifted_reg0_n_119;
  wire I_error_unshifted_reg0_n_120;
  wire I_error_unshifted_reg0_n_121;
  wire I_error_unshifted_reg0_n_122;
  wire I_error_unshifted_reg0_n_123;
  wire I_error_unshifted_reg0_n_124;
  wire I_error_unshifted_reg0_n_125;
  wire I_error_unshifted_reg0_n_126;
  wire I_error_unshifted_reg0_n_127;
  wire I_error_unshifted_reg0_n_128;
  wire I_error_unshifted_reg0_n_129;
  wire I_error_unshifted_reg0_n_130;
  wire I_error_unshifted_reg0_n_131;
  wire I_error_unshifted_reg0_n_132;
  wire I_error_unshifted_reg0_n_133;
  wire I_error_unshifted_reg0_n_134;
  wire I_error_unshifted_reg0_n_135;
  wire I_error_unshifted_reg0_n_136;
  wire I_error_unshifted_reg0_n_137;
  wire I_error_unshifted_reg0_n_138;
  wire I_error_unshifted_reg0_n_139;
  wire I_error_unshifted_reg0_n_140;
  wire I_error_unshifted_reg0_n_141;
  wire I_error_unshifted_reg0_n_142;
  wire I_error_unshifted_reg0_n_143;
  wire I_error_unshifted_reg0_n_144;
  wire I_error_unshifted_reg0_n_145;
  wire I_error_unshifted_reg0_n_146;
  wire I_error_unshifted_reg0_n_147;
  wire I_error_unshifted_reg0_n_148;
  wire I_error_unshifted_reg0_n_149;
  wire I_error_unshifted_reg0_n_150;
  wire I_error_unshifted_reg0_n_151;
  wire I_error_unshifted_reg0_n_152;
  wire I_error_unshifted_reg0_n_153;
  wire I_error_unshifted_reg0_n_58;
  wire I_error_unshifted_reg0_n_59;
  wire I_error_unshifted_reg0_n_60;
  wire I_error_unshifted_reg0_n_61;
  wire I_error_unshifted_reg0_n_62;
  wire I_error_unshifted_reg0_n_63;
  wire I_error_unshifted_reg0_n_64;
  wire I_error_unshifted_reg0_n_65;
  wire I_error_unshifted_reg0_n_66;
  wire I_error_unshifted_reg0_n_67;
  wire I_error_unshifted_reg0_n_68;
  wire I_error_unshifted_reg0_n_69;
  wire I_error_unshifted_reg0_n_70;
  wire I_error_unshifted_reg0_n_71;
  wire I_error_unshifted_reg0_n_72;
  wire I_error_unshifted_reg0_n_73;
  wire I_error_unshifted_reg0_n_74;
  wire I_error_unshifted_reg0_n_75;
  wire I_error_unshifted_reg0_n_76;
  wire I_error_unshifted_reg0_n_77;
  wire I_error_unshifted_reg0_n_78;
  wire I_error_unshifted_reg0_n_79;
  wire I_error_unshifted_reg0_n_80;
  wire I_error_unshifted_reg0_n_81;
  wire I_error_unshifted_reg0_n_82;
  wire I_error_unshifted_reg0_n_83;
  wire I_error_unshifted_reg0_n_84;
  wire I_error_unshifted_reg0_n_85;
  wire I_error_unshifted_reg0_n_86;
  wire I_error_unshifted_reg0_n_87;
  wire I_error_unshifted_reg0_n_88;
  wire I_error_unshifted_reg0_n_89;
  wire I_error_unshifted_reg0_n_90;
  wire I_error_unshifted_reg0_n_91;
  wire I_error_unshifted_reg0_n_92;
  wire I_error_unshifted_reg0_n_93;
  wire I_error_unshifted_reg0_n_94;
  wire I_error_unshifted_reg0_n_95;
  wire I_error_unshifted_reg0_n_96;
  wire I_error_unshifted_reg0_n_97;
  wire I_error_unshifted_reg0_n_98;
  wire I_error_unshifted_reg0_n_99;
  wire I_error_unshifted_reg_reg_n_58;
  wire I_error_unshifted_reg_reg_n_59;
  wire I_error_unshifted_reg_reg_n_60;
  wire I_error_unshifted_reg_reg_n_61;
  wire I_error_unshifted_reg_reg_n_62;
  wire I_error_unshifted_reg_reg_n_63;
  wire I_error_unshifted_reg_reg_n_64;
  wire I_error_unshifted_reg_reg_n_65;
  wire I_error_unshifted_reg_reg_n_66;
  wire I_error_unshifted_reg_reg_n_67;
  wire I_error_unshifted_reg_reg_n_68;
  wire I_error_unshifted_reg_reg_n_69;
  wire I_error_unshifted_reg_reg_n_70;
  wire I_error_unshifted_reg_reg_n_71;
  wire I_error_unshifted_reg_reg_n_72;
  wire I_error_unshifted_reg_reg_n_73;
  wire I_error_unshifted_reg_reg_n_74;
  wire I_error_unshifted_reg_reg_n_75;
  wire I_error_unshifted_reg_reg_n_76;
  wire \I_mon_reg[11]_i_2_n_0 ;
  wire \I_mon_reg[11]_i_3_n_0 ;
  wire \I_mon_reg[11]_i_4_n_0 ;
  wire \I_mon_reg[11]_i_5_n_0 ;
  wire \I_mon_reg[15]_i_2_n_0 ;
  wire \I_mon_reg[15]_i_3_n_0 ;
  wire \I_mon_reg[15]_i_4_n_0 ;
  wire \I_mon_reg[15]_i_5_n_0 ;
  wire \I_mon_reg[19]_i_2_n_0 ;
  wire \I_mon_reg[19]_i_3_n_0 ;
  wire \I_mon_reg[19]_i_4_n_0 ;
  wire \I_mon_reg[19]_i_5_n_0 ;
  wire \I_mon_reg[23]_i_2_n_0 ;
  wire \I_mon_reg[23]_i_3_n_0 ;
  wire \I_mon_reg[23]_i_4_n_0 ;
  wire \I_mon_reg[23]_i_5_n_0 ;
  wire \I_mon_reg[27]_i_2_n_0 ;
  wire \I_mon_reg[27]_i_3_n_0 ;
  wire \I_mon_reg[27]_i_4_n_0 ;
  wire \I_mon_reg[27]_i_5_n_0 ;
  wire \I_mon_reg[31]_i_2_n_0 ;
  wire \I_mon_reg[31]_i_3_n_0 ;
  wire \I_mon_reg[31]_i_4_n_0 ;
  wire \I_mon_reg[31]_i_5_n_0 ;
  wire \I_mon_reg[31]_i_6_n_0 ;
  wire \I_mon_reg[3]_i_2_n_0 ;
  wire \I_mon_reg[3]_i_3_n_0 ;
  wire \I_mon_reg[3]_i_4_n_0 ;
  wire \I_mon_reg[3]_i_5_n_0 ;
  wire \I_mon_reg[7]_i_2_n_0 ;
  wire \I_mon_reg[7]_i_3_n_0 ;
  wire \I_mon_reg[7]_i_4_n_0 ;
  wire \I_mon_reg[7]_i_5_n_0 ;
  wire \I_mon_reg_reg[11]_i_1_n_0 ;
  wire \I_mon_reg_reg[11]_i_1_n_1 ;
  wire \I_mon_reg_reg[11]_i_1_n_2 ;
  wire \I_mon_reg_reg[11]_i_1_n_3 ;
  wire \I_mon_reg_reg[15]_i_1_n_0 ;
  wire \I_mon_reg_reg[15]_i_1_n_1 ;
  wire \I_mon_reg_reg[15]_i_1_n_2 ;
  wire \I_mon_reg_reg[15]_i_1_n_3 ;
  wire \I_mon_reg_reg[19]_i_1_n_0 ;
  wire \I_mon_reg_reg[19]_i_1_n_1 ;
  wire \I_mon_reg_reg[19]_i_1_n_2 ;
  wire \I_mon_reg_reg[19]_i_1_n_3 ;
  wire \I_mon_reg_reg[23]_i_1_n_0 ;
  wire \I_mon_reg_reg[23]_i_1_n_1 ;
  wire \I_mon_reg_reg[23]_i_1_n_2 ;
  wire \I_mon_reg_reg[23]_i_1_n_3 ;
  wire \I_mon_reg_reg[27]_i_1_n_0 ;
  wire \I_mon_reg_reg[27]_i_1_n_1 ;
  wire \I_mon_reg_reg[27]_i_1_n_2 ;
  wire \I_mon_reg_reg[27]_i_1_n_3 ;
  wire \I_mon_reg_reg[31]_i_1_n_0 ;
  wire \I_mon_reg_reg[31]_i_1_n_1 ;
  wire \I_mon_reg_reg[31]_i_1_n_2 ;
  wire \I_mon_reg_reg[31]_i_1_n_3 ;
  wire \I_mon_reg_reg[3]_i_1_n_0 ;
  wire \I_mon_reg_reg[3]_i_1_n_1 ;
  wire \I_mon_reg_reg[3]_i_1_n_2 ;
  wire \I_mon_reg_reg[3]_i_1_n_3 ;
  wire \I_mon_reg_reg[7]_i_1_n_0 ;
  wire \I_mon_reg_reg[7]_i_1_n_1 ;
  wire \I_mon_reg_reg[7]_i_1_n_2 ;
  wire \I_mon_reg_reg[7]_i_1_n_3 ;
  wire [31:0]I_term_mon;
  wire [31:0]I_term_q;
  wire \I_term_q[0]_i_1_n_0 ;
  wire \I_term_q[10]_i_1_n_0 ;
  wire \I_term_q[11]_i_1_n_0 ;
  wire \I_term_q[12]_i_1_n_0 ;
  wire \I_term_q[13]_i_1_n_0 ;
  wire \I_term_q[14]_i_1_n_0 ;
  wire \I_term_q[15]_i_1_n_0 ;
  wire \I_term_q[16]_i_1_n_0 ;
  wire \I_term_q[17]_i_1_n_0 ;
  wire \I_term_q[1]_i_1_n_0 ;
  wire \I_term_q[2]_i_1_n_0 ;
  wire \I_term_q[3]_i_1_n_0 ;
  wire \I_term_q[4]_i_1_n_0 ;
  wire \I_term_q[5]_i_1_n_0 ;
  wire \I_term_q[6]_i_1_n_0 ;
  wire \I_term_q[7]_i_1_n_0 ;
  wire \I_term_q[8]_i_1_n_0 ;
  wire \I_term_q[9]_i_1_n_0 ;
  wire [2:0]\I_term_q_reg[0]_0 ;
  wire [0:0]\I_term_q_reg[30]_0 ;
  wire [0:0]\I_term_q_reg[31]_0 ;
  wire P_error_unshifted_reg0_n_100;
  wire P_error_unshifted_reg0_n_101;
  wire P_error_unshifted_reg0_n_102;
  wire P_error_unshifted_reg0_n_103;
  wire P_error_unshifted_reg0_n_104;
  wire P_error_unshifted_reg0_n_105;
  wire P_error_unshifted_reg0_n_106;
  wire P_error_unshifted_reg0_n_107;
  wire P_error_unshifted_reg0_n_108;
  wire P_error_unshifted_reg0_n_109;
  wire P_error_unshifted_reg0_n_110;
  wire P_error_unshifted_reg0_n_111;
  wire P_error_unshifted_reg0_n_112;
  wire P_error_unshifted_reg0_n_113;
  wire P_error_unshifted_reg0_n_114;
  wire P_error_unshifted_reg0_n_115;
  wire P_error_unshifted_reg0_n_116;
  wire P_error_unshifted_reg0_n_117;
  wire P_error_unshifted_reg0_n_118;
  wire P_error_unshifted_reg0_n_119;
  wire P_error_unshifted_reg0_n_120;
  wire P_error_unshifted_reg0_n_121;
  wire P_error_unshifted_reg0_n_122;
  wire P_error_unshifted_reg0_n_123;
  wire P_error_unshifted_reg0_n_124;
  wire P_error_unshifted_reg0_n_125;
  wire P_error_unshifted_reg0_n_126;
  wire P_error_unshifted_reg0_n_127;
  wire P_error_unshifted_reg0_n_128;
  wire P_error_unshifted_reg0_n_129;
  wire P_error_unshifted_reg0_n_130;
  wire P_error_unshifted_reg0_n_131;
  wire P_error_unshifted_reg0_n_132;
  wire P_error_unshifted_reg0_n_133;
  wire P_error_unshifted_reg0_n_134;
  wire P_error_unshifted_reg0_n_135;
  wire P_error_unshifted_reg0_n_136;
  wire P_error_unshifted_reg0_n_137;
  wire P_error_unshifted_reg0_n_138;
  wire P_error_unshifted_reg0_n_139;
  wire P_error_unshifted_reg0_n_140;
  wire P_error_unshifted_reg0_n_141;
  wire P_error_unshifted_reg0_n_142;
  wire P_error_unshifted_reg0_n_143;
  wire P_error_unshifted_reg0_n_144;
  wire P_error_unshifted_reg0_n_145;
  wire P_error_unshifted_reg0_n_146;
  wire P_error_unshifted_reg0_n_147;
  wire P_error_unshifted_reg0_n_148;
  wire P_error_unshifted_reg0_n_149;
  wire P_error_unshifted_reg0_n_150;
  wire P_error_unshifted_reg0_n_151;
  wire P_error_unshifted_reg0_n_152;
  wire P_error_unshifted_reg0_n_153;
  wire P_error_unshifted_reg0_n_58;
  wire P_error_unshifted_reg0_n_59;
  wire P_error_unshifted_reg0_n_60;
  wire P_error_unshifted_reg0_n_61;
  wire P_error_unshifted_reg0_n_62;
  wire P_error_unshifted_reg0_n_63;
  wire P_error_unshifted_reg0_n_64;
  wire P_error_unshifted_reg0_n_65;
  wire P_error_unshifted_reg0_n_66;
  wire P_error_unshifted_reg0_n_67;
  wire P_error_unshifted_reg0_n_68;
  wire P_error_unshifted_reg0_n_69;
  wire P_error_unshifted_reg0_n_70;
  wire P_error_unshifted_reg0_n_71;
  wire P_error_unshifted_reg0_n_72;
  wire P_error_unshifted_reg0_n_73;
  wire P_error_unshifted_reg0_n_74;
  wire P_error_unshifted_reg0_n_75;
  wire P_error_unshifted_reg0_n_76;
  wire P_error_unshifted_reg0_n_77;
  wire P_error_unshifted_reg0_n_78;
  wire P_error_unshifted_reg0_n_79;
  wire P_error_unshifted_reg0_n_80;
  wire P_error_unshifted_reg0_n_81;
  wire P_error_unshifted_reg0_n_82;
  wire P_error_unshifted_reg0_n_83;
  wire P_error_unshifted_reg0_n_84;
  wire P_error_unshifted_reg0_n_85;
  wire P_error_unshifted_reg0_n_86;
  wire P_error_unshifted_reg0_n_87;
  wire P_error_unshifted_reg0_n_88;
  wire P_error_unshifted_reg0_n_89;
  wire P_error_unshifted_reg0_n_90;
  wire P_error_unshifted_reg0_n_91;
  wire P_error_unshifted_reg0_n_92;
  wire P_error_unshifted_reg0_n_93;
  wire P_error_unshifted_reg0_n_94;
  wire P_error_unshifted_reg0_n_95;
  wire P_error_unshifted_reg0_n_96;
  wire P_error_unshifted_reg0_n_97;
  wire P_error_unshifted_reg0_n_98;
  wire P_error_unshifted_reg0_n_99;
  wire [31:0]P_error_unshifted_reg_reg_0;
  wire P_error_unshifted_reg_reg_n_58;
  wire P_error_unshifted_reg_reg_n_59;
  wire P_error_unshifted_reg_reg_n_60;
  wire P_error_unshifted_reg_reg_n_61;
  wire P_error_unshifted_reg_reg_n_62;
  wire P_error_unshifted_reg_reg_n_63;
  wire P_error_unshifted_reg_reg_n_64;
  wire P_error_unshifted_reg_reg_n_65;
  wire P_error_unshifted_reg_reg_n_66;
  wire P_error_unshifted_reg_reg_n_67;
  wire P_error_unshifted_reg_reg_n_68;
  wire P_error_unshifted_reg_reg_n_69;
  wire P_error_unshifted_reg_reg_n_70;
  wire P_error_unshifted_reg_reg_n_71;
  wire P_error_unshifted_reg_reg_n_72;
  wire P_error_unshifted_reg_reg_n_73;
  wire P_error_unshifted_reg_reg_n_74;
  wire P_error_unshifted_reg_reg_n_75;
  wire P_error_unshifted_reg_reg_n_76;
  wire P_error_unshifted_reg_reg_n_77;
  wire P_error_unshifted_reg_reg_n_78;
  wire P_error_unshifted_reg_reg_n_79;
  wire P_error_unshifted_reg_reg_n_80;
  wire P_error_unshifted_reg_reg_n_81;
  wire P_error_unshifted_reg_reg_n_82;
  wire P_error_unshifted_reg_reg_n_83;
  wire P_error_unshifted_reg_reg_n_84;
  wire P_error_unshifted_reg_reg_n_85;
  wire P_error_unshifted_reg_reg_n_86;
  wire P_error_unshifted_reg_reg_n_87;
  wire P_error_unshifted_reg_reg_n_88;
  wire P_error_unshifted_reg_reg_n_89;
  wire P_error_unshifted_reg_reg_n_90;
  wire [31:0]Q;
  wire [0:0]\autolock_input_maxdex_reg[0] ;
  wire [13:0]\autolock_input_maxdex_reg[13] ;
  wire [0:0]\autolock_max_reg_reg[30] ;
  wire \axi_pi_output_reg[13]_inv_i_10_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_11_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_12_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_13_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_15_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_16_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_17_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_18_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_19_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_20_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_21_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_22_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_24_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_25_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_26_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_27_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_28_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_29_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_30_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_31_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_33_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_34_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_35_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_36_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_37_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_38_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_39_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_40_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_42_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_43_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_44_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_45_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_46_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_47_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_48_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_49_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_51_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_52_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_53_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_54_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_55_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_56_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_57_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_58_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_59_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_60_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_61_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_62_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_63_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_64_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_65_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_66_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_67_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_68_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_69_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_6_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_70_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_71_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_72_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_73_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_74_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_7_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_8_n_0 ;
  wire \axi_pi_output_reg[13]_inv_i_9_n_0 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_14_n_0 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_14_n_1 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_14_n_2 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_14_n_3 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_23_n_0 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_23_n_1 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_23_n_2 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_23_n_3 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_32_n_0 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_32_n_1 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_32_n_2 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_32_n_3 ;
  wire [13:0]\axi_pi_output_reg_reg[13]_inv_i_3_0 ;
  wire [31:0]\axi_pi_output_reg_reg[13]_inv_i_3_1 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_3_n_1 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_3_n_2 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_3_n_3 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_41_n_0 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_41_n_1 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_41_n_2 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_41_n_3 ;
  wire [31:0]\axi_pi_output_reg_reg[13]_inv_i_4_0 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_4_n_1 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_4_n_2 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_4_n_3 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_50_n_0 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_50_n_1 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_50_n_2 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_50_n_3 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_5_n_0 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_5_n_1 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_5_n_2 ;
  wire \axi_pi_output_reg_reg[13]_inv_i_5_n_3 ;
  wire \control_reg_reg[1] ;
  wire [13:0]error_reg0;
  wire \error_reg[11]_i_2_n_0 ;
  wire \error_reg[11]_i_3_n_0 ;
  wire \error_reg[11]_i_4_n_0 ;
  wire \error_reg[11]_i_5_n_0 ;
  wire \error_reg[13]_i_3_n_0 ;
  wire \error_reg[13]_i_4_n_0 ;
  wire \error_reg[3]_i_2_n_0 ;
  wire \error_reg[3]_i_3_n_0 ;
  wire \error_reg[3]_i_4_n_0 ;
  wire \error_reg[3]_i_5_n_0 ;
  wire \error_reg[7]_i_2_n_0 ;
  wire \error_reg[7]_i_3_n_0 ;
  wire \error_reg[7]_i_4_n_0 ;
  wire \error_reg[7]_i_5_n_0 ;
  wire \error_reg_reg[11]_i_1_n_0 ;
  wire \error_reg_reg[11]_i_1_n_1 ;
  wire \error_reg_reg[11]_i_1_n_2 ;
  wire \error_reg_reg[11]_i_1_n_3 ;
  wire [13:0]\error_reg_reg[13]_0 ;
  wire \error_reg_reg[13]_i_2_n_3 ;
  wire \error_reg_reg[3]_i_1_n_0 ;
  wire \error_reg_reg[3]_i_1_n_1 ;
  wire \error_reg_reg[3]_i_1_n_2 ;
  wire \error_reg_reg[3]_i_1_n_3 ;
  wire \error_reg_reg[7]_i_1_n_0 ;
  wire \error_reg_reg[7]_i_1_n_1 ;
  wire \error_reg_reg[7]_i_1_n_2 ;
  wire \error_reg_reg[7]_i_1_n_3 ;
  wire \loop_output_reg[19]_i_10_n_0 ;
  wire \loop_output_reg[19]_i_11_n_0 ;
  wire \loop_output_reg[19]_i_13_n_0 ;
  wire \loop_output_reg[19]_i_14_n_0 ;
  wire \loop_output_reg[19]_i_15_n_0 ;
  wire \loop_output_reg[19]_i_16_n_0 ;
  wire \loop_output_reg[19]_i_18_n_0 ;
  wire \loop_output_reg[19]_i_19_n_0 ;
  wire \loop_output_reg[19]_i_20_n_0 ;
  wire \loop_output_reg[19]_i_21_n_0 ;
  wire \loop_output_reg[19]_i_22_n_0 ;
  wire \loop_output_reg[19]_i_23_n_0 ;
  wire \loop_output_reg[19]_i_24_n_0 ;
  wire \loop_output_reg[19]_i_25_n_0 ;
  wire \loop_output_reg[19]_i_3_n_0 ;
  wire \loop_output_reg[19]_i_4_n_0 ;
  wire \loop_output_reg[19]_i_5_n_0 ;
  wire \loop_output_reg[19]_i_6_n_0 ;
  wire \loop_output_reg[19]_i_8_n_0 ;
  wire \loop_output_reg[19]_i_9_n_0 ;
  wire \loop_output_reg[23]_i_2_n_0 ;
  wire \loop_output_reg[23]_i_3_n_0 ;
  wire \loop_output_reg[23]_i_4_n_0 ;
  wire \loop_output_reg[23]_i_5_n_0 ;
  wire \loop_output_reg[27]_i_2_n_0 ;
  wire \loop_output_reg[27]_i_3_n_0 ;
  wire \loop_output_reg[27]_i_4_n_0 ;
  wire \loop_output_reg[27]_i_5_n_0 ;
  wire \loop_output_reg[31]_i_2_n_0 ;
  wire \loop_output_reg[31]_i_3_n_0 ;
  wire \loop_output_reg[31]_i_4_n_0 ;
  wire \loop_output_reg[31]_i_5_n_0 ;
  wire \loop_output_reg_reg[19]_i_12_n_0 ;
  wire \loop_output_reg_reg[19]_i_12_n_1 ;
  wire \loop_output_reg_reg[19]_i_12_n_2 ;
  wire \loop_output_reg_reg[19]_i_12_n_3 ;
  wire \loop_output_reg_reg[19]_i_17_n_0 ;
  wire \loop_output_reg_reg[19]_i_17_n_1 ;
  wire \loop_output_reg_reg[19]_i_17_n_2 ;
  wire \loop_output_reg_reg[19]_i_17_n_3 ;
  wire \loop_output_reg_reg[19]_i_1_n_0 ;
  wire \loop_output_reg_reg[19]_i_1_n_1 ;
  wire \loop_output_reg_reg[19]_i_1_n_2 ;
  wire \loop_output_reg_reg[19]_i_1_n_3 ;
  wire \loop_output_reg_reg[19]_i_1_n_4 ;
  wire \loop_output_reg_reg[19]_i_1_n_5 ;
  wire \loop_output_reg_reg[19]_i_2_n_0 ;
  wire \loop_output_reg_reg[19]_i_2_n_1 ;
  wire \loop_output_reg_reg[19]_i_2_n_2 ;
  wire \loop_output_reg_reg[19]_i_2_n_3 ;
  wire \loop_output_reg_reg[19]_i_7_n_0 ;
  wire \loop_output_reg_reg[19]_i_7_n_1 ;
  wire \loop_output_reg_reg[19]_i_7_n_2 ;
  wire \loop_output_reg_reg[19]_i_7_n_3 ;
  wire \loop_output_reg_reg[23]_i_1_n_0 ;
  wire \loop_output_reg_reg[23]_i_1_n_1 ;
  wire \loop_output_reg_reg[23]_i_1_n_2 ;
  wire \loop_output_reg_reg[23]_i_1_n_3 ;
  wire \loop_output_reg_reg[23]_i_1_n_4 ;
  wire \loop_output_reg_reg[23]_i_1_n_5 ;
  wire \loop_output_reg_reg[23]_i_1_n_6 ;
  wire \loop_output_reg_reg[23]_i_1_n_7 ;
  wire \loop_output_reg_reg[27]_i_1_n_0 ;
  wire \loop_output_reg_reg[27]_i_1_n_1 ;
  wire \loop_output_reg_reg[27]_i_1_n_2 ;
  wire \loop_output_reg_reg[27]_i_1_n_3 ;
  wire \loop_output_reg_reg[27]_i_1_n_4 ;
  wire \loop_output_reg_reg[27]_i_1_n_5 ;
  wire \loop_output_reg_reg[27]_i_1_n_6 ;
  wire \loop_output_reg_reg[27]_i_1_n_7 ;
  wire [13:0]\loop_output_reg_reg[31]_0 ;
  wire [13:0]\loop_output_reg_reg[31]_1 ;
  wire \loop_output_reg_reg[31]_i_1_n_1 ;
  wire \loop_output_reg_reg[31]_i_1_n_2 ;
  wire \loop_output_reg_reg[31]_i_1_n_3 ;
  wire \loop_output_reg_reg[31]_i_1_n_4 ;
  wire \loop_output_reg_reg[31]_i_1_n_5 ;
  wire \loop_output_reg_reg[31]_i_1_n_6 ;
  wire \loop_output_reg_reg[31]_i_1_n_7 ;
  wire [30:18]p_0_in;
  wire [13:0]pid_loop_0_input_reg;
  wire [13:0]ramp_module_0_reset;
  wire \ramplitude_min_reg[21]_i_3_n_0 ;
  wire \ramplitude_min_reg[21]_i_4_n_0 ;
  wire \ramplitude_min_reg[21]_i_5_n_0 ;
  wire \ramplitude_min_reg[25]_i_3_n_0 ;
  wire \ramplitude_min_reg[25]_i_4_n_0 ;
  wire \ramplitude_min_reg[25]_i_5_n_0 ;
  wire \ramplitude_min_reg[25]_i_6_n_0 ;
  wire \ramplitude_min_reg[29]_i_3_n_0 ;
  wire \ramplitude_min_reg[29]_i_4_n_0 ;
  wire \ramplitude_min_reg[29]_i_5_n_0 ;
  wire \ramplitude_min_reg[29]_i_6_n_0 ;
  wire \ramplitude_min_reg[31]_i_31_n_0 ;
  wire \ramplitude_min_reg[31]_i_32_n_0 ;
  wire \ramplitude_min_reg_reg[21]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[21]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[21]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[21]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[25]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[25]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[25]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[25]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[29]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[29]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[29]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[29]_i_2_n_3 ;
  wire [13:0]\ramplitude_min_reg_reg[31]_i_6_0 ;
  wire \ramplitude_min_reg_reg[31]_i_6_n_3 ;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [13:0]setpoint_reg;
  wire [13:0]\setpoint_reg_reg[13]_0 ;
  wire NLW_I_error_unshifted_reg0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_I_error_unshifted_reg0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_I_error_unshifted_reg0_OVERFLOW_UNCONNECTED;
  wire NLW_I_error_unshifted_reg0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_I_error_unshifted_reg0_PATTERNDETECT_UNCONNECTED;
  wire NLW_I_error_unshifted_reg0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_I_error_unshifted_reg0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_I_error_unshifted_reg0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_I_error_unshifted_reg0_CARRYOUT_UNCONNECTED;
  wire NLW_I_error_unshifted_reg_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_I_error_unshifted_reg_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_I_error_unshifted_reg_reg_OVERFLOW_UNCONNECTED;
  wire NLW_I_error_unshifted_reg_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_I_error_unshifted_reg_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_I_error_unshifted_reg_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_I_error_unshifted_reg_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_I_error_unshifted_reg_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_I_error_unshifted_reg_reg_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_I_error_unshifted_reg_reg_PCOUT_UNCONNECTED;
  wire [3:1]\NLW_I_term_q_reg[31]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_I_term_q_reg[31]_i_2_O_UNCONNECTED ;
  wire NLW_P_error_unshifted_reg0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_P_error_unshifted_reg0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_P_error_unshifted_reg0_OVERFLOW_UNCONNECTED;
  wire NLW_P_error_unshifted_reg0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_P_error_unshifted_reg0_PATTERNDETECT_UNCONNECTED;
  wire NLW_P_error_unshifted_reg0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_P_error_unshifted_reg0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_P_error_unshifted_reg0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_P_error_unshifted_reg0_CARRYOUT_UNCONNECTED;
  wire NLW_P_error_unshifted_reg_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_P_error_unshifted_reg_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_P_error_unshifted_reg_reg_OVERFLOW_UNCONNECTED;
  wire NLW_P_error_unshifted_reg_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_P_error_unshifted_reg_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_P_error_unshifted_reg_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_P_error_unshifted_reg_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_P_error_unshifted_reg_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_P_error_unshifted_reg_reg_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_P_error_unshifted_reg_reg_PCOUT_UNCONNECTED;
  wire [3:0]\NLW_axi_pi_output_reg_reg[13]_inv_i_14_O_UNCONNECTED ;
  wire [3:0]\NLW_axi_pi_output_reg_reg[13]_inv_i_23_O_UNCONNECTED ;
  wire [3:0]\NLW_axi_pi_output_reg_reg[13]_inv_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_axi_pi_output_reg_reg[13]_inv_i_32_O_UNCONNECTED ;
  wire [3:0]\NLW_axi_pi_output_reg_reg[13]_inv_i_4_O_UNCONNECTED ;
  wire [3:0]\NLW_axi_pi_output_reg_reg[13]_inv_i_41_O_UNCONNECTED ;
  wire [3:0]\NLW_axi_pi_output_reg_reg[13]_inv_i_5_O_UNCONNECTED ;
  wire [3:0]\NLW_axi_pi_output_reg_reg[13]_inv_i_50_O_UNCONNECTED ;
  wire [3:1]\NLW_error_reg_reg[13]_i_2_CO_UNCONNECTED ;
  wire [3:2]\NLW_error_reg_reg[13]_i_2_O_UNCONNECTED ;
  wire [1:0]\NLW_loop_output_reg_reg[19]_i_1_O_UNCONNECTED ;
  wire [3:0]\NLW_loop_output_reg_reg[19]_i_12_O_UNCONNECTED ;
  wire [3:0]\NLW_loop_output_reg_reg[19]_i_17_O_UNCONNECTED ;
  wire [3:0]\NLW_loop_output_reg_reg[19]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_loop_output_reg_reg[19]_i_7_O_UNCONNECTED ;
  wire [3:3]\NLW_loop_output_reg_reg[31]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_ramplitude_min_reg_reg[31]_i_6_CO_UNCONNECTED ;
  wire [3:2]\NLW_ramplitude_min_reg_reg[31]_i_6_O_UNCONNECTED ;

  (* METHODOLOGY_DRC_VIOS = "{SYNTH-11 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    I_error_unshifted_reg0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_I_error_unshifted_reg0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({error_reg0[13],error_reg0[13],error_reg0[13],error_reg0[13],error_reg0}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_I_error_unshifted_reg0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_I_error_unshifted_reg0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_I_error_unshifted_reg0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b1),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(s00_axi_aclk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_I_error_unshifted_reg0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_I_error_unshifted_reg0_OVERFLOW_UNCONNECTED),
        .P({I_error_unshifted_reg0_n_58,I_error_unshifted_reg0_n_59,I_error_unshifted_reg0_n_60,I_error_unshifted_reg0_n_61,I_error_unshifted_reg0_n_62,I_error_unshifted_reg0_n_63,I_error_unshifted_reg0_n_64,I_error_unshifted_reg0_n_65,I_error_unshifted_reg0_n_66,I_error_unshifted_reg0_n_67,I_error_unshifted_reg0_n_68,I_error_unshifted_reg0_n_69,I_error_unshifted_reg0_n_70,I_error_unshifted_reg0_n_71,I_error_unshifted_reg0_n_72,I_error_unshifted_reg0_n_73,I_error_unshifted_reg0_n_74,I_error_unshifted_reg0_n_75,I_error_unshifted_reg0_n_76,I_error_unshifted_reg0_n_77,I_error_unshifted_reg0_n_78,I_error_unshifted_reg0_n_79,I_error_unshifted_reg0_n_80,I_error_unshifted_reg0_n_81,I_error_unshifted_reg0_n_82,I_error_unshifted_reg0_n_83,I_error_unshifted_reg0_n_84,I_error_unshifted_reg0_n_85,I_error_unshifted_reg0_n_86,I_error_unshifted_reg0_n_87,I_error_unshifted_reg0_n_88,I_error_unshifted_reg0_n_89,I_error_unshifted_reg0_n_90,I_error_unshifted_reg0_n_91,I_error_unshifted_reg0_n_92,I_error_unshifted_reg0_n_93,I_error_unshifted_reg0_n_94,I_error_unshifted_reg0_n_95,I_error_unshifted_reg0_n_96,I_error_unshifted_reg0_n_97,I_error_unshifted_reg0_n_98,I_error_unshifted_reg0_n_99,I_error_unshifted_reg0_n_100,I_error_unshifted_reg0_n_101,I_error_unshifted_reg0_n_102,I_error_unshifted_reg0_n_103,I_error_unshifted_reg0_n_104,I_error_unshifted_reg0_n_105}),
        .PATTERNBDETECT(NLW_I_error_unshifted_reg0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_I_error_unshifted_reg0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({I_error_unshifted_reg0_n_106,I_error_unshifted_reg0_n_107,I_error_unshifted_reg0_n_108,I_error_unshifted_reg0_n_109,I_error_unshifted_reg0_n_110,I_error_unshifted_reg0_n_111,I_error_unshifted_reg0_n_112,I_error_unshifted_reg0_n_113,I_error_unshifted_reg0_n_114,I_error_unshifted_reg0_n_115,I_error_unshifted_reg0_n_116,I_error_unshifted_reg0_n_117,I_error_unshifted_reg0_n_118,I_error_unshifted_reg0_n_119,I_error_unshifted_reg0_n_120,I_error_unshifted_reg0_n_121,I_error_unshifted_reg0_n_122,I_error_unshifted_reg0_n_123,I_error_unshifted_reg0_n_124,I_error_unshifted_reg0_n_125,I_error_unshifted_reg0_n_126,I_error_unshifted_reg0_n_127,I_error_unshifted_reg0_n_128,I_error_unshifted_reg0_n_129,I_error_unshifted_reg0_n_130,I_error_unshifted_reg0_n_131,I_error_unshifted_reg0_n_132,I_error_unshifted_reg0_n_133,I_error_unshifted_reg0_n_134,I_error_unshifted_reg0_n_135,I_error_unshifted_reg0_n_136,I_error_unshifted_reg0_n_137,I_error_unshifted_reg0_n_138,I_error_unshifted_reg0_n_139,I_error_unshifted_reg0_n_140,I_error_unshifted_reg0_n_141,I_error_unshifted_reg0_n_142,I_error_unshifted_reg0_n_143,I_error_unshifted_reg0_n_144,I_error_unshifted_reg0_n_145,I_error_unshifted_reg0_n_146,I_error_unshifted_reg0_n_147,I_error_unshifted_reg0_n_148,I_error_unshifted_reg0_n_149,I_error_unshifted_reg0_n_150,I_error_unshifted_reg0_n_151,I_error_unshifted_reg0_n_152,I_error_unshifted_reg0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(\control_reg_reg[1] ),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_I_error_unshifted_reg0_UNDERFLOW_UNCONNECTED));
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    I_error_unshifted_reg_reg
       (.A({Q[31],Q[31],Q[31],Q[31],Q[31],Q[31],Q[31],Q[31],Q[31],Q[31],Q[31],Q[31],Q[31],Q[31],Q[31],Q[31:17]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_I_error_unshifted_reg_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({error_reg0[13],error_reg0[13],error_reg0[13],error_reg0[13],error_reg0}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_I_error_unshifted_reg_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_I_error_unshifted_reg_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_I_error_unshifted_reg_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b1),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b1),
        .CLK(s00_axi_aclk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_I_error_unshifted_reg_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_I_error_unshifted_reg_reg_OVERFLOW_UNCONNECTED),
        .P({I_error_unshifted_reg_reg_n_58,I_error_unshifted_reg_reg_n_59,I_error_unshifted_reg_reg_n_60,I_error_unshifted_reg_reg_n_61,I_error_unshifted_reg_reg_n_62,I_error_unshifted_reg_reg_n_63,I_error_unshifted_reg_reg_n_64,I_error_unshifted_reg_reg_n_65,I_error_unshifted_reg_reg_n_66,I_error_unshifted_reg_reg_n_67,I_error_unshifted_reg_reg_n_68,I_error_unshifted_reg_reg_n_69,I_error_unshifted_reg_reg_n_70,I_error_unshifted_reg_reg_n_71,I_error_unshifted_reg_reg_n_72,I_error_unshifted_reg_reg_n_73,I_error_unshifted_reg_reg_n_74,I_error_unshifted_reg_reg_n_75,I_error_unshifted_reg_reg_n_76,I_error[31:3]}),
        .PATTERNBDETECT(NLW_I_error_unshifted_reg_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_I_error_unshifted_reg_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({I_error_unshifted_reg0_n_106,I_error_unshifted_reg0_n_107,I_error_unshifted_reg0_n_108,I_error_unshifted_reg0_n_109,I_error_unshifted_reg0_n_110,I_error_unshifted_reg0_n_111,I_error_unshifted_reg0_n_112,I_error_unshifted_reg0_n_113,I_error_unshifted_reg0_n_114,I_error_unshifted_reg0_n_115,I_error_unshifted_reg0_n_116,I_error_unshifted_reg0_n_117,I_error_unshifted_reg0_n_118,I_error_unshifted_reg0_n_119,I_error_unshifted_reg0_n_120,I_error_unshifted_reg0_n_121,I_error_unshifted_reg0_n_122,I_error_unshifted_reg0_n_123,I_error_unshifted_reg0_n_124,I_error_unshifted_reg0_n_125,I_error_unshifted_reg0_n_126,I_error_unshifted_reg0_n_127,I_error_unshifted_reg0_n_128,I_error_unshifted_reg0_n_129,I_error_unshifted_reg0_n_130,I_error_unshifted_reg0_n_131,I_error_unshifted_reg0_n_132,I_error_unshifted_reg0_n_133,I_error_unshifted_reg0_n_134,I_error_unshifted_reg0_n_135,I_error_unshifted_reg0_n_136,I_error_unshifted_reg0_n_137,I_error_unshifted_reg0_n_138,I_error_unshifted_reg0_n_139,I_error_unshifted_reg0_n_140,I_error_unshifted_reg0_n_141,I_error_unshifted_reg0_n_142,I_error_unshifted_reg0_n_143,I_error_unshifted_reg0_n_144,I_error_unshifted_reg0_n_145,I_error_unshifted_reg0_n_146,I_error_unshifted_reg0_n_147,I_error_unshifted_reg0_n_148,I_error_unshifted_reg0_n_149,I_error_unshifted_reg0_n_150,I_error_unshifted_reg0_n_151,I_error_unshifted_reg0_n_152,I_error_unshifted_reg0_n_153}),
        .PCOUT(NLW_I_error_unshifted_reg_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(\control_reg_reg[1] ),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(\control_reg_reg[1] ),
        .UNDERFLOW(NLW_I_error_unshifted_reg_reg_UNDERFLOW_UNCONNECTED));
  FDRE \I_error_unshifted_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_error_unshifted_reg0_n_91),
        .Q(I_error[0]),
        .R(\control_reg_reg[1] ));
  FDRE \I_error_unshifted_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_error_unshifted_reg0_n_90),
        .Q(I_error[1]),
        .R(\control_reg_reg[1] ));
  FDRE \I_error_unshifted_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(I_error_unshifted_reg0_n_89),
        .Q(I_error[2]),
        .R(\control_reg_reg[1] ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[11]_i_2 
       (.I0(I_term_q[11]),
        .I1(I_error[11]),
        .O(\I_mon_reg[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[11]_i_3 
       (.I0(I_term_q[10]),
        .I1(I_error[10]),
        .O(\I_mon_reg[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[11]_i_4 
       (.I0(I_term_q[9]),
        .I1(I_error[9]),
        .O(\I_mon_reg[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[11]_i_5 
       (.I0(I_term_q[8]),
        .I1(I_error[8]),
        .O(\I_mon_reg[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[15]_i_2 
       (.I0(I_term_q[15]),
        .I1(I_error[15]),
        .O(\I_mon_reg[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[15]_i_3 
       (.I0(I_term_q[14]),
        .I1(I_error[14]),
        .O(\I_mon_reg[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[15]_i_4 
       (.I0(I_term_q[13]),
        .I1(I_error[13]),
        .O(\I_mon_reg[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[15]_i_5 
       (.I0(I_term_q[12]),
        .I1(I_error[12]),
        .O(\I_mon_reg[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[19]_i_2 
       (.I0(I_term_q[19]),
        .I1(I_error[19]),
        .O(\I_mon_reg[19]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[19]_i_3 
       (.I0(I_term_q[18]),
        .I1(I_error[18]),
        .O(\I_mon_reg[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[19]_i_4 
       (.I0(I_term_q[17]),
        .I1(I_error[17]),
        .O(\I_mon_reg[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[19]_i_5 
       (.I0(I_term_q[16]),
        .I1(I_error[16]),
        .O(\I_mon_reg[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[23]_i_2 
       (.I0(I_term_q[23]),
        .I1(I_error[23]),
        .O(\I_mon_reg[23]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[23]_i_3 
       (.I0(I_term_q[22]),
        .I1(I_error[22]),
        .O(\I_mon_reg[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[23]_i_4 
       (.I0(I_term_q[21]),
        .I1(I_error[21]),
        .O(\I_mon_reg[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[23]_i_5 
       (.I0(I_term_q[20]),
        .I1(I_error[20]),
        .O(\I_mon_reg[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[27]_i_2 
       (.I0(I_term_q[27]),
        .I1(I_error[27]),
        .O(\I_mon_reg[27]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[27]_i_3 
       (.I0(I_term_q[26]),
        .I1(I_error[26]),
        .O(\I_mon_reg[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[27]_i_4 
       (.I0(I_term_q[25]),
        .I1(I_error[25]),
        .O(\I_mon_reg[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[27]_i_5 
       (.I0(I_term_q[24]),
        .I1(I_error[24]),
        .O(\I_mon_reg[27]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \I_mon_reg[31]_i_2 
       (.I0(I_term_q[31]),
        .O(\I_mon_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[31]_i_3 
       (.I0(I_term_q[31]),
        .I1(I_error[31]),
        .O(\I_mon_reg[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[31]_i_4 
       (.I0(I_term_q[30]),
        .I1(I_error[30]),
        .O(\I_mon_reg[31]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[31]_i_5 
       (.I0(I_term_q[29]),
        .I1(I_error[29]),
        .O(\I_mon_reg[31]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[31]_i_6 
       (.I0(I_term_q[28]),
        .I1(I_error[28]),
        .O(\I_mon_reg[31]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[3]_i_2 
       (.I0(I_term_q[3]),
        .I1(I_error[3]),
        .O(\I_mon_reg[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[3]_i_3 
       (.I0(I_term_q[2]),
        .I1(I_error[2]),
        .O(\I_mon_reg[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[3]_i_4 
       (.I0(I_term_q[1]),
        .I1(I_error[1]),
        .O(\I_mon_reg[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[3]_i_5 
       (.I0(I_term_q[0]),
        .I1(I_error[0]),
        .O(\I_mon_reg[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[7]_i_2 
       (.I0(I_term_q[7]),
        .I1(I_error[7]),
        .O(\I_mon_reg[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[7]_i_3 
       (.I0(I_term_q[6]),
        .I1(I_error[6]),
        .O(\I_mon_reg[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[7]_i_4 
       (.I0(I_term_q[5]),
        .I1(I_error[5]),
        .O(\I_mon_reg[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \I_mon_reg[7]_i_5 
       (.I0(I_term_q[4]),
        .I1(I_error[4]),
        .O(\I_mon_reg[7]_i_5_n_0 ));
  CARRY4 \I_mon_reg_reg[11]_i_1 
       (.CI(\I_mon_reg_reg[7]_i_1_n_0 ),
        .CO({\I_mon_reg_reg[11]_i_1_n_0 ,\I_mon_reg_reg[11]_i_1_n_1 ,\I_mon_reg_reg[11]_i_1_n_2 ,\I_mon_reg_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(I_term_q[11:8]),
        .O(I_term_mon[11:8]),
        .S({\I_mon_reg[11]_i_2_n_0 ,\I_mon_reg[11]_i_3_n_0 ,\I_mon_reg[11]_i_4_n_0 ,\I_mon_reg[11]_i_5_n_0 }));
  CARRY4 \I_mon_reg_reg[15]_i_1 
       (.CI(\I_mon_reg_reg[11]_i_1_n_0 ),
        .CO({\I_mon_reg_reg[15]_i_1_n_0 ,\I_mon_reg_reg[15]_i_1_n_1 ,\I_mon_reg_reg[15]_i_1_n_2 ,\I_mon_reg_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(I_term_q[15:12]),
        .O(I_term_mon[15:12]),
        .S({\I_mon_reg[15]_i_2_n_0 ,\I_mon_reg[15]_i_3_n_0 ,\I_mon_reg[15]_i_4_n_0 ,\I_mon_reg[15]_i_5_n_0 }));
  CARRY4 \I_mon_reg_reg[19]_i_1 
       (.CI(\I_mon_reg_reg[15]_i_1_n_0 ),
        .CO({\I_mon_reg_reg[19]_i_1_n_0 ,\I_mon_reg_reg[19]_i_1_n_1 ,\I_mon_reg_reg[19]_i_1_n_2 ,\I_mon_reg_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(I_term_q[19:16]),
        .O(I_term_mon[19:16]),
        .S({\I_mon_reg[19]_i_2_n_0 ,\I_mon_reg[19]_i_3_n_0 ,\I_mon_reg[19]_i_4_n_0 ,\I_mon_reg[19]_i_5_n_0 }));
  CARRY4 \I_mon_reg_reg[23]_i_1 
       (.CI(\I_mon_reg_reg[19]_i_1_n_0 ),
        .CO({\I_mon_reg_reg[23]_i_1_n_0 ,\I_mon_reg_reg[23]_i_1_n_1 ,\I_mon_reg_reg[23]_i_1_n_2 ,\I_mon_reg_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(I_term_q[23:20]),
        .O(I_term_mon[23:20]),
        .S({\I_mon_reg[23]_i_2_n_0 ,\I_mon_reg[23]_i_3_n_0 ,\I_mon_reg[23]_i_4_n_0 ,\I_mon_reg[23]_i_5_n_0 }));
  CARRY4 \I_mon_reg_reg[27]_i_1 
       (.CI(\I_mon_reg_reg[23]_i_1_n_0 ),
        .CO({\I_mon_reg_reg[27]_i_1_n_0 ,\I_mon_reg_reg[27]_i_1_n_1 ,\I_mon_reg_reg[27]_i_1_n_2 ,\I_mon_reg_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(I_term_q[27:24]),
        .O(I_term_mon[27:24]),
        .S({\I_mon_reg[27]_i_2_n_0 ,\I_mon_reg[27]_i_3_n_0 ,\I_mon_reg[27]_i_4_n_0 ,\I_mon_reg[27]_i_5_n_0 }));
  CARRY4 \I_mon_reg_reg[31]_i_1 
       (.CI(\I_mon_reg_reg[27]_i_1_n_0 ),
        .CO({\I_mon_reg_reg[31]_i_1_n_0 ,\I_mon_reg_reg[31]_i_1_n_1 ,\I_mon_reg_reg[31]_i_1_n_2 ,\I_mon_reg_reg[31]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\I_mon_reg[31]_i_2_n_0 ,I_term_q[30:28]}),
        .O(I_term_mon[31:28]),
        .S({\I_mon_reg[31]_i_3_n_0 ,\I_mon_reg[31]_i_4_n_0 ,\I_mon_reg[31]_i_5_n_0 ,\I_mon_reg[31]_i_6_n_0 }));
  CARRY4 \I_mon_reg_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\I_mon_reg_reg[3]_i_1_n_0 ,\I_mon_reg_reg[3]_i_1_n_1 ,\I_mon_reg_reg[3]_i_1_n_2 ,\I_mon_reg_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(I_term_q[3:0]),
        .O(I_term_mon[3:0]),
        .S({\I_mon_reg[3]_i_2_n_0 ,\I_mon_reg[3]_i_3_n_0 ,\I_mon_reg[3]_i_4_n_0 ,\I_mon_reg[3]_i_5_n_0 }));
  CARRY4 \I_mon_reg_reg[7]_i_1 
       (.CI(\I_mon_reg_reg[3]_i_1_n_0 ),
        .CO({\I_mon_reg_reg[7]_i_1_n_0 ,\I_mon_reg_reg[7]_i_1_n_1 ,\I_mon_reg_reg[7]_i_1_n_2 ,\I_mon_reg_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(I_term_q[7:4]),
        .O(I_term_mon[7:4]),
        .S({\I_mon_reg[7]_i_2_n_0 ,\I_mon_reg[7]_i_3_n_0 ,\I_mon_reg[7]_i_4_n_0 ,\I_mon_reg[7]_i_5_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[0]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[0]),
        .O(\I_term_q[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[10]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[10]),
        .O(\I_term_q[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[11]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[11]),
        .O(\I_term_q[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[12]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[12]),
        .O(\I_term_q[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[13]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[13]),
        .O(\I_term_q[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[14]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[14]),
        .O(\I_term_q[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[15]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[15]),
        .O(\I_term_q[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[16]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[16]),
        .O(\I_term_q[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[17]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[17]),
        .O(\I_term_q[17]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[18]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [0]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[18]),
        .O(p_0_in[18]));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[19]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [1]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[19]),
        .O(p_0_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[1]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[1]),
        .O(\I_term_q[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[20]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [2]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[20]),
        .O(p_0_in[20]));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[21]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [3]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[21]),
        .O(p_0_in[21]));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[22]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [4]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[22]),
        .O(p_0_in[22]));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[23]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [5]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[23]),
        .O(p_0_in[23]));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[24]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [6]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[24]),
        .O(p_0_in[24]));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[25]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [7]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[25]),
        .O(p_0_in[25]));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[26]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [8]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[26]),
        .O(p_0_in[26]));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[27]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [9]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[27]),
        .O(p_0_in[27]));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[28]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [10]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[28]),
        .O(p_0_in[28]));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[29]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [11]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[29]),
        .O(p_0_in[29]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[2]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[2]),
        .O(\I_term_q[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hBBB8B888)) 
    \I_term_q[30]_i_1 
       (.I0(\autolock_input_maxdex_reg[13] [12]),
        .I1(\control_reg_reg[1] ),
        .I2(\I_term_q_reg[30]_0 ),
        .I3(I_term_mon[31]),
        .I4(I_term_mon[30]),
        .O(p_0_in[30]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[3]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[3]),
        .O(\I_term_q[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[4]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[4]),
        .O(\I_term_q[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[5]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[5]),
        .O(\I_term_q[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[6]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[6]),
        .O(\I_term_q[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[7]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[7]),
        .O(\I_term_q[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[8]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[8]),
        .O(\I_term_q[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \I_term_q[9]_i_1 
       (.I0(\I_term_q_reg[30]_0 ),
        .I1(I_term_mon[31]),
        .I2(I_term_mon[9]),
        .O(\I_term_q[9]_i_1_n_0 ));
  FDRE \I_term_q_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[0]_i_1_n_0 ),
        .Q(I_term_q[0]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[10]_i_1_n_0 ),
        .Q(I_term_q[10]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[11]_i_1_n_0 ),
        .Q(I_term_q[11]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[12]_i_1_n_0 ),
        .Q(I_term_q[12]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[13]_i_1_n_0 ),
        .Q(I_term_q[13]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[14]_i_1_n_0 ),
        .Q(I_term_q[14]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[15]_i_1_n_0 ),
        .Q(I_term_q[15]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[16]_i_1_n_0 ),
        .Q(I_term_q[16]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[17]_i_1_n_0 ),
        .Q(I_term_q[17]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[18]),
        .Q(I_term_q[18]),
        .R(1'b0));
  FDRE \I_term_q_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[19]),
        .Q(I_term_q[19]),
        .R(1'b0));
  FDRE \I_term_q_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[1]_i_1_n_0 ),
        .Q(I_term_q[1]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[20]),
        .Q(I_term_q[20]),
        .R(1'b0));
  FDRE \I_term_q_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[21]),
        .Q(I_term_q[21]),
        .R(1'b0));
  FDRE \I_term_q_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[22]),
        .Q(I_term_q[22]),
        .R(1'b0));
  FDRE \I_term_q_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[23]),
        .Q(I_term_q[23]),
        .R(1'b0));
  FDRE \I_term_q_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[24]),
        .Q(I_term_q[24]),
        .R(1'b0));
  FDRE \I_term_q_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[25]),
        .Q(I_term_q[25]),
        .R(1'b0));
  FDRE \I_term_q_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[26]),
        .Q(I_term_q[26]),
        .R(1'b0));
  FDRE \I_term_q_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[27]),
        .Q(I_term_q[27]),
        .R(1'b0));
  FDRE \I_term_q_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[28]),
        .Q(I_term_q[28]),
        .R(1'b0));
  FDRE \I_term_q_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[29]),
        .Q(I_term_q[29]),
        .R(1'b0));
  FDRE \I_term_q_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[2]_i_1_n_0 ),
        .Q(I_term_q[2]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[30]),
        .Q(I_term_q[30]),
        .R(1'b0));
  FDRE \I_term_q_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q_reg[31]_0 ),
        .Q(I_term_q[31]),
        .R(1'b0));
  CARRY4 \I_term_q_reg[31]_i_2 
       (.CI(\I_mon_reg_reg[31]_i_1_n_0 ),
        .CO({\NLW_I_term_q_reg[31]_i_2_CO_UNCONNECTED [3:1],\I_term_q_reg[30]_0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_I_term_q_reg[31]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  FDRE \I_term_q_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[3]_i_1_n_0 ),
        .Q(I_term_q[3]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[4]_i_1_n_0 ),
        .Q(I_term_q[4]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[5]_i_1_n_0 ),
        .Q(I_term_q[5]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[6]_i_1_n_0 ),
        .Q(I_term_q[6]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[7]_i_1_n_0 ),
        .Q(I_term_q[7]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[8]_i_1_n_0 ),
        .Q(I_term_q[8]),
        .R(\control_reg_reg[1] ));
  FDRE \I_term_q_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\I_term_q[9]_i_1_n_0 ),
        .Q(I_term_q[9]),
        .R(\control_reg_reg[1] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-11 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    P_error_unshifted_reg0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,P_error_unshifted_reg_reg_0[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_P_error_unshifted_reg0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({error_reg0[13],error_reg0[13],error_reg0[13],error_reg0[13],error_reg0}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_P_error_unshifted_reg0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_P_error_unshifted_reg0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_P_error_unshifted_reg0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b1),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(s00_axi_aclk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_P_error_unshifted_reg0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_P_error_unshifted_reg0_OVERFLOW_UNCONNECTED),
        .P({P_error_unshifted_reg0_n_58,P_error_unshifted_reg0_n_59,P_error_unshifted_reg0_n_60,P_error_unshifted_reg0_n_61,P_error_unshifted_reg0_n_62,P_error_unshifted_reg0_n_63,P_error_unshifted_reg0_n_64,P_error_unshifted_reg0_n_65,P_error_unshifted_reg0_n_66,P_error_unshifted_reg0_n_67,P_error_unshifted_reg0_n_68,P_error_unshifted_reg0_n_69,P_error_unshifted_reg0_n_70,P_error_unshifted_reg0_n_71,P_error_unshifted_reg0_n_72,P_error_unshifted_reg0_n_73,P_error_unshifted_reg0_n_74,P_error_unshifted_reg0_n_75,P_error_unshifted_reg0_n_76,P_error_unshifted_reg0_n_77,P_error_unshifted_reg0_n_78,P_error_unshifted_reg0_n_79,P_error_unshifted_reg0_n_80,P_error_unshifted_reg0_n_81,P_error_unshifted_reg0_n_82,P_error_unshifted_reg0_n_83,P_error_unshifted_reg0_n_84,P_error_unshifted_reg0_n_85,P_error_unshifted_reg0_n_86,P_error_unshifted_reg0_n_87,P_error_unshifted_reg0_n_88,P_error_unshifted_reg0_n_89,P_error_unshifted_reg0_n_90,P_error_unshifted_reg0_n_91,P_error_unshifted_reg0_n_92,P_error_unshifted_reg0_n_93,P_error_unshifted_reg0_n_94,P_error_unshifted_reg0_n_95,P_error_unshifted_reg0_n_96,P_error_unshifted_reg0_n_97,P_error_unshifted_reg0_n_98,P_error_unshifted_reg0_n_99,P_error_unshifted_reg0_n_100,P_error_unshifted_reg0_n_101,P_error_unshifted_reg0_n_102,P_error_unshifted_reg0_n_103,P_error_unshifted_reg0_n_104,P_error_unshifted_reg0_n_105}),
        .PATTERNBDETECT(NLW_P_error_unshifted_reg0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_P_error_unshifted_reg0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({P_error_unshifted_reg0_n_106,P_error_unshifted_reg0_n_107,P_error_unshifted_reg0_n_108,P_error_unshifted_reg0_n_109,P_error_unshifted_reg0_n_110,P_error_unshifted_reg0_n_111,P_error_unshifted_reg0_n_112,P_error_unshifted_reg0_n_113,P_error_unshifted_reg0_n_114,P_error_unshifted_reg0_n_115,P_error_unshifted_reg0_n_116,P_error_unshifted_reg0_n_117,P_error_unshifted_reg0_n_118,P_error_unshifted_reg0_n_119,P_error_unshifted_reg0_n_120,P_error_unshifted_reg0_n_121,P_error_unshifted_reg0_n_122,P_error_unshifted_reg0_n_123,P_error_unshifted_reg0_n_124,P_error_unshifted_reg0_n_125,P_error_unshifted_reg0_n_126,P_error_unshifted_reg0_n_127,P_error_unshifted_reg0_n_128,P_error_unshifted_reg0_n_129,P_error_unshifted_reg0_n_130,P_error_unshifted_reg0_n_131,P_error_unshifted_reg0_n_132,P_error_unshifted_reg0_n_133,P_error_unshifted_reg0_n_134,P_error_unshifted_reg0_n_135,P_error_unshifted_reg0_n_136,P_error_unshifted_reg0_n_137,P_error_unshifted_reg0_n_138,P_error_unshifted_reg0_n_139,P_error_unshifted_reg0_n_140,P_error_unshifted_reg0_n_141,P_error_unshifted_reg0_n_142,P_error_unshifted_reg0_n_143,P_error_unshifted_reg0_n_144,P_error_unshifted_reg0_n_145,P_error_unshifted_reg0_n_146,P_error_unshifted_reg0_n_147,P_error_unshifted_reg0_n_148,P_error_unshifted_reg0_n_149,P_error_unshifted_reg0_n_150,P_error_unshifted_reg0_n_151,P_error_unshifted_reg0_n_152,P_error_unshifted_reg0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(\control_reg_reg[1] ),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_P_error_unshifted_reg0_UNDERFLOW_UNCONNECTED));
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    P_error_unshifted_reg_reg
       (.A({P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31],P_error_unshifted_reg_reg_0[31:17]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_P_error_unshifted_reg_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({error_reg0[13],error_reg0[13],error_reg0[13],error_reg0[13],error_reg0}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_P_error_unshifted_reg_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_P_error_unshifted_reg_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_P_error_unshifted_reg_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b1),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b1),
        .CLK(s00_axi_aclk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_P_error_unshifted_reg_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_P_error_unshifted_reg_reg_OVERFLOW_UNCONNECTED),
        .P({P_error_unshifted_reg_reg_n_58,P_error_unshifted_reg_reg_n_59,P_error_unshifted_reg_reg_n_60,P_error_unshifted_reg_reg_n_61,P_error_unshifted_reg_reg_n_62,P_error_unshifted_reg_reg_n_63,P_error_unshifted_reg_reg_n_64,P_error_unshifted_reg_reg_n_65,P_error_unshifted_reg_reg_n_66,P_error_unshifted_reg_reg_n_67,P_error_unshifted_reg_reg_n_68,P_error_unshifted_reg_reg_n_69,P_error_unshifted_reg_reg_n_70,P_error_unshifted_reg_reg_n_71,P_error_unshifted_reg_reg_n_72,P_error_unshifted_reg_reg_n_73,P_error_unshifted_reg_reg_n_74,P_error_unshifted_reg_reg_n_75,P_error_unshifted_reg_reg_n_76,P_error_unshifted_reg_reg_n_77,P_error_unshifted_reg_reg_n_78,P_error_unshifted_reg_reg_n_79,P_error_unshifted_reg_reg_n_80,P_error_unshifted_reg_reg_n_81,P_error_unshifted_reg_reg_n_82,P_error_unshifted_reg_reg_n_83,P_error_unshifted_reg_reg_n_84,P_error_unshifted_reg_reg_n_85,P_error_unshifted_reg_reg_n_86,P_error_unshifted_reg_reg_n_87,P_error_unshifted_reg_reg_n_88,P_error_unshifted_reg_reg_n_89,P_error_unshifted_reg_reg_n_90,D[31:17]}),
        .PATTERNBDETECT(NLW_P_error_unshifted_reg_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_P_error_unshifted_reg_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({P_error_unshifted_reg0_n_106,P_error_unshifted_reg0_n_107,P_error_unshifted_reg0_n_108,P_error_unshifted_reg0_n_109,P_error_unshifted_reg0_n_110,P_error_unshifted_reg0_n_111,P_error_unshifted_reg0_n_112,P_error_unshifted_reg0_n_113,P_error_unshifted_reg0_n_114,P_error_unshifted_reg0_n_115,P_error_unshifted_reg0_n_116,P_error_unshifted_reg0_n_117,P_error_unshifted_reg0_n_118,P_error_unshifted_reg0_n_119,P_error_unshifted_reg0_n_120,P_error_unshifted_reg0_n_121,P_error_unshifted_reg0_n_122,P_error_unshifted_reg0_n_123,P_error_unshifted_reg0_n_124,P_error_unshifted_reg0_n_125,P_error_unshifted_reg0_n_126,P_error_unshifted_reg0_n_127,P_error_unshifted_reg0_n_128,P_error_unshifted_reg0_n_129,P_error_unshifted_reg0_n_130,P_error_unshifted_reg0_n_131,P_error_unshifted_reg0_n_132,P_error_unshifted_reg0_n_133,P_error_unshifted_reg0_n_134,P_error_unshifted_reg0_n_135,P_error_unshifted_reg0_n_136,P_error_unshifted_reg0_n_137,P_error_unshifted_reg0_n_138,P_error_unshifted_reg0_n_139,P_error_unshifted_reg0_n_140,P_error_unshifted_reg0_n_141,P_error_unshifted_reg0_n_142,P_error_unshifted_reg0_n_143,P_error_unshifted_reg0_n_144,P_error_unshifted_reg0_n_145,P_error_unshifted_reg0_n_146,P_error_unshifted_reg0_n_147,P_error_unshifted_reg0_n_148,P_error_unshifted_reg0_n_149,P_error_unshifted_reg0_n_150,P_error_unshifted_reg0_n_151,P_error_unshifted_reg0_n_152,P_error_unshifted_reg0_n_153}),
        .PCOUT(NLW_P_error_unshifted_reg_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(\control_reg_reg[1] ),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(\control_reg_reg[1] ),
        .UNDERFLOW(NLW_P_error_unshifted_reg_reg_UNDERFLOW_UNCONNECTED));
  FDRE \P_error_unshifted_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_105),
        .Q(D[0]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_95),
        .Q(D[10]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_94),
        .Q(D[11]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_93),
        .Q(D[12]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_92),
        .Q(D[13]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_91),
        .Q(D[14]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_90),
        .Q(D[15]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_89),
        .Q(D[16]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_104),
        .Q(D[1]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_103),
        .Q(D[2]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_102),
        .Q(D[3]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_101),
        .Q(D[4]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_100),
        .Q(D[5]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_99),
        .Q(D[6]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_98),
        .Q(D[7]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_97),
        .Q(D[8]),
        .R(\control_reg_reg[1] ));
  FDRE \P_error_unshifted_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(P_error_unshifted_reg0_n_96),
        .Q(D[9]),
        .R(\control_reg_reg[1] ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[0]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [0]),
        .I1(\autolock_input_maxdex_reg[13] [0]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [0]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[10]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [10]),
        .I1(\autolock_input_maxdex_reg[13] [10]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [10]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[11]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [11]),
        .I1(\autolock_input_maxdex_reg[13] [11]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [11]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[12]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [12]),
        .I1(\autolock_input_maxdex_reg[13] [12]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [12]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[13]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [13]),
        .I1(\autolock_input_maxdex_reg[13] [13]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [13]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[1]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [1]),
        .I1(\autolock_input_maxdex_reg[13] [1]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[2]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [2]),
        .I1(\autolock_input_maxdex_reg[13] [2]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [2]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[3]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [3]),
        .I1(\autolock_input_maxdex_reg[13] [3]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [3]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[4]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [4]),
        .I1(\autolock_input_maxdex_reg[13] [4]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [4]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[5]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [5]),
        .I1(\autolock_input_maxdex_reg[13] [5]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [5]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[6]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [6]),
        .I1(\autolock_input_maxdex_reg[13] [6]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [6]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[7]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [7]),
        .I1(\autolock_input_maxdex_reg[13] [7]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [7]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[8]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [8]),
        .I1(\autolock_input_maxdex_reg[13] [8]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [8]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \autolock_input_mindex[9]_i_1 
       (.I0(\loop_output_reg_reg[31]_0 [9]),
        .I1(\autolock_input_maxdex_reg[13] [9]),
        .I2(\autolock_input_maxdex_reg[0] ),
        .O(\loop_output_reg_reg[31]_1 [9]));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_10 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [30]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [31]),
        .O(\axi_pi_output_reg[13]_inv_i_10_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_11 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [28]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_1 [29]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_11_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_12 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [26]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_1 [27]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_12_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_13 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [24]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_1 [25]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_13_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \axi_pi_output_reg[13]_inv_i_15 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [30]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [31]),
        .O(\axi_pi_output_reg[13]_inv_i_15_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \axi_pi_output_reg[13]_inv_i_16 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [28]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [29]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_16_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \axi_pi_output_reg[13]_inv_i_17 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [26]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [27]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_17_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \axi_pi_output_reg[13]_inv_i_18 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [24]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [25]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_18_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_19 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [30]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [31]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_19_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_20 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [28]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [29]),
        .O(\axi_pi_output_reg[13]_inv_i_20_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_21 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [26]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [27]),
        .O(\axi_pi_output_reg[13]_inv_i_21_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_22 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [24]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [25]),
        .O(\axi_pi_output_reg[13]_inv_i_22_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \axi_pi_output_reg[13]_inv_i_24 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [22]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [23]),
        .O(\axi_pi_output_reg[13]_inv_i_24_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \axi_pi_output_reg[13]_inv_i_25 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [20]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [21]),
        .O(\axi_pi_output_reg[13]_inv_i_25_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \axi_pi_output_reg[13]_inv_i_26 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [18]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [19]),
        .O(\axi_pi_output_reg[13]_inv_i_26_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \axi_pi_output_reg[13]_inv_i_27 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [16]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [17]),
        .O(\axi_pi_output_reg[13]_inv_i_27_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_28 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [22]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_1 [23]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_28_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_29 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [20]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_1 [21]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_29_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_30 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [18]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_1 [19]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_30_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_31 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [16]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_1 [17]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_31_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \axi_pi_output_reg[13]_inv_i_33 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [22]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [23]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_33_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \axi_pi_output_reg[13]_inv_i_34 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [20]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [21]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_34_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \axi_pi_output_reg[13]_inv_i_35 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [18]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [19]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_35_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \axi_pi_output_reg[13]_inv_i_36 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [16]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [17]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_36_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_37 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [22]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [23]),
        .O(\axi_pi_output_reg[13]_inv_i_37_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_38 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [20]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [21]),
        .O(\axi_pi_output_reg[13]_inv_i_38_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_39 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [18]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [19]),
        .O(\axi_pi_output_reg[13]_inv_i_39_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_40 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [16]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [17]),
        .O(\axi_pi_output_reg[13]_inv_i_40_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \axi_pi_output_reg[13]_inv_i_42 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [14]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [15]),
        .O(\axi_pi_output_reg[13]_inv_i_42_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_43 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [12]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [12]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_1 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_43_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_44 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [10]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [10]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [11]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_1 [11]),
        .O(\axi_pi_output_reg[13]_inv_i_44_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_45 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [8]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [8]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [9]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_1 [9]),
        .O(\axi_pi_output_reg[13]_inv_i_45_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_46 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [14]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_1 [15]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_46_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_47 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [12]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [12]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [13]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_47_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_48 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [10]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [10]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [11]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [11]),
        .O(\axi_pi_output_reg[13]_inv_i_48_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_49 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [8]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [8]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [9]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [9]),
        .O(\axi_pi_output_reg[13]_inv_i_49_n_0 ));
  LUT3 #(
    .INIT(8'h70)) 
    \axi_pi_output_reg[13]_inv_i_51 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [14]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [15]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_51_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_52 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [12]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [12]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [13]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_52_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_53 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [10]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [10]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [11]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [11]),
        .O(\axi_pi_output_reg[13]_inv_i_53_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_54 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [8]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [8]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [9]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [9]),
        .O(\axi_pi_output_reg[13]_inv_i_54_n_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \axi_pi_output_reg[13]_inv_i_55 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_4_0 [14]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [15]),
        .O(\axi_pi_output_reg[13]_inv_i_55_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_56 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [12]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [12]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_4_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_56_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_57 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [10]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [10]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [11]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_4_0 [11]),
        .O(\axi_pi_output_reg[13]_inv_i_57_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_58 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [8]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [8]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [9]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_4_0 [9]),
        .O(\axi_pi_output_reg[13]_inv_i_58_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_59 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [6]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [6]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [7]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_1 [7]),
        .O(\axi_pi_output_reg[13]_inv_i_59_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \axi_pi_output_reg[13]_inv_i_6 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [30]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_1 [31]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .O(\axi_pi_output_reg[13]_inv_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_60 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [4]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [4]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [5]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_1 [5]),
        .O(\axi_pi_output_reg[13]_inv_i_60_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_61 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [2]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [2]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [3]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_1 [3]),
        .O(\axi_pi_output_reg[13]_inv_i_61_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_62 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [0]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [0]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [1]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_1 [1]),
        .O(\axi_pi_output_reg[13]_inv_i_62_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_63 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [6]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [6]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [7]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [7]),
        .O(\axi_pi_output_reg[13]_inv_i_63_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_64 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [4]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [4]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [5]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [5]),
        .O(\axi_pi_output_reg[13]_inv_i_64_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_65 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [2]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [2]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [3]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [3]),
        .O(\axi_pi_output_reg[13]_inv_i_65_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_66 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [0]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [0]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [1]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [1]),
        .O(\axi_pi_output_reg[13]_inv_i_66_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_67 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [6]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [6]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [7]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [7]),
        .O(\axi_pi_output_reg[13]_inv_i_67_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_68 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [4]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [4]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [5]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [5]),
        .O(\axi_pi_output_reg[13]_inv_i_68_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_69 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [2]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [2]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [3]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [3]),
        .O(\axi_pi_output_reg[13]_inv_i_69_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \axi_pi_output_reg[13]_inv_i_7 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [28]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [29]),
        .O(\axi_pi_output_reg[13]_inv_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \axi_pi_output_reg[13]_inv_i_70 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [0]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [0]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_4_0 [1]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_3_0 [1]),
        .O(\axi_pi_output_reg[13]_inv_i_70_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_71 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [6]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [6]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [7]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_4_0 [7]),
        .O(\axi_pi_output_reg[13]_inv_i_71_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_72 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [4]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [4]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [5]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_4_0 [5]),
        .O(\axi_pi_output_reg[13]_inv_i_72_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_73 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [2]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [2]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [3]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_4_0 [3]),
        .O(\axi_pi_output_reg[13]_inv_i_73_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \axi_pi_output_reg[13]_inv_i_74 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_0 [0]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_4_0 [0]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_0 [1]),
        .I3(\axi_pi_output_reg_reg[13]_inv_i_4_0 [1]),
        .O(\axi_pi_output_reg[13]_inv_i_74_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \axi_pi_output_reg[13]_inv_i_8 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [26]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [27]),
        .O(\axi_pi_output_reg[13]_inv_i_8_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \axi_pi_output_reg[13]_inv_i_9 
       (.I0(\axi_pi_output_reg_reg[13]_inv_i_3_1 [24]),
        .I1(\axi_pi_output_reg_reg[13]_inv_i_3_0 [13]),
        .I2(\axi_pi_output_reg_reg[13]_inv_i_3_1 [25]),
        .O(\axi_pi_output_reg[13]_inv_i_9_n_0 ));
  CARRY4 \axi_pi_output_reg_reg[13]_inv_i_14 
       (.CI(\axi_pi_output_reg_reg[13]_inv_i_32_n_0 ),
        .CO({\axi_pi_output_reg_reg[13]_inv_i_14_n_0 ,\axi_pi_output_reg_reg[13]_inv_i_14_n_1 ,\axi_pi_output_reg_reg[13]_inv_i_14_n_2 ,\axi_pi_output_reg_reg[13]_inv_i_14_n_3 }),
        .CYINIT(1'b0),
        .DI({\axi_pi_output_reg[13]_inv_i_33_n_0 ,\axi_pi_output_reg[13]_inv_i_34_n_0 ,\axi_pi_output_reg[13]_inv_i_35_n_0 ,\axi_pi_output_reg[13]_inv_i_36_n_0 }),
        .O(\NLW_axi_pi_output_reg_reg[13]_inv_i_14_O_UNCONNECTED [3:0]),
        .S({\axi_pi_output_reg[13]_inv_i_37_n_0 ,\axi_pi_output_reg[13]_inv_i_38_n_0 ,\axi_pi_output_reg[13]_inv_i_39_n_0 ,\axi_pi_output_reg[13]_inv_i_40_n_0 }));
  CARRY4 \axi_pi_output_reg_reg[13]_inv_i_23 
       (.CI(\axi_pi_output_reg_reg[13]_inv_i_41_n_0 ),
        .CO({\axi_pi_output_reg_reg[13]_inv_i_23_n_0 ,\axi_pi_output_reg_reg[13]_inv_i_23_n_1 ,\axi_pi_output_reg_reg[13]_inv_i_23_n_2 ,\axi_pi_output_reg_reg[13]_inv_i_23_n_3 }),
        .CYINIT(1'b0),
        .DI({\axi_pi_output_reg[13]_inv_i_42_n_0 ,\axi_pi_output_reg[13]_inv_i_43_n_0 ,\axi_pi_output_reg[13]_inv_i_44_n_0 ,\axi_pi_output_reg[13]_inv_i_45_n_0 }),
        .O(\NLW_axi_pi_output_reg_reg[13]_inv_i_23_O_UNCONNECTED [3:0]),
        .S({\axi_pi_output_reg[13]_inv_i_46_n_0 ,\axi_pi_output_reg[13]_inv_i_47_n_0 ,\axi_pi_output_reg[13]_inv_i_48_n_0 ,\axi_pi_output_reg[13]_inv_i_49_n_0 }));
  CARRY4 \axi_pi_output_reg_reg[13]_inv_i_3 
       (.CI(\axi_pi_output_reg_reg[13]_inv_i_5_n_0 ),
        .CO({\autolock_max_reg_reg[30] ,\axi_pi_output_reg_reg[13]_inv_i_3_n_1 ,\axi_pi_output_reg_reg[13]_inv_i_3_n_2 ,\axi_pi_output_reg_reg[13]_inv_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({\axi_pi_output_reg[13]_inv_i_6_n_0 ,\axi_pi_output_reg[13]_inv_i_7_n_0 ,\axi_pi_output_reg[13]_inv_i_8_n_0 ,\axi_pi_output_reg[13]_inv_i_9_n_0 }),
        .O(\NLW_axi_pi_output_reg_reg[13]_inv_i_3_O_UNCONNECTED [3:0]),
        .S({\axi_pi_output_reg[13]_inv_i_10_n_0 ,\axi_pi_output_reg[13]_inv_i_11_n_0 ,\axi_pi_output_reg[13]_inv_i_12_n_0 ,\axi_pi_output_reg[13]_inv_i_13_n_0 }));
  CARRY4 \axi_pi_output_reg_reg[13]_inv_i_32 
       (.CI(\axi_pi_output_reg_reg[13]_inv_i_50_n_0 ),
        .CO({\axi_pi_output_reg_reg[13]_inv_i_32_n_0 ,\axi_pi_output_reg_reg[13]_inv_i_32_n_1 ,\axi_pi_output_reg_reg[13]_inv_i_32_n_2 ,\axi_pi_output_reg_reg[13]_inv_i_32_n_3 }),
        .CYINIT(1'b0),
        .DI({\axi_pi_output_reg[13]_inv_i_51_n_0 ,\axi_pi_output_reg[13]_inv_i_52_n_0 ,\axi_pi_output_reg[13]_inv_i_53_n_0 ,\axi_pi_output_reg[13]_inv_i_54_n_0 }),
        .O(\NLW_axi_pi_output_reg_reg[13]_inv_i_32_O_UNCONNECTED [3:0]),
        .S({\axi_pi_output_reg[13]_inv_i_55_n_0 ,\axi_pi_output_reg[13]_inv_i_56_n_0 ,\axi_pi_output_reg[13]_inv_i_57_n_0 ,\axi_pi_output_reg[13]_inv_i_58_n_0 }));
  CARRY4 \axi_pi_output_reg_reg[13]_inv_i_4 
       (.CI(\axi_pi_output_reg_reg[13]_inv_i_14_n_0 ),
        .CO({CO,\axi_pi_output_reg_reg[13]_inv_i_4_n_1 ,\axi_pi_output_reg_reg[13]_inv_i_4_n_2 ,\axi_pi_output_reg_reg[13]_inv_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({\axi_pi_output_reg[13]_inv_i_15_n_0 ,\axi_pi_output_reg[13]_inv_i_16_n_0 ,\axi_pi_output_reg[13]_inv_i_17_n_0 ,\axi_pi_output_reg[13]_inv_i_18_n_0 }),
        .O(\NLW_axi_pi_output_reg_reg[13]_inv_i_4_O_UNCONNECTED [3:0]),
        .S({\axi_pi_output_reg[13]_inv_i_19_n_0 ,\axi_pi_output_reg[13]_inv_i_20_n_0 ,\axi_pi_output_reg[13]_inv_i_21_n_0 ,\axi_pi_output_reg[13]_inv_i_22_n_0 }));
  CARRY4 \axi_pi_output_reg_reg[13]_inv_i_41 
       (.CI(1'b0),
        .CO({\axi_pi_output_reg_reg[13]_inv_i_41_n_0 ,\axi_pi_output_reg_reg[13]_inv_i_41_n_1 ,\axi_pi_output_reg_reg[13]_inv_i_41_n_2 ,\axi_pi_output_reg_reg[13]_inv_i_41_n_3 }),
        .CYINIT(1'b0),
        .DI({\axi_pi_output_reg[13]_inv_i_59_n_0 ,\axi_pi_output_reg[13]_inv_i_60_n_0 ,\axi_pi_output_reg[13]_inv_i_61_n_0 ,\axi_pi_output_reg[13]_inv_i_62_n_0 }),
        .O(\NLW_axi_pi_output_reg_reg[13]_inv_i_41_O_UNCONNECTED [3:0]),
        .S({\axi_pi_output_reg[13]_inv_i_63_n_0 ,\axi_pi_output_reg[13]_inv_i_64_n_0 ,\axi_pi_output_reg[13]_inv_i_65_n_0 ,\axi_pi_output_reg[13]_inv_i_66_n_0 }));
  CARRY4 \axi_pi_output_reg_reg[13]_inv_i_5 
       (.CI(\axi_pi_output_reg_reg[13]_inv_i_23_n_0 ),
        .CO({\axi_pi_output_reg_reg[13]_inv_i_5_n_0 ,\axi_pi_output_reg_reg[13]_inv_i_5_n_1 ,\axi_pi_output_reg_reg[13]_inv_i_5_n_2 ,\axi_pi_output_reg_reg[13]_inv_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({\axi_pi_output_reg[13]_inv_i_24_n_0 ,\axi_pi_output_reg[13]_inv_i_25_n_0 ,\axi_pi_output_reg[13]_inv_i_26_n_0 ,\axi_pi_output_reg[13]_inv_i_27_n_0 }),
        .O(\NLW_axi_pi_output_reg_reg[13]_inv_i_5_O_UNCONNECTED [3:0]),
        .S({\axi_pi_output_reg[13]_inv_i_28_n_0 ,\axi_pi_output_reg[13]_inv_i_29_n_0 ,\axi_pi_output_reg[13]_inv_i_30_n_0 ,\axi_pi_output_reg[13]_inv_i_31_n_0 }));
  CARRY4 \axi_pi_output_reg_reg[13]_inv_i_50 
       (.CI(1'b0),
        .CO({\axi_pi_output_reg_reg[13]_inv_i_50_n_0 ,\axi_pi_output_reg_reg[13]_inv_i_50_n_1 ,\axi_pi_output_reg_reg[13]_inv_i_50_n_2 ,\axi_pi_output_reg_reg[13]_inv_i_50_n_3 }),
        .CYINIT(1'b0),
        .DI({\axi_pi_output_reg[13]_inv_i_67_n_0 ,\axi_pi_output_reg[13]_inv_i_68_n_0 ,\axi_pi_output_reg[13]_inv_i_69_n_0 ,\axi_pi_output_reg[13]_inv_i_70_n_0 }),
        .O(\NLW_axi_pi_output_reg_reg[13]_inv_i_50_O_UNCONNECTED [3:0]),
        .S({\axi_pi_output_reg[13]_inv_i_71_n_0 ,\axi_pi_output_reg[13]_inv_i_72_n_0 ,\axi_pi_output_reg[13]_inv_i_73_n_0 ,\axi_pi_output_reg[13]_inv_i_74_n_0 }));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[11]_i_2 
       (.I0(setpoint_reg[11]),
        .I1(pid_loop_0_input_reg[11]),
        .O(\error_reg[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[11]_i_3 
       (.I0(setpoint_reg[10]),
        .I1(pid_loop_0_input_reg[10]),
        .O(\error_reg[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[11]_i_4 
       (.I0(setpoint_reg[9]),
        .I1(pid_loop_0_input_reg[9]),
        .O(\error_reg[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[11]_i_5 
       (.I0(setpoint_reg[8]),
        .I1(pid_loop_0_input_reg[8]),
        .O(\error_reg[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAABABABAFFFFFFFF)) 
    \error_reg[13]_i_1 
       (.I0(\I_term_q_reg[0]_0 [0]),
        .I1(\I_term_q_reg[0]_0 [1]),
        .I2(\I_term_q_reg[0]_0 [2]),
        .I3(CO),
        .I4(\autolock_max_reg_reg[30] ),
        .I5(s00_axi_aresetn),
        .O(\control_reg_reg[1] ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[13]_i_3 
       (.I0(setpoint_reg[13]),
        .I1(pid_loop_0_input_reg[13]),
        .O(\error_reg[13]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[13]_i_4 
       (.I0(setpoint_reg[12]),
        .I1(pid_loop_0_input_reg[12]),
        .O(\error_reg[13]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[3]_i_2 
       (.I0(setpoint_reg[3]),
        .I1(pid_loop_0_input_reg[3]),
        .O(\error_reg[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[3]_i_3 
       (.I0(setpoint_reg[2]),
        .I1(pid_loop_0_input_reg[2]),
        .O(\error_reg[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[3]_i_4 
       (.I0(setpoint_reg[1]),
        .I1(pid_loop_0_input_reg[1]),
        .O(\error_reg[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[3]_i_5 
       (.I0(setpoint_reg[0]),
        .I1(pid_loop_0_input_reg[0]),
        .O(\error_reg[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[7]_i_2 
       (.I0(setpoint_reg[7]),
        .I1(pid_loop_0_input_reg[7]),
        .O(\error_reg[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[7]_i_3 
       (.I0(setpoint_reg[6]),
        .I1(pid_loop_0_input_reg[6]),
        .O(\error_reg[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[7]_i_4 
       (.I0(setpoint_reg[5]),
        .I1(pid_loop_0_input_reg[5]),
        .O(\error_reg[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \error_reg[7]_i_5 
       (.I0(setpoint_reg[4]),
        .I1(pid_loop_0_input_reg[4]),
        .O(\error_reg[7]_i_5_n_0 ));
  FDRE \error_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[0]),
        .Q(\error_reg_reg[13]_0 [0]),
        .R(\control_reg_reg[1] ));
  FDRE \error_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[10]),
        .Q(\error_reg_reg[13]_0 [10]),
        .R(\control_reg_reg[1] ));
  FDRE \error_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[11]),
        .Q(\error_reg_reg[13]_0 [11]),
        .R(\control_reg_reg[1] ));
  CARRY4 \error_reg_reg[11]_i_1 
       (.CI(\error_reg_reg[7]_i_1_n_0 ),
        .CO({\error_reg_reg[11]_i_1_n_0 ,\error_reg_reg[11]_i_1_n_1 ,\error_reg_reg[11]_i_1_n_2 ,\error_reg_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(setpoint_reg[11:8]),
        .O(error_reg0[11:8]),
        .S({\error_reg[11]_i_2_n_0 ,\error_reg[11]_i_3_n_0 ,\error_reg[11]_i_4_n_0 ,\error_reg[11]_i_5_n_0 }));
  FDRE \error_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[12]),
        .Q(\error_reg_reg[13]_0 [12]),
        .R(\control_reg_reg[1] ));
  FDRE \error_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[13]),
        .Q(\error_reg_reg[13]_0 [13]),
        .R(\control_reg_reg[1] ));
  CARRY4 \error_reg_reg[13]_i_2 
       (.CI(\error_reg_reg[11]_i_1_n_0 ),
        .CO({\NLW_error_reg_reg[13]_i_2_CO_UNCONNECTED [3:1],\error_reg_reg[13]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,setpoint_reg[12]}),
        .O({\NLW_error_reg_reg[13]_i_2_O_UNCONNECTED [3:2],error_reg0[13:12]}),
        .S({1'b0,1'b0,\error_reg[13]_i_3_n_0 ,\error_reg[13]_i_4_n_0 }));
  FDRE \error_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[1]),
        .Q(\error_reg_reg[13]_0 [1]),
        .R(\control_reg_reg[1] ));
  FDRE \error_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[2]),
        .Q(\error_reg_reg[13]_0 [2]),
        .R(\control_reg_reg[1] ));
  FDRE \error_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[3]),
        .Q(\error_reg_reg[13]_0 [3]),
        .R(\control_reg_reg[1] ));
  CARRY4 \error_reg_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\error_reg_reg[3]_i_1_n_0 ,\error_reg_reg[3]_i_1_n_1 ,\error_reg_reg[3]_i_1_n_2 ,\error_reg_reg[3]_i_1_n_3 }),
        .CYINIT(1'b1),
        .DI(setpoint_reg[3:0]),
        .O(error_reg0[3:0]),
        .S({\error_reg[3]_i_2_n_0 ,\error_reg[3]_i_3_n_0 ,\error_reg[3]_i_4_n_0 ,\error_reg[3]_i_5_n_0 }));
  FDRE \error_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[4]),
        .Q(\error_reg_reg[13]_0 [4]),
        .R(\control_reg_reg[1] ));
  FDRE \error_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[5]),
        .Q(\error_reg_reg[13]_0 [5]),
        .R(\control_reg_reg[1] ));
  FDRE \error_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[6]),
        .Q(\error_reg_reg[13]_0 [6]),
        .R(\control_reg_reg[1] ));
  FDRE \error_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[7]),
        .Q(\error_reg_reg[13]_0 [7]),
        .R(\control_reg_reg[1] ));
  CARRY4 \error_reg_reg[7]_i_1 
       (.CI(\error_reg_reg[3]_i_1_n_0 ),
        .CO({\error_reg_reg[7]_i_1_n_0 ,\error_reg_reg[7]_i_1_n_1 ,\error_reg_reg[7]_i_1_n_2 ,\error_reg_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(setpoint_reg[7:4]),
        .O(error_reg0[7:4]),
        .S({\error_reg[7]_i_2_n_0 ,\error_reg[7]_i_3_n_0 ,\error_reg[7]_i_4_n_0 ,\error_reg[7]_i_5_n_0 }));
  FDRE \error_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[8]),
        .Q(\error_reg_reg[13]_0 [8]),
        .R(\control_reg_reg[1] ));
  FDRE \error_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(error_reg0[9]),
        .Q(\error_reg_reg[13]_0 [9]),
        .R(\control_reg_reg[1] ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_10 
       (.I0(D[13]),
        .I1(I_term_mon[13]),
        .O(\loop_output_reg[19]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_11 
       (.I0(D[12]),
        .I1(I_term_mon[12]),
        .O(\loop_output_reg[19]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_13 
       (.I0(D[11]),
        .I1(I_term_mon[11]),
        .O(\loop_output_reg[19]_i_13_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_14 
       (.I0(D[10]),
        .I1(I_term_mon[10]),
        .O(\loop_output_reg[19]_i_14_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_15 
       (.I0(D[9]),
        .I1(I_term_mon[9]),
        .O(\loop_output_reg[19]_i_15_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_16 
       (.I0(D[8]),
        .I1(I_term_mon[8]),
        .O(\loop_output_reg[19]_i_16_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_18 
       (.I0(D[7]),
        .I1(I_term_mon[7]),
        .O(\loop_output_reg[19]_i_18_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_19 
       (.I0(D[6]),
        .I1(I_term_mon[6]),
        .O(\loop_output_reg[19]_i_19_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_20 
       (.I0(D[5]),
        .I1(I_term_mon[5]),
        .O(\loop_output_reg[19]_i_20_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_21 
       (.I0(D[4]),
        .I1(I_term_mon[4]),
        .O(\loop_output_reg[19]_i_21_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_22 
       (.I0(D[3]),
        .I1(I_term_mon[3]),
        .O(\loop_output_reg[19]_i_22_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_23 
       (.I0(D[2]),
        .I1(I_term_mon[2]),
        .O(\loop_output_reg[19]_i_23_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_24 
       (.I0(D[1]),
        .I1(I_term_mon[1]),
        .O(\loop_output_reg[19]_i_24_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_25 
       (.I0(D[0]),
        .I1(I_term_mon[0]),
        .O(\loop_output_reg[19]_i_25_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_3 
       (.I0(D[19]),
        .I1(I_term_mon[19]),
        .O(\loop_output_reg[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_4 
       (.I0(D[18]),
        .I1(I_term_mon[18]),
        .O(\loop_output_reg[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_5 
       (.I0(D[17]),
        .I1(I_term_mon[17]),
        .O(\loop_output_reg[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_6 
       (.I0(D[16]),
        .I1(I_term_mon[16]),
        .O(\loop_output_reg[19]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_8 
       (.I0(D[15]),
        .I1(I_term_mon[15]),
        .O(\loop_output_reg[19]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[19]_i_9 
       (.I0(D[14]),
        .I1(I_term_mon[14]),
        .O(\loop_output_reg[19]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[23]_i_2 
       (.I0(D[23]),
        .I1(I_term_mon[23]),
        .O(\loop_output_reg[23]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[23]_i_3 
       (.I0(D[22]),
        .I1(I_term_mon[22]),
        .O(\loop_output_reg[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[23]_i_4 
       (.I0(D[21]),
        .I1(I_term_mon[21]),
        .O(\loop_output_reg[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[23]_i_5 
       (.I0(D[20]),
        .I1(I_term_mon[20]),
        .O(\loop_output_reg[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[27]_i_2 
       (.I0(D[27]),
        .I1(I_term_mon[27]),
        .O(\loop_output_reg[27]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[27]_i_3 
       (.I0(D[26]),
        .I1(I_term_mon[26]),
        .O(\loop_output_reg[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[27]_i_4 
       (.I0(D[25]),
        .I1(I_term_mon[25]),
        .O(\loop_output_reg[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[27]_i_5 
       (.I0(D[24]),
        .I1(I_term_mon[24]),
        .O(\loop_output_reg[27]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[31]_i_2 
       (.I0(D[31]),
        .I1(I_term_mon[31]),
        .O(\loop_output_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[31]_i_3 
       (.I0(D[30]),
        .I1(I_term_mon[30]),
        .O(\loop_output_reg[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[31]_i_4 
       (.I0(D[29]),
        .I1(I_term_mon[29]),
        .O(\loop_output_reg[31]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \loop_output_reg[31]_i_5 
       (.I0(D[28]),
        .I1(I_term_mon[28]),
        .O(\loop_output_reg[31]_i_5_n_0 ));
  FDRE \loop_output_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[19]_i_1_n_5 ),
        .Q(\loop_output_reg_reg[31]_0 [0]),
        .R(\control_reg_reg[1] ));
  FDRE \loop_output_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[19]_i_1_n_4 ),
        .Q(\loop_output_reg_reg[31]_0 [1]),
        .R(\control_reg_reg[1] ));
  CARRY4 \loop_output_reg_reg[19]_i_1 
       (.CI(\loop_output_reg_reg[19]_i_2_n_0 ),
        .CO({\loop_output_reg_reg[19]_i_1_n_0 ,\loop_output_reg_reg[19]_i_1_n_1 ,\loop_output_reg_reg[19]_i_1_n_2 ,\loop_output_reg_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(D[19:16]),
        .O({\loop_output_reg_reg[19]_i_1_n_4 ,\loop_output_reg_reg[19]_i_1_n_5 ,\NLW_loop_output_reg_reg[19]_i_1_O_UNCONNECTED [1:0]}),
        .S({\loop_output_reg[19]_i_3_n_0 ,\loop_output_reg[19]_i_4_n_0 ,\loop_output_reg[19]_i_5_n_0 ,\loop_output_reg[19]_i_6_n_0 }));
  CARRY4 \loop_output_reg_reg[19]_i_12 
       (.CI(\loop_output_reg_reg[19]_i_17_n_0 ),
        .CO({\loop_output_reg_reg[19]_i_12_n_0 ,\loop_output_reg_reg[19]_i_12_n_1 ,\loop_output_reg_reg[19]_i_12_n_2 ,\loop_output_reg_reg[19]_i_12_n_3 }),
        .CYINIT(1'b0),
        .DI(D[7:4]),
        .O(\NLW_loop_output_reg_reg[19]_i_12_O_UNCONNECTED [3:0]),
        .S({\loop_output_reg[19]_i_18_n_0 ,\loop_output_reg[19]_i_19_n_0 ,\loop_output_reg[19]_i_20_n_0 ,\loop_output_reg[19]_i_21_n_0 }));
  CARRY4 \loop_output_reg_reg[19]_i_17 
       (.CI(1'b0),
        .CO({\loop_output_reg_reg[19]_i_17_n_0 ,\loop_output_reg_reg[19]_i_17_n_1 ,\loop_output_reg_reg[19]_i_17_n_2 ,\loop_output_reg_reg[19]_i_17_n_3 }),
        .CYINIT(1'b0),
        .DI(D[3:0]),
        .O(\NLW_loop_output_reg_reg[19]_i_17_O_UNCONNECTED [3:0]),
        .S({\loop_output_reg[19]_i_22_n_0 ,\loop_output_reg[19]_i_23_n_0 ,\loop_output_reg[19]_i_24_n_0 ,\loop_output_reg[19]_i_25_n_0 }));
  CARRY4 \loop_output_reg_reg[19]_i_2 
       (.CI(\loop_output_reg_reg[19]_i_7_n_0 ),
        .CO({\loop_output_reg_reg[19]_i_2_n_0 ,\loop_output_reg_reg[19]_i_2_n_1 ,\loop_output_reg_reg[19]_i_2_n_2 ,\loop_output_reg_reg[19]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI(D[15:12]),
        .O(\NLW_loop_output_reg_reg[19]_i_2_O_UNCONNECTED [3:0]),
        .S({\loop_output_reg[19]_i_8_n_0 ,\loop_output_reg[19]_i_9_n_0 ,\loop_output_reg[19]_i_10_n_0 ,\loop_output_reg[19]_i_11_n_0 }));
  CARRY4 \loop_output_reg_reg[19]_i_7 
       (.CI(\loop_output_reg_reg[19]_i_12_n_0 ),
        .CO({\loop_output_reg_reg[19]_i_7_n_0 ,\loop_output_reg_reg[19]_i_7_n_1 ,\loop_output_reg_reg[19]_i_7_n_2 ,\loop_output_reg_reg[19]_i_7_n_3 }),
        .CYINIT(1'b0),
        .DI(D[11:8]),
        .O(\NLW_loop_output_reg_reg[19]_i_7_O_UNCONNECTED [3:0]),
        .S({\loop_output_reg[19]_i_13_n_0 ,\loop_output_reg[19]_i_14_n_0 ,\loop_output_reg[19]_i_15_n_0 ,\loop_output_reg[19]_i_16_n_0 }));
  FDRE \loop_output_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[23]_i_1_n_7 ),
        .Q(\loop_output_reg_reg[31]_0 [2]),
        .R(\control_reg_reg[1] ));
  FDRE \loop_output_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[23]_i_1_n_6 ),
        .Q(\loop_output_reg_reg[31]_0 [3]),
        .R(\control_reg_reg[1] ));
  FDRE \loop_output_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[23]_i_1_n_5 ),
        .Q(\loop_output_reg_reg[31]_0 [4]),
        .R(\control_reg_reg[1] ));
  FDRE \loop_output_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[23]_i_1_n_4 ),
        .Q(\loop_output_reg_reg[31]_0 [5]),
        .R(\control_reg_reg[1] ));
  CARRY4 \loop_output_reg_reg[23]_i_1 
       (.CI(\loop_output_reg_reg[19]_i_1_n_0 ),
        .CO({\loop_output_reg_reg[23]_i_1_n_0 ,\loop_output_reg_reg[23]_i_1_n_1 ,\loop_output_reg_reg[23]_i_1_n_2 ,\loop_output_reg_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(D[23:20]),
        .O({\loop_output_reg_reg[23]_i_1_n_4 ,\loop_output_reg_reg[23]_i_1_n_5 ,\loop_output_reg_reg[23]_i_1_n_6 ,\loop_output_reg_reg[23]_i_1_n_7 }),
        .S({\loop_output_reg[23]_i_2_n_0 ,\loop_output_reg[23]_i_3_n_0 ,\loop_output_reg[23]_i_4_n_0 ,\loop_output_reg[23]_i_5_n_0 }));
  FDRE \loop_output_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[27]_i_1_n_7 ),
        .Q(\loop_output_reg_reg[31]_0 [6]),
        .R(\control_reg_reg[1] ));
  FDRE \loop_output_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[27]_i_1_n_6 ),
        .Q(\loop_output_reg_reg[31]_0 [7]),
        .R(\control_reg_reg[1] ));
  FDRE \loop_output_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[27]_i_1_n_5 ),
        .Q(\loop_output_reg_reg[31]_0 [8]),
        .R(\control_reg_reg[1] ));
  FDRE \loop_output_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[27]_i_1_n_4 ),
        .Q(\loop_output_reg_reg[31]_0 [9]),
        .R(\control_reg_reg[1] ));
  CARRY4 \loop_output_reg_reg[27]_i_1 
       (.CI(\loop_output_reg_reg[23]_i_1_n_0 ),
        .CO({\loop_output_reg_reg[27]_i_1_n_0 ,\loop_output_reg_reg[27]_i_1_n_1 ,\loop_output_reg_reg[27]_i_1_n_2 ,\loop_output_reg_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(D[27:24]),
        .O({\loop_output_reg_reg[27]_i_1_n_4 ,\loop_output_reg_reg[27]_i_1_n_5 ,\loop_output_reg_reg[27]_i_1_n_6 ,\loop_output_reg_reg[27]_i_1_n_7 }),
        .S({\loop_output_reg[27]_i_2_n_0 ,\loop_output_reg[27]_i_3_n_0 ,\loop_output_reg[27]_i_4_n_0 ,\loop_output_reg[27]_i_5_n_0 }));
  FDRE \loop_output_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[31]_i_1_n_7 ),
        .Q(\loop_output_reg_reg[31]_0 [10]),
        .R(\control_reg_reg[1] ));
  FDRE \loop_output_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[31]_i_1_n_6 ),
        .Q(\loop_output_reg_reg[31]_0 [11]),
        .R(\control_reg_reg[1] ));
  FDRE \loop_output_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[31]_i_1_n_5 ),
        .Q(\loop_output_reg_reg[31]_0 [12]),
        .R(\control_reg_reg[1] ));
  FDRE \loop_output_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\loop_output_reg_reg[31]_i_1_n_4 ),
        .Q(\loop_output_reg_reg[31]_0 [13]),
        .R(\control_reg_reg[1] ));
  CARRY4 \loop_output_reg_reg[31]_i_1 
       (.CI(\loop_output_reg_reg[27]_i_1_n_0 ),
        .CO({\NLW_loop_output_reg_reg[31]_i_1_CO_UNCONNECTED [3],\loop_output_reg_reg[31]_i_1_n_1 ,\loop_output_reg_reg[31]_i_1_n_2 ,\loop_output_reg_reg[31]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,D[30:28]}),
        .O({\loop_output_reg_reg[31]_i_1_n_4 ,\loop_output_reg_reg[31]_i_1_n_5 ,\loop_output_reg_reg[31]_i_1_n_6 ,\loop_output_reg_reg[31]_i_1_n_7 }),
        .S({\loop_output_reg[31]_i_2_n_0 ,\loop_output_reg[31]_i_3_n_0 ,\loop_output_reg[31]_i_4_n_0 ,\loop_output_reg[31]_i_5_n_0 }));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[21]_i_3 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [3]),
        .I1(\loop_output_reg_reg[31]_0 [2]),
        .O(\ramplitude_min_reg[21]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[21]_i_4 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [2]),
        .I1(\loop_output_reg_reg[31]_0 [1]),
        .O(\ramplitude_min_reg[21]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[21]_i_5 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [1]),
        .I1(\loop_output_reg_reg[31]_0 [0]),
        .O(\ramplitude_min_reg[21]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[25]_i_3 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [7]),
        .I1(\loop_output_reg_reg[31]_0 [6]),
        .O(\ramplitude_min_reg[25]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[25]_i_4 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [6]),
        .I1(\loop_output_reg_reg[31]_0 [5]),
        .O(\ramplitude_min_reg[25]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[25]_i_5 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [5]),
        .I1(\loop_output_reg_reg[31]_0 [4]),
        .O(\ramplitude_min_reg[25]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[25]_i_6 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [4]),
        .I1(\loop_output_reg_reg[31]_0 [3]),
        .O(\ramplitude_min_reg[25]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[29]_i_3 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [11]),
        .I1(\loop_output_reg_reg[31]_0 [10]),
        .O(\ramplitude_min_reg[29]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[29]_i_4 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [10]),
        .I1(\loop_output_reg_reg[31]_0 [9]),
        .O(\ramplitude_min_reg[29]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[29]_i_5 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [9]),
        .I1(\loop_output_reg_reg[31]_0 [8]),
        .O(\ramplitude_min_reg[29]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[29]_i_6 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [8]),
        .I1(\loop_output_reg_reg[31]_0 [7]),
        .O(\ramplitude_min_reg[29]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[31]_i_31 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [13]),
        .I1(\loop_output_reg_reg[31]_0 [12]),
        .O(\ramplitude_min_reg[31]_i_31_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_min_reg[31]_i_32 
       (.I0(\ramplitude_min_reg_reg[31]_i_6_0 [12]),
        .I1(\loop_output_reg_reg[31]_0 [11]),
        .O(\ramplitude_min_reg[31]_i_32_n_0 ));
  CARRY4 \ramplitude_min_reg_reg[21]_i_2 
       (.CI(1'b0),
        .CO({\ramplitude_min_reg_reg[21]_i_2_n_0 ,\ramplitude_min_reg_reg[21]_i_2_n_1 ,\ramplitude_min_reg_reg[21]_i_2_n_2 ,\ramplitude_min_reg_reg[21]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg_reg[31]_i_6_0 [3:1],1'b0}),
        .O(ramp_module_0_reset[3:0]),
        .S({\ramplitude_min_reg[21]_i_3_n_0 ,\ramplitude_min_reg[21]_i_4_n_0 ,\ramplitude_min_reg[21]_i_5_n_0 ,\ramplitude_min_reg_reg[31]_i_6_0 [0]}));
  CARRY4 \ramplitude_min_reg_reg[25]_i_2 
       (.CI(\ramplitude_min_reg_reg[21]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[25]_i_2_n_0 ,\ramplitude_min_reg_reg[25]_i_2_n_1 ,\ramplitude_min_reg_reg[25]_i_2_n_2 ,\ramplitude_min_reg_reg[25]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI(\ramplitude_min_reg_reg[31]_i_6_0 [7:4]),
        .O(ramp_module_0_reset[7:4]),
        .S({\ramplitude_min_reg[25]_i_3_n_0 ,\ramplitude_min_reg[25]_i_4_n_0 ,\ramplitude_min_reg[25]_i_5_n_0 ,\ramplitude_min_reg[25]_i_6_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[29]_i_2 
       (.CI(\ramplitude_min_reg_reg[25]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[29]_i_2_n_0 ,\ramplitude_min_reg_reg[29]_i_2_n_1 ,\ramplitude_min_reg_reg[29]_i_2_n_2 ,\ramplitude_min_reg_reg[29]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI(\ramplitude_min_reg_reg[31]_i_6_0 [11:8]),
        .O(ramp_module_0_reset[11:8]),
        .S({\ramplitude_min_reg[29]_i_3_n_0 ,\ramplitude_min_reg[29]_i_4_n_0 ,\ramplitude_min_reg[29]_i_5_n_0 ,\ramplitude_min_reg[29]_i_6_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_6 
       (.CI(\ramplitude_min_reg_reg[29]_i_2_n_0 ),
        .CO({\NLW_ramplitude_min_reg_reg[31]_i_6_CO_UNCONNECTED [3:1],\ramplitude_min_reg_reg[31]_i_6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\ramplitude_min_reg_reg[31]_i_6_0 [12]}),
        .O({\NLW_ramplitude_min_reg_reg[31]_i_6_O_UNCONNECTED [3:2],ramp_module_0_reset[13:12]}),
        .S({1'b0,1'b0,\ramplitude_min_reg[31]_i_31_n_0 ,\ramplitude_min_reg[31]_i_32_n_0 }));
  FDRE \setpoint_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [0]),
        .Q(setpoint_reg[0]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [10]),
        .Q(setpoint_reg[10]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [11]),
        .Q(setpoint_reg[11]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [12]),
        .Q(setpoint_reg[12]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [13]),
        .Q(setpoint_reg[13]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [1]),
        .Q(setpoint_reg[1]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [2]),
        .Q(setpoint_reg[2]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [3]),
        .Q(setpoint_reg[3]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [4]),
        .Q(setpoint_reg[4]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [5]),
        .Q(setpoint_reg[5]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [6]),
        .Q(setpoint_reg[6]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [7]),
        .Q(setpoint_reg[7]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [8]),
        .Q(setpoint_reg[8]),
        .R(1'b0));
  FDRE \setpoint_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\setpoint_reg_reg[13]_0 [9]),
        .Q(setpoint_reg[9]),
        .R(1'b0));
endmodule

module system_AXI_PI_0_2_ramp_module
   (ramp_output,
    \ramp_reg_reg[31]_0 ,
    \ramp_reg_reg[30]_0 ,
    \ramp_reg_reg[31]_1 ,
    ramp_module_0_rst,
    \loop_locked_counter_reg_reg[30] ,
    E,
    ramp_corner_reg_reg_0,
    ramp_corner_reg_reg_1,
    D,
    \I_term_q_reg[31] ,
    \axi_pi_output_reg_reg[13]_inv ,
    \axi_pi_output_reg_reg[0] ,
    CO,
    s00_axi_aresetn,
    Q,
    loop_locked_counter_reg_reg,
    \status_reg_reg[10] ,
    \autolock_input_mindex_reg[0] ,
    \autolock_input_maxdex_reg[0] ,
    \ramp_step_reg_reg[31]_0 ,
    s00_axi_aclk,
    \ramplitude_lim_reg_reg[31]_0 ,
    \ramplitude_step_reg_reg[31]_0 ,
    ramp_module_0_reset,
    \ramp_reg_reg[17]_0 );
  output [13:0]ramp_output;
  output [0:0]\ramp_reg_reg[31]_0 ;
  output [12:0]\ramp_reg_reg[30]_0 ;
  output \ramp_reg_reg[31]_1 ;
  output ramp_module_0_rst;
  output [0:0]\loop_locked_counter_reg_reg[30] ;
  output [0:0]E;
  output [0:0]ramp_corner_reg_reg_0;
  output [0:0]ramp_corner_reg_reg_1;
  input [3:0]D;
  input [0:0]\I_term_q_reg[31] ;
  input [13:0]\axi_pi_output_reg_reg[13]_inv ;
  input [0:0]\axi_pi_output_reg_reg[0] ;
  input [0:0]CO;
  input s00_axi_aresetn;
  input [0:0]Q;
  input [31:0]loop_locked_counter_reg_reg;
  input [31:0]\status_reg_reg[10] ;
  input [0:0]\autolock_input_mindex_reg[0] ;
  input [0:0]\autolock_input_maxdex_reg[0] ;
  input [31:0]\ramp_step_reg_reg[31]_0 ;
  input s00_axi_aclk;
  input [31:0]\ramplitude_lim_reg_reg[31]_0 ;
  input [31:0]\ramplitude_step_reg_reg[31]_0 ;
  input [13:0]ramp_module_0_reset;
  input [17:0]\ramp_reg_reg[17]_0 ;

  wire [0:0]CO;
  wire [3:0]D;
  wire [0:0]E;
  wire [0:0]\I_term_q_reg[31] ;
  wire [0:0]Q;
  wire [0:0]\autolock_input_maxdex_reg[0] ;
  wire [0:0]\autolock_input_mindex_reg[0] ;
  wire [0:0]\axi_pi_output_reg_reg[0] ;
  wire [13:0]\axi_pi_output_reg_reg[13]_inv ;
  wire [31:1]data0;
  wire [31:0]data2;
  wire loop_locked_INST_0_i_10_n_0;
  wire loop_locked_INST_0_i_10_n_1;
  wire loop_locked_INST_0_i_10_n_2;
  wire loop_locked_INST_0_i_10_n_3;
  wire loop_locked_INST_0_i_11_n_0;
  wire loop_locked_INST_0_i_12_n_0;
  wire loop_locked_INST_0_i_13_n_0;
  wire loop_locked_INST_0_i_14_n_0;
  wire loop_locked_INST_0_i_15_n_0;
  wire loop_locked_INST_0_i_16_n_0;
  wire loop_locked_INST_0_i_17_n_0;
  wire loop_locked_INST_0_i_18_n_0;
  wire loop_locked_INST_0_i_19_n_0;
  wire loop_locked_INST_0_i_19_n_1;
  wire loop_locked_INST_0_i_19_n_2;
  wire loop_locked_INST_0_i_19_n_3;
  wire loop_locked_INST_0_i_1_n_0;
  wire loop_locked_INST_0_i_1_n_1;
  wire loop_locked_INST_0_i_1_n_2;
  wire loop_locked_INST_0_i_1_n_3;
  wire loop_locked_INST_0_i_20_n_0;
  wire loop_locked_INST_0_i_21_n_0;
  wire loop_locked_INST_0_i_22_n_0;
  wire loop_locked_INST_0_i_23_n_0;
  wire loop_locked_INST_0_i_24_n_0;
  wire loop_locked_INST_0_i_25_n_0;
  wire loop_locked_INST_0_i_26_n_0;
  wire loop_locked_INST_0_i_27_n_0;
  wire loop_locked_INST_0_i_28_n_0;
  wire loop_locked_INST_0_i_29_n_0;
  wire loop_locked_INST_0_i_2_n_0;
  wire loop_locked_INST_0_i_30_n_0;
  wire loop_locked_INST_0_i_31_n_0;
  wire loop_locked_INST_0_i_32_n_0;
  wire loop_locked_INST_0_i_33_n_0;
  wire loop_locked_INST_0_i_34_n_0;
  wire loop_locked_INST_0_i_35_n_0;
  wire loop_locked_INST_0_i_3_n_0;
  wire loop_locked_INST_0_i_4_n_0;
  wire loop_locked_INST_0_i_5_n_0;
  wire loop_locked_INST_0_i_6_n_0;
  wire loop_locked_INST_0_i_7_n_0;
  wire loop_locked_INST_0_i_8_n_0;
  wire loop_locked_INST_0_i_9_n_0;
  wire loop_locked_INST_0_n_1;
  wire loop_locked_INST_0_n_2;
  wire loop_locked_INST_0_n_3;
  wire [31:0]loop_locked_counter_reg_reg;
  wire [0:0]\loop_locked_counter_reg_reg[30] ;
  wire [30:0]p_0_in;
  wire [31:0]p_1_in;
  wire ramp_corner_reg_i_1_n_0;
  wire [0:0]ramp_corner_reg_reg_0;
  wire [0:0]ramp_corner_reg_reg_1;
  wire ramp_module_0_corner;
  wire [13:0]ramp_module_0_reset;
  wire ramp_module_0_rst;
  wire [13:0]ramp_output;
  wire [17:0]ramp_reg;
  wire \ramp_reg[0]_i_1_n_0 ;
  wire \ramp_reg[10]_i_1_n_0 ;
  wire \ramp_reg[11]_i_10_n_0 ;
  wire \ramp_reg[11]_i_11_n_0 ;
  wire \ramp_reg[11]_i_1_n_0 ;
  wire \ramp_reg[11]_i_4_n_0 ;
  wire \ramp_reg[11]_i_5_n_0 ;
  wire \ramp_reg[11]_i_6_n_0 ;
  wire \ramp_reg[11]_i_7_n_0 ;
  wire \ramp_reg[11]_i_8_n_0 ;
  wire \ramp_reg[11]_i_9_n_0 ;
  wire \ramp_reg[12]_i_1_n_0 ;
  wire \ramp_reg[13]_i_1_n_0 ;
  wire \ramp_reg[14]_i_1_n_0 ;
  wire \ramp_reg[15]_i_10_n_0 ;
  wire \ramp_reg[15]_i_11_n_0 ;
  wire \ramp_reg[15]_i_1_n_0 ;
  wire \ramp_reg[15]_i_4_n_0 ;
  wire \ramp_reg[15]_i_5_n_0 ;
  wire \ramp_reg[15]_i_6_n_0 ;
  wire \ramp_reg[15]_i_7_n_0 ;
  wire \ramp_reg[15]_i_8_n_0 ;
  wire \ramp_reg[15]_i_9_n_0 ;
  wire \ramp_reg[16]_i_1_n_0 ;
  wire \ramp_reg[17]_i_1_n_0 ;
  wire \ramp_reg[18]_i_1_n_0 ;
  wire \ramp_reg[19]_i_10_n_0 ;
  wire \ramp_reg[19]_i_11_n_0 ;
  wire \ramp_reg[19]_i_1_n_0 ;
  wire \ramp_reg[19]_i_4_n_0 ;
  wire \ramp_reg[19]_i_5_n_0 ;
  wire \ramp_reg[19]_i_6_n_0 ;
  wire \ramp_reg[19]_i_7_n_0 ;
  wire \ramp_reg[19]_i_8_n_0 ;
  wire \ramp_reg[19]_i_9_n_0 ;
  wire \ramp_reg[1]_i_1_n_0 ;
  wire \ramp_reg[20]_i_1_n_0 ;
  wire \ramp_reg[21]_i_1_n_0 ;
  wire \ramp_reg[22]_i_1_n_0 ;
  wire \ramp_reg[23]_i_10_n_0 ;
  wire \ramp_reg[23]_i_11_n_0 ;
  wire \ramp_reg[23]_i_1_n_0 ;
  wire \ramp_reg[23]_i_4_n_0 ;
  wire \ramp_reg[23]_i_5_n_0 ;
  wire \ramp_reg[23]_i_6_n_0 ;
  wire \ramp_reg[23]_i_7_n_0 ;
  wire \ramp_reg[23]_i_8_n_0 ;
  wire \ramp_reg[23]_i_9_n_0 ;
  wire \ramp_reg[24]_i_1_n_0 ;
  wire \ramp_reg[25]_i_1_n_0 ;
  wire \ramp_reg[26]_i_1_n_0 ;
  wire \ramp_reg[27]_i_10_n_0 ;
  wire \ramp_reg[27]_i_11_n_0 ;
  wire \ramp_reg[27]_i_1_n_0 ;
  wire \ramp_reg[27]_i_4_n_0 ;
  wire \ramp_reg[27]_i_5_n_0 ;
  wire \ramp_reg[27]_i_6_n_0 ;
  wire \ramp_reg[27]_i_7_n_0 ;
  wire \ramp_reg[27]_i_8_n_0 ;
  wire \ramp_reg[27]_i_9_n_0 ;
  wire \ramp_reg[28]_i_1_n_0 ;
  wire \ramp_reg[29]_i_1_n_0 ;
  wire \ramp_reg[2]_i_1_n_0 ;
  wire \ramp_reg[30]_i_1_n_0 ;
  wire \ramp_reg[31]_i_10_n_0 ;
  wire \ramp_reg[31]_i_11_n_0 ;
  wire \ramp_reg[31]_i_12_n_0 ;
  wire \ramp_reg[31]_i_1_n_0 ;
  wire \ramp_reg[31]_i_2_n_0 ;
  wire \ramp_reg[31]_i_5_n_0 ;
  wire \ramp_reg[31]_i_6_n_0 ;
  wire \ramp_reg[31]_i_7_n_0 ;
  wire \ramp_reg[31]_i_8_n_0 ;
  wire \ramp_reg[31]_i_9_n_0 ;
  wire \ramp_reg[3]_i_10_n_0 ;
  wire \ramp_reg[3]_i_11_n_0 ;
  wire \ramp_reg[3]_i_1_n_0 ;
  wire \ramp_reg[3]_i_4_n_0 ;
  wire \ramp_reg[3]_i_5_n_0 ;
  wire \ramp_reg[3]_i_6_n_0 ;
  wire \ramp_reg[3]_i_7_n_0 ;
  wire \ramp_reg[3]_i_8_n_0 ;
  wire \ramp_reg[3]_i_9_n_0 ;
  wire \ramp_reg[4]_i_1_n_0 ;
  wire \ramp_reg[5]_i_1_n_0 ;
  wire \ramp_reg[6]_i_1_n_0 ;
  wire \ramp_reg[7]_i_10_n_0 ;
  wire \ramp_reg[7]_i_11_n_0 ;
  wire \ramp_reg[7]_i_1_n_0 ;
  wire \ramp_reg[7]_i_4_n_0 ;
  wire \ramp_reg[7]_i_5_n_0 ;
  wire \ramp_reg[7]_i_6_n_0 ;
  wire \ramp_reg[7]_i_7_n_0 ;
  wire \ramp_reg[7]_i_8_n_0 ;
  wire \ramp_reg[7]_i_9_n_0 ;
  wire \ramp_reg[8]_i_1_n_0 ;
  wire \ramp_reg[9]_i_1_n_0 ;
  wire \ramp_reg_reg[11]_i_2_n_0 ;
  wire \ramp_reg_reg[11]_i_2_n_1 ;
  wire \ramp_reg_reg[11]_i_2_n_2 ;
  wire \ramp_reg_reg[11]_i_2_n_3 ;
  wire \ramp_reg_reg[11]_i_2_n_4 ;
  wire \ramp_reg_reg[11]_i_2_n_5 ;
  wire \ramp_reg_reg[11]_i_2_n_6 ;
  wire \ramp_reg_reg[11]_i_2_n_7 ;
  wire \ramp_reg_reg[11]_i_3_n_0 ;
  wire \ramp_reg_reg[11]_i_3_n_1 ;
  wire \ramp_reg_reg[11]_i_3_n_2 ;
  wire \ramp_reg_reg[11]_i_3_n_3 ;
  wire \ramp_reg_reg[15]_i_2_n_0 ;
  wire \ramp_reg_reg[15]_i_2_n_1 ;
  wire \ramp_reg_reg[15]_i_2_n_2 ;
  wire \ramp_reg_reg[15]_i_2_n_3 ;
  wire \ramp_reg_reg[15]_i_3_n_0 ;
  wire \ramp_reg_reg[15]_i_3_n_1 ;
  wire \ramp_reg_reg[15]_i_3_n_2 ;
  wire \ramp_reg_reg[15]_i_3_n_3 ;
  wire \ramp_reg_reg[15]_i_3_n_4 ;
  wire \ramp_reg_reg[15]_i_3_n_5 ;
  wire \ramp_reg_reg[15]_i_3_n_6 ;
  wire \ramp_reg_reg[15]_i_3_n_7 ;
  wire [17:0]\ramp_reg_reg[17]_0 ;
  wire \ramp_reg_reg[19]_i_2_n_0 ;
  wire \ramp_reg_reg[19]_i_2_n_1 ;
  wire \ramp_reg_reg[19]_i_2_n_2 ;
  wire \ramp_reg_reg[19]_i_2_n_3 ;
  wire \ramp_reg_reg[19]_i_2_n_4 ;
  wire \ramp_reg_reg[19]_i_2_n_5 ;
  wire \ramp_reg_reg[19]_i_2_n_6 ;
  wire \ramp_reg_reg[19]_i_2_n_7 ;
  wire \ramp_reg_reg[19]_i_3_n_0 ;
  wire \ramp_reg_reg[19]_i_3_n_1 ;
  wire \ramp_reg_reg[19]_i_3_n_2 ;
  wire \ramp_reg_reg[19]_i_3_n_3 ;
  wire \ramp_reg_reg[23]_i_2_n_0 ;
  wire \ramp_reg_reg[23]_i_2_n_1 ;
  wire \ramp_reg_reg[23]_i_2_n_2 ;
  wire \ramp_reg_reg[23]_i_2_n_3 ;
  wire \ramp_reg_reg[23]_i_2_n_4 ;
  wire \ramp_reg_reg[23]_i_2_n_5 ;
  wire \ramp_reg_reg[23]_i_2_n_6 ;
  wire \ramp_reg_reg[23]_i_2_n_7 ;
  wire \ramp_reg_reg[23]_i_3_n_0 ;
  wire \ramp_reg_reg[23]_i_3_n_1 ;
  wire \ramp_reg_reg[23]_i_3_n_2 ;
  wire \ramp_reg_reg[23]_i_3_n_3 ;
  wire \ramp_reg_reg[27]_i_2_n_0 ;
  wire \ramp_reg_reg[27]_i_2_n_1 ;
  wire \ramp_reg_reg[27]_i_2_n_2 ;
  wire \ramp_reg_reg[27]_i_2_n_3 ;
  wire \ramp_reg_reg[27]_i_2_n_4 ;
  wire \ramp_reg_reg[27]_i_2_n_5 ;
  wire \ramp_reg_reg[27]_i_2_n_6 ;
  wire \ramp_reg_reg[27]_i_2_n_7 ;
  wire \ramp_reg_reg[27]_i_3_n_0 ;
  wire \ramp_reg_reg[27]_i_3_n_1 ;
  wire \ramp_reg_reg[27]_i_3_n_2 ;
  wire \ramp_reg_reg[27]_i_3_n_3 ;
  wire [12:0]\ramp_reg_reg[30]_0 ;
  wire [0:0]\ramp_reg_reg[31]_0 ;
  wire \ramp_reg_reg[31]_1 ;
  wire \ramp_reg_reg[31]_i_3_n_1 ;
  wire \ramp_reg_reg[31]_i_3_n_2 ;
  wire \ramp_reg_reg[31]_i_3_n_3 ;
  wire \ramp_reg_reg[31]_i_4_n_1 ;
  wire \ramp_reg_reg[31]_i_4_n_2 ;
  wire \ramp_reg_reg[31]_i_4_n_3 ;
  wire \ramp_reg_reg[31]_i_4_n_4 ;
  wire \ramp_reg_reg[31]_i_4_n_5 ;
  wire \ramp_reg_reg[31]_i_4_n_6 ;
  wire \ramp_reg_reg[31]_i_4_n_7 ;
  wire \ramp_reg_reg[3]_i_2_n_0 ;
  wire \ramp_reg_reg[3]_i_2_n_1 ;
  wire \ramp_reg_reg[3]_i_2_n_2 ;
  wire \ramp_reg_reg[3]_i_2_n_3 ;
  wire \ramp_reg_reg[3]_i_2_n_4 ;
  wire \ramp_reg_reg[3]_i_2_n_5 ;
  wire \ramp_reg_reg[3]_i_2_n_6 ;
  wire \ramp_reg_reg[3]_i_2_n_7 ;
  wire \ramp_reg_reg[3]_i_3_n_0 ;
  wire \ramp_reg_reg[3]_i_3_n_1 ;
  wire \ramp_reg_reg[3]_i_3_n_2 ;
  wire \ramp_reg_reg[3]_i_3_n_3 ;
  wire \ramp_reg_reg[7]_i_2_n_0 ;
  wire \ramp_reg_reg[7]_i_2_n_1 ;
  wire \ramp_reg_reg[7]_i_2_n_2 ;
  wire \ramp_reg_reg[7]_i_2_n_3 ;
  wire \ramp_reg_reg[7]_i_2_n_4 ;
  wire \ramp_reg_reg[7]_i_2_n_5 ;
  wire \ramp_reg_reg[7]_i_2_n_6 ;
  wire \ramp_reg_reg[7]_i_2_n_7 ;
  wire \ramp_reg_reg[7]_i_3_n_0 ;
  wire \ramp_reg_reg[7]_i_3_n_1 ;
  wire \ramp_reg_reg[7]_i_3_n_2 ;
  wire \ramp_reg_reg[7]_i_3_n_3 ;
  wire [31:0]ramp_step_reg;
  wire [31:0]\ramp_step_reg_reg[31]_0 ;
  wire ramp_up_i_1_n_0;
  wire ramp_up_reg_n_0;
  wire [31:0]ramplitude_lim_reg;
  wire [31:0]\ramplitude_lim_reg_reg[31]_0 ;
  wire [31:0]ramplitude_max_reg0;
  wire ramplitude_max_reg1;
  wire \ramplitude_max_reg[11]_i_3_n_0 ;
  wire \ramplitude_max_reg[11]_i_4_n_0 ;
  wire \ramplitude_max_reg[11]_i_5_n_0 ;
  wire \ramplitude_max_reg[11]_i_6_n_0 ;
  wire \ramplitude_max_reg[15]_i_3_n_0 ;
  wire \ramplitude_max_reg[15]_i_4_n_0 ;
  wire \ramplitude_max_reg[15]_i_5_n_0 ;
  wire \ramplitude_max_reg[15]_i_6_n_0 ;
  wire \ramplitude_max_reg[19]_i_3_n_0 ;
  wire \ramplitude_max_reg[19]_i_4_n_0 ;
  wire \ramplitude_max_reg[19]_i_5_n_0 ;
  wire \ramplitude_max_reg[19]_i_6_n_0 ;
  wire \ramplitude_max_reg[23]_i_3_n_0 ;
  wire \ramplitude_max_reg[23]_i_4_n_0 ;
  wire \ramplitude_max_reg[23]_i_5_n_0 ;
  wire \ramplitude_max_reg[23]_i_6_n_0 ;
  wire \ramplitude_max_reg[27]_i_3_n_0 ;
  wire \ramplitude_max_reg[27]_i_4_n_0 ;
  wire \ramplitude_max_reg[27]_i_5_n_0 ;
  wire \ramplitude_max_reg[27]_i_6_n_0 ;
  wire \ramplitude_max_reg[31]_i_10_n_0 ;
  wire \ramplitude_max_reg[31]_i_11_n_0 ;
  wire \ramplitude_max_reg[31]_i_12_n_0 ;
  wire \ramplitude_max_reg[31]_i_13_n_0 ;
  wire \ramplitude_max_reg[31]_i_14_n_0 ;
  wire \ramplitude_max_reg[31]_i_15_n_0 ;
  wire \ramplitude_max_reg[31]_i_16_n_0 ;
  wire \ramplitude_max_reg[31]_i_17_n_0 ;
  wire \ramplitude_max_reg[31]_i_19_n_0 ;
  wire \ramplitude_max_reg[31]_i_1_n_0 ;
  wire \ramplitude_max_reg[31]_i_20_n_0 ;
  wire \ramplitude_max_reg[31]_i_21_n_0 ;
  wire \ramplitude_max_reg[31]_i_22_n_0 ;
  wire \ramplitude_max_reg[31]_i_23_n_0 ;
  wire \ramplitude_max_reg[31]_i_24_n_0 ;
  wire \ramplitude_max_reg[31]_i_25_n_0 ;
  wire \ramplitude_max_reg[31]_i_26_n_0 ;
  wire \ramplitude_max_reg[31]_i_28_n_0 ;
  wire \ramplitude_max_reg[31]_i_29_n_0 ;
  wire \ramplitude_max_reg[31]_i_30_n_0 ;
  wire \ramplitude_max_reg[31]_i_31_n_0 ;
  wire \ramplitude_max_reg[31]_i_32_n_0 ;
  wire \ramplitude_max_reg[31]_i_33_n_0 ;
  wire \ramplitude_max_reg[31]_i_34_n_0 ;
  wire \ramplitude_max_reg[31]_i_35_n_0 ;
  wire \ramplitude_max_reg[31]_i_36_n_0 ;
  wire \ramplitude_max_reg[31]_i_37_n_0 ;
  wire \ramplitude_max_reg[31]_i_38_n_0 ;
  wire \ramplitude_max_reg[31]_i_39_n_0 ;
  wire \ramplitude_max_reg[31]_i_40_n_0 ;
  wire \ramplitude_max_reg[31]_i_41_n_0 ;
  wire \ramplitude_max_reg[31]_i_42_n_0 ;
  wire \ramplitude_max_reg[31]_i_43_n_0 ;
  wire \ramplitude_max_reg[31]_i_6_n_0 ;
  wire \ramplitude_max_reg[31]_i_7_n_0 ;
  wire \ramplitude_max_reg[31]_i_8_n_0 ;
  wire \ramplitude_max_reg[31]_i_9_n_0 ;
  wire \ramplitude_max_reg[3]_i_3_n_0 ;
  wire \ramplitude_max_reg[3]_i_4_n_0 ;
  wire \ramplitude_max_reg[3]_i_5_n_0 ;
  wire \ramplitude_max_reg[3]_i_6_n_0 ;
  wire \ramplitude_max_reg[7]_i_3_n_0 ;
  wire \ramplitude_max_reg[7]_i_4_n_0 ;
  wire \ramplitude_max_reg[7]_i_5_n_0 ;
  wire \ramplitude_max_reg[7]_i_6_n_0 ;
  wire \ramplitude_max_reg_reg[11]_i_2_n_0 ;
  wire \ramplitude_max_reg_reg[11]_i_2_n_1 ;
  wire \ramplitude_max_reg_reg[11]_i_2_n_2 ;
  wire \ramplitude_max_reg_reg[11]_i_2_n_3 ;
  wire \ramplitude_max_reg_reg[15]_i_2_n_0 ;
  wire \ramplitude_max_reg_reg[15]_i_2_n_1 ;
  wire \ramplitude_max_reg_reg[15]_i_2_n_2 ;
  wire \ramplitude_max_reg_reg[15]_i_2_n_3 ;
  wire \ramplitude_max_reg_reg[19]_i_2_n_0 ;
  wire \ramplitude_max_reg_reg[19]_i_2_n_1 ;
  wire \ramplitude_max_reg_reg[19]_i_2_n_2 ;
  wire \ramplitude_max_reg_reg[19]_i_2_n_3 ;
  wire \ramplitude_max_reg_reg[23]_i_2_n_0 ;
  wire \ramplitude_max_reg_reg[23]_i_2_n_1 ;
  wire \ramplitude_max_reg_reg[23]_i_2_n_2 ;
  wire \ramplitude_max_reg_reg[23]_i_2_n_3 ;
  wire \ramplitude_max_reg_reg[27]_i_2_n_0 ;
  wire \ramplitude_max_reg_reg[27]_i_2_n_1 ;
  wire \ramplitude_max_reg_reg[27]_i_2_n_2 ;
  wire \ramplitude_max_reg_reg[27]_i_2_n_3 ;
  wire \ramplitude_max_reg_reg[31]_i_18_n_0 ;
  wire \ramplitude_max_reg_reg[31]_i_18_n_1 ;
  wire \ramplitude_max_reg_reg[31]_i_18_n_2 ;
  wire \ramplitude_max_reg_reg[31]_i_18_n_3 ;
  wire \ramplitude_max_reg_reg[31]_i_27_n_0 ;
  wire \ramplitude_max_reg_reg[31]_i_27_n_1 ;
  wire \ramplitude_max_reg_reg[31]_i_27_n_2 ;
  wire \ramplitude_max_reg_reg[31]_i_27_n_3 ;
  wire \ramplitude_max_reg_reg[31]_i_3_n_1 ;
  wire \ramplitude_max_reg_reg[31]_i_3_n_2 ;
  wire \ramplitude_max_reg_reg[31]_i_3_n_3 ;
  wire \ramplitude_max_reg_reg[31]_i_4_n_1 ;
  wire \ramplitude_max_reg_reg[31]_i_4_n_2 ;
  wire \ramplitude_max_reg_reg[31]_i_4_n_3 ;
  wire \ramplitude_max_reg_reg[31]_i_5_n_0 ;
  wire \ramplitude_max_reg_reg[31]_i_5_n_1 ;
  wire \ramplitude_max_reg_reg[31]_i_5_n_2 ;
  wire \ramplitude_max_reg_reg[31]_i_5_n_3 ;
  wire \ramplitude_max_reg_reg[3]_i_2_n_0 ;
  wire \ramplitude_max_reg_reg[3]_i_2_n_1 ;
  wire \ramplitude_max_reg_reg[3]_i_2_n_2 ;
  wire \ramplitude_max_reg_reg[3]_i_2_n_3 ;
  wire \ramplitude_max_reg_reg[7]_i_2_n_0 ;
  wire \ramplitude_max_reg_reg[7]_i_2_n_1 ;
  wire \ramplitude_max_reg_reg[7]_i_2_n_2 ;
  wire \ramplitude_max_reg_reg[7]_i_2_n_3 ;
  wire \ramplitude_max_reg_reg_n_0_[0] ;
  wire \ramplitude_max_reg_reg_n_0_[10] ;
  wire \ramplitude_max_reg_reg_n_0_[11] ;
  wire \ramplitude_max_reg_reg_n_0_[12] ;
  wire \ramplitude_max_reg_reg_n_0_[13] ;
  wire \ramplitude_max_reg_reg_n_0_[14] ;
  wire \ramplitude_max_reg_reg_n_0_[15] ;
  wire \ramplitude_max_reg_reg_n_0_[16] ;
  wire \ramplitude_max_reg_reg_n_0_[17] ;
  wire \ramplitude_max_reg_reg_n_0_[18] ;
  wire \ramplitude_max_reg_reg_n_0_[19] ;
  wire \ramplitude_max_reg_reg_n_0_[1] ;
  wire \ramplitude_max_reg_reg_n_0_[20] ;
  wire \ramplitude_max_reg_reg_n_0_[21] ;
  wire \ramplitude_max_reg_reg_n_0_[22] ;
  wire \ramplitude_max_reg_reg_n_0_[23] ;
  wire \ramplitude_max_reg_reg_n_0_[24] ;
  wire \ramplitude_max_reg_reg_n_0_[25] ;
  wire \ramplitude_max_reg_reg_n_0_[26] ;
  wire \ramplitude_max_reg_reg_n_0_[27] ;
  wire \ramplitude_max_reg_reg_n_0_[28] ;
  wire \ramplitude_max_reg_reg_n_0_[29] ;
  wire \ramplitude_max_reg_reg_n_0_[2] ;
  wire \ramplitude_max_reg_reg_n_0_[30] ;
  wire \ramplitude_max_reg_reg_n_0_[31] ;
  wire \ramplitude_max_reg_reg_n_0_[3] ;
  wire \ramplitude_max_reg_reg_n_0_[4] ;
  wire \ramplitude_max_reg_reg_n_0_[5] ;
  wire \ramplitude_max_reg_reg_n_0_[6] ;
  wire \ramplitude_max_reg_reg_n_0_[7] ;
  wire \ramplitude_max_reg_reg_n_0_[8] ;
  wire \ramplitude_max_reg_reg_n_0_[9] ;
  wire [31:0]ramplitude_min_reg0;
  wire ramplitude_min_reg1;
  wire \ramplitude_min_reg[0]_i_1_n_0 ;
  wire \ramplitude_min_reg[10]_i_1_n_0 ;
  wire \ramplitude_min_reg[11]_i_1_n_0 ;
  wire \ramplitude_min_reg[11]_i_3_n_0 ;
  wire \ramplitude_min_reg[11]_i_4_n_0 ;
  wire \ramplitude_min_reg[11]_i_5_n_0 ;
  wire \ramplitude_min_reg[11]_i_6_n_0 ;
  wire \ramplitude_min_reg[12]_i_1_n_0 ;
  wire \ramplitude_min_reg[13]_i_1_n_0 ;
  wire \ramplitude_min_reg[14]_i_1_n_0 ;
  wire \ramplitude_min_reg[15]_i_1_n_0 ;
  wire \ramplitude_min_reg[15]_i_3_n_0 ;
  wire \ramplitude_min_reg[15]_i_4_n_0 ;
  wire \ramplitude_min_reg[15]_i_5_n_0 ;
  wire \ramplitude_min_reg[15]_i_6_n_0 ;
  wire \ramplitude_min_reg[16]_i_1_n_0 ;
  wire \ramplitude_min_reg[17]_i_1_n_0 ;
  wire \ramplitude_min_reg[18]_i_1_n_0 ;
  wire \ramplitude_min_reg[19]_i_1_n_0 ;
  wire \ramplitude_min_reg[19]_i_3_n_0 ;
  wire \ramplitude_min_reg[19]_i_4_n_0 ;
  wire \ramplitude_min_reg[19]_i_5_n_0 ;
  wire \ramplitude_min_reg[19]_i_6_n_0 ;
  wire \ramplitude_min_reg[1]_i_1_n_0 ;
  wire \ramplitude_min_reg[20]_i_1_n_0 ;
  wire \ramplitude_min_reg[21]_i_1_n_0 ;
  wire \ramplitude_min_reg[22]_i_1_n_0 ;
  wire \ramplitude_min_reg[23]_i_1_n_0 ;
  wire \ramplitude_min_reg[23]_i_3_n_0 ;
  wire \ramplitude_min_reg[23]_i_4_n_0 ;
  wire \ramplitude_min_reg[23]_i_5_n_0 ;
  wire \ramplitude_min_reg[23]_i_6_n_0 ;
  wire \ramplitude_min_reg[24]_i_1_n_0 ;
  wire \ramplitude_min_reg[25]_i_1_n_0 ;
  wire \ramplitude_min_reg[26]_i_1_n_0 ;
  wire \ramplitude_min_reg[27]_i_1_n_0 ;
  wire \ramplitude_min_reg[27]_i_3_n_0 ;
  wire \ramplitude_min_reg[27]_i_4_n_0 ;
  wire \ramplitude_min_reg[27]_i_5_n_0 ;
  wire \ramplitude_min_reg[27]_i_6_n_0 ;
  wire \ramplitude_min_reg[28]_i_1_n_0 ;
  wire \ramplitude_min_reg[29]_i_1_n_0 ;
  wire \ramplitude_min_reg[2]_i_1_n_0 ;
  wire \ramplitude_min_reg[30]_i_1_n_0 ;
  wire \ramplitude_min_reg[31]_i_100_n_0 ;
  wire \ramplitude_min_reg[31]_i_101_n_0 ;
  wire \ramplitude_min_reg[31]_i_102_n_0 ;
  wire \ramplitude_min_reg[31]_i_103_n_0 ;
  wire \ramplitude_min_reg[31]_i_104_n_0 ;
  wire \ramplitude_min_reg[31]_i_105_n_0 ;
  wire \ramplitude_min_reg[31]_i_106_n_0 ;
  wire \ramplitude_min_reg[31]_i_107_n_0 ;
  wire \ramplitude_min_reg[31]_i_108_n_0 ;
  wire \ramplitude_min_reg[31]_i_109_n_0 ;
  wire \ramplitude_min_reg[31]_i_110_n_0 ;
  wire \ramplitude_min_reg[31]_i_111_n_0 ;
  wire \ramplitude_min_reg[31]_i_112_n_0 ;
  wire \ramplitude_min_reg[31]_i_113_n_0 ;
  wire \ramplitude_min_reg[31]_i_114_n_0 ;
  wire \ramplitude_min_reg[31]_i_115_n_0 ;
  wire \ramplitude_min_reg[31]_i_116_n_0 ;
  wire \ramplitude_min_reg[31]_i_117_n_0 ;
  wire \ramplitude_min_reg[31]_i_118_n_0 ;
  wire \ramplitude_min_reg[31]_i_119_n_0 ;
  wire \ramplitude_min_reg[31]_i_11_n_0 ;
  wire \ramplitude_min_reg[31]_i_120_n_0 ;
  wire \ramplitude_min_reg[31]_i_121_n_0 ;
  wire \ramplitude_min_reg[31]_i_122_n_0 ;
  wire \ramplitude_min_reg[31]_i_123_n_0 ;
  wire \ramplitude_min_reg[31]_i_124_n_0 ;
  wire \ramplitude_min_reg[31]_i_125_n_0 ;
  wire \ramplitude_min_reg[31]_i_126_n_0 ;
  wire \ramplitude_min_reg[31]_i_127_n_0 ;
  wire \ramplitude_min_reg[31]_i_128_n_0 ;
  wire \ramplitude_min_reg[31]_i_129_n_0 ;
  wire \ramplitude_min_reg[31]_i_12_n_0 ;
  wire \ramplitude_min_reg[31]_i_130_n_0 ;
  wire \ramplitude_min_reg[31]_i_131_n_0 ;
  wire \ramplitude_min_reg[31]_i_13_n_0 ;
  wire \ramplitude_min_reg[31]_i_14_n_0 ;
  wire \ramplitude_min_reg[31]_i_15_n_0 ;
  wire \ramplitude_min_reg[31]_i_16_n_0 ;
  wire \ramplitude_min_reg[31]_i_17_n_0 ;
  wire \ramplitude_min_reg[31]_i_18_n_0 ;
  wire \ramplitude_min_reg[31]_i_1_n_0 ;
  wire \ramplitude_min_reg[31]_i_20_n_0 ;
  wire \ramplitude_min_reg[31]_i_21_n_0 ;
  wire \ramplitude_min_reg[31]_i_22_n_0 ;
  wire \ramplitude_min_reg[31]_i_23_n_0 ;
  wire \ramplitude_min_reg[31]_i_24_n_0 ;
  wire \ramplitude_min_reg[31]_i_25_n_0 ;
  wire \ramplitude_min_reg[31]_i_26_n_0 ;
  wire \ramplitude_min_reg[31]_i_27_n_0 ;
  wire \ramplitude_min_reg[31]_i_28_n_0 ;
  wire \ramplitude_min_reg[31]_i_2_n_0 ;
  wire \ramplitude_min_reg[31]_i_34_n_0 ;
  wire \ramplitude_min_reg[31]_i_35_n_0 ;
  wire \ramplitude_min_reg[31]_i_36_n_0 ;
  wire \ramplitude_min_reg[31]_i_37_n_0 ;
  wire \ramplitude_min_reg[31]_i_38_n_0 ;
  wire \ramplitude_min_reg[31]_i_39_n_0 ;
  wire \ramplitude_min_reg[31]_i_40_n_0 ;
  wire \ramplitude_min_reg[31]_i_41_n_0 ;
  wire \ramplitude_min_reg[31]_i_42_n_0 ;
  wire \ramplitude_min_reg[31]_i_43_n_0 ;
  wire \ramplitude_min_reg[31]_i_44_n_0 ;
  wire \ramplitude_min_reg[31]_i_45_n_0 ;
  wire \ramplitude_min_reg[31]_i_46_n_0 ;
  wire \ramplitude_min_reg[31]_i_47_n_0 ;
  wire \ramplitude_min_reg[31]_i_48_n_0 ;
  wire \ramplitude_min_reg[31]_i_49_n_0 ;
  wire \ramplitude_min_reg[31]_i_51_n_0 ;
  wire \ramplitude_min_reg[31]_i_52_n_0 ;
  wire \ramplitude_min_reg[31]_i_53_n_0 ;
  wire \ramplitude_min_reg[31]_i_54_n_0 ;
  wire \ramplitude_min_reg[31]_i_55_n_0 ;
  wire \ramplitude_min_reg[31]_i_56_n_0 ;
  wire \ramplitude_min_reg[31]_i_57_n_0 ;
  wire \ramplitude_min_reg[31]_i_58_n_0 ;
  wire \ramplitude_min_reg[31]_i_60_n_0 ;
  wire \ramplitude_min_reg[31]_i_61_n_0 ;
  wire \ramplitude_min_reg[31]_i_62_n_0 ;
  wire \ramplitude_min_reg[31]_i_63_n_0 ;
  wire \ramplitude_min_reg[31]_i_64_n_0 ;
  wire \ramplitude_min_reg[31]_i_65_n_0 ;
  wire \ramplitude_min_reg[31]_i_66_n_0 ;
  wire \ramplitude_min_reg[31]_i_67_n_0 ;
  wire \ramplitude_min_reg[31]_i_69_n_0 ;
  wire \ramplitude_min_reg[31]_i_70_n_0 ;
  wire \ramplitude_min_reg[31]_i_71_n_0 ;
  wire \ramplitude_min_reg[31]_i_72_n_0 ;
  wire \ramplitude_min_reg[31]_i_73_n_0 ;
  wire \ramplitude_min_reg[31]_i_74_n_0 ;
  wire \ramplitude_min_reg[31]_i_75_n_0 ;
  wire \ramplitude_min_reg[31]_i_76_n_0 ;
  wire \ramplitude_min_reg[31]_i_77_n_0 ;
  wire \ramplitude_min_reg[31]_i_78_n_0 ;
  wire \ramplitude_min_reg[31]_i_79_n_0 ;
  wire \ramplitude_min_reg[31]_i_80_n_0 ;
  wire \ramplitude_min_reg[31]_i_82_n_0 ;
  wire \ramplitude_min_reg[31]_i_83_n_0 ;
  wire \ramplitude_min_reg[31]_i_84_n_0 ;
  wire \ramplitude_min_reg[31]_i_85_n_0 ;
  wire \ramplitude_min_reg[31]_i_86_n_0 ;
  wire \ramplitude_min_reg[31]_i_87_n_0 ;
  wire \ramplitude_min_reg[31]_i_88_n_0 ;
  wire \ramplitude_min_reg[31]_i_89_n_0 ;
  wire \ramplitude_min_reg[31]_i_91_n_0 ;
  wire \ramplitude_min_reg[31]_i_92_n_0 ;
  wire \ramplitude_min_reg[31]_i_93_n_0 ;
  wire \ramplitude_min_reg[31]_i_94_n_0 ;
  wire \ramplitude_min_reg[31]_i_95_n_0 ;
  wire \ramplitude_min_reg[31]_i_96_n_0 ;
  wire \ramplitude_min_reg[31]_i_97_n_0 ;
  wire \ramplitude_min_reg[31]_i_98_n_0 ;
  wire \ramplitude_min_reg[31]_i_9_n_0 ;
  wire \ramplitude_min_reg[3]_i_1_n_0 ;
  wire \ramplitude_min_reg[3]_i_3_n_0 ;
  wire \ramplitude_min_reg[3]_i_4_n_0 ;
  wire \ramplitude_min_reg[3]_i_5_n_0 ;
  wire \ramplitude_min_reg[3]_i_6_n_0 ;
  wire \ramplitude_min_reg[4]_i_1_n_0 ;
  wire \ramplitude_min_reg[5]_i_1_n_0 ;
  wire \ramplitude_min_reg[6]_i_1_n_0 ;
  wire \ramplitude_min_reg[7]_i_1_n_0 ;
  wire \ramplitude_min_reg[7]_i_3_n_0 ;
  wire \ramplitude_min_reg[7]_i_4_n_0 ;
  wire \ramplitude_min_reg[7]_i_5_n_0 ;
  wire \ramplitude_min_reg[7]_i_6_n_0 ;
  wire \ramplitude_min_reg[8]_i_1_n_0 ;
  wire \ramplitude_min_reg[9]_i_1_n_0 ;
  wire \ramplitude_min_reg_reg[11]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[11]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[11]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[11]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[12]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[12]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[12]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[12]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[15]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[15]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[15]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[15]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[16]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[16]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[16]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[16]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[19]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[19]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[19]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[19]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[20]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[20]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[20]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[20]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[23]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[23]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[23]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[23]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[24]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[24]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[24]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[24]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[27]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[27]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[27]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[27]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[28]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[28]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[28]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[28]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_10_n_0 ;
  wire \ramplitude_min_reg_reg[31]_i_10_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_10_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_10_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_19_n_0 ;
  wire \ramplitude_min_reg_reg[31]_i_19_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_19_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_19_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_33_n_0 ;
  wire \ramplitude_min_reg_reg[31]_i_33_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_33_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_33_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_3_n_0 ;
  wire \ramplitude_min_reg_reg[31]_i_3_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_3_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_3_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_4_n_0 ;
  wire \ramplitude_min_reg_reg[31]_i_4_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_4_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_4_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_50_n_0 ;
  wire \ramplitude_min_reg_reg[31]_i_50_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_50_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_50_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_59_n_0 ;
  wire \ramplitude_min_reg_reg[31]_i_59_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_59_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_59_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_5_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_5_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_68_n_0 ;
  wire \ramplitude_min_reg_reg[31]_i_68_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_68_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_68_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_7_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_7_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_7_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_81_n_0 ;
  wire \ramplitude_min_reg_reg[31]_i_81_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_81_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_81_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_8_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_8_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_8_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_90_n_0 ;
  wire \ramplitude_min_reg_reg[31]_i_90_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_90_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_90_n_3 ;
  wire \ramplitude_min_reg_reg[31]_i_99_n_0 ;
  wire \ramplitude_min_reg_reg[31]_i_99_n_1 ;
  wire \ramplitude_min_reg_reg[31]_i_99_n_2 ;
  wire \ramplitude_min_reg_reg[31]_i_99_n_3 ;
  wire \ramplitude_min_reg_reg[3]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[3]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[3]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[3]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[4]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[4]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[4]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[4]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[7]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[7]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[7]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[7]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg[8]_i_2_n_0 ;
  wire \ramplitude_min_reg_reg[8]_i_2_n_1 ;
  wire \ramplitude_min_reg_reg[8]_i_2_n_2 ;
  wire \ramplitude_min_reg_reg[8]_i_2_n_3 ;
  wire \ramplitude_min_reg_reg_n_0_[0] ;
  wire \ramplitude_min_reg_reg_n_0_[10] ;
  wire \ramplitude_min_reg_reg_n_0_[11] ;
  wire \ramplitude_min_reg_reg_n_0_[12] ;
  wire \ramplitude_min_reg_reg_n_0_[13] ;
  wire \ramplitude_min_reg_reg_n_0_[14] ;
  wire \ramplitude_min_reg_reg_n_0_[15] ;
  wire \ramplitude_min_reg_reg_n_0_[16] ;
  wire \ramplitude_min_reg_reg_n_0_[17] ;
  wire \ramplitude_min_reg_reg_n_0_[18] ;
  wire \ramplitude_min_reg_reg_n_0_[19] ;
  wire \ramplitude_min_reg_reg_n_0_[1] ;
  wire \ramplitude_min_reg_reg_n_0_[20] ;
  wire \ramplitude_min_reg_reg_n_0_[21] ;
  wire \ramplitude_min_reg_reg_n_0_[22] ;
  wire \ramplitude_min_reg_reg_n_0_[23] ;
  wire \ramplitude_min_reg_reg_n_0_[24] ;
  wire \ramplitude_min_reg_reg_n_0_[25] ;
  wire \ramplitude_min_reg_reg_n_0_[26] ;
  wire \ramplitude_min_reg_reg_n_0_[27] ;
  wire \ramplitude_min_reg_reg_n_0_[28] ;
  wire \ramplitude_min_reg_reg_n_0_[29] ;
  wire \ramplitude_min_reg_reg_n_0_[2] ;
  wire \ramplitude_min_reg_reg_n_0_[30] ;
  wire \ramplitude_min_reg_reg_n_0_[31] ;
  wire \ramplitude_min_reg_reg_n_0_[3] ;
  wire \ramplitude_min_reg_reg_n_0_[4] ;
  wire \ramplitude_min_reg_reg_n_0_[5] ;
  wire \ramplitude_min_reg_reg_n_0_[6] ;
  wire \ramplitude_min_reg_reg_n_0_[7] ;
  wire \ramplitude_min_reg_reg_n_0_[8] ;
  wire \ramplitude_min_reg_reg_n_0_[9] ;
  wire [31:0]ramplitude_step_reg;
  wire [31:0]\ramplitude_step_reg_reg[31]_0 ;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [31:0]\status_reg_reg[10] ;
  wire [3:0]NLW_loop_locked_INST_0_O_UNCONNECTED;
  wire [3:0]NLW_loop_locked_INST_0_i_1_O_UNCONNECTED;
  wire [3:0]NLW_loop_locked_INST_0_i_10_O_UNCONNECTED;
  wire [3:0]NLW_loop_locked_INST_0_i_19_O_UNCONNECTED;
  wire [3:3]\NLW_ramp_reg_reg[31]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_ramp_reg_reg[31]_i_4_CO_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_max_reg_reg[31]_i_18_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_max_reg_reg[31]_i_27_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_max_reg_reg[31]_i_3_O_UNCONNECTED ;
  wire [3:3]\NLW_ramplitude_max_reg_reg[31]_i_4_CO_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_max_reg_reg[31]_i_5_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_10_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_19_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_33_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_4_O_UNCONNECTED ;
  wire [3:2]\NLW_ramplitude_min_reg_reg[31]_i_5_CO_UNCONNECTED ;
  wire [3:3]\NLW_ramplitude_min_reg_reg[31]_i_5_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_50_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_59_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_68_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_7_O_UNCONNECTED ;
  wire [3:3]\NLW_ramplitude_min_reg_reg[31]_i_8_CO_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_81_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_90_O_UNCONNECTED ;
  wire [3:0]\NLW_ramplitude_min_reg_reg[31]_i_99_O_UNCONNECTED ;

  LUT3 #(
    .INIT(8'h8B)) 
    \I_term_q[31]_i_1 
       (.I0(ramp_output[13]),
        .I1(D[1]),
        .I2(\I_term_q_reg[31] ),
        .O(\ramp_reg_reg[31]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \autolock_input_max[13]_i_1 
       (.I0(\loop_locked_counter_reg_reg[30] ),
        .I1(\autolock_input_maxdex_reg[0] ),
        .I2(ramp_module_0_corner),
        .O(ramp_corner_reg_reg_1));
  LUT3 #(
    .INIT(8'hFE)) 
    \autolock_input_min[13]_i_1 
       (.I0(\loop_locked_counter_reg_reg[30] ),
        .I1(\autolock_input_mindex_reg[0] ),
        .I2(ramp_module_0_corner),
        .O(ramp_corner_reg_reg_0));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[0]_i_1 
       (.I0(ramp_output[0]),
        .I1(\axi_pi_output_reg_reg[13]_inv [0]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [0]));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[10]_i_1 
       (.I0(ramp_output[10]),
        .I1(\axi_pi_output_reg_reg[13]_inv [10]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [10]));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[11]_i_1 
       (.I0(ramp_output[11]),
        .I1(\axi_pi_output_reg_reg[13]_inv [11]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [11]));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[12]_i_1 
       (.I0(ramp_output[12]),
        .I1(\axi_pi_output_reg_reg[13]_inv [12]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [12]));
  LUT6 #(
    .INIT(64'h5355555553535353)) 
    \axi_pi_output_reg[13]_inv_i_2 
       (.I0(ramp_output[13]),
        .I1(\axi_pi_output_reg_reg[13]_inv [13]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[31]_1 ));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[1]_i_1 
       (.I0(ramp_output[1]),
        .I1(\axi_pi_output_reg_reg[13]_inv [1]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [1]));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[2]_i_1 
       (.I0(ramp_output[2]),
        .I1(\axi_pi_output_reg_reg[13]_inv [2]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [2]));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[3]_i_1 
       (.I0(ramp_output[3]),
        .I1(\axi_pi_output_reg_reg[13]_inv [3]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [3]));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[4]_i_1 
       (.I0(ramp_output[4]),
        .I1(\axi_pi_output_reg_reg[13]_inv [4]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [4]));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[5]_i_1 
       (.I0(ramp_output[5]),
        .I1(\axi_pi_output_reg_reg[13]_inv [5]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [5]));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[6]_i_1 
       (.I0(ramp_output[6]),
        .I1(\axi_pi_output_reg_reg[13]_inv [6]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [6]));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[7]_i_1 
       (.I0(ramp_output[7]),
        .I1(\axi_pi_output_reg_reg[13]_inv [7]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [7]));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[8]_i_1 
       (.I0(ramp_output[8]),
        .I1(\axi_pi_output_reg_reg[13]_inv [8]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [8]));
  LUT6 #(
    .INIT(64'hACAAAAAAACACACAC)) 
    \axi_pi_output_reg[9]_i_1 
       (.I0(ramp_output[9]),
        .I1(\axi_pi_output_reg_reg[13]_inv [9]),
        .I2(D[2]),
        .I3(\axi_pi_output_reg_reg[0] ),
        .I4(CO),
        .I5(D[3]),
        .O(\ramp_reg_reg[30]_0 [9]));
  CARRY4 loop_locked_INST_0
       (.CI(loop_locked_INST_0_i_1_n_0),
        .CO({\loop_locked_counter_reg_reg[30] ,loop_locked_INST_0_n_1,loop_locked_INST_0_n_2,loop_locked_INST_0_n_3}),
        .CYINIT(1'b0),
        .DI({loop_locked_INST_0_i_2_n_0,loop_locked_INST_0_i_3_n_0,loop_locked_INST_0_i_4_n_0,loop_locked_INST_0_i_5_n_0}),
        .O(NLW_loop_locked_INST_0_O_UNCONNECTED[3:0]),
        .S({loop_locked_INST_0_i_6_n_0,loop_locked_INST_0_i_7_n_0,loop_locked_INST_0_i_8_n_0,loop_locked_INST_0_i_9_n_0}));
  CARRY4 loop_locked_INST_0_i_1
       (.CI(loop_locked_INST_0_i_10_n_0),
        .CO({loop_locked_INST_0_i_1_n_0,loop_locked_INST_0_i_1_n_1,loop_locked_INST_0_i_1_n_2,loop_locked_INST_0_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({loop_locked_INST_0_i_11_n_0,loop_locked_INST_0_i_12_n_0,loop_locked_INST_0_i_13_n_0,loop_locked_INST_0_i_14_n_0}),
        .O(NLW_loop_locked_INST_0_i_1_O_UNCONNECTED[3:0]),
        .S({loop_locked_INST_0_i_15_n_0,loop_locked_INST_0_i_16_n_0,loop_locked_INST_0_i_17_n_0,loop_locked_INST_0_i_18_n_0}));
  CARRY4 loop_locked_INST_0_i_10
       (.CI(loop_locked_INST_0_i_19_n_0),
        .CO({loop_locked_INST_0_i_10_n_0,loop_locked_INST_0_i_10_n_1,loop_locked_INST_0_i_10_n_2,loop_locked_INST_0_i_10_n_3}),
        .CYINIT(1'b0),
        .DI({loop_locked_INST_0_i_20_n_0,loop_locked_INST_0_i_21_n_0,loop_locked_INST_0_i_22_n_0,loop_locked_INST_0_i_23_n_0}),
        .O(NLW_loop_locked_INST_0_i_10_O_UNCONNECTED[3:0]),
        .S({loop_locked_INST_0_i_24_n_0,loop_locked_INST_0_i_25_n_0,loop_locked_INST_0_i_26_n_0,loop_locked_INST_0_i_27_n_0}));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_11
       (.I0(loop_locked_counter_reg_reg[22]),
        .I1(\status_reg_reg[10] [22]),
        .I2(\status_reg_reg[10] [23]),
        .I3(loop_locked_counter_reg_reg[23]),
        .O(loop_locked_INST_0_i_11_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_12
       (.I0(loop_locked_counter_reg_reg[20]),
        .I1(\status_reg_reg[10] [20]),
        .I2(\status_reg_reg[10] [21]),
        .I3(loop_locked_counter_reg_reg[21]),
        .O(loop_locked_INST_0_i_12_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_13
       (.I0(loop_locked_counter_reg_reg[18]),
        .I1(\status_reg_reg[10] [18]),
        .I2(\status_reg_reg[10] [19]),
        .I3(loop_locked_counter_reg_reg[19]),
        .O(loop_locked_INST_0_i_13_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_14
       (.I0(loop_locked_counter_reg_reg[16]),
        .I1(\status_reg_reg[10] [16]),
        .I2(\status_reg_reg[10] [17]),
        .I3(loop_locked_counter_reg_reg[17]),
        .O(loop_locked_INST_0_i_14_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_15
       (.I0(loop_locked_counter_reg_reg[22]),
        .I1(\status_reg_reg[10] [22]),
        .I2(loop_locked_counter_reg_reg[23]),
        .I3(\status_reg_reg[10] [23]),
        .O(loop_locked_INST_0_i_15_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_16
       (.I0(loop_locked_counter_reg_reg[20]),
        .I1(\status_reg_reg[10] [20]),
        .I2(loop_locked_counter_reg_reg[21]),
        .I3(\status_reg_reg[10] [21]),
        .O(loop_locked_INST_0_i_16_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_17
       (.I0(loop_locked_counter_reg_reg[18]),
        .I1(\status_reg_reg[10] [18]),
        .I2(loop_locked_counter_reg_reg[19]),
        .I3(\status_reg_reg[10] [19]),
        .O(loop_locked_INST_0_i_17_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_18
       (.I0(loop_locked_counter_reg_reg[16]),
        .I1(\status_reg_reg[10] [16]),
        .I2(loop_locked_counter_reg_reg[17]),
        .I3(\status_reg_reg[10] [17]),
        .O(loop_locked_INST_0_i_18_n_0));
  CARRY4 loop_locked_INST_0_i_19
       (.CI(1'b0),
        .CO({loop_locked_INST_0_i_19_n_0,loop_locked_INST_0_i_19_n_1,loop_locked_INST_0_i_19_n_2,loop_locked_INST_0_i_19_n_3}),
        .CYINIT(1'b0),
        .DI({loop_locked_INST_0_i_28_n_0,loop_locked_INST_0_i_29_n_0,loop_locked_INST_0_i_30_n_0,loop_locked_INST_0_i_31_n_0}),
        .O(NLW_loop_locked_INST_0_i_19_O_UNCONNECTED[3:0]),
        .S({loop_locked_INST_0_i_32_n_0,loop_locked_INST_0_i_33_n_0,loop_locked_INST_0_i_34_n_0,loop_locked_INST_0_i_35_n_0}));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_2
       (.I0(loop_locked_counter_reg_reg[30]),
        .I1(\status_reg_reg[10] [30]),
        .I2(\status_reg_reg[10] [31]),
        .I3(loop_locked_counter_reg_reg[31]),
        .O(loop_locked_INST_0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_20
       (.I0(loop_locked_counter_reg_reg[14]),
        .I1(\status_reg_reg[10] [14]),
        .I2(\status_reg_reg[10] [15]),
        .I3(loop_locked_counter_reg_reg[15]),
        .O(loop_locked_INST_0_i_20_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_21
       (.I0(loop_locked_counter_reg_reg[12]),
        .I1(\status_reg_reg[10] [12]),
        .I2(\status_reg_reg[10] [13]),
        .I3(loop_locked_counter_reg_reg[13]),
        .O(loop_locked_INST_0_i_21_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_22
       (.I0(loop_locked_counter_reg_reg[10]),
        .I1(\status_reg_reg[10] [10]),
        .I2(\status_reg_reg[10] [11]),
        .I3(loop_locked_counter_reg_reg[11]),
        .O(loop_locked_INST_0_i_22_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_23
       (.I0(loop_locked_counter_reg_reg[8]),
        .I1(\status_reg_reg[10] [8]),
        .I2(\status_reg_reg[10] [9]),
        .I3(loop_locked_counter_reg_reg[9]),
        .O(loop_locked_INST_0_i_23_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_24
       (.I0(loop_locked_counter_reg_reg[14]),
        .I1(\status_reg_reg[10] [14]),
        .I2(loop_locked_counter_reg_reg[15]),
        .I3(\status_reg_reg[10] [15]),
        .O(loop_locked_INST_0_i_24_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_25
       (.I0(loop_locked_counter_reg_reg[12]),
        .I1(\status_reg_reg[10] [12]),
        .I2(loop_locked_counter_reg_reg[13]),
        .I3(\status_reg_reg[10] [13]),
        .O(loop_locked_INST_0_i_25_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_26
       (.I0(loop_locked_counter_reg_reg[10]),
        .I1(\status_reg_reg[10] [10]),
        .I2(loop_locked_counter_reg_reg[11]),
        .I3(\status_reg_reg[10] [11]),
        .O(loop_locked_INST_0_i_26_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_27
       (.I0(loop_locked_counter_reg_reg[8]),
        .I1(\status_reg_reg[10] [8]),
        .I2(loop_locked_counter_reg_reg[9]),
        .I3(\status_reg_reg[10] [9]),
        .O(loop_locked_INST_0_i_27_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_28
       (.I0(loop_locked_counter_reg_reg[6]),
        .I1(\status_reg_reg[10] [6]),
        .I2(\status_reg_reg[10] [7]),
        .I3(loop_locked_counter_reg_reg[7]),
        .O(loop_locked_INST_0_i_28_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_29
       (.I0(loop_locked_counter_reg_reg[4]),
        .I1(\status_reg_reg[10] [4]),
        .I2(\status_reg_reg[10] [5]),
        .I3(loop_locked_counter_reg_reg[5]),
        .O(loop_locked_INST_0_i_29_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_3
       (.I0(loop_locked_counter_reg_reg[28]),
        .I1(\status_reg_reg[10] [28]),
        .I2(\status_reg_reg[10] [29]),
        .I3(loop_locked_counter_reg_reg[29]),
        .O(loop_locked_INST_0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_30
       (.I0(loop_locked_counter_reg_reg[2]),
        .I1(\status_reg_reg[10] [2]),
        .I2(\status_reg_reg[10] [3]),
        .I3(loop_locked_counter_reg_reg[3]),
        .O(loop_locked_INST_0_i_30_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_31
       (.I0(loop_locked_counter_reg_reg[0]),
        .I1(\status_reg_reg[10] [0]),
        .I2(\status_reg_reg[10] [1]),
        .I3(loop_locked_counter_reg_reg[1]),
        .O(loop_locked_INST_0_i_31_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_32
       (.I0(loop_locked_counter_reg_reg[6]),
        .I1(\status_reg_reg[10] [6]),
        .I2(loop_locked_counter_reg_reg[7]),
        .I3(\status_reg_reg[10] [7]),
        .O(loop_locked_INST_0_i_32_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_33
       (.I0(loop_locked_counter_reg_reg[4]),
        .I1(\status_reg_reg[10] [4]),
        .I2(loop_locked_counter_reg_reg[5]),
        .I3(\status_reg_reg[10] [5]),
        .O(loop_locked_INST_0_i_33_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_34
       (.I0(loop_locked_counter_reg_reg[2]),
        .I1(\status_reg_reg[10] [2]),
        .I2(loop_locked_counter_reg_reg[3]),
        .I3(\status_reg_reg[10] [3]),
        .O(loop_locked_INST_0_i_34_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_35
       (.I0(loop_locked_counter_reg_reg[0]),
        .I1(\status_reg_reg[10] [0]),
        .I2(loop_locked_counter_reg_reg[1]),
        .I3(\status_reg_reg[10] [1]),
        .O(loop_locked_INST_0_i_35_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_4
       (.I0(loop_locked_counter_reg_reg[26]),
        .I1(\status_reg_reg[10] [26]),
        .I2(\status_reg_reg[10] [27]),
        .I3(loop_locked_counter_reg_reg[27]),
        .O(loop_locked_INST_0_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    loop_locked_INST_0_i_5
       (.I0(loop_locked_counter_reg_reg[24]),
        .I1(\status_reg_reg[10] [24]),
        .I2(\status_reg_reg[10] [25]),
        .I3(loop_locked_counter_reg_reg[25]),
        .O(loop_locked_INST_0_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_6
       (.I0(loop_locked_counter_reg_reg[30]),
        .I1(\status_reg_reg[10] [30]),
        .I2(loop_locked_counter_reg_reg[31]),
        .I3(\status_reg_reg[10] [31]),
        .O(loop_locked_INST_0_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_7
       (.I0(loop_locked_counter_reg_reg[28]),
        .I1(\status_reg_reg[10] [28]),
        .I2(loop_locked_counter_reg_reg[29]),
        .I3(\status_reg_reg[10] [29]),
        .O(loop_locked_INST_0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_8
       (.I0(loop_locked_counter_reg_reg[26]),
        .I1(\status_reg_reg[10] [26]),
        .I2(loop_locked_counter_reg_reg[27]),
        .I3(\status_reg_reg[10] [27]),
        .O(loop_locked_INST_0_i_8_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    loop_locked_INST_0_i_9
       (.I0(loop_locked_counter_reg_reg[24]),
        .I1(\status_reg_reg[10] [24]),
        .I2(loop_locked_counter_reg_reg[25]),
        .I3(\status_reg_reg[10] [25]),
        .O(loop_locked_INST_0_i_9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \mindex_reg[31]_i_1 
       (.I0(\loop_locked_counter_reg_reg[30] ),
        .I1(ramp_module_0_corner),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    ramp_corner_reg_i_1
       (.I0(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I1(\ramplitude_min_reg_reg[31]_i_4_n_0 ),
        .I2(ramp_module_0_rst),
        .O(ramp_corner_reg_i_1_n_0));
  FDRE ramp_corner_reg_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_corner_reg_i_1_n_0),
        .Q(ramp_module_0_corner),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF0FFF000F0EEF044)) 
    \ramp_reg[0]_i_1 
       (.I0(\ramp_reg[31]_i_2_n_0 ),
        .I1(data2[0]),
        .I2(\ramp_reg_reg[17]_0 [0]),
        .I3(ramp_module_0_rst),
        .I4(\ramp_reg_reg[3]_i_2_n_7 ),
        .I5(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .O(\ramp_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[10]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [10]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[11]_i_2_n_5 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[10]),
        .O(\ramp_reg[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[11]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [11]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[11]_i_2_n_4 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[11]),
        .O(\ramp_reg[11]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[11]_i_10 
       (.I0(ramp_step_reg[9]),
        .I1(ramp_reg[9]),
        .O(\ramp_reg[11]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[11]_i_11 
       (.I0(ramp_step_reg[8]),
        .I1(ramp_reg[8]),
        .O(\ramp_reg[11]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[11]_i_4 
       (.I0(ramp_reg[11]),
        .I1(ramp_step_reg[11]),
        .O(\ramp_reg[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[11]_i_5 
       (.I0(ramp_reg[10]),
        .I1(ramp_step_reg[10]),
        .O(\ramp_reg[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[11]_i_6 
       (.I0(ramp_reg[9]),
        .I1(ramp_step_reg[9]),
        .O(\ramp_reg[11]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[11]_i_7 
       (.I0(ramp_reg[8]),
        .I1(ramp_step_reg[8]),
        .O(\ramp_reg[11]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[11]_i_8 
       (.I0(ramp_step_reg[11]),
        .I1(ramp_reg[11]),
        .O(\ramp_reg[11]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[11]_i_9 
       (.I0(ramp_step_reg[10]),
        .I1(ramp_reg[10]),
        .O(\ramp_reg[11]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[12]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [12]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[15]_i_3_n_7 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[12]),
        .O(\ramp_reg[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[13]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [13]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[13]),
        .I5(\ramp_reg_reg[15]_i_3_n_6 ),
        .O(\ramp_reg[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[14]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [14]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[14]),
        .I5(\ramp_reg_reg[15]_i_3_n_5 ),
        .O(\ramp_reg[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[15]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [15]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[15]),
        .I5(\ramp_reg_reg[15]_i_3_n_4 ),
        .O(\ramp_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[15]_i_10 
       (.I0(ramp_reg[13]),
        .I1(ramp_step_reg[13]),
        .O(\ramp_reg[15]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[15]_i_11 
       (.I0(ramp_reg[12]),
        .I1(ramp_step_reg[12]),
        .O(\ramp_reg[15]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[15]_i_4 
       (.I0(ramp_step_reg[15]),
        .I1(ramp_reg[15]),
        .O(\ramp_reg[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[15]_i_5 
       (.I0(ramp_step_reg[14]),
        .I1(ramp_reg[14]),
        .O(\ramp_reg[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[15]_i_6 
       (.I0(ramp_step_reg[13]),
        .I1(ramp_reg[13]),
        .O(\ramp_reg[15]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[15]_i_7 
       (.I0(ramp_step_reg[12]),
        .I1(ramp_reg[12]),
        .O(\ramp_reg[15]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[15]_i_8 
       (.I0(ramp_reg[15]),
        .I1(ramp_step_reg[15]),
        .O(\ramp_reg[15]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[15]_i_9 
       (.I0(ramp_reg[14]),
        .I1(ramp_step_reg[14]),
        .O(\ramp_reg[15]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[16]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [16]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[19]_i_2_n_7 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[16]),
        .O(\ramp_reg[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[17]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [17]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[19]_i_2_n_6 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[17]),
        .O(\ramp_reg[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[18]_i_1 
       (.I0(ramp_module_0_reset[0]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[19]_i_2_n_5 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[18]),
        .O(\ramp_reg[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[19]_i_1 
       (.I0(ramp_module_0_reset[1]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[19]_i_2_n_4 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[19]),
        .O(\ramp_reg[19]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[19]_i_10 
       (.I0(ramp_step_reg[17]),
        .I1(ramp_reg[17]),
        .O(\ramp_reg[19]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[19]_i_11 
       (.I0(ramp_step_reg[16]),
        .I1(ramp_reg[16]),
        .O(\ramp_reg[19]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[19]_i_4 
       (.I0(ramp_output[1]),
        .I1(ramp_step_reg[19]),
        .O(\ramp_reg[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[19]_i_5 
       (.I0(ramp_output[0]),
        .I1(ramp_step_reg[18]),
        .O(\ramp_reg[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[19]_i_6 
       (.I0(ramp_reg[17]),
        .I1(ramp_step_reg[17]),
        .O(\ramp_reg[19]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[19]_i_7 
       (.I0(ramp_reg[16]),
        .I1(ramp_step_reg[16]),
        .O(\ramp_reg[19]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[19]_i_8 
       (.I0(ramp_step_reg[19]),
        .I1(ramp_output[1]),
        .O(\ramp_reg[19]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[19]_i_9 
       (.I0(ramp_step_reg[18]),
        .I1(ramp_output[0]),
        .O(\ramp_reg[19]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF0E40000F0E4)) 
    \ramp_reg[1]_i_1 
       (.I0(\ramp_reg[31]_i_2_n_0 ),
        .I1(data2[1]),
        .I2(\ramp_reg_reg[3]_i_2_n_6 ),
        .I3(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I4(ramp_module_0_rst),
        .I5(\ramp_reg_reg[17]_0 [1]),
        .O(\ramp_reg[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[20]_i_1 
       (.I0(ramp_module_0_reset[2]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[23]_i_2_n_7 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[20]),
        .O(\ramp_reg[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[21]_i_1 
       (.I0(ramp_module_0_reset[3]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[21]),
        .I5(\ramp_reg_reg[23]_i_2_n_6 ),
        .O(\ramp_reg[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[22]_i_1 
       (.I0(ramp_module_0_reset[4]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[23]_i_2_n_5 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[22]),
        .O(\ramp_reg[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[23]_i_1 
       (.I0(ramp_module_0_reset[5]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[23]_i_2_n_4 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[23]),
        .O(\ramp_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[23]_i_10 
       (.I0(ramp_step_reg[21]),
        .I1(ramp_output[3]),
        .O(\ramp_reg[23]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[23]_i_11 
       (.I0(ramp_step_reg[20]),
        .I1(ramp_output[2]),
        .O(\ramp_reg[23]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[23]_i_4 
       (.I0(ramp_output[5]),
        .I1(ramp_step_reg[23]),
        .O(\ramp_reg[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[23]_i_5 
       (.I0(ramp_output[4]),
        .I1(ramp_step_reg[22]),
        .O(\ramp_reg[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[23]_i_6 
       (.I0(ramp_output[3]),
        .I1(ramp_step_reg[21]),
        .O(\ramp_reg[23]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[23]_i_7 
       (.I0(ramp_output[2]),
        .I1(ramp_step_reg[20]),
        .O(\ramp_reg[23]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[23]_i_8 
       (.I0(ramp_step_reg[23]),
        .I1(ramp_output[5]),
        .O(\ramp_reg[23]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[23]_i_9 
       (.I0(ramp_step_reg[22]),
        .I1(ramp_output[4]),
        .O(\ramp_reg[23]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[24]_i_1 
       (.I0(ramp_module_0_reset[6]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[27]_i_2_n_7 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[24]),
        .O(\ramp_reg[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[25]_i_1 
       (.I0(ramp_module_0_reset[7]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[25]),
        .I5(\ramp_reg_reg[27]_i_2_n_6 ),
        .O(\ramp_reg[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[26]_i_1 
       (.I0(ramp_module_0_reset[8]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[27]_i_2_n_5 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[26]),
        .O(\ramp_reg[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[27]_i_1 
       (.I0(ramp_module_0_reset[9]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[27]_i_2_n_4 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[27]),
        .O(\ramp_reg[27]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[27]_i_10 
       (.I0(ramp_step_reg[25]),
        .I1(ramp_output[7]),
        .O(\ramp_reg[27]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[27]_i_11 
       (.I0(ramp_step_reg[24]),
        .I1(ramp_output[6]),
        .O(\ramp_reg[27]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[27]_i_4 
       (.I0(ramp_output[9]),
        .I1(ramp_step_reg[27]),
        .O(\ramp_reg[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[27]_i_5 
       (.I0(ramp_output[8]),
        .I1(ramp_step_reg[26]),
        .O(\ramp_reg[27]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[27]_i_6 
       (.I0(ramp_output[7]),
        .I1(ramp_step_reg[25]),
        .O(\ramp_reg[27]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[27]_i_7 
       (.I0(ramp_output[6]),
        .I1(ramp_step_reg[24]),
        .O(\ramp_reg[27]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[27]_i_8 
       (.I0(ramp_step_reg[27]),
        .I1(ramp_output[9]),
        .O(\ramp_reg[27]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[27]_i_9 
       (.I0(ramp_step_reg[26]),
        .I1(ramp_output[8]),
        .O(\ramp_reg[27]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[28]_i_1 
       (.I0(ramp_module_0_reset[10]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[28]),
        .I5(\ramp_reg_reg[31]_i_4_n_7 ),
        .O(\ramp_reg[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[29]_i_1 
       (.I0(ramp_module_0_reset[11]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[29]),
        .I5(\ramp_reg_reg[31]_i_4_n_6 ),
        .O(\ramp_reg[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[2]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [2]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[2]),
        .I5(\ramp_reg_reg[3]_i_2_n_5 ),
        .O(\ramp_reg[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[30]_i_1 
       (.I0(ramp_module_0_reset[12]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[30]),
        .I5(\ramp_reg_reg[31]_i_4_n_5 ),
        .O(\ramp_reg[30]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[31]_i_1 
       (.I0(ramp_module_0_reset[13]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[31]),
        .I5(\ramp_reg_reg[31]_i_4_n_4 ),
        .O(\ramp_reg[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[31]_i_10 
       (.I0(ramp_step_reg[30]),
        .I1(ramp_output[12]),
        .O(\ramp_reg[31]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[31]_i_11 
       (.I0(ramp_step_reg[29]),
        .I1(ramp_output[11]),
        .O(\ramp_reg[31]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[31]_i_12 
       (.I0(ramp_step_reg[28]),
        .I1(ramp_output[10]),
        .O(\ramp_reg[31]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ramp_reg[31]_i_2 
       (.I0(\ramplitude_min_reg_reg[31]_i_4_n_0 ),
        .I1(ramp_up_reg_n_0),
        .O(\ramp_reg[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[31]_i_5 
       (.I0(ramp_output[13]),
        .I1(ramp_step_reg[31]),
        .O(\ramp_reg[31]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[31]_i_6 
       (.I0(ramp_output[12]),
        .I1(ramp_step_reg[30]),
        .O(\ramp_reg[31]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[31]_i_7 
       (.I0(ramp_output[11]),
        .I1(ramp_step_reg[29]),
        .O(\ramp_reg[31]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[31]_i_8 
       (.I0(ramp_output[10]),
        .I1(ramp_step_reg[28]),
        .O(\ramp_reg[31]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[31]_i_9 
       (.I0(ramp_step_reg[31]),
        .I1(ramp_output[13]),
        .O(\ramp_reg[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[3]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [3]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[3]_i_2_n_4 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[3]),
        .O(\ramp_reg[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[3]_i_10 
       (.I0(ramp_step_reg[1]),
        .I1(ramp_reg[1]),
        .O(\ramp_reg[3]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[3]_i_11 
       (.I0(ramp_step_reg[0]),
        .I1(ramp_reg[0]),
        .O(\ramp_reg[3]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[3]_i_4 
       (.I0(ramp_reg[3]),
        .I1(ramp_step_reg[3]),
        .O(\ramp_reg[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[3]_i_5 
       (.I0(ramp_reg[2]),
        .I1(ramp_step_reg[2]),
        .O(\ramp_reg[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[3]_i_6 
       (.I0(ramp_reg[1]),
        .I1(ramp_step_reg[1]),
        .O(\ramp_reg[3]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[3]_i_7 
       (.I0(ramp_reg[0]),
        .I1(ramp_step_reg[0]),
        .O(\ramp_reg[3]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[3]_i_8 
       (.I0(ramp_step_reg[3]),
        .I1(ramp_reg[3]),
        .O(\ramp_reg[3]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[3]_i_9 
       (.I0(ramp_step_reg[2]),
        .I1(ramp_reg[2]),
        .O(\ramp_reg[3]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[4]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [4]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[7]_i_2_n_7 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[4]),
        .O(\ramp_reg[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[5]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [5]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[5]),
        .I5(\ramp_reg_reg[7]_i_2_n_6 ),
        .O(\ramp_reg[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[6]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [6]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[7]_i_2_n_5 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[6]),
        .O(\ramp_reg[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[7]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [7]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[7]_i_2_n_4 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[7]),
        .O(\ramp_reg[7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[7]_i_10 
       (.I0(ramp_step_reg[5]),
        .I1(ramp_reg[5]),
        .O(\ramp_reg[7]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[7]_i_11 
       (.I0(ramp_step_reg[4]),
        .I1(ramp_reg[4]),
        .O(\ramp_reg[7]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[7]_i_4 
       (.I0(ramp_reg[7]),
        .I1(ramp_step_reg[7]),
        .O(\ramp_reg[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[7]_i_5 
       (.I0(ramp_reg[6]),
        .I1(ramp_step_reg[6]),
        .O(\ramp_reg[7]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[7]_i_6 
       (.I0(ramp_reg[5]),
        .I1(ramp_step_reg[5]),
        .O(\ramp_reg[7]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramp_reg[7]_i_7 
       (.I0(ramp_reg[4]),
        .I1(ramp_step_reg[4]),
        .O(\ramp_reg[7]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[7]_i_8 
       (.I0(ramp_step_reg[7]),
        .I1(ramp_reg[7]),
        .O(\ramp_reg[7]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramp_reg[7]_i_9 
       (.I0(ramp_step_reg[6]),
        .I1(ramp_reg[6]),
        .O(\ramp_reg[7]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBB8888B8888)) 
    \ramp_reg[8]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [8]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg[31]_i_2_n_0 ),
        .I4(data2[8]),
        .I5(\ramp_reg_reg[11]_i_2_n_7 ),
        .O(\ramp_reg[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBB88BB8BBB88B888)) 
    \ramp_reg[9]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [9]),
        .I1(ramp_module_0_rst),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(\ramp_reg_reg[11]_i_2_n_6 ),
        .I4(\ramp_reg[31]_i_2_n_0 ),
        .I5(data2[9]),
        .O(\ramp_reg[9]_i_1_n_0 ));
  FDRE \ramp_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[0]_i_1_n_0 ),
        .Q(ramp_reg[0]),
        .R(1'b0));
  FDRE \ramp_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[10]_i_1_n_0 ),
        .Q(ramp_reg[10]),
        .R(1'b0));
  FDRE \ramp_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[11]_i_1_n_0 ),
        .Q(ramp_reg[11]),
        .R(1'b0));
  CARRY4 \ramp_reg_reg[11]_i_2 
       (.CI(\ramp_reg_reg[7]_i_2_n_0 ),
        .CO({\ramp_reg_reg[11]_i_2_n_0 ,\ramp_reg_reg[11]_i_2_n_1 ,\ramp_reg_reg[11]_i_2_n_2 ,\ramp_reg_reg[11]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI(ramp_reg[11:8]),
        .O({\ramp_reg_reg[11]_i_2_n_4 ,\ramp_reg_reg[11]_i_2_n_5 ,\ramp_reg_reg[11]_i_2_n_6 ,\ramp_reg_reg[11]_i_2_n_7 }),
        .S({\ramp_reg[11]_i_4_n_0 ,\ramp_reg[11]_i_5_n_0 ,\ramp_reg[11]_i_6_n_0 ,\ramp_reg[11]_i_7_n_0 }));
  CARRY4 \ramp_reg_reg[11]_i_3 
       (.CI(\ramp_reg_reg[7]_i_3_n_0 ),
        .CO({\ramp_reg_reg[11]_i_3_n_0 ,\ramp_reg_reg[11]_i_3_n_1 ,\ramp_reg_reg[11]_i_3_n_2 ,\ramp_reg_reg[11]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI(ramp_reg[11:8]),
        .O(data2[11:8]),
        .S({\ramp_reg[11]_i_8_n_0 ,\ramp_reg[11]_i_9_n_0 ,\ramp_reg[11]_i_10_n_0 ,\ramp_reg[11]_i_11_n_0 }));
  FDRE \ramp_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[12]_i_1_n_0 ),
        .Q(ramp_reg[12]),
        .R(1'b0));
  FDRE \ramp_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[13]_i_1_n_0 ),
        .Q(ramp_reg[13]),
        .R(1'b0));
  FDRE \ramp_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[14]_i_1_n_0 ),
        .Q(ramp_reg[14]),
        .R(1'b0));
  FDRE \ramp_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[15]_i_1_n_0 ),
        .Q(ramp_reg[15]),
        .R(1'b0));
  CARRY4 \ramp_reg_reg[15]_i_2 
       (.CI(\ramp_reg_reg[11]_i_3_n_0 ),
        .CO({\ramp_reg_reg[15]_i_2_n_0 ,\ramp_reg_reg[15]_i_2_n_1 ,\ramp_reg_reg[15]_i_2_n_2 ,\ramp_reg_reg[15]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI(ramp_reg[15:12]),
        .O(data2[15:12]),
        .S({\ramp_reg[15]_i_4_n_0 ,\ramp_reg[15]_i_5_n_0 ,\ramp_reg[15]_i_6_n_0 ,\ramp_reg[15]_i_7_n_0 }));
  CARRY4 \ramp_reg_reg[15]_i_3 
       (.CI(\ramp_reg_reg[11]_i_2_n_0 ),
        .CO({\ramp_reg_reg[15]_i_3_n_0 ,\ramp_reg_reg[15]_i_3_n_1 ,\ramp_reg_reg[15]_i_3_n_2 ,\ramp_reg_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI(ramp_reg[15:12]),
        .O({\ramp_reg_reg[15]_i_3_n_4 ,\ramp_reg_reg[15]_i_3_n_5 ,\ramp_reg_reg[15]_i_3_n_6 ,\ramp_reg_reg[15]_i_3_n_7 }),
        .S({\ramp_reg[15]_i_8_n_0 ,\ramp_reg[15]_i_9_n_0 ,\ramp_reg[15]_i_10_n_0 ,\ramp_reg[15]_i_11_n_0 }));
  FDRE \ramp_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[16]_i_1_n_0 ),
        .Q(ramp_reg[16]),
        .R(1'b0));
  FDRE \ramp_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[17]_i_1_n_0 ),
        .Q(ramp_reg[17]),
        .R(1'b0));
  FDRE \ramp_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[18]_i_1_n_0 ),
        .Q(ramp_output[0]),
        .R(1'b0));
  FDRE \ramp_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[19]_i_1_n_0 ),
        .Q(ramp_output[1]),
        .R(1'b0));
  CARRY4 \ramp_reg_reg[19]_i_2 
       (.CI(\ramp_reg_reg[15]_i_3_n_0 ),
        .CO({\ramp_reg_reg[19]_i_2_n_0 ,\ramp_reg_reg[19]_i_2_n_1 ,\ramp_reg_reg[19]_i_2_n_2 ,\ramp_reg_reg[19]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({ramp_output[1:0],ramp_reg[17:16]}),
        .O({\ramp_reg_reg[19]_i_2_n_4 ,\ramp_reg_reg[19]_i_2_n_5 ,\ramp_reg_reg[19]_i_2_n_6 ,\ramp_reg_reg[19]_i_2_n_7 }),
        .S({\ramp_reg[19]_i_4_n_0 ,\ramp_reg[19]_i_5_n_0 ,\ramp_reg[19]_i_6_n_0 ,\ramp_reg[19]_i_7_n_0 }));
  CARRY4 \ramp_reg_reg[19]_i_3 
       (.CI(\ramp_reg_reg[15]_i_2_n_0 ),
        .CO({\ramp_reg_reg[19]_i_3_n_0 ,\ramp_reg_reg[19]_i_3_n_1 ,\ramp_reg_reg[19]_i_3_n_2 ,\ramp_reg_reg[19]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({ramp_output[1:0],ramp_reg[17:16]}),
        .O(data2[19:16]),
        .S({\ramp_reg[19]_i_8_n_0 ,\ramp_reg[19]_i_9_n_0 ,\ramp_reg[19]_i_10_n_0 ,\ramp_reg[19]_i_11_n_0 }));
  FDRE \ramp_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[1]_i_1_n_0 ),
        .Q(ramp_reg[1]),
        .R(1'b0));
  FDRE \ramp_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[20]_i_1_n_0 ),
        .Q(ramp_output[2]),
        .R(1'b0));
  FDRE \ramp_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[21]_i_1_n_0 ),
        .Q(ramp_output[3]),
        .R(1'b0));
  FDRE \ramp_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[22]_i_1_n_0 ),
        .Q(ramp_output[4]),
        .R(1'b0));
  FDRE \ramp_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[23]_i_1_n_0 ),
        .Q(ramp_output[5]),
        .R(1'b0));
  CARRY4 \ramp_reg_reg[23]_i_2 
       (.CI(\ramp_reg_reg[19]_i_2_n_0 ),
        .CO({\ramp_reg_reg[23]_i_2_n_0 ,\ramp_reg_reg[23]_i_2_n_1 ,\ramp_reg_reg[23]_i_2_n_2 ,\ramp_reg_reg[23]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI(ramp_output[5:2]),
        .O({\ramp_reg_reg[23]_i_2_n_4 ,\ramp_reg_reg[23]_i_2_n_5 ,\ramp_reg_reg[23]_i_2_n_6 ,\ramp_reg_reg[23]_i_2_n_7 }),
        .S({\ramp_reg[23]_i_4_n_0 ,\ramp_reg[23]_i_5_n_0 ,\ramp_reg[23]_i_6_n_0 ,\ramp_reg[23]_i_7_n_0 }));
  CARRY4 \ramp_reg_reg[23]_i_3 
       (.CI(\ramp_reg_reg[19]_i_3_n_0 ),
        .CO({\ramp_reg_reg[23]_i_3_n_0 ,\ramp_reg_reg[23]_i_3_n_1 ,\ramp_reg_reg[23]_i_3_n_2 ,\ramp_reg_reg[23]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI(ramp_output[5:2]),
        .O(data2[23:20]),
        .S({\ramp_reg[23]_i_8_n_0 ,\ramp_reg[23]_i_9_n_0 ,\ramp_reg[23]_i_10_n_0 ,\ramp_reg[23]_i_11_n_0 }));
  FDRE \ramp_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[24]_i_1_n_0 ),
        .Q(ramp_output[6]),
        .R(1'b0));
  FDRE \ramp_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[25]_i_1_n_0 ),
        .Q(ramp_output[7]),
        .R(1'b0));
  FDRE \ramp_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[26]_i_1_n_0 ),
        .Q(ramp_output[8]),
        .R(1'b0));
  FDRE \ramp_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[27]_i_1_n_0 ),
        .Q(ramp_output[9]),
        .R(1'b0));
  CARRY4 \ramp_reg_reg[27]_i_2 
       (.CI(\ramp_reg_reg[23]_i_2_n_0 ),
        .CO({\ramp_reg_reg[27]_i_2_n_0 ,\ramp_reg_reg[27]_i_2_n_1 ,\ramp_reg_reg[27]_i_2_n_2 ,\ramp_reg_reg[27]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI(ramp_output[9:6]),
        .O({\ramp_reg_reg[27]_i_2_n_4 ,\ramp_reg_reg[27]_i_2_n_5 ,\ramp_reg_reg[27]_i_2_n_6 ,\ramp_reg_reg[27]_i_2_n_7 }),
        .S({\ramp_reg[27]_i_4_n_0 ,\ramp_reg[27]_i_5_n_0 ,\ramp_reg[27]_i_6_n_0 ,\ramp_reg[27]_i_7_n_0 }));
  CARRY4 \ramp_reg_reg[27]_i_3 
       (.CI(\ramp_reg_reg[23]_i_3_n_0 ),
        .CO({\ramp_reg_reg[27]_i_3_n_0 ,\ramp_reg_reg[27]_i_3_n_1 ,\ramp_reg_reg[27]_i_3_n_2 ,\ramp_reg_reg[27]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI(ramp_output[9:6]),
        .O(data2[27:24]),
        .S({\ramp_reg[27]_i_8_n_0 ,\ramp_reg[27]_i_9_n_0 ,\ramp_reg[27]_i_10_n_0 ,\ramp_reg[27]_i_11_n_0 }));
  FDRE \ramp_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[28]_i_1_n_0 ),
        .Q(ramp_output[10]),
        .R(1'b0));
  FDRE \ramp_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[29]_i_1_n_0 ),
        .Q(ramp_output[11]),
        .R(1'b0));
  FDRE \ramp_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[2]_i_1_n_0 ),
        .Q(ramp_reg[2]),
        .R(1'b0));
  FDRE \ramp_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[30]_i_1_n_0 ),
        .Q(ramp_output[12]),
        .R(1'b0));
  FDRE \ramp_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[31]_i_1_n_0 ),
        .Q(ramp_output[13]),
        .R(1'b0));
  CARRY4 \ramp_reg_reg[31]_i_3 
       (.CI(\ramp_reg_reg[27]_i_3_n_0 ),
        .CO({\NLW_ramp_reg_reg[31]_i_3_CO_UNCONNECTED [3],\ramp_reg_reg[31]_i_3_n_1 ,\ramp_reg_reg[31]_i_3_n_2 ,\ramp_reg_reg[31]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,ramp_output[12:10]}),
        .O(data2[31:28]),
        .S({\ramp_reg[31]_i_5_n_0 ,\ramp_reg[31]_i_6_n_0 ,\ramp_reg[31]_i_7_n_0 ,\ramp_reg[31]_i_8_n_0 }));
  CARRY4 \ramp_reg_reg[31]_i_4 
       (.CI(\ramp_reg_reg[27]_i_2_n_0 ),
        .CO({\NLW_ramp_reg_reg[31]_i_4_CO_UNCONNECTED [3],\ramp_reg_reg[31]_i_4_n_1 ,\ramp_reg_reg[31]_i_4_n_2 ,\ramp_reg_reg[31]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,ramp_output[12:10]}),
        .O({\ramp_reg_reg[31]_i_4_n_4 ,\ramp_reg_reg[31]_i_4_n_5 ,\ramp_reg_reg[31]_i_4_n_6 ,\ramp_reg_reg[31]_i_4_n_7 }),
        .S({\ramp_reg[31]_i_9_n_0 ,\ramp_reg[31]_i_10_n_0 ,\ramp_reg[31]_i_11_n_0 ,\ramp_reg[31]_i_12_n_0 }));
  FDRE \ramp_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[3]_i_1_n_0 ),
        .Q(ramp_reg[3]),
        .R(1'b0));
  CARRY4 \ramp_reg_reg[3]_i_2 
       (.CI(1'b0),
        .CO({\ramp_reg_reg[3]_i_2_n_0 ,\ramp_reg_reg[3]_i_2_n_1 ,\ramp_reg_reg[3]_i_2_n_2 ,\ramp_reg_reg[3]_i_2_n_3 }),
        .CYINIT(1'b1),
        .DI(ramp_reg[3:0]),
        .O({\ramp_reg_reg[3]_i_2_n_4 ,\ramp_reg_reg[3]_i_2_n_5 ,\ramp_reg_reg[3]_i_2_n_6 ,\ramp_reg_reg[3]_i_2_n_7 }),
        .S({\ramp_reg[3]_i_4_n_0 ,\ramp_reg[3]_i_5_n_0 ,\ramp_reg[3]_i_6_n_0 ,\ramp_reg[3]_i_7_n_0 }));
  CARRY4 \ramp_reg_reg[3]_i_3 
       (.CI(1'b0),
        .CO({\ramp_reg_reg[3]_i_3_n_0 ,\ramp_reg_reg[3]_i_3_n_1 ,\ramp_reg_reg[3]_i_3_n_2 ,\ramp_reg_reg[3]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI(ramp_reg[3:0]),
        .O(data2[3:0]),
        .S({\ramp_reg[3]_i_8_n_0 ,\ramp_reg[3]_i_9_n_0 ,\ramp_reg[3]_i_10_n_0 ,\ramp_reg[3]_i_11_n_0 }));
  FDRE \ramp_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[4]_i_1_n_0 ),
        .Q(ramp_reg[4]),
        .R(1'b0));
  FDRE \ramp_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[5]_i_1_n_0 ),
        .Q(ramp_reg[5]),
        .R(1'b0));
  FDRE \ramp_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[6]_i_1_n_0 ),
        .Q(ramp_reg[6]),
        .R(1'b0));
  FDRE \ramp_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[7]_i_1_n_0 ),
        .Q(ramp_reg[7]),
        .R(1'b0));
  CARRY4 \ramp_reg_reg[7]_i_2 
       (.CI(\ramp_reg_reg[3]_i_2_n_0 ),
        .CO({\ramp_reg_reg[7]_i_2_n_0 ,\ramp_reg_reg[7]_i_2_n_1 ,\ramp_reg_reg[7]_i_2_n_2 ,\ramp_reg_reg[7]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI(ramp_reg[7:4]),
        .O({\ramp_reg_reg[7]_i_2_n_4 ,\ramp_reg_reg[7]_i_2_n_5 ,\ramp_reg_reg[7]_i_2_n_6 ,\ramp_reg_reg[7]_i_2_n_7 }),
        .S({\ramp_reg[7]_i_4_n_0 ,\ramp_reg[7]_i_5_n_0 ,\ramp_reg[7]_i_6_n_0 ,\ramp_reg[7]_i_7_n_0 }));
  CARRY4 \ramp_reg_reg[7]_i_3 
       (.CI(\ramp_reg_reg[3]_i_3_n_0 ),
        .CO({\ramp_reg_reg[7]_i_3_n_0 ,\ramp_reg_reg[7]_i_3_n_1 ,\ramp_reg_reg[7]_i_3_n_2 ,\ramp_reg_reg[7]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI(ramp_reg[7:4]),
        .O(data2[7:4]),
        .S({\ramp_reg[7]_i_8_n_0 ,\ramp_reg[7]_i_9_n_0 ,\ramp_reg[7]_i_10_n_0 ,\ramp_reg[7]_i_11_n_0 }));
  FDRE \ramp_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[8]_i_1_n_0 ),
        .Q(ramp_reg[8]),
        .R(1'b0));
  FDRE \ramp_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_reg[9]_i_1_n_0 ),
        .Q(ramp_reg[9]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [0]),
        .Q(ramp_step_reg[0]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [10]),
        .Q(ramp_step_reg[10]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [11]),
        .Q(ramp_step_reg[11]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [12]),
        .Q(ramp_step_reg[12]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [13]),
        .Q(ramp_step_reg[13]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [14]),
        .Q(ramp_step_reg[14]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [15]),
        .Q(ramp_step_reg[15]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [16]),
        .Q(ramp_step_reg[16]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [17]),
        .Q(ramp_step_reg[17]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [18]),
        .Q(ramp_step_reg[18]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [19]),
        .Q(ramp_step_reg[19]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [1]),
        .Q(ramp_step_reg[1]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [20]),
        .Q(ramp_step_reg[20]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [21]),
        .Q(ramp_step_reg[21]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [22]),
        .Q(ramp_step_reg[22]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [23]),
        .Q(ramp_step_reg[23]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [24]),
        .Q(ramp_step_reg[24]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [25]),
        .Q(ramp_step_reg[25]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [26]),
        .Q(ramp_step_reg[26]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [27]),
        .Q(ramp_step_reg[27]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [28]),
        .Q(ramp_step_reg[28]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [29]),
        .Q(ramp_step_reg[29]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [2]),
        .Q(ramp_step_reg[2]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [30]),
        .Q(ramp_step_reg[30]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [31]),
        .Q(ramp_step_reg[31]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [3]),
        .Q(ramp_step_reg[3]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [4]),
        .Q(ramp_step_reg[4]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [5]),
        .Q(ramp_step_reg[5]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [6]),
        .Q(ramp_step_reg[6]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [7]),
        .Q(ramp_step_reg[7]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [8]),
        .Q(ramp_step_reg[8]),
        .R(1'b0));
  FDRE \ramp_step_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramp_step_reg_reg[31]_0 [9]),
        .Q(ramp_step_reg[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'hFF0E)) 
    ramp_up_i_1
       (.I0(ramp_up_reg_n_0),
        .I1(\ramplitude_min_reg_reg[31]_i_4_n_0 ),
        .I2(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I3(ramp_module_0_rst),
        .O(ramp_up_i_1_n_0));
  FDRE ramp_up_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ramp_up_i_1_n_0),
        .Q(ramp_up_reg_n_0),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [0]),
        .Q(ramplitude_lim_reg[0]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [10]),
        .Q(ramplitude_lim_reg[10]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [11]),
        .Q(ramplitude_lim_reg[11]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [12]),
        .Q(ramplitude_lim_reg[12]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [13]),
        .Q(ramplitude_lim_reg[13]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [14]),
        .Q(ramplitude_lim_reg[14]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [15]),
        .Q(ramplitude_lim_reg[15]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [16]),
        .Q(ramplitude_lim_reg[16]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [17]),
        .Q(ramplitude_lim_reg[17]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [18]),
        .Q(ramplitude_lim_reg[18]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [19]),
        .Q(ramplitude_lim_reg[19]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [1]),
        .Q(ramplitude_lim_reg[1]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [20]),
        .Q(ramplitude_lim_reg[20]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [21]),
        .Q(ramplitude_lim_reg[21]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [22]),
        .Q(ramplitude_lim_reg[22]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [23]),
        .Q(ramplitude_lim_reg[23]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [24]),
        .Q(ramplitude_lim_reg[24]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [25]),
        .Q(ramplitude_lim_reg[25]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [26]),
        .Q(ramplitude_lim_reg[26]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [27]),
        .Q(ramplitude_lim_reg[27]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [28]),
        .Q(ramplitude_lim_reg[28]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [29]),
        .Q(ramplitude_lim_reg[29]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [2]),
        .Q(ramplitude_lim_reg[2]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [30]),
        .Q(ramplitude_lim_reg[30]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [31]),
        .Q(ramplitude_lim_reg[31]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [3]),
        .Q(ramplitude_lim_reg[3]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [4]),
        .Q(ramplitude_lim_reg[4]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [5]),
        .Q(ramplitude_lim_reg[5]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [6]),
        .Q(ramplitude_lim_reg[6]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [7]),
        .Q(ramplitude_lim_reg[7]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [8]),
        .Q(ramplitude_lim_reg[8]),
        .R(1'b0));
  FDRE \ramplitude_lim_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_lim_reg_reg[31]_0 [9]),
        .Q(ramplitude_lim_reg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hB8BBFFFFB8880000)) 
    \ramplitude_max_reg[0]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [0]),
        .I1(ramp_module_0_rst),
        .I2(ramplitude_max_reg0[0]),
        .I3(ramplitude_max_reg1),
        .I4(\ramplitude_min_reg[31]_i_9_n_0 ),
        .I5(ramplitude_lim_reg[0]),
        .O(p_1_in[0]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[10]_i_1 
       (.I0(ramplitude_lim_reg[10]),
        .I1(\ramp_reg_reg[17]_0 [10]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[10]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[10]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[11]_i_1 
       (.I0(ramplitude_lim_reg[11]),
        .I1(\ramp_reg_reg[17]_0 [11]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[11]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[11]));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[11]_i_3 
       (.I0(\ramplitude_max_reg_reg_n_0_[11] ),
        .I1(ramplitude_step_reg[11]),
        .O(\ramplitude_max_reg[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[11]_i_4 
       (.I0(\ramplitude_max_reg_reg_n_0_[10] ),
        .I1(ramplitude_step_reg[10]),
        .O(\ramplitude_max_reg[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[11]_i_5 
       (.I0(\ramplitude_max_reg_reg_n_0_[9] ),
        .I1(ramplitude_step_reg[9]),
        .O(\ramplitude_max_reg[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[11]_i_6 
       (.I0(\ramplitude_max_reg_reg_n_0_[8] ),
        .I1(ramplitude_step_reg[8]),
        .O(\ramplitude_max_reg[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[12]_i_1 
       (.I0(ramplitude_lim_reg[12]),
        .I1(\ramp_reg_reg[17]_0 [12]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[12]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[12]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[13]_i_1 
       (.I0(ramplitude_lim_reg[13]),
        .I1(\ramp_reg_reg[17]_0 [13]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[13]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[13]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[14]_i_1 
       (.I0(ramplitude_lim_reg[14]),
        .I1(\ramp_reg_reg[17]_0 [14]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[14]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[14]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[15]_i_1 
       (.I0(ramplitude_lim_reg[15]),
        .I1(\ramp_reg_reg[17]_0 [15]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[15]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[15]));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[15]_i_3 
       (.I0(\ramplitude_max_reg_reg_n_0_[15] ),
        .I1(ramplitude_step_reg[15]),
        .O(\ramplitude_max_reg[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[15]_i_4 
       (.I0(\ramplitude_max_reg_reg_n_0_[14] ),
        .I1(ramplitude_step_reg[14]),
        .O(\ramplitude_max_reg[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[15]_i_5 
       (.I0(\ramplitude_max_reg_reg_n_0_[13] ),
        .I1(ramplitude_step_reg[13]),
        .O(\ramplitude_max_reg[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[15]_i_6 
       (.I0(\ramplitude_max_reg_reg_n_0_[12] ),
        .I1(ramplitude_step_reg[12]),
        .O(\ramplitude_max_reg[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[16]_i_1 
       (.I0(ramplitude_lim_reg[16]),
        .I1(\ramp_reg_reg[17]_0 [16]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[16]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[16]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[17]_i_1 
       (.I0(ramplitude_lim_reg[17]),
        .I1(\ramp_reg_reg[17]_0 [17]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[17]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[17]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[18]_i_1 
       (.I0(ramplitude_lim_reg[18]),
        .I1(ramp_module_0_reset[0]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[18]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[18]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[19]_i_1 
       (.I0(ramplitude_lim_reg[19]),
        .I1(ramp_module_0_reset[1]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[19]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[19]));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[19]_i_3 
       (.I0(\ramplitude_max_reg_reg_n_0_[19] ),
        .I1(ramplitude_step_reg[19]),
        .O(\ramplitude_max_reg[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[19]_i_4 
       (.I0(\ramplitude_max_reg_reg_n_0_[18] ),
        .I1(ramplitude_step_reg[18]),
        .O(\ramplitude_max_reg[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[19]_i_5 
       (.I0(\ramplitude_max_reg_reg_n_0_[17] ),
        .I1(ramplitude_step_reg[17]),
        .O(\ramplitude_max_reg[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[19]_i_6 
       (.I0(\ramplitude_max_reg_reg_n_0_[16] ),
        .I1(ramplitude_step_reg[16]),
        .O(\ramplitude_max_reg[19]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[1]_i_1 
       (.I0(ramplitude_lim_reg[1]),
        .I1(\ramp_reg_reg[17]_0 [1]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[1]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[1]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[20]_i_1 
       (.I0(ramplitude_lim_reg[20]),
        .I1(ramp_module_0_reset[2]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[20]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[20]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[21]_i_1 
       (.I0(ramplitude_lim_reg[21]),
        .I1(ramp_module_0_reset[3]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[21]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[21]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[22]_i_1 
       (.I0(ramplitude_lim_reg[22]),
        .I1(ramp_module_0_reset[4]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[22]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[22]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[23]_i_1 
       (.I0(ramplitude_lim_reg[23]),
        .I1(ramp_module_0_reset[5]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[23]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[23]));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[23]_i_3 
       (.I0(\ramplitude_max_reg_reg_n_0_[23] ),
        .I1(ramplitude_step_reg[23]),
        .O(\ramplitude_max_reg[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[23]_i_4 
       (.I0(\ramplitude_max_reg_reg_n_0_[22] ),
        .I1(ramplitude_step_reg[22]),
        .O(\ramplitude_max_reg[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[23]_i_5 
       (.I0(\ramplitude_max_reg_reg_n_0_[21] ),
        .I1(ramplitude_step_reg[21]),
        .O(\ramplitude_max_reg[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[23]_i_6 
       (.I0(\ramplitude_max_reg_reg_n_0_[20] ),
        .I1(ramplitude_step_reg[20]),
        .O(\ramplitude_max_reg[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[24]_i_1 
       (.I0(ramplitude_lim_reg[24]),
        .I1(ramp_module_0_reset[6]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[24]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[24]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[25]_i_1 
       (.I0(ramplitude_lim_reg[25]),
        .I1(ramp_module_0_reset[7]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[25]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[25]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[26]_i_1 
       (.I0(ramplitude_lim_reg[26]),
        .I1(ramp_module_0_reset[8]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[26]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[26]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[27]_i_1 
       (.I0(ramplitude_lim_reg[27]),
        .I1(ramp_module_0_reset[9]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[27]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[27]));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[27]_i_3 
       (.I0(\ramplitude_max_reg_reg_n_0_[27] ),
        .I1(ramplitude_step_reg[27]),
        .O(\ramplitude_max_reg[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[27]_i_4 
       (.I0(\ramplitude_max_reg_reg_n_0_[26] ),
        .I1(ramplitude_step_reg[26]),
        .O(\ramplitude_max_reg[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[27]_i_5 
       (.I0(\ramplitude_max_reg_reg_n_0_[25] ),
        .I1(ramplitude_step_reg[25]),
        .O(\ramplitude_max_reg[27]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[27]_i_6 
       (.I0(\ramplitude_max_reg_reg_n_0_[24] ),
        .I1(ramplitude_step_reg[24]),
        .O(\ramplitude_max_reg[27]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[28]_i_1 
       (.I0(ramplitude_lim_reg[28]),
        .I1(ramp_module_0_reset[10]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[28]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[28]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[29]_i_1 
       (.I0(ramplitude_lim_reg[29]),
        .I1(ramp_module_0_reset[11]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[29]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[29]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[2]_i_1 
       (.I0(ramplitude_lim_reg[2]),
        .I1(\ramp_reg_reg[17]_0 [2]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[2]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[2]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[30]_i_1 
       (.I0(ramplitude_lim_reg[30]),
        .I1(ramp_module_0_reset[12]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[30]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[30]));
  LUT2 #(
    .INIT(4'hE)) 
    \ramplitude_max_reg[31]_i_1 
       (.I0(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I1(ramp_module_0_rst),
        .O(\ramplitude_max_reg[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_10 
       (.I0(ramplitude_lim_reg[31]),
        .I1(\ramplitude_max_reg_reg_n_0_[31] ),
        .I2(\ramplitude_max_reg_reg_n_0_[30] ),
        .I3(ramplitude_lim_reg[30]),
        .O(\ramplitude_max_reg[31]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_11 
       (.I0(\ramplitude_max_reg_reg_n_0_[29] ),
        .I1(ramplitude_lim_reg[29]),
        .I2(\ramplitude_max_reg_reg_n_0_[28] ),
        .I3(ramplitude_lim_reg[28]),
        .O(\ramplitude_max_reg[31]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_12 
       (.I0(\ramplitude_max_reg_reg_n_0_[27] ),
        .I1(ramplitude_lim_reg[27]),
        .I2(\ramplitude_max_reg_reg_n_0_[26] ),
        .I3(ramplitude_lim_reg[26]),
        .O(\ramplitude_max_reg[31]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_13 
       (.I0(\ramplitude_max_reg_reg_n_0_[25] ),
        .I1(ramplitude_lim_reg[25]),
        .I2(\ramplitude_max_reg_reg_n_0_[24] ),
        .I3(ramplitude_lim_reg[24]),
        .O(\ramplitude_max_reg[31]_i_13_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[31]_i_14 
       (.I0(\ramplitude_max_reg_reg_n_0_[31] ),
        .I1(ramplitude_step_reg[31]),
        .O(\ramplitude_max_reg[31]_i_14_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[31]_i_15 
       (.I0(\ramplitude_max_reg_reg_n_0_[30] ),
        .I1(ramplitude_step_reg[30]),
        .O(\ramplitude_max_reg[31]_i_15_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[31]_i_16 
       (.I0(\ramplitude_max_reg_reg_n_0_[29] ),
        .I1(ramplitude_step_reg[29]),
        .O(\ramplitude_max_reg[31]_i_16_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[31]_i_17 
       (.I0(\ramplitude_max_reg_reg_n_0_[28] ),
        .I1(ramplitude_step_reg[28]),
        .O(\ramplitude_max_reg[31]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_19 
       (.I0(ramplitude_lim_reg[23]),
        .I1(\ramplitude_max_reg_reg_n_0_[23] ),
        .I2(ramplitude_lim_reg[22]),
        .I3(\ramplitude_max_reg_reg_n_0_[22] ),
        .O(\ramplitude_max_reg[31]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[31]_i_2 
       (.I0(ramplitude_lim_reg[31]),
        .I1(ramp_module_0_reset[13]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[31]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_20 
       (.I0(ramplitude_lim_reg[21]),
        .I1(\ramplitude_max_reg_reg_n_0_[21] ),
        .I2(ramplitude_lim_reg[20]),
        .I3(\ramplitude_max_reg_reg_n_0_[20] ),
        .O(\ramplitude_max_reg[31]_i_20_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_21 
       (.I0(ramplitude_lim_reg[19]),
        .I1(\ramplitude_max_reg_reg_n_0_[19] ),
        .I2(ramplitude_lim_reg[18]),
        .I3(\ramplitude_max_reg_reg_n_0_[18] ),
        .O(\ramplitude_max_reg[31]_i_21_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_22 
       (.I0(ramplitude_lim_reg[17]),
        .I1(\ramplitude_max_reg_reg_n_0_[17] ),
        .I2(ramplitude_lim_reg[16]),
        .I3(\ramplitude_max_reg_reg_n_0_[16] ),
        .O(\ramplitude_max_reg[31]_i_22_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_23 
       (.I0(\ramplitude_max_reg_reg_n_0_[23] ),
        .I1(ramplitude_lim_reg[23]),
        .I2(\ramplitude_max_reg_reg_n_0_[22] ),
        .I3(ramplitude_lim_reg[22]),
        .O(\ramplitude_max_reg[31]_i_23_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_24 
       (.I0(\ramplitude_max_reg_reg_n_0_[21] ),
        .I1(ramplitude_lim_reg[21]),
        .I2(\ramplitude_max_reg_reg_n_0_[20] ),
        .I3(ramplitude_lim_reg[20]),
        .O(\ramplitude_max_reg[31]_i_24_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_25 
       (.I0(\ramplitude_max_reg_reg_n_0_[19] ),
        .I1(ramplitude_lim_reg[19]),
        .I2(\ramplitude_max_reg_reg_n_0_[18] ),
        .I3(ramplitude_lim_reg[18]),
        .O(\ramplitude_max_reg[31]_i_25_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_26 
       (.I0(\ramplitude_max_reg_reg_n_0_[17] ),
        .I1(ramplitude_lim_reg[17]),
        .I2(\ramplitude_max_reg_reg_n_0_[16] ),
        .I3(ramplitude_lim_reg[16]),
        .O(\ramplitude_max_reg[31]_i_26_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_28 
       (.I0(ramplitude_lim_reg[15]),
        .I1(\ramplitude_max_reg_reg_n_0_[15] ),
        .I2(ramplitude_lim_reg[14]),
        .I3(\ramplitude_max_reg_reg_n_0_[14] ),
        .O(\ramplitude_max_reg[31]_i_28_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_29 
       (.I0(ramplitude_lim_reg[13]),
        .I1(\ramplitude_max_reg_reg_n_0_[13] ),
        .I2(ramplitude_lim_reg[12]),
        .I3(\ramplitude_max_reg_reg_n_0_[12] ),
        .O(\ramplitude_max_reg[31]_i_29_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_30 
       (.I0(ramplitude_lim_reg[11]),
        .I1(\ramplitude_max_reg_reg_n_0_[11] ),
        .I2(ramplitude_lim_reg[10]),
        .I3(\ramplitude_max_reg_reg_n_0_[10] ),
        .O(\ramplitude_max_reg[31]_i_30_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_31 
       (.I0(ramplitude_lim_reg[9]),
        .I1(\ramplitude_max_reg_reg_n_0_[9] ),
        .I2(ramplitude_lim_reg[8]),
        .I3(\ramplitude_max_reg_reg_n_0_[8] ),
        .O(\ramplitude_max_reg[31]_i_31_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_32 
       (.I0(\ramplitude_max_reg_reg_n_0_[15] ),
        .I1(ramplitude_lim_reg[15]),
        .I2(\ramplitude_max_reg_reg_n_0_[14] ),
        .I3(ramplitude_lim_reg[14]),
        .O(\ramplitude_max_reg[31]_i_32_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_33 
       (.I0(\ramplitude_max_reg_reg_n_0_[13] ),
        .I1(ramplitude_lim_reg[13]),
        .I2(\ramplitude_max_reg_reg_n_0_[12] ),
        .I3(ramplitude_lim_reg[12]),
        .O(\ramplitude_max_reg[31]_i_33_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_34 
       (.I0(\ramplitude_max_reg_reg_n_0_[11] ),
        .I1(ramplitude_lim_reg[11]),
        .I2(\ramplitude_max_reg_reg_n_0_[10] ),
        .I3(ramplitude_lim_reg[10]),
        .O(\ramplitude_max_reg[31]_i_34_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_35 
       (.I0(\ramplitude_max_reg_reg_n_0_[9] ),
        .I1(ramplitude_lim_reg[9]),
        .I2(\ramplitude_max_reg_reg_n_0_[8] ),
        .I3(ramplitude_lim_reg[8]),
        .O(\ramplitude_max_reg[31]_i_35_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_36 
       (.I0(ramplitude_lim_reg[7]),
        .I1(\ramplitude_max_reg_reg_n_0_[7] ),
        .I2(ramplitude_lim_reg[6]),
        .I3(\ramplitude_max_reg_reg_n_0_[6] ),
        .O(\ramplitude_max_reg[31]_i_36_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_37 
       (.I0(ramplitude_lim_reg[5]),
        .I1(\ramplitude_max_reg_reg_n_0_[5] ),
        .I2(ramplitude_lim_reg[4]),
        .I3(\ramplitude_max_reg_reg_n_0_[4] ),
        .O(\ramplitude_max_reg[31]_i_37_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_38 
       (.I0(ramplitude_lim_reg[3]),
        .I1(\ramplitude_max_reg_reg_n_0_[3] ),
        .I2(ramplitude_lim_reg[2]),
        .I3(\ramplitude_max_reg_reg_n_0_[2] ),
        .O(\ramplitude_max_reg[31]_i_38_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_39 
       (.I0(ramplitude_lim_reg[1]),
        .I1(\ramplitude_max_reg_reg_n_0_[1] ),
        .I2(ramplitude_lim_reg[0]),
        .I3(\ramplitude_max_reg_reg_n_0_[0] ),
        .O(\ramplitude_max_reg[31]_i_39_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_40 
       (.I0(\ramplitude_max_reg_reg_n_0_[7] ),
        .I1(ramplitude_lim_reg[7]),
        .I2(\ramplitude_max_reg_reg_n_0_[6] ),
        .I3(ramplitude_lim_reg[6]),
        .O(\ramplitude_max_reg[31]_i_40_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_41 
       (.I0(\ramplitude_max_reg_reg_n_0_[5] ),
        .I1(ramplitude_lim_reg[5]),
        .I2(\ramplitude_max_reg_reg_n_0_[4] ),
        .I3(ramplitude_lim_reg[4]),
        .O(\ramplitude_max_reg[31]_i_41_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_42 
       (.I0(\ramplitude_max_reg_reg_n_0_[3] ),
        .I1(ramplitude_lim_reg[3]),
        .I2(\ramplitude_max_reg_reg_n_0_[2] ),
        .I3(ramplitude_lim_reg[2]),
        .O(\ramplitude_max_reg[31]_i_42_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_max_reg[31]_i_43 
       (.I0(\ramplitude_max_reg_reg_n_0_[1] ),
        .I1(ramplitude_lim_reg[1]),
        .I2(\ramplitude_max_reg_reg_n_0_[0] ),
        .I3(ramplitude_lim_reg[0]),
        .O(\ramplitude_max_reg[31]_i_43_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_6 
       (.I0(\ramplitude_max_reg_reg_n_0_[31] ),
        .I1(ramplitude_lim_reg[31]),
        .I2(ramplitude_lim_reg[30]),
        .I3(\ramplitude_max_reg_reg_n_0_[30] ),
        .O(\ramplitude_max_reg[31]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_7 
       (.I0(ramplitude_lim_reg[29]),
        .I1(\ramplitude_max_reg_reg_n_0_[29] ),
        .I2(ramplitude_lim_reg[28]),
        .I3(\ramplitude_max_reg_reg_n_0_[28] ),
        .O(\ramplitude_max_reg[31]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_8 
       (.I0(ramplitude_lim_reg[27]),
        .I1(\ramplitude_max_reg_reg_n_0_[27] ),
        .I2(ramplitude_lim_reg[26]),
        .I3(\ramplitude_max_reg_reg_n_0_[26] ),
        .O(\ramplitude_max_reg[31]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_max_reg[31]_i_9 
       (.I0(ramplitude_lim_reg[25]),
        .I1(\ramplitude_max_reg_reg_n_0_[25] ),
        .I2(ramplitude_lim_reg[24]),
        .I3(\ramplitude_max_reg_reg_n_0_[24] ),
        .O(\ramplitude_max_reg[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[3]_i_1 
       (.I0(ramplitude_lim_reg[3]),
        .I1(\ramp_reg_reg[17]_0 [3]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[3]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[3]));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[3]_i_3 
       (.I0(\ramplitude_max_reg_reg_n_0_[3] ),
        .I1(ramplitude_step_reg[3]),
        .O(\ramplitude_max_reg[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[3]_i_4 
       (.I0(\ramplitude_max_reg_reg_n_0_[2] ),
        .I1(ramplitude_step_reg[2]),
        .O(\ramplitude_max_reg[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[3]_i_5 
       (.I0(\ramplitude_max_reg_reg_n_0_[1] ),
        .I1(ramplitude_step_reg[1]),
        .O(\ramplitude_max_reg[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[3]_i_6 
       (.I0(\ramplitude_max_reg_reg_n_0_[0] ),
        .I1(ramplitude_step_reg[0]),
        .O(\ramplitude_max_reg[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[4]_i_1 
       (.I0(ramplitude_lim_reg[4]),
        .I1(\ramp_reg_reg[17]_0 [4]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[4]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[4]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[5]_i_1 
       (.I0(ramplitude_lim_reg[5]),
        .I1(\ramp_reg_reg[17]_0 [5]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[5]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[5]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[6]_i_1 
       (.I0(ramplitude_lim_reg[6]),
        .I1(\ramp_reg_reg[17]_0 [6]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[6]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[6]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[7]_i_1 
       (.I0(ramplitude_lim_reg[7]),
        .I1(\ramp_reg_reg[17]_0 [7]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[7]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[7]));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[7]_i_3 
       (.I0(\ramplitude_max_reg_reg_n_0_[7] ),
        .I1(ramplitude_step_reg[7]),
        .O(\ramplitude_max_reg[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[7]_i_4 
       (.I0(\ramplitude_max_reg_reg_n_0_[6] ),
        .I1(ramplitude_step_reg[6]),
        .O(\ramplitude_max_reg[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[7]_i_5 
       (.I0(\ramplitude_max_reg_reg_n_0_[5] ),
        .I1(ramplitude_step_reg[5]),
        .O(\ramplitude_max_reg[7]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \ramplitude_max_reg[7]_i_6 
       (.I0(\ramplitude_max_reg_reg_n_0_[4] ),
        .I1(ramplitude_step_reg[4]),
        .O(\ramplitude_max_reg[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[8]_i_1 
       (.I0(ramplitude_lim_reg[8]),
        .I1(\ramp_reg_reg[17]_0 [8]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[8]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[8]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_max_reg[9]_i_1 
       (.I0(ramplitude_lim_reg[9]),
        .I1(\ramp_reg_reg[17]_0 [9]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_max_reg1),
        .I4(ramplitude_max_reg0[9]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(p_1_in[9]));
  FDRE \ramplitude_max_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[0]),
        .Q(\ramplitude_max_reg_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[10]),
        .Q(\ramplitude_max_reg_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[11]),
        .Q(\ramplitude_max_reg_reg_n_0_[11] ),
        .R(1'b0));
  CARRY4 \ramplitude_max_reg_reg[11]_i_2 
       (.CI(\ramplitude_max_reg_reg[7]_i_2_n_0 ),
        .CO({\ramplitude_max_reg_reg[11]_i_2_n_0 ,\ramplitude_max_reg_reg[11]_i_2_n_1 ,\ramplitude_max_reg_reg[11]_i_2_n_2 ,\ramplitude_max_reg_reg[11]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_max_reg_reg_n_0_[11] ,\ramplitude_max_reg_reg_n_0_[10] ,\ramplitude_max_reg_reg_n_0_[9] ,\ramplitude_max_reg_reg_n_0_[8] }),
        .O(ramplitude_max_reg0[11:8]),
        .S({\ramplitude_max_reg[11]_i_3_n_0 ,\ramplitude_max_reg[11]_i_4_n_0 ,\ramplitude_max_reg[11]_i_5_n_0 ,\ramplitude_max_reg[11]_i_6_n_0 }));
  FDRE \ramplitude_max_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[12]),
        .Q(\ramplitude_max_reg_reg_n_0_[12] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[13]),
        .Q(\ramplitude_max_reg_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[14]),
        .Q(\ramplitude_max_reg_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[15]),
        .Q(\ramplitude_max_reg_reg_n_0_[15] ),
        .R(1'b0));
  CARRY4 \ramplitude_max_reg_reg[15]_i_2 
       (.CI(\ramplitude_max_reg_reg[11]_i_2_n_0 ),
        .CO({\ramplitude_max_reg_reg[15]_i_2_n_0 ,\ramplitude_max_reg_reg[15]_i_2_n_1 ,\ramplitude_max_reg_reg[15]_i_2_n_2 ,\ramplitude_max_reg_reg[15]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_max_reg_reg_n_0_[15] ,\ramplitude_max_reg_reg_n_0_[14] ,\ramplitude_max_reg_reg_n_0_[13] ,\ramplitude_max_reg_reg_n_0_[12] }),
        .O(ramplitude_max_reg0[15:12]),
        .S({\ramplitude_max_reg[15]_i_3_n_0 ,\ramplitude_max_reg[15]_i_4_n_0 ,\ramplitude_max_reg[15]_i_5_n_0 ,\ramplitude_max_reg[15]_i_6_n_0 }));
  FDRE \ramplitude_max_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[16]),
        .Q(\ramplitude_max_reg_reg_n_0_[16] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[17]),
        .Q(\ramplitude_max_reg_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[18]),
        .Q(\ramplitude_max_reg_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[19]),
        .Q(\ramplitude_max_reg_reg_n_0_[19] ),
        .R(1'b0));
  CARRY4 \ramplitude_max_reg_reg[19]_i_2 
       (.CI(\ramplitude_max_reg_reg[15]_i_2_n_0 ),
        .CO({\ramplitude_max_reg_reg[19]_i_2_n_0 ,\ramplitude_max_reg_reg[19]_i_2_n_1 ,\ramplitude_max_reg_reg[19]_i_2_n_2 ,\ramplitude_max_reg_reg[19]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_max_reg_reg_n_0_[19] ,\ramplitude_max_reg_reg_n_0_[18] ,\ramplitude_max_reg_reg_n_0_[17] ,\ramplitude_max_reg_reg_n_0_[16] }),
        .O(ramplitude_max_reg0[19:16]),
        .S({\ramplitude_max_reg[19]_i_3_n_0 ,\ramplitude_max_reg[19]_i_4_n_0 ,\ramplitude_max_reg[19]_i_5_n_0 ,\ramplitude_max_reg[19]_i_6_n_0 }));
  FDRE \ramplitude_max_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[1]),
        .Q(\ramplitude_max_reg_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[20]),
        .Q(\ramplitude_max_reg_reg_n_0_[20] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[21]),
        .Q(\ramplitude_max_reg_reg_n_0_[21] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[22]),
        .Q(\ramplitude_max_reg_reg_n_0_[22] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[23]),
        .Q(\ramplitude_max_reg_reg_n_0_[23] ),
        .R(1'b0));
  CARRY4 \ramplitude_max_reg_reg[23]_i_2 
       (.CI(\ramplitude_max_reg_reg[19]_i_2_n_0 ),
        .CO({\ramplitude_max_reg_reg[23]_i_2_n_0 ,\ramplitude_max_reg_reg[23]_i_2_n_1 ,\ramplitude_max_reg_reg[23]_i_2_n_2 ,\ramplitude_max_reg_reg[23]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_max_reg_reg_n_0_[23] ,\ramplitude_max_reg_reg_n_0_[22] ,\ramplitude_max_reg_reg_n_0_[21] ,\ramplitude_max_reg_reg_n_0_[20] }),
        .O(ramplitude_max_reg0[23:20]),
        .S({\ramplitude_max_reg[23]_i_3_n_0 ,\ramplitude_max_reg[23]_i_4_n_0 ,\ramplitude_max_reg[23]_i_5_n_0 ,\ramplitude_max_reg[23]_i_6_n_0 }));
  FDRE \ramplitude_max_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[24]),
        .Q(\ramplitude_max_reg_reg_n_0_[24] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[25]),
        .Q(\ramplitude_max_reg_reg_n_0_[25] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[26]),
        .Q(\ramplitude_max_reg_reg_n_0_[26] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[27]),
        .Q(\ramplitude_max_reg_reg_n_0_[27] ),
        .R(1'b0));
  CARRY4 \ramplitude_max_reg_reg[27]_i_2 
       (.CI(\ramplitude_max_reg_reg[23]_i_2_n_0 ),
        .CO({\ramplitude_max_reg_reg[27]_i_2_n_0 ,\ramplitude_max_reg_reg[27]_i_2_n_1 ,\ramplitude_max_reg_reg[27]_i_2_n_2 ,\ramplitude_max_reg_reg[27]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_max_reg_reg_n_0_[27] ,\ramplitude_max_reg_reg_n_0_[26] ,\ramplitude_max_reg_reg_n_0_[25] ,\ramplitude_max_reg_reg_n_0_[24] }),
        .O(ramplitude_max_reg0[27:24]),
        .S({\ramplitude_max_reg[27]_i_3_n_0 ,\ramplitude_max_reg[27]_i_4_n_0 ,\ramplitude_max_reg[27]_i_5_n_0 ,\ramplitude_max_reg[27]_i_6_n_0 }));
  FDRE \ramplitude_max_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[28]),
        .Q(\ramplitude_max_reg_reg_n_0_[28] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[29]),
        .Q(\ramplitude_max_reg_reg_n_0_[29] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[2]),
        .Q(\ramplitude_max_reg_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[30]),
        .Q(\ramplitude_max_reg_reg_n_0_[30] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[31]),
        .Q(\ramplitude_max_reg_reg_n_0_[31] ),
        .R(1'b0));
  CARRY4 \ramplitude_max_reg_reg[31]_i_18 
       (.CI(\ramplitude_max_reg_reg[31]_i_27_n_0 ),
        .CO({\ramplitude_max_reg_reg[31]_i_18_n_0 ,\ramplitude_max_reg_reg[31]_i_18_n_1 ,\ramplitude_max_reg_reg[31]_i_18_n_2 ,\ramplitude_max_reg_reg[31]_i_18_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_max_reg[31]_i_28_n_0 ,\ramplitude_max_reg[31]_i_29_n_0 ,\ramplitude_max_reg[31]_i_30_n_0 ,\ramplitude_max_reg[31]_i_31_n_0 }),
        .O(\NLW_ramplitude_max_reg_reg[31]_i_18_O_UNCONNECTED [3:0]),
        .S({\ramplitude_max_reg[31]_i_32_n_0 ,\ramplitude_max_reg[31]_i_33_n_0 ,\ramplitude_max_reg[31]_i_34_n_0 ,\ramplitude_max_reg[31]_i_35_n_0 }));
  CARRY4 \ramplitude_max_reg_reg[31]_i_27 
       (.CI(1'b0),
        .CO({\ramplitude_max_reg_reg[31]_i_27_n_0 ,\ramplitude_max_reg_reg[31]_i_27_n_1 ,\ramplitude_max_reg_reg[31]_i_27_n_2 ,\ramplitude_max_reg_reg[31]_i_27_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_max_reg[31]_i_36_n_0 ,\ramplitude_max_reg[31]_i_37_n_0 ,\ramplitude_max_reg[31]_i_38_n_0 ,\ramplitude_max_reg[31]_i_39_n_0 }),
        .O(\NLW_ramplitude_max_reg_reg[31]_i_27_O_UNCONNECTED [3:0]),
        .S({\ramplitude_max_reg[31]_i_40_n_0 ,\ramplitude_max_reg[31]_i_41_n_0 ,\ramplitude_max_reg[31]_i_42_n_0 ,\ramplitude_max_reg[31]_i_43_n_0 }));
  CARRY4 \ramplitude_max_reg_reg[31]_i_3 
       (.CI(\ramplitude_max_reg_reg[31]_i_5_n_0 ),
        .CO({ramplitude_max_reg1,\ramplitude_max_reg_reg[31]_i_3_n_1 ,\ramplitude_max_reg_reg[31]_i_3_n_2 ,\ramplitude_max_reg_reg[31]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_max_reg[31]_i_6_n_0 ,\ramplitude_max_reg[31]_i_7_n_0 ,\ramplitude_max_reg[31]_i_8_n_0 ,\ramplitude_max_reg[31]_i_9_n_0 }),
        .O(\NLW_ramplitude_max_reg_reg[31]_i_3_O_UNCONNECTED [3:0]),
        .S({\ramplitude_max_reg[31]_i_10_n_0 ,\ramplitude_max_reg[31]_i_11_n_0 ,\ramplitude_max_reg[31]_i_12_n_0 ,\ramplitude_max_reg[31]_i_13_n_0 }));
  CARRY4 \ramplitude_max_reg_reg[31]_i_4 
       (.CI(\ramplitude_max_reg_reg[27]_i_2_n_0 ),
        .CO({\NLW_ramplitude_max_reg_reg[31]_i_4_CO_UNCONNECTED [3],\ramplitude_max_reg_reg[31]_i_4_n_1 ,\ramplitude_max_reg_reg[31]_i_4_n_2 ,\ramplitude_max_reg_reg[31]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\ramplitude_max_reg_reg_n_0_[30] ,\ramplitude_max_reg_reg_n_0_[29] ,\ramplitude_max_reg_reg_n_0_[28] }),
        .O(ramplitude_max_reg0[31:28]),
        .S({\ramplitude_max_reg[31]_i_14_n_0 ,\ramplitude_max_reg[31]_i_15_n_0 ,\ramplitude_max_reg[31]_i_16_n_0 ,\ramplitude_max_reg[31]_i_17_n_0 }));
  CARRY4 \ramplitude_max_reg_reg[31]_i_5 
       (.CI(\ramplitude_max_reg_reg[31]_i_18_n_0 ),
        .CO({\ramplitude_max_reg_reg[31]_i_5_n_0 ,\ramplitude_max_reg_reg[31]_i_5_n_1 ,\ramplitude_max_reg_reg[31]_i_5_n_2 ,\ramplitude_max_reg_reg[31]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_max_reg[31]_i_19_n_0 ,\ramplitude_max_reg[31]_i_20_n_0 ,\ramplitude_max_reg[31]_i_21_n_0 ,\ramplitude_max_reg[31]_i_22_n_0 }),
        .O(\NLW_ramplitude_max_reg_reg[31]_i_5_O_UNCONNECTED [3:0]),
        .S({\ramplitude_max_reg[31]_i_23_n_0 ,\ramplitude_max_reg[31]_i_24_n_0 ,\ramplitude_max_reg[31]_i_25_n_0 ,\ramplitude_max_reg[31]_i_26_n_0 }));
  FDRE \ramplitude_max_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[3]),
        .Q(\ramplitude_max_reg_reg_n_0_[3] ),
        .R(1'b0));
  CARRY4 \ramplitude_max_reg_reg[3]_i_2 
       (.CI(1'b0),
        .CO({\ramplitude_max_reg_reg[3]_i_2_n_0 ,\ramplitude_max_reg_reg[3]_i_2_n_1 ,\ramplitude_max_reg_reg[3]_i_2_n_2 ,\ramplitude_max_reg_reg[3]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_max_reg_reg_n_0_[3] ,\ramplitude_max_reg_reg_n_0_[2] ,\ramplitude_max_reg_reg_n_0_[1] ,\ramplitude_max_reg_reg_n_0_[0] }),
        .O(ramplitude_max_reg0[3:0]),
        .S({\ramplitude_max_reg[3]_i_3_n_0 ,\ramplitude_max_reg[3]_i_4_n_0 ,\ramplitude_max_reg[3]_i_5_n_0 ,\ramplitude_max_reg[3]_i_6_n_0 }));
  FDRE \ramplitude_max_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[4]),
        .Q(\ramplitude_max_reg_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[5]),
        .Q(\ramplitude_max_reg_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[6]),
        .Q(\ramplitude_max_reg_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[7]),
        .Q(\ramplitude_max_reg_reg_n_0_[7] ),
        .R(1'b0));
  CARRY4 \ramplitude_max_reg_reg[7]_i_2 
       (.CI(\ramplitude_max_reg_reg[3]_i_2_n_0 ),
        .CO({\ramplitude_max_reg_reg[7]_i_2_n_0 ,\ramplitude_max_reg_reg[7]_i_2_n_1 ,\ramplitude_max_reg_reg[7]_i_2_n_2 ,\ramplitude_max_reg_reg[7]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_max_reg_reg_n_0_[7] ,\ramplitude_max_reg_reg_n_0_[6] ,\ramplitude_max_reg_reg_n_0_[5] ,\ramplitude_max_reg_reg_n_0_[4] }),
        .O(ramplitude_max_reg0[7:4]),
        .S({\ramplitude_max_reg[7]_i_3_n_0 ,\ramplitude_max_reg[7]_i_4_n_0 ,\ramplitude_max_reg[7]_i_5_n_0 ,\ramplitude_max_reg[7]_i_6_n_0 }));
  FDRE \ramplitude_max_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[8]),
        .Q(\ramplitude_max_reg_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \ramplitude_max_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_max_reg[31]_i_1_n_0 ),
        .D(p_1_in[9]),
        .Q(\ramplitude_max_reg_reg_n_0_[9] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hB8BBFFFFB8880000)) 
    \ramplitude_min_reg[0]_i_1 
       (.I0(\ramp_reg_reg[17]_0 [0]),
        .I1(ramp_module_0_rst),
        .I2(ramplitude_min_reg0[0]),
        .I3(ramplitude_min_reg1),
        .I4(\ramplitude_min_reg[31]_i_9_n_0 ),
        .I5(ramplitude_lim_reg[0]),
        .O(\ramplitude_min_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[10]_i_1 
       (.I0(data0[10]),
        .I1(\ramp_reg_reg[17]_0 [10]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[10]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[11]_i_1 
       (.I0(data0[11]),
        .I1(\ramp_reg_reg[17]_0 [11]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[11]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[11]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[11]_i_3 
       (.I0(\ramplitude_min_reg_reg_n_0_[11] ),
        .I1(ramplitude_step_reg[11]),
        .O(\ramplitude_min_reg[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[11]_i_4 
       (.I0(\ramplitude_min_reg_reg_n_0_[10] ),
        .I1(ramplitude_step_reg[10]),
        .O(\ramplitude_min_reg[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[11]_i_5 
       (.I0(\ramplitude_min_reg_reg_n_0_[9] ),
        .I1(ramplitude_step_reg[9]),
        .O(\ramplitude_min_reg[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[11]_i_6 
       (.I0(\ramplitude_min_reg_reg_n_0_[8] ),
        .I1(ramplitude_step_reg[8]),
        .O(\ramplitude_min_reg[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[12]_i_1 
       (.I0(data0[12]),
        .I1(\ramp_reg_reg[17]_0 [12]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[12]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[12]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[12]_i_3 
       (.I0(ramplitude_lim_reg[12]),
        .O(p_0_in[12]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[12]_i_4 
       (.I0(ramplitude_lim_reg[11]),
        .O(p_0_in[11]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[12]_i_5 
       (.I0(ramplitude_lim_reg[10]),
        .O(p_0_in[10]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[12]_i_6 
       (.I0(ramplitude_lim_reg[9]),
        .O(p_0_in[9]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[13]_i_1 
       (.I0(data0[13]),
        .I1(\ramp_reg_reg[17]_0 [13]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[13]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[14]_i_1 
       (.I0(data0[14]),
        .I1(\ramp_reg_reg[17]_0 [14]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[14]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[15]_i_1 
       (.I0(data0[15]),
        .I1(\ramp_reg_reg[17]_0 [15]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[15]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[15]_i_3 
       (.I0(\ramplitude_min_reg_reg_n_0_[15] ),
        .I1(ramplitude_step_reg[15]),
        .O(\ramplitude_min_reg[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[15]_i_4 
       (.I0(\ramplitude_min_reg_reg_n_0_[14] ),
        .I1(ramplitude_step_reg[14]),
        .O(\ramplitude_min_reg[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[15]_i_5 
       (.I0(\ramplitude_min_reg_reg_n_0_[13] ),
        .I1(ramplitude_step_reg[13]),
        .O(\ramplitude_min_reg[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[15]_i_6 
       (.I0(\ramplitude_min_reg_reg_n_0_[12] ),
        .I1(ramplitude_step_reg[12]),
        .O(\ramplitude_min_reg[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[16]_i_1 
       (.I0(data0[16]),
        .I1(\ramp_reg_reg[17]_0 [16]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[16]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[16]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[16]_i_3 
       (.I0(ramplitude_lim_reg[16]),
        .O(p_0_in[16]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[16]_i_4 
       (.I0(ramplitude_lim_reg[15]),
        .O(p_0_in[15]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[16]_i_5 
       (.I0(ramplitude_lim_reg[14]),
        .O(p_0_in[14]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[16]_i_6 
       (.I0(ramplitude_lim_reg[13]),
        .O(p_0_in[13]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[17]_i_1 
       (.I0(data0[17]),
        .I1(\ramp_reg_reg[17]_0 [17]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[17]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[18]_i_1 
       (.I0(data0[18]),
        .I1(ramp_module_0_reset[0]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[18]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[19]_i_1 
       (.I0(data0[19]),
        .I1(ramp_module_0_reset[1]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[19]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[19]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[19]_i_3 
       (.I0(\ramplitude_min_reg_reg_n_0_[19] ),
        .I1(ramplitude_step_reg[19]),
        .O(\ramplitude_min_reg[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[19]_i_4 
       (.I0(\ramplitude_min_reg_reg_n_0_[18] ),
        .I1(ramplitude_step_reg[18]),
        .O(\ramplitude_min_reg[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[19]_i_5 
       (.I0(\ramplitude_min_reg_reg_n_0_[17] ),
        .I1(ramplitude_step_reg[17]),
        .O(\ramplitude_min_reg[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[19]_i_6 
       (.I0(\ramplitude_min_reg_reg_n_0_[16] ),
        .I1(ramplitude_step_reg[16]),
        .O(\ramplitude_min_reg[19]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[1]_i_1 
       (.I0(data0[1]),
        .I1(\ramp_reg_reg[17]_0 [1]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[1]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[20]_i_1 
       (.I0(data0[20]),
        .I1(ramp_module_0_reset[2]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[20]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[20]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[20]_i_3 
       (.I0(ramplitude_lim_reg[20]),
        .O(p_0_in[20]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[20]_i_4 
       (.I0(ramplitude_lim_reg[19]),
        .O(p_0_in[19]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[20]_i_5 
       (.I0(ramplitude_lim_reg[18]),
        .O(p_0_in[18]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[20]_i_6 
       (.I0(ramplitude_lim_reg[17]),
        .O(p_0_in[17]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[21]_i_1 
       (.I0(data0[21]),
        .I1(ramp_module_0_reset[3]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[21]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[22]_i_1 
       (.I0(data0[22]),
        .I1(ramp_module_0_reset[4]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[22]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[23]_i_1 
       (.I0(data0[23]),
        .I1(ramp_module_0_reset[5]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[23]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[23]_i_3 
       (.I0(\ramplitude_min_reg_reg_n_0_[23] ),
        .I1(ramplitude_step_reg[23]),
        .O(\ramplitude_min_reg[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[23]_i_4 
       (.I0(\ramplitude_min_reg_reg_n_0_[22] ),
        .I1(ramplitude_step_reg[22]),
        .O(\ramplitude_min_reg[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[23]_i_5 
       (.I0(\ramplitude_min_reg_reg_n_0_[21] ),
        .I1(ramplitude_step_reg[21]),
        .O(\ramplitude_min_reg[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[23]_i_6 
       (.I0(\ramplitude_min_reg_reg_n_0_[20] ),
        .I1(ramplitude_step_reg[20]),
        .O(\ramplitude_min_reg[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[24]_i_1 
       (.I0(data0[24]),
        .I1(ramp_module_0_reset[6]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[24]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[24]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[24]_i_3 
       (.I0(ramplitude_lim_reg[24]),
        .O(p_0_in[24]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[24]_i_4 
       (.I0(ramplitude_lim_reg[23]),
        .O(p_0_in[23]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[24]_i_5 
       (.I0(ramplitude_lim_reg[22]),
        .O(p_0_in[22]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[24]_i_6 
       (.I0(ramplitude_lim_reg[21]),
        .O(p_0_in[21]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[25]_i_1 
       (.I0(data0[25]),
        .I1(ramp_module_0_reset[7]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[25]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[26]_i_1 
       (.I0(data0[26]),
        .I1(ramp_module_0_reset[8]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[26]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[27]_i_1 
       (.I0(data0[27]),
        .I1(ramp_module_0_reset[9]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[27]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[27]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[27]_i_3 
       (.I0(\ramplitude_min_reg_reg_n_0_[27] ),
        .I1(ramplitude_step_reg[27]),
        .O(\ramplitude_min_reg[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[27]_i_4 
       (.I0(\ramplitude_min_reg_reg_n_0_[26] ),
        .I1(ramplitude_step_reg[26]),
        .O(\ramplitude_min_reg[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[27]_i_5 
       (.I0(\ramplitude_min_reg_reg_n_0_[25] ),
        .I1(ramplitude_step_reg[25]),
        .O(\ramplitude_min_reg[27]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[27]_i_6 
       (.I0(\ramplitude_min_reg_reg_n_0_[24] ),
        .I1(ramplitude_step_reg[24]),
        .O(\ramplitude_min_reg[27]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[28]_i_1 
       (.I0(data0[28]),
        .I1(ramp_module_0_reset[10]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[28]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[28]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[28]_i_3 
       (.I0(ramplitude_lim_reg[28]),
        .O(p_0_in[28]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[28]_i_4 
       (.I0(ramplitude_lim_reg[27]),
        .O(p_0_in[27]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[28]_i_5 
       (.I0(ramplitude_lim_reg[26]),
        .O(p_0_in[26]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[28]_i_6 
       (.I0(ramplitude_lim_reg[25]),
        .O(p_0_in[25]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[29]_i_1 
       (.I0(data0[29]),
        .I1(ramp_module_0_reset[11]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[29]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[2]_i_1 
       (.I0(data0[2]),
        .I1(\ramp_reg_reg[17]_0 [2]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[2]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[30]_i_1 
       (.I0(data0[30]),
        .I1(ramp_module_0_reset[12]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[30]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[30]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hBA)) 
    \ramplitude_min_reg[31]_i_1 
       (.I0(ramp_module_0_rst),
        .I1(\ramplitude_min_reg_reg[31]_i_3_n_0 ),
        .I2(\ramplitude_min_reg_reg[31]_i_4_n_0 ),
        .O(\ramplitude_min_reg[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_100 
       (.I0(\ramplitude_min_reg_reg_n_0_[15] ),
        .I1(data0[15]),
        .I2(\ramplitude_min_reg_reg_n_0_[14] ),
        .I3(data0[14]),
        .O(\ramplitude_min_reg[31]_i_100_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_101 
       (.I0(\ramplitude_min_reg_reg_n_0_[13] ),
        .I1(data0[13]),
        .I2(\ramplitude_min_reg_reg_n_0_[12] ),
        .I3(data0[12]),
        .O(\ramplitude_min_reg[31]_i_101_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_102 
       (.I0(\ramplitude_min_reg_reg_n_0_[11] ),
        .I1(data0[11]),
        .I2(\ramplitude_min_reg_reg_n_0_[10] ),
        .I3(data0[10]),
        .O(\ramplitude_min_reg[31]_i_102_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_103 
       (.I0(\ramplitude_min_reg_reg_n_0_[9] ),
        .I1(data0[9]),
        .I2(\ramplitude_min_reg_reg_n_0_[8] ),
        .I3(data0[8]),
        .O(\ramplitude_min_reg[31]_i_103_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_104 
       (.I0(data0[15]),
        .I1(\ramplitude_min_reg_reg_n_0_[15] ),
        .I2(data0[14]),
        .I3(\ramplitude_min_reg_reg_n_0_[14] ),
        .O(\ramplitude_min_reg[31]_i_104_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_105 
       (.I0(data0[13]),
        .I1(\ramplitude_min_reg_reg_n_0_[13] ),
        .I2(data0[12]),
        .I3(\ramplitude_min_reg_reg_n_0_[12] ),
        .O(\ramplitude_min_reg[31]_i_105_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_106 
       (.I0(data0[11]),
        .I1(\ramplitude_min_reg_reg_n_0_[11] ),
        .I2(data0[10]),
        .I3(\ramplitude_min_reg_reg_n_0_[10] ),
        .O(\ramplitude_min_reg[31]_i_106_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_107 
       (.I0(data0[9]),
        .I1(\ramplitude_min_reg_reg_n_0_[9] ),
        .I2(data0[8]),
        .I3(\ramplitude_min_reg_reg_n_0_[8] ),
        .O(\ramplitude_min_reg[31]_i_107_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_108 
       (.I0(ramp_reg[7]),
        .I1(\ramplitude_max_reg_reg_n_0_[7] ),
        .I2(ramp_reg[6]),
        .I3(\ramplitude_max_reg_reg_n_0_[6] ),
        .O(\ramplitude_min_reg[31]_i_108_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_109 
       (.I0(ramp_reg[5]),
        .I1(\ramplitude_max_reg_reg_n_0_[5] ),
        .I2(ramp_reg[4]),
        .I3(\ramplitude_max_reg_reg_n_0_[4] ),
        .O(\ramplitude_min_reg[31]_i_109_n_0 ));
  LUT4 #(
    .INIT(16'h44D4)) 
    \ramplitude_min_reg[31]_i_11 
       (.I0(ramp_output[13]),
        .I1(\ramplitude_max_reg_reg_n_0_[31] ),
        .I2(ramp_output[12]),
        .I3(\ramplitude_max_reg_reg_n_0_[30] ),
        .O(\ramplitude_min_reg[31]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_110 
       (.I0(ramp_reg[3]),
        .I1(\ramplitude_max_reg_reg_n_0_[3] ),
        .I2(ramp_reg[2]),
        .I3(\ramplitude_max_reg_reg_n_0_[2] ),
        .O(\ramplitude_min_reg[31]_i_110_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_111 
       (.I0(ramp_reg[1]),
        .I1(\ramplitude_max_reg_reg_n_0_[1] ),
        .I2(ramp_reg[0]),
        .I3(\ramplitude_max_reg_reg_n_0_[0] ),
        .O(\ramplitude_min_reg[31]_i_111_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_112 
       (.I0(\ramplitude_max_reg_reg_n_0_[7] ),
        .I1(ramp_reg[7]),
        .I2(\ramplitude_max_reg_reg_n_0_[6] ),
        .I3(ramp_reg[6]),
        .O(\ramplitude_min_reg[31]_i_112_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_113 
       (.I0(\ramplitude_max_reg_reg_n_0_[5] ),
        .I1(ramp_reg[5]),
        .I2(\ramplitude_max_reg_reg_n_0_[4] ),
        .I3(ramp_reg[4]),
        .O(\ramplitude_min_reg[31]_i_113_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_114 
       (.I0(\ramplitude_max_reg_reg_n_0_[3] ),
        .I1(ramp_reg[3]),
        .I2(\ramplitude_max_reg_reg_n_0_[2] ),
        .I3(ramp_reg[2]),
        .O(\ramplitude_min_reg[31]_i_114_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_115 
       (.I0(\ramplitude_max_reg_reg_n_0_[1] ),
        .I1(ramp_reg[1]),
        .I2(\ramplitude_max_reg_reg_n_0_[0] ),
        .I3(ramp_reg[0]),
        .O(\ramplitude_min_reg[31]_i_115_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_116 
       (.I0(\ramplitude_min_reg_reg_n_0_[7] ),
        .I1(ramp_reg[7]),
        .I2(\ramplitude_min_reg_reg_n_0_[6] ),
        .I3(ramp_reg[6]),
        .O(\ramplitude_min_reg[31]_i_116_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_117 
       (.I0(\ramplitude_min_reg_reg_n_0_[5] ),
        .I1(ramp_reg[5]),
        .I2(\ramplitude_min_reg_reg_n_0_[4] ),
        .I3(ramp_reg[4]),
        .O(\ramplitude_min_reg[31]_i_117_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_118 
       (.I0(\ramplitude_min_reg_reg_n_0_[3] ),
        .I1(ramp_reg[3]),
        .I2(\ramplitude_min_reg_reg_n_0_[2] ),
        .I3(ramp_reg[2]),
        .O(\ramplitude_min_reg[31]_i_118_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_119 
       (.I0(\ramplitude_min_reg_reg_n_0_[1] ),
        .I1(ramp_reg[1]),
        .I2(\ramplitude_min_reg_reg_n_0_[0] ),
        .I3(ramp_reg[0]),
        .O(\ramplitude_min_reg[31]_i_119_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_12 
       (.I0(ramp_output[11]),
        .I1(\ramplitude_max_reg_reg_n_0_[29] ),
        .I2(ramp_output[10]),
        .I3(\ramplitude_max_reg_reg_n_0_[28] ),
        .O(\ramplitude_min_reg[31]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_120 
       (.I0(ramp_reg[7]),
        .I1(\ramplitude_min_reg_reg_n_0_[7] ),
        .I2(ramp_reg[6]),
        .I3(\ramplitude_min_reg_reg_n_0_[6] ),
        .O(\ramplitude_min_reg[31]_i_120_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_121 
       (.I0(ramp_reg[5]),
        .I1(\ramplitude_min_reg_reg_n_0_[5] ),
        .I2(ramp_reg[4]),
        .I3(\ramplitude_min_reg_reg_n_0_[4] ),
        .O(\ramplitude_min_reg[31]_i_121_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_122 
       (.I0(ramp_reg[3]),
        .I1(\ramplitude_min_reg_reg_n_0_[3] ),
        .I2(ramp_reg[2]),
        .I3(\ramplitude_min_reg_reg_n_0_[2] ),
        .O(\ramplitude_min_reg[31]_i_122_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_123 
       (.I0(ramp_reg[1]),
        .I1(\ramplitude_min_reg_reg_n_0_[1] ),
        .I2(ramp_reg[0]),
        .I3(\ramplitude_min_reg_reg_n_0_[0] ),
        .O(\ramplitude_min_reg[31]_i_123_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_124 
       (.I0(\ramplitude_min_reg_reg_n_0_[7] ),
        .I1(data0[7]),
        .I2(\ramplitude_min_reg_reg_n_0_[6] ),
        .I3(data0[6]),
        .O(\ramplitude_min_reg[31]_i_124_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_125 
       (.I0(\ramplitude_min_reg_reg_n_0_[5] ),
        .I1(data0[5]),
        .I2(\ramplitude_min_reg_reg_n_0_[4] ),
        .I3(data0[4]),
        .O(\ramplitude_min_reg[31]_i_125_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_126 
       (.I0(\ramplitude_min_reg_reg_n_0_[3] ),
        .I1(data0[3]),
        .I2(\ramplitude_min_reg_reg_n_0_[2] ),
        .I3(data0[2]),
        .O(\ramplitude_min_reg[31]_i_126_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_127 
       (.I0(\ramplitude_min_reg_reg_n_0_[1] ),
        .I1(data0[1]),
        .I2(\ramplitude_min_reg_reg_n_0_[0] ),
        .I3(ramplitude_lim_reg[0]),
        .O(\ramplitude_min_reg[31]_i_127_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_128 
       (.I0(data0[7]),
        .I1(\ramplitude_min_reg_reg_n_0_[7] ),
        .I2(data0[6]),
        .I3(\ramplitude_min_reg_reg_n_0_[6] ),
        .O(\ramplitude_min_reg[31]_i_128_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_129 
       (.I0(data0[5]),
        .I1(\ramplitude_min_reg_reg_n_0_[5] ),
        .I2(data0[4]),
        .I3(\ramplitude_min_reg_reg_n_0_[4] ),
        .O(\ramplitude_min_reg[31]_i_129_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_13 
       (.I0(ramp_output[9]),
        .I1(\ramplitude_max_reg_reg_n_0_[27] ),
        .I2(ramp_output[8]),
        .I3(\ramplitude_max_reg_reg_n_0_[26] ),
        .O(\ramplitude_min_reg[31]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_130 
       (.I0(data0[3]),
        .I1(\ramplitude_min_reg_reg_n_0_[3] ),
        .I2(data0[2]),
        .I3(\ramplitude_min_reg_reg_n_0_[2] ),
        .O(\ramplitude_min_reg[31]_i_130_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_131 
       (.I0(data0[1]),
        .I1(\ramplitude_min_reg_reg_n_0_[1] ),
        .I2(ramplitude_lim_reg[0]),
        .I3(\ramplitude_min_reg_reg_n_0_[0] ),
        .O(\ramplitude_min_reg[31]_i_131_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_14 
       (.I0(ramp_output[7]),
        .I1(\ramplitude_max_reg_reg_n_0_[25] ),
        .I2(ramp_output[6]),
        .I3(\ramplitude_max_reg_reg_n_0_[24] ),
        .O(\ramplitude_min_reg[31]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_15 
       (.I0(\ramplitude_max_reg_reg_n_0_[31] ),
        .I1(ramp_output[13]),
        .I2(\ramplitude_max_reg_reg_n_0_[30] ),
        .I3(ramp_output[12]),
        .O(\ramplitude_min_reg[31]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_16 
       (.I0(\ramplitude_max_reg_reg_n_0_[29] ),
        .I1(ramp_output[11]),
        .I2(\ramplitude_max_reg_reg_n_0_[28] ),
        .I3(ramp_output[10]),
        .O(\ramplitude_min_reg[31]_i_16_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_17 
       (.I0(\ramplitude_max_reg_reg_n_0_[27] ),
        .I1(ramp_output[9]),
        .I2(\ramplitude_max_reg_reg_n_0_[26] ),
        .I3(ramp_output[8]),
        .O(\ramplitude_min_reg[31]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_18 
       (.I0(\ramplitude_max_reg_reg_n_0_[25] ),
        .I1(ramp_output[7]),
        .I2(\ramplitude_max_reg_reg_n_0_[24] ),
        .I3(ramp_output[6]),
        .O(\ramplitude_min_reg[31]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[31]_i_2 
       (.I0(data0[31]),
        .I1(ramp_module_0_reset[13]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[31]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[31]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_20 
       (.I0(ramp_output[13]),
        .I1(\ramplitude_min_reg_reg_n_0_[31] ),
        .I2(\ramplitude_min_reg_reg_n_0_[30] ),
        .I3(ramp_output[12]),
        .O(\ramplitude_min_reg[31]_i_20_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_21 
       (.I0(\ramplitude_min_reg_reg_n_0_[29] ),
        .I1(ramp_output[11]),
        .I2(\ramplitude_min_reg_reg_n_0_[28] ),
        .I3(ramp_output[10]),
        .O(\ramplitude_min_reg[31]_i_21_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_22 
       (.I0(\ramplitude_min_reg_reg_n_0_[27] ),
        .I1(ramp_output[9]),
        .I2(\ramplitude_min_reg_reg_n_0_[26] ),
        .I3(ramp_output[8]),
        .O(\ramplitude_min_reg[31]_i_22_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_23 
       (.I0(\ramplitude_min_reg_reg_n_0_[25] ),
        .I1(ramp_output[7]),
        .I2(\ramplitude_min_reg_reg_n_0_[24] ),
        .I3(ramp_output[6]),
        .O(\ramplitude_min_reg[31]_i_23_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_24 
       (.I0(\ramplitude_min_reg_reg_n_0_[31] ),
        .I1(ramp_output[13]),
        .I2(ramp_output[12]),
        .I3(\ramplitude_min_reg_reg_n_0_[30] ),
        .O(\ramplitude_min_reg[31]_i_24_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_25 
       (.I0(ramp_output[11]),
        .I1(\ramplitude_min_reg_reg_n_0_[29] ),
        .I2(ramp_output[10]),
        .I3(\ramplitude_min_reg_reg_n_0_[28] ),
        .O(\ramplitude_min_reg[31]_i_25_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_26 
       (.I0(ramp_output[9]),
        .I1(\ramplitude_min_reg_reg_n_0_[27] ),
        .I2(ramp_output[8]),
        .I3(\ramplitude_min_reg_reg_n_0_[26] ),
        .O(\ramplitude_min_reg[31]_i_26_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_27 
       (.I0(ramp_output[7]),
        .I1(\ramplitude_min_reg_reg_n_0_[25] ),
        .I2(ramp_output[6]),
        .I3(\ramplitude_min_reg_reg_n_0_[24] ),
        .O(\ramplitude_min_reg[31]_i_27_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[31]_i_28 
       (.I0(ramplitude_lim_reg[31]),
        .O(\ramplitude_min_reg[31]_i_28_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[31]_i_29 
       (.I0(ramplitude_lim_reg[30]),
        .O(p_0_in[30]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[31]_i_30 
       (.I0(ramplitude_lim_reg[29]),
        .O(p_0_in[29]));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_34 
       (.I0(data0[31]),
        .I1(\ramplitude_min_reg_reg_n_0_[31] ),
        .I2(\ramplitude_min_reg_reg_n_0_[30] ),
        .I3(data0[30]),
        .O(\ramplitude_min_reg[31]_i_34_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_35 
       (.I0(\ramplitude_min_reg_reg_n_0_[29] ),
        .I1(data0[29]),
        .I2(\ramplitude_min_reg_reg_n_0_[28] ),
        .I3(data0[28]),
        .O(\ramplitude_min_reg[31]_i_35_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_36 
       (.I0(\ramplitude_min_reg_reg_n_0_[27] ),
        .I1(data0[27]),
        .I2(\ramplitude_min_reg_reg_n_0_[26] ),
        .I3(data0[26]),
        .O(\ramplitude_min_reg[31]_i_36_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_37 
       (.I0(\ramplitude_min_reg_reg_n_0_[25] ),
        .I1(data0[25]),
        .I2(\ramplitude_min_reg_reg_n_0_[24] ),
        .I3(data0[24]),
        .O(\ramplitude_min_reg[31]_i_37_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_38 
       (.I0(\ramplitude_min_reg_reg_n_0_[31] ),
        .I1(data0[31]),
        .I2(data0[30]),
        .I3(\ramplitude_min_reg_reg_n_0_[30] ),
        .O(\ramplitude_min_reg[31]_i_38_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_39 
       (.I0(data0[29]),
        .I1(\ramplitude_min_reg_reg_n_0_[29] ),
        .I2(data0[28]),
        .I3(\ramplitude_min_reg_reg_n_0_[28] ),
        .O(\ramplitude_min_reg[31]_i_39_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_40 
       (.I0(data0[27]),
        .I1(\ramplitude_min_reg_reg_n_0_[27] ),
        .I2(data0[26]),
        .I3(\ramplitude_min_reg_reg_n_0_[26] ),
        .O(\ramplitude_min_reg[31]_i_40_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_41 
       (.I0(data0[25]),
        .I1(\ramplitude_min_reg_reg_n_0_[25] ),
        .I2(data0[24]),
        .I3(\ramplitude_min_reg_reg_n_0_[24] ),
        .O(\ramplitude_min_reg[31]_i_41_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[31]_i_42 
       (.I0(\ramplitude_min_reg_reg_n_0_[31] ),
        .I1(ramplitude_step_reg[31]),
        .O(\ramplitude_min_reg[31]_i_42_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[31]_i_43 
       (.I0(\ramplitude_min_reg_reg_n_0_[30] ),
        .I1(ramplitude_step_reg[30]),
        .O(\ramplitude_min_reg[31]_i_43_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[31]_i_44 
       (.I0(\ramplitude_min_reg_reg_n_0_[29] ),
        .I1(ramplitude_step_reg[29]),
        .O(\ramplitude_min_reg[31]_i_44_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[31]_i_45 
       (.I0(\ramplitude_min_reg_reg_n_0_[28] ),
        .I1(ramplitude_step_reg[28]),
        .O(\ramplitude_min_reg[31]_i_45_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \ramplitude_min_reg[31]_i_46 
       (.I0(ramplitude_step_reg[10]),
        .I1(ramplitude_step_reg[9]),
        .I2(ramplitude_step_reg[11]),
        .I3(ramplitude_step_reg[8]),
        .I4(\ramplitude_min_reg[31]_i_77_n_0 ),
        .O(\ramplitude_min_reg[31]_i_46_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \ramplitude_min_reg[31]_i_47 
       (.I0(ramplitude_step_reg[30]),
        .I1(ramplitude_step_reg[29]),
        .I2(ramplitude_step_reg[31]),
        .I3(ramplitude_step_reg[28]),
        .I4(\ramplitude_min_reg[31]_i_78_n_0 ),
        .O(\ramplitude_min_reg[31]_i_47_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \ramplitude_min_reg[31]_i_48 
       (.I0(ramplitude_step_reg[18]),
        .I1(ramplitude_step_reg[17]),
        .I2(ramplitude_step_reg[19]),
        .I3(ramplitude_step_reg[16]),
        .I4(\ramplitude_min_reg[31]_i_79_n_0 ),
        .O(\ramplitude_min_reg[31]_i_48_n_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    \ramplitude_min_reg[31]_i_49 
       (.I0(ramplitude_step_reg[6]),
        .I1(ramplitude_step_reg[5]),
        .I2(ramplitude_step_reg[7]),
        .I3(ramplitude_step_reg[4]),
        .I4(\ramplitude_min_reg[31]_i_80_n_0 ),
        .O(\ramplitude_min_reg[31]_i_49_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_51 
       (.I0(ramp_output[5]),
        .I1(\ramplitude_max_reg_reg_n_0_[23] ),
        .I2(ramp_output[4]),
        .I3(\ramplitude_max_reg_reg_n_0_[22] ),
        .O(\ramplitude_min_reg[31]_i_51_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_52 
       (.I0(ramp_output[3]),
        .I1(\ramplitude_max_reg_reg_n_0_[21] ),
        .I2(ramp_output[2]),
        .I3(\ramplitude_max_reg_reg_n_0_[20] ),
        .O(\ramplitude_min_reg[31]_i_52_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_53 
       (.I0(ramp_output[1]),
        .I1(\ramplitude_max_reg_reg_n_0_[19] ),
        .I2(ramp_output[0]),
        .I3(\ramplitude_max_reg_reg_n_0_[18] ),
        .O(\ramplitude_min_reg[31]_i_53_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_54 
       (.I0(ramp_reg[17]),
        .I1(\ramplitude_max_reg_reg_n_0_[17] ),
        .I2(ramp_reg[16]),
        .I3(\ramplitude_max_reg_reg_n_0_[16] ),
        .O(\ramplitude_min_reg[31]_i_54_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_55 
       (.I0(\ramplitude_max_reg_reg_n_0_[23] ),
        .I1(ramp_output[5]),
        .I2(\ramplitude_max_reg_reg_n_0_[22] ),
        .I3(ramp_output[4]),
        .O(\ramplitude_min_reg[31]_i_55_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_56 
       (.I0(\ramplitude_max_reg_reg_n_0_[21] ),
        .I1(ramp_output[3]),
        .I2(\ramplitude_max_reg_reg_n_0_[20] ),
        .I3(ramp_output[2]),
        .O(\ramplitude_min_reg[31]_i_56_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_57 
       (.I0(\ramplitude_max_reg_reg_n_0_[19] ),
        .I1(ramp_output[1]),
        .I2(\ramplitude_max_reg_reg_n_0_[18] ),
        .I3(ramp_output[0]),
        .O(\ramplitude_min_reg[31]_i_57_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_58 
       (.I0(\ramplitude_max_reg_reg_n_0_[17] ),
        .I1(ramp_reg[17]),
        .I2(\ramplitude_max_reg_reg_n_0_[16] ),
        .I3(ramp_reg[16]),
        .O(\ramplitude_min_reg[31]_i_58_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_60 
       (.I0(\ramplitude_min_reg_reg_n_0_[23] ),
        .I1(ramp_output[5]),
        .I2(\ramplitude_min_reg_reg_n_0_[22] ),
        .I3(ramp_output[4]),
        .O(\ramplitude_min_reg[31]_i_60_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_61 
       (.I0(\ramplitude_min_reg_reg_n_0_[21] ),
        .I1(ramp_output[3]),
        .I2(\ramplitude_min_reg_reg_n_0_[20] ),
        .I3(ramp_output[2]),
        .O(\ramplitude_min_reg[31]_i_61_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_62 
       (.I0(\ramplitude_min_reg_reg_n_0_[19] ),
        .I1(ramp_output[1]),
        .I2(\ramplitude_min_reg_reg_n_0_[18] ),
        .I3(ramp_output[0]),
        .O(\ramplitude_min_reg[31]_i_62_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_63 
       (.I0(\ramplitude_min_reg_reg_n_0_[17] ),
        .I1(ramp_reg[17]),
        .I2(\ramplitude_min_reg_reg_n_0_[16] ),
        .I3(ramp_reg[16]),
        .O(\ramplitude_min_reg[31]_i_63_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_64 
       (.I0(ramp_output[5]),
        .I1(\ramplitude_min_reg_reg_n_0_[23] ),
        .I2(ramp_output[4]),
        .I3(\ramplitude_min_reg_reg_n_0_[22] ),
        .O(\ramplitude_min_reg[31]_i_64_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_65 
       (.I0(ramp_output[3]),
        .I1(\ramplitude_min_reg_reg_n_0_[21] ),
        .I2(ramp_output[2]),
        .I3(\ramplitude_min_reg_reg_n_0_[20] ),
        .O(\ramplitude_min_reg[31]_i_65_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_66 
       (.I0(ramp_output[1]),
        .I1(\ramplitude_min_reg_reg_n_0_[19] ),
        .I2(ramp_output[0]),
        .I3(\ramplitude_min_reg_reg_n_0_[18] ),
        .O(\ramplitude_min_reg[31]_i_66_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_67 
       (.I0(ramp_reg[17]),
        .I1(\ramplitude_min_reg_reg_n_0_[17] ),
        .I2(ramp_reg[16]),
        .I3(\ramplitude_min_reg_reg_n_0_[16] ),
        .O(\ramplitude_min_reg[31]_i_67_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_69 
       (.I0(\ramplitude_min_reg_reg_n_0_[23] ),
        .I1(data0[23]),
        .I2(\ramplitude_min_reg_reg_n_0_[22] ),
        .I3(data0[22]),
        .O(\ramplitude_min_reg[31]_i_69_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_70 
       (.I0(\ramplitude_min_reg_reg_n_0_[21] ),
        .I1(data0[21]),
        .I2(\ramplitude_min_reg_reg_n_0_[20] ),
        .I3(data0[20]),
        .O(\ramplitude_min_reg[31]_i_70_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_71 
       (.I0(\ramplitude_min_reg_reg_n_0_[19] ),
        .I1(data0[19]),
        .I2(\ramplitude_min_reg_reg_n_0_[18] ),
        .I3(data0[18]),
        .O(\ramplitude_min_reg[31]_i_71_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_72 
       (.I0(\ramplitude_min_reg_reg_n_0_[17] ),
        .I1(data0[17]),
        .I2(\ramplitude_min_reg_reg_n_0_[16] ),
        .I3(data0[16]),
        .O(\ramplitude_min_reg[31]_i_72_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_73 
       (.I0(data0[23]),
        .I1(\ramplitude_min_reg_reg_n_0_[23] ),
        .I2(data0[22]),
        .I3(\ramplitude_min_reg_reg_n_0_[22] ),
        .O(\ramplitude_min_reg[31]_i_73_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_74 
       (.I0(data0[21]),
        .I1(\ramplitude_min_reg_reg_n_0_[21] ),
        .I2(data0[20]),
        .I3(\ramplitude_min_reg_reg_n_0_[20] ),
        .O(\ramplitude_min_reg[31]_i_74_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_75 
       (.I0(data0[19]),
        .I1(\ramplitude_min_reg_reg_n_0_[19] ),
        .I2(data0[18]),
        .I3(\ramplitude_min_reg_reg_n_0_[18] ),
        .O(\ramplitude_min_reg[31]_i_75_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_76 
       (.I0(data0[17]),
        .I1(\ramplitude_min_reg_reg_n_0_[17] ),
        .I2(data0[16]),
        .I3(\ramplitude_min_reg_reg_n_0_[16] ),
        .O(\ramplitude_min_reg[31]_i_76_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ramplitude_min_reg[31]_i_77 
       (.I0(ramplitude_step_reg[24]),
        .I1(ramplitude_step_reg[27]),
        .I2(ramplitude_step_reg[25]),
        .I3(ramplitude_step_reg[26]),
        .O(\ramplitude_min_reg[31]_i_77_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ramplitude_min_reg[31]_i_78 
       (.I0(ramplitude_step_reg[12]),
        .I1(ramplitude_step_reg[15]),
        .I2(ramplitude_step_reg[13]),
        .I3(ramplitude_step_reg[14]),
        .O(\ramplitude_min_reg[31]_i_78_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ramplitude_min_reg[31]_i_79 
       (.I0(ramplitude_step_reg[0]),
        .I1(ramplitude_step_reg[3]),
        .I2(ramplitude_step_reg[1]),
        .I3(ramplitude_step_reg[2]),
        .O(\ramplitude_min_reg[31]_i_79_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ramplitude_min_reg[31]_i_80 
       (.I0(ramplitude_step_reg[20]),
        .I1(ramplitude_step_reg[23]),
        .I2(ramplitude_step_reg[21]),
        .I3(ramplitude_step_reg[22]),
        .O(\ramplitude_min_reg[31]_i_80_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_82 
       (.I0(ramp_reg[15]),
        .I1(\ramplitude_max_reg_reg_n_0_[15] ),
        .I2(ramp_reg[14]),
        .I3(\ramplitude_max_reg_reg_n_0_[14] ),
        .O(\ramplitude_min_reg[31]_i_82_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_83 
       (.I0(ramp_reg[13]),
        .I1(\ramplitude_max_reg_reg_n_0_[13] ),
        .I2(ramp_reg[12]),
        .I3(\ramplitude_max_reg_reg_n_0_[12] ),
        .O(\ramplitude_min_reg[31]_i_83_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_84 
       (.I0(ramp_reg[11]),
        .I1(\ramplitude_max_reg_reg_n_0_[11] ),
        .I2(ramp_reg[10]),
        .I3(\ramplitude_max_reg_reg_n_0_[10] ),
        .O(\ramplitude_min_reg[31]_i_84_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_85 
       (.I0(ramp_reg[9]),
        .I1(\ramplitude_max_reg_reg_n_0_[9] ),
        .I2(ramp_reg[8]),
        .I3(\ramplitude_max_reg_reg_n_0_[8] ),
        .O(\ramplitude_min_reg[31]_i_85_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_86 
       (.I0(\ramplitude_max_reg_reg_n_0_[15] ),
        .I1(ramp_reg[15]),
        .I2(\ramplitude_max_reg_reg_n_0_[14] ),
        .I3(ramp_reg[14]),
        .O(\ramplitude_min_reg[31]_i_86_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_87 
       (.I0(\ramplitude_max_reg_reg_n_0_[13] ),
        .I1(ramp_reg[13]),
        .I2(\ramplitude_max_reg_reg_n_0_[12] ),
        .I3(ramp_reg[12]),
        .O(\ramplitude_min_reg[31]_i_87_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_88 
       (.I0(\ramplitude_max_reg_reg_n_0_[11] ),
        .I1(ramp_reg[11]),
        .I2(\ramplitude_max_reg_reg_n_0_[10] ),
        .I3(ramp_reg[10]),
        .O(\ramplitude_min_reg[31]_i_88_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_89 
       (.I0(\ramplitude_max_reg_reg_n_0_[9] ),
        .I1(ramp_reg[9]),
        .I2(\ramplitude_max_reg_reg_n_0_[8] ),
        .I3(ramp_reg[8]),
        .O(\ramplitude_min_reg[31]_i_89_n_0 ));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \ramplitude_min_reg[31]_i_9 
       (.I0(\ramplitude_min_reg[31]_i_46_n_0 ),
        .I1(\ramplitude_min_reg[31]_i_47_n_0 ),
        .I2(\ramplitude_min_reg[31]_i_48_n_0 ),
        .I3(\ramplitude_min_reg[31]_i_49_n_0 ),
        .O(\ramplitude_min_reg[31]_i_9_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_91 
       (.I0(\ramplitude_min_reg_reg_n_0_[15] ),
        .I1(ramp_reg[15]),
        .I2(\ramplitude_min_reg_reg_n_0_[14] ),
        .I3(ramp_reg[14]),
        .O(\ramplitude_min_reg[31]_i_91_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_92 
       (.I0(\ramplitude_min_reg_reg_n_0_[13] ),
        .I1(ramp_reg[13]),
        .I2(\ramplitude_min_reg_reg_n_0_[12] ),
        .I3(ramp_reg[12]),
        .O(\ramplitude_min_reg[31]_i_92_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_93 
       (.I0(\ramplitude_min_reg_reg_n_0_[11] ),
        .I1(ramp_reg[11]),
        .I2(\ramplitude_min_reg_reg_n_0_[10] ),
        .I3(ramp_reg[10]),
        .O(\ramplitude_min_reg[31]_i_93_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \ramplitude_min_reg[31]_i_94 
       (.I0(\ramplitude_min_reg_reg_n_0_[9] ),
        .I1(ramp_reg[9]),
        .I2(\ramplitude_min_reg_reg_n_0_[8] ),
        .I3(ramp_reg[8]),
        .O(\ramplitude_min_reg[31]_i_94_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_95 
       (.I0(ramp_reg[15]),
        .I1(\ramplitude_min_reg_reg_n_0_[15] ),
        .I2(ramp_reg[14]),
        .I3(\ramplitude_min_reg_reg_n_0_[14] ),
        .O(\ramplitude_min_reg[31]_i_95_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_96 
       (.I0(ramp_reg[13]),
        .I1(\ramplitude_min_reg_reg_n_0_[13] ),
        .I2(ramp_reg[12]),
        .I3(\ramplitude_min_reg_reg_n_0_[12] ),
        .O(\ramplitude_min_reg[31]_i_96_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_97 
       (.I0(ramp_reg[11]),
        .I1(\ramplitude_min_reg_reg_n_0_[11] ),
        .I2(ramp_reg[10]),
        .I3(\ramplitude_min_reg_reg_n_0_[10] ),
        .O(\ramplitude_min_reg[31]_i_97_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ramplitude_min_reg[31]_i_98 
       (.I0(ramp_reg[9]),
        .I1(\ramplitude_min_reg_reg_n_0_[9] ),
        .I2(ramp_reg[8]),
        .I3(\ramplitude_min_reg_reg_n_0_[8] ),
        .O(\ramplitude_min_reg[31]_i_98_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[3]_i_1 
       (.I0(data0[3]),
        .I1(\ramp_reg_reg[17]_0 [3]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[3]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[3]_i_3 
       (.I0(\ramplitude_min_reg_reg_n_0_[3] ),
        .I1(ramplitude_step_reg[3]),
        .O(\ramplitude_min_reg[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[3]_i_4 
       (.I0(\ramplitude_min_reg_reg_n_0_[2] ),
        .I1(ramplitude_step_reg[2]),
        .O(\ramplitude_min_reg[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[3]_i_5 
       (.I0(\ramplitude_min_reg_reg_n_0_[1] ),
        .I1(ramplitude_step_reg[1]),
        .O(\ramplitude_min_reg[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[3]_i_6 
       (.I0(\ramplitude_min_reg_reg_n_0_[0] ),
        .I1(ramplitude_step_reg[0]),
        .O(\ramplitude_min_reg[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[4]_i_1 
       (.I0(data0[4]),
        .I1(\ramp_reg_reg[17]_0 [4]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[4]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[4]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[4]_i_3 
       (.I0(ramplitude_lim_reg[0]),
        .O(p_0_in[0]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[4]_i_4 
       (.I0(ramplitude_lim_reg[4]),
        .O(p_0_in[4]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[4]_i_5 
       (.I0(ramplitude_lim_reg[3]),
        .O(p_0_in[3]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[4]_i_6 
       (.I0(ramplitude_lim_reg[2]),
        .O(p_0_in[2]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[4]_i_7 
       (.I0(ramplitude_lim_reg[1]),
        .O(p_0_in[1]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[5]_i_1 
       (.I0(data0[5]),
        .I1(\ramp_reg_reg[17]_0 [5]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[5]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[6]_i_1 
       (.I0(data0[6]),
        .I1(\ramp_reg_reg[17]_0 [6]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[6]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[7]_i_1 
       (.I0(data0[7]),
        .I1(\ramp_reg_reg[17]_0 [7]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[7]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[7]_i_3 
       (.I0(\ramplitude_min_reg_reg_n_0_[7] ),
        .I1(ramplitude_step_reg[7]),
        .O(\ramplitude_min_reg[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[7]_i_4 
       (.I0(\ramplitude_min_reg_reg_n_0_[6] ),
        .I1(ramplitude_step_reg[6]),
        .O(\ramplitude_min_reg[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[7]_i_5 
       (.I0(\ramplitude_min_reg_reg_n_0_[5] ),
        .I1(ramplitude_step_reg[5]),
        .O(\ramplitude_min_reg[7]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \ramplitude_min_reg[7]_i_6 
       (.I0(\ramplitude_min_reg_reg_n_0_[4] ),
        .I1(ramplitude_step_reg[4]),
        .O(\ramplitude_min_reg[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[8]_i_1 
       (.I0(data0[8]),
        .I1(\ramp_reg_reg[17]_0 [8]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[8]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[8]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[8]_i_3 
       (.I0(ramplitude_lim_reg[8]),
        .O(p_0_in[8]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[8]_i_4 
       (.I0(ramplitude_lim_reg[7]),
        .O(p_0_in[7]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[8]_i_5 
       (.I0(ramplitude_lim_reg[6]),
        .O(p_0_in[6]));
  LUT1 #(
    .INIT(2'h1)) 
    \ramplitude_min_reg[8]_i_6 
       (.I0(ramplitude_lim_reg[5]),
        .O(p_0_in[5]));
  LUT6 #(
    .INIT(64'hCFCAC0CAAAAAAAAA)) 
    \ramplitude_min_reg[9]_i_1 
       (.I0(data0[9]),
        .I1(\ramp_reg_reg[17]_0 [9]),
        .I2(ramp_module_0_rst),
        .I3(ramplitude_min_reg1),
        .I4(ramplitude_min_reg0[9]),
        .I5(\ramplitude_min_reg[31]_i_9_n_0 ),
        .O(\ramplitude_min_reg[9]_i_1_n_0 ));
  FDRE \ramplitude_min_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[0]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[10]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[11]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[11] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[11]_i_2 
       (.CI(\ramplitude_min_reg_reg[7]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[11]_i_2_n_0 ,\ramplitude_min_reg_reg[11]_i_2_n_1 ,\ramplitude_min_reg_reg[11]_i_2_n_2 ,\ramplitude_min_reg_reg[11]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg_reg_n_0_[11] ,\ramplitude_min_reg_reg_n_0_[10] ,\ramplitude_min_reg_reg_n_0_[9] ,\ramplitude_min_reg_reg_n_0_[8] }),
        .O(ramplitude_min_reg0[11:8]),
        .S({\ramplitude_min_reg[11]_i_3_n_0 ,\ramplitude_min_reg[11]_i_4_n_0 ,\ramplitude_min_reg[11]_i_5_n_0 ,\ramplitude_min_reg[11]_i_6_n_0 }));
  FDRE \ramplitude_min_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[12]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[12] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[12]_i_2 
       (.CI(\ramplitude_min_reg_reg[8]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[12]_i_2_n_0 ,\ramplitude_min_reg_reg[12]_i_2_n_1 ,\ramplitude_min_reg_reg[12]_i_2_n_2 ,\ramplitude_min_reg_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(p_0_in[12:9]));
  FDRE \ramplitude_min_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[13]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[14]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[15]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[15] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[15]_i_2 
       (.CI(\ramplitude_min_reg_reg[11]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[15]_i_2_n_0 ,\ramplitude_min_reg_reg[15]_i_2_n_1 ,\ramplitude_min_reg_reg[15]_i_2_n_2 ,\ramplitude_min_reg_reg[15]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg_reg_n_0_[15] ,\ramplitude_min_reg_reg_n_0_[14] ,\ramplitude_min_reg_reg_n_0_[13] ,\ramplitude_min_reg_reg_n_0_[12] }),
        .O(ramplitude_min_reg0[15:12]),
        .S({\ramplitude_min_reg[15]_i_3_n_0 ,\ramplitude_min_reg[15]_i_4_n_0 ,\ramplitude_min_reg[15]_i_5_n_0 ,\ramplitude_min_reg[15]_i_6_n_0 }));
  FDRE \ramplitude_min_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[16]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[16] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[16]_i_2 
       (.CI(\ramplitude_min_reg_reg[12]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[16]_i_2_n_0 ,\ramplitude_min_reg_reg[16]_i_2_n_1 ,\ramplitude_min_reg_reg[16]_i_2_n_2 ,\ramplitude_min_reg_reg[16]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[16:13]),
        .S(p_0_in[16:13]));
  FDRE \ramplitude_min_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[17]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[18]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[19]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[19] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[19]_i_2 
       (.CI(\ramplitude_min_reg_reg[15]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[19]_i_2_n_0 ,\ramplitude_min_reg_reg[19]_i_2_n_1 ,\ramplitude_min_reg_reg[19]_i_2_n_2 ,\ramplitude_min_reg_reg[19]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg_reg_n_0_[19] ,\ramplitude_min_reg_reg_n_0_[18] ,\ramplitude_min_reg_reg_n_0_[17] ,\ramplitude_min_reg_reg_n_0_[16] }),
        .O(ramplitude_min_reg0[19:16]),
        .S({\ramplitude_min_reg[19]_i_3_n_0 ,\ramplitude_min_reg[19]_i_4_n_0 ,\ramplitude_min_reg[19]_i_5_n_0 ,\ramplitude_min_reg[19]_i_6_n_0 }));
  FDRE \ramplitude_min_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[1]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[20]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[20] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[20]_i_2 
       (.CI(\ramplitude_min_reg_reg[16]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[20]_i_2_n_0 ,\ramplitude_min_reg_reg[20]_i_2_n_1 ,\ramplitude_min_reg_reg[20]_i_2_n_2 ,\ramplitude_min_reg_reg[20]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[20:17]),
        .S(p_0_in[20:17]));
  FDRE \ramplitude_min_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[21]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[21] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[22]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[22] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[23]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[23] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[23]_i_2 
       (.CI(\ramplitude_min_reg_reg[19]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[23]_i_2_n_0 ,\ramplitude_min_reg_reg[23]_i_2_n_1 ,\ramplitude_min_reg_reg[23]_i_2_n_2 ,\ramplitude_min_reg_reg[23]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg_reg_n_0_[23] ,\ramplitude_min_reg_reg_n_0_[22] ,\ramplitude_min_reg_reg_n_0_[21] ,\ramplitude_min_reg_reg_n_0_[20] }),
        .O(ramplitude_min_reg0[23:20]),
        .S({\ramplitude_min_reg[23]_i_3_n_0 ,\ramplitude_min_reg[23]_i_4_n_0 ,\ramplitude_min_reg[23]_i_5_n_0 ,\ramplitude_min_reg[23]_i_6_n_0 }));
  FDRE \ramplitude_min_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[24]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[24] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[24]_i_2 
       (.CI(\ramplitude_min_reg_reg[20]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[24]_i_2_n_0 ,\ramplitude_min_reg_reg[24]_i_2_n_1 ,\ramplitude_min_reg_reg[24]_i_2_n_2 ,\ramplitude_min_reg_reg[24]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[24:21]),
        .S(p_0_in[24:21]));
  FDRE \ramplitude_min_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[25]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[25] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[26]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[26] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[27]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[27] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[27]_i_2 
       (.CI(\ramplitude_min_reg_reg[23]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[27]_i_2_n_0 ,\ramplitude_min_reg_reg[27]_i_2_n_1 ,\ramplitude_min_reg_reg[27]_i_2_n_2 ,\ramplitude_min_reg_reg[27]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg_reg_n_0_[27] ,\ramplitude_min_reg_reg_n_0_[26] ,\ramplitude_min_reg_reg_n_0_[25] ,\ramplitude_min_reg_reg_n_0_[24] }),
        .O(ramplitude_min_reg0[27:24]),
        .S({\ramplitude_min_reg[27]_i_3_n_0 ,\ramplitude_min_reg[27]_i_4_n_0 ,\ramplitude_min_reg[27]_i_5_n_0 ,\ramplitude_min_reg[27]_i_6_n_0 }));
  FDRE \ramplitude_min_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[28]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[28] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[28]_i_2 
       (.CI(\ramplitude_min_reg_reg[24]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[28]_i_2_n_0 ,\ramplitude_min_reg_reg[28]_i_2_n_1 ,\ramplitude_min_reg_reg[28]_i_2_n_2 ,\ramplitude_min_reg_reg[28]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[28:25]),
        .S(p_0_in[28:25]));
  FDRE \ramplitude_min_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[29]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[29] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[2]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[30]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[30] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[31]_i_2_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[31] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[31]_i_10 
       (.CI(\ramplitude_min_reg_reg[31]_i_50_n_0 ),
        .CO({\ramplitude_min_reg_reg[31]_i_10_n_0 ,\ramplitude_min_reg_reg[31]_i_10_n_1 ,\ramplitude_min_reg_reg[31]_i_10_n_2 ,\ramplitude_min_reg_reg[31]_i_10_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg[31]_i_51_n_0 ,\ramplitude_min_reg[31]_i_52_n_0 ,\ramplitude_min_reg[31]_i_53_n_0 ,\ramplitude_min_reg[31]_i_54_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_10_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_55_n_0 ,\ramplitude_min_reg[31]_i_56_n_0 ,\ramplitude_min_reg[31]_i_57_n_0 ,\ramplitude_min_reg[31]_i_58_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_19 
       (.CI(\ramplitude_min_reg_reg[31]_i_59_n_0 ),
        .CO({\ramplitude_min_reg_reg[31]_i_19_n_0 ,\ramplitude_min_reg_reg[31]_i_19_n_1 ,\ramplitude_min_reg_reg[31]_i_19_n_2 ,\ramplitude_min_reg_reg[31]_i_19_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg[31]_i_60_n_0 ,\ramplitude_min_reg[31]_i_61_n_0 ,\ramplitude_min_reg[31]_i_62_n_0 ,\ramplitude_min_reg[31]_i_63_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_19_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_64_n_0 ,\ramplitude_min_reg[31]_i_65_n_0 ,\ramplitude_min_reg[31]_i_66_n_0 ,\ramplitude_min_reg[31]_i_67_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_3 
       (.CI(\ramplitude_min_reg_reg[31]_i_10_n_0 ),
        .CO({\ramplitude_min_reg_reg[31]_i_3_n_0 ,\ramplitude_min_reg_reg[31]_i_3_n_1 ,\ramplitude_min_reg_reg[31]_i_3_n_2 ,\ramplitude_min_reg_reg[31]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg[31]_i_11_n_0 ,\ramplitude_min_reg[31]_i_12_n_0 ,\ramplitude_min_reg[31]_i_13_n_0 ,\ramplitude_min_reg[31]_i_14_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_3_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_15_n_0 ,\ramplitude_min_reg[31]_i_16_n_0 ,\ramplitude_min_reg[31]_i_17_n_0 ,\ramplitude_min_reg[31]_i_18_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_33 
       (.CI(\ramplitude_min_reg_reg[31]_i_68_n_0 ),
        .CO({\ramplitude_min_reg_reg[31]_i_33_n_0 ,\ramplitude_min_reg_reg[31]_i_33_n_1 ,\ramplitude_min_reg_reg[31]_i_33_n_2 ,\ramplitude_min_reg_reg[31]_i_33_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg[31]_i_69_n_0 ,\ramplitude_min_reg[31]_i_70_n_0 ,\ramplitude_min_reg[31]_i_71_n_0 ,\ramplitude_min_reg[31]_i_72_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_33_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_73_n_0 ,\ramplitude_min_reg[31]_i_74_n_0 ,\ramplitude_min_reg[31]_i_75_n_0 ,\ramplitude_min_reg[31]_i_76_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_4 
       (.CI(\ramplitude_min_reg_reg[31]_i_19_n_0 ),
        .CO({\ramplitude_min_reg_reg[31]_i_4_n_0 ,\ramplitude_min_reg_reg[31]_i_4_n_1 ,\ramplitude_min_reg_reg[31]_i_4_n_2 ,\ramplitude_min_reg_reg[31]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg[31]_i_20_n_0 ,\ramplitude_min_reg[31]_i_21_n_0 ,\ramplitude_min_reg[31]_i_22_n_0 ,\ramplitude_min_reg[31]_i_23_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_4_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_24_n_0 ,\ramplitude_min_reg[31]_i_25_n_0 ,\ramplitude_min_reg[31]_i_26_n_0 ,\ramplitude_min_reg[31]_i_27_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_5 
       (.CI(\ramplitude_min_reg_reg[28]_i_2_n_0 ),
        .CO({\NLW_ramplitude_min_reg_reg[31]_i_5_CO_UNCONNECTED [3:2],\ramplitude_min_reg_reg[31]_i_5_n_2 ,\ramplitude_min_reg_reg[31]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_ramplitude_min_reg_reg[31]_i_5_O_UNCONNECTED [3],data0[31:29]}),
        .S({1'b0,\ramplitude_min_reg[31]_i_28_n_0 ,p_0_in[30:29]}));
  CARRY4 \ramplitude_min_reg_reg[31]_i_50 
       (.CI(\ramplitude_min_reg_reg[31]_i_81_n_0 ),
        .CO({\ramplitude_min_reg_reg[31]_i_50_n_0 ,\ramplitude_min_reg_reg[31]_i_50_n_1 ,\ramplitude_min_reg_reg[31]_i_50_n_2 ,\ramplitude_min_reg_reg[31]_i_50_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg[31]_i_82_n_0 ,\ramplitude_min_reg[31]_i_83_n_0 ,\ramplitude_min_reg[31]_i_84_n_0 ,\ramplitude_min_reg[31]_i_85_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_50_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_86_n_0 ,\ramplitude_min_reg[31]_i_87_n_0 ,\ramplitude_min_reg[31]_i_88_n_0 ,\ramplitude_min_reg[31]_i_89_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_59 
       (.CI(\ramplitude_min_reg_reg[31]_i_90_n_0 ),
        .CO({\ramplitude_min_reg_reg[31]_i_59_n_0 ,\ramplitude_min_reg_reg[31]_i_59_n_1 ,\ramplitude_min_reg_reg[31]_i_59_n_2 ,\ramplitude_min_reg_reg[31]_i_59_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg[31]_i_91_n_0 ,\ramplitude_min_reg[31]_i_92_n_0 ,\ramplitude_min_reg[31]_i_93_n_0 ,\ramplitude_min_reg[31]_i_94_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_59_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_95_n_0 ,\ramplitude_min_reg[31]_i_96_n_0 ,\ramplitude_min_reg[31]_i_97_n_0 ,\ramplitude_min_reg[31]_i_98_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_68 
       (.CI(\ramplitude_min_reg_reg[31]_i_99_n_0 ),
        .CO({\ramplitude_min_reg_reg[31]_i_68_n_0 ,\ramplitude_min_reg_reg[31]_i_68_n_1 ,\ramplitude_min_reg_reg[31]_i_68_n_2 ,\ramplitude_min_reg_reg[31]_i_68_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg[31]_i_100_n_0 ,\ramplitude_min_reg[31]_i_101_n_0 ,\ramplitude_min_reg[31]_i_102_n_0 ,\ramplitude_min_reg[31]_i_103_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_68_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_104_n_0 ,\ramplitude_min_reg[31]_i_105_n_0 ,\ramplitude_min_reg[31]_i_106_n_0 ,\ramplitude_min_reg[31]_i_107_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_7 
       (.CI(\ramplitude_min_reg_reg[31]_i_33_n_0 ),
        .CO({ramplitude_min_reg1,\ramplitude_min_reg_reg[31]_i_7_n_1 ,\ramplitude_min_reg_reg[31]_i_7_n_2 ,\ramplitude_min_reg_reg[31]_i_7_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg[31]_i_34_n_0 ,\ramplitude_min_reg[31]_i_35_n_0 ,\ramplitude_min_reg[31]_i_36_n_0 ,\ramplitude_min_reg[31]_i_37_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_7_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_38_n_0 ,\ramplitude_min_reg[31]_i_39_n_0 ,\ramplitude_min_reg[31]_i_40_n_0 ,\ramplitude_min_reg[31]_i_41_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_8 
       (.CI(\ramplitude_min_reg_reg[27]_i_2_n_0 ),
        .CO({\NLW_ramplitude_min_reg_reg[31]_i_8_CO_UNCONNECTED [3],\ramplitude_min_reg_reg[31]_i_8_n_1 ,\ramplitude_min_reg_reg[31]_i_8_n_2 ,\ramplitude_min_reg_reg[31]_i_8_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\ramplitude_min_reg_reg_n_0_[30] ,\ramplitude_min_reg_reg_n_0_[29] ,\ramplitude_min_reg_reg_n_0_[28] }),
        .O(ramplitude_min_reg0[31:28]),
        .S({\ramplitude_min_reg[31]_i_42_n_0 ,\ramplitude_min_reg[31]_i_43_n_0 ,\ramplitude_min_reg[31]_i_44_n_0 ,\ramplitude_min_reg[31]_i_45_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_81 
       (.CI(1'b0),
        .CO({\ramplitude_min_reg_reg[31]_i_81_n_0 ,\ramplitude_min_reg_reg[31]_i_81_n_1 ,\ramplitude_min_reg_reg[31]_i_81_n_2 ,\ramplitude_min_reg_reg[31]_i_81_n_3 }),
        .CYINIT(1'b1),
        .DI({\ramplitude_min_reg[31]_i_108_n_0 ,\ramplitude_min_reg[31]_i_109_n_0 ,\ramplitude_min_reg[31]_i_110_n_0 ,\ramplitude_min_reg[31]_i_111_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_81_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_112_n_0 ,\ramplitude_min_reg[31]_i_113_n_0 ,\ramplitude_min_reg[31]_i_114_n_0 ,\ramplitude_min_reg[31]_i_115_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_90 
       (.CI(1'b0),
        .CO({\ramplitude_min_reg_reg[31]_i_90_n_0 ,\ramplitude_min_reg_reg[31]_i_90_n_1 ,\ramplitude_min_reg_reg[31]_i_90_n_2 ,\ramplitude_min_reg_reg[31]_i_90_n_3 }),
        .CYINIT(1'b1),
        .DI({\ramplitude_min_reg[31]_i_116_n_0 ,\ramplitude_min_reg[31]_i_117_n_0 ,\ramplitude_min_reg[31]_i_118_n_0 ,\ramplitude_min_reg[31]_i_119_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_90_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_120_n_0 ,\ramplitude_min_reg[31]_i_121_n_0 ,\ramplitude_min_reg[31]_i_122_n_0 ,\ramplitude_min_reg[31]_i_123_n_0 }));
  CARRY4 \ramplitude_min_reg_reg[31]_i_99 
       (.CI(1'b0),
        .CO({\ramplitude_min_reg_reg[31]_i_99_n_0 ,\ramplitude_min_reg_reg[31]_i_99_n_1 ,\ramplitude_min_reg_reg[31]_i_99_n_2 ,\ramplitude_min_reg_reg[31]_i_99_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg[31]_i_124_n_0 ,\ramplitude_min_reg[31]_i_125_n_0 ,\ramplitude_min_reg[31]_i_126_n_0 ,\ramplitude_min_reg[31]_i_127_n_0 }),
        .O(\NLW_ramplitude_min_reg_reg[31]_i_99_O_UNCONNECTED [3:0]),
        .S({\ramplitude_min_reg[31]_i_128_n_0 ,\ramplitude_min_reg[31]_i_129_n_0 ,\ramplitude_min_reg[31]_i_130_n_0 ,\ramplitude_min_reg[31]_i_131_n_0 }));
  FDRE \ramplitude_min_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[3]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[3] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[3]_i_2 
       (.CI(1'b0),
        .CO({\ramplitude_min_reg_reg[3]_i_2_n_0 ,\ramplitude_min_reg_reg[3]_i_2_n_1 ,\ramplitude_min_reg_reg[3]_i_2_n_2 ,\ramplitude_min_reg_reg[3]_i_2_n_3 }),
        .CYINIT(1'b1),
        .DI({\ramplitude_min_reg_reg_n_0_[3] ,\ramplitude_min_reg_reg_n_0_[2] ,\ramplitude_min_reg_reg_n_0_[1] ,\ramplitude_min_reg_reg_n_0_[0] }),
        .O(ramplitude_min_reg0[3:0]),
        .S({\ramplitude_min_reg[3]_i_3_n_0 ,\ramplitude_min_reg[3]_i_4_n_0 ,\ramplitude_min_reg[3]_i_5_n_0 ,\ramplitude_min_reg[3]_i_6_n_0 }));
  FDRE \ramplitude_min_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[4]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[4] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\ramplitude_min_reg_reg[4]_i_2_n_0 ,\ramplitude_min_reg_reg[4]_i_2_n_1 ,\ramplitude_min_reg_reg[4]_i_2_n_2 ,\ramplitude_min_reg_reg[4]_i_2_n_3 }),
        .CYINIT(p_0_in[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(p_0_in[4:1]));
  FDRE \ramplitude_min_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[5]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[6]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \ramplitude_min_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[7]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[7] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[7]_i_2 
       (.CI(\ramplitude_min_reg_reg[3]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[7]_i_2_n_0 ,\ramplitude_min_reg_reg[7]_i_2_n_1 ,\ramplitude_min_reg_reg[7]_i_2_n_2 ,\ramplitude_min_reg_reg[7]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\ramplitude_min_reg_reg_n_0_[7] ,\ramplitude_min_reg_reg_n_0_[6] ,\ramplitude_min_reg_reg_n_0_[5] ,\ramplitude_min_reg_reg_n_0_[4] }),
        .O(ramplitude_min_reg0[7:4]),
        .S({\ramplitude_min_reg[7]_i_3_n_0 ,\ramplitude_min_reg[7]_i_4_n_0 ,\ramplitude_min_reg[7]_i_5_n_0 ,\ramplitude_min_reg[7]_i_6_n_0 }));
  FDRE \ramplitude_min_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[8]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[8] ),
        .R(1'b0));
  CARRY4 \ramplitude_min_reg_reg[8]_i_2 
       (.CI(\ramplitude_min_reg_reg[4]_i_2_n_0 ),
        .CO({\ramplitude_min_reg_reg[8]_i_2_n_0 ,\ramplitude_min_reg_reg[8]_i_2_n_1 ,\ramplitude_min_reg_reg[8]_i_2_n_2 ,\ramplitude_min_reg_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(p_0_in[8:5]));
  FDRE \ramplitude_min_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\ramplitude_min_reg[31]_i_1_n_0 ),
        .D(\ramplitude_min_reg[9]_i_1_n_0 ),
        .Q(\ramplitude_min_reg_reg_n_0_[9] ),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [0]),
        .Q(ramplitude_step_reg[0]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [10]),
        .Q(ramplitude_step_reg[10]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [11]),
        .Q(ramplitude_step_reg[11]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [12]),
        .Q(ramplitude_step_reg[12]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [13]),
        .Q(ramplitude_step_reg[13]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [14]),
        .Q(ramplitude_step_reg[14]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [15]),
        .Q(ramplitude_step_reg[15]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [16]),
        .Q(ramplitude_step_reg[16]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [17]),
        .Q(ramplitude_step_reg[17]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [18]),
        .Q(ramplitude_step_reg[18]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [19]),
        .Q(ramplitude_step_reg[19]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [1]),
        .Q(ramplitude_step_reg[1]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [20]),
        .Q(ramplitude_step_reg[20]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [21]),
        .Q(ramplitude_step_reg[21]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [22]),
        .Q(ramplitude_step_reg[22]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [23]),
        .Q(ramplitude_step_reg[23]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [24]),
        .Q(ramplitude_step_reg[24]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [25]),
        .Q(ramplitude_step_reg[25]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [26]),
        .Q(ramplitude_step_reg[26]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [27]),
        .Q(ramplitude_step_reg[27]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [28]),
        .Q(ramplitude_step_reg[28]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [29]),
        .Q(ramplitude_step_reg[29]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [2]),
        .Q(ramplitude_step_reg[2]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [30]),
        .Q(ramplitude_step_reg[30]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [31]),
        .Q(ramplitude_step_reg[31]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [3]),
        .Q(ramplitude_step_reg[3]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [4]),
        .Q(ramplitude_step_reg[4]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [5]),
        .Q(ramplitude_step_reg[5]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [6]),
        .Q(ramplitude_step_reg[6]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [7]),
        .Q(ramplitude_step_reg[7]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [8]),
        .Q(ramplitude_step_reg[8]),
        .R(1'b0));
  FDRE \ramplitude_step_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ramplitude_step_reg_reg[31]_0 [9]),
        .Q(ramplitude_step_reg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFF5D55FFFF)) 
    \status_reg[9]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(\loop_locked_counter_reg_reg[30] ),
        .I2(D[2]),
        .I3(D[3]),
        .I4(D[0]),
        .I5(Q),
        .O(ramp_module_0_rst));
endmodule

(* CHECK_LICENSE_TYPE = "system_AXI_PI_0_2,AXI_PI_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "AXI_PI_v1_0,Vivado 2019.1.1" *) 
(* NotValidForBitStream *)
module system_AXI_PI_0_2
   (pid_loop_input,
    axi_pi_output,
    autolock_input,
    output_sel,
    input_reset,
    loop_locked,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  input [13:0]pid_loop_input;
  output [13:0]axi_pi_output;
  input [13:0]autolock_input;
  input output_sel;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 input_reset RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME input_reset, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) output input_reset;
  output loop_locked;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) input [6:0]s00_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [6:0]s00_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 32, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 125000000, ID_WIDTH 0, ADDR_WIDTH 7, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input s00_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 125000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire [13:0]autolock_input;
  wire [13:0]axi_pi_output;
  wire input_reset;
  wire loop_locked;
  wire output_sel;
  wire [13:0]pid_loop_input;
  wire s00_axi_aclk;
  wire [6:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [6:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  system_AXI_PI_0_2_AXI_PI_v1_0 inst
       (.D(loop_locked),
        .S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .autolock_input(autolock_input),
        .axi_pi_output(axi_pi_output),
        .input_reset(input_reset),
        .output_sel(output_sel),
        .pid_loop_input(pid_loop_input),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[6:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[6:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
