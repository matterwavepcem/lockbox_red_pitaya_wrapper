-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1.1 (lin64) Build 2580384 Sat Jun 29 08:04:45 MDT 2019
-- Date        : Thu Nov 14 12:26:58 2019
-- Host        : laoshu running 64-bit unknown
-- Command     : write_vhdl -force -mode funcsim
--               /home/carter/Documents/personal/school/research/mueller/red_pitaya/lockbox/lockbox_project.srcs/sources_1/bd/system/ip/system_diff_io_mgr_0_0/system_diff_io_mgr_0_0_sim_netlist.vhdl
-- Design      : system_diff_io_mgr_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_diff_io_mgr_0_0_diff_io_mgr is
  port (
    dout_n : out STD_LOGIC_VECTOR ( 1 downto 0 );
    din : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_diff_io_mgr_0_0_diff_io_mgr : entity is "diff_io_mgr";
end system_diff_io_mgr_0_0_diff_io_mgr;

architecture STRUCTURE of system_diff_io_mgr_0_0_diff_io_mgr is
begin
\dout_n[0]_INST_0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => din(0),
      O => dout_n(0)
    );
\dout_n[1]_INST_0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => din(1),
      O => dout_n(1)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_diff_io_mgr_0_0 is
  port (
    din : in STD_LOGIC_VECTOR ( 1 downto 0 );
    dout_p : inout STD_LOGIC_VECTOR ( 7 downto 0 );
    dout_n : inout STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of system_diff_io_mgr_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of system_diff_io_mgr_0_0 : entity is "system_diff_io_mgr_0_0,diff_io_mgr,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of system_diff_io_mgr_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of system_diff_io_mgr_0_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of system_diff_io_mgr_0_0 : entity is "diff_io_mgr,Vivado 2019.1.1";
end system_diff_io_mgr_0_0;

architecture STRUCTURE of system_diff_io_mgr_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^din\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^dout_n\ : STD_LOGIC_VECTOR ( 1 downto 0 );
begin
  dout_n(7) <= \<const1>\;
  dout_n(6) <= \<const1>\;
  dout_n(5) <= \<const1>\;
  dout_n(4) <= \<const1>\;
  dout_n(3) <= \<const1>\;
  dout_n(2) <= \<const1>\;
  dout_p(7) <= \<const0>\;
  dout_p(6) <= \<const0>\;
  dout_p(5) <= \<const0>\;
  dout_p(4) <= \<const0>\;
  dout_p(3) <= \<const0>\;
  dout_p(2) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.system_diff_io_mgr_0_0_diff_io_mgr
     port map (
      din(1 downto 0) => \^din\(1 downto 0),
      dout_n(1 downto 0) => dout_n(1 downto 0)
    );
end STRUCTURE;
