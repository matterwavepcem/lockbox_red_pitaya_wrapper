// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1.1 (lin64) Build 2580384 Sat Jun 29 08:04:45 MDT 2019
// Date        : Thu Nov 14 12:26:58 2019
// Host        : laoshu running 64-bit unknown
// Command     : write_verilog -force -mode funcsim
//               /home/carter/Documents/personal/school/research/mueller/red_pitaya/lockbox/lockbox_project.srcs/sources_1/bd/system/ip/system_diff_io_mgr_0_0/system_diff_io_mgr_0_0_sim_netlist.v
// Design      : system_diff_io_mgr_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "system_diff_io_mgr_0_0,diff_io_mgr,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "diff_io_mgr,Vivado 2019.1.1" *) 
(* NotValidForBitStream *)
module system_diff_io_mgr_0_0
   (din,
    dout_p,
    dout_n);
  input [1:0]din;
  inout [7:0]dout_p;
  inout [7:0]dout_n;

  wire \<const0> ;
  wire \<const1> ;
  wire [1:0]din;
  wire [1:0]\^dout_n ;

  assign dout_p[1:0] = din[1:0];
  xVIA dout_n_2via (dout_n[2], dout_n[7]);
  xVIA dout_n_3via (dout_n[3], dout_n[7]);
  xVIA dout_n_4via (dout_n[4], dout_n[7]);
  xVIA dout_n_5via (dout_n[5], dout_n[7]);
  xVIA dout_n_6via (dout_n[6], dout_n[7]);
  xVIA dout_p_2via (dout_p[2], dout_p[7]);
  xVIA dout_p_3via (dout_p[3], dout_p[7]);
  xVIA dout_p_4via (dout_p[4], dout_p[7]);
  xVIA dout_p_5via (dout_p[5], dout_p[7]);
  xVIA dout_p_6via (dout_p[6], dout_p[7]);
  GND GND
       (.G(dout_p[7]));
  VCC VCC
       (.P(dout_n[7]));
  system_diff_io_mgr_0_0_diff_io_mgr inst
       (.din(dout_p[1:0]),
        .dout_n(dout_n[1:0]));
endmodule

(* ORIG_REF_NAME = "diff_io_mgr" *) 
module system_diff_io_mgr_0_0_diff_io_mgr
   (dout_n,
    din);
  output [1:0]dout_n;
  input [1:0]din;

  wire [1:0]din;
  wire [1:0]dout_n;

  LUT1 #(
    .INIT(2'h1)) 
    \dout_n[0]_INST_0 
       (.I0(din[0]),
        .O(dout_n[0]));
  LUT1 #(
    .INIT(2'h1)) 
    \dout_n[1]_INST_0 
       (.I0(din[1]),
        .O(dout_n[1]));
endmodule
module xVIA(.a(w),.b(w));
inout w;
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
