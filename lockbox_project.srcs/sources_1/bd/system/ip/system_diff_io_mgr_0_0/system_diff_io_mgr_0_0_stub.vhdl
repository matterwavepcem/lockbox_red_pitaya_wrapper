-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1.1 (lin64) Build 2580384 Sat Jun 29 08:04:45 MDT 2019
-- Date        : Thu Nov 14 12:26:58 2019
-- Host        : laoshu running 64-bit unknown
-- Command     : write_vhdl -force -mode synth_stub
--               /home/carter/Documents/personal/school/research/mueller/red_pitaya/lockbox/lockbox_project.srcs/sources_1/bd/system/ip/system_diff_io_mgr_0_0/system_diff_io_mgr_0_0_stub.vhdl
-- Design      : system_diff_io_mgr_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity system_diff_io_mgr_0_0 is
  Port ( 
    din : in STD_LOGIC_VECTOR ( 1 downto 0 );
    dout_p : inout STD_LOGIC_VECTOR ( 7 downto 0 );
    dout_n : inout STD_LOGIC_VECTOR ( 7 downto 0 )
  );

end system_diff_io_mgr_0_0;

architecture stub of system_diff_io_mgr_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "din[1:0],dout_p[7:0],dout_n[7:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "diff_io_mgr,Vivado 2019.1.1";
begin
end;
