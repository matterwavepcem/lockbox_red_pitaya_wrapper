// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1.1 (lin64) Build 2580384 Sat Jun 29 08:04:45 MDT 2019
// Date        : Thu Nov 14 12:26:58 2019
// Host        : laoshu running 64-bit unknown
// Command     : write_verilog -force -mode synth_stub
//               /home/carter/Documents/personal/school/research/mueller/red_pitaya/lockbox/lockbox_project.srcs/sources_1/bd/system/ip/system_diff_io_mgr_0_0/system_diff_io_mgr_0_0_stub.v
// Design      : system_diff_io_mgr_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "diff_io_mgr,Vivado 2019.1.1" *)
module system_diff_io_mgr_0_0(din, dout_p, dout_n)
/* synthesis syn_black_box black_box_pad_pin="din[1:0],dout_p[7:0],dout_n[7:0]" */;
  input [1:0]din;
  inout [7:0]dout_p;
  inout [7:0]dout_n;
endmodule
